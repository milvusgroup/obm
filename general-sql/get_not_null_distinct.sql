CREATE OR REPLACE FUNCTION get_distinct_values(tbl varchar, col varchar) RETURNS TABLE (name varchar, count bigint) AS $$
BEGIN
    RETURN QUERY EXECUTE format('SELECT distinct %I::varchar, count(*) FROM %I WHERE %I IS NOT NULL GROUP BY %I', col, tbl, col, col);
    
END; 
$$ 
LANGUAGE 'plpgsql' IMMUTABLE STRICT;