INSERT INTO elc_rules (data_table,row_id,read,write,sensitivity) 
SELECT 'elc', m.obm_id, u."group", ARRAY[u.uploader_id], m.obm_access 
FROM elc m 
LEFT JOIN uploadings u ON m.obm_uploading_id = u.id;