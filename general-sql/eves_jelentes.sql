DROP TABLE tmp.eves_jelentes_2020;
CREATE TABLE tmp.eves_jelentes_2020 AS
WITH uat as (
    SELECT id, name, geometry, geometry_4326, judet FROM shared.romania_administrative_units WHERE natlevel = '3rdOrder'
),
obm as (
    SELECT DISTINCT
        comuna, judet
    FROM milvus m 
    LEFT JOIN milvus_qgrids ON data_table = 'milvus' AND row_id = obm_id
    WHERE 
        m.date BETWEEN '2020-01-01' AND '2020-12-31'
),
omm_uat as (
    SELECT DISTINCT id, name, geometry, 'omm' as src FROM uat
    LEFT JOIN mammalia mm ON mm.date BETWEEN '2020-01-01' AND '2020-12-31' AND ST_Intersects(mm.obm_geometry, uat.geometry_4326)
    WHERE mm.obm_id IS NOT NULL
),
ohm_uat as (
    SELECT DISTINCT id, name, geometry, 'ohm' as src FROM uat
    LEFT JOIN openherpmaps ohm ON ohm.data BETWEEN '2020-01-01' AND '2020-12-31' AND ST_Intersects(ohm.obm_geometry, uat.geometry_4326)
    WHERE ohm.obm_id IS NOT NULL
),
obm_uat as (
    SELECT DISTINCT id, name, geometry, 'obm' as src FROM uat
    LEFT JOIN obm ON obm.comuna = uat.name AND obm.judet = uat.judet
    WHERE obm.comuna IS NOT NULL
),
un as (
    SELECT * FROM obm_uat 
    UNION 
    SELECT * FROM omm_uat
    UNION 
    SELECT * FROM ohm_uat
)
SELECT ROW_NUMBER() OVER () as rn, un.* FROM un;

ALTER TABLE tmp.eves_jelentes_2020 ADD CONSTRAINT rn_pk PRIMARY KEY (rn);
CREATE INDEX eves_jelentes_2020_geom_idx
  ON tmp.eves_jelentes_2020
  USING GIST (geometry);

grant all on tmp.eves_jelentes_2020 TO milvus_admin;


DROP TABLE tmp.eves_jelentes_obm_2020;
CREATE TABLE tmp.eves_jelentes_obm_2020 AS
WITH obm as (
    SELECT ST_Centroid(obm_geometry) as geom, 'obm' as project
    FROM milvus m 
    WHERE 
        m.date BETWEEN '2020-01-01' AND '2020-12-31'
),
omm as (
    SELECT ST_Centroid(obm_geometry) as geom, 'omm' as project
    FROM mammalia mm
    WHERE 
        mm.date BETWEEN '2020-01-01' AND '2020-12-31' 
),
ohm as (
    SELECT ST_Centroid(obm_geometry) as geom, 'ohm' as project
    FROM openherpmaps ohm 
    WHERE ohm.data BETWEEN '2020-01-01' AND '2020-12-31'
),
un as (
    SELECT * FROM obm 
    UNION 
    SELECT * FROM omm
    UNION 
    SELECT * FROM ohm
)
SELECT ROW_NUMBER() OVER () as rn, un.* FROM un;

ALTER TABLE tmp.eves_jelentes_obm_2020 ADD CONSTRAINT rn_pk PRIMARY KEY (rn);
CREATE INDEX eves_jelentes_obm_2020_geom_idx
  ON tmp.eves_jelentes_obm_2020
  USING GIST (geom);

grant all on tmp.eves_jelentes_obm_2020 TO milvus_admin;

-- 2021

DROP TABLE tmp.eves_jelentes_2021;
CREATE TABLE tmp.eves_jelentes_2021 AS
WITH uat as (
    SELECT id, name, geometry, geometry_4326, judet FROM shared.romania_administrative_units WHERE natlevel = '3rdOrder'
),
obm as (
    SELECT DISTINCT
        comuna, judet
    FROM milvus m 
    LEFT JOIN milvus_qgrids ON data_table = 'milvus' AND row_id = obm_id
    WHERE 
        m.date BETWEEN '2021-01-01' AND '2021-12-31'
),
ohm as (
    SELECT DISTINCT
        comuna, judet
    FROM openherpmaps m 
    LEFT JOIN openherpmaps_qgrids ON data_table = 'openherpmaps' AND row_id = obm_id
    WHERE 
        m.data BETWEEN '2021-01-01' AND '2021-12-31'
),
omm as (
    SELECT DISTINCT
        comuna, judet
    FROM mammalia m 
    LEFT JOIN mammalia_qgrids ON data_table = 'mammalia' AND row_id = obm_id
    WHERE 
        m.date BETWEEN '2021-01-01' AND '2021-12-31'
),
omm_uat as (
    SELECT DISTINCT id, name, geometry, 'omm' as src FROM uat
    LEFT JOIN omm ON omm.comuna = uat.name AND omm.judet = uat.judet
    WHERE omm.comuna IS NOT NULL
),
ohm_uat as (
    SELECT DISTINCT id, name, geometry, 'ohm' as src FROM uat
    LEFT JOIN ohm ON ohm.comuna = uat.name AND ohm.judet = uat.judet
    WHERE ohm.comuna IS NOT NULL
),
obm_uat as (
    SELECT DISTINCT id, name, geometry, 'obm' as src FROM uat
    LEFT JOIN obm ON obm.comuna = uat.name AND obm.judet = uat.judet
    WHERE obm.comuna IS NOT NULL
),
un as (
    SELECT * FROM obm_uat 
    UNION 
    SELECT * FROM omm_uat
    UNION 
    SELECT * FROM ohm_uat
)
SELECT ROW_NUMBER() OVER () as rn, un.* FROM un;

ALTER TABLE tmp.eves_jelentes_2021 ADD CONSTRAINT rn_pk PRIMARY KEY (rn);
CREATE INDEX eves_jelentes_2021_geom_idx
  ON tmp.eves_jelentes_2021
  USING GIST (geometry);

grant all on tmp.eves_jelentes_2021 TO milvus_admin;


DROP TABLE tmp.eves_jelentes_obm_2021;
CREATE TABLE tmp.eves_jelentes_obm_2021 AS
WITH obm as (
    SELECT ST_Centroid(obm_geometry) as geom, 'obm' as project
    FROM milvus m 
    WHERE 
        m.date BETWEEN '2021-01-01' AND '2021-12-31'
),
omm as (
    SELECT ST_Centroid(obm_geometry) as geom, 'omm' as project
    FROM mammalia mm
    WHERE 
        mm.date BETWEEN '2021-01-01' AND '2021-12-31' 
),
ohm as (
    SELECT ST_Centroid(obm_geometry) as geom, 'ohm' as project
    FROM openherpmaps ohm 
    WHERE ohm.data BETWEEN '2021-01-01' AND '2021-12-31'
),
un as (
    SELECT * FROM obm 
    UNION 
    SELECT * FROM omm
    UNION 
    SELECT * FROM ohm
)
SELECT ROW_NUMBER() OVER () as rn, un.* FROM un;

ALTER TABLE tmp.eves_jelentes_obm_2021 ADD CONSTRAINT rn_pk PRIMARY KEY (rn);
CREATE INDEX eves_jelentes_obm_2021_geom_idx
  ON tmp.eves_jelentes_obm_2021
  USING GIST (geom);

grant all on tmp.eves_jelentes_obm_2021 TO milvus_admin;
