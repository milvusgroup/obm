CREATE OR REPLACE FUNCTION "add_term" ("term" character varying(128), "tbl" character varying(64), "subj" character varying(128), "project" character varying(64) , "termstable" character varying(64) default NULL) RETURNS integer LANGUAGE plpgsql AS $$
DECLARE
sid integer;
BEGIN
    IF term IS NOT NULL AND tbl IS NOT NULL AND subj IS NOT NULL THEN
        IF termstable IS NULL THEN                
            EXECUTE format('SELECT obm_id FROM %s_search ms WHERE word = %L AND "ms"."subject" = %L AND data_table = %L;', project, term, subj, tbl) INTO sid;
            IF sid IS NULL THEN
                EXECUTE format('INSERT INTO %4$s_search (data_table,subject,word,taxon_db) SELECT %2$L,%3$L,%1$L,1 RETURNING obm_id;',term, tbl, subj, project) INTO sid;
            END IF;
        ELSIF termstable = 'taxon' THEN
            EXECUTE format('SELECT taxon_id FROM %s_taxon WHERE word = %L;', project, term) INTO sid;
            IF (sid IS NULL) THEN
                EXECUTE format('INSERT INTO %2$s_taxon (lang,word,taxon_db) SELECT %3$L,%1$L,1 RETURNING taxon_id',term, project, subj);
            END IF;
        ELSIF termstable = 'terms' THEN
            EXECUTE format('SELECT term_id FROM %s_terms ms WHERE term = %L AND "ms"."subject" = %L AND data_table = %L;', project, term, subj, tbl) INTO sid;
            IF sid IS NULL THEN
                EXECUTE format('INSERT INTO %4$s_terms (data_table,subject,term,status,taxon_db) SELECT %2$L,%3$L,%1$L,''undefined'', 1 RETURNING term_id;',term, tbl, subj, project) INTO sid;
            END IF;
        END IF;
        RETURN sid;
    END IF;
END;
$$;
