CREATE OR REPLACE FUNCTION "remove_term" ("term" character varying(128), "tbl" character varying(64), "subj" character varying(128), "project" character varying(64) , "termstable" character varying(64) default NULL) RETURNS void LANGUAGE plpgsql AS $$
BEGIN
	IF term IS NOT NULL AND tbl IS NOT NULL AND subj IS NOT NULL THEN
        IF termstable IS NULL THEN
            EXECUTE format('DELETE FROM %4$s_search WHERE data_table = %3$L AND subject = %1$L AND word = %2$L AND taxon_db = 0;',subj, term, tbl, project);
        ELSIF termstable = 'taxon' THEN
            EXECUTE format('DELETE FROM %2$s_taxon WHERE word = %1$L AND taxon_db = 0;', term, project);
        ELSIF termstable = 'terms' THEN
            EXECUTE format('DELETE FROM %4$s_terms WHERE data_table = %3$L AND subject = %1$L AND term = %2$L AND taxon_db = 0;', subj, term, tbl, project);
        END IF;
	END IF;
END;
$$;
