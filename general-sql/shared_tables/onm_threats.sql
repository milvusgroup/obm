DROP VIEW shared.onm_threats;
CREATE OR REPLACE VIEW shared.onm_threats AS
SELECT 
    'obm' as project, obm_id, obm_geometry, obm_files_id, date, site, 
    observers, x, y, impact_type, impact_l1, impact_l2, impact_l3, impact_l4, 
    impact_custom, impact_intensity, time, observations, 
    threatened_species_group, management_measure
FROM milvus_threats
UNION
SELECT 
    'omm' as project, obm_id, obm_geometry, obm_files_id, date, site, 
    observers, x, y, impact_type, impact_l1, impact_l2, impact_l3, impact_l4, 
    impact_custom, impact_intensity, time, observations, 
    threatened_species_group, management_measure
FROM mammalia_threats
UNION
SELECT 
    'ohm' as project, obm_id, obm_geometry, obm_files_id, date, site, 
    observers, x, y, impact_type, impact_l1, impact_l2, impact_l3, impact_l4, 
    impact_custom, impact_intensity, time, observations, 
    threatened_species_group, management_measure
FROM openherpmaps_threats
ORDER BY project, obm_id;

GRANT SELECT ON shared.onm_threats TO milvus_admin;
GRANT SELECT ON shared.onm_threats TO mammalia_admin;
GRANT SELECT ON shared.onm_threats TO openherpmaps_admin;
