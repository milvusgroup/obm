CREATE TABLE shared.amenintari_wide AS SELECT * FROM elc_threats_mv;
ALTER TABLE ONLY shared.amenintari_wide ADD CONSTRAINT amenintari_wide_uid_pkey PRIMARY KEY (uid);
GRANT SELECT ON shared.amenintari_wide TO milvus_admin;
GRANT SELECT ON shared.amenintari_wide TO openherpmaps_admin;
GRANT SELECT ON shared.amenintari_wide TO mammalia_admin;
GRANT SELECT ON shared.amenintari_wide TO pisces_admin;
GRANT SELECT ON shared.amenintari_wide TO elc_admin;
