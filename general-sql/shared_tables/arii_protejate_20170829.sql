ALTER TABLE shared.arii_protejate_20170829 ADD COLUMN nume varchar;
BEGIN; 
WITH nam As (
    SELECT DISTINCT q.sci, a.nume 
    FROM milvus_qgrids q 
    LEFT JOIN shared.arii_protejate_20170829 a ON tip_ap = 'Sit de importanță comunitară' AND q.sci = a.localid 
    WHERE q.sci != 'NA'
) 
UPDATE milvus_qgrids qq SET sci = nam.nume FROM nam WHERE nam.sci = qq.sci;
COMMIT;

BEGIN; 
WITH nam As (
    SELECT DISTINCT q.spa, a.nume 
    FROM milvus_qgrids q 
    LEFT JOIN shared.arii_protejate_20170829 a ON tip_ap = 'Arie de protecție specială avifaunistică' AND q.spa = a.localid 
    WHERE q.spa != 'NA'
) 
UPDATE milvus_qgrids qq SET spa = nam.nume FROM nam WHERE nam.spa = qq.spa;
COMMIT;
