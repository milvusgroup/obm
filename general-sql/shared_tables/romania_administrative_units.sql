CREATE INDEX romania_administrative_units_geometry_4326_idx ON shared.romania_administrative_units USING GIST (geometry_4326);
ALTER TABLE shared.romania_administrative_units ADD COLUMN geom_simpl geometry;
UPDATE shared.romania_administrative_units SET geom_simpl = ST_SimplifyPreserveTopology(geometry_4326, 0.01);