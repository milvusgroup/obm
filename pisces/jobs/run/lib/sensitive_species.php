<?php
class sensitive_species extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('sensitive_species initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID, $BID;

        $params = parent::getJobParams(__CLASS__);
        $sensitive_species = $params;

        $cmd = sprintf("UPDATE pisces SET access = 1 WHERE species_sci IN (%s) AND access::integer < 1 RETURNING obm_id", implode(',',array_map('quote',$sensitive_species)));
        if (!$res = pg_query($ID, $cmd)){
            job_log('Query error');
            exit();
        }

        if ($results = pg_fetch_all($res)) {
            $ids = array_column($results,'obm_id');
            $msg = "obm_access of the following records has been updated: (".implode(', ',$ids).")";
        }
        else
            $msg = "no visible sensitive data found";

        job_log($msg);


    }
}
?>