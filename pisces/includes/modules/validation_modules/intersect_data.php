<?php
class intersect_data extends validation_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('intersect_data initialized', __FILE__, __LINE__);
        return true;
    }

    public function get_results() {
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        $cmdpart = [];
        $tblHeader = ['total rows'];
        foreach ($params as $col => $opts) {
            if ($col = 'dem') {
                continue;
            }
            $cmdpart[] = "count(CASE WHEN $col IS NOT NULL THEN 1 END) AS $col";
            $tblHeader[] = $col;
        }
        if (!count($cmdpart)) {
            return 'Params not defined';
        }
        $cmd = sprintf("SELECT count(*) AS total, %s FROM %s_qgrids;", implode(', ', $cmdpart), PROJECTTABLE);

        if (!$res = parent::query($ID,$cmd))
            return 'ERROR: query error';


        $results = pg_fetch_assoc($res[0]);

        $t = [$results['total']];
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader($tblHeader);

        foreach ($params as $col => $opts) {
            if ($col = 'dem') {
                continue;
            }
            $t[] = $results[$col]. ' ('. round($results[$col] * 100 / $t[0],2) .'%)';
        }

        $tbl->addRows($t);

        return $tbl->printOut();
    }


    static function run() {
        global $ID;
        $params = parent::getJobParams(__CLASS__);
        $cmd = [];
        $limit = round(1000 / count((array)$params));
        foreach ($params as $col => $opts) {
            if (in_array($col, ['dem'])) {
                $method = "intersect_with_$col";
                if (method_exists(__CLASS__,$method)) {
                    self::$method();
                }
                continue;
            }
            $filter = (isset($opts->filterColumn) && isset($opts->filterValue))
                ? "g.{$opts->filterColumn} = '{$opts->filterValue}'"
                : "1 = 1";

            $transform = (isset($opts->srid))
                ? sprintf("ST_Transform(rqg.centroid,%s)", $opts->srid)
                : "rqg.centroid";

            $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.%2$s FROM %9$s_qgrids rqg LEFT JOIN %3$s.%4$s g ON ST_Intersects(%6$s,%7$s) WHERE %1$s IS NULL AND %5$s ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = rows.%2$s FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;',
                $col,
                $opts->valueColumn,
                $opts->schema,
                $opts->table,
                $filter,
                $transform,
                $opts->geomColumn,
                $limit,
                PROJECTTABLE
            );
            if ($opts->valueColumn !== 'geometry') {
                $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id FROM %9$s_qgrids rqg WHERE %1$s IS NULL AND NOT EXISTS (SELECT 1 FROM %3$s.%4$s g WHERE ST_Intersects(%6$s,%7$s) AND %5$s) ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = \'NA\' FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;',
                    $col,
                    $opts->valueColumn,
                    $opts->schema,
                    $opts->table,
                    $filter,
                    $transform,
                    $opts->geomColumn,
                    $limit,
                    PROJECTTABLE
                );
            }
        }
        if (count($cmd))
            parent::query($ID, $cmd);
    }

    static function intersect_with_dem() {
        global $ID;
        $cmd = [];

        // főtábla
        $cmd[] = " WITH rows AS (
            SELECT obm_id, round(st_value(rast, ST_Centroid(obm_geometry))::numeric,2) as altitude
            FROM pisces p LEFT JOIN shared.eu_dem_v11_e50n20 ON ST_Intersects(rast, ST_Centroid(obm_geometry))
            WHERE p.altitude IS NULL LIMIT 1000
        )
        UPDATE pisces p SET altitude = rows.altitude FROM rows WHERE rows.obm_id = p.obm_id;";

        parent::query($ID, $cmd);
    }
}
?>
