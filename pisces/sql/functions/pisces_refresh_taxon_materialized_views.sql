CREATE OR REPLACE FUNCTION "pisces_refresh_taxon_materialized_views" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW pisces.pisces_taxon_valid;
    REFRESH MATERIALIZED VIEW pisces.pisces_taxon_magyar;
    REFRESH MATERIALIZED VIEW pisces.pisces_taxon_roman;
    REFRESH MATERIALIZED VIEW pisces.pisces_taxon_angol;
    RETURN NULL;
END
$$;
