CREATE OR REPLACE FUNCTION "public"."pisces_rules_f" () RETURNS trigger AS 
$$
DECLARE
 sens integer DEFAULT 0;
BEGIN
    IF TG_TABLE_NAME  = 'pisces_survey_records' THEN
        IF tg_op = 'INSERT' THEN
            sens := new."obm_access";
            
            execute format('INSERT INTO pisces_rules ("data_table","row_id","write","read","sensitivity") 
                            SELECT ''pisces'',%L,ARRAY[uploader_id],%L,%L FROM uploadings WHERE id = %L',
                            new.obm_id,
                            (SELECT uploader_id || "group" FROM uploadings WHERE id = new.obm_uploading_id),
                            sens,
                            new.obm_uploading_id
                    );
            RETURN new; 
        ELSIF tg_op = 'UPDATE' AND OLD."obm_access" != NEW."obm_access" THEN
            sens := new."obm_access";
            execute format('UPDATE pisces_rules SET sensitivity = %L WHERE row_id = %L',sens,NEW.obm_id);
            RETURN new; 
        ELSE
            RETURN new;
        END IF;
    ELSIF TG_TABLE_NAME  = 'pisces_survey_events' AND tg_op = 'UPDATE' AND OLD."obm_access" != NEW."obm_access" THEN
        sens := new."obm_access";
        execute format('UPDATE pisces_rules SET sensitivity = %L WHERE row_id IN (SELECT obm_id FROM pisces_survey_records WHERE event_id = %L)',sens,NEW.obm_id);
        RETURN new;
    ELSIF TG_TABLE_NAME  = 'pisces_survey_sites' AND tg_op = 'UPDATE' AND OLD."obm_access" != NEW."obm_access" THEN
        sens := new."obm_access";
        execute format('UPDATE pisces_rules SET sensitivity = %L WHERE row_id IN (
                            SELECT obm_id 
                            FROM pisces_survey_records r
                            INNER JOIN pisces_survey_events e ON e.obm_id = r.event_id 
                            WHERE e.site_id = %L)',sens,NEW.obm_id);
        RETURN new; 
    END IF;
    return new;
END; 
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;