CREATE OR REPLACE FUNCTION "system"."pisces_survey_events_l_sync_row" () RETURNS trigger AS 
$$
DECLARE 
    tbl varchar;
    row_id integer;
BEGIN
    tbl := 'pisces';
    IF (TG_OP = 'UPDATE' AND new.team != old.team) THEN
        row_id = NEW.obm_id;
        EXECUTE FORMAT('UPDATE system.%s_linnaeus SET updated_at = NOW() WHERE row_id IN (SELECT obm_id FROM pisces_survey_records WHERE event_id = %L);', tbl, row_id);
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;