CREATE OR REPLACE FUNCTION "update_pisces_taxonlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    observer RECORD;
    tbl varchar(64) = TG_TABLE_NAME;
    sid integer;
    sids integer[] DEFAULT '{}';
    term varchar(128);
BEGIN
    IF tg_op = 'INSERT' THEN
        term := NEW.species;
        PERFORM add_term(term, tbl, 'species','pisces','taxon'::varchar);

        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  OLD.species != NEW.species THEN
            PERFORM add_term(NEW.species, tbl, 'species','pisces','taxon'::varchar);
        END IF;


        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        RETURN OLD;

    END IF;                                                                                         
END;
$$;

-- INSERT INTO pisces_search_connect (data_table, row_id, observers_ids) SELECT 'pisces', foo.obm_id, array_agg(search_id) FROM (SELECT obm_id, unnest(regexp_split_to_array(concat_ws(',',observers),E'[,;]\\s*')) as names FROM pisces ) as foo LEFT JOIN pisces_search o ON names = word GROUP by foo.obm_id;

--UPDATE pisces_search_connect sc SET species_ids = ARRAY[search_id] FROM (SELECT search_id, b.obm_id FROM pisces b LEFT JOIN pisces_search tx ON b.species = tx.word AND b.species IS NOT NULL WHERE b.obm_id > 221) as t WHERE sc.row_id = t.obm_id;

