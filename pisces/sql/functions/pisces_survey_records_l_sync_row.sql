CREATE OR REPLACE FUNCTION "system"."pisces_survey_records_l_sync_row" () RETURNS trigger AS 
$$
DECLARE 
    tbl varchar;
    row_id integer;
BEGIN
    tbl := 'pisces';
    IF (TG_OP = 'INSERT') THEN
        row_id = NEW.obm_id;
        EXECUTE FORMAT('INSERT INTO system.%s_linnaeus (row_id) VALUES (%L);', tbl, row_id);
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
