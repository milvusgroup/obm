CREATE OR REPLACE FUNCTION "public"."pisces_survey_sites_update_qgrids" () RETURNS trigger AS 
$$
DECLARE
 tbl varchar;
 geom geometry;
 centr geometry;
 etrs RECORD;
BEGIN
    tbl := TG_TABLE_NAME;
    IF tg_op = 'INSERT' THEN

        geom := new."obm_geometry";
		centr := ST_Centroid(geom);
        
        SELECT geometry,name INTO etrs FROM shared.etrs10 g WHERE ST_Intersects(centr, g.geometry);

        execute format('INSERT INTO pisces_survey_sites_qgrids ("data_table","row_id","original","etrs_geom",etrs_name, centroid) VALUES (%L,%L,%L,%L,%L, %L);',tbl,new.obm_id,new.obm_geometry,etrs.geometry, '10km'||etrs.name, centr);

        RETURN new; 

    ELSIF tg_op = 'UPDATE' AND ST_Equals(OLD."obm_geometry",NEW."obm_geometry") THEN

        geom := new."obm_geometry";
		centr := ST_Centroid(geom);
        
        SELECT geometry,name INTO etrs FROM shared.etrs10 g WHERE ST_Intersects(centr, g.geometry);
        
        EXECUTE format('UPDATE pisces_survey_sites_qgrids set "original" = %L, "etrs_geom" = %L, etrs_name = %L, centroid = %L WHERE data_table = %L AND row_id = %L;',new.obm_geometry,etrs.geometry,'10km'||etrs.name,centr,tbl,new.obm_id);
        RETURN new; 

    ELSIF tg_op = 'DELETE' THEN
        EXECUTE format('DELETE FROM pisces_survey_sites_qgrids WHERE data_table = %L AND row_id = %L',tbl, OLD.obm_id);
        RETURN old;
    ELSE
        RETURN new;
    END IF;
END; 
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
