CREATE OR REPLACE FUNCTION "system"."pisces_add_term" () RETURNS trigger AS 
$$
DECLARE 
	project varchar; 
	tbl varchar; 
	sbj varchar; 
	trm varchar;
BEGIN
	-- do not modify this function it is generated automatically
 	project := 'pisces'; tbl := 'pisces';
	sbj := 'team'; trm := NEW.team;
	IF trm IS NOT NULL THEN 
		EXECUTE format('INSERT INTO %s_terms (data_table,subject,term,status) SELECT %L, %L, trim(unnest(regexp_split_to_array(%L,E'',\s*''))) as o, ''undefined'' ON CONFLICT DO NOTHING;', project, tbl, sbj, trm);
	END IF;

	RETURN NEW;
 END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;