CREATE OR REPLACE FUNCTION "public"."pisces_survey_sites_bi_fun" () RETURNS trigger AS 
$$
DECLARE
 tbl varchar;
 geom geometry;
 centr geometry;
 b RECORD;
BEGIN
    tbl := TG_TABLE_NAME;
    IF tg_op = 'INSERT' THEN
        geom := new."obm_geometry";
		centr := ST_Centroid(geom);
        
        SELECT bazin, code INTO b FROM shared.bazine_hidrografice_mari g WHERE ST_Intersects(centr, g.geometry);

        new.basin = b.bazin;
        new.basin_code = b.code;
        new.survey_station_code = b.code || LPAD((new.obm_id)::varchar, 6, '0');
        return new;
    END IF;
END; 
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
