CREATE OR REPLACE FUNCTION "public"."pisces_view_insert_row" () RETURNS trigger AS 
$$
DECLARE
 tbl varchar;
 site RECORD;
 eid integer;
BEGIN
  tbl := TG_TABLE_NAME;
  -- felmérés adat
  IF new.location_start = 'str_occasional_observation' THEN
    SELECT * FROM pisces_survey_sites INTO site WHERE location_start = new.location_start;
    IF site.obm_id IS NULL THEN
      RAISE EXCEPTION '% survey site does not exist!', new.location_start
        USING HINT = 'Please check the spelling or create it first!';
      RETURN NULL;
    END IF;
    INSERT INTO pisces_survey_events (obm_uploading_id, site_id, team, obm_access, date, method) VALUES (new.obm_uploading_id, site.obm_id, new.team, new.access, new.date, new.method) RETURNING obm_id INTO eid;

    INSERT INTO pisces_survey_records (obm_uploading_id, event_id, obm_geometry, species, total_no, juvenile, adult, obm_files_id, comments, obm_access, exact_time)
    VALUES (new.obm_uploading_id, eid, new.obm_geometry, new.species, new.total_no, new.juvenile, new.adult, new.obm_files_id, new.observation, new.access, new.exact_time);

    return new;
  
  -- lista
  ELSIF new.location_start = 'str_occasional_observation_list' AND new.obm_observation_list_id IS NOT NULL THEN
    SELECT * FROM pisces_survey_sites INTO site WHERE location_start = new.location_start;
    IF site.obm_id IS NULL THEN
      RAISE EXCEPTION '% survey site does not exist!', new.location_start
        USING HINT = 'Please check the spelling or create it first!';
      RETURN NULL;
    END IF;

    SELECT obm_id FROM pisces_survey_events INTO eid WHERE site_id = site.obm_id AND obm_observation_list_id = new.obm_observation_list_id;
    IF eid IS NULL THEN
      INSERT INTO pisces_survey_events (site_id, team, obm_access, date, method, obm_observation_list_id) VALUES (site.obm_id, new.team, new.access, new.date, new.method, new.obm_observation_list_id) ON CONFLICT (obm_observation_list_id) DO NOTHING RETURNING obm_id INTO eid;
    END IF;

    INSERT INTO pisces_survey_records (obm_uploading_id, event_id, obm_geometry, species, total_no, juvenile, adult, obm_files_id, comments, obm_access, exact_time, obm_observation_list_id)
      VALUES (new.obm_uploading_id, eid, new.obm_geometry, new.species, new.total_no, new.juvenile, new.adult, new.obm_files_id, new.observation, new.access, new.exact_time, new.obm_observation_list_id);
  END IF;
  RETURN NULL;

END; 
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;

-- INSERT INTO pisces (location_start, species) VALUES ('proba2', 'teszzt');
--CREATE TRIGGER pisces_ins INSTEAD OF INSERT ON pisces FOR EACH ROW EXECUTE FUNCTION pisces_view_insert_row();
