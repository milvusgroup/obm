CREATE RULE pisces_del AS
    ON DELETE TO public.pisces DO INSTEAD  DELETE FROM pisces_survey_records
  WHERE (pisces_survey_records.obm_id = old.obm_id);
