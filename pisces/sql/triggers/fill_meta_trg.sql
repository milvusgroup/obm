CREATE TRIGGER "pisces_search_fill_meta" BEFORE INSERT OR UPDATE ON "pisces_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "pisces_taxon_fill_meta" BEFORE INSERT OR UPDATE ON "pisces_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
