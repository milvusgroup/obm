CREATE TRIGGER "pisces_refresh_taxon_materialized_views_tr" AFTER INSERT OR UPDATE OR DELETE ON "pisces_taxon" FOR EACH STATEMENT EXECUTE PROCEDURE pisces_refresh_taxon_materialized_views();
