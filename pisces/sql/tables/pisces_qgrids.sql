DROP TABLE IF EXISTS "pisces_survey_sites_qgrids";
CREATE TABLE "public"."pisces_survey_sites_qgrids" (
    "row_id" integer NOT NULL,
    "original" geometry,
    "centroid" geometry,
    "etrs_name" character varying,
    "etrs_geom" geometry,
    "data_table" character varying DEFAULT 'pisces_survey_sites',
    "sci" character varying,
    "ccm2" character varying(64),
    "country" character varying(64),
    "judet" character varying,
    "comuna" character varying,
    "altitude" numeric,
    "corine_code" character varying(3),
    CONSTRAINT "pisces_survey_sites_qgrids_pkey" PRIMARY KEY ("row_id")
) WITH (oids = false);

GRANT ALL ON pisces_survey_sites_qgrids TO pisces_admin;

ALTER TABLE "pisces_survey_sites_qgrids"
ADD FOREIGN KEY ("row_id") REFERENCES "pisces_survey_sites" ("obm_id") ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO pisces_survey_sites_qgrids (row_id, original, centroid)
SELECT obm_id, obm_geometry, st_centroid(obm_geometry)
FROM pisces_survey_sites;
-- 2024-05-01 10:48:21.784772+02

