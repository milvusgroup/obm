-- Adminer 4.8.1 PostgreSQL 11.10 (Debian 11.10-1.pgdg90+1) dump

DROP TABLE IF EXISTS "pisces";
DROP SEQUENCE IF EXISTS pisces_obm_id_seq;
CREATE SEQUENCE pisces_obm_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "pisces"."pisces" (
    "obm_id" integer DEFAULT nextval('pisces_obm_id_seq') NOT NULL,
    "obm_geometry" public.geometry,
    "obm_datum" timestamptz,
    "obm_uploading_id" integer,
    "obm_validation" numeric,
    "obm_comments" text[],
    "obm_modifier_id" integer,
    "obm_files_id" character varying(128),
    "location_start" character varying(255),
    "location_end" character varying(255),
    "basin" character varying(255),
    "river" character varying(255),
    "date" date,
    "time_start" numeric,
    "time_end" numeric,
    "team" character varying(255),
    "site_length" double precision,
    "site_width" double precision,
    "area_surface" integer,
    "depth_mean" integer,
    "depth_max" integer,
    "covering" integer,
    "shading" integer,
    "meander" integer,
    "electrofisher_type" integer,
    "type_of_electrofishing" character varying(128),
    "water_level" numeric,
    "land_use_right" numeric,
    "rb_fine_sediment" integer,
    "rb_sand" integer,
    "rb_gravel" integer,
    "rb_stone" integer,
    "rb_large_stone" integer,
    "rb_concrete" integer,
    "rb_clay" integer,
    "lu_right_bank" character varying(255),
    "lu_left_bank" character varying(255),
    "lbv_grasses" integer,
    "lbv_wood" integer,
    "lbv_stone" integer,
    "lbv_concrete" integer,
    "rbv_grasses" integer,
    "rbv_wood" integer,
    "rbv_stone" integer,
    "rbv_concrete" integer,
    "ws_rapid" integer,
    "ws_medium" integer,
    "ws_slow" integer,
    "ws_stagnant" integer,
    "transparency" integer,
    "naturalness" integer,
    "date_of_modification" character varying(128),
    "ph" double precision,
    "conductivity" integer,
    "tds" integer,
    "disolved_oxygen_proc" integer,
    "disolved_oxygen_mgl" double precision,
    "temperature" double precision,
    "species" character varying(128),
    "total_no" integer,
    "juvenile" integer,
    "adult" integer,
    "natura2000" boolean,
    "pressures" character varying(255),
    "threats" character varying(255),
    "photo_number" character varying(32),
    "repertofon" character varying(32),
    "observation" text,
    "access" smallint,
    "site_width_max" double precision,
    "end_geometry" public.geometry,
    "lbv_bushes" integer,
    "rbv_bushes" integer,
    "info_source" character varying(255),
    "other_species" character varying,
    "altitude" numeric,
    "water_type" character varying(16),
    "survey_station_code" character varying,
    "program" text,
    "species_hu" character varying(128),
    "species_ro" character varying(128),
    "species_en" character varying(128),
    "species_sci" character varying(128),
    "obm_observation_list_id" character varying(64),
    "exact_time" time without time zone,
    "survey_station_code_deprecated" character varying,
    "method" text,
    "site_id" integer,
    "event_id" integer,
    CONSTRAINT "pisces_pkey" PRIMARY KEY ("obm_id"),
    CONSTRAINT "enforce_dims_end_geometry" CHECK (public.st_ndims(end_geometry) = 2),
    CONSTRAINT "enforce_dims_obm_geometry" CHECK (public.st_ndims(obm_geometry) = 2),
    CONSTRAINT "enforce_geotype_end_geometry" CHECK ((public.geometrytype(end_geometry) = 'POINT'::text) OR (end_geometry IS NULL)),
    CONSTRAINT "enforce_geotype_obm_geometry" CHECK ((public.geometrytype(obm_geometry) = 'POINT'::text) OR (public.geometrytype(obm_geometry) = 'LINESTRING'::text) OR (public.geometrytype(obm_geometry) = 'POLYGON'::text) OR (obm_geometry IS NULL)),
    CONSTRAINT "enforce_srid_end_geometry" CHECK (public.st_srid(end_geometry) = 4326),
    CONSTRAINT "enforce_srid_obm_geometry" CHECK (public.st_srid(obm_geometry) = 4326)
) WITH (oids = false);

COMMENT ON COLUMN "pisces"."pisces"."survey_station_code_deprecated" IS 'lásd: https://redmine.milvus.ro/issues/1920';


DELIMITER ;;

CREATE TRIGGER "file_connection" AFTER INSERT ON "pisces"."pisces" FOR EACH ROW EXECUTE PROCEDURE public.file_connect_status_update();;

CREATE TRIGGER "pisces_add_term_tr" AFTER INSERT OR UPDATE ON "pisces"."pisces" FOR EACH ROW EXECUTE PROCEDURE system.pisces_add_term();;

CREATE TRIGGER "pisces_linnaeus_sync_rows" AFTER DELETE OR INSERT OR UPDATE ON "pisces"."pisces" FOR EACH ROW EXECUTE PROCEDURE system.pisces_l_sync_row();;

CREATE TRIGGER "pisces_qgrid_trigger" AFTER DELETE OR INSERT OR UPDATE ON "pisces"."pisces" FOR EACH ROW EXECUTE PROCEDURE public.pisces_update_qgrids();;

CREATE TRIGGER "rules_pisces" BEFORE DELETE OR INSERT OR UPDATE ON "pisces"."pisces" FOR EACH ROW EXECUTE PROCEDURE public.pisces_rules_f();;

CREATE TRIGGER "taxon_update_pisces" BEFORE INSERT ON "pisces"."pisces" FOR EACH ROW EXECUTE PROCEDURE public.update_pisces_taxonlist();;

CREATE TRIGGER "history_update" AFTER DELETE OR UPDATE ON "pisces"."pisces" FOR EACH ROW EXECUTE PROCEDURE public.history_pisces();;

DELIMITER ;

ALTER TABLE ONLY "pisces"."pisces" ADD CONSTRAINT "pisces_site_id_fkey" FOREIGN KEY (site_id) REFERENCES public.pisces_survey_sites(obm_id) ON UPDATE RESTRICT ON DELETE RESTRICT NOT DEFERRABLE;

-- 2024-05-01 08:01:44.63527+02
