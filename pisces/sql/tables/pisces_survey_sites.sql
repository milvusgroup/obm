CREATE SEQUENCE pisces_survey_sites_obm_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."pisces_survey_sites" (
    "obm_id" integer DEFAULT nextval('pisces_survey_sites_obm_id_seq') NOT NULL,
    "obm_geometry" geometry,
    "obm_uploading_id" integer,
    "obm_validation" numeric,
    "obm_comments" text[],
    "obm_modifier_id" integer,
    "obm_files_id" character varying(32),
    "obm_access" integer,
    "survey_station_code" character varying,
    "location_start" character varying,
    "location_end" character varying,
    "end_geometry" geometry,
    "water_type" character varying,
    "river" character varying,
    "survey_station_code_deprecated" character varying,
    "comments" character varying,
    "basin" character varying,
    "basin_code" character(2),
    "basin_minor" character varying,
    CONSTRAINT "pisces_survey_sites_pkey" PRIMARY KEY ("obm_id"),
    CONSTRAINT "pisces_survey_sites_survey_station_code" UNIQUE ("survey_station_code"),
    CONSTRAINT "enforce_dims_obm_geometry" CHECK (public.st_ndims(obm_geometry) = 2),
    CONSTRAINT "enforce_srid_obm_geometry" CHECK (public.st_srid(obm_geometry) = 4326)
) WITH (oids = false);
