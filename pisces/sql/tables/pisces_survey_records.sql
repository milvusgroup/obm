ALTER TABLE pisces_survey_records ADD COLUMN "obm_access" integer;
ALTER TABLE pisces_survey_records ADD COLUMN "event_id" integer;
ALTER TABLE pisces_survey_records ADD COLUMN "species" varchar;
ALTER TABLE pisces_survey_records ADD COLUMN "total_no" integer;
ALTER TABLE pisces_survey_records ADD COLUMN "juvenile" integer;
ALTER TABLE pisces_survey_records ADD COLUMN "adult" integer;
ALTER TABLE pisces_survey_records ADD COLUMN "photo_number" varchar;
ALTER TABLE pisces_survey_records ADD COLUMN "comments" varchar;
ALTER TABLE pisces_survey_records ADD COLUMN "info_source" varchar;
ALTER TABLE pisces_survey_records ADD COLUMN "obm_observation_list_id" varchar;
ALTER TABLE pisces_survey_records ADD COLUMN "exact_time" time without time zone;

ALTER TABLE pisces_survey_records ADD COLUMN basin varchar;
ALTER TABLE pisces_survey_records ADD COLUMN basin_minor varchar;
ALTER TABLE pisces_survey_records ADD COLUMN location_start varchar;