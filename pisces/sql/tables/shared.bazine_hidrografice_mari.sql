DROP TABLE shared.bazine_hidrografice_mari;
CREATE TABLE shared.bazine_hidrografice_mari as SELECT bazin::varchar, st_transform(st_union(geometry), 4326) as geometry from shared.bazine_hidrografice group by bazin;
ALTER TABLE shared.bazine_hidrografice_mari ALTER COLUMN bazin TYPE varchar;
ALTER TABLE shared.bazine_hidrografice_mari ADD COLUMN code character(2) NULL;

UPDATE shared.bazine_hidrografice_mari SET bazin = 'Tisa' WHERE bazin = 'I';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Someş' WHERE bazin = 'II';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Crişul Alb' WHERE bazin = 'III';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Mureş' WHERE bazin = 'IV';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Bega' WHERE bazin = 'V';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Nera' WHERE bazin = 'VI';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Jiu' WHERE bazin = 'VII';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Olt' WHERE bazin = 'VIII';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Vedea' WHERE bazin = 'IX';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Argeş' WHERE bazin = 'X';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Ialomiţa' WHERE bazin = 'XI';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Siret' WHERE bazin = 'XII';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Prut' WHERE bazin = 'XIII';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Dunărea' WHERE bazin = 'XIV';
UPDATE shared.bazine_hidrografice_mari SET bazin = 'Litoral' WHERE bazin = 'XV';

UPDATE shared.bazine_hidrografice_mari SET code = SUBSTRING(bazin, 1, 2);

GRANT SELECT ON shared.bazine_hidrografice_mari to pisces_admin;