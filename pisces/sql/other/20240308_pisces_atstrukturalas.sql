CREATE TABLE tmp.pisces_zookeys_adatsor (
    site_id varchar,
    river varchar,
    date date,
    species varchar,
    altitude numeric,
    site_length integer,
    area integer,
    x numeric,
    y numeric,
    total integer,
    adult integer,
    juvenile integer
);

WITH counts as (
    SELECT distinct zk.site_id, zk.species, sum(zk.total) as zkc, sum(p.total_no) as pc 
    FROM 
        pisces p 
    INNER JOIN tmp.pisces_zookeys_adatsor zk ON survey_station_code_deprecated = site_id and zk.species = p.species_sci
    group BY site_id, zk.species
)
SELECT * FROM counts WHErE zkc != pc;


-- azok a pontok, ahol a zookeys - db pont távolság nagyobb mint 10 méter
\copy (

select site_id, round from (
SELECT distinct zk.site_id, round(st_distance(st_transform(obm_geometry, 3844), st_transform(st_geomfromtext('POINT(' || zk.x::varchar || ' ' || zk.y::varchar || ')', 4326), 3844))::numeric, 2)
FROM 
    pisces p
INNER JOIN tmp.pisces_zookeys_adatsor zk ON survey_station_code_deprecated = site_id) foo 
WHERE round > 10;



) to /tmp/zookey_station_code_gt10m.csv csv header;

-- 0. pisces backup
-- pg_dump -h app -U gisadmin -p 23456 -t pisces -f pisces_backup_20240321.sql gisdata
-- Backup fájl a Nextcloudon az Adatbázisos/OpenFishMaps/location_start_atnevezes mappában

-- 1. location_start elirasok javitasa
\copy (
    select distinct 
        location_start, 
        bazin,
        st_astext(obm_geometry) as geom
    from pisces p
    LEFT JOIN pisces_qgrids qg on qg.data_table = 'pisces' AND row_id = obm_id
    where location_start is not null order by bazin, location_start
) to /tmp/location_start_geometry.csv csv header;

-- Tilla javtításai alapján:
-- 1.a törlések
BEGIN;
    DELETE FROM pisces WHERE location_start = 'BU3AMCAT' AND st_astext(obm_geometry) = 'POINT(24.6789851096456 45.5194116459646)';
    DELETE FROM pisces WHERE location_start = 'Capra M 3 T' AND st_astext(obm_geometry) = 'POINT(24.5832693837839 45.4906717886977)';
    DELETE FROM pisces WHERE location_start = 'Capra M 3 T' AND st_astext(obm_geometry) = 'POINT(24.5834348782114 45.4899395921507)';
    DELETE FROM pisces WHERE location_start = 'NK 1 T' AND st_astext(obm_geometry) = 'POINT(25.393892 46.452312)';
    DELETE FROM pisces WHERE location_start = 'Dejani02 T' AND st_astext(obm_geometry) = 'POINT(25.0533000379801 45.6575580406934)';
    DELETE FROM pisces WHERE location_start = 'Dejani05 T' AND st_astext(obm_geometry) = 'POINT(25.0619390141219 45.6361090019345)';
    DELETE FROM pisces WHERE location_start = 'KM2' AND st_astext(obm_geometry) = 'POINT(25.640016 46.472235)';
    DELETE FROM pisces WHERE location_start = 'KM3' AND st_astext(obm_geometry) = 'POINT(25.659527 46.481073)';
    DELETE FROM pisces WHERE location_start = 'Lupsa01 T' AND st_astext(obm_geometry) = 'POINT(25.0559160299599 45.6707939878106)';
    DELETE FROM pisces WHERE location_start = 'UCAMCET' AND st_astext(obm_geometry) = 'POINT(24.7077299871214 45.68201926646)';
    DELETE FROM pisces WHERE location_start = 'VIAMCAT' AND st_astext(obm_geometry) = 'POINT(24.7651591802346 45.6591055719617)';
    DELETE FROM pisces WHERE location_start = 'Baska 1 T' AND st_astext(obm_geometry) = 'POINT(26.222066 46.3004)';
    DELETE FROM pisces WHERE location_start = 'Baska 1 T' AND st_astext(obm_geometry) = 'POINT(26.223679 46.310166)';
    DELETE FROM pisces WHERE location_start = 'Uz 4 T' AND st_astext(obm_geometry) = 'POINT(26.218507 46.314914)';
    DELETE FROM pisces WHERE location_start = 'Uz 6 T' AND st_astext(obm_geometry) = 'POINT(26.233122 46.330231)';
    DELETE FROM pisces WHERE location_start = 'Uz 9 T' AND st_astext(obm_geometry) = 'POINT(26.263478 46.342395)';
COMMIT;
-- 1.b átnevezések
BEGIN;
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'ARGES2T' AND st_astext(obm_geometry) = 'POINT(24.6029635622937 45.462774457951)';
    UPDATE pisces SET location_start = 'CA4AMCAT' WHERE location_start = 'ARGES5T' AND st_astext(obm_geometry) = 'POINT(24.5907935957522 45.5354249292056)';
    UPDATE pisces SET location_start = 'BUDA5T' WHERE location_start = 'BU1AMCAT' AND st_astext(obm_geometry) = 'POINT(24.7046899800892 45.5774130618421)';
    UPDATE pisces SET location_start = 'BUDA4T' WHERE location_start = 'BU2AVCAT' AND st_astext(obm_geometry) = 'POINT(24.6805197048453 45.5503570448969)';
    UPDATE pisces SET location_start = 'BUDA3T' WHERE location_start = 'BU3AVCAT' AND st_astext(obm_geometry) = 'POINT(24.6789368617987 45.5148661958413)';
    UPDATE pisces SET location_start = 'BUDA3T' WHERE location_start = 'BU3AVCAT' AND st_astext(obm_geometry) = 'POINT(24.6791741322205 45.5146759349283)';
    UPDATE pisces SET location_start = 'BUDA2T' WHERE location_start = 'BU4AMCAT' AND st_astext(obm_geometry) = 'POINT(24.6474960015491 45.4965932287263)';
    UPDATE pisces SET location_start = 'BUDA2T' WHERE location_start = 'BU4AMCAT' AND st_astext(obm_geometry) = 'POINT(24.6479821364362 45.4970787154574)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'CA6AMCAT' AND st_astext(obm_geometry) = 'POINT(24.5831006810922 45.4907599025597)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'CA6AMCAT' AND st_astext(obm_geometry) = 'POINT(24.5833819449357 45.4912506562215)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'CA6AVAMCET' AND st_astext(obm_geometry) = 'POINT(24.5973821215313 45.477517165862)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'CA6AVAMCET' AND st_astext(obm_geometry) = 'POINT(24.599041806738 45.4763588356413)';
    UPDATE pisces SET location_start = 'CAPRAM3T' WHERE location_start = 'CA6AVCAT' AND st_astext(obm_geometry) = 'POINT(24.5839616584498 45.487343513991)';
    UPDATE pisces SET location_start = 'CAPRAM3T' WHERE location_start = 'CA6AVCAT' AND st_astext(obm_geometry) = 'POINT(24.5846768222623 45.4863134576815)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'CA7AVCET' AND st_astext(obm_geometry) = 'POINT(24.6030189489046 45.4625716760354)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'CA7AVCET' AND st_astext(obm_geometry) = 'POINT(24.6032426359392 45.4624275957608)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'Capra M 1 T' AND st_astext(obm_geometry) = 'POINT(24.6029162721654 45.4627793667696)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'Capra M 1 T' AND st_astext(obm_geometry) = 'POINT(24.6030177447683 45.4625831902522)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'Capra M 1 T' AND st_astext(obm_geometry) = 'POINT(24.603022536287 45.4626020987515)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'Capra M 1 T' AND st_astext(obm_geometry) = 'POINT(24.6030398195434 45.4625971010199)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'Capra M 1 T' AND st_astext(obm_geometry) = 'POINT(24.603048367463 45.4626361826112)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'Capra M 1 T' AND st_astext(obm_geometry) = 'POINT(24.603057963371 45.4626278303753)';
    UPDATE pisces SET location_start = 'CAPRAM1T' WHERE location_start = 'Capra M 1 T' AND st_astext(obm_geometry) = 'POINT(24.6030921510099 45.4626064579394)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'Capra M 2 T' AND st_astext(obm_geometry) = 'POINT(24.5943408237943 45.4791833595945)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'Capra M 2 T' AND st_astext(obm_geometry) = 'POINT(24.5946992743797 45.4783515978712)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'Capra M 2 T' AND st_astext(obm_geometry) = 'POINT(24.5966909372683 45.4774394005836)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'Capra M 2 T' AND st_astext(obm_geometry) = 'POINT(24.5974123792682 45.4774660778783)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'Capra M 2 T' AND st_astext(obm_geometry) = 'POINT(24.6003591601357 45.4744688986626)';
    UPDATE pisces SET location_start = 'CAPRAM2T' WHERE location_start = 'Capra M 2 T' AND st_astext(obm_geometry) = 'POINT(24.6010803078186 45.472245907199)';
    UPDATE pisces SET location_start = 'CAPRAM3T' WHERE location_start = 'Capra M 3 T' AND st_astext(obm_geometry) = 'POINT(24.5843408795237 45.4861910547157)';
    UPDATE pisces SET location_start = 'CAPRAM3T' WHERE location_start = 'Capra M 3 T' AND st_astext(obm_geometry) = 'POINT(24.5845000702134 45.4859019200359)';
    UPDATE pisces SET location_start = 'CAPRAM3T' WHERE location_start = 'Capra M 3 T' AND st_astext(obm_geometry) = 'POINT(24.5846748456377 45.4858078328901)';
    UPDATE pisces SET location_start = 'CAPRAM3T' WHERE location_start = 'Capra M 3 UJ T' AND st_astext(obm_geometry) = 'POINT(24.5845377759989 45.486003978758)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'Capra M 4 T' AND st_astext(obm_geometry) = 'POINT(24.5831299613348 45.4908646509685)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'Capra M 4 T' AND st_astext(obm_geometry) = 'POINT(24.5831727083915 45.4909911425822)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'Capra M 4 T' AND st_astext(obm_geometry) = 'POINT(24.5832296383989 45.4910106853059)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'Capra M 4 T' AND st_astext(obm_geometry) = 'POINT(24.5832645570304 45.4911218348734)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'Capra M 4 T' AND st_astext(obm_geometry) = 'POINT(24.5843008394625 45.4941686760172)';
    UPDATE pisces SET location_start = 'CAPRAM4T' WHERE location_start = 'Capra M 4 UJ T' AND st_astext(obm_geometry) = 'POINT(24.5833254175248 45.4912313939022)';
    UPDATE pisces SET location_start = 'OTIC2T' WHERE location_start = 'OTAMCAT' AND st_astext(obm_geometry) = 'POINT(24.6521951304824 45.4637544104736)';
    UPDATE pisces SET location_start = 'OTIC2T' WHERE location_start = 'OTAMCAT' AND st_astext(obm_geometry) = 'POINT(24.6523858564974 45.4639933380329)';
    UPDATE pisces SET location_start = 'GORGENY2' WHERE location_start = 'Görgény2' AND st_astext(obm_geometry) = 'POINT(25.1989000088051 46.767560001548)';
    UPDATE pisces SET location_start = 'GORGENY3' WHERE location_start = 'Görgény3' AND st_astext(obm_geometry) = 'POINT(25.2136700087993 46.7695100015458)';
    UPDATE pisces SET location_start = 'NK5T_IVO' WHERE location_start = 'Ivo' AND st_astext(obm_geometry) = 'POINT(25.3868723229209 46.4456695059877)';
    UPDATE pisces SET location_start = 'NK5T_IVO' WHERE location_start = 'Ivo 5 T' AND st_astext(obm_geometry) = 'POINT(25.3872531966007 46.4456787467596)';
    UPDATE pisces SET location_start = 'NK1T_GAT_ALATT' WHERE location_start = 'Nagy-Kukullo gat alatt' AND st_astext(obm_geometry) = 'POINT(25.4001450220796 46.4579703601676)';
    UPDATE pisces SET location_start = 'NK2T_GAT_ALATT' WHERE location_start = 'Nagy-Kukullo gat alatt a masodik' AND st_astext(obm_geometry) = 'POINT(25.3936661462038 46.4523113698561)';
    UPDATE pisces SET location_start = 'NK4T_GAT_ALATT' WHERE location_start = 'Nagy-Kukullo Ivo beomlesenel' AND st_astext(obm_geometry) = 'POINT(25.3864324406419 46.4459689062639)';
    UPDATE pisces SET location_start = 'NK3T_GAT_ALATT' WHERE location_start = 'Nagy-Kukullo kozepso' AND st_astext(obm_geometry) = 'POINT(25.3902369419784 46.449625337976)';
    UPDATE pisces SET location_start = 'NK1T_UDVH_ALATT' WHERE location_start = 'NK1T' AND st_astext(obm_geometry) = 'POINT(25.2783910185099 46.2811120413244)';
    UPDATE pisces SET location_start = 'NK1T_UDVH_ALATT' WHERE location_start = 'NK1T' AND st_astext(obm_geometry) = 'POINT(25.2785535453115 46.2810553912244)';
    UPDATE pisces SET location_start = 'NK1T_GAT_ALATT' WHERE location_start = 'NK 1 T' AND st_astext(obm_geometry) = 'POINT(25.4000473335991 46.4580230206617)';
    UPDATE pisces SET location_start = 'NK1T_GAT_ALATT' WHERE location_start = 'NK 1 T' AND st_astext(obm_geometry) = 'POINT(25.400092 46.458027)';
    UPDATE pisces SET location_start = 'NK2SZENNYVT_UDVH_ALATT' WHERE location_start = 'NK2T' AND st_astext(obm_geometry) = 'POINT(25.2799669839442 46.2777249980718)';
    UPDATE pisces SET location_start = 'NK2T_GAT_ALATT' WHERE location_start = 'NK 2 T' AND st_astext(obm_geometry) = 'POINT(25.393892 46.452312)';
    UPDATE pisces SET location_start = 'NK2T_GAT_ALATT' WHERE location_start = 'NK 2 T' AND st_astext(obm_geometry) = 'POINT(25.3943878725792 46.452346480505)';
    UPDATE pisces SET location_start = 'NK3T_UDVH_ALATT' WHERE location_start = 'NK3T' AND st_astext(obm_geometry) = 'POINT(25.26826 46.264278)';
    UPDATE pisces SET location_start = 'NK3T_GAT_ALATT' WHERE location_start = 'NK 3 T' AND st_astext(obm_geometry) = 'POINT(25.3900856093198 46.4494562438158)';
    UPDATE pisces SET location_start = 'NK3T_GAT_ALATT' WHERE location_start = 'NK 3 T' AND st_astext(obm_geometry) = 'POINT(25.390236 46.449573)';
    UPDATE pisces SET location_start = 'NK4HT_UDVH_ALATT' WHERE location_start = 'NK4HT' AND st_astext(obm_geometry) = 'POINT(25.2481210324913 46.2619070056826)';
    UPDATE pisces SET location_start = 'NK4T_UDVH_ALATT' WHERE location_start = 'NK4T' AND st_astext(obm_geometry) = 'POINT(25.258448 46.261543)';
    UPDATE pisces SET location_start = 'NK4T_GAT_ALATT' WHERE location_start = 'NK 4 T' AND st_astext(obm_geometry) = 'POINT(25.3861427620684 46.4457822433203)';
    UPDATE pisces SET location_start = 'NK4T_GAT_ALATT' WHERE location_start = 'NK 4 T' AND st_astext(obm_geometry) = 'POINT(25.38623 46.445739)';
    UPDATE pisces SET location_start = 'NK4T_GAT_ALATT' WHERE location_start = 'NK 4 T' AND st_astext(obm_geometry) = 'POINT(25.386283 46.445649)';
    UPDATE pisces SET location_start = 'NK5T_UDVH_ALATT' WHERE location_start = 'Nk5T' AND st_astext(obm_geometry) = 'POINT(25.231416 46.259764)';
    UPDATE pisces SET location_start = 'NK5T_IVO' WHERE location_start = 'NK 5 T' AND st_astext(obm_geometry) = 'POINT(25.386283 46.445649)';
    UPDATE pisces SET location_start = 'CUCIULATA2T' WHERE location_start = 'CCAMCAT' AND st_astext(obm_geometry) = 'POINT(25.0373681075265 45.650572178438)';
    UPDATE pisces SET location_start = 'CUCIULATA2T' WHERE location_start = 'CCAMCAT' AND st_astext(obm_geometry) = 'POINT(25.0376850128694 45.6506049620612)';
    UPDATE pisces SET location_start = 'CUCIULATA1T' WHERE location_start = 'CCAVCAT' AND st_astext(obm_geometry) = 'POINT(25.0451283180364 45.6598306962854)';
    UPDATE pisces SET location_start = 'CUCIULATA1T' WHERE location_start = 'CCAVCAT' AND st_astext(obm_geometry) = 'POINT(25.0451388898043 45.6598563167865)';
    UPDATE pisces SET location_start = 'CUCIULATA1T' WHERE location_start = 'Cuciulata1 T' AND st_astext(obm_geometry) = 'POINT(25.0454369746149 45.6597950030118)';
    UPDATE pisces SET location_start = 'CUCIULATA2T' WHERE location_start = 'Cuciulata2 T' AND st_astext(obm_geometry) = 'POINT(25.0375419761986 45.6504210177809)';
    UPDATE pisces SET location_start = 'DEJANIFELSOT' WHERE location_start = 'DEAMCAT' AND st_astext(obm_geometry) = 'POINT(24.9398250767993 45.6262925045011)';
    UPDATE pisces SET location_start = 'DEJANIFELSOT' WHERE location_start = 'DEAMCAT' AND st_astext(obm_geometry) = 'POINT(24.9398928503167 45.6262305380934)';
    UPDATE pisces SET location_start = 'DEJANI02T' WHERE location_start = 'DEAMCET' AND st_astext(obm_geometry) = 'POINT(24.9241853450347 45.6683110894271)';
    UPDATE pisces SET location_start = 'DEJANI02T' WHERE location_start = 'DEAMCET' AND st_astext(obm_geometry) = 'POINT(24.9246757961107 45.667961105368)';
    UPDATE pisces SET location_start = 'DEJANI01T' WHERE location_start = 'DEAVCET' AND st_astext(obm_geometry) = 'POINT(24.9207772486371 45.6743373619165)';
    UPDATE pisces SET location_start = 'DEJANI01T' WHERE location_start = 'DEAVCET' AND st_astext(obm_geometry) = 'POINT(24.9208497084798 45.6743102808688)';
    UPDATE pisces SET location_start = 'DEJANI01T' WHERE location_start = 'Dejani 01T' AND st_astext(obm_geometry) = 'POINT(24.920864040032 45.6744049955159)';
    UPDATE pisces SET location_start = 'DEJANI01T' WHERE location_start = 'Dejani 01T' AND st_astext(obm_geometry) = 'POINT(24.92088 45.674409)';
    UPDATE pisces SET location_start = 'DEJANI01T' WHERE location_start = 'Dejani01 T' AND st_astext(obm_geometry) = 'POINT(24.920864040032 45.6744049955159)';
    UPDATE pisces SET location_start = 'DEJANI02T' WHERE location_start = 'Dejani02 T' AND st_astext(obm_geometry) = 'POINT(24.9246860202402 45.6679769977927)';
    UPDATE pisces SET location_start = 'DEJANI03T' WHERE location_start = 'Dejani03 T' AND st_astext(obm_geometry) = 'POINT(24.929131 45.650581)';
    UPDATE pisces SET location_start = 'DEJANI03T' WHERE location_start = 'Dejani03 T' AND st_astext(obm_geometry) = 'POINT(24.9291341314325 45.6509765809544)';
    UPDATE pisces SET location_start = 'DEJANI04T' WHERE location_start = 'Dejani04 T' AND st_astext(obm_geometry) = 'POINT(24.935475 45.645438)';
    UPDATE pisces SET location_start = 'DEJANI04T' WHERE location_start = 'Dejani04 T' AND st_astext(obm_geometry) = 'POINT(24.9357743690574 45.645462838249)';
    UPDATE pisces SET location_start = 'DEJANIFELSOT' WHERE location_start = 'Dejani06 T' AND st_astext(obm_geometry) = 'POINT(24.9410009756684 45.6252520065755)';
    UPDATE pisces SET location_start = 'DEJANI1TSCI' WHERE location_start = 'DEJANI1T' AND st_astext(obm_geometry) = 'POINT(24.9257054076151 45.6885380774758)';
    UPDATE pisces SET location_start = 'SEBES09T' WHERE location_start = 'GROAVCAT' AND st_astext(obm_geometry) = 'POINT(25.0268309684984 45.6136399019981)';
    UPDATE pisces SET location_start = 'SEBES09T' WHERE location_start = 'GROAVCAT' AND st_astext(obm_geometry) = 'POINT(25.0269070238758 45.6140465316597)';
    UPDATE pisces SET location_start = 'NM1' WHERE location_start = 'NM1 2021' AND st_astext(obm_geometry) = 'POINT(25.7274672702128 46.5004489708113)';
    UPDATE pisces SET location_start = 'NM1' WHERE location_start = 'NM1 2022' AND st_astext(obm_geometry) = 'POINT(25.7272980698765 46.5006212468809)';
    UPDATE pisces SET location_start = 'SE1AMCAT' WHERE location_start = 'SE1AMCAT' AND st_astext(obm_geometry) = 'POINT(25.0080667725407 45.6125548124599)';
    UPDATE pisces SET location_start = 'SE1AVCAT' WHERE location_start = 'SE1AVCAT' AND st_astext(obm_geometry) = 'POINT(25.0137420117644 45.6138717090557)';
    UPDATE pisces SET location_start = 'SEBES08T' WHERE location_start = 'SE2AMCAT' AND st_astext(obm_geometry) = 'POINT(25.0363979812696 45.6174217516676)';
    UPDATE pisces SET location_start = 'SEBES08T' WHERE location_start = 'SE2AMCAT' AND st_astext(obm_geometry) = 'POINT(25.0367113773991 45.6173300334742)';
    UPDATE pisces SET location_start = 'SEBES02T' WHERE location_start = 'SE3AMCET' AND st_astext(obm_geometry) = 'POINT(25.0532781093295 45.6576182295172)';
    UPDATE pisces SET location_start = 'SEBES02T' WHERE location_start = 'SE3AMCET' AND st_astext(obm_geometry) = 'POINT(25.0534089980767 45.6576419454496)';
    UPDATE pisces SET location_start = 'SEBES01T' WHERE location_start = 'SE3AVCET' AND st_astext(obm_geometry) = 'POINT(25.0561961036523 45.6709150414648)';
    UPDATE pisces SET location_start = 'SEBES01T' WHERE location_start = 'Sebes01 T' AND st_astext(obm_geometry) = 'POINT(25.0559160299599 45.6707939878106)';
    UPDATE pisces SET location_start = 'SEBES02T' WHERE location_start = 'Sebes 02T' AND st_astext(obm_geometry) = 'POINT(25.053422 45.657489)';
    UPDATE pisces SET location_start = 'SEBES02T' WHERE location_start = 'Sebes02 T' AND st_astext(obm_geometry) = 'POINT(25.0533000379801 45.6575580406934)';
    UPDATE pisces SET location_start = 'SE1AVCAT' WHERE location_start = 'Sebes03 T' AND st_astext(obm_geometry) = 'POINT(25.0105225459202 45.6138080010108)';
    UPDATE pisces SET location_start = 'SEBES05T' WHERE location_start = 'Sebes05 T' AND st_astext(obm_geometry) = 'POINT(25.0619390141219 45.6361090019345)';
    UPDATE pisces SET location_start = 'SEBES08T' WHERE location_start = 'Sebes08 T' AND st_astext(obm_geometry) = 'POINT(25.0368430092931 45.6173180323094)';
    UPDATE pisces SET location_start = 'SEBES09T' WHERE location_start = 'Sebes09 T' AND st_astext(obm_geometry) = 'POINT(25.0263989903033 45.6127850152552)';
    UPDATE pisces SET location_start = 'SE1AVCAT' WHERE location_start = 'Sebes11 T' AND st_astext(obm_geometry) = 'POINT(25.0126359891146 45.6134020071477)';
    UPDATE pisces SET location_start = 'SE1AMCAT' WHERE location_start = 'Sebes13 T' AND st_astext(obm_geometry) = 'POINT(25.0076940190047 45.6124339811504)';
    UPDATE pisces SET location_start = 'SZK1T_ALSO' WHERE location_start = 'SzK1 T(also)' AND st_astext(obm_geometry) = 'POINT(25.8633747523219 46.2920332106283)';
    UPDATE pisces SET location_start = 'SZK1T_ALSO' WHERE location_start = 'SzK1 T(also)' AND st_astext(obm_geometry) = 'POINT(25.863715 46.293457)';
    UPDATE pisces SET location_start = 'SZK1T_ALSO' WHERE location_start = 'SzK1 T(also)' AND st_astext(obm_geometry) = 'POINT(25.8638299703189 46.2936677795853)';
    UPDATE pisces SET location_start = 'SZK1T_FELSO' WHERE location_start = 'SzK1 T(felso)' AND st_astext(obm_geometry) = 'POINT(25.833025 46.31089)';
    UPDATE pisces SET location_start = 'SZK1T_FELSO' WHERE location_start = 'SzK1 T(felso)' AND st_astext(obm_geometry) = 'POINT(25.8330547673989 46.3109643771355)';
    UPDATE pisces SET location_start = 'SZK1T_FELSO' WHERE location_start = 'SzK1 T(felso)' AND st_astext(obm_geometry) = 'POINT(25.8331366617488 46.3109784940056)';
    UPDATE pisces SET location_start = 'SZK2T' WHERE location_start = 'SzK2 T' AND st_astext(obm_geometry) = 'POINT(25.8618762230789 46.3000915368028)';
    UPDATE pisces SET location_start = 'SZK2T' WHERE location_start = 'SzK2 T' AND st_astext(obm_geometry) = 'POINT(25.8619019478201 46.3001424532358)';
    UPDATE pisces SET location_start = 'SZK2T' WHERE location_start = 'SzK2 T' AND st_astext(obm_geometry) = 'POINT(25.862245 46.299454)';
    UPDATE pisces SET location_start = 'SZK3T' WHERE location_start = 'SzK3 T' AND st_astext(obm_geometry) = 'POINT(25.8555771100357 46.306980662973)';
    UPDATE pisces SET location_start = 'SZK3T' WHERE location_start = 'SzK3 T' AND st_astext(obm_geometry) = 'POINT(25.855623159311 46.3069218056545)';
    UPDATE pisces SET location_start = 'SZK3T' WHERE location_start = 'SzK3 T' AND st_astext(obm_geometry) = 'POINT(25.855701 46.306784)';
    UPDATE pisces SET location_start = 'UCAVCAT' WHERE location_start = 'UCAVCA T' AND st_astext(obm_geometry) = 'POINT(24.7065528051642 45.6884232298898)';
    UPDATE pisces SET location_start = 'UCAVCAT' WHERE location_start = 'UCAVCA T' AND st_astext(obm_geometry) = 'POINT(24.707388 45.688256)';
    UPDATE pisces SET location_start = 'UCAMCAT' WHERE location_start = 'Ucea amonte captare T' AND st_astext(obm_geometry) = 'POINT(24.70861 45.683989)';
    UPDATE pisces SET location_start = 'UCAMCET' WHERE location_start = 'Ucea amonte MHC T' AND st_astext(obm_geometry) = 'POINT(24.690066 45.739509)';
    UPDATE pisces SET location_start = 'UCAMCET' WHERE location_start = 'Ucea amonte T' AND st_astext(obm_geometry) = 'POINT(24.690066 45.739509)';
    UPDATE pisces SET location_start = 'UCAVCAT' WHERE location_start = 'Ucea aval captare T' AND st_astext(obm_geometry) = 'POINT(24.707388 45.688256)';
    UPDATE pisces SET location_start = 'UCAVCET' WHERE location_start = 'Ucea aval MHC T' AND st_astext(obm_geometry) = 'POINT(24.688455 45.743429)';
    UPDATE pisces SET location_start = 'VAMCAT' WHERE location_start = 'VAMCAPTT' AND st_astext(obm_geometry) = 'POINT(25.574782 46.418217)';
COMMIT;

-- 2. geometriák javitasa
\copy (
    with lss as (
        select location_start from pisces where location_start is not null GROUP BY location_start HAVING count(DISTINCT obm_geometry) > 1
    )
    select distinct 
        p.location_start, 
        qg.bazin,
        st_astext(obm_geometry) as geom
    from pisces p
    INNER JOIN lss ON lss.location_start = p.location_start
    LEFT JOIN pisces_qgrids qg on qg.data_table = 'pisces' AND row_id = obm_id
    ORDER BY bazin, location_start
) to /tmp/pisces_geometria_javitasok.csv csv header;


with lss as (
    select location_start from pisces where location_start is not null GROUP BY location_start HAVING count(DISTINCT obm_geometry) > 1
), 
asdf as (
    select distinct 
        p.location_start, 
        qg.bazin,
        st_astext(obm_geometry) as geom
    from pisces p
    INNER JOIN lss ON lss.location_start = p.location_start
    LEFT JOIN pisces_qgrids qg on qg.data_table = 'pisces' AND row_id = obm_id
    ORDER BY bazin, location_start
)
SELECT count(distinct location_start) from asdf;





-- átteszem azokat, ahol a sscd nem null

SELECT count(distinct location_start) from pisces WHERE survey_station_code_deprecated IS NOT NULL;
-- 978
SELECT count(distinct survey_station_code_deprecated) from pisces WHERE survey_station_code_deprecated IS NOT NULL;
-- 980
SELECT location_start, count(distinct survey_station_code_deprecated) from pisces WHERE survey_station_code_deprecated IS NOT NULL group by location_start HAVING count(distinct survey_station_code_deprecated) > 1;
--  location_start | count 
-- ----------------+-------
--  Uz 1 T         |     2


\copy (SELECT DISTINCT 
    st_astext(obm_geometry) as wkt,
    survey_station_code_deprecated as survey_station_code,
    location_start,
    location_end,
    end_geometry,
    water_type,
    river,
    altitude,
    survey_station_code_deprecated,
    bazin,
    bazin_code,
    sci,
    bazin2_ccm2 as ccm2,
    judet,
    comuna,
    corine_code,
    country
FROM pisces p
LEFT JOIN pisces_qgrids qg on qg.data_table = 'pisces' AND row_id = obm_id
WHERE location_start IS NOT NULL) to '/tmp/survey_sites.csv' CSV HEADER;

SELECT DISTINCT location_start, river, bazin FROM pisces inner join pisces_qgrids on row_id = obm_id WHERE location_start IN (SELECT location_start FROM pisces GROUP BY location_start HAVING count(DISTINCT coalesce(river, 'unknown')) > 1) ORDER BY location_start;

update pisces p set water_type = c.water_type FROM (
    SELECT DISTINCT location_start, water_type FROM pisces where location_start is not null and water_type is not null
) as c WHERE p.location_start = c.location_start;


SELECT distinct location_start, min(survey_station_code_deprecated), string_agg(distinct survey_station_code_deprecated, ',') FROM pisces WHERE survey_station_code_deprecated IS NOT NULL GROUP BY location_start HAVING count(distinct survey_station_code_deprecated) > 1 ORDER BY location_start;

SELECT distinct survey_station_code_deprecated, string_agg(distinct location_start, ',') FROM pisces WHERE survey_station_code_deprecated IS NOT NULL GROUP BY survey_station_code_deprecated HAVING count(distinct location_start) > 1 ORDER BY survey_station_code_deprecated;

select site_id, round from (
SELECT distinct zk.site_id, round(st_distance(st_transform(obm_geometry, 3844), st_transform(st_geomfromtext('POINT(' || zk.x::varchar || ' ' || zk.y::varchar || ')', 4326), 3844))::numeric, 2)
FROM 
    pisces p
INNER JOIN tmp.pisces_zookeys_adatsor zk ON survey_station_code_deprecated = site_id) foo 
WHERE round > 1;

UPDATE pisces SET survey_station_code = NULL;

update pisces p set survey_station_code = c.survey_station_code_deprecated FROM (
SELECT DISTINCT location_start, survey_station_code_deprecated FROM pisces where survey_station_code_deprecated IS NOT NULL AND location_start in (SELECT distinct location_start FROM pisces WHERE location_start is not null and survey_station_code_deprecated IS NOT NULL GROUP BY location_start HAVING count(distinct survey_station_code_deprecated) = 1 ORDER BY location_start)
) as c WHERE p.location_start = c.location_start AND p.survey_station_code_deprecated IS NULL;


update pisces p set survey_station_code = c.min FROM (
    SELECT distinct location_start, min(survey_station_code_deprecated), string_agg(distinct survey_station_code_deprecated, ',') FROM pisces WHERE survey_station_code_deprecated IS NOT NULL GROUP BY location_start HAVING count(distinct survey_station_code_deprecated) > 1 ORDER BY location_start;
) as c WHERE p.location_start = c.location_start;

SELECT DISTINCT location_start, sci FROM pisces left join pisces_qgrids on row_id = obm_id WHERE location_start IN (SELECT location_start FROM pisces left join pisces_qgrids on row_id = obm_id GROUP BY location_start HAVING count(DISTINCT coalesce(sci, 'unknown')) > 1) ORDER BY location_start;

update pisces p set water_type = c.water_type FROM (
    SELECT DISTINCT location_start, water_type FROM pisces where location_start is not null and water_type is not null
) as c WHERE p.location_start = c.location_start;

INSERT INTO pisces_survey_sites (obm_geometry, location_start, location_end, survey_station_code, survey_station_code_deprecated, end_geometry, water_type, river, basin, basin_code, ccm2, country)
SELECT DISTINCT
    obm_geometry,
    location_start,
    location_end,
    survey_station_code,
    string_agg(distinct survey_station_code_deprecated, ',') as zookeys_ssc,
    end_geometry,
    water_type,
    river,
    bazin as basin,
    bazin_code as basin_code,
    bazin2_ccm2 as ccm2,
    country
FROM pisces p
LEFT JOIN pisces_qgrids q ON row_id = obm_id
WHERE location_start is not null
GROUP BY obm_geometry, location_start, location_end, survey_station_code, end_geometry, water_type, river,
    bazin, bazin_code, bazin2_ccm2, country;


ALTER TABLE pisces ADD COLUMN site_id integer;
ALTER TABLE ONLY "public"."pisces" ADD CONSTRAINT "pisces_site_id_fkey" FOREIGN KEY (site_id) REFERENCES pisces_survey_sites(obm_id) ON UPDATE RESTRICT ON DELETE RESTRICT NOT DEFERRABLE;

UPDATE pisces p SET site_id = s.obm_id FROM pisces_survey_sites s WHERE p.location_start = s.location_start AND p.obm_geometry = s.obm_geometry;





------------------------------------
-- EVENTS
------------------------------------
INSERT INTO pisces_survey_events (
    site_id, "date", time_start, time_end, team, depth_mean, depth_max, covering, shading, meander, electrofisher_type, type_of_electrofishing, water_level, land_use_right, rb_fine_sediment, rb_sand, rb_gravel, rb_stone, rb_large_stone, rb_concrete, rb_clay, lu_right_bank, "lu_left_bank", "lbv_grasses", "lbv_wood", "lbv_stone", "lbv_concrete", "rbv_grasses", "rbv_wood", "rbv_stone", "rbv_concrete", "ws_rapid", "ws_medium", "ws_slow", "ws_stagnant", "transparency", "naturalness", "date_of_modification", "ph", "conductivity", "tds", "disolved_oxygen_proc", "disolved_oxygen_mgl", "temperature", "photo_number", "repertofon", comments, "lbv_bushes", "rbv_bushes", "method", "site_length", "site_width", "site_width_max", "area_surface", "pressures", "threats"
)

SELECT DISTINCT 
        site_id,
        "date",                 -- 2480
        time_start,             -- 2480
        time_end,               -- 2480
        team,                   -- 2484
        depth_mean,             -- 2489
        depth_max,              -- 2489
        covering,               -- 2489
        shading,                -- 2489
        meander,                -- 2489
        electrofisher_type,     -- 2489
        type_of_electrofishing, -- 2489
        water_level,            -- 2490
        land_use_right,         -- 2490
        rb_fine_sediment,       -- 2490
        rb_sand,                -- 2490
        rb_gravel,              -- 2490
        rb_stone,               -- 2490
        rb_large_stone,         -- 2490
        rb_concrete,            -- 2490
        rb_clay,                -- 2490
        lu_right_bank,
        "lu_left_bank",
        "lbv_grasses",
        "lbv_wood",
        "lbv_stone",
        "lbv_concrete",
        "rbv_grasses",
        "rbv_wood",
        "rbv_stone",
        "rbv_concrete",
        "ws_rapid",
        "ws_medium",
        "ws_slow",
        "ws_stagnant",
        "transparency",
        "naturalness",
        "date_of_modification",
        "ph",
        "conductivity",
        "tds",
        "disolved_oxygen_proc",
        "disolved_oxygen_mgl",
        "temperature",
        "photo_number",
        "repertofon",
        "observation" as comments,
        "lbv_bushes",
        "rbv_bushes",
        "method",
        "site_length",
        "site_width",
        "site_width_max",
        "area_surface",
        "pressures",
        "threats"          -- 2490
    FROM pisces WHERE site_id is not null;


-- team
-------

SELECT DISTINCT site_id, date, team FROM pisces WHERE (site_id, date) IN (
    SELECT site_id, date FROM pisces GROUP BY site_id, date HAVING count(DISTINCT coalesce(team, 'unknown')) > 1
) ORDER BY site_id;

--  site_id |    date    |                     team                     
-- ---------+------------+----------------------------------------------
--     1915 | 2021-10-11 | Imecs István, Kelemen Alpár
--     1915 | 2021-10-11 | Kelemen Alpár, Imecs István,
--     2454 | 2021-05-12 | Imecs Istvan Gyorgy Leonard
--     2454 | 2021-05-12 | Kelemen Alpár, Imecs István, György Leonárd,
--     2863 | 2018-09-04 | Imecs István, Nagy András Attila,
--     2863 | 2018-09-04 | Nagy Andras Attila, Imecs Istvan
--     3264 | 2015-06-13 | Imecs István, Gothárd Ferenc Alpár,
--     3264 | 2015-06-13 | Nagy András Attila, Gothárd Ferenc Alpár,


-- depth_mean
-------------

SELECT DISTINCT site_id, date, team, depth_mean FROM pisces WHERE (site_id, date, team) IN (
    SELECT site_id, date, team FROM pisces GROUP BY site_id, date, team HAVING count(DISTINCT coalesce(depth_mean, -1)) > 1
) ORDER BY site_id;

--  site_id |    date    |                team                 | depth_mean 
-- ---------+------------+-------------------------------------+------------
--     1911 | 2020-07-03 | Imecs Istvan Kelemen Alpar          |          9
--     1911 | 2020-07-03 | Imecs Istvan Kelemen Alpar          |         10
--     1915 | 2022-10-21 | Kelemen Alpár, Erős Nándor,         |         20
--     1915 | 2022-10-21 | Kelemen Alpár, Erős Nándor,         |         25
--     2739 | 2014-10-25 | Imecs Istvan, Kelemen Alpar         |         15
--     2739 | 2014-10-25 | Imecs Istvan, Kelemen Alpar         |           
--     2966 | 2019-09-03 | Imecs Istvan Kelemen Alpar          |         14
--     2966 | 2019-09-03 | Imecs Istvan Kelemen Alpar          |         15
--     2981 | 2018-08-05 | Imecs István, Gothárd Ferenc Alpár, |         25
--     2981 | 2018-08-05 | Imecs István, Gothárd Ferenc Alpár, |         40

-- water_level
--------------

SELECT DISTINCT site_id, date, team, water_level FROM pisces WHERE (site_id, date, team, depth_mean) IN (
    SELECT site_id, date, team, depth_mean FROM pisces GROUP BY site_id, date, team, depth_mean HAVING count(DISTINCT coalesce(water_level, -1)) > 1
) ORDER BY site_id;

--  site_id |    date    |        team         | water_level 
-- ---------+------------+---------------------+-------------
--     2238 | 2015-06-06 | Nagy András Attila, |           1
--     2238 | 2015-06-06 | Nagy András Attila, |           4



---------------------------
-- pisces records
---------------------------

ALTER TABLE pisces ADD COLUMN event_id integer;

UPDATE pisces p SET event_id = e.obm_id FROM pisces_survey_events e WHERE 
    p.site_id is not distinct from e.site_id AND
    p.date is not distinct from e.date AND
    p.team is not distinct from e.team AND
    p.depth_mean is not distinct from e.depth_mean AND
    p.water_level is not distinct from e.water_level;

SELECT count(*) FROM pisces WHERE event_id is not null; --10221

INSERT INTO pisces_survey_records (event_id, species, total_no, juvenile, adult, photo_number, info_source, exact_time)
SELECT
    event_id,
    species,            -- 10212
    total_no,
    juvenile,
    adult,
    photo_number,
    info_source,
    exact_time
FROM pisces
WHERE event_id is not null;

select count(*) from pisces_survey_records;


-- electrofisher_type
ALTER TABLE pisces_survey_events ALTER COLUMN "electrofisher_type" TYPE character varying(32);

{"list":{"_empty_":[],"0":["Samus 1000"],"1":["Samus725"],"2":["Other"],"3":["Hans Grassl EL 64 II GI"]}}

UPDATE pisces_survey_events SET electrofisher_type = 'Samus 1000' WHERE electrofisher_type = '0';
UPDATE pisces_survey_events SET electrofisher_type = 'Samus725' WHERE electrofisher_type = '1';
UPDATE pisces_survey_events SET electrofisher_type = 'Other' WHERE electrofisher_type = '2';

INSERT INTO pisces_terms (data_table, subject, term, status) VALUES 
    ('pisces_survey_events', 'electrofisher_type', 'Samus 1000', 'accepted'),
    ('pisces_survey_events', 'electrofisher_type', 'Samus725', 'accepted'),
    ('pisces_survey_events', 'electrofisher_type', 'Other', 'accepted'),
    ('pisces_survey_events', 'electrofisher_type', 'Hans Grassl EL 64 II GI', 'accepted');


-- type_of_electrofishing
INSERT INTO pisces_terms (data_table, subject, term, status) select 'pisces_survey_events', 'type_of_electrofishing', name, 'accepted' from  get_not_null_distinct('pisces_survey_events', 'type_of_electrofishing');


-- water_level
ALTER TABLE pisces_survey_events ALTER COLUMN "water_level" TYPE character varying(32);

{"list":{"_empty_":[],"1":["very low"],"2":["low"],"3":["normal"],"4":["high"],"5":["very high"]}}

UPDATE pisces_survey_events SET water_level = 'very low' WHERE water_level = '1';
UPDATE pisces_survey_events SET water_level = 'low' WHERE water_level = '2';
UPDATE pisces_survey_events SET water_level = 'normal' WHERE water_level = '3';
UPDATE pisces_survey_events SET water_level = 'high' WHERE water_level = '4';
UPDATE pisces_survey_events SET water_level = 'very high' WHERE water_level = '5';

INSERT INTO pisces_terms (data_table, subject, term, status) select 'pisces_survey_events', 'water_level', name, 'accepted' from  get_not_null_distinct('pisces_survey_events', 'water_level');

-- lu_right_bank
select distinct trim(unnest(string_to_array(name, ','))) as name from  get_not_null_distinct('pisces_survey_events', 'lu_right_bank') order by name;
-- lu_left_bank
select distinct trim(unnest(string_to_array(name, ','))) as name from  get_not_null_distinct('pisces_survey_events', 'lu_left_bank') order by name;


-- transparency
ALTER TABLE pisces_survey_events ALTER COLUMN "transparency" TYPE character varying(8);

{"list": {"_empty_": [],"1": ["0-10"],"2": ["10-20"],"3": ["20-40"],"4": ["40-70"],"5": [">70"]}}

UPDATE pisces_survey_events SET transparency = '0-10' WHERE transparency = '1';
UPDATE pisces_survey_events SET transparency = '10-20' WHERE transparency = '2';
UPDATE pisces_survey_events SET transparency = '20-40' WHERE transparency = '3';
UPDATE pisces_survey_events SET transparency = '40-70' WHERE transparency = '4';
UPDATE pisces_survey_events SET transparency = '>70' WHERE transparency = '5';


INSERT INTO pisces_terms (data_table, subject, term, status) select 'pisces_survey_events', 'transparency', name, 'accepted' from  get_not_null_distinct('pisces_survey_events', 'transparency');

-- naturalness
ALTER TABLE pisces_survey_events ALTER COLUMN "naturalness" TYPE character varying(24);

{"list": {"_empty_": [],"1": ["almost natural"],"2": ["slightly modified"],"3": ["modified"],"4": ["very modified"]}}

UPDATE pisces_survey_events SET naturalness = 'almost natural' WHERE naturalness = '1';
UPDATE pisces_survey_events SET naturalness = 'slightly modified' WHERE naturalness = '2';
UPDATE pisces_survey_events SET naturalness = 'modified' WHERE naturalness = '3';
UPDATE pisces_survey_events SET naturalness = 'very modified' WHERE naturalness = '4';


INSERT INTO pisces_terms (data_table, subject, term, status) select 'pisces_survey_events', 'naturalness', name, 'accepted' from  get_not_null_distinct('pisces_survey_events', 'naturalness');


-- date_of_modification


{"list": {"_empty_": [],"0-1 year": [],"1-3 year": [],"3-6 year": [],"6-10 year": [],"10-20 year": [],"20-30 year": [],"30-50 year": [],"50-100 year": [],">100 year": []}}

UPDATE pisces_survey_events SET date_of_modification = '30-50 year' WHERE date_of_modification = ' 30-50 year';


INSERT INTO pisces_terms (data_table, subject, term, status) select 'pisces_survey_events', 'date_of_modification', name, 'accepted' from  get_not_null_distinct('pisces_survey_events', 'date_of_modification');


-- qgrids tábla

TRUNCATE pisces_qgrids;
INSERT INTO pisces_qgrids (row_id, original, etrs_name, etrs_geom, data_table, centroid) 
SELECT 
    obm_id,
    obm_geometry,
    e.name,
    e.geometry,
    'pisces',
    st_centroid(obm_geometry)
FROM pisces r
LEFT JOIN shared.etrs10 e ON st_intersects(st_centroid(r.obm_geometry), e.geometry)

-- TÖRÖLTEM A DUPLÁKAT MÉG RÁ KELL ELLENŐRIZNI

--  obm_uploading_id |               team                |    date    |       species       | event_id 
-- ------------------+-----------------------------------+------------+---------------------+----------
--             25171 | Nagy András Attila, Kovács Attila | 2018-08-31 | Gobio gobio         |     1730
--             25171 | Nagy András Attila, Kovács Attila | 2018-08-31 | Pseudorasbora parva |     1730
--             25171 | Nagy András Attila, Kovács Attila | 2018-08-31 | Rhodeus amarus      |     1730
--             25172 | Nagy András Attila, Kovács Attila | 2018-08-31 | Gobio gobio         |     1730
--             25172 | Nagy András Attila, Kovács Attila | 2018-08-31 | Pseudorasbora parva |     1730
--             25172 | Nagy András Attila, Kovács Attila | 2018-08-31 | Rhodeus amarus      |     1730
--            693003 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Cottus gobio        |     1816
--            693003 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Oncorhynchus mykiss |     1816
--            693003 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Phoxinus phoxinus   |     1816
--            693003 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Salmo trutta        |     1816
--            693005 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Cottus gobio        |     1816
--            693005 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Oncorhynchus mykiss |     1816
--            693005 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Phoxinus phoxinus   |     1816
--            693005 | Kelemen Alpár, Imecs István,      | 2018-07-18 | Salmo trutta        |     1816



select distinct 
    p.obm_uploading_id, p.team, p.date, p.species, p.event_id 
from pisces r 
inner join pisces.pisces p on 
    r.team is not distinct from p.team and 
    r.date is not distinct from p.date and 
    r.species is not distinct from p.species and 
    r.event_id = p.event_id 
where p.location_start is not null group by p.obm_uploading_id, p.team, p.date, p.species, p.event_id having count(*) > 1;

-- uploading id-k másolása a surevey_records-ba
with ids as (
select r.obm_id, rr.*
from pisces r 
inner join pisces.pisces p on 
    r.team is not distinct from p.team and 
    r.date is not distinct from p.date and 
    r.species is not distinct from p.species and 
    p.event_id = r.event_id 
INNER JOIN pisces.pisces_rules_old rr ON p.obm_id = rr.row_id
where p.location_start is not null
)
UPDATE pisces_rules SET 
    "owners" = ids."owners", 
    "groups" = ids.groups, 
    rights = ids.rights,
    "read" = ids."read",
    "write" = ids."write",
    sensitivity = ids.sensitivity,
    download = ids.download
FROM ids
WHERE pisces_rules.row_id = ids.obm_id;

