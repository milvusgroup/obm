ALTER TABLE shared.bazine_hidrografice ADD COLUMN geom_4326 geometry(MultiPolygon, 4326);
UPDATE shared.bazine_hidrografice SET geom_4326 = st_transform(geometry, 4326);
CREATE INDEX bazine_hidrografice_geom_4326_idx ON shared.bazine_hidrografice USING GIST(geom_4326);
ALTER TABLE shared.bazine_hidrografice ADD COLUMN bazin_mare varchar(32);

update pisces_survey_sites set basin_minor = denumire from shared.bazine_hidrografice where st_intersects(geom_4326, obm_geometry);
UPDATE shared.bazine_hidrografice b SET bazin_mare = bm.bazin FROM shared.bazine_hidrografice_mari bm WHERE st_intersects(st_centroid(b.geom_4326), bm.geometry );

UPDATE pisces_survey_events SET basin = s.basin, basin_minor = s.basin_minor FROM pisces_survey_sites s WHERE site_id = s.obm_id;