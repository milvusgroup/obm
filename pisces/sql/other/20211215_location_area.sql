-- select * from information_schema.columns WHERE table_name = 'pisces';
-- BEGIN; 
-- UPDATE pisces SET area_surface = site_width*3 WHERE area_surface IS NULL AND site_width IS NOT NULL;
-- COMMIT;
-- 
-- -- undo
-- BEGIN;
-- WITH modified as (
--     SELECT row_id from pisces_history where hist_time::varchar LIKE '2021-12-15 11:27%'
--     ),
-- area_calculated as (
--     
--     SELECT 
--         obm_id,
--         CASE 
--             WHEN site_width < 3 THEN (site_length*site_width)::integer
--             ELSE (site_length * 3) 
--         END as area_surface, 
--         site_width, 
--         site_length 
--     FROM pisces p LEFT JOIN modified m ON m.row_id = p.obm_id WHERE m.row_id IS NOT NULL
--     )
-- UPDATE pisces p SET area_surface = ac.area_surface FROM area_calculated ac WHERE p.obm_id = ac.obm_id;
-- COMMIT;

select count(*) from pisces;
select count(DISTINCT(location_start)) FROM pisces;