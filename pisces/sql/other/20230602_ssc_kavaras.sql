UPDATE pisces SET survey_station_code_deprecated = survey_station_code;
-- 605537 feltöltés előtt rendben voltak a survey_station_code-ok.
UPDATE pisces SET survey_station_code_deprecated = NULL WHERE obm_uploading_id > 605537;


-- az új formátumu ssc, mely tartalmazza a hónapot is
UPDATE pisces SET survey_station_code = NULL;
BEGIN;
WITH points as (
    SELECT DISTINCT location_start, bazin_code, extract(year from obm_datum) as ev, extract(month from obm_datum) as honap, obm_datum FROM pisces LEFT JOIN pisces_qgrids ON obm_id = row_id ORDER BY obm_datum
    ),
codes as (
    SELECT location_start, obm_datum, bazin_code || SUBSTRING(ev::varchar, 3, 4) || lpad(honap::text, 2, '0') || '_' || LPAD(ROW_NUMBER() OVER(PARTITION BY bazin_code, ev ORDER BY obm_datum)::varchar, 3, '0') as ssc FROM points 
    )
UPDATE pisces SET survey_station_code = ssc FROM codes WHERE codes.location_start = pisces.location_start AND codes.obm_datum = pisces.obm_datum;
commit;
