UPDATE pisces SET survey_station_code = NULL;
BEGIN;
WITH points as (
    SELECT DISTINCT location_start, bazin_code, extract(year from obm_datum) as ev, obm_datum FROM pisces LEFT JOIN pisces_qgrids ON obm_id = row_id 
    ),
codes as (
    SELECT location_start, bazin_code || SUBSTRING(ev::varchar, 3, 4) || LPAD(ROW_NUMBER() OVER(PARTITION BY bazin_code, ev ORDER BY obm_datum)::varchar, 3, '0') as ssc FROM points 
    )
UPDATE pisces SET survey_station_code = ssc FROM codes WHERE codes.location_start = pisces.location_start;
commit;
