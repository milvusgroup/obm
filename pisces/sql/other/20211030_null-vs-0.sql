-- BACKUP
COPY (SELECT * FROM pisces) TO '/tmp/pisces_backup_20211030.csv' CSV HEADER;

-- RIVER BED

SELECT column_name from information_schema.columns WHERE table_name = 'pisces' AND column_name LIKE 'rb\_%';
-- rb_fine_sediment
-- rb_sand
-- rb_gravel
-- rb_stone
-- rb_large_stone
-- rb_concrete
-- rb_clay

WITH rb as (
    SELECT 
        obm_id,
        rb_fine_sediment, rb_sand, rb_gravel, rb_stone, rb_large_stone, rb_clay, rb_concrete,
        COALESCE(rb_fine_sediment, 0) + COALESCE(rb_sand, 0) + COALESCE(rb_gravel, 0) + 
        COALESCE(rb_stone, 0) + COALESCE(rb_large_stone, 0) + COALESCE(rb_clay, 0) + 
        COALESCE(rb_concrete, 0) AS sum
    FROM pisces
    ),
ids as (
    SELECT obm_id FROM rb WHERE sum = 100
    )
SELECT * FROM rb WHERE sum != 0 AND sum != 100
UPDATE pisces SET rb_clay = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rb_clay IS NULL;
UPDATE pisces SET rb_concrete = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rb_concrete IS NULL;
UPDATE pisces SET rb_large_stone = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rb_large_stone IS NULL;
UPDATE pisces SET rb_stone = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rb_stone IS NULL;
UPDATE pisces SET rb_gravel = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rb_gravel IS NULL;
UPDATE pisces SET rb_sand = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rb_sand IS NULL;
UPDATE pisces SET rb_fine_sediment = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rb_fine_sediment IS NULL;

COPY (
    ) TO '/tmp/rb_hianyos.csv' CSV HEADER;




-- LEFT BANK VEGETATION

SELECT column_name from information_schema.columns WHERE table_name = 'pisces' AND column_name LIKE 'lbv\_%';
-- lbv_grasses
-- lbv_wood
-- lbv_stone
-- lbv_concrete
-- lbv_bushes

BEGIN;
WITH lbv as (
    SELECT 
        obm_id,
        lbv_grasses, lbv_wood, lbv_stone, lbv_concrete, lbv_bushes,
        COALESCE(lbv_grasses, 0) + COALESCE(lbv_wood, 0) + COALESCE(lbv_stone, 0) +
        COALESCE(lbv_concrete, 0) + COALESCE(lbv_bushes, 0) as sum
    FROM pisces
    ),
ids as (
    SELECT obm_id FROM lbv WHERE sum = 100
    )
SELECT * FROM lbv;
UPDATE pisces SET lbv_grasses = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND lbv_grasses IS NULL;
UPDATE pisces SET lbv_wood = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND lbv_wood IS NULL;
UPDATE pisces SET lbv_stone = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND lbv_stone IS NULL;
UPDATE pisces SET lbv_concrete = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND lbv_concrete IS NULL;
UPDATE pisces SET lbv_bushes = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND lbv_bushes IS NULL;
COMMIT;

SELECT * FROM lbv WHERE sum != 0 AND sum != 100

COPY (
    ) TO '/tmp/lbv_hianyos.csv' CSV HEADER;

-- RIGHT BANK VEGETATION

SELECT column_name from information_schema.columns WHERE table_name = 'pisces' AND column_name LIKE 'rbv\_%';
-- rbv_grasses
-- rbv_wood
-- rbv_stone
-- rbv_concrete
-- rbv_bushes

BEGIN;
WITH rbv as (
    SELECT 
        obm_id,
        rbv_grasses, rbv_wood, rbv_stone, rbv_concrete, rbv_bushes,
        COALESCE(rbv_grasses, 0) + COALESCE(rbv_wood, 0) + COALESCE(rbv_stone, 0) +
        COALESCE(rbv_concrete, 0) + COALESCE(rbv_bushes, 0) as sum
    FROM pisces
    ),
ids as (
    SELECT obm_id FROM rbv WHERE sum = 100
    )
SELECT * FROM rbv;
UPDATE pisces SET rbv_grasses = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rbv_grasses IS NULL;
UPDATE pisces SET rbv_wood = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rbv_wood IS NULL;
UPDATE pisces SET rbv_stone = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rbv_stone IS NULL;
UPDATE pisces SET rbv_concrete = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rbv_concrete IS NULL;
UPDATE pisces SET rbv_bushes = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND rbv_bushes IS NULL;
COMMIT;

COPY (
    SELECT * FROM rbv WHERE sum != 0 AND sum != 100
) TO '/tmp/rbv_hianyos.csv' CSV HEADER;

-- WATER SPEED

SELECT column_name from information_schema.columns WHERE table_name = 'pisces' AND column_name LIKE 'ws\_%';
-- ws_rapid
-- ws_medium
-- ws_slow
-- ws_stagnant

BEGIN;
WITH ws as (
    SELECT 
        obm_id,
        ws_rapid, ws_medium, ws_slow, ws_stagnant, 
        COALESCE(ws_rapid, 0) + COALESCE(ws_medium, 0) + COALESCE(ws_slow, 0) +
        COALESCE(ws_stagnant, 0) as sum
    FROM pisces
    ),
ids as (
    SELECT obm_id FROM ws WHERE sum = 100
    )
UPDATE pisces SET ws_stagnant = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND ws_stagnant IS NULL;
UPDATE pisces SET ws_rapid = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND ws_rapid IS NULL;
UPDATE pisces SET ws_medium = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND ws_medium IS NULL;
UPDATE pisces SET ws_slow = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND ws_slow IS NULL;

COMMIT;

SELECT * FROM ws WHERE ws_rapid IS NULL AND sum = 100;

COPY (
    SELECT * FROM ws WHERE sum != 0 AND sum != 100
    ) TO '/tmp/ws_hianyos.csv' CSV HEADER;


-- EGYEDSZAMOK

BEGIN;
WITH n as (
    SELECT obm_id, juvenile, adult, total_no, COALESCE(juvenile, 0) + COALESCE(adult, 0) as sum FROM pisces
    ),
ids as (
    SELECT obm_id FROM n WHERE sum = total_no
    )
UPDATE pisces SET adult = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND adult IS NULL;
UPDATE pisces SET juvenile = 0 FROM ids WHERE pisces.obm_id = ids.obm_id AND juvenile IS NULL;
COMMIT;

COPY (
    SELECT * FROM n WHERE total_no = 0
) TO '/tmp/egyedszam_0.csv' CSV HEADER;

COPY (
    SELECT * FROM n WHERE total_no != sum AND sum != 0
) TO '/tmp/egyedszam_hibas.csv' CSV HEADER;

begin;
WITH n as (
    SELECT obm_id, juvenile, adult, total_no, COALESCE(juvenile, 0) + COALESCE(adult, 0) as sum FROM pisces
    ),
ids as (
    SELECT obm_id, sum FROM n WHERE sum != total_no AND sum != 0
    )
UPDATE pisces SET total_no = sum FROM ids WHERE pisces.obm_id = ids.obm_id;

UPDATE pisces SET rbv_stone = 40 WHERE obm_id = 3974;
UPDATE pisces SET rbv_stone = 40 WHERE obm_id = 3975;
UPDATE pisces SET rbv_stone = 40 WHERE obm_id = 3976;
UPDATE pisces SET rbv_stone = 40 WHERE obm_id = 3977;

UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3978;
UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3979;
UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3980;
UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3981;
UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3982;
UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3983;
UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3984;
UPDATE pisces SET rb_stone = 45 WHERE obm_id = 3985;

UPDATE pisces SET rb_gravel = 45 WHERE obm_id = 4080;
UPDATE pisces SET rb_gravel = 45 WHERE obm_id = 4081;
UPDATE pisces SET rb_gravel = 45 WHERE obm_id = 4082;
UPDATE pisces SET rb_gravel = 45 WHERE obm_id = 4083;
UPDATE pisces SET rb_gravel = 45 WHERE obm_id = 4084;
UPDATE pisces SET rb_gravel = 45 WHERE obm_id = 4085;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4102;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4103;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4104;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4105;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4106;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4107;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4108;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4109;
UPDATE pisces SET rb_gravel = 30 WHERE obm_id = 4110;

UPDATE pisces SET lbv_grasses = 60 WHERE obm_id = 3768;
UPDATE pisces SET lbv_grasses = 60 WHERE obm_id = 3766;
UPDATE pisces SET lbv_grasses = 60 WHERE obm_id = 3771;
UPDATE pisces SET lbv_grasses = 60 WHERE obm_id = 3769;
UPDATE pisces SET lbv_grasses = 60 WHERE obm_id = 3767;
UPDATE pisces SET lbv_grasses = 60 WHERE obm_id = 3770;
COMMIT;


