
-- ALTER TABLE pisces SET SCHEMA pisces;

--DROP VIEW public.pisces;
CREATE OR REPLACE VIEW public.pisces AS
    SELECT 
    r.obm_id,
    coalesce(r.obm_geometry, e.obm_geometry, s.obm_geometry) as obm_geometry,
    null::timestamptz as obm_datum,
    r.obm_uploading_id,
    r.obm_validation,
    r.obm_comments,
    r.obm_modifier_id,
    r.obm_files_id,
    s.location_start,
    s.location_end,
    s.basin,
    s.river,
    e."date",
    e.time_start,
    e.time_end,
    e.team,
    e.site_length,
    e.site_width,
    e.area_surface,
    e.depth_mean,
    e.depth_max,
    e.covering,
    e.shading,
    e.meander,
    e.electrofisher_type,
    e.type_of_electrofishing,
    e.water_level,
    e.land_use_right,
    e.rb_fine_sediment,
    e.rb_sand,
    e.rb_gravel,
    e.rb_stone,
    e.rb_large_stone,
    e.rb_concrete,
    e.rb_clay,
    e.lu_right_bank,
    e.lu_left_bank,
    e.lbv_grasses,
    e.lbv_wood,
    e.lbv_stone,
    e.lbv_concrete,
    e.rbv_grasses,
    e.rbv_wood,
    e.rbv_stone,
    e.rbv_concrete,
    e.ws_rapid,
    e.ws_medium,
    e.ws_slow,
    e.ws_stagnant,
    e.transparency,
    e.naturalness,
    e.date_of_modification,
    e.ph,
    e.conductivity,
    e.tds,
    e.disolved_oxygen_proc,
    e.disolved_oxygen_mgl,
    e.temperature,
    r.species,
    r.total_no,
    r.juvenile,
    r.adult,
    null as natura2000,
    e.pressures,
    e.threats,
    r.photo_number,
    e.repertofon,
    concat_ws(' | ', s.comments, e.comments, r.comments) as observation,
    greatest(s.obm_access, e.obm_access, r.obm_access) as access,
    e.site_width_max,
    s.end_geometry,
    e.lbv_bushes,
    e.rbv_bushes,
    r.info_source,
    null as other_species,
    sq.altitude,
    s.water_type,
    s.survey_station_code,
    null as program,
    vf3.word as species_hu,
    vf5.word as species_ro,
    vf4.word as species_en,
    vf2.word as species_sci,
    null as obm_observation_list_id,
    r.exact_time,
    s.survey_station_code_deprecated,
    null as method,
    s.obm_id as site_id,
    e.obm_id as event_id,
    sq.sci,
    sq.ccm2,
    sq.country,
    sq.judet,
    sq.comuna,
    sq.corine_code
-- COLUMNS FROM THE MAIN TABLE
-- COLUMNS FROM THE JOINED TABLE
-- MAIN TABLE REFERENCE
    FROM public.pisces_survey_records r
    INNER JOIN pisces_survey_events e ON r.event_id = e.obm_id
    INNER JOIN pisces_survey_sites s ON e.site_id = s.obm_id
    LEFT JOIN pisces_survey_sites_qgrids sq ON s.obm_id = sq.row_id
    LEFT JOIN pisces_taxon vf1 ON r.species::text = vf1.word::text
    LEFT JOIN pisces.pisces_taxon_valid vf2 ON vf1.taxon_id = vf2.taxon_id
    LEFT JOIN pisces.pisces_taxon_magyar vf3 ON vf1.taxon_id = vf3.taxon_id
    LEFT JOIN pisces.pisces_taxon_angol vf4 ON vf1.taxon_id = vf4.taxon_id
    LEFT JOIN pisces.pisces_taxon_roman vf5 ON vf1.taxon_id = vf5.taxon_id;
-- JOIN STATEMENT
-- LEFT JOIN X vf ON (d.X = vf.X);

ALTER VIEW public.pisces OWNER TO pisces_admin;



-- view pisces_roadkill depends on view pisces
-- view pisces_roadkill_qgrids depends on view pisces_roadkill
-- materialized view pisces.capreolus_user_species_list_species_sci depends on view pisces
-- materialized view pisces.capreolus_project_species_list_species_sci depends on view pisces
-- materialized view pisces.capreolus_user_species_list_species_ro depends on view pisces
-- materialized view pisces.capreolus_project_species_list_species_ro depends on view pisces
-- materialized view pisces.capreolus_user_species_list_species_hu depends on view pisces
-- materialized view pisces.capreolus_project_species_list_species_hu depends on view pisces
-- materialized view pisces.capreolus_user_species_list_species_en depends on view pisces
-- materialized view pisces.capreolus_project_species_list_species_en depends on view pisces


