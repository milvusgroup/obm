
DROP MATERIALIZED VIEW pisces.pisces_taxon_angol;
CREATE MATERIALIZED VIEW pisces.pisces_taxon_angol AS
SELECT DISTINCT ON (pisces_taxon.taxon_id) pisces_taxon.taxon_id,
    pisces_taxon.word,
    pisces_taxon.status
   FROM pisces_taxon
  WHERE pisces_taxon.lang = 'species_en' AND pisces_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON pisces.pisces_taxon_angol (taxon_id);

GRANT SELECT ON pisces.pisces_taxon_angol TO public; 
ALTER MATERIALIZED VIEW pisces.pisces_taxon_angol OWNER TO pisces_admin;
