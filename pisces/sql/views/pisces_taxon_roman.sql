
DROP MATERIALIZED VIEW pisces.pisces_taxon_roman;
CREATE MATERIALIZED VIEW pisces.pisces_taxon_roman AS
SELECT DISTINCT ON (pisces_taxon.taxon_id) pisces_taxon.taxon_id,
    pisces_taxon.word,
    pisces_taxon.status
   FROM pisces_taxon
  WHERE pisces_taxon.lang = 'species_ro' AND pisces_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON pisces.pisces_taxon_roman (taxon_id);

GRANT SELECT ON pisces.pisces_taxon_roman TO public; 
ALTER MATERIALIZED VIEW pisces.pisces_taxon_roman OWNER TO pisces_admin;
