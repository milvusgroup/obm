DROP MATERIALIZED VIEW pisces.pisces_taxon_valid;
CREATE MATERIALIZED VIEW pisces.pisces_taxon_valid AS
SELECT DISTINCT ON (pisces_taxon.taxon_id) pisces_taxon.taxon_id,
    pisces_taxon.word,
    pisces_taxon.status
   FROM pisces_taxon
  WHERE pisces_taxon.lang = 'species_sci' AND pisces_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON pisces.pisces_taxon_valid (taxon_id);

GRANT SELECT ON pisces.pisces_taxon_valid TO public; 
ALTER MATERIALIZED VIEW pisces.pisces_taxon_valid OWNER TO pisces_admin;
