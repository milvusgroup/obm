
DROP MATERIALIZED VIEW pisces.pisces_taxon_magyar;
CREATE MATERIALIZED VIEW pisces.pisces_taxon_magyar AS
SELECT DISTINCT ON (pisces_taxon.taxon_id) pisces_taxon.taxon_id,
    pisces_taxon.word,
    pisces_taxon.status
   FROM pisces_taxon
  WHERE pisces_taxon.lang = 'species_hu' AND pisces_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON pisces.pisces_taxon_magyar (taxon_id);

GRANT SELECT ON pisces.pisces_taxon_magyar TO public; 
ALTER MATERIALIZED VIEW pisces.pisces_taxon_magyar OWNER TO pisces_admin;
