/*
Created on : Jul 4, 2017, 12:43:10 AM
Author     : Atta-Ur-Rehman Shah (http://attacomsian.com)
*/

/*
$(document).ready (function () {
    return;
  var map = new L.Map('mures-map', {
    trackResize: true,
  });

  const common_options = {
    map: 'PMAP',
    untiled: false,
    isBaseLayer: 'false',
    format: 'image/png',
    visibility: 'true',
    opacity: 1.0,
    transparent: true,
    numzoomlevels: 20,
    maxZoom: 18,
    minZoom: 6,
  }

  const wms_url = "http://milvus.openbiomaps.org/projects/mures/private/proxy.php";

  var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
  });


  var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
    foo: 'bar',
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
  }).addTo(map);

  map.setView([46.7055625, 24.4305467], 10)

  // MASURI DE MANAGEMENT
  $('#mm-results').accordion({
    collapsible: true,
    active: false,
    heightStyle: 'content',
  });
  let mmSource = L.WMS.Source.extend({
    'showFeatureInfo': function(latlng, info) {
      const ul = $('#mm-results');
      ul.html('');
      ul.accordion();
      let mm;
      if (info.length > 2) {
        try {
          mm = JSON.parse(info);
        }
        catch (err) {
          mm = [ { "id": "", "properties": { "specia": "", "masuri_de_management": "Invalid string", "descriere": "Invalid string" } } ];
        }
      }
      else {
          mm = [ { "id": "", "properties": { "specia": "", "masuri_de_management": "Nu există nicio măsură de management sau restricție în acest loc!", "descriere": "" } } ];
      }
      $.each(mm, function(k,m) {
        ul.append('<h3>'+m.properties.masuri_de_management + '</h3>');
        ul.append('<div>'+m.properties.descriere + '</div>');
      });
      ul.accordion('refresh');
    }
  });

  let masuri_options = {
    attribution: 'Mures Măsuri de management',
    layers: "layer_masuri_de_management_1",
    identify: true,
    INFO_FORMAT: 'text/html',
    FEATURE_COUNT: 20,
  };

  let mdm = new mmSource(wms_url, Object.assign(common_options,masuri_options));
  mdm.addSubLayer('layer_masuri_de_management_1');
  mdm.addTo(map);

  
  // CATEGORII DE FOLOSINTA
  $('#cf-results').accordion({
    collapsible: true,
    active: false,
    heightStyle: 'content',
  });
  let cfSource = L.WMS.Source.extend({
    'showFeatureInfo': function(latlng, info) {
      const ul = $('#cf-results');
      ul.html('');
      ul.accordion();
      let cf;
      try {
        cf = JSON.parse(info);
      }
      catch (err) {
        cf = [ { "id": "", "properties": {"natura2000": "", "uat": "", "numarul_cadastral": "", "clc_l1": "", "clc_l2": "", "categoria_de_folosinta_oficiala": "", "categoria_de_folosinta_reala": "" } } ];
      }
      $.each(cf, function(k,m) {
        ul.append('<h3>'+m.properties.numarul_cadastral + '</h3>');
        let list = '<div><ul>';
        Object.keys(m.properties).forEach((k) => {
            list += '<li><strong>' + k + ':</strong> ' + m.properties[k] + '</li>'
        });
        list += '</div>'
        ul.append(list);
        
      });
      ul.accordion('refresh');
    }
  });
  
  let cf_options = {
  };

  let cf = new cfSource(wms_url, Object.assign(common_options,{
    attribution: 'Mures categorii de folosinta',
    layers: "layer_categ_de_folosinta",
    identify: true,
    INFO_FORMAT: 'text/html',
    FEATURE_COUNT: 20,
  }));
  cf.addSubLayer('layer_categ_de_folosinta');
  cf.addTo(map);

  // SCI
  let sci_options = {
    attribution: 'SCI',
    layers: "layer_data_sci",
    cname: "layer_data_sci",
    identify: true,
  };

  var sci = L.WMS.overlay(wms_url, Object.assign(common_options, sci_options));
  sci.addTo(map);

  var theMarker = {};

  var baseLayers = {
    "OpenStreetMap" : osm,
    "Esri WorldImagery" : Esri_WorldImagery
  }
  var layers = {
    'Natura2000': sci,
  }
  L.control.layers(baseLayers, layers).addTo(map);

  map.on('click',(e) => {
    if (theMarker != undefined) {
      map.removeLayer(theMarker);
    };
    theMarker = L.marker([
        e.latlng.lat,
        e.latlng.lng
    ]).addTo(map);
  })

  $('#mm-filter select').SumoSelect({
    okCancelInMulti: true,
    placeholder: 'Selectează măsurile sau restricțiile dorite!',
    search: true,
    searchText: 'Căutare ...'
  });

  let masuri_options2 = Object.assign(common_options, {
    attribution: 'Mures Măsuri de management',
    layers: "layer_masuri_de_management_2",
  });

  let mdm2 = new L.WMS.tileLayer(wms_url,masuri_options2).addTo(map);
  $('#mm-filter .SumoSelect .btnOk').on('click', function(ev) {
    let values = [];
    $('.SumoSelect :selected').each(function(k,opt) {
      values.push("'"+opt.value+"'");
    });
    if (values.length !== 0) {
      masuri_options2['filter'] = 'ARRAY['+values.join()+']';
      if (! map.hasLayer(mdm2)){
        mdm2.addTo(map);
      }
    }
    else {
      delete masuri_options2.filter;
      map.removeLayer(mdm2);
    }
    mdm2.setParams(masuri_options2, false);
  });
});
*/


$(document).ready(function () {
    let mData = {}
    if ($('#mures-app').length) {
        const loadData = (async () => {
            const resp = await $.getJSON('ajax_mures', { action: 'init' })
            if (resp.status != 'success') {
                throw new Error(resp.message);
            }
            mData = resp.data;
        })()
        
        loadData.then(() => {
            var l = new Vue({
                el: '#mures-app',
                data () {
                    return {
                        mData,
                        app: "distribution",
                        filter: undefined
                    }
                },
                template: `
                <div id='mures-app'>
                    <appSelector :mData="mData" :selectedApp="app" @select="onSelectApp"/>
                    <div class="pure-g">
                        <div class="pure-u-1-4">
                            <distributionFilters v-if="app === 'distribution'" :mData="mData" @filterChanged="onFilterChanged" />
                            <threatsFilters v-if="app === 'threats'" :mData="mData" @filterChanged="onFilterChanged" />
                            <managementFilters v-if="app === 'management'" :mData="mData" @filterChanged="onFilterChanged" />
                        </div>
                        <div class="pure-u-3-4">
                            <muresMap v-if="app" :filter="filter" />
                        </div>
                    </div>
                </div>
                `,
                methods: {
                    onSelectApp (app) {
                        this.app = app
                    },
                    onFilterChanged (filter) {
                        this.filter = filter
                    }
                }
            })
        }).catch(console.error)
    }
    
    Vue.component('appSelector', {
        props: {
            mData: Object,
            selectedApp: String,
        },
        template: `
        <div class="pure-menu pure-menu-horizontal">
            <ul class="pure-menu-list">
                <li v-for="(name, app) in mData.apps" :class="(app === selectedApp) ? 'pure-menu-item pure-menu-selected' : 'pure-menu-item '">
                    <a href="#" class="pure-menu-link" @click="$emit('select', app)">{{ name }}</a>
                </li>
            </ul>
        </div>
        `,
    })
    
    Vue.component('distributionFilters', {
        data () {
            return {
                site: undefined,
                species_group: undefined,
                species: undefined,
                habitat: undefined,
                
                species_list: {},
                habitat_list: []
            }
        },
        props: {
            mData: Object,
        },
        template: `
            <form class="pure-form pure-form-stacked">
                <fieldset>
                    <siteSelector v-model="site" :sites="mData.sites" />
                    <hr>
                    <label for="species_group">Alege grupul</label>
                    <select id="species_group" v-model="species_group">
                        <option selected disabled>Alege grupul</option>
                        <option v-for="species_group in Object.keys(species_list)" :key="species_group">{{ species_group }}</option>
                    </select>
                    <speciesSelector v-model="species" :species_list="species_list[species_group]" />
                    <hr>
                    <label for="habitat">Alege habitatul</label>
                    <select id="habitat" v-model="habitat" @change="onHabitatChange">
                        <option selected disabled>Alege habitatul</option>
                        <option v-for="habitat in habitat_list" :key="habitat.code" :value="habitat.code">{{ habitat.code + ' - ' + habitat.description }}</option>
                    </select>
                </fieldset>
            </form>
        `,
        watch: {
            async site () {
                try {
                    this.reset()
                    this.$emit('filterChanged', this.filter)
                    const resp = await $.getJSON('ajax_mures', {
                        action: "load_site_data",
                        app: 'distribution',
                        site: this.site
                    })
                    if (resp.status !== 'success') {
                        throw resp.message
                    }
                    this.species_list = resp.data.species_list
                    this.habitat_list = resp.data.habitat_list
                }
                catch (e) {
                    console.log(new Error(e));
                }
            },
            species () {
                this.$emit('filterChanged', this.filter)
            }
        },
        methods: {
            reset () {
                this.species_group = undefined
                this.species = undefined
                this.habitat = undefined
            },
            onHabitatChange () {
                this.$emit('filterChanged', this.filter)
            }
        },
        computed: {
            filter () {
                return {
                    app: "distribution",
                    site: this.mData.sites[this.site],
                    species: this.species,
                    habitat: this.habitat
                }
            }
        }
    })

    Vue.component('threatsFilters', {
        data () {
            return {
                site: undefined,
                species_group: undefined,
                species: undefined,
                impact_category: undefined,
                impact: undefined,
                
                species_list: {},
                impact_list: [],
                app: "threats"
            }
        },
        props: {
            mData: Object,
        },
        template: `
        <form class="pure-form pure-form-stacked">
            <fieldset>
                <siteSelector v-model="site" :sites="mData.sites" />
                <hr>
                <label for="species_group">Alege grupul</label>
                <select id="species_group" v-model="species_group">
                    <option selected disabled>Alege grupul</option>
                    <option v-for="species_group in Object.keys(species_list)" :key="species_group">{{ species_group }}</option>
                </select>
                <speciesSelector v-model="species" :species_list="species_list[species_group]" />
                <label for="impact_category">Categoria de impact</label>
                <select id="impact_category" v-model="impact_category">
                    <option selected disabled>Categoria de impact</option>
                    <option v-for="category in Object.keys(impact_list)" :key="category">{{ category }}</options>
                </select>
                <label for="impact">Impact</label>
                <select id="impact" v-model="impact">
                    <option selected disabled>Impact</option>
                    <option v-for="impact in impact_list[impact_category]" :key="impact">{{ impact }}</options>
                </select>
                <hr>
                <label> Legenda intesitate impact </label>
                <p><span class="impact-legend impact-legend-s">scazuta</span>
                <span class="impact-legend impact-legend-m">medie</span>
                <span class="impact-legend impact-legend-r">ridicata</span> </p>
            </fieldset>
        </form>
        `,
        watch: {
            async site () {
                try {
                    this.reset('all')
                    this.$emit('filterChanged', this.filter)
                    const resp = await $.getJSON('ajax_mures', {
                        action: "load_site_data",
                        app: this.app,
                        site: this.site
                    })
                    if (resp.status !== 'success') {
                        throw resp.message
                    }
                    this.species_list = resp.data.species_list
                }
                catch (e) {
                    console.log(new Error(e));
                }
            },
            async species () {
                try {
                    this.reset()
                    const resp = await $.getJSON('ajax_mures', {
                        action: "load_impacts",
                        app: this.app,
                        site: this.site,
                        species: this.species
                    })
                    if (resp.status !== 'success') {
                        throw resp.message
                    }
                    this.impact_list = resp.data
                    
                    this.$emit('filterChanged', this.filter)
                } catch (e) {
                    console.log(new Error(e));
                }
            },
            impact () {
                this.$emit('filterChanged', this.filter)
            }
        },
        methods: {
            reset (what) {
                if (what === 'all') {
                    this.species_group = undefined
                    this.species = undefined
                }
                this.impact_category = undefined
                this.impact = undefined
            },
        },
        computed: {
            filter () {
                return {
                    app: this.app,
                    site: this.mData.sites[this.site],
                    species: this.species,
                    impact_category: this.impact_category,
                    impact: this.impact
                }
            }
        }
    })
    
    Vue.component('managementFilters', {
        data () {
            return {
                site: undefined,
                species_group: undefined,
                species: undefined,
                
                measure: undefined,
                
                species_list: {},
                measure_list: [],
                app: 'management'
            }
        },
        props: {
            mData: Object,
        },
        template: `
        <form class="pure-form pure-form-stacked">
            <fieldset>
                <siteSelector v-model="site" :sites="mData.sites" />
                <hr>
                <label for="species_group">Alege grupul</label>
                <select id="species_group" v-model="species_group">
                    <option selected disabled>Alege grupul</option>
                    <option v-for="species_group in Object.keys(species_list)" :key="species_group">{{ species_group }}</option>
                </select>
                <speciesSelector v-model="species" :species_list="species_list[species_group]" />
                <label for="measure">Măsura de management</label>
                <select id="measure" v-model="measure">
                    <option selected disabled>Măsura de management</option>
                    <option v-for="measure in measure_list" :key="measure">{{ measure }}</options>
                </select>
            </fieldset>
        </form>
        `,
        watch: {
            async site () {
                try {
                    this.reset('all')
                    this.$emit('filterChanged', this.filter)
                    const resp = await $.getJSON('ajax_mures', {
                        action: "load_site_data",
                        app: this.app,
                        site: this.site
                    })
                    if (resp.status !== 'success') {
                        throw resp.message
                    }
                    this.species_list = resp.data.species_list
                }
                catch (e) {
                    console.log(new Error(e));
                }
            },
            async species () {
                try {
                    this.reset()
                    const resp = await $.getJSON('ajax_mures', {
                        action: "load_measures",
                        app: this.app,
                        site: this.site,
                        species: this.species
                    })
                    if (resp.status !== 'success') {
                        throw resp.message
                    }
                    this.measure_list = resp.data
                    
                    this.$emit('filterChanged', this.filter)
                } catch (e) {
                    console.log(new Error(e));
                }
            },
            measure () {
                this.$emit('filterChanged', this.filter)
            }
        },
        methods: {
            reset (what) {
                if (what === 'all') {
                    this.species_group = undefined
                    this.species = undefined
                }
                this.measure = undefined
            },
        },
        computed: {
            filter () {
                return {
                    app: this.app,
                    site: this.mData.sites[this.site],
                    species: this.species,
                    measure: this.measure
                }
            }
        }
    })
    
    Vue.component('siteSelector', {
        props: {
            sites: Object,
        },
        data () {
            return {
                site: null
            }
        },
        template: `
        <div>
            <label for="site">Alege situl Natura 2000</label>
            <select id="site" v-model="site" @change="onSiteChange">
                <option selected disabled>Alege situl Natura 2000</option>
                <option v-for="site in sites" :key="site.id" :value='site.id' >{{ site.name }}</option>
            </select>
        </div>
        `,
        methods: {
            onSiteChange() {
                this.$emit('input', this.site)
            }
        }
    })
    
    Vue.component('speciesSelector', {
        props: {
            species_list: Object,
        },
        data () {
            return {
                species: null
            }
        },
        template: `
        <div>
            <label for="species">Alege specia</label>
            <select id="species" v-model="species" @change="onSpeciesChange">
                <option selected disabled>Alege specia</option>
                <option v-for="species in species_list" :key="species">{{ species }}</option>
            </select>
        </div>
        `,
        methods: {
            onSpeciesChange() {
                this.$emit('input', this.species)
            }
        }
    })
    
    Vue.component('muresMap', {
        data () {
            return {
                map: {},
                common_options: {
                    map: 'PMAP', untiled: false, isBaseLayer: 'false', format: 'image/png', visibility: 'true', transparent: true, numzoomlevels: 20, maxZoom: 18, minZoom: 6,
                },
                sci_options: {
                    attribution: 'SCI', layers: "layer_data_sci", cname: "layer_data_sci", identify: true, opacity: 1.0
                },
                point_options: {
                    attribution: 'Data points', layers: "layer_data_points", cname: "layer_data_points", identify: true, opacity: 1.0, 
                },
                polygon_options: {
                    attribution: 'Data polygons', identify: true, opacity: 0.5, 
                },
                wms_url: "http://milvus.openbiomaps.org/projects/mures/private/proxy.php",
                custom_wms_url: "http://milvus.openbiomaps.org/projects/mures/private/proxy_mures.php",
            }
        },
        watch: {
            filter () {
                const siteid = this.filter.site.id
                this.map.setView([this.filter.site.y, this.filter.site.x], this.filter.site.zoom)
                this.polygon_options.site = siteid
                if (this.filter.app === "distribution") {
                    this.point_options.site = siteid
                    this.point_options.species = this.filter.species
                    this.point_layer.setParams(Object.assign(this.common_options, this.point_options))
                    
                    this.polygon_options.layers = "layer_data_polygons"
                    this.polygon_options.cname = "layer_data_polygons"
                    this.polygon_options.habitat = this.filter.habitat
                    this.polygon_layer.setParams(Object.assign(this.common_options, this.polygon_options))
                }
                else if (this.filter.app === "threats") {
                    // remove point distribution layers
                    this.point_options.species = undefined
                    this.point_layer.setParams(Object.assign(this.common_options, this.point_options))
                    
                    this.polygon_options.layers = "layer_data_impacts"
                    this.polygon_options.cname = "layer_data_impacts"
                    this.polygon_options.species = this.filter.species
                    this.polygon_options.impact_category = this.filter.impact_category
                    this.polygon_options.impact = this.filter.impact
                    this.polygon_layer.setParams(Object.assign(this.common_options, this.polygon_options))
                }
                else if (this.filter.app === "management") {
                    // remove point distribution layers
                    this.point_options.species = undefined
                    this.point_layer.setParams(Object.assign(this.common_options, this.point_options))
                    
                    this.polygon_options.layers = "layer_data_measures"
                    this.polygon_options.cname = "layer_data_measures"
                    this.polygon_options.species = this.filter.species
                    this.polygon_options.measure = this.filter.measure
                    this.polygon_layer.setParams(Object.assign(this.common_options, this.polygon_options))
                }
            }
        },
        props: {
            filter: Object
        },
        computed: {
            point_layer () {
                return L.WMS.overlay(this.custom_wms_url, Object.assign(this.common_options, this.point_options))
            },
            polygon_layer () {
                return L.WMS.overlay(this.custom_wms_url, Object.assign(this.common_options, this.polygon_options))
            }
        },
        template: `<div id='mures-map'></div>`,
        mounted () {
            this.map = new L.Map('mures-map', {
                trackResize: true,
            });
            var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
            }).addTo(this.map);
            
            var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
                foo: 'bar',
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
            });
            
            var sci = L.WMS.overlay(this.wms_url, Object.assign(this.common_options, this.sci_options));
            
            sci.addTo(this.map);
            this.point_layer.addTo(this.map);
            this.polygon_layer.addTo(this.map);
            
            var baseLayers = {
                "Esri WorldImagery" : Esri_WorldImagery,
                "OpenStreetMap" : osm,
            }
            var layers = {
                'Natura2000': sci,
                'Distributie': this.point_layer,
                'Habitate': this.polygon_layer
            }
            L.control.layers(baseLayers, layers).addTo(this.map);
            
            this.map.setView([46.7055625, 24.4305467], 10)
        }
    })
})
