<?php
function get_masuri() {
    global $ID;

    $cmd = 'SELECT DISTINCT masuri_de_management as m FROM mures_masuri_management';
    if (!$res = pg_query($ID,$cmd)) {
        log_action('get_masuri query error');
        return false;
    }

    $rows = pg_fetch_all($res);

    return $rows;
}
?>
