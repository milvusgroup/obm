<?php
class mgrid {
    // `elements`.php should be exists in local includes dir:
    // e.g. includes/sidebar1.php ...
    var $elements = array('mures-map');
    
    // default layout - will be overdefined width grids
    var $definition = '
    <div class="wrapper">
      <div class="box mures-map">#mures-map</div>
    </div>';
 
   // minimal width layout
   var $gap = '1em';
   var $gap600 = '1em';
   var $gap900 = '1em';
   var $gap1400 = '1em';
   
   var $columns600 = 'auto';
   var $columns900 = 'auto';
   var $columns1400 = 'auto';
   
   var $areas = 'mures-map';
   var $areas600 = 'mures-map';
   var $areas900 = 'mures-map';
   var $areas1400 = 'mures-map'; 

}
?>

