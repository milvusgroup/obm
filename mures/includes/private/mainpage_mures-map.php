<?php
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
?>
<link rel="stylesheet" type="text/css" href="<?= $protocol ?>://<?= URL ?>/vendor/leaflet/leaflet.css?rev=<?= rev('vendor/leaflet/leaflet.css') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?= $protocol ?>://<?= URL ?>/css/sumoselect.min.css?rev=<?= rev('sumoselect.min.css') ?>"></script>
<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/vendor/leaflet/leaflet.js?rev=<?= rev('vendor/leaflet/leaflet.js') ?>"></script>
<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/vendor/leaflet/leaflet.wms.js?rev=<?= rev('vendor/leaflet/leaflet.wms.js') ?>"></script>
<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/js/jquery.sumoselect.min.js?rev=<?= rev('js/jquery.sumoselect.min.js') ?>"></script>
<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/js/private/scripts.js?rev=<?= rev('js/private/scripts.js') ?>"></script>

<div id="mures-app"></div>
