<?php 
session_start();
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'languages.php');

if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
    if (method_exists('AF', $action)) {
        echo AF::$action($_REQUEST);
        exit;
    }
}
else {
    echo 'Bad request';
    exit;
}

/**
 * 
 */
class AF {
    static function init() {
        
        return common_message('ok', [
            'apps' => [
                'distribution' => 'Distribuție',
                'threats' => 'Presiuni și amenințări',
                'management' => 'Măsuri de management'
            ],
            'sites' => self::load_sites(),
        ]);
    }
    
    private static function load_sites() {
        global $ID;
        $cmd = "SELECT 
                    localid as id, 
                    localid || ' ' || \"text\" as name, 
                    ST_X(ST_Centroid(ST_Envelope(ST_Transform(geometry, 4326)))) as x, 
                    ST_Y(ST_Centroid(ST_Envelope(ST_Transform(geometry, 4326)))) as y,
                    CASE localid 
                        WHEN 'ROSCI0210' THEN 14
                        WHEN 'ROSCI0369' THEN 14
                        WHEN 'ROSCI0367' THEN 13
                        WHEN 'ROSCI0368' THEN 12
                        ELSE 12
                    END as zoom
                FROM mures_situri_natura2000;";
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        $sites = [];
        while ($row = pg_fetch_assoc($res)) {
            $sites[$row['id']] = $row;
        }
        return $sites;
    }
    
    static function load_site_data($request) {
        $data = [
            'species_list' => self::load_species_list($request),
        ];
        
        if ($request['app'] === 'distribution') {
            $data['habitat_list'] = self::load_habitat_list($request);
        }
        
        return common_message('ok', $data);
    }
    
    private static function load_species_list($request) {
        global $ID;
        $sp_list = [];
        if ($request['app'] === 'distribution') {
            $table = 'mures';
            $column = 'specia';
        }
        elseif (in_array($request['app'], ['threats', 'management'])) {
            $table = 'mures_amenintari';
            $column = 'grup_tinta_specii';
        }
        $cmd = sprintf("SELECT DISTINCT 
                            %1\$s as specia, species_group 
                        FROM %2\$s m 
                        LEFT JOIN mures_taxon tx ON m.%1\$s = tx.word 
                        LEFT JOIN mures_taxonmeta tm ON tx.taxon_id = tm.taxon_id 
                        WHERE 
                            %1\$s IS NOT NULL AND 
                            %1\$s NOT IN ('null', '-') AND 
                            site = %3\$s 
                        ORDER BY species_group, %1\$s;",
            $column, $table, quote($request['site'])
        );
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return [];
        }
        $species_list = [];
        while ($row = pg_fetch_assoc($res)) {
            if (!isset($species_list[$row['species_group']])) {
                $species_list[$row['species_group']] = [];
            }
            $species_list[$row['species_group']][] = $row['specia'];
        }
        return $species_list;
    }
    
    private static function load_habitat_list($request) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT n2000 as code, description FROM mures_habitat_rezultate h LEFT JOIN mures_habitate_n2000 ON code LIKE n2000 || '%%' WHERE n2000 IS NOT NULL AND site = %s ORDER BY n2000;", quote($request['site']));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return [];
        }
        $habitat_list = (pg_num_rows($res) === 0) ? [] : pg_fetch_all($res);
        return $habitat_list;
    }
    
    static function load_impacts($request) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT categorie_impact, impact_general as impact FROM mures_amenintari WHERE categorie_impact in ('amenintare', 'presiune') AND impact_general IS NOT NULL AND site = %s AND grup_tinta_specii = %s ORDER BY impact;", quote($request['site']), quote($request['species']));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return common_message('error', 'query error');
        }
        $impact_list = [];
        while ($row = pg_fetch_assoc($res)) {
            $cat = $row['categorie_impact'];
            if (!isset($impact_list[$cat])) {
                $impact_list[$cat] = [];
            }
            $impact_list[$cat][] = $row['impact'];
        }
        return common_message('ok', $impact_list);
    }
    
    static function load_measures($request) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT masura_de_management as measure FROM mures_amenintari WHERE masura_de_management IS NOT NULL AND site = %s AND grup_tinta_specii = %s ORDER BY measure;", quote($request['site']), quote($request['species']));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return common_message('error', 'query error');
        }
        $measure_list = [];
        while ($row = pg_fetch_assoc($res)) {
            $measure_list[] = $row['measure'];
        }
        return common_message('ok', $measure_list);
        
    }
}

?>