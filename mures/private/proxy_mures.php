<?php
require_once(getenv('OB_LIB_DIR').'modules_class.php');

session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

$_SESSION['current_query_table'] = 'mures';

// If login session expired
if (!isset($_SESSION['token'])) {
    require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    st_col('default_table');
}
# project switch check
if (isset($_SESSION['current_query_table'])) {
    if (!preg_match('/^'.PROJECTTABLE.'/',$_SESSION['current_query_table'])) {
        require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
    }
}

$MAP = realpath('./private.map');

// custom mapfile
$p_p = pathinfo(PRIVATE_MAPFILE);
if ($p_p['extension']=='map' and $p_p['filename']=='private') {
    if ($p_p['dirname'] != '.') {
        $MAP = $p_p['dirname'].'/'.$p_p['basename'];
    } else {
        $MAP = realpath( './'.PRIVATE_MAPFILE );
    }
}


$MAPSERVER = (defined('MAPSERVER')) ? MAPSERVER."?" : "http://localhost/cgi-bin/mapserv?";

$referer = '';

if ($MAP === '') {
    print "No map defined.";
    exit;
}

$extent = '';

//will be processed in .htaccess
//maybe it is used only in public access: to change the real path to dummy path
$map = "&ACCESS=$MAP";

//default query_string - not used the default any more!
//$url = $_SERVER['QUERY_STRING'].$map;

// parse QUERY_STRING
parse_str($_SERVER['QUERY_STRING'], $HttpQuery);

if (isset($HttpQuery['layers'])) {
    $HttpQuery['LAYERS'] = $HttpQuery['layers'];
    $HttpQuery['BBOX'] = $HttpQuery['bbox'];
    $HttpQuery['CNAME'] = $HttpQuery['cname'];
}

if ($HttpQuery['layers'] === 'layer_data_points') {
    $table = 'mures';

    $wh = [
        "specia NOT IN ('null', '-')",
        "GeometryType(obm_geometry) = 'POINT'"
    ];
    if (isset($HttpQuery['site']) && isset($HttpQuery['species'])) {
        $wh[] = sprintf('site = %s', quote($HttpQuery['site']));
        $wh[] = sprintf('specia = %s', quote($HttpQuery['species']));
    }
    else {
        $wh[] = "1 = 0";
    }
    $query = "SELECT obm_id, obm_geometry , 0 AS selected FROM $table WHERE ";
}
elseif ($HttpQuery['layers'] === 'layer_data_polygons') {
    $table = "mures_habitat_rezultate";
    $wh = [
        "n2000 IS NOT NULL",
        "GeometryType(obm_geometry) = 'POLYGON'"
    ];
    if (isset($HttpQuery['site']) && isset($HttpQuery['habitat'])) {
        $wh[] = sprintf('site = %s', quote($HttpQuery['site']));
        $wh[] = sprintf('n2000 = %s', quote($HttpQuery['habitat']));
    }
    else {
        $wh[] = "1 = 0";
    }
    $query = "SELECT obm_id, obm_geometry , 0 AS selected FROM $table WHERE ";
}
elseif ($HttpQuery['layers'] === 'layer_data_impacts') {
    $table = "mures_amenintari";
    $wh = [
        "GeometryType(obm_geometry) = 'POLYGON'"
    ];
    
    if (isset($HttpQuery['site']) && isset($HttpQuery['species']) && isset($HttpQuery['impact_category']) && isset($HttpQuery['impact'])) {
        $wh[] = sprintf('site = %s', quote($HttpQuery['site']));
        $wh[] = sprintf('grup_tinta_specii = %s', quote($HttpQuery['species']));
        $wh[] = sprintf('categorie_impact = %s', quote($HttpQuery['impact_category']));
        $wh[] = sprintf('impact_general = %s', quote($HttpQuery['impact']));
    }
    else {
        $wh[] = "1 = 0";
    }
    $query = "SELECT 
                obm_id, 
                obm_geometry, 
                CASE intensitate_presiune 
                    WHEN 'S – scazuta' THEN 1
                    WHEN 'M – medie' THEN 2
                    WHEN 'R – ridicata' THEN 3
                    ELSE 0
                END AS intensity 
            FROM $table WHERE ";
}
elseif ($HttpQuery['layers'] === 'layer_data_measures') {
    $table = "mures_amenintari";
    $wh = [
        "GeometryType(obm_geometry) = 'POLYGON'"
    ];
    
    if (isset($HttpQuery['site']) && isset($HttpQuery['species']) && isset($HttpQuery['measure'])) {
        $wh[] = sprintf('site = %s', quote($HttpQuery['site']));
        $wh[] = sprintf('grup_tinta_specii = %s', quote($HttpQuery['species']));
        $wh[] = sprintf('masura_de_management = %s', quote($HttpQuery['measure']));
    }
    else {
        $wh[] = "1 = 0";
    }
    $query = "SELECT obm_id, obm_geometry FROM $table WHERE ";
}

//push query and assemble the new url
$HttpQuery['query'] = $query . implode(' AND ', $wh);
$mapserver_get_string = http_build_query($HttpQuery);
$mapserver_get_string = preg_replace('/TILE=X%2BY%2BZ/','TILE=X+Y+Z',$mapserver_get_string);

# Assemble the final mapserver GET url with forcing real BBOX
$url = $mapserver_get_string.$extent.$map;

/* repeat  - shall we use it? */ 
if (isset($HttpQuery['repeat'])) {
    $_SESSION['wfs_rows_counter']=$HttpQuery['repeat'];
    //pages, requets from maps.js
    if(isset($_SESSION['repeat_url'])) {
        $url=$_SESSION['repeat_url'];
    } else {
        exit;
    }
}
$_SESSION['repeat_url'] = $url;

// Close session write
session_write_close();

/* we don't print the output xml if has refered from the web apps... */
if (isset($_SERVER['HTTP_REFERER']))
$referer = implode("/",array_slice(preg_split("/\//",$_SERVER['HTTP_REFERER']), 2,-1));

/* show xml response in local query
* This called from the maps.js->#customLayer".click function
* when somebody load a custom shp file as a new Layer */
if (isset($HttpQuery['showXML'])) {
    $referer = ''; 
}


// POST not works, POST-XML?
$fp = fopen($MAPSERVER.$url, 'r', false);

if (is_resource($fp)) {
    
    if (isset($HttpQuery['FORMAT'])) $format = $HttpQuery['FORMAT'];
    else $format = 'image/png';
    header("Content-type: $format");
    echo stream_get_contents($fp);
    //echo $content;
} else {
    log_action('Mapserver does not available',__FILE__,__LINE__);
}
exit;


?>
