// mapserver template
[resultset layer=layer_masuri_de_management_1]
[
    [feature trimlast=","]
      {
    "id": "[obm_id]",
    "properties": {
        "specia": "[nume_spec]",
        "masuri_de_management": "[masuri_de_management]",
        "descriere": "[descriere]"
    }
      },
    [/feature]
]
[/resultset]
[resultset layer=layer_categ_de_folosinta]
[
    [feature trimlast=","]
      {
    "id": "[obm_id]",
    "properties": {
        "natura2000": "[natura2000]",
        "uat": "[uat]",
        "numarul_cadastral": "[numarul_cadastral]",
        "clc_l1": "[clc_l1]",
        "clc_l2": "[clc_l2]",
        "categoria_de_folosinta_oficiala": "[categoria_de_folosinta_oficiala]",
        "categoria_de_folosinta_reala": "[categoria_de_folosinta_reala]"
    }
      },
    [/feature]
]
[/resultset]
