CREATE TRIGGER "mures_taxon_meta_fill" BEFORE INSERT OR UPDATE ON "mures_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "mures_taxon_meta_fill" BEFORE INSERT OR UPDATE ON "mures_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "mures_amenintari_taxon_update" BEFORE INSERT OR UPDATE ON "mures_amenintari" FOR EACH ROW EXECUTE PROCEDURE update_mures_amenintari_taxonlist();
