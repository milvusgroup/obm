ALTER TABLE mures_nevertebrate ADD COLUMN azonositatlan_nem integer NULL;
ALTER TABLE mures_nevertebrate ADD COLUMN acoperire_sol_nud integer NULL;
ALTER TABLE mures_nevertebrate ADD COLUMN intensitate_insolatie integer NULL;
ALTER TABLE mures_nevertebrate ADD COLUMN meres_ideje time without time zone  NULL;
ALTER TABLE mures_nevertebrate ADD COLUMN holdfazis character varying (16) NULL;  
ALTER TABLE mures_nevertebrate ADD COLUMN gyepboritas integer NULL;
ALTER TABLE mures_nevertebrate ADD COLUMN gyepmagassag integer NULL;
ALTER TABLE mures_nevertebrate ADD COLUMN oua integer NULL;
