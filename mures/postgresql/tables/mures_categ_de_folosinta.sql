ALTER TABLE tmp.mures_tehasz ADD COLUMN "data" date NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "specia" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "numar_exemplar" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "unitate" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "site" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "punct_gps" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "observator" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "observatii" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "x" numeric NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "y" numeric NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "obm_files_id" varchar NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "obm_access" integer NULL;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "id" character varying(12);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "natura2000" character varying(12);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "uat" character varying(29);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "localizare" character varying(59);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "numarul_cadastral" character varying(45);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "suprafata_totala_surse_oficial" smallint;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "suprafata_totala_desen" smallint;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "data_sursei_informatie" character varying(10);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "clc_l1" character varying(32);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "clc_l2" character varying(45);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "categoria_de_folosinta_oficiala" character varying(30);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "categoria_de_folosinta_reala" character varying(24);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "forma_de_proprietate" character varying(31);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "intocmit_de" character varying(22);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "prop_link" character varying(43);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "nume_proprietari" character varying(254);
ALTER TABLE tmp.mures_tehasz ADD COLUMN "nr_titlu_de_proprietate" smallint;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "nr_cf" smallint;
ALTER TABLE tmp.mures_tehasz ADD COLUMN "foto" character varying(42);

ALTER TABLE tmp.mures_tehasz ALTER COLUMN "nr_cf" TYPE integer;
ALTER TABLE tmp.mures_tehasz ALTER COLUMN "nr_titlu_de_proprietate" TYPE integer;
ALTER TABLE tmp.mures_tehasz ALTER COLUMN "suprafata_totala_surse_oficial" TYPE integer;
ALTER TABLE tmp.mures_tehasz ALTER COLUMN "suprafata_totala_desen" TYPE integer;

\copy mures_categ_de_folosinta ( id,natura2000,uat,localizare,numarul_cadastral,suprafata_totala_surse_oficial,suprafata_totala_desen,data_sursei_informatie,clc_l1,clc_l2,categoria_de_folosinta_oficiala,categoria_de_folosinta_reala,forma_de_proprietate,intocmit_de,prop_link,nume_proprietari,observatii,nr_titlu_de_proprietate,nr_cf,foto ) FROM '~/Documents/milvus/maros_POIM/terulethasznalat/tmp/osszes.csv' CSV HEADER;

UPDATE tmp.mures_tehasz t SET id = cf.id FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET natura2000 = cf.natura2000 FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET uat = cf.uat FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET localizare = cf.localizare FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET numarul_cadastral = cf.numarul_cadastral FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET suprafata_totala_surse_oficial = cf.suprafata_totala_surse_oficial FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET suprafata_totala_desen = cf.suprafata_totala_desen FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET data_sursei_informatie = cf.data_sursei_informatie FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET clc_l1 = cf.clc_l1 FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET clc_l2 = cf.clc_l2 FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET categoria_de_folosinta_oficiala = cf.categoria_de_folosinta_oficiala FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET categoria_de_folosinta_reala = cf.categoria_de_folosinta_reala FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET forma_de_proprietate = cf.forma_de_proprietate FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET intocmit_de = cf.intocmit_de FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET prop_link = cf.prop_link FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET nume_proprietari = cf.nume_proprietari FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET observatii = cf.observatii FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET nr_titlu_de_proprietate = cf.nr_titlu_de_proprietate FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET nr_cf = cf.nr_cf FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;
UPDATE tmp.mures_tehasz t SET foto = cf.foto FROM mures_categ_de_folosinta cf WHERE t.id = cf.id;

truncate mures_categ_de_folosinta;

INSERT INTO mures_categ_de_folosinta (
    obm_geometry,id,natura2000,uat,localizare,numarul_cadastral,suprafata_totala_surse_oficial,suprafata_totala_desen,data_sursei_informatie,clc_l1,clc_l2,categoria_de_folosinta_oficiala,categoria_de_folosinta_reala,forma_de_proprietate,intocmit_de,prop_link,nume_proprietari,observatii,nr_titlu_de_proprietate,nr_cf,foto
)
SELECT geometry,id,natura2000,uat,localizare,numarul_cadastral,suprafata_totala_surse_oficial,suprafata_totala_desen,data_sursei_informatie,clc_l1,clc_l2,categoria_de_folosinta_oficiala,categoria_de_folosinta_reala,forma_de_proprietate,intocmit_de,prop_link,nume_proprietari,observatii,nr_titlu_de_proprietate,nr_cf,foto
FROM tmp.mures_tehasz;
