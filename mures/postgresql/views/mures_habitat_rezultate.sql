DROP VIEW mures_habitat_rezultate;
CREATE OR REPLACE view mures_habitat_rezultate AS
WITH 
    sps AS (
        SELECT 
            he.obm_id, he.obm_uploading_id, he.obm_validation, he.site, he.cod_poligon, he.obm_geometry, he.cod_habitat, he.n2000,
            he.relief, he.inclinatie, he.expozitie, he.utilizare_teren, he.adancimea_apa,
            he.viteza_apa, he.grad_de_naturalitate, he.acoperire_strat_arboricol,
            he.inaltime_medie_strat_arboricol, he.acoperire_strat_arbustiv, he.inaltime_medie_strat_arbustiv,
            he.acoperire_strat_ierbos, he.inaltime_medie_strat_ierbos, he.sol_nud_la_suprafata,
            he.subtip_habitat, he.suprafata_subtip, he.cod_habitat_ro,
            he.asoaciatii_vegetale, he.obm_modifier_id, he.obm_files_id, string_agg(h.specia, ', ') as specii
        FROM mures_habitat_esantion he 
        LEFT JOIN (SELECT DISTINCT cod_poligon, specia FROM mures_habitat) h ON h.cod_poligon = he.cod_poligon
        GROUP BY he.obm_geometry, he.site, he.cod_habitat, he.n2000, he.relief, he.inclinatie, he.expozitie,
            he.utilizare_teren, he.adancimea_apa, he.viteza_apa, he.grad_de_naturalitate,
            he.acoperire_strat_arboricol, he.inaltime_medie_strat_arboricol, he.acoperire_strat_arbustiv,
            he.inaltime_medie_strat_arbustiv, he.acoperire_strat_ierbos, he.inaltime_medie_strat_ierbos,
            he.sol_nud_la_suprafata, he.subtip_habitat, he.suprafata_subtip, he.cod_habitat_ro,
            he.asoaciatii_vegetale, he.obm_id, he.cod_poligon, he.obm_uploading_id,
            he.obm_validation, he.obm_modifier_id, he.obm_files_id
    ),
    ams AS (SELECT DISTINCT cod_poligon, impact_general FROM mures_amenintari WHERE cod_poligon IS NOT NULL)
SELECT sps.*, string_agg(ams.impact_general, ', ') as presiuni_amenintari
FROM sps
LEFT JOIN ams ON ams.cod_poligon = sps.cod_poligon
GROUP BY sps.obm_geometry, sps.site, sps.cod_habitat, sps.n2000, sps.relief, sps.inclinatie, sps.expozitie,
    sps.utilizare_teren, sps.adancimea_apa, sps.viteza_apa, sps.grad_de_naturalitate,
    sps.acoperire_strat_arboricol, sps.inaltime_medie_strat_arboricol, sps.acoperire_strat_arbustiv,
    sps.inaltime_medie_strat_arbustiv, sps.acoperire_strat_ierbos, sps.inaltime_medie_strat_ierbos,
    sps.sol_nud_la_suprafata, sps.subtip_habitat, sps.suprafata_subtip, sps.cod_habitat_ro,
    sps.asoaciatii_vegetale, sps.obm_id, sps.cod_poligon, sps.specii, sps.obm_uploading_id,
    sps.obm_validation, sps.obm_modifier_id, sps.obm_files_id;
    
GRANT ALL ON mures_habitat_rezultate TO mures_admin;

DELETE FROM mures_rules WHERE data_table = 'mures_habitat_rezultate';
INSERT INTO mures_rules (data_table, row_id, owner, read, write, sensitivity) select 'mures_habitat_rezultate', row_id, owner, read, write, sensitivity from mures_rules where data_table = 'mures_habitat_esantion';

