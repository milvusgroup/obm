CREATE OR REPLACE VIEW mures_rezultate AS
SELECT g.obm_id,
g.obm_geometry,
g.obm_uploading_id,
g.obm_validation,
g.obm_comments,
g.obm_modifier_id,
g.rid,
g.sitecode,
g.sitename,
g.cod,
g.nume_spec,
rez.area_ha,
rez.populatie,
rez.popmin,
rez.popmax,
rez.unit,
rez.quality,
rez.density,
rez.sensitive,
rez.cons_statu,
rez.cod_foto,
rez.managplan,
rez.obs,
string_agg(DISTINCT (((('<a href="http://milvus.openbiomaps.org/projects/mures/index.php?data&id='::text || imp.obm_id) || '&table=mures_distrib_amenintari" target="_blank">'::text) || (imp.cod_impact)::text) || '</a>'::text), ', '::text) AS impact,
string_agg(DISTINCT (((('<a href="http://milvus.openbiomaps.org/projects/mures/index.php?data&id='::text || mm.obm_id) || '&table=mures_masuri_management" target="_blank">'::text) || mm.masuri_de_management) || '</a>'::text), ', '::text) AS masuri_de_management
FROM (((mures_centralizator_geom g
LEFT JOIN mures_centralizator_rez rez ON ((((g.sitecode)::text = (rez.sitecode)::text) AND ((g.cod)::text = (rez.cod)::text))))
LEFT JOIN mures_distrib_amenintari imp ON ((((imp.sitecode)::text = (g.sitecode)::text) AND ((imp.cod)::text = (g.cod)::text))))
LEFT JOIN mures_masuri_management mm ON ((((mm.sitecode)::text = (g.sitecode)::text) AND ((mm.cod)::text = (g.cod)::text))))
GROUP BY g.obm_id, rez.obm_id;
