CREATE OR REPLACE FUNCTION "mures_habitat_fill_observator_data" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
  cp RECORD;
BEGIN
    IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') AND (NEW.cod_poligon IS NOT NULL) THEN
        SELECT data, observator INTO cp FROM mures_habitat_esantion WHERE cod_poligon = NEW.cod_poligon ORDER BY obm_id DESC LIMIT 1;
        NEW.data = cp.data;
        NEW.observator = cp.observator;
    END IF;
    
    RETURN NEW;
END;
$$;
