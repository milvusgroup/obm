SELECT word, species_group FROM mures_taxon tx LEFT JOIN mures_taxonmeta tm ON tx.taxon_id = tm.taxon_id WHERE species_group IS NOT NULL ORDER BY species_group;
UPDATE mures_taxonmeta SET species_group = 'plante' WHERE species_group = 'mures_habitat';
UPDATE mures_taxonmeta SET species_group = 'amfibieni și reptile' WHERE species_group = 'mures_herps';
UPDATE mures_taxonmeta SET species_group = 'moluște' WHERE species_group = 'mures_moluste';
UPDATE mures_taxonmeta SET species_group = 'nevertebrate' WHERE species_group = 'mures_nevertebrate';
UPDATE mures_taxonmeta SET species_group = 'pești' WHERE species_group = 'mures_pesti';
UPDATE mures_taxonmeta SET species_group = 'mamifere' WHERE species_group = 'mures_vidra';

SELECT tx.taxon_id, word, species_group FROM mures_taxon tx LEFT JOIN mures_taxonmeta tm ON tx.taxon_id = tm.taxon_id WHERE species_group IS NULL ORDER BY species_group;

DELETE FROM mures_taxon WHERE word = 'Neptis hylas';


-- ellenorzom, hogy minden megvan-e

SELECT 'total', count(*) FROM mures;

with asasd as (
SELECT 'mures_herps' as tip_data, count(*) FROM mures_herps
UNION
SELECT 'mures_moluste' as tip_data, count(*) FROM mures_moluste
UNION
SELECT 'mures_nevertebrate' as tip_data, count(*) FROM mures_nevertebrate
UNION
SELECT 'mures_pesti' as tip_data, count(*) FROM mures_pesti
UNION
SELECT 'mures_habitat' as tip_data, count(*) FROM mures_habitat
UNION
SELECT 'mures_vidra' as tip_data, count(*) FROM mures_vidra
)
SELECT * FROM asasd 
LEFT JOIN
    (select distinct tip_data, count(*) from mures group by tip_data) m
ON asasd.tip_data = m.tip_data;

COPY (
    SELECT min(orig_id) from mures m LEFT JOIN mures_habitat mh ON m.orig_id = mh.obm_id WHERE m.tip_data = 'mures_habitat' AND mh.obm_id IS NULL GROUP BY orig_id ORDER BY orig_id
) TO '/tmp/mures_habitat_pluszban van.csv' CSV HEADER;
BEGIN; 
DELETE FROM mures where tip_data = 'mures_habitat' AND orig_id BETWEEN 495 AND 1327;
COMMIT;

select distinct specia, species_group from mures m LEFT JOIN mures_taxon tx ON m.specia = tx.word LEFT JOIN mures_taxonmeta tm ON tx.taxon_id = tm.taxon_id WHERE specia NOT IN ('null', '-');

BEGIN;
WITH asd as (
select files.id FROM mures LEFT JOIN system.file_connect ON obm_files_id = conid LEFT JOIN system.files ON file_id = id where tip_data = 'torolni' AND obm_files_id IS NOT NULL
)
DELETE from system.files WHERE id in (select id from asd);
COMMIT;
BEGIN;
DELETE FROM mures where tip_data = 'torolni';
COMMIT;

select distinct site, count(*) from mures group by site;

BEGIN;
WITH g as (
SELECT DISTINCT mh.cod_poligon, e.obm_geometry from mures m LEFT JOIN mures_habitat mh ON m.orig_id = mh.obm_id LEFT JOIN mures_habitat_esantion e ON mh.cod_poligon = e.cod_poligon WHERE m.tip_data = 'mures_habitat'
)
UPDATE mures_habitat h SET obm_geometry = g.obm_geometry FROM g WHERE h.cod_poligon = g.cod_poligon;
COMMIT;

BEGIN; 
UPDATE mures m SET obm_geometry = h.obm_geometry FROM mures_habitat h WHERE m.tip_data = 'mures_habitat' AND m.orig_id = h.obm_id;
COMMIT;


-- habitat esantion javitasa
UPDATE mures_habitat_esantion SET site = SUBSTRING(site, 0, 10);
UPDATE mures_amenintari SET site = SUBSTRING(site, 0, 10) WHERE site LIKE 'RO%';
UPDATE mures_amenintari SET site = 'ROSCI0367' WHERE site = 'Râul Mureș între Morești și Ogra';

UPDATE mures_amenintari SET categorie_impact = 'amenintare' WHERE categorie_impact = 'amenințare';

