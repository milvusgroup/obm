-- https://redmine.milvus.ro/issues/1279
INSERT INTO mures_moluste ("obm_files_id", "site", "obm_geometry", "punct_gps", 
  "unitate", "obm_access", "x", "observator", "specia", "observatii", 
  "obm_uploading_id", "data", "numar_exemplar", "y")
SELECT "obm_files_id", "site", "obm_geometry", "punct_gps", "unitate", 
  "obm_access", "x", "observator", "specia", "observatii", "obm_uploading_id", 
  "data", "numar_exemplar", "y" 
FROM mures
WHERE specia = 'Unio crassus';

-- a trigger visszatette ugyanezeket az adatokat a főtáblába. Törlöm őket:
BEGIN;
DELETE FROM mures WHERE specia = 'Unio crassus' AND tip_data IS NULL;
ROLLBACK;

UPDATE system.uploadings SET project_table = 'mures_moluste' WHERE id IN (43660, 140260);

