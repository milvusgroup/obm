--BEGIN;
--WITH torolnivalo as (
--    WITH valami as (
--        WITH duplak as (
--            SELECT DISTINCT obm_geometry, data, ROW_NUMBER() OVER (ORDER BY data, obm_geometry) as rn FROM (
--                SELECT DISTINCT obm_geometry, data, count(*) c 
--                FROM mures_amenintari 
--                WHERE observator = 'Dénes Anna' 
--                GROUP BY obm_geometry, data
--            ) foo
--            WHERE foo.c > 1
--        )
--        SELECT u.uploading_date, obm_id, a.obm_geometry, a.data, rn, obm_files_id, conid, reference 
--        FROM mures_amenintari a
--        LEFT JOIN system.uploadings u ON a.obm_uploading_id = u.id
--        LEFT JOIN duplak ON a.obm_geometry = duplak.obm_geometry AND a.data = duplak.data
--        LEFT JOIN system.file_connect ON obm_files_id = conid
--        LEFT JOIN system.files ON file_id = files.id
--        WHERE duplak.rn IS NOT NULL
--        --GROUP BY obm_geometry, data, obm_files_id
--        ORDER BY a.data, a.obm_geometry, obm_files_id DESC
--    ),
--    nincs_kep as (SELECT * FROM valami WHERE conid IS NULL),
--    van_kep as (SELECT * FROM valami WHERE conid IS NOT NULL)
--
--    SELECT nincs_kep.obm_id
--    FROM nincs_kep
--    LEFT JOIN van_kep ON nincs_kep.rn = van_kep.rn
--    WHERE van_kep.rn IS NOT NULL
--)
--DELETE FROM mures_amenintari WHERE obm_id IN (SELECT obm_id FROM torolnivalo);
--COMMIT;

--BEGIN;
--WITH torolnivalo as (
--    WITH valami as (
--        WITH duplak as (
--            SELECT DISTINCT obm_geometry, data, ROW_NUMBER() OVER (ORDER BY data, obm_geometry) as rn FROM (
--                SELECT DISTINCT obm_geometry, data, count(*) c 
--                FROM mures_amenintari 
--                WHERE observator = 'Dénes Anna' 
--                GROUP BY obm_geometry, data
--            ) foo
--            WHERE foo.c > 1
--        )
--        SELECT u.uploading_date, obm_id, a.obm_geometry, a.data, rn, obm_files_id, conid, reference 
--        FROM mures_amenintari a
--        LEFT JOIN system.uploadings u ON a.obm_uploading_id = u.id
--        LEFT JOIN duplak ON a.obm_geometry = duplak.obm_geometry AND a.data = duplak.data
--        LEFT JOIN system.file_connect ON obm_files_id = conid
--        LEFT JOIN system.files ON file_id = files.id
--        WHERE duplak.rn IS NOT NULL
--        --GROUP BY obm_geometry, data, obm_files_id
--        ORDER BY a.data, a.obm_geometry, obm_files_id DESC
--    ),
--    nincs_kep AS (SELECT * FROM valami WHERE obm_files_id IS NULL),
--    van_kep AS (SELECT * FROM valami WHERE obm_files_id IS NOT NULL)
--    SELECT DISTINCT nincs_kep.obm_id
--    FROM nincs_kep
--    LEFT JOIN van_kep ON nincs_kep.rn = van_kep.rn
--    WHERE van_kep.rn IS NOT NULL
--)
--DELETE FROM mures_amenintari WHERE obm_id IN (SELECT obm_id FROM torolnivalo);
--COMMIT;

--BEGIN;
--WITH torolnivalo as (
--    WITH valami as (
--        WITH duplak as (
--            SELECT DISTINCT obm_geometry, data, ROW_NUMBER() OVER (ORDER BY data, obm_geometry) as rn FROM (
--                SELECT DISTINCT obm_geometry, data, count(*) c 
--                FROM mures_amenintari 
--                WHERE observator = 'Dénes Anna' 
--                GROUP BY obm_geometry, data
--            ) foo
--            WHERE foo.c > 1
--        )
--        SELECT u.uploading_date, obm_id, a.obm_geometry, a.data, rn, obm_files_id, conid, reference 
--        FROM mures_amenintari a
--        LEFT JOIN system.uploadings u ON a.obm_uploading_id = u.id
--        LEFT JOIN duplak ON a.obm_geometry = duplak.obm_geometry AND a.data = duplak.data
--        LEFT JOIN system.file_connect ON obm_files_id = conid
--        LEFT JOIN system.files ON file_id = files.id
--        WHERE duplak.rn IS NOT NULL
--        --GROUP BY obm_geometry, data, obm_files_id
--        ORDER BY a.data, a.obm_geometry, obm_files_id DESC
--    ),
--    max_dates as (SELECT rn, max(uploading_date) as uploading_date FROM valami GROUP BY rn)
--    SELECT obm_id FROM valami WHERE (rn, uploading_date) NOT IN (SELECT * FROM max_dates)
--)
--DELETE FROM mures_amenintari WHERE obm_id IN (SELECT obm_id FROM torolnivalo);
--COMMIT;
    
    
COPY (
WITH valami as (
    SELECT u.uploading_date, obm_id, st_astext(a.obm_geometry) as obm_geometry, a.data, obm_files_id, impact_specific, u.metadata
    FROM mures_amenintari a
    LEFT JOIN system.uploadings u ON a.obm_uploading_id = u.id
    LEFT JOIN system.file_connect ON obm_files_id = conid
    LEFT JOIN system.files ON file_id = files.id
    WHERE uploader_name = 'Dénes Anna' AND obm_files_id IS NOT NULL AND conid IS NULL
    --GROUP BY obm_geometry, data, obm_files_id
    ORDER BY a.data, a.obm_geometry, obm_files_id DESC
)
SELECT * FROM valami) 
TO '/tmp/hinyzo_fotok.csv' CSV HEADER;


image-f26bd838-d69d-452c-8fca-d14dc9a8ef6f.jpg
image-c524c7dd-d381-49db-81a4-4f3b814bd71c.jpg
image-800bc60a-9825-4e3f-9994-ca2385534269.jpg
image-b4058ec5-8097-4126-888e-1c593830b343.jpg
image-b6351e44-6779-43f2-bbf7-d954516cb8ab.jpg
IMG_20190912_120313.jpg
image-be6f3128-4d1e-42a8-91b1-389dc9a6c514.jpg
image-35af9407-3752-4722-893b-45d527f7a812.jpg
IMG_20190912_112454.jpg
image-31bfe8ff-3631-4940-8346-382ac251eeeb.jpg
image-74a5ef0b-66c7-4c06-8d06-94329f7d85d7.jpg
image-d39ce1dc-102b-4f13-9d9a-2a256f8be6a3.jpg
image-e63acea9-595f-430f-868d-c46c2c87cecd.jpg
image-821ea02f-e1b3-4e13-8160-853099b1178a.jpg
image-ae640b40-1ee5-489e-a64c-6dcaa8e9a19d.jpg
image-fcea54bb-1ee9-42f5-98d8-b05433723da0.jpg
image-4ccf6607-d95d-4292-9278-f36d387bb3c4.jpg
image-baa1510e-8d46-4b7c-a319-da79d61efb56.jpg
image-ec232340-dd83-4c0b-be02-00557a9c2831.jpg
image-e908670e-73d2-4e91-960c-f4c3bf4e00ba.jpg
image-53619520-dcce-4ed6-a652-020d7c970c17.jpg
image-49516490-742a-4e17-83e0-accca81831cf.jpg
image-67f56ff5-9da5-4fc7-bc91-01138d220372.jpg
image-cec3e500-ba03-47c2-a5fe-a4babb2e78ad.jpg
image-afe759dc-50ad-4613-9ed8-63587565f9be.jpg
image-dfbe36ac-7d08-4cdf-adea-d3487e2fd4f2.jpg
image-c86aaaf9-05a9-425e-9035-0fdc23f00e7b.jpg
image-d4d9a2b4-f34c-4631-8717-4344174b034a.jpg
image-6864c6f8-50d3-40d6-9f2f-54576c0fbfd7.jpg
image-fd3c65ac-2d46-44ae-bf31-ea14bb7ce48a.jpg
image-967f6254-e87e-40f3-9cc6-f3bccb131082.jpg
image-ab8a8f39-7f3b-42d4-b838-6f3c989b5799.jpg
image-ecdb1476-18bc-4ea3-b613-883896903492.jpg
image-43415535-9431-422b-a865-895d3fa676af.jpg
image-8e5e573c-f142-4fdb-8e6a-cf7b33bfcc71.jpg
image-26cebbc3-89b4-45b9-9771-14188f1db954.jpg
image-0b838d86-cd32-42d6-8d5d-07f1b81981c1.jpg
image-8294a4d5-9c93-43a7-bef3-bbc7e643bb0e.jpg
image-84cb9bad-826d-4c8b-b65b-4335b304f24a.jpg
image-4a01aecb-4e39-4d5a-9e28-5ad9af02e3fd.jpg
image-9acd4e90-cd44-41ff-9f28-b98c1e158226.jpg
image-080d1a76-3979-4fca-800f-7b8a0e5d28f7.jpg
image-8abf9f18-2b6e-4070-a4a4-ede96aabd9db.jpg
image-0d3138f9-aee7-4927-b167-a045919a44fe.jpg
image-4acdd24c-1b64-4409-8d86-e1f68f02689f.jpg
image-a3b9d976-59c8-407f-9d55-8b0363e2f16a.jpg
image-7c5b2139-7033-468e-8002-1a2f017ddb4d.jpg
image-4cf90bb0-fc7b-44f0-bcde-a6a337ede4ee.jpg
image-a054a320-9d32-49c7-9c8e-257de205e1d7.jpg
image-91504f0a-12ff-4326-be15-05c90ad83d1c.jpg
image-634f936a-c1f9-4a8d-a39d-2abc1b1be371.jpg
image-35a9fba5-d17a-43dc-8001-b45426230299.jpg
image-3f0a26d6-1a77-48af-9bd9-605cb1784181.jpg
IMG_20190911_181944.jpg
image-77eb3c19-140f-4243-a999-7e74d66789ca.jpg
image-0aa1d56d-51ec-4f66-9d64-012793d5c587.jpg
image-d914bea5-8a6e-4058-91db-1d164ad5086e.jpg
image-3cfd3f31-6a9a-484b-8dd0-e5aa9b782ea1.jpg
image-1770f889-4880-4ef5-98b2-f400a20101dc.jpg
image-0223f44d-ad8d-412e-b9ea-e39770e49c87.jpg
image-10ed8814-50fb-44b6-bc0a-5b5aafb1fc98.jpg
image-c075324b-4c39-4855-8e8b-c3ad6e50343d.jpg
image-e311b952-04eb-44ba-950e-249690bc2bd5.jpg
image-e262a946-55e0-4f72-99c9-2b0e0157cfca.jpg
image-eaee6806-4564-433b-9c43-11a5c766dd63.jpg
image-efb62522-73d8-4273-9e3f-bcd6b0d8a01d.jpg
image-68d4c743-9939-4648-98cc-c2cd2bc2dab9.jpg
IMG_20190911_172455.jpg
image-786247e4-039a-49c6-8412-2ff75ff18f87.jpg
image-e438b381-cffa-4753-b5d0-f160c6daedac.jpg
image-0a557fbc-8bf6-4229-99ad-532579ea0e4d.jpg
image-cfd8b919-d8d6-4fa0-afe0-7cb6fcf52a29.jpg
image-88e16978-d69a-4016-b057-d93805d76035.jpg
image-b274ebff-3c0e-4957-9191-8c972fce64ba.jpg
image-a45f50b0-89c8-4eed-bb74-845842dfaf50.jpg
image-6076584b-7cd1-493f-b5f4-7b9e0b68270b.jpg
image-71edd00d-ecf5-4e11-a63d-5c879812e52c.jpg
image-5078f5a6-1ad2-48be-8fad-381eda6b72cc.jpg
image-47d82ca7-b822-4eb4-a1a8-bdff99ba5a45.jpg
image-251995b5-5c6d-4117-abc5-7eda6c0bb917.jpg
image-9bdf1652-7fc3-4827-9cf9-5b4fd8ccde7e.jpg
image-0782644c-8d15-4d83-898b-cb462fa04280.jpg
image-4b3223ef-4f89-4435-82da-143eda813b12.jpg
image-fc171fb2-dd84-4087-8a1b-38360072effd.jpg
image-834ab727-8010-4b45-8ca2-1fea8822656c.jpg
image-a14f4610-10b5-4685-abb8-42d67b219534.jpg
image-a1b4d715-701e-406b-a301-cf1ab4375b7b.jpg
image-0c61b722-2605-4f89-a0c2-48ee0c9145a7.jpg
image-6e32779a-8073-410d-9e8e-34e03b97c645.jpg
image-e8706860-6b7a-4171-b63e-e6c433e5328b.jpg
image-8935b791-8b08-43f4-bbda-2314e1e4d406.jpg
image-16d96117-29f2-4357-a340-2929d7633a0e.jpg
image-21107cb6-6f88-4f58-b44f-5ce9fc7cc543.jpg
image-9a1c2bef-bf57-4ff0-bdd8-39d37fa61cce.jpg
image-cecdfc4e-a057-4eab-aa97-197bf446a032.jpg
image-82614ed0-6fbb-4845-85f2-827ae1adb2e1.jpg
image-4e786ed0-5ac3-4a39-85da-719d2a442dcc.jpg
image-ea1dbefb-1a9f-4f3c-8676-ccc574fb70cf.jpg
image-a889e22d-14ee-4707-9b05-8328db71a761.jpg
image-e4e090df-a1bc-4bb3-976c-053ec8df903d.jpg
image-00168109-33d8-4ba1-8742-ca6fea995be8.jpg
image-e2776b70-0bac-4541-b00a-0cb0a2912f6c.jpg
image-f0d34f69-e996-4072-b60c-6fbb99bdfb7c.jpg
image-13c92718-c341-4dae-aeaf-7ac877022a44.jpg
image-403a0523-b3d2-4e08-9296-c3d2582dd585.jpg
image-d9017526-c8d1-4ed1-968d-ef9af3930aac.jpg
image-6b241499-7c06-4065-bff1-0baff4e3b271.jpg
image-4b857d69-b1af-4f0d-9abe-009700f8276c.jpg
image-b6b1c3ec-599b-45a8-810d-06364aafe232.jpg
image-66e6a102-aa60-4e0a-b9ec-0a00316d7d44.jpg
image-6131173c-54f8-4b48-85a7-72bdbd35edf0.jpg
image-7d26233b-aa70-42ab-9167-ed742e0fcbe6.jpg
image-7c4c2455-8add-429c-aee9-51c8abe80e44.jpg
image-eef13d52-064d-4f0a-936a-5f654a64d282.jpg
image-0248511a-8ca2-4941-8394-9aa983c64abf.jpg
image-e346880d-2822-416f-a85a-157ac2c643dd.jpg
image-7d91d0fa-fa15-49d2-922a-f6bd82e83dad.jpg
image-53454746-2e4e-40ba-b16c-1ffbffa6c794.jpg
image-612474e9-e695-4a68-a8fb-25a594e13825.jpg
image-9db146cf-33ef-49ac-b3d5-646c7253cb7d.jpg
image-d4c04ecf-084a-4ebe-aed2-72fd57e30618.jpg
IMG_20190912_151241.jpg
image-fcc36936-13db-4c47-91ee-292ab8fe1e36.jpg
image-3f7b1be4-e76e-4ed8-9a95-3e55565682d5.jpg
image-12050b17-5cf3-4445-bf59-f74aa6edcf72.jpg
image-6cee100f-f086-43ce-b77c-21381bace8b2.jpg
image-1b915d4d-d778-4b2c-b6cf-8099742063b2.jpg
image-27eb6eba-ae9f-42e0-b888-e2d1ecc46bcc.jpg
IMG_20190912_173229.jpg



WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-f26bd838-d69d-452c-8fca-d14dc9a8ef6f.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	199) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-c524c7dd-d381-49db-81a4-4f3b814bd71c.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	202) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-800bc60a-9825-4e3f-9994-ca2385534269.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	201) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-b4058ec5-8097-4126-888e-1c593830b343.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	198) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-b6351e44-6779-43f2-bbf7-d954516cb8ab.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	197) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'IMG_20190912_120313.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	194) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-be6f3128-4d1e-42a8-91b1-389dc9a6c514.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	193) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-35af9407-3752-4722-893b-45d527f7a812.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	192) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'IMG_20190912_112454.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	190) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-31bfe8ff-3631-4940-8346-382ac251eeeb.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	189) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-74a5ef0b-66c7-4c06-8d06-94329f7d85d7.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	188) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-d39ce1dc-102b-4f13-9d9a-2a256f8be6a3.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	173) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e63acea9-595f-430f-868d-c46c2c87cecd.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	174) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-821ea02f-e1b3-4e13-8160-853099b1178a.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	172) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-ae640b40-1ee5-489e-a64c-6dcaa8e9a19d.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	168) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-fcea54bb-1ee9-42f5-98d8-b05433723da0.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	169) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-4ccf6607-d95d-4292-9278-f36d387bb3c4.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	170) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-baa1510e-8d46-4b7c-a319-da79d61efb56.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	171) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-ec232340-dd83-4c0b-be02-00557a9c2831.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	175) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e908670e-73d2-4e91-960c-f4c3bf4e00ba.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	176) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-53619520-dcce-4ed6-a652-020d7c970c17.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	177) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-49516490-742a-4e17-83e0-accca81831cf.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	181) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-67f56ff5-9da5-4fc7-bc91-01138d220372.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	179) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-cec3e500-ba03-47c2-a5fe-a4babb2e78ad.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	178) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-afe759dc-50ad-4613-9ed8-63587565f9be.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	180) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-dfbe36ac-7d08-4cdf-adea-d3487e2fd4f2.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	183) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-c86aaaf9-05a9-425e-9035-0fdc23f00e7b.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	182) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-d4d9a2b4-f34c-4631-8717-4344174b034a.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	185) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-6864c6f8-50d3-40d6-9f2f-54576c0fbfd7.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	215) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-fd3c65ac-2d46-44ae-bf31-ea14bb7ce48a.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	210) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-967f6254-e87e-40f3-9cc6-f3bccb131082.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	214) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-ab8a8f39-7f3b-42d4-b838-6f3c989b5799.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	211) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-ecdb1476-18bc-4ea3-b613-883896903492.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	209) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-43415535-9431-422b-a865-895d3fa676af.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	205) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-8e5e573c-f142-4fdb-8e6a-cf7b33bfcc71.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	206) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-26cebbc3-89b4-45b9-9771-14188f1db954.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	207) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0b838d86-cd32-42d6-8d5d-07f1b81981c1.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	208) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-8294a4d5-9c93-43a7-bef3-bbc7e643bb0e.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	218) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-84cb9bad-826d-4c8b-b65b-4335b304f24a.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	216) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-4a01aecb-4e39-4d5a-9e28-5ad9af02e3fd.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	212) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-9acd4e90-cd44-41ff-9f28-b98c1e158226.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	223) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-080d1a76-3979-4fca-800f-7b8a0e5d28f7.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	217) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-8abf9f18-2b6e-4070-a4a4-ede96aabd9db.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	221) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0d3138f9-aee7-4927-b167-a045919a44fe.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	213) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-4acdd24c-1b64-4409-8d86-e1f68f02689f.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	222) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-a3b9d976-59c8-407f-9d55-8b0363e2f16a.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	46) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-7c5b2139-7033-468e-8002-1a2f017ddb4d.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	45) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-4cf90bb0-fc7b-44f0-bcde-a6a337ede4ee.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	43) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-a054a320-9d32-49c7-9c8e-257de205e1d7.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	41) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-91504f0a-12ff-4326-be15-05c90ad83d1c.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	48) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-634f936a-c1f9-4a8d-a39d-2abc1b1be371.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	44) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-35a9fba5-d17a-43dc-8001-b45426230299.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	42) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-3f0a26d6-1a77-48af-9bd9-605cb1784181.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	57) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'IMG_20190911_181944.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	52) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-77eb3c19-140f-4243-a999-7e74d66789ca.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	51) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0aa1d56d-51ec-4f66-9d64-012793d5c587.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	49) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-d914bea5-8a6e-4058-91db-1d164ad5086e.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	47) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-3cfd3f31-6a9a-484b-8dd0-e5aa9b782ea1.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	50) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-1770f889-4880-4ef5-98b2-f400a20101dc.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	54) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0223f44d-ad8d-412e-b9ea-e39770e49c87.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	53) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-10ed8814-50fb-44b6-bc0a-5b5aafb1fc98.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	58) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-c075324b-4c39-4855-8e8b-c3ad6e50343d.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	63) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e311b952-04eb-44ba-950e-249690bc2bd5.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	62) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e262a946-55e0-4f72-99c9-2b0e0157cfca.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	60) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-eaee6806-4564-433b-9c43-11a5c766dd63.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	55) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-efb62522-73d8-4273-9e3f-bcd6b0d8a01d.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	61) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-68d4c743-9939-4648-98cc-c2cd2bc2dab9.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	66) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'IMG_20190911_172455.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	67) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-786247e4-039a-49c6-8412-2ff75ff18f87.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	65) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e438b381-cffa-4753-b5d0-f160c6daedac.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	59) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0a557fbc-8bf6-4229-99ad-532579ea0e4d.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	64) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-cfd8b919-d8d6-4fa0-afe0-7cb6fcf52a29.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	70) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-88e16978-d69a-4016-b057-d93805d76035.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	69) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-b274ebff-3c0e-4957-9191-8c972fce64ba.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	68) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-a45f50b0-89c8-4eed-bb74-845842dfaf50.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	72) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-6076584b-7cd1-493f-b5f4-7b9e0b68270b.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	71) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-71edd00d-ecf5-4e11-a63d-5c879812e52c.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	75) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-5078f5a6-1ad2-48be-8fad-381eda6b72cc.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	81) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-47d82ca7-b822-4eb4-a1a8-bdff99ba5a45.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	80) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-251995b5-5c6d-4117-abc5-7eda6c0bb917.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	77) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-9bdf1652-7fc3-4827-9cf9-5b4fd8ccde7e.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	79) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0782644c-8d15-4d83-898b-cb462fa04280.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	74) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-4b3223ef-4f89-4435-82da-143eda813b12.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	109) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-fc171fb2-dd84-4087-8a1b-38360072effd.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	115) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-834ab727-8010-4b45-8ca2-1fea8822656c.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	112) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-a14f4610-10b5-4685-abb8-42d67b219534.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	113) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-a1b4d715-701e-406b-a301-cf1ab4375b7b.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	110) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0c61b722-2605-4f89-a0c2-48ee0c9145a7.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	105) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-6e32779a-8073-410d-9e8e-34e03b97c645.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	121) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e8706860-6b7a-4171-b63e-e6c433e5328b.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	125) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-8935b791-8b08-43f4-bbda-2314e1e4d406.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	124) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-16d96117-29f2-4357-a340-2929d7633a0e.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	123) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-21107cb6-6f88-4f58-b44f-5ce9fc7cc543.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	119) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-9a1c2bef-bf57-4ff0-bdd8-39d37fa61cce.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	118) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-cecdfc4e-a057-4eab-aa97-197bf446a032.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	117) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-82614ed0-6fbb-4845-85f2-827ae1adb2e1.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	149) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-4e786ed0-5ac3-4a39-85da-719d2a442dcc.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	153) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-ea1dbefb-1a9f-4f3c-8676-ccc574fb70cf.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	154) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-a889e22d-14ee-4707-9b05-8328db71a761.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	151) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e4e090df-a1bc-4bb3-976c-053ec8df903d.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	152) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-00168109-33d8-4ba1-8742-ca6fea995be8.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	155) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e2776b70-0bac-4541-b00a-0cb0a2912f6c.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	158) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-f0d34f69-e996-4072-b60c-6fbb99bdfb7c.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	160) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-13c92718-c341-4dae-aeaf-7ac877022a44.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	162) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-d9017526-c8d1-4ed1-968d-ef9af3930aac.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	159) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-6b241499-7c06-4065-bff1-0baff4e3b271.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	163) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-4b857d69-b1af-4f0d-9abe-009700f8276c.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	165) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-b6b1c3ec-599b-45a8-810d-06364aafe232.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	164) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-66e6a102-aa60-4e0a-b9ec-0a00316d7d44.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	166) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-6131173c-54f8-4b48-85a7-72bdbd35edf0.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	161) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-7d26233b-aa70-42ab-9167-ed742e0fcbe6.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	156) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-7c4c2455-8add-429c-aee9-51c8abe80e44.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	167) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-eef13d52-064d-4f0a-936a-5f654a64d282.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	145) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-0248511a-8ca2-4941-8394-9aa983c64abf.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	143) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-e346880d-2822-416f-a85a-157ac2c643dd.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	147) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-7d91d0fa-fa15-49d2-922a-f6bd82e83dad.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	148) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-53454746-2e4e-40ba-b16c-1ffbffa6c794.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	150) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-612474e9-e695-4a68-a8fb-25a594e13825.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	140) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-9db146cf-33ef-49ac-b3d5-646c7253cb7d.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	141) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-d4c04ecf-084a-4ebe-aed2-72fd57e30618.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	139) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'IMG_20190912_151241.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	137) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-fcc36936-13db-4c47-91ee-292ab8fe1e36.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	136) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-3f7b1be4-e76e-4ed8-9a95-3e55565682d5.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	132) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-12050b17-5cf3-4445-bf59-f74aa6edcf72.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	130) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-6cee100f-f086-43ce-b77c-21381bace8b2.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	131) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-1b915d4d-d778-4b2c-b6cf-8099742063b2.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	142) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'image-27eb6eba-ae9f-42e0-b888-e2d1ecc46bcc.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	82) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;
WITH fid as (SELECT id FROM system.files WHERE project_table = 'mures' AND reference = 'IMG_20190912_173229.jpg'), c as (SELECT obm_files_id FROM mures_amenintari WHERE obm_id =	97) INSERT INTO system.file_connect (file_id, conid) SELECT fid.id, c.obm_files_id FROM fid join c on 1=1;

