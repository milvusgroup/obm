CREATE TRIGGER "rehab_events_biu_trigger" BEFORE INSERT OR UPDATE ON "rehab_events" FOR EACH ROW EXECUTE PROCEDURE rehab_events_biu();
CREATE TRIGGER "rehab_events_aiu_trigger" AFTER INSERT OR UPDATE ON "rehab_events" FOR EACH ROW EXECUTE PROCEDURE rehab_events_aiu();
CREATE TRIGGER "rehab_events_personlist_trigger" AFTER INSERT OR UPDATE OR DELETE ON "rehab_events" FOR EACH ROW EXECUTE PROCEDURE update_rehab_event_personlist();
