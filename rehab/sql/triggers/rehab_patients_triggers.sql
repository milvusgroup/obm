CREATE TRIGGER "rehab_patients_biu_trigger" BEFORE INSERT OR UPDATE ON "rehab" FOR EACH ROW EXECUTE PROCEDURE rehab_patients_biu();
CREATE TRIGGER "rehab_patients_aiu_trigger" AFTER INSERT OR UPDATE ON "rehab" FOR EACH ROW EXECUTE PROCEDURE rehab_patients_aiu();
