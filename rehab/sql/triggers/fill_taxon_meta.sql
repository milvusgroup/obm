CREATE TRIGGER "rehab_taxon_meta_fill" BEFORE INSERT OR UPDATE ON "rehab_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
