-- egyelőre nem szükséges általánosítani, esetleg ha lesz egy termék belőle
CREATE TABLE rehab_institute_info AS (
    denumire varchar,
    proprietar varchar,
    tip_proprietate varchar,
    administrator varchar,
    adresa varchar,
    telefon varchar,
    fax varchar,
    email varchar,
    website varchar,
    data_infiintarii date,
    suprafata_totala numeric,
    suprafata_amplasament numeric,
    nr_am varchar,
    data_emiterii_am date,
    date_expirarii_am date,
    nr_asv varchar,
    data_emiterii_asv date,
    date_expirarii_asv date,

)