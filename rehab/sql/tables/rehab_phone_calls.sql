-- Adminer 4.8.1 PostgreSQL 11.16 dump

DROP TABLE IF EXISTS "rehab_phone_calls";
DROP SEQUENCE IF EXISTS rehab_phone_calls_obm_id_seq;
CREATE SEQUENCE rehab_phone_calls_obm_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."rehab_phone_calls" (
    "obm_id" integer DEFAULT nextval('rehab_phone_calls_obm_id_seq') NOT NULL,
    "obm_geometry" geometry,
    "obm_uploading_id" integer,
    "obm_validation" numeric,
    "obm_comments" text[],
    "obm_modifier_id" integer,
    "obm_files_id" character varying(32),
    "date" date,
    "call_count" integer,
    CONSTRAINT "rehab_phone_calls_pkey" PRIMARY KEY ("obm_id"),
    CONSTRAINT "enforce_dims_obm_geometry" CHECK (st_ndims(obm_geometry) = 2),
    CONSTRAINT "enforce_srid_obm_geometry" CHECK (st_srid(obm_geometry) = 4326)
) WITH (oids = false);


ALTER TABLE ONLY "public"."rehab_phone_calls" ADD CONSTRAINT "uploading_id" FOREIGN KEY (obm_uploading_id) REFERENCES system.uploadings(id) NOT DEFERRABLE;

-- 2022-07-06 14:53:00.307765+03
