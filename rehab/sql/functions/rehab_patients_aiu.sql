CREATE OR REPLACE FUNCTION "rehab_patients_aiu" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    uploader varchar;
BEGIN
    IF tg_op = 'INSERT' THEN
        SELECT uploader_name INTO uploader FROM system.uploadings WHERE id = new.obm_uploading_id;
        EXECUTE FORMAT('INSERT INTO rehab_events (obm_uploading_id, patient_id, event, date, location, diagnosis, executed_by, recorded_by) VALUES (%L, %L, %L, %L, %L, %L, %L, %L);', new.obm_uploading_id, new.obm_id, 'str_entry', new.date_of_entry, new.current_location, new.first_diagnosis, new.registered_by, uploader);
        RETURN new;
    END IF;
    RETURN new;
END;
$$;

--OR (tg_op = 'UPDATE' AND old.nickname LIKE '% | %') THEN
