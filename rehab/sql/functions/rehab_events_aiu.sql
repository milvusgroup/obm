CREATE OR REPLACE FUNCTION "rehab_events_aiu" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    markings varchar;
    status varchar;
    loc record;
BEGIN 
    IF tg_op = 'INSERT' THEN 
        
        IF new.event IN ('str_death', 'str_euthanasia') THEN
            EXECUTE format('UPDATE rehab SET state = ''str_dead'', date_of_exit = %L WHERE obm_id = %L', new."date", new."patient_id");

        ELSIF new.event IN ('str_release') THEN
            EXECUTE format('UPDATE rehab SET state = ''str_released'', date_of_exit = %L WHERE obm_id = %L', new."date", new."patient_id");
            
        ELSIF new.event IN ('str_return') THEN
            EXECUTE format('UPDATE rehab SET state = ''str_in_rehab'', date_of_exit = NULL WHERE obm_id = %L', new."patient_id");
            
        ELSIF new.event = 'str_ringing' THEN
            markings = CONCAT_WS('|', 'LR:' || new.left_ring, 'RR:' || new.right_ring,  'OR:' || new.other_ring, 'M:' || new.marking);
            EXECUTE format('UPDATE rehab SET markings = concat_ws('', '', markings, %L) WHERE obm_id = %L', markings, new."patient_id");
            
        ELSIF new.event = 'str_relocate' THEN
            SELECT * INTO loc FROM rehab_locations WHERE "location" = new."location";
            IF loc."external" = TRUE THEN
                EXECUTE format('UPDATE rehab SET state = ''str_in_custody'', current_location = %L, date_of_exit = %L WHERE obm_id = %L', new."location", new."date", new."patient_id");
            ELSE
                EXECUTE format('UPDATE rehab SET current_location = %L WHERE obm_id = %L', new."location", new."patient_id");
            END IF;

        END IF;
        RETURN new;
    END IF;
END $$;
