CREATE OR REPLACE FUNCTION "rehab_patients_biu" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF (tg_op = 'INSERT' AND new.nickname IS NULL) OR (tg_op = 'UPDATE' AND old.nickname LIKE '% | %') THEN
        new.nickname = new.obm_id::varchar || ' | ' || COALESCE(new.species, 'Unknown species') || ' | ' || new.current_location || ' | ' || COALESCE(new.finding_location, 'Unknown location') || ' | ' || COALESCE(new.markings, 'Unmarked');
        RETURN new;
    END IF;
    RETURN new;
END;
$$;
