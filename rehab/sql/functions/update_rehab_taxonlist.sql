CREATE OR REPLACE FUNCTION "update_rehab_taxonlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
observer RECORD;
tbl varchar(64) = TG_TABLE_NAME;
sid integer;
sids integer[] DEFAULT '{}';
term varchar(128);
BEGIN
    IF tg_op = 'INSERT' THEN
        term := NEW.species;
        PERFORM add_term(NEW.species, tbl, 'species','rehab','taxon'::varchar);
        raise notice '%' , NEW.species;
        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  OLD.species != NEW.species THEN
            PERFORM add_term(NEW.species, tbl, 'species','rehab','taxon'::varchar);
        END IF;

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        PERFORM remove_term(OLD.species, tbl, 'species','rehab','taxon');

        RETURN OLD;

    END IF;                                                                                         
END $$;