CREATE OR REPLACE FUNCTION "rehab_events_biu" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN 
    IF tg_op = 'INSERT' THEN 
        IF new.obm_geometry IS NULL THEN 
            SELECT l.obm_geometry INTO new.obm_geometry FROM rehab r LEFT JOIN rehab_locations l ON current_location = location WHERE r.obm_id = new.patient_id;
        END IF;
        RETURN new;
    END IF;
END $$;
