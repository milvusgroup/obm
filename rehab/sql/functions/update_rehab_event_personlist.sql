CREATE OR REPLACE FUNCTION "update_rehab_event_personlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    executed_by RECORD;
    tbl varchar(64) = TG_TABLE_NAME;
BEGIN
    IF tg_op = 'UPDATE' THEN 
        IF (OLD.executed_by != NEW.executed_by) THEN
            FOR executed_by IN (SELECT unnest(regexp_split_to_array(OLD.executed_by,E',\\s*')) as o) LOOP 
                PERFORM remove_term(executed_by.o, tbl, 'executed_by','rehab', 'terms');
            END LOOP;
        END IF;
        IF (OLD.recorded_by != NEW.recorded_by) THEN
            FOR executed_by IN (SELECT unnest(regexp_split_to_array(OLD.recorded_by,E',\\s*')) as o) LOOP 
                PERFORM remove_term(executed_by.o, tbl, 'executed_by','rehab', 'terms');
            END LOOP;
        END IF;
        
    END IF;


    IF tg_op = 'INSERT' OR tg_op = 'UPDATE' THEN

        FOR executed_by IN (SELECT unnest(regexp_split_to_array(NEW.executed_by,E',\\s*')) as n) LOOP 
            PERFORM add_term(executed_by.n, tbl, 'executed_by'::varchar,'rehab', 'terms');
        END LOOP;
        FOR executed_by IN (SELECT unnest(regexp_split_to_array(NEW.recorded_by,E',\\s*')) as n) LOOP 
            PERFORM add_term(executed_by.n, tbl, 'executed_by'::varchar,'rehab', 'terms');
        END LOOP;

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        FOR executed_by IN (SELECT unnest(regexp_split_to_array(OLD.executed_by,E',\\s*')) as o) LOOP 
            PERFORM remove_term(executed_by.o, tbl, 'executed_by','rehab', 'terms');
        END LOOP;
        FOR executed_by IN (SELECT unnest(regexp_split_to_array(OLD.recorded_by,E',\\s*')) as o) LOOP 
            PERFORM remove_term(executed_by.o, tbl, 'executed_by','rehab', 'terms');
        END LOOP;

        RETURN OLD;

    END IF;                                                                                         
END $$;