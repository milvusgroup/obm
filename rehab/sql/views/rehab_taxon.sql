DROP MATERIALIZED VIEW rehab_taxon CASCADE;
CREATE MATERIALIZED VIEW rehab_taxon AS
SELECT 
    meta, word,
    CASE lang 
        WHEN 'species_valid' THEN 'species_sci'
        WHEN 'species_hu' THEN 'species_hun'
        WHEN 'species_ro' THEN 'species_rom'
        WHEN 'species_en' THEN 'species_eng'
        ELSE 'species'
    END as lang,
    'b_' || taxon_id::varchar as taxon_id,
    status,
    taxon_db,
    'str_bird' as spg,
    'b_' || wid::varchar as wid
FROM milvus_taxon 
WHERE 
    lang IN ('species_valid', 'species_hu', 'species_ro', 'species_en', 'species') AND
    status != 'undefined'
UNION
SELECT 
    meta, word,
    CASE lang 
        WHEN 'specia_sci' THEN 'species_sci'
        WHEN 'specia_hu' THEN 'species_hun'
        WHEN 'specia_ro' THEN 'species_rom'
        WHEN 'specia_en' THEN 'species_eng'
        ELSE 'species'
    END as lang,
    'h_' || taxon_id::varchar as taxon_id,
    status,
    taxon_db,
    'str_reptile' as spg,
    'h_' || wid::varchar as wid
FROM openherpmaps_taxon WHERE 
    lang IN ('specia_sci', 'specia_hu', 'specia_ro', 'specia_en', 'specia') AND
    status != 'undefined'
UNION
SELECT 
    meta, word,
    CASE lang 
        WHEN 'species_sci' THEN 'species_sci'
        WHEN 'species_hu' THEN 'species_hun'
        WHEN 'species_ro' THEN 'species_rom'
        WHEN 'species_en' THEN 'species_eng'
        ELSE 'species'
    END as lang,
    'm_' || taxon_id::varchar as taxon_id,
    status,
    taxon_db,
    'str_mammal' as spg,
    'm_' || wid::varchar as wid
FROM mammalia_taxon WHERE 
    lang IN ('species_sci', 'species_hu', 'species_ro', 'species_en', 'species') AND
    status != 'undefined'
;
GRANT ALL on rehab_taxon TO rehab_admin;
DROP MATERIALIZED VIEW rehab_taxonmeta CASCADE;
CREATE MATERIALIZED VIEW rehab_taxonmeta AS
SELECT DISTINCT 
    'b_' || taxon_id::varchar as taxon_id,
    'str_bird'::varchar as species_group
FROM milvus_taxon WHERE lang IN ('species_valid', 'species_hu', 'species_ro', 'species_en', 'species')
UNION
SELECT DISTINCT 
    'h_' || taxon_id::varchar as taxon_id,
    'str_reptile'::varchar as species_group
FROM openherpmaps_taxon WHERE lang IN ('specia_sci', 'specia_hu', 'specia_ro', 'specia_en', 'specia')
UNION
SELECT DISTINCT 
    'm_' || taxon_id::varchar as taxon_id,
    'str_mammal'::varchar as species_group
FROM mammalia_taxon WHERE lang IN ('species_sci', 'species_hu', 'species_ro', 'species_en', 'species')
;
GRANT ALL on rehab_taxonmeta TO rehab_admin;