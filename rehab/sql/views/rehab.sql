CREATE VIEW rehab AS
 SELECT d.obm_id,
    d.obm_geometry,
    d.obm_datum,
    d.obm_uploading_id,
    d.obm_validation,
    d.obm_comments,
    d.obm_modifier_id,
    d.obm_files_id,
    d.obm_access,
    d.species,
    d.age,
    d.sex,
    d.markings,
    d.current_location,
    d.state,
    d.comments,
    d.finding_date,
    d.finding_location,
    d.finding_county,
    d.circumstances,
    d.found_by_id,
    d.transport_mode,
    d.brought_by_id,
    d.first_diagnosis,
    d.geometry_accuracy,
    d.date_of_entry,
    d.date_of_exit,
    d.nickname,
    d.serial_number,
    d.founder_phone,
    d.registered_by,
    d.cause_of_injury,
    d.injured_part,
    vf1.word AS species_rom,
    vf3.word AS species_hun,
    vf2.word AS species_eng,
    vf4.word AS species_sci,
    vf5.species_group
   FROM (((((rehab.rehab d
     LEFT JOIN ( SELECT DISTINCT rehab_taxon.word,
            rehab_taxon.taxon_id
           FROM rehab_taxon) tx ON (((d.species)::text = (tx.word)::text)))
     LEFT JOIN rehab_taxon vf1 ON (((tx.taxon_id = vf1.taxon_id) AND ((vf1.lang)::text = 'species_rom'::text) AND ((vf1.status)::text = 'accepted'::text))))
     LEFT JOIN rehab_taxon vf2 ON (((tx.taxon_id = vf2.taxon_id) AND ((vf2.lang)::text = 'species_eng'::text) AND ((vf2.status)::text = 'accepted'::text))))
     LEFT JOIN rehab_taxon vf3 ON (((tx.taxon_id = vf3.taxon_id) AND ((vf3.lang)::text = 'species_hun'::text) AND ((vf3.status)::text = 'accepted'::text))))
     LEFT JOIN rehab_taxon vf4 ON (((tx.taxon_id = vf4.taxon_id) AND ((vf4.lang)::text = 'species_sci'::text) AND ((vf4.status)::text = 'accepted'::text))))
     LEFT JOIN rehab_taxonmeta vf5 ON vf4.taxon_id = vf5.taxon_id;

ALTER VIEW rehab OWNER TO rehab_admin ;