CREATE RULE rehab_upd AS ON UPDATE TO public.rehab
        DO INSTEAD
        UPDATE rehab.rehab SET
            obm_geometry = NEW.obm_geometry,obm_datum = NEW.obm_datum,obm_uploading_id = NEW.obm_uploading_id,obm_validation = NEW.obm_validation,obm_comments = NEW.obm_comments,obm_modifier_id = NEW.obm_modifier_id,obm_files_id = NEW.obm_files_id,obm_access = NEW.obm_access,species = NEW.species,age = NEW.age,sex = NEW.sex,markings = NEW.markings,current_location = NEW.current_location,state = NEW.state,comments = NEW.comments,finding_date = NEW.finding_date,finding_location = NEW.finding_location,finding_county = NEW.finding_county,circumstances = NEW.circumstances,found_by_id = NEW.found_by_id,transport_mode = NEW.transport_mode,brought_by_id = NEW.brought_by_id,first_diagnosis = NEW.first_diagnosis,geometry_accuracy = NEW.geometry_accuracy,date_of_entry = NEW.date_of_entry,date_of_exit = NEW.date_of_exit,nickname = NEW.nickname,serial_number = NEW.serial_number,founder_phone = NEW.founder_phone,registered_by = NEW.registered_by,cause_of_injury = NEW.cause_of_injury,injured_part = NEW.injured_part 
    WHERE
         obm_id = NEW.obm_id;
