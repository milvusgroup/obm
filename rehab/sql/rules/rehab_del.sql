CREATE RULE rehab_del AS ON DELETE TO public.rehab
        DO INSTEAD
        DELETE FROM rehab.rehab 
        WHERE
         obm_id = OLD.obm_id;
