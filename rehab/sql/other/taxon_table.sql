SELECT 
    word, 
    CASE 
        WHEN lang = 'species_valid' THEN 'species_sci'
        WHEN lang = 'species_hu' THEN 'species_hun'
        WHEN lang = 'species_ro' THEN 'species_rom'
        WHEN lang = 'species_en' THEN 'species_eng'
        ELSE lang END as lang,
    taxon_id - 2 as taxon_id,
    status
FROM milvus_taxon WHERE lang IN ('species_en', 'species_hu', 'species_ro', 'species_valid', 'species')
UNION
SELECT 
    word, 
    CASE 
        WHEN lang = 'species_hu' THEN 'species_hun'
        WHEN lang = 'species_ro' THEN 'species_rom'
        WHEN lang = 'species_en' THEN 'species_eng'
        ELSE lang END as lang,
    taxon_id + 436 as taxon_id,
    status
FROM mammalia_taxon WHERE lang IN ('species_en', 'species_hu', 'species_ro', 'species_sci', 'species')
UNION
SELECT 
    word, 
    CASE 
        WHEN lang = 'specia_hu' THEN 'species_hun'
        WHEN lang = 'specia_ro' THEN 'species_rom'
        WHEN lang = 'specia_en' THEN 'species_eng'
        ELSE lang END as lang,
    taxon_id + 2030 as taxon_id,
    status
FROM openherpmaps_taxon WHERE lang IN ('specia_en', 'specia_hu', 'specia_ro', 'specia_sci', 'specia')
