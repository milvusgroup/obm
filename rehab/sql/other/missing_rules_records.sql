SELECT * FROM rehab_rules;

ALTER TABLE rehab.rehab ADD COLUMN obm_access integer NOT NULL DEFAULT 3;

WITH upl as (
    SELECT id, "group" as rd, owner as wr FROM system.uploadings WHERE project_table = 'rehab'
)
SELECT 'rehab', obm_id, upl.rd, upl.wr, obm_access FROM rehab r LEFT JOIN upl ON r.obm_uploading_id = upl.id;

