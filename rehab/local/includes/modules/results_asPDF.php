<?php

/**
 * Class results_asPDF_CM
 * @author yourname
 */
class results_asPDF_CM {
    /**
     * undocumented function
     *
     * @return void
     */
    public static function prepare_report_fisa_de_evidenta($de)
    {
        $st_col = $_SESSION['st_col'];
        $trimesters = [
            [ 'no' => 4, 'from' => new DateTime((date('Y') - 1). '-10-01'), 'until' => new DateTime((date('Y') - 1). '-12-31') ],
            [ 'no' => 1, 'from' => new DateTime(date('Y'). '-01-01'), 'until' => new DateTime(date('Y'). '-03-31') ],
            [ 'no' => 2, 'from' => new DateTime(date('Y'). '-04-01'), 'until' => new DateTime(date('Y'). '-06-30') ],
            [ 'no' => 3, 'from' => new DateTime(date('Y'). '-07-01'), 'until' => new DateTime(date('Y'). '-09-30') ]
        ];
        $trim = $trimesters[ceil(date('m') / 3) - 1];
        $year = (date('m') < 4) ? date('Y') - 1 : date('Y');
        $events = self::load_rehab_events($trim['from']->format('Y-m-d'), $trim['until']->format('Y-m-d'));

        return [
            'error' => false,
            'species_no' => count(array_unique(array_column($events, $st_col['SPECIES_C_SCI']))),
            'indiv_no' => count(array_unique(array_column($events, 'patient_id'))),
            // egyedek száma
            'intrari_ex' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_entry');
            })),
            // fajok száma 
            'intrari_sp' => count(array_unique(array_column(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_entry');
            }), $st_col['SPECIES_C_SCI']))),
            'decedati' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_death');
            })),
            'eutanasie' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_euthanasia');
            })),
            'eliberat' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_release');
            })),
            'trimester' => $trim['no'],
            'from' => $trim['from']->format('d.m.Y.'),
            'until' => $trim['until']->format('d.m.Y.'),
            'year' => $year,
            'collaborators' => [
                'Asociația Vets4Wild',
                'ACDB (Asociația pentru Conservarea Diversității Biologice)',
                'FundațiaVisul Luanei - Luana’s dream',
                'IDNR (Iniţiativa "Drepneaua neagră" România)',
                'USAMV Cluj Napoca',
                'Asociația Prietenii Berzelor',
            ],
            'anexa1' => self::prepare_anexa1($events, $trim),
            'anexa2' => self::prepare_anexa2($year)
        ];
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function prepare_anexa2($year) {
        global $ID;
        $st_col = $_SESSION['st_col'];
        $cmd = sprintf("SELECT obm_id as id, floor((extract(month from date_of_entry)::integer - 1) / 3) + 1 as trimestru, date_of_entry as data_intrarii, %s as specia_latina, %s specia_romana, age as varsta, sex, finding_county as judet, finding_location as localitate, date_of_exit as data_iesirii, state FROM %s WHERE extract(year from date_of_entry) = %d ORDER BY date_of_entry;", $st_col['SPECIES_C_SCI'], $st_col['NATIONAL_C']['ro'], PROJECTTABLE, $year);
        if (!$res = pg_query($ID, $cmd)) {
            log_action("query error: $cmd", __FILE__, __LINE__);
            return [];
        }

        if (pg_num_rows($res) == 0) {
           return []; 
        }
        $results = pg_fetch_all($res);
        $header = array_keys($results[0]);
        $rows = array_map(function($v) {
            return ['row' => array_values(array_map(function ($c) {
                return defined($c) ? constant($c) : $c;
            },$v))];
        }, $results);
        
        return compact('header', 'rows');
    }
    
    /**
     * undocumented function
     *
     * @return void
     */
    public function prepare_anexa1($events, $trim) {

        $patient_ids = array_unique(array_column($events, 'patient_id'));
        $anexa1_patient_list = self::load_rehab_patient_list($patient_ids);
        
        return array_merge(
            [
                'patient_list' => $anexa1_patient_list,
                'phone_calls' => self::count_phone_calls($trim['from']->format('Y-m-d'), $trim['until']->format('Y-m-d')),
                'location_data' => self::load_rehab_locations($patient_ids),
            ],
            self::count_species_groups($anexa1_patient_list)
        );
    }
    

    /**
     * loads the rehab events between two dates
     *
     * @return array
     */
    private static function load_rehab_events($from, $until)
    {
        global $ID;
        $cmd = sprintf(
            "SELECT * FROM %s_events e LEFT JOIN rehab r ON e.patient_id = r.obm_id WHERE r.species_sci IS NOT NULL AND date BETWEEN %s AND %s", 
            PROJECTTABLE, 
            quote($from), 
            quote($until)
        );
        debug($cmd, __FILE__, __LINE__);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return [];
        }
        return (pg_num_rows($res) == 0) ? [] : pg_fetch_all($res);
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function load_rehab_patient_list($ids) {
        global $ID;
        $st_col = $_SESSION['st_col'];
        $cmd = sprintf(
            "SELECT ROW_NUMBER() OVER() as nr, %1\$s as sci_name, %2\$s as rom_name, r.species_group, count(*) as count 
             FROM %3\$s r 
             WHERE obm_id IN (%4\$s) 
             GROUP BY %1\$s, %2\$s, r.species_group",
            $st_col['SPECIES_C_SCI'], 
            $st_col['NATIONAL_C']['ro'], 
            PROJECTTABLE, 
            implode(',', $ids)
        );
        if (!$res = pg_query($ID, $cmd)) {
            log_action("query error: $cmd", __FILE__, __LINE__);
            return [];
        }
        return (pg_num_rows($res) == 0) ? [] : pg_fetch_all($res);
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function load_rehab_locations($ids) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT count(DISTINCT finding_county) as counties, count(DISTINCT finding_location) as locations FROM %s WHERE obm_id IN (%s)", PROJECTTABLE, implode(',', $ids));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return [];
        }
        return (pg_num_rows($res) == 0) ? [] : pg_fetch_assoc($res);
    }
    

    /**
     * undocumented function
     *
     * @return integer
     */
    public function count_phone_calls($from, $until) {
        global $ID;
        $cmd = sprintf("SELECT sum(call_count) as cnt FROM %s_phone_calls WHERE date BETWEEN %s AND %s;", PROJECTTABLE, quote($from), quote($until));
        if (!$res = pg_query($ID, $cmd)) {
            log_action("query error: $cmd", __FILE__, __LINE__);
            return [];
        }
        if (pg_num_rows($res) == 0) {
            return 0;
        }
        else {
            $results = pg_fetch_assoc($res);
            return (int)$results['cnt'];
        }
    }
    
    /**
     * undocumented function
     *
     * @return void
     */
    public function count_species_groups($species_list) {
        $groups = ['str_bird', 'str_mammal', 'str_reptile', 'str_amphibian'];
        $counts = array_map(function($v) use ($species_list) {
            return count(array_filter($species_list, function ($r) use ($v) {
                return ($r['species_group'] === $v);
            }));
        }, $groups);
        $labels = array_map(function($v) {
            return substr($v, 4);
        }, $groups);
        return array_combine($labels, $counts);
    }
}
