(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        var menuHeight = $('#mainNav').outerHeight();
        $('html, body').animate({
          scrollTop: (target.offset().top - menuHeight)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Collapse the navbar when page is scrolled
  $(window).scroll(function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  });

  birdForm();
})(jQuery); // End of use strict


function birdForm() {
    var qa = 
    {
        1: {
            question: 'Sérült vagy vérzik?',
            answers: 
            {
                0: {
                    a: 'Igen',
                    n: 2,
                    img: ''
                },
                1: {
                    a: 'Nem',
                    n: 3,
                    img: ''
                }
            },
            prev: 0,
            level: 1
        },
        2: {
            question: 'Kérj tanácsot a rehabilitációs központ szakembereitől!',
            answers: 
            {
                0: {
                    a: '0722-533816',
                    n: 2,
                    img: ''
                }
            },
            prev: 1,
            level: 2
        },
        3: {
            question: 'Hogy néz ki?',
            answers: 
            {
                0: {
                    a: 'Csupasz vagy tokos',
                    n: 4,
                    img: ''
                },
                1: {
                    a: 'Tollas',
                    n: 5,
                    img: ''
                },
                2: {
                    a: 'Sarlósfecske',
                    n: 6,
                    img: ''
                }
            },
            prev: 1,
            level: 2
        },
        4: {
            question: 'Látod a fészket?',
            answers: 
            {
                0: {
                    a: 'Igen',
                    n: 7,
                    img: ''
                },
                1: {
                    a: 'Nem',
                    n: 8,
                    img: ''
                }
            },
            prev: 3,
            level: 3
        },
        5: {
            question: 'Közvetlen veszélyben van?',
            answers: 
            {
                0: {
                    a: 'Igen',
                    n: 9,
                    img: ''
                },
                1: {
                    a: 'Nem',
                    n: 10,
                    img: ''
                }
            },
            prev: 3,
            level: 3
        },
        6: {
            question: 'Lépj kapcsolatba a sarlósfecskék rehabilitációs központ szakemberével:',
            answers: 
            {
                0: {
                    a: 'Inițiativa „Drepneaua Neagră România” 0730-366428',
                    n: 6,
                    img: ''
                }
            },
            prev: 3,
            level: 3
        },
        7: {
            question: 'Ha lehetséges, nyugodtan tedd vissza a fészekbe, ne izgulj, a szülei nem fogják megérezni a szagodat.',
            answers: 
            {
                0: {
                    a: '',
                    n: 11,
                    img: ''
                }
            },
            prev: 4,
            level: 4
        },
        8: {
            question: 'Tedd a fiókát egy kosárba, majd a kosarat kötözd fel egy közeli faágra, hogy a szülők hang alapján megtalálják kicsinyüket ne vidd haza a fiókát!',
            answers: 
            {
                0: {
                    a: '',
                    n: 12,
                    img: ''
                }
            },
            prev: 4,
            level: 4
        },
        9: {
            question: 'Óvatosan tereld biztonságos helyre, ha szükséges tedd egy közeli bokor ágai közé. Háziállataidat (kutyát, macskát) ne engedd a fióka közelébe.',
            answers: 
            {
                0: {
                    a: '',
                    n: 13,
                    img: ''
                }
            },
            prev: 5,
            level: 4
        },
        10: {
            question: 'Ne lépj közbe!! Lehet, hogy úgy tűnik, hogy segítségre lenne szüksége, de a kirepült fiókának ez egy természetes állapot. A szülők továbbra is gondoskodnak róla.',
            answers: 
            {
                0: {
                    a: '',
                    n: 14,
                    img: ''
                }
            },
            prev: 5,
            level: 4
        },
        11: {
            question: 'Figyeld megetetik a szülök a fiókákat 2 órán belül?',
            answers: 
            {
                0: {
                    a: 'igen',
                    n: 15,
                    img: ''
                },
                1: {
                    a: 'nem',
                    n: 2,
                    img: ''
                }
            },
            prev: 7,
            level: 5
        },
        12: {
            question: 'Figyeld megetetik a szülök a fiókákat 2 órán belül?',
            answers: 
            {
                0: {
                    a: 'igen',
                    n: 16,
                    img: ''
                },
                1: {
                    a: 'nem',
                    n: 2,
                    img: ''
                }
            },
            prev: 8,
            level: 5
        },
        13: {
            question: 'Mosolyogj, hiszen a lehető legjobb dolgot tetted.',
            answers: 
            {
                0: {
                    a: ':-)',
                    n: 13,
                    img: ''
                }
            },
            prev: 9,
            level: 5
        },
        14: {
            question: 'Mosolyogj, hiszen a lehető legjobb dolgot tetted.',
            answers: 
            {
                0: {
                    a: ':-)',
                    n: 14,
                    img: ''
                }
            },
            prev: 10,
            level: 5
        },
        15: {
            question: 'Mosolyogj, hiszen a lehető legjobb dolgot tetted.',
            answers: 
            {
                0: {
                    a: ':-)',
                    n: 15,
                    img: ''
                }
            },
            prev: 11,
            level: 6
        },
        16: {
            question: 'Mosolyogj, hiszen a lehető legjobb dolgot tetted.',
            answers: 
            {
                0: {
                    a: ':-)',
                    n: 16,
                    img: ''
                }
            },
            prev: 12,
            level: 6
        }
    };
    birdSlide(qa,1);
}
function birdSlide(arr,idx) {
    if ($('.slide').length) {
        $('.slide').animate({right:'1500px'});
        console.log('teszt');
    }
    var slide = arr[idx];
    var s = '<div class="slide"><button class="btn btn-secondary btn-birdForm" data-next="'+slide.prev+'" type="button"><</button>';
    s += '<h2>'+slide.question+'</h2><div>';
    var sl = slide.answers;
    for (var key in sl) {
        s += '<button class="btn btn-secondary btn-birdForm" data-next="'+sl[key].n+'" type="button">'+sl[key].a+'</button>';
    }
    s += '</div><div>';
    for (var i = 0, lev = slide.level; i < lev; i++) {
        s += 'o';
    }
    s += '</div></div>';
    setTimeout(function() {
        $('#birdForm').html(s);
        $('.btn-birdForm').click(function() {
            var next = $(this).attr('data-next');
            birdSlide(arr,next);
        });
    },500);

}

