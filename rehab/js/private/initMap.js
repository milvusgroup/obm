function initMap(){
    var map = new google.maps.Map(document.getElementById('partnersMap'),
            {
                center: {lat: 46.0435077, lng: 24.9820964},
                zoom: 7,
                mapTypeControlOptions: {
                      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                      position: google.maps.ControlPosition.LEFT_BOTTOM
                }
            });

    $.getJSON('ajax_rehab',{action: 'init-map'})
        .done(function(data, textStatus, jqXHR) {

            var d = JSON.parse(data[0].json_build_object);
            var markers = map.data.addGeoJson(d);
            var cross = 'images/blue_cross.png';
            
            map.data.setStyle(function(feature) {
                return {icon:cross};
            }); 

            var infowindow = new google.maps.InfoWindow({
                content: ""
            });

            google.maps.event.addListener(map, 'idle', function(event) {
                var bounds = map.getBounds();

                emergencyList(bounds);
            });

            map.data.addListener('click', function(event) {
                var name = event.feature.getProperty("name");
                var address = event.feature.getProperty("address");
                var county = event.feature.getProperty("county");
                var city = event.feature.getProperty("city");
                var phone = event.feature.getProperty("phone");
                var organization = event.feature.getProperty("organization");
                var email = event.feature.getProperty("email");

                var myHTML = '<h3>'+name+'</h3>';
                myHTML += '<div>' + city + ', ' + address+ ', jud. ' + county + '</div>';
                myHTML += '<div>'+phone+'</div>';
                myHTML += '<div>'+organization+'</div>';
                myHTML += '<div>'+email+'</div>';

                infowindow.setContent("<div id='grid' style='width:100%; text-align: center;'>"+myHTML+"</div>");
                infowindow.setPosition(event.latLng);

                //infowindow.setOptions({pixelOffset: new google.maps.Size(0,-30)});
                disableScrollwheel(map);

                infowindow.open(map);
            });  

            infowindow.addListener('closeclick',function(){
                enableScrollwheel(map);
            });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
    


};

/* Enables mouse scroll zoom */

function enableScrollwheel(map) {
    if(map) map.setOptions({ scrollwheel: true });
}

/* Disables mouse scroll zoom */

function disableScrollwheel(map) {
    if(map) map.setOptions({ scrollwheel: false });
}

function emergencyList(bounds) {
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
    var parameters = {
        xmin: sw.lng(),
        xmax: ne.lng(),
        ymin: sw.lat(),
        ymax: ne.lat(),
        action: 'update-map'
    }
    $.getJSON('ajax_rehab',parameters)
        .done(function(data, textStatus, jqXHR) {
            // call typeahead's callback with search results (i.e., places)
            $('#emergencyList').html('');
            $.each(data, function(key,val) {
                var card = '<div class="emergencyPoint"><h5>' + val.name + '</h5>';
                card += '<div>' + val.city + '</div>';
                card += '<div>' + val.phone + '</div>';
                $('#emergencyList').append(card);
            });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
        
}

