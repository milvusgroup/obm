    <link rel="stylesheet" href="https://<?php echo URL ?>/css/roller.css?rev=<?php echo rev('css/roller.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="https://<?php echo URL ?>/css/mapsdata.css?rev=<?php echo rev('css/mapsdata.css'); ?>" type="text/css" />
    <header class="masthead">
      <div class="header-content">
        <div class="header-content-inner">
          <h1 id="homeHeading" style="color: white !important"><?= str_wrc ?></h1>
          <hr>
          <div id="homeSlogan">
              <?= str_emergency_number; ?><h3 style="color: white !important"> 0722 - 533816 </h3>
          </div>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#emergency-points"><?= str_emergency_points; ?></a>
        </div>
      </div>
    </header>

