<?php
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

/* GeoJSON for the simple white-stork nests map built for the electricity companies */
if (isset($_GET['rehab_contacts'])) {
    //$cmd = " SELECT json_build_object( 'type', 'FeatureCollection', 'features', json_agg(t)) as geojson FROM ( SELECT json_build_object( 'type', 'Feature', 'id', obm_id, 'geometry', ST_AsGeoJSON(obm_geometry)::json, 'properties', json_build_object( 'species', species, 'nest_place', nest_place, 'nest_status', nest_status)) FROM milvus WHERE method = 'str_occasional_observation' AND species IN ('Ciconia ciconia','null') AND observed_unit = 'str_nest_cavity' AND GeometryType(obm_geometry) = 'POINT' AND access_to_data = 0 LIMIT 100) as t;";
    
    
    $cmd = "SELECT row_to_json(fc) as geojson
            FROM ( 
                SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                FROM (
                    SELECT 'Feature' As type
                    , ST_AsGeoJSON(lg.obm_geometry)::json As geometry
                    , row_to_json((SELECT l FROM (SELECT 
                        obm_id, 
                        name, 
                        city,
                        address,
                        county,
                        email,
                        organization,
                        phone,
                        postal_code 
                        ) As l
                    )) As properties

                    FROM rehab_contacts As lg   
                    WHERE 
                        type = 'firstAidPoint' AND
                        GeometryType(obm_geometry) = 'POINT'
                    ) As f 
                )  As fc;";


    if (!$res = pg_query($ID, $cmd)) {
        echo common_message('error','query error');
        exit;
    }
    $result = pg_fetch_assoc($res);
    header("Content-type: application/json");
    echo $result['geojson'];
    exit;
}



?>
