    <section class="bg-primary" id="emergency-points">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white" style="color: white !important; margin: 0 0 .5rem !important;"><?= str_emergency_points; ?></h2>
          </div>
        </div>
        <div class="row">
          <div id="partnersMapCol" class="col-sm-12 col-md-9"><div id="partnersMap"></div></div>
          <div id="emergencyList" class="col-sm-12 col-md-3"></div>
        </div>
      </div>
    </section>
    <section id="about">
        <div class="container">
            <div class="row"> 
                <div class="col">
                    <?=  str_about_us ?>
                </div>  
            </div>
        </div> 
    </section>
    <section id="qa">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <div id="birdForm"></div>
          </div>
        </div>
      </div>
    </section>
    <section id="contact" style="background: #6d850a;">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading" style="color: white !important;">Let's Get In Touch!</h2>
            <hr class="primary">
            <p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-phone fa-3x sr-contact"></i>
            <p>0722 533 816</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fa fa-envelope-o fa-3x sr-contact"></i>
            <p>
                <script type="text/javascript"> 
                    gen_mail_to_link('rehab','milvus.ro');
                    </script>
                <noscript>
                  <em>Email address protected by JavaScript. Activate JavaScript to see the email.</em>
                </noscript>
            </p>
          </div>
        </div>
      </div>
    </section>
