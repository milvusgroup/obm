<script>
/**
 * Wildlife Rehabilitation Center
 * by Milvus Group
 */

$(document).ready( function () {

    if ($('#rehabDashboard').length) {
        patientsList();
        $('#patients').on('click',function(ev) {
            ev.preventDefault();
            patientsList();
        });

        checkDuplicateIds();
    }
})

function checkDuplicateIds() {
    $('[id]').each(function(){
        var ids = $('[id="'+this.id+'"]');
        if(ids.length>1 && ids[0]==this)
            console.warn('Multiple IDs #'+this.id);
    });
}

async function showEvents(id, all = false) {

    try {

        let html = '<div class="tabs"><li class="tab pure-menu-item"><a href="" class="pure-menu-link showDetails" data-obm_id="'+id+'">Details</a></li><li class="tab activeTab pure-menu-item"><a href="" class="showEvents pure-menu-link" data-obm_id="'+id+'">Events</a></li></div>'

        html += await eventsTable(id,all);
        html += '<a href="<?= $ref ?>upload/?form=77&type=web&set_fields={%22patient_id%22:'+id+'}" class="addEvent pure-button"> <i class="fa fa-plus"></i> Add event </a>';

        $('#patientDetails').html(html);

        $('.showDetails').on('click', function(ev) {
            ev.preventDefault();
            patientDetails($(this).data('obm_id'));
        });

        $('.showEvents').on('click', function(ev) {
            ev.preventDefault();
            showEvents(id, true);
        });
    }
    catch (error) {
        console.log(error)
    }
}

async function eventsTable(id, all = false) {
    try {
        const data = await $.getJSON("ajax_rehab", {
            obm_id: id,
            action: 'showEvents',
            all: all
        })

        let html = '';
        $.each(data, function(key,row) {
            html += '<div class="row"><div class="col-4 pdKeys">' + row.date + '</div><div class="col-8 pdValues">' + row.event + '</div></div>';
        });

        return html;
    }
    catch (error) {
        console.log(error);
    }
}

async function patientDetails(id) {
    try {

        const tabs = '<div class="tabs"><li class="tab activeTab pure-menu-item"><a href="" class="pure-menu-link showDetails" data-obm_id="'+id+'">Details</a></li><li class="tab pure-menu-item"><a href="" class="showEvents pure-menu-link" data-obm_id="'+id+'">Events</a></li></div>'
        const data = await $.getJSON("ajax_rehab", { action: 'patientDetails', patient_id: id })

        var html = tabs;
        $('#patientDetails').html(html);
        $.each(data, function(key,value) {

            html = '<div class="row"><div class="col-4 pdKeys">' + key + '</div><div class="col-8 pdValues">' + value + '</div></div>';


            $('#patientDetails').append(html);
        });

        html = '<div class="row"><div class="col">';
        html += '<a href="" class="editPatient pure-button" data-obm_id="'+id+'"> <i class="fa fa-edit"></i> Edit </a>';
        html += '</div></div>';
        html += '<h5>Important events</h5>';

        html += await eventsTable(id);
        
        $('#patientDetails').append(html);

        $('.editPatient').on('click',async function(ev) {
            ev.preventDefault();
            const url = await $.post('ajax_rehab',{action:'editPatient',obm_id: id})
            window.open(url,'_blank');
        });

        $('.showDetails').on('click', function(ev) {
            ev.preventDefault();
            patientDetails($(this).data('obm_id'));
        });

        $('.showEvents').off().on('click', function(ev) {
            ev.preventDefault();
            showEvents(id, true);
        });


    }
    catch (error) {
        console.log(error)
    }
}


async function patientsList () {
    try {
        const data = await $.getJSON("ajax_rehab", { action: 'patients-full-list' })

        $('#patientsList').html("");
        let l = data.length;
        let allFed = 0;
        for (var i = 0; i < l; i++) {
            var patient = data[i];
            var classes = '';

            var prev = "<div class='patientNav col-1 prev'><i class='fa fa-angle-left'></i></div>";
            var next = "<div class='patientNav col-1 next'><i class='fa fa-angle-right'></i></div>";

            if (i == 0) { 
                classes = ' pActive';
                prev = "<div class='col-1'></div>";
            } else if (i == l - 1) {
                next = "<div class='col-1'></div>";
            } 

            const addEvent = '<a href="<?= $ref ?>upload/?form=77&type=web&set_fields={%22patient_id%22:'+patient.obm_id+'}" class="addEvent pure-button button-large"> <i class="fa fa-plus"></i></a>';
            let fed = '';
            if (patient.fed === null) 
                fed = "<a target='_blank' class='pure-button button-large feed' href='' data-patient_id="+patient.obm_id+"><i class='fa fa-cutlery'></i></a>";
            else
                allFed++;

            $('#patientsList').append("<li class='row patientSelect"+classes+"'>"+prev+"<div class='pS col-10 col-md-12' data-obm_id='"+patient.obm_id+"'><div class='patientSpecies'>"+patient.obm_id+" - <em>"+patient.species+"</em></div><div><div class='col-1'>"+ addEvent + fed +"</div></div></div>"+next+"</li>" );
        }

        if (allFed === l) {
            $('.feedAll').attr('disabled','disabled');
        }

        patientDetails($('.pActive').children('.pS').data('obm_id'));

        $('.pS').on('click',function(ev) {
            patientDetails($(this).data('obm_id'));
            $('.patientSelect').removeClass('pActive');
            $(this).parent().addClass('pActive');
        });
        $('.patientNav').on('click',function() {
            $('.patientSelect').removeClass('pActive');
            if ($(this).hasClass('next')) {
                var next = $(this).parent().next();
                next.addClass('pActive');
                patientDetails(next.children('.pS').data('obm_id'));
            }
            else {
                var prev = $(this).parent().prev();
                prev.addClass('pActive');
                patientDetails(prev.children('.pS').data('obm_id'));
            }
        });

        $('.feed').on('click', async function (event) {

            event.preventDefault();
            const patient_id = $(this).data('patient_id');

            var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            var now = (new Date(Date.now() - tzoffset)).toISOString().slice(0, 10);
            const mezo = {
                'header': ['patient_id','event','date','comments'],
                'default_data': [patient_id,'str_feeding',now,''],
                'rows':[]
            }

            $.ajax({
                'url': "ajax_rehab", 
                'type': 'POST',
                'data': { action: 'uploadOpenpage', form_id: 79 }, 
                'async': false
            })
                .done(function() {
                    $.post("ajax", {'upload_table_post':JSON.stringify(mezo),'form_page_id': 79,'form_page_type': 'web',"description":"" }, function(data) {
                        if(data=='') 
                            alert('Session expired or other error occured.');
                        else {
                            var retval = jsendp(data);
                            if (retval['status']=='error') {
                                alert( retval['message']);
                            } else if (retval['status']=='fail') {
                                alert( "An error occured while uploading data.");
                            } else if (retval['status']=='success') {
                            }
                        }
                    });
                });

            patientsList();


        });

        $('.feedAll').on('click', function(ev) {
            ev.preventDefault();
            const feed = data.filter(function(patient){
                return patient.fed === null;
            });
            feed.forEach(async function(el) {
                var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
                var now = (new Date(Date.now() - tzoffset)).toISOString().slice(0, 10);
                const mezo = {
                    'header': ['patient_id','event','date','comments'],
                    'default_data': [el.obm_id,'str_feeding',now,''],
                    'rows':[]
                }
                $.ajax({
                    'url': "ajax_rehab", 
                    'type': 'POST',
                    'data': { action: 'uploadOpenpage', form_id: 79 }, 
                    'async': false
                })
                    .done(function() {
                        $.post("ajax", {'upload_table_post':JSON.stringify(mezo),'form_page_id': 79,'form_page_type': 'web',"description":"" }, function(data) {
                            if(data=='') 
                                alert('Session expired or other error occured.');
                            else {
                                var retval = jsendp(data);
                                if (retval['status']=='error') {
                                    alert( retval['message']);
                                } else if (retval['status']=='fail') {
                                    alert( "An error occured while uploading data.");
                                } else if (retval['status']=='success') {
                                }
                            }
                        });
                    });
            });
            patientsList();
        });

    }
    catch (error) {
        console.log(error);
    }
}




function contactTable() {
    $("#contact-table").tabulator({
        groupBy:"status",
        fitColumns:true, //fit columns to width of table (optional)
        columns:[ //Define Table Columns
            {title:"id",field:"id"},
            {title:"Név",field:"name"},
            {title:"Email",field:"email"},
            {title:"Telefonszám",field:"telephone"},
            {title:"Mobil",field:"mobile"},
            {title:"Fax",field:"fax"},
            {title:"Cím",field:"address"},
            {title:"Megye",field:"county"},
            {title:"Település",field:"location"},
            {title:"Irányítószám",field:"postal_code"},
            {title:"Státus",field:"status"},
            {title:"Weblap",field:"website"},
            {title:"Megjegyzések",field:"comments"}
        ]
    });

    var parameters = {
        action: 'contact-table'
    };
    $.getJSON("afuncs.php", parameters)
        .done(function(data, textStatus, jqXHR) {
            // call typeahead's callback with search results (i.e., places)
            $('#contact-table').tabulator("setData", data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
}

function eventsFullTable (patient_id) {

    $("#events-full-table").tabulator({
        columns:[ //Define Table Columns
            {title:"ID", field:"event_id", sorter:"number", width:'50'},
            {title:"Esemény", field:"value", sorter:"string", widht:'150'},
            {title:"Dátum", field:"date", sorter:"date", width:'80'},
            {title:"Fotók", field:"has_photos", sortable:false, width:'50', align:'center'},
            {title:"Megjegyzések", field:"comments", sortable:false, width:'100'},
            {title:"Település", field:"location", sorter:"string", width:'100'},
            {title:"Megye", field:"county", sorter:"string", width:'80'},
            {title:"Megtalálás körölményei", field:"circumstances", sortable:false},
            {title:"Napok száma", field:"days_between", sortable:false},
            {title:"Táplálék", field:"food_given", sortable:false},
            {title:"Tartás körülményei", field:"keeping", sortable:false},
            {title:"Célállomás", field:"destination", sortable:false},
            {title:"Szállítás módja", field:"transport_mode", sortable:false},
            {title:"Diagnózis", field:"diagnosis", sortable:false, sorter:"string"},
            {title:"Kezelés", field:"treatment", sortable:false, sorter:"string"},
            {title:"Behozás oka", field:"brought_in_becau", sortable:false},
            {title:"Sérülés oka", field:"cause_of_injury", sorter:"string", sortable:false},
            {title:"Sérült testtáj", field:"injury_tract", sorter:"string", sortable:false}
        ],
    });

    var parameters = {
        id: patient_id,
        action: 'get-events'
    };
    $.getJSON("afuncs.php", parameters)
        .done(function(data, textStatus, jqXHR) {
            // call typeahead's callback with search results (i.e., places)
            $('#events-full-table').tabulator("setData", data);
            $("a[data-target=#basicModal]").click(function(ev) {
                ev.preventDefault();
                var target = $(this).attr("href");
                // load the url and show modal on success
                $("#basicModal .modal-content").load(target, function() { 
                    $("#basicModal").modal("show"); 
                });
            });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
}














/*************************************************************
 * Form függvények
 *
 ************************************************************/

function addContact () {
    var form = $('#add-new-contact');
    var formData = $(form).serialize();
    $.ajax({
        type: 'POST',
        url: $(form).attr('action'),
        data: formData
    })
        .done(function(response) {
            $('.modal-body').html('<h4>Kapcsolat hozzáadva</h4><button type="button" class="close" data-dismiss="modal" aria-hidden="true">OK</button></br>');
        })
        .fail(function(data) {
            // log error to browser's console
            console.log('error');
        });
}

function extendForm (selected) {
    var parameters = {
        evnt: selected,
        action: 'extendform'
    };
    $.get("afuncs.php", parameters)
        .done(function(data, textStatus, jqXHR) {
            $('#event-details').html(data);
            placesTypeahead();
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
}











/*********************************************************
 * Fotó kezelés
 *
 ***********************************************************/

function deletePhoto(file_path) {
    if (confirm("Biztos törlöd a képet?") == true) {
        var parameters = {
            photo: file_path,
            action: 'delete-photo'
        };
        $.get("afuncs.php", parameters)
            .done(function(data, textStatus, jqXHR) {
                $('#'+file_path.replace('/','_').replace('.','_')).hide();
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                // log error to browser's console
                console.log(errorThrown.toString());
            });
    }
}













/*********************************************************
 * Egyéb függvények
 *
 ************************************************************/


function deleteRecord(patient_id, event_id = null) {
    if (event_id == null) {
        var message = "Biztos törlöd a(z) " + patient_id + ". számú beteget, és a hozzá tartozó eseményeket?";
    }
    else {
        var message = "Biztos törlöd a(z) " + event_id + ". számú eseményt?";
    }
    if (confirm(message) == true) {
        var parameters = {
            pid: patient_id,
            eid: event_id,
            action: 'delete-record'
        };
        $.get("afuncs.php", parameters)
            .done(function(data, textStatus, jqXHR) {
                if (event_id == null) {
                    patientsList();
                }
                /*                else {
                    var target = $('div[data-id='+patient_id+']').parent().parent().parent().attr('id')
                    eventsTable(patient_id,*/
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                // log error to browser's console
                console.log(errorThrown.toString());
            });
    }
}


/**********************************************************
 * Typeahead függvények
 *
 *********************************************************/

function contactsTypeahead () {
    $("#found_by_id").typeahead({
        autoselect: true,
        highlight: true,
        minLength: 2 
    },
        {
            source: searchContacts,
            templates: {
                empty: "nobody matches",
                suggestion: _.template("<p><%- name %></p>")
            }
        });

    $("#found_by_id").on("typeahead:selected", function(eventObject, suggestion) {
        $("#found_by_id").typeahead("val", suggestion.name);
    });
}

function searchContacts(query, cb)
{
    var county = $('#county').val();
    // get places matching query (asynchronously)
    var parameters = {
        name: query,
        action: 'contacts' 
    };
    $.getJSON("afuncs.php", parameters)
        .done(function(data, textStatus, jqXHR) {
            // call typeahead's callback with search results (i.e., places)
            cb(data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
}

function placesTypeahead () {
    $("#location").typeahead({
        autoselect: true,
        highlight: true,
        minLength: 3 
    },
        {
            source: searchPlaces,
            templates: {
                empty: "no locations found yet",
                suggestion: _.template("<p><%- name %>, <%- county %>, <%- postalCode %></p>")
            }
        });

    $("#location").on("typeahead:selected", function(eventObject, suggestion) {
        $("#location").typeahead("val", suggestion.name);
        $("#x").val(suggestion.X);
        $("#y").val(suggestion.Y);
        $("#postal_code").val(suggestion.postalCode);

    });
}

function searchPlaces(query, cb)
{
    var county = $('#county').val();
    // get places matching query (asynchronously)
    var parameters = {
        place: query,
        county: county,
        action: 'places' 
    };
    $.getJSON("afuncs.php", parameters)
        .done(function(data, textStatus, jqXHR) {
            // call typeahead's callback with search results (i.e., places)
            cb(data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
}

function speciesTypeahead () {
    $("#species").typeahead({
        autoselect: true,
        highlight: true,
        minLength: 3
    },
        {
            source: searchSpecies,
            templates: {
                empty: "no species found yet",
                suggestion: _.template("<p><%- scientific_name %>, <%- hungarian_name %>, <%- english_name %></p>")
            }
        });

    $("#species").on("typeahead:selected", function(eventObject, suggestion) {
        $("#species").typeahead("val", suggestion.scientific_name);
    });
}

function searchSpecies(query, cb)
{
    // get places matching query (asynchronously)
    var parameters = {
        species: query,
        action: 'species' 
    };
    $.getJSON("afuncs.php", parameters)
        .done(function(data, textStatus, jqXHR) {
            // call typeahead's callback with search results (i.e., places)
            cb(data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // log error to browser's console
            console.log(errorThrown.toString());
        });
}

</script> 
