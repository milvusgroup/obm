<?php
$_SESSION['page'] = 'home';
class mgrid {
    // `elements`.php should be exists in local includes dir:
    // e.g. includes/sidebar1.php ...
    var $elements = array('header','content1');

    
    // default layout - will be overdefined width grids
    var $definition = '
    <div class="wrapper">
      <div class="box header">#header</div>
      <div class="box content1" style="background-color: #fff; color: #000;">#content1</div>
    </div>';
 
   // minimal width layout
   var $areas = 'header
     sidebar1
     content1
     sidebar2
     sidebar3
     content2
     footer';
   var $gap = "0";

   // min 600px width layout
   var $gap600 = '0';
   var $columns600 = '50% auto';
   var $areas600 = 'header   header
                    content1 content1
                    sidebar1 sidebar2
                    content2 sidebar3
                    content2 content3
                    footer   footer'; 

   // min 900px width layout
   var $gap900 = '0';
   var $columns900 = '34% 33% auto';
   var $areas900 = 'header   header   header
                    content1 content1 content1
                    sidebar2 sidebar2 sidebar1
                    sidebar2 sidebar2 sidebar3
                    content2 content2 sidebar3
                    content2 content2 sidebar3
                    content2 content2 footer'; 

   // min 1400 px width layout
   var $gap1400 = '0';
   var $columns1400 = 'auto 350px 200px auto';
   var $areas1400 = 'header  header  header header
                     content1 content1 content1 content1
                     sidebar1 sidebar2 sidebar3 content2
                     sidebar1 sidebar2 sidebar3 content2
                     sidebar1 footer  footer content2'; 

}
?>

