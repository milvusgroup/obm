<?php
session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'upload_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

$cmd = "SELECT short,long,language FROM project_descriptions WHERE projecttable='".PROJECTTABLE."'";
$res = pg_query($BID,$cmd);
$row = pg_fetch_all($res);
$k = array_search($_SESSION['LANG'], array_column($row, 'language'));
if(!$k) $k = 0;
$OB_project_title = $row[$k]['short'];
$OB_project_description = $row[$k]['short'];

?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="cache-control" content="no-cache" />
<title><?php echo $OB_project_title.' - '.str_upload ?></title>
    <link rel="stylesheet" href="http://<?php echo URL ?>/js/ui/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="http://<?php echo URL ?>/css/pure/pure-min.css" type="text/css" />
    <link rel="stylesheet" href="http://<?php echo URL ?>/css/font-awesome/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="http://<?php echo URL ?>/css/mapsdata.css?rev=<?php echo rev('css/mapsdata.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="http://<?php echo URL ?>/css/style.css?rev=<?php echo rev('css/style.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="http://<?php echo URL ?>/css/uploader.css?rev=<?php echo rev('css/uploader.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="http://<?php echo URL ?>/css/dropit.css?rev=<?php echo rev('css/dropit.css'); ?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="http://<?php echo URL ?>/css/roller.css?rev=<?php echo rev('css/roller.css'); ?>" type="text/css" />
    <script type="text/javascript" src="http://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/functions.js?rev=<?php echo rev('js/functions.js'); ?>"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/uploader.js?rev=<?php echo rev('js/uploader.js'); ?>"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/dropit.js?rev=<?php echo rev('js/dropit.js'); ?>"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/roller.js?rev=<?php echo rev('js/roller.js'); ?>"></script>
    <script type="text/javascript" src="http://<?php echo URL ?>/js/main.js?rev=<?php echo rev('js/main.js'); ?>"></script>
<?php
    include_once('includes/local_main_javascripts.php');
?>
<?php
// Include custom css if exists
if (file_exists('includes/custom.css')) {
    echo '<link rel="stylesheet" href="http://'. URL .'/includes/custom.css?rev='.rev('includes/custom.css').'" type="text/css" />';
}
?>
</head>
<style>
#footer {position: relative;}
.content {
    width: 80%; 
    max-width: 1100px;
    margin: 0 auto;
}
</style>
<body id='upload'>
<div id='holder'>
<?php
    include('header.php.inc');
?>
<div class="content">
<?php if (isset($_GET['clearCookie'])) : ?>
    <script>
    $(document).ready(function() {
        $.removeCookie('systemMessage',{expires: 365, path: '/'});
        console.log($.cookie('systemMessage'));
    });
    </script>
<?php endif; ?>
<?php if ($_SESSION['LANG'] == 'ro') : ?>

<h2>25. aprilie 2018.</h2>
<h3>Noutăți:</h3>
<ul>
	<li>Fotografiile încărcate la observații pot fi vizualizate din Tabelul compact.</li>
	<li>Filtru pentru vizualizarea observațiilor cu fotografii.</li>
	<li>Două tipuri de filtre pentru câmpul Statutul exemplarelor (de ex. coduri de cuibărit, exemplare în migrație activă). Cu ajutorul acestora codurile de cuibărit pot fi filtrate atât grupate cât și unul câte unul.</li>
	<li>Pe pagina de Profil sub punctul Activitate poate fi vizualizată lista de specii încărcată de utilizator, numărul de încărcări la fiecare specie, respectiv un poligon despre arealul de unde provin observațiile utilizatorului.</li>
	<li>Poate fi vizualizată parțial pagina de Profil a celorlalți membri. Acesta este accesibil din Sumar.</li>
	<li>În cazul interogării spațiale cu poligoane preîncărcate poate fi setat, dacă dorim ca rezultatele să conțină toate geometriile intersectate de poligonul utilizat ca filtru (situația de până acum) sau numai geometriile, care sunt complet în interiorul poligonului utilizat ca filtru.</li>

</ul>

<h3>Corecturi și completări:</h3>
<ul>

	<li>Modulul de import Observado: Ordinea coloanelor nu mai trebuie setat de utilizator, acesta este în mod automat selectat la alegerea formularului.</li>
	<li>Alegerea limbii a devenit posibil și cu ajutorul iconițelor din dreapta sus a paginii.</li>
	<li>În filtrul Program-Cod eșantion centralizator-Cod unitate de monitorizare, dacă eșantionul centralizator era aceeași în mai multe programe, au fost listate codurile de unitate din toate aceste programe. Această eroare a fost remediată.</li>
	<li>Câteva schimbări în stil.</li>
</ul>

<h3>Formulare noi:</h3>
<ul>

	<li>A fost readăugat, cu anumite schimbări, un Formular simplu pentru încărcarea ușoară a datelor din observații ocazionale, liste incomplete.</li>
	<li>Formular pentru importarea datelor colectate cu ObsMap în cadrul Programului de monitorizare a păsărilor răpitoare de zi cuibăritoare.</li>
</ul>

<hr>

<h2>06. martie 2018.</h2>
<ul>
<li>După filtrare este posibil ordonarea Tabelului compact și a Tabelului complet pe baza valorilor din coloane. Pentru asta dați click pe coloana dorită în capul de tabel!</li>
<li>Bug fix: Lista speciilor și sumarul interogării include și observațiile încărcate ca sinonimele denumirilor de specii acceptate</li>
<li>Bug fix: funcționează filtrul de date fără an și dacă perioada de filtrare include 31 decembrie și 1 ianuarie.</li>
</ul>


<hr>

<h2>02. februarie 2018.</h2>

<p>A devenit posibil importarea ușoară a datelor din Observation.org (Observado) în OpenBirdMaps. Acest lucru facilitează folosirea aplicației ObsMapp pentru colectarea datelor pe teren. Înainte de a folosi modulul vă rugăm citiți cu atenție instrucțiunile referitoare la colectarea de date și importarea lor: <a href="http://openbirdmaps.ro/wiki/index.php/OpenBirdMaps_Help#Importare_de_date_din_Observation.org_.28Observado.29" target="_blank" rel="help">Importare de date din Observation.org (Observado)</a></p> 

<hr>

<h2>11. decembrie 2017.</h2>


<p>A avut loc un upgrade major al sistemului OpenBirdMaps, care aduce câteva schimbări importante, fundamentale, respectiv au fost corectate câteva greșeli majore. Cele mai relevante schimbări sunt:</p>

<ol>
	<li/> <b>Funcționează încărcarea de fotografii!</b> De fapt, poate fi încărcat orice tip de fișier (de ex. documente, fișiere audio) de dimensiuni relativ mici (max. 10 MB). Folosiți această funcție pentru documentarea observațiilor!
	<li/> <b>A fost eliminată limita de 2000 de date la filtrare,</b> fiind prezentate pe hartă toate observațiile din rezultatul filtrării, oricât de multe ar fi.  
        <li/> Poate fi efectuată <b>filtrarea recursivă</b>, adică după o filtrare, rezultatele pot fi în continuare filtrate. Această funcție este deosebit de utilă dacă doriți să efectuați o <b>filtrare de geometrie combinată cu un filtru de text</b> (de ex dorim să aflăm ce observații are o specie într-o zonă, într-o anumită perioadă). Pentru a folosi filtrarea recursivă activați această opțiune sub hartă.</p>
	<li/> Poate fi vizualizat un <b>sumar al observațiilor dând click pe geometriile de pe hartă</b> (punctele, liniile, poligoanele). Pentru acest lucru folosiți unealta „Identifică” de sub hartă.
	<li/> <b>Rezultatele filtrării sunt sumarizate într-un tabel</b> în secțiunea „Lista speciilor și sumarul interogării”. Tabelul conține lista speciilor, numărul de observații pe specie, respectiv totalul numărului de exemplare pe specie.
	<li/> <b>Sistemul de acces la date</b> a fost reprogramat, au fost eliminate greșeli, precum: utilizatorul nu a putut modifica datele proprii, utilizatorii nu au avut acces la datele care teoretic trebuiau să le fie accesibile sau nu au fost prezentate corect datele cu acces parțial. Acest lucru implică și schimbări în prezentarea datelor parțial accesibile.  Mai multe puteți citi în secțiunea Ajutor.
	<li/> Baza de date OpenBirdMaps va primi un <b>identificator DOI.</b> Adițional, există posibilitatea (deocamdată prin administratori) de cerere a unui identificator DOI și pentru interogările salvate (seturile de date interogate de ex. pentru un articol). Acest sistem va ușura citarea corectă a sursei și a autorilor datelor.
	<li/> <b>Funcționează listele în formularele de încărcare de fișiere</b>. Aceasta ușurează considerabil încărcarea de date din fișiere (cu toate că la acest capitol mai este de lucru).
	<li/> Filtrele tip text nu mai apar în mod automatic pe pagina de filtrare. Filtrele dorite pot fi activate selectându-le din lista filtrelor.
	<li/> Unele formulare au fost reorganizate. Formularele de „Liste incomplete” și „Liste complete” a fost unificate.  Unitățile, care nu implică exemplare vii (cadavre, cuiburi, colonii, diferite urme) pot fi încărcate numai printr-un formular nou.</p>
	<li/> Lista statutului de cuibărit a fost modificat în două locuri:
</ol>
<ul>
    <ul>
        <li/> a fost introdusă o categorie nouă de cuibărit probabil, care a lipsit din lista EBCC, dar considerăm că are locul ei în această listă: <i>B.5.2 – protejarea teritoriului în perioada de cuibărit sau activitate teritorial, caracteristică exclusiv perioadei de cuibărit (darabana ciocănitorilor, zborul nupțial al păsărilor răpitoare de zi)</i>
        <li/> Datorită faptului, că, mai ales la speciile acvatice nidifuge există o diferență uriașă în ceea ce privește semnificația unei observații de pui cu puf, care înseamnă cuibărit sigur, respectiv o observație de juvenil zburător, care adeseori nu înseamnă nimic relevant pentru cuibărit, categoria <i>C.12 – juvenil recent zburat din cuib sau pui cu puf</i> a fost separată în două categorii <i>C.12.1 – juvenil recent zburat din cuib</i> și <i>C.12.2 – pui cu puf (specii nidifuge).</i></p>
    </ul>
</ul>
<br/>

</p> Parțial datorită schimbărilor enumerate mai sus, au fost efectuate schimbări și la secțiunea „Reguli și condiții”. Pentru detalii vă rugăm citiți această secțiune.  Schimbările se referă la:</p>
<ol>
	<li/> Observațiile speciale (specii rare pe plan național sau regional, număr de exemplare foarte ridicat, observația unei specii într-un sezon în care în mod normal nu este prezentă) insuficient documentate chiar și după ce administratorii au cerut acest lucru, pot fi nevalidate. Vă rugăm documentați observațiile de acest gen!
	<li/> Se schimbă citarea datelor descărcate datorită aderării bazei de date la sistemul DOI.
	<li/> Accesibilitatea datelor și prezentarea datelor parțial accesibile s-a schimbat datorită sistemului nou.
	<li/> Fotografiile, înregistrările audio și video atașate observațiilor reprezintă proprietatea utilizatorului, care le-a încărcat și nu poate fi folosite fără acordul lui.
	<li/> Deoarece anumite date cu utilizare restricționată sunt publice și pot fi descărcate de oricine, OpenBirdMaps ține o evidență a descărcărilor (când și cine a descărcat datele), care nu va fi publică.   
</ol>

<?php elseif ($_SESSION['LANG'] =='en') : ?>
<h2>25<sup>th</sup> of April 2018</h2>

<h3>New features:</h3>
<ul>
<li> Photos can be viewed from the Compact table.</li>
<li>Filter for records with photo.</li>
<li>Two types of filters for Status of individuals (breeding codes, in active migration, etc.). With the help of these the breeding status codes can be filtered both one by one or grouped.</li>
<li>The species list and the number of records for each species uploaded by the user, as well as the geographic range of the uploaded records will be available for each user on the Profile page under the Activity point.</li>
<li>The Profile page of other database members will be partially available, it can be accessed from the Summary page.
When using a previously uploaded (custom) polygon for spatial query, it is possible to set, whether the results should contain all the data with geometries intersected by the polygon (the default setting) or only the data, that have their geometries completely within the polygon.</li>
</ul>

<h3>Improvements, bug fixes:</h3>
<ul>
<li>Observado import module: the selection of the Columns’ order template is no longer necessary. It will be automatically selected when the form is chosen.</li>
<li>Language change is now possible also in the upper right corner of the page.</li>
<li>Bug fix of the Program-Grouping code-Sampling unit filter.</li>
<li>Some style improvements.</li>
</ul>

<h3>New upload forms:</h3>
<ul>
<li>A Simple form for fast data upload of incomplete lists form casual observations.</li>
<li>A form for importing observations for the Breeding birds of prey monitoring program recorded in ObsMap.</li>
</ul>
<hr>

<h2>06<sup>th</sup> March 2018</h2>
<ul>
<li>After a query it is possible to order the data according to the column values in the Compact and Full tables. To do this click on the column header!</li>
<li>Bug fix: The list of species and summary of the query includes now the observations of a species uploaded with a synonym of the accepted name.</li>
<li>Bug fix: the date range (without year) filter works correctly also if the queried period contains December 31st and January 1st.</li>
</ul>

<hr>

<h2>02<sup>nd</sup> February 2018</h2>

<p>A new module is available, that helps easy data import from Observation.org (Observado) to OpenBirdMaps. This tool facilitates the use of ObsMapp application for data collection in the field. Before use please read the instructions for data collection and import: <a href="http://openbirdmaps.ro/wiki/index.php/OpenBirdMaps_Help#Importare_de_date_din_Observation.org_.28Observado.29" target="_blank" rel="help">Importare de date din Observation.org (Observado)</a></p> 

<hr>

<h2>11<sup>th</sup> December 2017.</h2>

<p>Available in <a href="/whatsnew.php?lang=ro">Romanian</a> and <a href="/whatsnew.php?lang=hu">Hungarian</a> languages.</p>

<?php else : ?>
<h2>2018. április 25.</h2>
<h3>Újdonságok:</h3>
<ul>
	<li>A megfigyelések mellé feltöltött fényképek megtekinthetőek a Kompakt táblázatból.</li>
	<li>Szűrő a fényképeket tartalmazó adatokra.</li>
	<li>Két típusú szűrő az Egyedek státusára (pl. költési kódok, vonuló egyedek). Ezek segítségével a költési kódok egyenként és csoportosítva is szűrhetőek.</li>
	<li>A Profil oldalon az Aktivitás pont alatt megtekinthető a felhasználó által feltöltött fajok listája, fajonként a feltöltések száma, illetve egy poligon a felhasználó által feltöltött adatok földrajzi elterjedéséről.</li>
	<li>Részben megtekinthetővé vált az adatbázis tagjainak a Profil oldala. Ez az Összefoglalóból érhető el.</li>
	<li>Az Előre feltöltött poligonokkal való szűrés esetén beállítható, hogy a poligon által metszett összes adatot mutassa-e meg (eddig ez történt) vagy csak teljesen a poligon belsejében levő adatokat listázza ki.</li>
</ul>
<h3>Javítások, kiegészítések:</h3>
<ul>
	<li>Observado modul: az Oszlop sorrend sablon automatikus kapcsolása a megfelelő ívhez (nem kell többet külön kiválasztani)</li>
	<li>A nyelvválasztás lehetségessé vált a zászló ikonok segítségével a jobb felső sarokban is.</li>
	<li>A Program-Gyűjtőkód-Egységkód szűrőben több azonos gyűjtőkóddal rendelkező program egységkódjait is kilistázta, ez most javításra került.</li>
	<li>Néhány stílusbeli javítás</li>
</ul>

<h3>Új adatfelviteli ívek:</h3>
<ul>
	<li>Visszakerült, kissé módosítva, az Egyszerű adatfelviteli ív, mely az alkalmi megfigyelések felvitelét hivatott megkönnyíteni. Ez részleges fajlisták böngészőből történő felvitelére alkalmas.
	<li>Egy új ív a Fészkelő ragadozómadár monitoring program keretében ObsMap-be jegyzetelt megfigyeléseknek.
</ul>

<hr>

<h2>2018. március 6.</h2>
<ul>
<li>A lekérdezések eredményeit bemutató Kompakt és Teljes táblázatok rendezhetőek az oszlopok értékei alapján. Ehhez kattintson az oszlop fejlécére!</li>
<li>Hiba javítás: A lekérdezésben előforduló fajok listája és adatok összesítése most már tartalmazza az elfogadott fajnevek szinonimájaként feltöltött adatokat is.</li>
<li>Hiba javítás: működik az év nélküli dátum filter akkor is, ha a kiválasztott periódus tartalmazza december 31.-ét és január 1.-ét.</li>
</ul>

<hr>

<h2>2018. február 2.</h2>


<p>Elkészült az Observation.org (Observado) portálból az OpenBirdMapsbe történő adatimportálást segítő modul. Ez lehetőséget nyújt az ObsMapp aplikáció segítségével gyűjtött adatok egyszerű feltöltésére. A modul használata előtt kérjük figyelmesen olvassa el az adatgyűjtésre és importálásra vonatkozó útmutatót: <a href="http://openbirdmaps.ro/wiki/index.php/OpenBirdMaps_Help#Importare_de_date_din_Observation.org_.28Observado.29" target="_blank" rel="help">Importare de date din Observation.org (Observado)</a></p>

<hr>

<h2>2017. december 11.</h2>

<p>Az OpenBirdMaps adatbázis mostantól az OpenBioMaps 2.2 verzióját használja, melyben számos fontos hiba javításra került illetve számos újdonság vált elérhetővé. A legfontosabb újítások:</p>
<ol>
    <li/> <b>Működik a fénykép feltöltés!</b> Valójában bármilyen, relatíve kis méretű (max. 10 MB) file csatolható az adatokhoz (pl. dokumentumok, audio file-ok). Használják ezt a funkciót a megfigyeléseik megfelelő dokumentálására!
    <li/> <b>A lekérdezések eredményeinek a bemutatásakor megszűnt a 2000 adatos határ.</b> Most már bármennyi adat megjeleníthető a térképen.  
    <li/> Lehetséges <b>rekurzív lekérdezéseket</b> végezni, azaz egy lekérdezés után az eremények tovább szűkíthetőek egy újabb lekérdezéssel. Ez rendkívül hasznos, ha egy <b>geometria típusú lekérdezést szeretnénk szöveges lekérdezéssel kombinálni</b> (erre eddig csak nagyon kis területeken volt lehetőség). Például kiváncsiak vagyunk, hogy egy adott területen milyen megfigyelései vannak egy adott fajnak egy adott időszakból. A rekurzív szűrő a térkép alatti kapcsolóval aktiválható.</p>
    <li/> A térkép alatt elhelyezett "Azonosít" eszközzel <b>a térképen levő pontokra, vonalakra, poligonokra kattintva megtekinthetőek bizonyos részletek az illető megfigyelésről.</b>
    <li/> <b>A lekérdezés eredményéről készül egy összesítő táblázat</b>  az „ A lekérdezésben előforduló fajok listája és adatok összesítése” részben. A táblázat tartalmazza a lekérdezéseben előforduló fajok listáját, illetve minden fajra a megfigyelések számát és az össz egyedszámot.
    <li/> Újra lett írva <b>az adatokhoz való hozzáférés rendszere</b>. Ezáltal megszűntek a következő hibák: a felhasználó nem tudta a saját adatait módosítani, nem volt hozzáférése olyan adatokhoz, amihez hozzáférése kellett volna legyen, a részleges hozzáférésű adatok helytelenül jelentek meg. Ezáltal változott a részleges hozzáférésű adatok bemutatásának módja is (részletek a Segítségben).
    <li/> Az OpenBirdMaps adatbázis <b>DOI azonosítót</b> fog kapni. Továbbá lehetőség lesz - egyelőre az adminisztrátorokon keresztül - DOI azonosító igénylésére az adatbázisban elmentett lekérdezésekre is (azaz egy lekérdezett adatszettre pl. egy tudományos publikációhoz). Ez a rendszer biztosítja az adatok és az adat tulajdonosok megfelelő idézését.
    <li/> <b>Működnek a listák a file feltöltő formokban</b>. Ez jelentősen megkönnyíti a file feltöltést.
    <li/> A szöveges szűrők nem jelennek meg automatikusan a Térkép oldal jobb oldalán, csak a listájuk látható. Ebből kell kiválasztani a kívánt szűrőt, mely kiválasztáskor automatikusan aktiválódik.
    <li/> Egyes formokat jelentősen átalakítottunk. A „Részleges lista” és „Teljes lista” formokat egysítettük. A nem élő egyedekre vonatkozó adatokat (tetemek, fészkek, telepek, különböző nyomok) egy új formból lehet feltölteni.</p>
    <li/> A költési kódokat két esetben módosítottuk:
</ol>
<ul>
    <ul>
        <li/> hozzáadtunk a listához egy új valószínű költésre utaló kódot, mely az EBCC listájából hiányzott, de úgy gondoljuk, hogy itt a helye: <i>B.5.2 – terület védelmezés költési időben vagy kizárólag költési időre jellemző terület jelölés (pl harkályok dobolása, ragadozómadarak nászrepülése)</i>
        <li/> A <i>C.12 – frissen kirepült fiatal vagy pelyhes fióka</i> kód esetén, főleg a fészekhagyó vízimadarak esetében jelentős különbség van aközött, hogy pelyhes fiókát észlel a megfigyelő, ami biztos költést jelent, vagy frissen kirepült röpképes juvenilis egyedeket, ami gyakran nem jelent semmit ami a területen való fészkelést illeti. Emiatt ezt a kódot helyesebbnek éreztük ketté választani: <i>C.12.1 – frissen kirepült fiatal</i> és <i>C.12.2 – pelyhes fióka (fészekhagyó fajok).</i></p>
    </ul>
</ul>
<br/>
</p> Főleg a fenti változások miatt a <b>„Szabályok és feltételek”</b> rész is módosul, kérjük olvassa újra. A fontosabb változások összefoglalva:</p>
<ol>
    <li/> Az üzemeltetők/adat ellenőrök kérésére is elégtelenül dokumentált kivételes megfigyelések (országos vagy regionális szinten ritka fajok, nagyon magas egyedszám, olyan évszakban történő megfigyelés, amikor az illető faj normálisan nem tartózkodik az illető régióban) vagy hibás adatok státusát az üzemeltetők megváltoztathatják "el nem fogadott adatra". Kérjük dokumentálja fényképpel vagy legalább egy leírással az ilyen jellegű megfigyeléseit!
    <li/> Mivel az adatbázis DOI azonosítót kap és lehetőség nyílik meghatározott adatszettek esetében is a DOI azonosító igénylésére, változik az adatok idézésének a módja is. 
    <li/> Az új rendszer miatt változik a részlegesen hozzáférhető adatok megjelenítése.
    <li/> Az adatokhoz feltöltött fényképek, hang- és videofelvételek a szerzők tulajdonát képezik. Nem használhatóak a szerzők előzetes beleegyezése nélkül.
    <li/> Mivel olyan adatszettek is teljesen publikálásra kerülnek és bárki által letölthetőek, melyeknek a használata korlátozott, az OpenBirdMaps nyilvántartja a letöltéseket (mikor és ki töltötte le az illető adatokat). Ezek az információk nem publikusak.   
</ol>
<?php endif; ?>
</div>
<?php
    include('footer.php.inc');
?>
</div><!--/holder-->
</body>
</html>
