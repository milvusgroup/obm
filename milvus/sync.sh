#!/bin/bash

while inotifywait -r -e modify ./local; do
    rsync -avz ./local/includes/modules/ app:obm/obm-composer/local/milvus/local/includes/modules/
    rsync -avz ./local/styles/ app:obm/obm-composer/local/milvus/local/styles/
done