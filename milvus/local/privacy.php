<style>
</style>
<?php if ($_SESSION['LANG'] === 'en'): ?>
    <div class="container">
        <p>Available in <a href="?lang=ro">Romanian</a> and <a href="?lang=hu">Hungarian</a> languages</p>
    </div>
<?php elseif ($_SESSION['LANG'] === 'hu'): ?>

<div class="container">

    <h3>Az OpenBirdMaps adatkezelési tájékoztatója</h3>

    <h4>1. Az OpenBirdMaps adatbázis üzemeltetője:</h4>

    <p>
    Milvus Csoport Egyesület (Asociația Grupul Milvus)<br/>
    Munkapont címe: str. Márton Áron nr. 9/B, 540058 Târgu Mureș<br/>
    Hivatalos cím: str. Crinului nr.22, 540343 Târgu Mureş<br/>
    Tel/Fax: +40 265 264726<br/>
    e-mail (Milvus Csoport): <a class="western" href="mailto:office@milvus.ro">office@milvus.ro</a><br/>
    e-mail (OpenBirdMaps): <a class="western" href="mailto:openbirdmaps@milvus.ro">openbirdmaps@milvus.ro</a><br/>
    </p>

    <h4>2. Meghatározások</h4>

    <ul>
        <li><strong>személyes adat:</strong>azonosított vagy azonosítható természetes
            személyre (&quot;érintett&quot;) vonatkozó bármely információ;
            azonosítható az a természetes személy, aki közvetlen vagy
            közvetett módon, különösen valamely azonosító, például név,
            szám, helymeghatározó adat, online azonosító vagy a természetes
            személy testi, fiziológiai, genetikai, szellemi, gazdasági,
            kulturális vagy szociális azonosságára vonatkozó egy vagy több
            tényező alapján azonosítható; 
        </li>
        <li><strong>adatkezelés:</strong> a személyes adatokon vagy adatállományokon
            automatizált vagy nem automatizált módon végzett bármely művelet
            vagy műveletek összessége, így a gyűjtés, rögzítés,
            rendszerezés, tagolás, tárolás, átalakítás vagy
            megváltoztatás, lekérdezés, betekintés, felhasználás, közlés
            továbbítás, terjesztés vagy egyéb módon történő
            hozzáférhetővé tétel útján, összehangolás vagy
            összekapcsolás, korlátozás, törlés, illetve megsemmisítés;</li>
        <li><strong>üzemeltető:</strong> A Milvus Csoport Egyesület és az általa megjelölt
            természetes személyek, akik az adatbázis adminisztrációjáért
            felelősek (adminisztrátorok). Az OpenBirdMaps adminisztrátorok
            neve és elérhetősége az Összefoglaló lapján nyilvánosan
            elérhető.</li>
        <li><strong>OpenBioMaps:</strong> Az OpenBirdMaps adatbázis működésének alapjául
            az OpenBioMaps szoftver szolgál.</li>
        <li><strong>OpenBioMaps Konzorcium:</strong> Az OpenBirdMapset üzemeltető Milvus
            Csoport Egyesület tagja az OpenBioMaps konzorciumnak, melynek
            elsődleges célja az OpenBioMaps közösségi adatbázis
            keretrendszer fejlesztése és üzemeltetése. Ugyanakkor a
            konzorciumi tagok szerverei fontos adatbiztonsági szerepet is
            betöltenek.</li>
        <li><strong>szerver:</strong> Az OpenBioMaps Konzorcium tagszervezetei által
            üzemeltetett szerverek.</li>
        <li><strong>felhasználó:</strong> Azok a regisztrált természetes vagy jogi
            személyek, akik, miután más regisztrált felhasználótól
            meghívást kaptak az OpenBirdMaps használatára, elfogadják a
            felhasználási feltételeket és felhasználói azonosítóval
            bejelentkezve használják az OpenBirdMaps szolgáltatásait.</li>
        <li><strong>látogató:</strong> Az OpenBirdMaps webes szolgáltatásait használó nem
            bejelentkezett felhasználók.</li>
        <li><strong>névjegy adatok:</strong> a felhasználó által kötelezően vagy
            opcionálisan megadott személyes adatok, melyek a felhasználó
            egyedi azonosítását segítik elő. A kötelező adatok a
            felhasználó neve és e-mail címe, az opcionálisak pedig a
            felhasználó munkahelye, a szervezet, amihez tartozik, a címe és
            külső webes hivatkozások (pl. ResearchGate, OrcID).</li>
        <li><strong>profil menü:</strong> névjegy adatokat kezelő felület.</li>
        <li><strong>naplófájl:</strong> Az OpenBirdMaps weboldalakat kiszolgáló webszerver
            automatikus naplófájljai, amik a webes kérések forrását,
            eszközét és hivatkozását tárolják. A naplófájlok
            automatikusan archiválva vannak 1 évig.</li>
        <li><strong>biotikai adatok:</strong> A madár előfordulások megfigyelési adatai
            vagy más, a madarak elterjedéséhez kapcsolódó természetvédelmi,
            biológiai kutatási vagy oktatási célú adatok. Ezen adatok
            gyűjtése és bemutatása az OpenBirdMaps adatbázis elsődleges
            célja.</li>
    </ul>

    <h4>3. Az adatkezelés jogalapja</h4>

    <p>Az Európai Parlament és a Tanács (EU) 2016/679 rendelete (2016.
    április 27.) a természetes személyeknek a személyes adatok
    kezelése tekintetében történő védelméről és az ilyen adatok
    szabad áramlásáról, valamint a 95/46/EK rendelet hatályon kívül
    helyezéséről (általános adatvédelmi rendelet).</p>

    <h4>4. Az OpenBirdMaps által kezelt adatok köre</h4>

    <p>Adatkezelés a felhasználók önkéntes, tájékozott, és
    kifejezett hozzájárulása alapján az OpenBirdMaps szerverek
    honlapjain regisztrált felhasználók esetében történik a
    adatbázisokon való regisztráció, adatfeltöltés, véleményezés
    és adatmódosítás során.</p>
    <ul>
        <li>
            A felhasználó által a regisztráció alkalmával
            kötelezően megadott adatok a felhasználó neve és az e-mail
            címe. A felhasználó neve a többi felhasználó számára
            megismerhető. A felhasználó e-mail címét alap esetben csak a
            projekt üzemeltetők látják, de a felhasználó a saját döntése
            alapján (opcionálisan) láthatóvá teheti a többi felhasználó
            számára is.</li>
        <li>
            A regisztrációkor automatikusan rögzítésre kerül a
            regisztráció időpontja és a meghívó személy belső numerikus
            azonosítója. Ezt az adatot csak az adatbázis üzemeltetők
            láthatják.</li>
        <li>
            A felhasználó döntése alapján a saját profil menüben
            megadott adatok a munkahelye neve, a szervezet, amihez tartozik, a
            címe és külső webes hivatkozások (pl. ResearchGate, OrcID).
            Ezen adatokat csak a többi bejelentkezett felhasználó ismerheti.</li>
        <li>
            Az OpenBirdMaps látogatása során rögzítésre kerül a
            látogató számítógépének IP-címe, a látogatás kezdő
            időpontja, illetve egyes esetekben – a felhasználó
            számítógépének beállításától függően – a böngésző
            és az operációs rendszer típusa. Rögzítésre kerülnek továbbá
            a megfigyelések részleteinek megtekintése és ennek az időpontja,
            az adatletöltések és ezek időpontja. Ezen adatokat a rendszer
            automatikusan naplózza a naplófájlban és csak a projekt
            üzemeltetők számára elérhető.</li>
        <li>
            A felhasználók által feltöltött biotikai adatoknak is
            van egy személyes adat komponense, ugyanis az adatot gyűjtő
            (megfigyelő) és az adatot feltöltő személy neve rögzítésre
            kerül az adatok többi komponenséhez (pl hely, dátum) kapcsolva.
            Ezen adatok nyilvánosak lesznek, vagy bizonyos esetekben a
            részlegesen nyilvánosak.</li>
    </ul>

    <h4>5. Az adatkezelés célja</h4>

    <ul>
        <li>
            A biotikai adatok gyűjtése és nyilvános bemutatása az
            OpenBirdMaps elsődleges célja. Az OpenBirdMaps egy on-line
            felületként szolgál a madár megfigyelések és az ehhez
            kapcsolódó biotikus adatok tudományos leközléséhez. A legtöbb
            biotikus adat teljesen vagy részlegesen nyilvános lesz, a
            személyes adat komponenst is beleértve.</li>
        <li>
            A regisztrált felhasználó által kötelezően megadott
            adatok esetében az adatok kezelésének célja a felhasználó
            jogosultságának azonosítása (biotikai adat feltöltés, biotikai
            adat módosítás és biotikai adat hozzáférés), valamint a
            felhasználói aktivitás minőségének értékelése a többi
            felhasználó által. Az adatokat az üzemeltető felhasználhatja
            arra, hogy a jogsértő tartalmak esetén a felhasználót
            felszólítsa a feltöltött jogsértő tartalmak eltávolítására.</li>
        <li>
            A felhasználó által a névjegyben önkéntes döntés
            alapján megadott adatok a belső internetes közösség alkotására,
            a felhasználók azonosítására vagy megkülönböztetésére
            szolgálnak.</li>
        <li>
            Az OpenBirdMaps használata során a naplófájlban
            automatikusan rögzítésre kerülő adatok esetében az adatok
            tárolása, felhasználása elsősorban technikai célokra
            (szerverek biztonságos működésének elemzésére, utólagos
            ellenőrzésre) történik a szerver üzemeltetők által.
            Ugyanakkor bizonyos adatokat az üzemeltetők felhasználhatnak a
            szabályokat megszegő felhasználók tetten érésére (pl. az
            adatok felhasználási feltételeinek be nem tartása esetén). Az
            így nyert adatsorokat más forrásból származó, személyes
            azonosításra alkalmas információkkal nem kapcsolja össze az
            üzemeltető, másoknak nem továbbítja. 
        </li>
        <li>
            Az anonim látogató azonosítása nem történik meg. A
            naplózott IP címek önmagában semmilyen módon nem képesek a
            látogatót azonosítani, csak a látogató gépének felismerésére
            alkalmas. Az adatkezelő az ilyen, a fent említett, személyes
            adatot nem tartalmazó anonim azonosítókat nem kapcsolja össze
            semmilyen módon más személyes adatokkal az anonim látogató
            vonatkozásában.</li>
        <li>
            Az adatkezelő a megjelölt céloktól eltérő célra a
            személyes adatokat nem használja, személyes adatokat harmadik
            személyeknek nem ad át. Ez nem vonatkozik az esetleges, törvény
            alapján kötelező adattovábbításokra.</li>
    </ul>

    <h4>6. Az adatkezelés időtartama</h4>

    <ul>
        <li>
            A felhasználó által kötelezően megadott és a
            használat során keletkezett kapcsolódó adatok esetében: amíg
            érvényes regisztrációja van az OpenBirdMaps adatbázisban.</li>
        <li>
            A felhasználó döntése alapján a névjegyben megadott
            adatok esetében: amíg a felhasználó nem törli vagy amíg a
            felhasználónak érvényes regisztrációja van az OpenBirdMaps
            adatbázisnak.</li>
        <li>
            Az OpenBirdMaps portálok használata során a naplófájlban
            rögzítésre kerülő adatok esetében: a látogatástól számított
            1 évig tárolja a rendszer az adatokat.</li>
        <li>
            A biotikai adatok személyes komponense, azaz a megfigyelő
            és a felhasználó neve végérvényesen a biotikai adat része
            marad, kivéve olyan esetekben, amikor az adminisztrátorok
            indokoltnak tartják ezek törlését.</li>
    </ul>

    <h4>7. A személyes adatok módosítása, törlése</h4>

    <p>A felhasználó jelszava megadásával bármikor módosíthatja a
    megadott nevét és e-mail címét, illetve a többi önkéntesen
    megadott névjegy adatokat. 
    </p>

    <p>A felhasználó bármikor törölheti a felhasználói fiókját
    vagy kérheti az adatbázis üzemeltetőjétől a felhasználói
    fiókja törlését. A felhasználói fiók törlésével együtt
    törlődnek a névjegy adatai az OpenBirdMaps felhasználói
    adatbázisból.</p>

    <p>A felhasználó által feltöltött biotikai adatok személyes
    adat komponense, azaz a megfigyelők neve és a felhasználó neve,
    nem törlődik a felhasználói fiók törlésével. Ezen adatok
    törlését kizárólag az adminisztrátorok tudják végrehajtani
    olyan ritka esetekben, amikor ezt indokoltnak érzik. A biotikus
    adatokat a felhasználó tudatosan és önkéntesen tudományos
    publikációként töltötte fel az OpenBirdMaps adatbázisba, ezért,
    hasonlóan a nyomtatott formában leközölt adatokhoz, az adatok
    törlésére ebben az esetben sem kerülhet sor. 
    </p>

    <h4>8. Adatbiztonsági intézkedések</h4>

    <p>Az adatkezelő biztonsági okokból a biotikai és a személyes
    adatokat is az OpenBioMaps titkosított privát hálózatán
    egymáshoz kapcsolódó, az OpenBioMaps Konzorcium tagjai által
    saját telephelyeiken üzemeltetett szerverek egy részén is
    tárolja. A szerverek kezelésére a konzorciumi tagok saját
    szerverfenntartási szabályozásai vonatkoznak. Jelszavas
    hozzáférése csak a Konzorcium által kijelölt rendszergazdáknak
    van. Az OpenBirdMaps szerverről történő személyes adattörlés
    maga után vonja a többi szerverről történő adattörlést is.</p>

    <p>Az OpenBioMaps Konzorcium kezelésében álló más szerverek
    nevei és üzemeltetőik:</p>

    <ol>
        <li>
            <p>http://openbiomaps.org<br/>
            Szerver üzemeltető: Debreceni
            Egyetem Informatikai Szolgáltató Központ<br/>
            Elérhetősége:
            H-4032 Debrecen, Egyetem tér 1., e-posta címe:
            helpdesk@it.unideb.hu</p></li>
        <li>
            <p>http://dinpi.openbiomaps.org<br/>
            Szerver üzemeltető:
            Duna-Ipoly Nemzeti Park Igazgatóság<br/>
            Elérhetősége:
            Duna-Ipoly Nemzeti Park Igazgatóság, H-1121 Budapest, Költő u.
            21., e-posta címe: bercess@dinpi.hu</p></li>
    </ol>

    <h4>9. Az adatkezelési szabályzat módosításának lehetősége</h4>

    <p>Az adatkezelő fenntartja a jogot, hogy jelen adatkezelési
    szabályzatot a felhasználók előzetes értesítése mellett
    egyoldalúan módosítsa. A felhasználó a módosítás
    hatálybalépését követő első belépéssel elfogadja a
    módosított adatkezelési szabályzatot.</p>

    <hr>

    <p>Utolsó frissítés: 2018. június 29.</p>

</div>
<?php else: ?>
    
<div class="container">

    <h3>Regulamentul privind protecția datelor personale în OpenBirdMaps</h3>
    <h4>1. Operatorul bazei de date OpenBirdMaps:</h4>
    <p>
    Asociația Grupul Milvus</br>
    Punct de lucru: str. Márton Áron nr. 9/B, 540058 Târgu Mureș</br>
    Sediu: str. Crinului nr.  22, 540343 Târgu Mureş</br>
    Tel/Fax: +40 265 264726</br>
    E-mail (organizație): office@milvus.ro</br>
    E-mail (OpenBirdMaps): openbirdmaps@milvus.ro</br>
    </p>
    <h4>2. Definiții</h4>
    <ul>
        <li><strong>„Date cu Caracter Personal”</strong>
            înseamnă orice informații privind o persoană fizică identificată
            sau identificabilă; o persoană identificabilă este o persoană
            care poate fi identificată, direct sau indirect, în special prin
            referire la un element de identificare, cum ar fi un nume, un număr
            de identificare, date de localizare, un identificator online sau unul
            sau mai multe elemente specifice, proprii identității sale fizice,
            fiziologice, genetice, psihice, economice, culturale sau sociale.</li>
        <li><strong>„Prelucrare”</strong> (și
            derivatele sale) înseamnă orice operațiune sau set de operațiuni
            efectuate asupra Datelor cu Caracter Personal, incluzând, fără
            limitare, colectarea, înregistrarea, păstrarea, modificarea,
            utilizarea, dezvăluirea, accesul, transferul sau distrugerea
            acestora.</li>
        <li><strong>„Operatorul”</strong> este
            Asociația Grupul Milvus, respectiv persoanele fizice desemnate de
            aceasta, responsabile pentru administrarea bazei de date OpenBirdMaps
            (administratori). Numele și detaliile de contact ale
            administratorilor sunt publicate pe pagina de Sumar în OpenBirdMaps.</li>
        <li><strong>„OpenBioMaps”</strong>
            este software-ul care stă la baza funcționării bazei de date
            OpenBirdMaps.</li>
        <li><strong>„Consorțiul OpenBioMaps”</strong>:
            Asociația Grupul Milvus, operatorul bazei de date OpenBirdMaps,
            este membrul consorțiului OpenBioMaps. Scopul principal al acesteia
            este dezvoltarea și susținerea software-ului OpenBioMaps, dar
            serverele membrilor din consorțiu au și un rol important de
            siguranță a datelor încărcate în bazele de date operate de
            membrii consorțiului.</li>
        <li>Prin <strong>„servere”</strong>
            se vor înțelege serverele operate de membrii consorțiului
            OpenBioMaps.</li>
        <li><strong>„Utilizatorii”</strong>
            sunt acele persoane fizice sau juridice, care primesc invitație de
            accedere la baza de date OpenBirdMaps de la o persoană înregistrată,
            acceptă Termenele și Condițiile de utilizare și folosește baza
            de date după intrare (logare).</li>
        <li><strong>„Vizitatorii”</strong>
            sunt persoanele, care folosesc suprafața de web OpenBirdMaps fără
            intrare (logare).</li>
        <li><strong>„Datele de identificare utilizator”</strong>
            sunt acele date obligatorii sau opționale furnizate din propria
            inițiativă de utilizator, care sunt necesare sau ajută la
            identificarea utilizatorului. Datele de identificare obligatorii sunt
            numele și adresa de e-mail ale utilizatorului. Datele opționale
            includ locul de muncă, organizația de care aparține, adresa sau
            referințe externe de web (de ex. ResearchGate, OrcID).</li>
        <li><strong>„Meniu Profil”</strong>
            este suprafața unde pot fi administrate datele cu caracter personal.</li>
        <li><strong>„Fișierele log”</strong> sunt fișierele create în mod automat de
            serverul web al paginilor web OpenBirdMaps, care includ sursa,
            instrumentul și referințele interogărilor web.</li>
        <li>Prin <strong>„datele
                biotice”</strong> se înțeleg datele de
            distribuție a păsărilor respectiv alte date de cercetare
            biologică, de protecția naturii și de educație ecologică legate
            de datele de distribuție. Colectarea și prezentarea acestor date
            constituie principalul scop al funcționării bazei de date
            OpenBirdMaps.</li>
    </ul>
    <h4>3. Referințe la legislația în vigoare:</h4>
    <p>Regulamentul (UE) 2016/679 al Parlamentului European și al
    Consiliului din 27 aprilie 2016 privind protecția persoanelor fizice
    în ceea ce privește prelucrarea datelor cu caracter personal și
    privind libera circulație a acestor date și de abrogare a
    Directivei 95/46/CE (Regulamentul general privind protecția datelor)</p>
    <h4>4. Date personale prelucrate de OpenBirdMaps</h4>
    
    <p>Datele prelucrate de OpenBirdMaps sunt oferite în mod voluntar și
        conștient de utilizatori prin înregistrarea la OpenBirdMaps,
        încărcarea și modificarea de date în OpenBirdMaps, respectiv prin
        comentarii (toate datele personale cu excepția fișierelor log), sau
        sunt obținute cu acordul acestora (fișiere log).</p>
    <ul>
        <li>Adresa de email și numele sunt datele care trebuie furnizate în
            mod obligatoriu de utilizator în cursul înregistrării în
            OpenBirdMaps. Numele utilizatorului este vizibil pentru ceilalți
            utilizatori. Adresa de email este vizibilă numai administratorilor
            în mod implicit, dar utilizatorul are opțiunea de a o face vizibilă
            și celorlalți utilizatori.</li>
        <li>Odată cu înregistrarea unui utilizator în baza de date, se
            înregistrează în mod automat timpul exact al înregistrării și
            îi este atribuit un număr de identificare intern. Aceste date sunt
            accesibile numai administratorilor bazei de date.</li>
        <li>Utilizatorul are opțiunea de a oferi și alte date de
            identificare în meniul profil, precum locul de muncă, organizația
            de care aparține, adresa sau referințe externe de web (de ex.
            ResearchGate, OrcID). Aceste date vor fi vizibile celorlalți
            utilizatori. 
        </li>
        <li>În cursul vizitării bazei de date OpenBirdMaps vor fi
            înregistrate în mod automat adresa de IP, timpul începerii
            vizitei, respectiv – conform setărilor de pe calculatorul
            utilizatorului – poate fi înregistrat browser-ul, respectiv
            sistemul de operare folosit. Sunt înregistrate totodată toate
            cazurile de vizualizare a datelor, descărcarea datelor, împreună
            cu timpul acestora. Aceste date sunt vizibile numai
            administratorilor.</li>
        <li>Datele biotice încărcate de utilizatori au și ele un component
            care se încadrează în rândul datelor cu caracter personal: pe de
            o parte utilizatorul are obligația de a indica numele celui care a
            colectat informația respectivă (observatorul), pe de altă parte
            numele utilizatorului care a încărcat observația va fi legată de
            observație. În ambele cazuri numele este legat de alte componente
            ale observației, ca locul sau data (timpul) observației. Aceste
            date vor fi publice sau în anumite cazuri parțial accesibile.</li>
    </ul>

    <h4>5. Scopul prelucrării datelor</h4>

    <ul>
        <li>Colectarea și prezentarea datelor biotice constituie scopul
            principal al bazei de date OpenBirdMaps. OpenBirdMaps funcționează
            ca o suprafață on-line de publicare științifică a observațiilor
            de păsări și alte date biotice legate de acesta. Majoritatea
            datelor încărcate vor fi publice sau parțial publice, inclusiv
            componentul personal.</li>
        <li>Datele furnizate în mod obligatoriu de utilizatori (numele și
            adresa de e-mail) au rolul de a identifica în mod individual
            utilizatorii, de a permite/limita accesul (încărcare, vizualizare
            și editare date) la date, respectiv de a permite celorlalți
            utilizatori evaluarea activității utilizatorului. Aceste date pot
            fi folosite de administratori pentru sancționarea utilizatorilor în
            cazul încălcării regulamentului și condițiilor de utilizare ale
            bazei de date OpenBirdMaps.</li>
        <li>Datele opționale date de utilizatori în meniul profil au rolul
            de formare a unei comunități online sau/și de identificare a
            utilizatorilor.</li>
        <li>Datele colectate în mod automatizat în cursul utilizării
            OpenBirdMaps în fișierele log sunt utilizate în primul rând în
            scopuri tehnice (evaluarea funcționării și siguranței serverelor,
            verificări ulterioare a funcțiilor) de administratori. Totodată,
            anumite date din fișierele log pot fi utilizate de administratori
            pentru detectarea încălcării anumitor reguli de utilizatori.
            Datele din fișierele log nu vor fi combinate cu alte date cu
            caracter personal obținute din surse exterioare și nu vor fi
            predate altor entități.</li>
        <li>Vizitatorii anonimi nu sunt identificați. Adresele IP
            înregistrate nu pot fi folosite pentru identificarea vizitatorului.
            Administratorii nu leagă aceste date anonime cu alte date cu
            caracter personal prin care ar fi posibilă identificarea
            vizitatorilor.</li>
        <li>Operatorul nu va folosi datele personale pentru alte scopuri decât
            cele enumerate mai sus. Operatorul nu va preda datele personale altor
            persoane sau organizații, cu excepția eventualelor situații
            prevăzute prin lege.</li>
    </ul>

    <h4>6. Durata prelucrării datelor</h4>

    <ul>
        <li>Datele cu caracter personal, cu excepția datelor biotice,
            furnizate în mod obligatoriu de utilizator, respectiv datele
            colectate în mod automatizat în cursul utilizării bazei de date
            vor fi păstrate până când utilizatorul va avea un cont valid în
            OpenBirdMaps.</li>
        <li>Datele cu caracter personal opționale vor fi păstrate până
            când utilizatorul le șterge sau până când utilizatorul va avea
            un cont valid în OpenBirdMaps.</li>
        <li>Datele din fișierele log ale paginilor de web OpenBirdMaps sunt
            stocate timp de 1 an de la data vizitei.</li>
        <li>Componenta personală a datelor biotice, adică numele
            observatorilor, respectiv numele utilizatorilor, odată publicate de
            utilizatori vor rămâne definitiv în datele biotice, cu excepția
            unor cazuri rare în care administratorii consideră ștergerea
            justificată.</li>
    </ul>

    <h4>7. Editarea și ștergerea datelor cu caracter personal</h4>

    <p>Utilizatorul are oricând posibilitatea modificării numelui, al
    adresei de e-mail și a celorlalte date cu caracter personal
    furnizate în mod voluntar.</p>

    <p>Utilizatorul poate desființa (șterge) oricând contul său de
    utilizator sau poate cere ștergerea contului de către
    administratori. Odată cu ștergerea contului vor fi șterse toate
    datele de identificare utilizator.</p>

    <p>Componenta personală, adică numele observatorilor respectiv
    numele utilizatorilor, nu va fi ștearsă din datele biotice, cu
    excepția unor cazuri rare considerate justificate de către
    administratori. Datele biotice
    au fost publicate de utilizator în mod voluntar și conștient
    de faptul că, datele biotice vor avea statut de publicație
    științifică, iar, similar publicațiilor tipărite, ștergerea
    acestor date nu va fi posibilă.</p>

    <h4>8. Măsuri de siguranța datelor</h4>

    <p>Din motive de siguranță operatorul bazei de date păstrează o
    copie a datelor biotice și a altor date cu caracter personal în
    afara serverelor proprii și pe alte servere din sediul celorlalți
    membri ai Consorțiului OpenBioMaps, legate printr-o rețea
    securizată privată. Administrarea acestor servere este realizată
    conform regulilor proprii ale membrilor consorțiului. La aceste date
    au acces cu parolă numai administratorii desemnați de consorțiu.
    Ștergerea datelor personale din baza de date OpenBirdMaps atrage
    după sine și ștergerea acestor date de pe celelalte servere.</p>

    <p>Celelalte servere aflate în administrația consorțiului
    OpenBioMaps:</p>

    <ol>
        <li>
            <p>http://openbiomaps.org<br/>
            Operator: Debreceni Egyetem
            Informatikai Szolgáltató Központ<br/>
            Adresă: H-4032 Debrecen,
            Egyetem tér 1., Ungaria</p>
            <p>E-mail: helpdesk@it.unideb.hu</p></li>
        <li>
            <p>http://dinpi.openbiomaps.org<br/>
            Operator: Duna-Ipoly Nemzeti
            Park Igazgatóság<br/>
            Adersă: Duna-Ipoly Nemzeti Park Igazgatóság,
            H-1121 Budapest, Költő u. 21., Ungaria</p>
            <p>E-mail: <a href="mailto:bercess@dinpi.hu" target="_top">bercess@dinpi.hu</a></p></li>
    </ol>

    <h4>9. Modificarea regulamentului  privind protecția datelor personale în OpenBirdMaps</h4>

    <p>Operatorul își rezervă dreptul de a modifica prezentul
    regulament unilateral, având obligația de a anunța utilizatorii
    despre modificări. Utilizatorul va avea posibilitatea acceptării
    modificărilor la primare logare după intrarea în vigoare a
    regulamentului modificat.</p>

    <hr>
    
    <p>Data ultimei modificări: 29.06.2018</p>
</div>
<?php endif; ?>
