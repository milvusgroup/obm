<?php 
require_once(getenv('OB_LIB_DIR'). '/private/page.php');
class helpPage extends Page
{
    function __construct($subpage)
    {
        // backward compatibility
        if (!$subpage && isset($_GET['tutoriale'])) {
            $subpage = 'tutoriale';
        }
        if (!$subpage && isset($_GET['tutoriale_video'])) {
            $subpage = 'tutoriale_video';
        }
        $this->content = $this->prepare_content($subpage);
    }
    
    static function ajax($request) {
        // 
    }
    
    private function get_stylesheet() {
        $css = file_get_contents(getenv('OB_LIB_DIR').'/private/help.css');
        return $css;
    }
    private function get_scripts() {
        $file = getenv('OB_LIB_DIR').'/private/help.js';
        $css = file_exists($file) ? file_get_contents($file) : "";
        return $css;
    }
    
    private function prepare_content($subpage) {
        ob_start();
        ?>
        <style media="screen">
        <?= $this->get_stylesheet(); ?>
        </style>
        <div class="grid-container">
            <header class="header">
                <ul class="header__list">
                    <li class="header__list-item"> <a href="/help/"> Help general </a> </li>
                    <li class="header__list-item"> <a href="/help/tutoriale"> Tutoriale </a> </li>
                    <li class="header__list-item"> <a href="/help/tutoriale_video"> Tutoriale video </a> </li>
                </ul>
            </header>
            <aside class="sidenav">
                <div class="sidenav-content">
                    
                    <?php if ($subpage === 'tutoriale') : ?> 
                        <h3>Aplicația OpenBioMaps</h3>
                        <ul class="sidenav__list">
                            <li class="sidenav__list-item change-pdf-src active" data-src="1_OBM App_ Autentificare, selectare baza de date si formulare.pdf"> Autentificare, selectare baza de date si formulare </li>
                            <li class="sidenav__list-item change-pdf-src" data-src="2_OBM App_ Inregistrare si sincronizare date.pdf"> Inregistrare si sincronizare date </li>
                            <li class="sidenav__list-item change-pdf-src" data-src="3_OBM App_Formularul nocturne si Burhinus.pdf"> Formularul nocturne si Burhinus </li>
                            <li class="sidenav__list-item change-pdf-src" data-src="OBM-App_Formular-barza-alba.pdf"> Formularul barza albă </li>
                            <li class="sidenav__list-item change-pdf-src" data-src="5_OBM_App_Formularul_rapitoare_de_zi.pdf"> POIM - Monitorizarea păsărilor răpitoare de zi </li>
                        </ul>
                        <h3>OBM Tools</h3>
                        <ul class="sidenav__list">
                            <li class="sidenav__list-item change-pdf-src active" data-src="Ghid_pentru_validarea_patratelor_OBM_tools.pdf"> Ghid pentru validarea observațiilor dintr-un pătrat de monitorizare - schema răpitoare de zi</li>
                        </ul>
                    <?php elseif ($subpage === 'tutoriale_video') : ?>
                        <h3>Formulare web OpenBirdMaps</h3>
                        <ul class="sidenav__list">
                            <li class="sidenav__list-item change-yt-src" data-src="zEz_MUXVqOE"> Monitorizarea păsărilor nocturne din habitate deschise 2020 </li>
                            <li class="sidenav__list-item change-yt-src" data-src="YC7CzbmEuMY"> Încărcarea informațiilor auxiliare </li>
                            <li class="sidenav__list-item change-yt-src" data-src="xFAO7p7JkXE"> Descărcarea punctelor de monitorizare </li>
                        </ul>
                        <h3>OBM Tools</h3>
                        <ul class="sidenav__list">
                            <li class="sidenav__list-item change-yt-src" data-src="fU56CXyc_04"> Validarea pătratelor de monitorizare - monitorizarea păsărilor răpitoare de zi </li>
                        </ul>
                    <?php else: ?>
                        <ul class="sidenav__list">
                            <li class="toclevel-1 tocsection-1"><a href="#Prezentare_general.C4.83"><span class="tocnumber">1</span> <span class="toctext">Prezentare generală</span></a></li>
                            <li class="toclevel-1 tocsection-2"><a href="#Ce_fel_de_date_a.C8.99tept.C4.83m.3F"><span class="tocnumber">2</span> <span class="toctext">Ce fel de date așteptăm?</span></a>
                                <ul>
                                    <li class="toclevel-2 tocsection-3"><a href="#Valoarea_datelor"><span class="tocnumber">2.1</span> <span class="toctext">Valoarea datelor</span></a></li>
                                </ul>
                            </li>
                            <li class="toclevel-1 tocsection-4"><a href="#Accesibilitatea_datelor"><span class="tocnumber">3</span> <span class="toctext">Accesibilitatea datelor</span></a>
                                <ul>
                                    <li class="toclevel-2 tocsection-5"><a href="#Nivele_de_acces"><span class="tocnumber">3.1</span> <span class="toctext">Nivele de acces</span></a></li>
                                </ul>
                            </li>
                            <li class="toclevel-1 tocsection-6"><a href="#.C3.8Enc.C4.83rcare_de_date"><span class="tocnumber">4</span> <span class="toctext">Încărcare de date</span></a>
                                <ul>
                                    <li class="toclevel-2 tocsection-7"><a href="#Esen.C8.9Bial_de_citit"><span class="tocnumber">4.1</span> <span class="toctext">Esențial de citit</span></a></li>
                                    <li class="toclevel-2 tocsection-8"><a href="#Formulare"><span class="tocnumber">4.2</span> <span class="toctext">Formulare</span></a> </li>
                                </ul>
                            </li>
                            <li class="toclevel-1 tocsection-39"><a href="#Metode_2"><span class="tocnumber">5</span> <span class="toctext">Metode</span></a>
                                <ul>
                                    <li class="toclevel-2 tocsection-40"><a href="#Observa.C8.9Bii_ocazionale"><span class="tocnumber">5.1</span> <span class="toctext">Observații ocazionale</span></a></li>
                                    <li class="toclevel-2 tocsection-41"><a href="#Metode_generale_definite_.C3.AEn_OpenBirdMaps"><span class="tocnumber">5.2</span> <span class="toctext">Metode generale definite în <i>OpenBirdMaps</i></span></a> </li>
                                    <li class="toclevel-2 tocsection-46"><a href="#Metode_specifice_din_scheme_de_monitorizare.2Fevaluare"><span class="tocnumber">5.3</span> <span class="toctext">Metode specifice din scheme de monitorizare/evaluare</span></a></li>
                                </ul>
                            </li>
                            <li class="toclevel-1 tocsection-47"><a href="#Profil"><span class="tocnumber">6</span> <span class="toctext">Profil</span></a>
                                <ul>
                                    <li class="toclevel-2 tocsection-48"><a href="#.C3.8Enc.C4.83rcare_.C8.99i_manipulare_geometrii"><span class="tocnumber">6.1</span> <span class="toctext">Încărcare și manipulare geometrii</span></a></li>
                                    <li class="toclevel-2 tocsection-49"><a href="#Cereri_desc.C4.83rcare_de_date"><span class="tocnumber">6.2</span> <span class="toctext">Cereri descărcare de date</span></a></li>
                                </ul>
                            </li>
                            <li class="toclevel-1 tocsection-50"><a href="#Hart.C4.83_.C8.99i_interogare_.28filtrare.29_date"><span class="tocnumber">7</span> <span class="toctext">Hartă și interogare (filtrare) date</span></a></li>
                        </ul>
                    <?php endif; ?>
                </div>
            </aside>
                
            <main class="main">
                <div class="main-content">
                    <?php
                    switch ($subpage) {
                        case 'tutoriale':
                            include('help_pages/tutoriale.html');
                            break;
                        
                        case 'tutoriale_video':
                            include('help_pages/tutoriale_video.html');
                            break;
                            
                        default:
                            include('help_pages/help_general.html');
                            break;
                    }
                    ?>
                </div>
            </main>
        </div>
        <script type="text/javascript">
        <?= $this->get_scripts(); ?>
        </script>
        <?php 
        $out = ob_get_clean();
        return $out;
    }
    
}
?>
