$(document).ready(function() {
    $('.change-pdf-src').on('click', function() {
        const src = $(this).data('src');
        $('main iframe').attr('src',"https://openbirdmaps.ro/downloads/" + src);
    })
    $('.change-yt-src').on('click', function() {
        const src = $(this).data('src');
        $('main iframe').attr('src',"https://www.youtube.com/embed/" + src);
    })
})