<?php 
/**
 * 
 */
class Program_18 extends Program
{
    
    public static function menu_items() {
        if (!isset($_SESSION['Tid'])) {
            return json_encode([]);
        }
        $menu = [];
        if (has_access('master')) {
            $menu[] = 'Download sampling units';
        }
        return json_encode($menu);
    }
    public static function main_page() {
        return "";
    }
    
    public static function download_sampling_units() {
        ob_start();
        $file_path = getenv('PROJECT_DIR') . '/downloads/strix_sampling_units.zip';
            
        //a táblázat
        ?>
        <div class="buttons">
            <button type="button" id="regenerate_gpx" data-program_id="18" class="pure-button"> Regenerate GPX files </button>
            <?php if (file_exists($file_path)): ?>
                <a href="https://openbirdmaps.ro/downloads/strix_sampling_units.zip" target="_blank" id="download_gpx" class="pure-button" download> Dowload ZIP </a>
            <?php endif; ?>
        </div>
        <div class="wait">
        </div>
        <?php
        return ob_get_clean();
    }
    
    public static function regenerate_gpx() {
      global $ID;
      
      $cmd = "SELECT DISTINCT gykod FROM milvus_grouping_codes WHERE program = 18;";
      if (!$res = pg_query($ID, $cmd)) {
        return common_message('error', 'regenerate_gpx query error');
      }
      $gcs = array_column( pg_fetch_all($res), 'gykod');
      $tmpdir = '/tmp/strix_sampling_units';
      $dldir = getenv('PROJECT_DIR') . '/downloads';
      
      unlink($dldir.'/strix_sampling_units.zip');
      if (!is_dir($tmpdir)) {
        mkdir($tmpdir);
      }
      else {
        array_map('unlink', glob("$tmpdir/*.*"));
      }
      $results = [];
      foreach ($gcs as $gc) {
        $cmd = "ogr2ogr -f GPX $tmpdir/$gc.gpx PG:\"host='localhost' user='" . gisdb_user. "' port='5433' dbname='gisdata' password='" . gisdb_pass . "'\" -nlt POINT -sql \"SELECT alapegyseg AS name, obm_geometry as geometry FROM milvus_sampling_units WHERE program = 18 AND gykod_nev = '$gc' AND obm_geometry IS NOT NULL\"";
        $results[] = shell_exec($cmd);
      }
      $cmd = "zip $dldir$tmp/strix_sampling_units.zip $tmpdir/*.gpx";
      $results[] = shell_exec($cmd);
      return common_message('success', $results);
    }
}

?>