<?php 
/**
 * 
 */
class Program_9 extends Program
{
    
    public static function menu_items() {
        if (!isset($_SESSION['Tid'])) {
            return json_encode([]);
        }
        $menu = [];
        if (has_access('master')) {
            $menu[] = 'Point priority';
        }
        return json_encode($menu);
    }
    public static function main_page() {
        return "";
    }
    public static function prepare_point_priority() {
        global $ID;
        
        $negyzetek_cmd = "SELECT gykod as grouping_code, array_to_json(array_agg(alapegyseg)) as sampling_units, count(*) as point_count 
            FROM milvus_grouping_codes gc 
            LEFT JOIN milvus_sampling_units su ON gc.obm_id = su.gykod_id AND gc.program = su.program 
            WHERE gc.program = 9 AND su.obm_geometry IS NOT NULL GROUP BY gykod ORDER BY gykod;";
        if (! $res = pg_query($ID, $negyzetek_cmd) ) {
            return "Query error!";
        }
        $negyzetek = pg_fetch_all($res);
        
        
                
        // hány évből van adat a négyzetről: 
        $cmd = "SELECT grouping_code , array_to_json(array_agg(year)) as years, count(*) year_count FROM (SELECT DISTINCT national_grouping_code as grouping_code, extract('year' from date) as year FROM milvus WHERE national_program = 'str_nocturnal_monitoring_program' ) as foo GROUP BY grouping_code"; 
        if (! $res = pg_query($ID, $cmd) ) {
            return "Query error!";
        }
        
        $hanyszor = []; 
        while ($row = pg_fetch_assoc($res)) {
            $hanyszor[$row['grouping_code']] = $row;
        }
        
        // melyik pont hanyszor volt megcsinálva
        $pontok_cmd = "SELECT grouping_code, sampling_unit, count(*) as point_visited_count FROM (SELECT DISTINCT national_grouping_code as grouping_code, national_sampling_unit as sampling_unit, extract(year from \"date\") FROM milvus WHERE national_program = 'str_nocturnal_monitoring_program') foo GROUP BY sampling_unit, grouping_code;";
        if (! $res = pg_query($ID, $pontok_cmd) ) {
            return "Query error!";
        }
        $pontok_hanyszor = pg_fetch_all($res);
        
        $negyzetek = array_map(function($n) use ($hanyszor, $pontok_hanyszor) {
            $grouping_code = $n['grouping_code'];
            
            
            $sampling_units = [];
            $number_of_visited_points = 0;
            foreach (json_decode($n['sampling_units']) as $su) {
                $sampling_units_counts = array_values(array_filter($pontok_hanyszor, function($p) use ($su) {
                    return $p['sampling_unit'] === $su;
                }));
                $points_visited_count = (isset($sampling_units_counts[0]['point_visited_count'])) ? (int)$sampling_units_counts[0]['point_visited_count'] : 0;
                $sampling_units[] = [
                    'sampling_unit' => $su,
                    'point_visited_count' => $points_visited_count,
                ];
                if ($points_visited_count > 0) {
                    $number_of_visited_points++;
                }
            }
            
            $n = [
                'grouping_code' => $grouping_code,
                'sampling_units' => $sampling_units,
                'point_count' => $n['point_count'],
                'years' => (isset($hanyszor[$n['grouping_code']])) ? json_decode($hanyszor[$n['grouping_code']]['years']) : [],
                'year_count' => $hanyszor[$n['grouping_code']]['year_count'] ?? 0,
                'number_of_visited_points' => $number_of_visited_points,
            ];
            if ($n['year_count'] === 0) {
                self::initial_order($n['sampling_units'], $n['grouping_code']);
            }
            else {
                self::order_by_number_of_points_visited($n['sampling_units'], $n['grouping_code']);
            }
            /*
            elseif ($number_of_visited_points < 10 ) {
                self::order_if_once($n['sampling_units'], $n['grouping_code']);
            }
            */
            return $n;
            
        }, $negyzetek);
        
        return $negyzetek;
    }
    
    public static function point_priority() {
        ob_start();
        $negyzetek = self::prepare_point_priority();
        $file_path = getenv('PROJECT_DIR') . '/downloads/priority.xlsx';
            
        //a táblázat
        ?>
        <div class="buttons">
            <button type="button" id="dl-xls" class="pure-button"> Regenerate XLS </button>
            <?php if (file_exists($file_path)): ?>
                <a href="https://openbirdmaps.ro/downloads/priority.xlsx" target="_blank" class="pure-button" download> Dowload XLS </a>
            <?php endif; ?>
            <button type="button" id="refresh-excluded" class="pure-button button-danger"> Update the excluded column in the database </button>
        </div>
        <table class="pure-table pure-table-striped">
            <colgroup>
                <col span="1" style="width: 5%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 42.5%;">
                <col span="1" style="width: 42.5%;">
            </colgroup>
            <thead>
                <tr>
                    <th>grouping_code</th>
                    <th>years when surveyed</th>
                    <th>priority points</th>
                    <th>secondary points</th>
                </tr>
            </thead> 
            <tbody>
                <?php foreach ($negyzetek as $n): ?>
                <?php 
                $su = array_map(function($el) {
                    $str = "{$el['sampling_unit']}";
                    if ($el['point_visited_count'] !== 0) {
                        $str .= " ({$el['point_visited_count']})"; 
                    } 
                    return $str;
                }, $n['sampling_units']); 
                
                ?>
                    <tr>
                        <td><?= $n['grouping_code'] ?></td>
                        <td><?= implode(', ', $n['years']) ?></td>
                        <td><?= implode(', ', array_slice($su, 0, 10)) ?></td>
                        <td><?= implode(', ', array_slice($su, 10)) ?></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
        <?php
        return ob_get_clean();
    }
    
    public static function update_excluded() {
        global $ID;
        $cmd = [
            "UPDATE milvus_sampling_units SET excluded = true WHERE program = 9;"
        ];
        $in = [];
        foreach (self::prepare_point_priority() as $n) {
            foreach ($n['sampling_units'] as $su) {
                $in[] = "'{$su['sampling_unit']}'";
            }
        }
        $cmd[] = sprintf("UPDATE milvus_sampling_units SET excluded = false WHERE program = 9 AND alapegyseg IN (%s);", implode(',',$in));
        
        $res = query($ID, $cmd);
        return 'done';
    }
    public static function dl_xls() {
        $negyzetek = array_map(function($n) {
            $su = array_map(function($el) {
                preg_match('/\.(\w*)$/', $el['sampling_unit'], $m);
                return $m[1];
            }, $n['sampling_units']); 
            
            $y = implode(', ', $n['years']);
            $priority = implode(', ', array_slice($su, 0, 10));
            $secondary = implode(', ', array_slice($su, 10));
            return [
                'grouping_code' => $n['grouping_code'],
                'priority_points' => $priority,
                'secondary_points' => $secondary,
            ];
        }, self::prepare_point_priority());
        
        
        require(getenv('OB_LIB_DIR'). '/vendor/autoload.php');
        
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator($_SESSION['Tname'])->setLastModifiedBy($_SESSION['Tname']);
        
        $sheet = $spreadsheet->getActiveSheet();
        
        // set the names of header cells
        $sheet->setCellValue('A1', 'grouping_code');
        $sheet->setCellValue('B1', 'priority');
        $sheet->setCellValue('C1', 'secondary');
        // Add some data
        $x = 2;
        foreach($negyzetek as $n){
            $sheet->setCellValue('A'.$x, $n['grouping_code']);
            $sheet->setCellValue('B'.$x, $n['priority_points']);
            $sheet->setCellValue('C'.$x, $n['secondary_points']);
            $sheet->getStyle("A$x:C$x")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $x++;
        }
        foreach (range("A","C") as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        //Create file excel.xlsx
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save(getenv('PROJECT_DIR').'downloads/priority.xlsx');
        exit;
    }
    
    public static function apped_order(&$sampling_units, $seq = 1) {
        $ord1 = ['01','03','04','06','08','09','10','11','14','16','02','05','07','12','13','15'];
        $ord2 = ['01','03','04','06','08','09','10','11','14','16','05','15','02','12','13','07']; 
        $ord = ${"ord$seq"};
        
        $k = 17;
        for ($i=0, $l = count($sampling_units); $i < $l ; $i++) {
            $su = $sampling_units[$i];
            preg_match('/\.(\w*)$/', $su['sampling_unit'], $m);

//            $tmp[$su['sampling_unit']] = $su['point_visited_count'];
            $idx = array_search($m[1], $ord);
            $sampling_units[$i]['ord'] = ($idx !== false) ? $idx : $k;
            if ($idx === false) {
                $k++;
            }
        
        }
    }
    
    public static function initial_order(&$sampling_units, $gc, $seq = 1) {
        
        self::apped_order($sampling_units);
        $ord = array_column($sampling_units, 'ord');
        array_multisort($ord, SORT_NUMERIC, SORT_ASC, $sampling_units);
    }
    
    public static function order_by_number_of_points_visited(&$sampling_units, $gc) {
        self::apped_order($sampling_units,2);
        
        $visited = array_values(array_filter($sampling_units, function($su) {
            return ($su['point_visited_count'] !== 0);
        }));
        $points_visited_count = array_column($visited, 'point_visited_count');
        $ord = array_column($visited, 'ord');
        array_multisort($points_visited_count, SORT_NUMERIC, SORT_DESC, $ord, SORT_NUMERIC, SORT_ASC, $visited);
        
        $notvisited = [];
        if (count($visited) <= 10) {
            $notvisited = array_values(array_filter($sampling_units, function($su) {
                return ($su['point_visited_count'] === 0);
            }));
            
            $points_visited_count = array_column($notvisited, 'point_visited_count');
            $ord = array_column($notvisited, 'ord');
            array_multisort($points_visited_count, SORT_NUMERIC, SORT_DESC, $ord, SORT_NUMERIC, SORT_ASC, $notvisited);
        }
        $sampling_units = array_merge($visited, $notvisited);
    }
}

?>