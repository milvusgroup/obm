-- save_square_info_insert
WITH ins as (
    INSERT INTO milvus.raptor_survey_session (period_id, grouping_code, responsible) VALUES (%d, %s, %d) RETURNING id
)
INSERT INTO milvus.raptor_observer (session_id, observer) SELECT id, unnest(ARRAY[%s]::integer[]) FROM ins;

-- save_square_info_update
UPDATE milvus.raptor_survey_session SET responsible = %1$d WHERE id = %2$d;
DELETE FROM milvus.raptor_observer WHERE session_id = %2$d;
INSERT INTO milvus.raptor_observer (session_id, observer) SELECT %2$d, unnest(ARRAY[%3$s]::integer[]);

-- get_gcs
SELECT DISTINCT s.id, s.grouping_code, s.finished
FROM milvus.raptor_survey_session s
LEFT JOIN milvus.raptor_observer o ON s.id = o.session_id
WHERE 
    period_id = %1$d AND 
    ( responsible = %2$d OR observer = %2$d );

-- unoccupiedGcs_selector
SELECT gc.grouping_code 
FROM milvus_grouping_codes_mv gc 
LEFT JOIN milvus.raptor_period p ON gc.project = p.project AND gc.program = p.program
LEFT JOIN milvus.raptor_survey_session s ON s.grouping_code = gc.grouping_code AND s.period_id = p.id
WHERE p.id = %d AND s.responsible IS NULL ORDER BY gc.grouping_code;

-- get_observed_species_list_pair_tool
SELECT DISTINCT species_valid as species 
FROM milvus m
LEFT JOIN milvus.raptor_period p ON
    m.projekt = p.project AND
    m.national_program = p.program AND 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.species_valid IN ( SELECT sp FROM unnest(p.species_list) as t(sp) )  
WHERE
    p.id = %d AND
    m.national_grouping_code = %s AND 
    m.national_sampling_unit = %s AND 
    GeometryType(m.obm_geometry) = 'POINT';
        
-- get_observed_species_list_double_tool
SELECT DISTINCT species_valid as species 
FROM milvus m
LEFT JOIN milvus.raptor_period p ON
    m.projekt = p.project AND
    m.national_program = p.program AND 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.species_valid IN ( SELECT sp FROM unnest(p.species_list) as t(sp) )  
WHERE
    p.id = %d AND
    m.national_grouping_code = %s AND 
    GeometryType(m.obm_geometry) = 'POINT';

-- get_observations
SELECT row_to_json(fc) as geojson
FROM ( 
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
    FROM (
        SELECT 
            'Feature' As type,
            ST_AsGeoJSON(lg.obm_geometry)::json As geometry,
            row_to_json((
                SELECT l FROM (
                    SELECT lg.obm_id, observers, date, exact_time, species_valid as species, number, age,
                        gender, status_of_individuals, comments_observation, po.pair_id, status
                    ) as l
            )) As properties
        FROM milvus As lg   
        LEFT JOIN milvus.raptor_pair_observation po ON lg.obm_id = po.obm_id
        LEFT JOIN milvus.raptor_pair pr ON pr.id = po.pair_id
        LEFT JOIN milvus.raptor_period p ON
            lg.projekt = p.project AND
            lg.national_program = p.program AND 
            lg."date" BETWEEN p.date_from AND p.date_until
        WHERE 
            p.id = %d AND
            lg.national_grouping_code = %s AND 
            lg.national_sampling_unit = %s AND 
            lg.species_valid = %s AND
            GeometryType(obm_geometry) = 'POINT'
    ) As f 
)  As fc;

-- get_results
WITH 
doubles as (
    SELECT 
        d.id as double_id, 
        s.grouping_code,
        p.species,
        p.id as pair_id, 
        d.status as double_status, 
        d.min,
        d.max
    FROM milvus.raptor_double d
    LEFT JOIN milvus.raptor_double_pair dp ON  dp.double_id = d.id
    LEFT JOIN milvus.raptor_pair p ON dp.pair_id = p.id
    LEFT JOIN milvus.raptor_survey_session s ON p.session_id = s.id
    WHERE 
        s.period_id = %1$d AND 
        s.grouping_code IN (%2$s)
),
pairs as (
    SELECT 
        p.id as pair_id, 
        s.grouping_code, 
        p.species, 
        p.min, 
        p.max
    FROM milvus.raptor_pair p
    LEFT JOIN milvus.raptor_survey_session s ON s.id = p.session_id
    LEFT JOIN doubles dbl on p.id = dbl.pair_id
    WHERE 
        dbl.pair_id IS NULL AND
        s.period_id = %1$d AND
        s.grouping_code IN (%2$s) AND 
        p.status IN ('str_certain_pair', 'str_possible_pair')
),
unionn as (
    SELECT double_id as id, grouping_code, species, min, max FROM doubles
    UNION 
    SELECT pair_id as id, grouping_code, species, min, max FROM pairs
) SELECT grouping_code, species, sum(min) as min, sum(max) as max FROM unionn GROUP by grouping_code, species;

-- get_results_2
SELECT 
    national_grouping_code as grouping_code, 
    national_sampling_unit as sampling_unit, 
    species_valid as species, 
    count(*) as total,
    count(pair_id) as evaluated,
    count(DISTINCT pair_id) as no_of_pairs
FROM milvus m
LEFT JOIN milvus.raptor_period p ON 
    m.projekt = p.project AND
    m.national_program = p.program AND 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.species_valid IN ( SELECT sp FROM unnest(p.species_list) as t(sp) )  
WHERE 
    p.id = %d AND
    national_grouping_code IN (%s) AND
    GeometryType(obm_geometry) = 'POINT' 
GROUP BY national_grouping_code, national_sampling_unit, species_valid
ORDER BY national_grouping_code, national_sampling_unit, species_valid;

-- get_results_3
SELECT grouping_code FROM milvus.raptor_survey_session WHERE period_id = %d AND grouping_code IN (%s) AND finished = 'true';

-- save_pair_status
WITH ins1 as (    
    INSERT INTO milvus.raptor_pair (session_id, sampling_unit, species, min, max, status, geometry)
    VALUES (%d, %s, %s, %d, %d, %s , ST_GeometryFromText(%s, 4326)) RETURNING id
),
ins2 as (
    INSERT INTO milvus.raptor_pair_observation (pair_id, obm_id) 
    SELECT id, unnest(ARRAY[%s]) FROM ins1
)
SELECT id as pair_id FROM ins1;

-- is_pair_tool_finished
SELECT count(*) as unevaluated 
FROM milvus m
LEFT JOIN milvus.raptor_period p ON 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.national_program = p.program AND
    m.projekt = p.project
WHERE 
    p.id = %d AND
    national_grouping_code = %s AND
    species_valid = %s AND
    GeometryType(obm_geometry) = 'POINT' AND 
    pair_id IS NULL;

-- get_pairs
SELECT 
    row_to_json(fc) as geojson
FROM ( 
    SELECT 
        'FeatureCollection' As type, 
        array_to_json(array_agg(f)) As features
    FROM (
        SELECT 
            'Feature' As type, 
            ST_AsGeoJSON(p.geometry)::json As geometry,
            row_to_json(
                (
                    SELECT l FROM (
                        SELECT 
                            p.id as obm_id, 
                            p.species, 
                            s.grouping_code, 
                            p.sampling_unit,
                            p.status,
                            p.min,
                            p.max,
                            d.id as double_id, 
                            d.status as double_status
                    ) as l
                )
            ) As properties
        FROM milvus.raptor_pair As p
        LEFT JOIN milvus.raptor_survey_session s ON p.session_id = s.id
        LEFT JOIN milvus.raptor_double_pair dp ON p.id = dp.pair_id
        LEFT JOIN milvus.raptor_double d ON dp.double_id = d.id
        WHERE 
            s.period_id = %d AND
            s.grouping_code = %s AND 
            p.species = %s AND
            p.status IN ('str_certain_pair', 'str_possible_pair')
    ) As f 
)  As fc;

-- save_double_status
WITH mx AS (
    SELECT max("min") as min_pairs from milvus.raptor_pair where id IN (%s)
),
ins AS (
    INSERT INTO milvus.raptor_double (session_id, min, max, status) SELECT %d, min_pairs, %d, %s FROM mx RETURNING id
)
INSERT INTO milvus.raptor_double_pair (double_id, pair_id) SELECT id, unnest(ARRAY[%s]) FROM ins RETURNING double_id;
