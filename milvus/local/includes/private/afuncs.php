<?php
session_start();
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'languages.php');
/** Nem tudom ez mi de ha kiderítem leírom **/
if (isset($_GET['form'])) {
    global $BID;
    $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id = %s",quote($_GET['form']));
    $res = pg_query($BID,$cmd);

    header("Content-type: application/json");
    if ($res and pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        echo json_encode($row);
    } else
        echo json_encode(array());
}

if (isset($_REQUEST['page'])) {
    if (!in_array($_REQUEST['page'], ['monitoring'] ) ) {
        return false;
    }
    
    $p = "{$_REQUEST['page']}Page";
    require_once(getenv('PROJECT_DIR').'/local/includes/private/page.php');
    require_once(getenv('PROJECT_DIR')."/local/includes/private/{$_REQUEST['page']}.php");
    
    echo $p::ajax($_REQUEST);
    exit;
}

/* GeoJSON for the simple white-stork nests map built for the electricity companies */
if (isset($_GET['stork-map'])) {
    //$cmd = " SELECT json_build_object( 'type', 'FeatureCollection', 'features', json_agg(t)) as geojson FROM ( SELECT json_build_object( 'type', 'Feature', 'id', obm_id, 'geometry', ST_AsGeoJSON(obm_geometry)::json, 'properties', json_build_object( 'species', species, 'nest_place', nest_place, 'nest_status', nest_status)) FROM milvus WHERE method = 'str_occasional_observation' AND species IN ('Ciconia ciconia','null') AND observed_unit = 'str_nest_cavity' AND GeometryType(obm_geometry) = 'POINT' AND access_to_data = 0 LIMIT 100) as t;";
    
    
    $cmd = "SELECT row_to_json(fc) as geojson
            FROM ( 
                SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                FROM (
                    SELECT 'Feature' As type
                    , ST_AsGeoJSON(lg.obm_geometry)::json As geometry
                    , row_to_json((SELECT l FROM (SELECT 
                        obm_id, 
                        CASE WHEN species = 'null' THEN 'Üres állvány' ELSE species END as species, 
                        nest_place, 
                        'https://openbirdmaps.ro/getphoto?ref=' || f.reference || '&getthumbnailimage' as image) As l
                    )) As properties

                    FROM milvus As lg   
                    LEFT JOIN system.file_connect fc ON lg.obm_files_id = fc.conid
                    LEFT JOIN system.files f ON fc.file_id = f.id
                    WHERE 
                        method = 'str_occasional_observation' AND 
                        species IN ('Ciconia ciconia','null') AND 
                        observed_unit = 'str_nest_cavity' AND 
                        nest_place IN ('str_pylon_with_nest_support', 'str_pylon_without_nest_support') AND
                        GeometryType(obm_geometry) = 'POINT'
                    ) As f 
                )  As fc;";


    if (!$res = pg_query($ID, $cmd)) {
        echo common_message('error','query error');
        exit;
    }
    $result = pg_fetch_assoc($res);
    header("Content-type: application/json");
    echo $result['geojson'];
    exit;
}

if (isset($_GET['etrs10_geojson'])) {
    
    $cmd = "SELECT row_to_json(fc) as geojson
            FROM ( 
                SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                FROM (
                    SELECT 'Feature' As type
                    , ST_AsGeoJSON(lg.geometry)::json As geometry
                    , row_to_json((SELECT l FROM (SELECT 
                        id, 
                        name, 
                        milvus_code) As l
                    )) As properties

                    FROM shared.etrs10 As lg   
                    ) As f 
                )  As fc;";


    if (!$res = pg_query($ID, $cmd)) {
        echo common_message('error','query error');
        exit;
    }
    $result = pg_fetch_assoc($res);
    header("Content-type: application/json");
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET");
    echo $result['geojson'];
    exit;
}



?>
