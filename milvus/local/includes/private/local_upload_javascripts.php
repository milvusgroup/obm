<!--
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOLHig3gFN6stl8NyyJUw4G2y0T8Sztjo" type="text/javascript">
</script>
-->


<script>

// Természetvédelemi titkosítás engedélyezése ritka fajoknak, fészkeknek és telepeknek.

$(document).ready(function() {

    var egyseg_idx;
    var faj_idx;
    var titkos_idx;
    var fajok = ["Aquila heliaca", "Aquila chrysaetos", "Falco cherrug", "Falco peregrinus", "Bubo bubo", "Otis tarda", "Tetrao tetrix"];

    //oszlop azonosítók
    $("#upload-data-table thead").find("li").each(function(){
        if($(this).attr("id")=="observed_unit") {
            egyseg_idx = $(this).closest("th").index()+1;
        }
        if($(this).attr("id")=="species") {
            faj_idx = $(this).closest("th").index()+1;
        }
        if($(this).attr("id")=="access_to_data") {
            titkos_idx = $(this).closest("th").index()+1;
        }
    });
    //Kitett mezők
    if( $("#default-observed_unit").val()=="str_nest_cavity" || $("#default-observed_unit").val()=="str_colony" ) {
        if ($("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").length>0) {
            $("#upload-data-table tbody").find(".cf-"+titkos_idx).val("1");
        }
        $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
        $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
    }
    $(".mezok").on("change","#default-observed_unit",function(){
        if ($(this).val()=="nest_cavity" || $(this).val()=="str_nest_cavity" || $(this).val()=="str_colony"){
            if ($("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").length>0) {
                $("#upload-data-table tbody").find(".cf-"+titkos_idx).val("1");
            }
            $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
            $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
        } else {
            if ($("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=0]").length>0) {
                $("#upload-data-table tbody").find(".cf-"+titkos_idx).val("0");
            }
            $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",true);
            $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",true);

        }
    });

    //tábla
    $("#upload-data-table").on("change focusout",".cf-"+faj_idx,function(){
        var rix = $(this).closest("tr").index();
        var a=$.inArray($(this).val(),fajok);
        if(a==-1) {
            var e = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+egyseg_idx).val();
            if (e!="str_nest_cavity" && e != "str_colony") {
                const titkos_mezo = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx);
                if (titkos_mezo.length > 0) {
                    titkos_mezo[0].selectedIndex = 0;
                    titkos_mezo.prop("selectedIndex", 0);
                    titkos_mezo.attr("selectedIndex", 0);
                }
            }
        }
    });
    $("#upload-data-table").on("change focusout",".cf-"+egyseg_idx,function(){
        var rix = $(this).closest("tr").index();
        if ($(this).val()!="str_nest_cavity" && $(this).val()!="str_colony") {
            var a=$.inArray( $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+faj_idx).val(),fajok);
            if (a==-1) {
                const titkos_mezo = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx);
                if (titkos_mezo.length > 0) {
                    titkos_mezo[0].selectedIndex = 0;
                    titkos_mezo.prop("selectedIndex", 0);
                    titkos_mezo.attr("selectedIndex", 0);
                }
            }
        }

    });
    $("#upload-data-table").on("focus",".cf-"+titkos_idx,function(){
        var rix = $(this).closest("tr").index();
        var Rfaj = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+faj_idx).val();
        var Regy = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+egyseg_idx).val();

        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",true);
        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",true);

        var a=$.inArray(Rfaj,fajok);
        if(a!=-1) {
            $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
            $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
        }
        if (Regy == "str_nest_cavity" || Regy == "str_colony") {
            $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
          $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
        }
    });
});
</script>
