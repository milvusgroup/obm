<?php 
require_once(getenv('OB_LIB_DIR').'private/private_map_js.php');

// Include custom css if exists
if (file_exists('css/private/custom.css')) {
    echo '<link rel="stylesheet" href="'.$protocol.'://'. URL .'/css/private/custom.css?rev='.rev('../css/private/custom.css').'" type="text/css" />';
}
?>
<div id='custom_page'>
<?php 

if ($custom_load_page=='terms') { 
    echo "<style> .container {max-width: 900px; margin: 0 auto;} </style>";
    echo "<div class='container'><a href='http://openbirdmaps.ro/aszf_{$_SESSION['LANG']}.pdf' target='_blank'><h2>". t(str_terms_and_conditions) . "</h2></a></div>";
    require(getenv('PROJECT_DIR').'/aszf_'.$_SESSION['LANG'].'.html');
}
elseif ($custom_load_page=='privacy') { 
    echo "<style> .container {max-width: 900px; margin: 0 auto;} </style>";
    echo "<div class='container'><a href='http://openbirdmaps.ro/at_{$_SESSION['LANG']}.pdf' target='_blank'><h2>". t(str_privacy_policy_sajat) . "</h2></a></div>";
    require(getenv('PROJECT_DIR').'/at_'.$_SESSION['LANG'].'.html');
}
elseif ($custom_load_page=='cookies') { 
    require(getenv('PROJECT_DIR').'/cookies_'.$_SESSION['LANG'].'.html');
}
elseif (in_array($custom_load_page, ['monitoring','help'])) {
    require(getenv('PROJECT_DIR') . "/local/includes/private/$custom_load_page.php");
    $class = $custom_load_page . 'Page';
    if (!class_exists($class)) {
        echo "$class object does not exists.";
        exit;
    }
    $subpage = $clp_subpage ?? "";
    $page = new $class($subpage);
    echo $page->the_content();
}
?>
</div>
