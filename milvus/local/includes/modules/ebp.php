<?php
#' ---
#' Module:
#'   ebp
#' Files:
#'   [ebp.php]
#' Description: >
#'   EuroBirdPortal data provision module
#' Methods:
#'   [getMenuItem, adminPage, profileItem, print_css]
#' Module-type:
#'   project
#' Examples: 
#' Author:
#'   gabor.bone@milvus.ro
#' Version:
#'   2.0
class ebp extends module
{
    var $apiURL = 'https://api-v2.eurobirdportal.org/';
    var $error = '';
    var $retval;
    var $token;
    public $project_email = "openbirdmaps@milvus.ro";

    function __construct($action = null, $params = null, $pa = null)
    {
        require_once('ebp/ebp_classes.php');

        if ($action)
            $this->retval = $this->$action($params, $pa);
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function getMenuItem()
    {
        return ['label' => 'EuroBirdPortal', 'url' => 'ebp'];
    }

    public function adminPage()
    {
        $strings = ['str_no_access_to_this_function'];
        return render_view('modules/ebp', [
            'is_master' => has_access('master'),
            't' => array_combine($strings, array_map('t', $strings)),
        ], false);
    }


    public function profileItem()
    {
        return [
            'label' => 'EuroBirdPortal DataProvisions',
            'fa' => 'fa-globe',
            'item' => render_view('modules/ebp_profile_item', [], false) 
        ];
    }

    public function print_js($request)
    {
        $js = (file_exists(getenv('OB_LIB_DIR') . 'modules/ebp/ebp.js')) ? file_get_contents(getenv('OB_LIB_DIR') . 'modules/ebp/ebp.js') : '';
        return $js;
    }

    public function print_css($request)
    {
        $module = 'ebp';
        $mdule_header = "/* $module styles */\n";
        return (file_exists(getenv('PROJECT_DIR') . "/local/includes/modules/$module.css")) ? $mdule_header . file_get_contents(getenv('PROJECT_DIR') . "/local/includes/modules/$module.css") : '';
    }

    static function db_log($action, $cm)
    {
        global $ID;
        $cm = json_decode($cm);
        $status = $cm->status;
        $message = ($cm->message) ? $cm->message : $cm->data;
        $cmd = sprintf("INSERT INTO ebp.log (action, status, message) VALUES (%s,%s,%s)", quote($action), quote($status), quote($message));

        if (!pg_query($ID, $cmd)) {
            throw new Exception(pg_last_error($ID), 1);
        }
    }
}

?>