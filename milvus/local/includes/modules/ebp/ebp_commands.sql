-- manually export data example. see: https://redmine.milvus.ro/issues/1992
COPY (WITH records_raw AS (
    SELECT  
        pt.obm_id,
        u.uploading_date::date,
        COALESCE(protocol_code, '') as protocol_id,
        CASE 
            WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || '_' || g.etrs10_name
            ELSE 
                CASE 
                    WHEN pt.list_id IS NULL THEN pt.obm_uploading_id || '_' || pt.method 
                    ELSE pt.obm_uploading_id || '.' || pt.list_id || '_' || pt.method 
                END
        END as event_id,
        CASE 
            WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN 'C'
            WHEN (pt.comlete_list = 1) THEN 'L'
            WHEN (pt.comlete_list = 0 AND protocol_code IS NOT NULL) THEN 'F'
        END as data_type,
        date,
        CASE 
            WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
            ELSE round((duration::numeric/60),2)
        END as duration,
        CASE 
            WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
            ELSE make_interval(mins := time_of_start)
        END as time,
        pt.obm_geometry as geometry,
        pt.number,
        s.observers_ids AS observer_ids_array,
        g.etrs10_name as etrs_code,
        CASE
            WHEN (u.observer_name IS NOT NULL AND regexp_split_to_array(pt.observers,E',\\s*') @> ARRAY[u.observer_name::text] ) THEN u.observer_id
            ELSE (SELECT term_id FROM milvus_terms WHERE subject = 'observers' AND term LIKE (regexp_split_to_array(observers,E',\\s*'))[1]::text LIMIT 1)
        END as first_observer,
        bc.breeding_code as breeding_code,
        CASE 
            WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
            ELSE 
                CASE
                    WHEN status_of_individuals = 'str_overflying' THEN 'Y'
                    ELSE 'N'
                END
        END as flying_over,
        CASE
            WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || '_' || g.etrs10_name || '_' || tx.ebp_id
            ELSE CASE WHEN pt.list_id IS NULL THEN pt.obm_uploading_id || '_' || pt.method || '_' || tx.ebp_id ELSE pt.obm_uploading_id || '.' || pt.list_id || '_' || pt.method || '_' || tx.ebp_id  END
        END as record_id,
        tx.word as species_code,
        tx.taxon_id as local_taxon_id,
        'new'::varchar as status   
    FROM public.milvus pt 
    LEFT JOIN (SELECT * FROM system.uploadings up LEFT JOIN (SELECT term_id AS observer_id, term AS observer_name FROM public.milvus_terms WHERE subject = 'observers') as fooo ON observer_name = uploader_name) u
        ON pt.obm_uploading_id = u.id 
    LEFT JOIN ebp.protocol_connect pc 
        ON (
            method = obm_method AND 
            (national_program = obm_national_program OR (national_program IS NULL AND obm_national_program IS NULL)) AND 
            (local_program = obm_local_program OR (local_program IS NULL AND obm_local_program IS NULL))
        ) 
    LEFT JOIN ebp.breeding_codes_connect bc ON pt.status_of_individuals = bc.local_code
    LEFT JOIN public.milvus_qgrids g 
        ON g.data_table = 'milvus' AND pt.obm_id = g.row_id
    LEFT JOIN system.milvus_linnaeus s
        ON pt.obm_id = s.row_id
    LEFT JOIN (SELECT taxon_id, ebp_id, word FROM public.milvus_taxon tax LEFT JOIN ebp.species ebps ON tax.taxon_id = ebps.local_id) as tx 
        ON tx.word = pt.species 
    WHERE 
        species_valid in ('Branta leucopsis', 'Haematopus ostralegus', 'Ichthyaetus melanocephalus', 'Larus fuscus') AND
        protocol_code IS DISTINCT FROM 'excluded' AND 
        pt.date >= '2010-01-01' AND 
        pt.observed_unit = 'str_individuals' AND 
        method IS NOT NULL AND
        comlete_list IS NOT NULL AND 
        g.etrs10_name IS NOT NULL AND 
        extract('year' FROM pt.date) >= 2015 AND 
        extract('year' FROM pt.date) <= 2022
),
events as (
    SELECT bar.event_id,
        bar.protocol_id,
        bar.data_type,
        bar.date,
        bar.duration,
        bar."time",
        bar.location_mode,
        sum(bar.records) AS records,
        (sum(bar.observer))::character varying AS observer,
            CASE
                WHEN (bar.data_type = 'C'::text) THEN (( SELECT g.name
                FROM shared.etrs10 g
                WHERE public.st_intersects(g.geometry, bar.centroid)))::text
                ELSE public.st_astext(bar.centroid)
            END AS location,
            CASE
                WHEN (bar.data_type = 'C'::text) THEN NULL::integer
                ELSE ( SELECT (st_minimumboundingradius.radius)::integer AS radius
                FROM public.st_minimumboundingradius(public.st_transform(bar.geometryc, 31700)) st_minimumboundingradius(center, radius))
            END AS radius,
        1 AS state
    FROM ( SELECT foo.event_id,
                foo.protocol_id,
                foo.data_type,
                foo.date,
                foo.duration,
                foo."time",
                    CASE
                        WHEN (foo.data_type = 'C'::text) THEN count(DISTINCT foo.first_observer)
                        ELSE (max(foo.first_observer))::bigint
                    END AS observer,
                    CASE
                        WHEN (foo.data_type = 'C'::text) THEN 'A'::text
                        ELSE 'E'::text
                    END AS location_mode,
                public.st_collect(foo.geometry) AS geometryc,
                public.st_centroid(public.st_collect(foo.geometry)) AS centroid,
                    CASE
                        WHEN (foo.data_type = 'C'::text) THEN count(DISTINCT ROW(foo.local_taxon_id, foo.first_observer))
                        ELSE count(DISTINCT foo.local_taxon_id)
                    END AS records
            FROM ( SELECT bar_1.obm_id,
                        bar_1.uploading_date,
                        bar_1.protocol_id,
                        bar_1.event_id,
                        bar_1.data_type,
                        bar_1.date,
                        bar_1.duration,
                        bar_1."time",
                        bar_1.geometry,
                        bar_1.number,
                        bar_1.observer_ids_array,
                        bar_1.etrs_code,
                        bar_1.first_observer,
                        bar_1.breeding_code,
                        bar_1.flying_over,
                        bar_1.record_id,
                        bar_1.species_code,
                        bar_1.local_taxon_id,
                        bar_1.status,
                        unnest(bar_1.observer_ids_array) AS observer_ids
                    FROM records_raw bar_1
                    WHERE ((bar_1.status)::text = ANY (ARRAY[('new'::character varying)::text, ('modified'::character varying)::text]))) foo
            GROUP BY foo.event_id, foo.protocol_id, foo.data_type, foo.date, foo.duration, foo."time") bar
    GROUP BY bar.event_id, bar.protocol_id, bar.data_type, bar.date, bar.duration, bar."time", bar.location_mode,
            CASE
                WHEN (bar.data_type = 'C'::text) THEN NULL::integer
                ELSE ( SELECT (st_minimumboundingradius.radius)::integer AS radius
                FROM public.st_minimumboundingradius(public.st_transform(bar.geometryc, 31700)) st_minimumboundingradius(center, radius))
            END, bar.geometryc, bar.centroid
),
records as (
    SELECT 
        max(foo.breeding_code) AS breeding_code,
        foo.event_id,
        CASE
            WHEN (length(string_agg(DISTINCT lower(foo.flying_over), ''::text)) = 2) THEN 'N'::text
            ELSE string_agg(DISTINCT foo.flying_over, ''::text)
        END AS flying_over,
        foo.record_id,
        CASE
            WHEN (foo.data_type = 'C'::text) THEN count(*)
            ELSE (1)::bigint
        END AS records_of_species,
        foo.species_code,
        sum(foo.number) AS count
    FROM records_raw foo
    WHERE 
        foo.species_code IS NOT NULL AND 
        foo.status IN ('new', 'modified')
    GROUP BY foo.event_id, foo.record_id, foo.species_code, foo.data_type
),
records_ready AS (
    SELECT 
        breeding_code, 
        "count", 
        event_id, 
        flying_over, 
        record_id, 
        records_of_species, 
        species_code, 
        1::integer as state 
    FROM records
),
events_ready as (
    SELECT 
        data_type, 
        date, 
        duration, 
        event_id, 
        location_mode, 
        location, 
        observer, 
        protocol_id, 
        radius, 
        records, 
        time, 
        1::integer as state 
    FROM events
)
SELECT * FROM events_ready) TO '/tmp/OBM_4species_events.csv' CSV HEADER;
