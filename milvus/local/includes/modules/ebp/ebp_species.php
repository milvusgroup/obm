<?php

class ebp_species {

    static function setLocalId($request) {
        global $ID;
        $local_id = quote(preg_replace('/\D/','',$request['local_id']));
        $ebp_id = quote(preg_replace('/\D/','',$request['ebp_id']));
        $cmd = sprintf("UPDATE ebp.species SET local_id = %s WHERE ebp_id = %s;", $local_id, $ebp_id);
        if (query($ID, $cmd))
            return common_message('success','local_id updated');
        else
            common_message('fail','query error');

    }


    static function table() {
        global $ID;

        $res = query($ID,"SELECT * FROM ebp.species ORDER BY latin;");
        $species = [];
        while($row = pg_fetch_assoc($res[0]))
            $species[] = $row;

        $tbl = new createTable();
        $tbl->def(['tid'=>'ebpSpeciesTable','tclass'=>'resultstable']);
        $tbl->addHeader(['latin','english','local_id','ebp_id']);
        foreach ($species as $sp) {
            $tbl->addRows([
                $sp['latin'],
                $sp['english'],
                $sp['local_id'],
                $sp['ebp_id']
            ]);
        }
        return $tbl->printOut();
    }
    
    
    static function getLocalList() {
        global $ID;

        $cmd = sprintf("SELECT taxon_id, word FROM %s_taxon ORDER BY word;", PROJECTTABLE);
        if ($results = query($ID, $cmd)) {
            $list = [['taxon_id' => 0, 'word' => 'N/A']];
            while ($row = pg_fetch_assoc($results[0]))
                $list[] = $row;
            return json_encode($list);
        }
    }

    
    static function getEbpList () {
        global $ID;

        $species = ebp_curl::get(['url' => 'species/ebp']);

        pg_query($ID, 'TRUNCATE "ebp"."species";');
        pg_query($ID, 'COPY "ebp"."species" ("ebp_id", "latin", "english") FROM STDIN');

        foreach ($species as $sp) {
            pg_put_line($ID, "{$sp->species_id}\t{$sp->latin}\t{$sp->english}\n");
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

        $breeding_codes = ebp_curl::get(['url'=> 'breeding_code/']);

        pg_query($ID, 'TRUNCATE "ebp"."breeding_codes";');
        pg_query($ID, 'COPY "ebp"."breeding_codes" ("breeding_code", "breeding_code_text") FROM STDIN');

        foreach ($breeding_codes as $bc) {
            pg_put_line($ID, "{$bc->breeding_code}\t{$bc->breeding_code_text}\n");
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);


        $cmd = [
            sprintf('UPDATE ebp.species SET local_id = t.taxon_id FROM %s_taxon t WHERE latin = t.word;',PROJECTTABLE),
            "SELECT count(*) c FROM ebp.species WHERE local_id IS NULL;"
        ];
        if ($results = query($ID, $cmd)) {
            $state = pg_fetch_assoc($results[1]);
        }
        else
            return common_message('fail','assinging taxons failed');


        pg_query($ID, "TRUNCATE ebp.breeding_codes_connect;");
        if (file_exists(getenv('OB_LIB_DIR').'modules/ebp/breeding_codes.sql')) {
            $cmd = file_get_contents(getenv('OB_LIB_DIR').'modules/ebp/breeding_codes.sql');
            pg_query($ID, $cmd);
        }
        else
            return common_message('error','breeding_codes.sql missing');

        //return common_message('success', "{$state['c']} taxons are not assigned");
        return common_message('success','ok');
    }
}
?>
