<?php
class ebp_protocols {
    
    static function table($args = []) {

        $protocols = call_user_func([__CLASS__,'getProtocols']);

        $currentProtocolCodes = array_keys($protocols['current']); //array_column($protocols['current'],'protocol_code');
        $uploadedProtocolCodes = array_keys($protocols['uploaded']); //array_column($protocols['uploaded'],'protocol_code');

        $newProtocols = array_diff($currentProtocolCodes,$uploadedProtocolCodes);

        $tbl = new createTable();
        $tbl->def(['tid'=>'protocolsTable','tclass'=>'resultstable']);
$protocolColumns = ['citation','end_year','protocol_code','title','description','fixed_list_tags','link','ongoing','protocol_details','website','ebp_data_structure','geographic_coverage','method','project_type','start_year'];
        $tbl->addHeader(array_merge(['status'],$protocolColumns));
        foreach ($protocols['uploaded'] as $sp) {
            $cols = [sprintf('<button class="pure-button button-sm button-warning ebpActions" data-f="protocols_protocols" data-method="DELETE" data-code="%s"> delete </button>',$sp->protocol_code)];
            foreach($protocolColumns as $col) 
                $cols[] = (isset($sp->$col)) ? $sp->$col : '';
            $tbl->addRows($cols);
        }
        foreach ($protocols['current'] as $sp) {
            if (in_array($sp['protocol_code'], $newProtocols)) {
                $cols = [sprintf('<button class="pure-button button-sm ebpActions" data-f="protocols_protocols" data-method="POST" data-code="%s"> upload </button>',$sp['protocol_code']) ];
                foreach($protocolColumns as $col) 
                    $cols[] = $sp[$col];
                $tbl->addRows($cols);
            }
        }
        return $tbl->printOut();
    }

    static function deleteProtocols() {

        $protocols = call_user_func([__CLASS__,'getProtocols']);

        $currentProtocolCodes = array_column($protocols['current'],'protocol_code');
        $uploadedProtocolCodes = array_column($protocols['uploaded'],'protocol_code');

        $newProtocols = array_diff($currentProtocolCodes,$uploadedProtocolCodes);

        $results = [];
        foreach ($uploadedProtocolCodes as $upc) {
            $results[] = ebp_protocols::protocols([
                'url' => 'protocols/',
                'method' => 'DELETE',
                'code' => $upc,
                'return' => 'html'
            ]);
        }
        print json_encode($results,JSON_PRETTY_PRINT);
        return;
    }

    static function postProtocols() {

        $protocols = call_user_func([$this,'getProtocols']);

        $currentProtocolCodes = array_column($protocols['current'],'protocol_code');
        $uploadedProtocolCodes = array_column($protocols['uploaded'],'protocol_code');

        $newProtocols = array_diff($currentProtocolCodes,$uploadedProtocolCodes);

        $results = [];
        foreach ($protocols['current'] as $sp) {
            if (in_array($sp['protocol_code'], $newProtocols)) {
                $results[] = $this->protocols([
                    'url' => 'protocols/',
                    'method' => 'POST',
                    'code' => $sp['protocol_code'],
                    'return' => 'html'
                ]);
            }
        }
        print json_encode($results,JSON_PRETTY_PRINT);
        return;
    }

    static function protocols( $args = [] ) {
        global $ID;

        $defaults = [
            'url' => 'protocols/',
            'method' => 'GET',
            'code' => null,
            'return' => 'html',
        ];

        foreach ($defaults as $key => $value) {
            if (!isset($args[$key]))
                $args[$key] = $value;
        }

        if ($args['method'] == 'POST') 
            $url = $args['url'];
        else
            $url = ($args['code']) ? $args['url'] . $args['code'] : $args['url'];

        $postfields = [];
        // $fp = fopen(dirname(__FILE__).'/errorlog.txt', 'a+');
        if ($args['method'] == 'POST') {
            if ($results = query($ID,sprintf("SELECT citation,end_year,protocol_code,title,description,fixed_list_tags,link,ongoing,protocol_details,website,ebp_data_structure,geographic_coverage,method,project_type,start_year FROM ebp.protocols WHERE protocol_code = %s;", quote($args['code'])))) {
                if (!$protocol = pg_fetch_object($results[0])) {
                    $this->error = 'protocol not found';
                    return;
                }
            }
            $protocol->ongoing = ($protocol->ongoing === 'f') ? false : true;
            if ($protocol->end_year === 0) $protocol->end_year = null;
            $postfields = json_encode($protocol, JSON_NUMERIC_CHECK);

        }
        
        $protocols = ebp_curl::get([
            'url' => $url,
            'auth' => true,
            'followlocation' => true,
            'method' => $args['method'],
            'info_header_out' => true,
            'postfields' => $postfields,
        ]);

        //fclose($fp);
        
        if ($args['return'] == 'html')
            return common_message("success","{$protocols['http_status']}: {$protocols['results']->protocol}");
        elseif ($args['return'] == 'array')
            return $protocols;
    }


    static function getProtocols() {
        global $ID;

        $protocols = ebp_curl::get([
            'url' => 'protocols/',
            'auth' => true,
        ]);

        $uploadedProtocols = [];
        foreach ($protocols as $prot) {
            $uploadedProtocols[$prot->protocol_code] = $prot;
        }

        $currentProtocols = [];

        if ($results = query($ID,"SELECT * FROM ebp.protocols ORDER BY protocol_code;")) {
            while ($row = pg_fetch_assoc($results[0])) 
                $currentProtocols[$row['protocol_code']] = $row;
        }

        return ['current' => $currentProtocols, 'uploaded' => $uploadedProtocols];
    }

}
?>
