<?php
require_once('ebp_species.php');
require_once('ebp_protocols.php');
require_once('ebp_data.php');

class ebp_curl {
    public static $cr;

    static function get( $args = [] ) {

        $handle = curl_init();
        self::loadCredentials();
        
        self::setDefaultArgs($args);

        curl_setopt($handle, CURLOPT_URL, $args['url']);
        if ($args['auth']) {
            $token = self::getToken();
            $authorization = "Authorization: Bearer $token";
            curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        }
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, $args['returntransfer']);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, $args['followlocation']);

        if ($args['gettoken']) {
            curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($handle, CURLOPT_USERPWD, $args['userpwd']);
        }
        if ($args['method'] == 'POST') {
            curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $args['postfields']);
        }
        elseif ($args['method'] == 'DELETE') {
            curl_setopt($handle, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        if ($args['info_header_out']) {
            curl_setopt($handle, CURLINFO_HEADER_OUT, true);
        }
        $output = curl_exec($handle);

        if (curl_error($handle)) {
            $err = curl_error($handle);
            log_action($err,__FILE__,__LINE__);
            return common_message('fail',$err);
        }

        if ($args['info_header_out']) {
            $status_code = curl_getinfo($handle, CURLINFO_HTTP_CODE);   //get status code
            curl_close($handle);
            return ['results' => json_decode($output), 'http_status' => $status_code ];
        }

        curl_close($handle);

        return json_decode($output);
    }


    static function setDefaultArgs(&$args) {
        $apiURL = self::$cr->apiURL;
        $defaults = [
            'method' => 'GET',
            'returntransfer' => true,
            'gettoken' => false,
            'info_header_out' => false,
            'auth' => false,
            'followlocation' => false,
        ];

        if (!isset($args['url'])) {
            throw new Exception("url not provided", 1);
        }

        $args['url'] = $apiURL . $args['url'];
        foreach ($defaults as $key => $value) {
            if (!isset($args[$key]))
                $args[$key] = $value;
        }
    }

    static function getToken() {
        if ($token = self::loadValidToken()) {
            return $token;
        }
        $fields = [
            "grant_type" => "password",
            "username" => self::$cr->username,
            "password" => self::$cr->password,
            "scope" => 'api'
        ];

        $results = self::get([
            'url' => 'oauth/token/',
            'method' => 'POST',
            'postfields' => $fields,
            'gettoken' => true,
            'userpwd' => self::$cr->clientID. ":" . self::$cr->clientSecret,
            'info_header_out' => true
        ]);

        if (!isset($results['http_status'])) {
            throw new Exception("No response from the EBP server.", 1);
        }
        if (isset($results['http_status']) && $results['http_status'] != '200') {
            job_log( $results['results'], __FILE__,__LINE__); 
            throw new Exception("HTTP response: {$results['http_status']}", 1);
        }

        $expirationTime = (new DateTime())->add(new DateInterval('PT' . $results['results']->expires_in . 'S'));

        return self::saveToken($results['results']->access_token, $expirationTime->format('Y-m-d H:i:s'));
    }

    private static function loadValidToken() {
        global $ID;

        $cmd = "SELECT token FROM ebp.tokens WHERE expires_at > now() LIMIT 1;";
        if (!$res = pg_query($ID, $cmd)) {
            throw new Exception(__FUNCTION__ . " query error", 1);
        }
        if (pg_num_rows($res) == 0) {
            return false;
        }
        $result = pg_fetch_assoc($res);
        $token = $result['token'];
        return $token;
    }

    private static function saveToken($token, $expires_at) {
        global $ID;
        self::deletePreviousTokens();

        $cmd = sprintf("INSERT INTO ebp.tokens (token, expires_at) VALUES (%s, %s) RETURNING token;", quote($token), quote($expires_at));
        job_log($cmd, __FILE__, __LINE__);
        if (!$res = pg_query($ID, $cmd)) {
            throw new Exception(__FUNCTION__ . " query error", 1);
        }
        if (pg_num_rows($res) == 0) {
            return false;
        }
        $result = pg_fetch_assoc($res);
        $token = $result['token'];
        return $token;
    }

    private static function deletePreviousTokens() {
        global $ID;

        if (!pg_query($ID, "TRUNCATE ebp.tokens;")) {
            throw new Exception(__FUNCTION__ . " query error", 1);
        }
    }

    private static function loadCredentials() {
        $credentials = getenv('OB_LIB_DIR') . '../local/includes/modules/ebp/ebp.json';
        
        if (!file_exists($credentials)) {
            throw new Exception('credentials file missing', 1);
        }
        if (!$cr = json_decode(file_get_contents($credentials))) {
            throw new Exception('credentials file syntax error', 1);
        }
        self::$cr = $cr;
    }
}

?>
