<?php
$path = $argv[1];

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'/local_vars.php.inc');
require_once($path.'/includes/job_functions.php');

putenv("OB_LIB_DIR={$path}/includes/");


if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) {
    die("Unsuccessful connect to GIS database.");
}
if (!$BID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host)) {
    die("Unsuccesful connect to UI database.");
}
require_once($path.'includes/postgres_functions.php');
require_once($path.'includes/common_pg_funcs.php');
require_once($path.'includes/default_modules.php');
require_once( $path . 'local/includes/modules/ebp.php');
require_once( $path . 'local/includes/modules/ebp/ebp_classes.php');

try {
    #ebp_data::standardProvision(['mode' => 'T']);
    ebp_data::bulkProvision(['mode' => 'T']);
} catch (\Throwable $th) {
    job_log('EBP ERROR: ' . $th->getMessage());
}

?>
