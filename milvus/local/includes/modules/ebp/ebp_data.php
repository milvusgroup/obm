<?php

class ebp_data {

    static function bulkProvision($request = [], $bulkdate = '') {
        global $ID;

        $new_data = self::requestNewData('bulk',$bulkdate);
        if ($new_data == 'bulk data provision finished') {
            ebp::db_log("bulk_$bulkdate", common_message('success', $new_data));
            return false;
        }
        elseif ($new_data['n'] == 0) {
            ebp::db_log('requestNewData',common_message('success','no new data'));
            ebp::db_log("bulk_$bulkdate", common_message('success',json_encode(['year' => $new_data['year'], 'month' => $new_data['month']])));
            return false;
        }
        
        self::testData();
        self::prepareData();

        $provision = self::provision($request);
        
        if ($provision == 'no data to send') {
            ebp::db_log('data provision', common_message('success',$provision));
            return;
        }
        if (!$provision['results']->id) {
            ebp::db_log('data provision',common_message('error','unsuccesfull data provision'));
            return false;
        }

        if (empty($provision->errors)) {
            foreach (['events','records'] as $t) {
                if (!$res = query($ID, "WITH rows AS (UPDATE ebp.{$t}_ready SET state = 'uploaded' WHERE state IN ('new','modified') RETURNING 1) SELECT count(*) c FROM rows;")) {
                    $cmsg = common_message('error','query error');
                    ebp::db_log("$t audit processing", $cmsg);
                }
                else {
                    $result = pg_fetch_assoc($res[0]);
                    ebp::db_log("$t audit processing", common_message('success',"$t uploaded: {$result['c']}"));
                    if ($t == 'events') {
                        if (!$ress = query($ID, "WITH rows AS (UPDATE ebp.records_raw r SET status = 'uploaded' FROM ebp.events_ready er WHERE r.status = 'prepared' AND er.event_id = r.event_id AND er.state = 'uploaded' RETURNING 1) SELECT count(*) c FROM rows;")) {
                            ebp::db_log('audit processing', common_message('error','records_raw update error'));
                        }
                        $result = pg_fetch_assoc($ress[0]);
                        ebp::db_log("audit processing", common_message('success',"records_raw status changed to 'uploaded': {$result['c']}"));
                        ebp::db_log("bulk_$bulkdate", common_message('success',json_encode(['year' => $new_data['year'], 'month' => $new_data['month']])));
                    }
                }
            }
        }
    }
    
    static function standardProvision($request = []) {
        global $ID;
        
        $new_data = self::requestNewData('standard');
        if ($new_data['n'] == 0) {            
            ebp::db_log('requestNewData',common_message('success','no new data'));
        }
        job_log('vege');
        return;
        $errors = self::testData();
        $p = self::prepareData();
        $provision = self::provision($request);
            
        if ($provision == 'no data to send') {
            ebp::db_log('data provision', common_message('success',$provision));
            return;
        }
        if (!$provision['results']->audit_id) {
            ebp::db_log('data provision',common_message('error','unsuccesfull data provision'));
            return false;
        }

        $audit = self::audit($provision['results']->audit_id);

        if (empty($audit->data_log)) {
            foreach (['events','records'] as $t) {
                if (!$res = query($ID, "WITH rows AS (UPDATE ebp.{$t}_ready SET state = 'uploaded' WHERE state IN ('new','modified') RETURNING 1) SELECT count(*) c FROM rows;")) {
                    $cmsg = common_message('error','query error');
                    ebp::db_log("$t audit processing", $cmsg);
                }
                else {
                    $result = pg_fetch_assoc($res[0]);
                    ebp::db_log("$t audit processing", common_message('success',"$t uploaded: {$result['c']}"));
                    if ($t == 'events') {
                        if (!$ress = query($ID, "WITH rows AS (UPDATE ebp.records_raw r SET status = 'uploaded' FROM ebp.events_ready er WHERE r.status = 'prepared' AND er.event_id = r.event_id AND er.state = 'uploaded' RETURNING 1) SELECT count(*) c FROM rows;"))
                            ebp::db_log('audit processing', common_message('error','records_raw update error'));
                        $result = pg_fetch_assoc($ress[0]);
                        ebp::db_log("audit processing", common_message('success',"records_raw status changed to 'uploaded': {$result['c']}"));
                    }
                }
            }
        }
    }

    static function requestNewData ($mode, $bulkdate = '') {
        global $ID;

        if ($mode == 'bulk') {
            $cmd = sprintf("SELECT * FROM ebp.log WHERE action = %s ORDER BY created_at DESC LIMIT 1", quote("bulk_$bulkdate"));
            if (!$res = query($ID,$cmd)) {
                throw new Exception('log table query_error', 1);
            }
            $results = pg_fetch_assoc($res[0]);
            if (empty($results)) {
                $d = new DateTime('2010-01-01');
                $cmd = sprintf("SELECT requestebpdata('bulk',NULL,NULL,%s,%s) as n;", $d->format('Y'), $d->format('m'));
            }
            else {
                if ($results['status'] == 'success') {
                    $last = json_decode($results['message']);
                    if ($last->year == (int)date('Y') && $last->month == date('m')) {
                        self::stopJob();
                        return 'bulk data provision finished';
                    }
                    $d = new DateTime("{$last->year}-{$last->month}-01");
                    $d->add(new DateInterval('P1M'));

                    $cmd = sprintf("SELECT requestebpdata('bulk',NULL,NULL,%s,%s) as n;", $d->format('Y'), $d->format('m'));
                    //TODO
                }
                elseif ($results['status'] == 'error') {
                    //TODO
                    self::stopJob();
                    throw new Exception("error in bulk data provision", 1);
                }
            }

        }
        elseif ($mode == 'standard') {

            if (!$res = pg_query($ID,"SELECT max(uploading_date)::date as start FROM ebp.records_raw;")) {
                throw new Exception("requestNewData query_error");
            }
            $results = pg_fetch_assoc($res);
            $start = $results['start'];
            if (is_null($start)) {
                $start = '2015-12-10';
            }
            $cmd = sprintf("SELECT requestEbpData('standard',%s::date, TIMESTAMP 'yesterday'::date) as n;", quote($start));
        }

        if (!$res = pg_query($ID, $cmd)) {
            throw new Exception("requestNewData query_error");
        }
        
        $results = pg_fetch_assoc($res);
        return [
            'n' => $results['n'], 
            'year' => (isset($d)) ? $d->format('Y') : '', 
            'month' => (isset($d)) ? $d->format('m') : ''
        ];
        
    }

    static function testData($request = []) {
        global $ID;

        $ajax = (isset($request['action']) && $request['action'] == 'data_testData');
        $tests = [
            ['test' => 'event_id is null', 'cmd' => "WITH rows AS (UPDATE ebp.records_raw SET status = 'error: event_id is null' WHERE event_id IS NULL RETURNING 1) SELECT count(*) c FROM rows;"],
            ['test' => 'unique event_id', 'cmd' => "WITH rows AS (UPDATE ebp.records_raw SET status = 'error: event_id not unique' WHERE event_id IN (SELECT event_id FROM ebp.events GROUP BY event_id HAVING count( event_id ) > 1 order by event_id) RETURNING 1) SELECT count(*) c FROM rows;"],
            ['test' => 'unique record_id', 'cmd' => "WITH rows AS (UPDATE ebp.records_raw SET status = 'error: record_id not unique' WHERE record_id IN (SELECT record_id FROM ebp.records GROUP BY record_id HAVING count( record_id ) > 1 order by record_id) RETURNING 1) SELECT count(*) c FROM rows;"],
            ['test' => 'observer_ids_array IS NULL', 'cmd' => "WITH rows AS (UPDATE ebp.records_raw SET status = 'error: observer_ids_array IS NULL' WHERE status = 'new' AND observer_ids_array IS NULL RETURNING 1) SELECT count(*) c FROM rows;"],
            ['test' => 'date out of range', 'cmd' => "WITH rows AS (UPDATE ebp.records_raw SET status = 'error: date out of range' WHERE status = 'new' AND (date < '2010-01-01' OR date > TIMESTAMP 'yesterday'::date) RETURNING 1) SELECT count(*) c FROM rows;"],
            ['test' => 'duration > 24h', 'cmd' => "WITH rows AS (UPDATE ebp.records_raw SET status = 'error: duration > 24h' WHERE status = 'new' AND duration >= 24 RETURNING 1) SELECT count(*) c FROM rows;"],
        ];

        $results = [];
        foreach ($tests as $test) {
            if (!$res = pg_query($ID, $test['cmd'])) {
                throw new Exception("Test {$test['test']} failed: " . pg_last_error($ID), 1);
            }
            $r = pg_fetch_assoc($res);
            if ($r['c'] > 0) {
                ebp::db_log('testData', common_message('warning', "{$test['test']}: {$r['c']}"));
            }
            $results[$test['test']] = $r['c'];
        }
        return ($ajax) ? json_encode($results) : $results;

    }

    static function prepareData($request = []) {
        global $ID;

        $preparation = [
            ["label" => "new data to previous event", "cmd" => "WITH rows as (UPDATE ebp.records_raw set status = 'new' where status = 'uploaded' and event_id IN (SELECT DISTINCT event_id FROM ebp.records_raw WHERE status = 'new') RETURNING 1) SELECT count(*) c FROM rows;"],
            ["label" => "replace uploaded events", "cmd" => "WITH rows as (DELETE FROM ebp.events_ready WHERE event_id IN (SELECT event_id from ebp.events) RETURNING 1) SELECT count(*) c FROM rows;"],
            ["label" => "replace uploaded records", "cmd" => "WITH rows as (DELETE FROM ebp.records_ready WHERE record_id IN (SELECT record_id from ebp.records) RETURNING 1) SELECT count(*) c FROM rows;"],
            ["label" => "events prepared", "cmd" => "WITH rows as (INSERT INTO ebp.events_ready SELECT event_id, row_to_json(foo) as event, 'new'::varchar as state FROM (SELECT data_type, date, duration, event_id, location_mode, location, observer, protocol_id, radius, records, time, 1::integer as state FROM ebp.events) as foo RETURNING 1) SELECT count(*) c FROM rows;"],
            ["label" => "records prepared", "cmd" => "WITH rows as (INSERT INTO ebp.records_ready SELECT record_id, row_to_json(foo) as record, 'new'::varchar as state FROM (SELECT breeding_code, \"count\", event_id, flying_over, record_id, records_of_species, species_code, 1::integer as state FROM ebp.records) as foo RETURNING 1) SELECT count(*) c FROM rows;"],
            ["label" => "records_raw status updated to 'prepared'", "cmd" => "WITH rows AS (UPDATE ebp.records_raw SET status = 'prepared' WHERE status = 'new' AND event_id IN (SELECT event_id FROM ebp.events_ready WHERE state = 'new') RETURNING 1) SELECT count(*) c FROM rows;"],
        ];

        $prep_results = [];
        foreach ($preparation as $task) {
            if (!$res = pg_query($ID, $task['cmd'])) {
                throw new Exception("Data preparation {$task['label']} failed: " . pg_last_error($ID), 1);
            }
            
            
            $results = pg_fetch_assoc($res);
            ebp::db_log('prepareData',common_message('ok', "{$task['label']}: {$results['c']}"));
            $prep_results[$task['label']] = $results['c'];
        }

        return $prep_results;
        
    }

    static function provision($request = []) {
        global $ID;

        $mode = (isset($request['mode'])) ? $request['mode'] : 'T';
        
        $partner_source = 'RO2_OBM';


        if (!$res = pg_query($ID, "SELECT * FROM ebp.events_ready WHERE state = 'new';")) {
            throw new Exception(pg_last_error($ID), 1);
        }
        if (pg_num_rows($res) == 0) {
            return 'no data to send';
        }

        $events = [];
        $dates = [];
        while ($row = pg_fetch_assoc($res)) {
            $event = json_decode($row['event']);
            $dates[] = $event->date;
            $events[] = $event;
        }    
        $start_date = min($dates);
        $end_date = max($dates);

        
        if (!$res = pg_query($ID, "SELECT * FROM ebp.records_ready WHERE state = 'new';")) {
            throw new Exception(pg_last_error($ID), 1);
        }
        while ($row = pg_fetch_assoc($res)) {
            $records[] = json_decode($row['record']);
        }

        $dataProvision = compact('mode', 'partner_source', 'events', 'records', 'start_date', 'end_date');
        
        unset($events);
        unset($records);
        #job_log($dataProvision);
        #file_put_contents(getenv('PROJECTDIR'). '/local/downloads/bulk.json', json_encode($dataProvision));
        $result = ebp_curl::get([
            'auth' => true,
            'url' => 'data/',
            'method' => 'POST',
            'followlocation' => true,
            'postfields' => json_encode($dataProvision),
            'info_header_out' => true,
        ]);
        unset($dataProvision);
        job_log($result);
        return $result;

    }

    static function auditLast($request = []) {
        $ajax = (isset($request['action']) && $request['action'] == 'data_auditLast');
        $lastAudit = self::audit('last');
        return ($ajax) ? json_encode($lastAudit, JSON_PRETTY_PRINT) : $lastAudit;
    }

    static function auditCode($request) {
        return json_encode(self::audit($request['code']));
    }

    static function audit($id) {
        if ($id == 'last') {

            $audit = ebp_curl::get([
                'url' => 'audit/last',
                'auth' => true,
            ]);

        }
        else {
            $code = preg_replace('/[^0-9]/','',$id);
            $audit = ebp_curl::get([
                'url' => "audit/$code",
                'auth' => true,
            ]);

        } 
        ebp::db_log('audit',common_message('ok',json_encode($audit)));
        return $audit;
    }

    private function test_event($e) {
        $fields = ['event_id','data_type','date','time','location_mode','location','protocol_id','radius','duration','records','observer','protocol_id','state'];

        foreach ($fields as $field) {
            if (!array_key_exists($field, $e)) {
                log_action("$field field is missing",__FILE__,__LINE__);
                return false;
            }
        }
 
        if ($e['data_type'] === 'F') {
            if ($e['protocol_id'] === null) {
                log_action('protocol_id is null with fixed list',__FILE__,__LINE__);
                return false;
            }
            if (!in_array($e['location_mode'],['E','D'])) {
                log_action('location mode not E or D for fixed list',__FILE__,__LINE__);
                return false;
            }
        }
        if ($e['data_type'] === 'L') {
            if (!in_array($e['location_mode'],['E','D'])) {
                log_action('location mode not E or D for complete list',__FILE__,__LINE__);
                return false;
            }
        }
        elseif ($e['data_type'] === 'C') {
            if ($e['location_mode'] !== 'A') {
                log_action('location mode not A for casual record',__FILE__,__LINE__);
                return false;
            }
        }
        if ($e['location_mode'] == 'A') {
            if ($e['time'] !== null || $e['radius'] !== null || $e['duration'] !== null) {
                log_action('time, radius or duration not null with aggregated data',__FILE__,__LINE__);
                return false;
            }
            if (!preg_match('/^10kmE\d{3}N\d{3}$/',$e['location'],$m)) {
                log_action('etrs code pattern do not match',__FILE__,__LINE__);
                return false;
            }
        }
        elseif ($e['location_mode'] == 'E') {
            if (!preg_match('/^POINT ?\(/',$e['location'],$m)) {
                log_action('etrs code pattern do not match',__FILE__,__LINE__);
                return false;
            }
        }
        return true;
    }

    static function stopJob() {
        $jobs = read_jobs();
        unset($jobs['ebp_bulk_cron.php']);
        return (write_jobs($jobs)) 
            ? ebp::db_log('stopJob',common_message('success','jobs stopped'))
            : ebp::db_log('stopJob',common_message('error','jobs NOT stopped'));

        //TODO
    }

}
?>
