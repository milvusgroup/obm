function ebp_js() {
    $(document).ready(function() {
        $(".ebpTables").off().on("click", async function(ev) {
            ev.preventDefault();
            try {
                const action = $(this).data("f");
                let start_date = null;
                let end_date = null;
                let code = null;
                if (action == 'data_auditCode') {
                    code = $('#auditCode').val();
                    console.log(code);
                }
                if (action == 'prepareEvents') {
                    start_date = pgFormatDate($('#start_date')[0].valueAsDate);
                    end_date = pgFormatDate($('#end_date')[0].valueAsDate);
                    if (start_date > end_date)
                        throw 'start date greater than end date';
                }

                const html = await $.get("ajax",{ "m": "ebp", "action": action, "start_date": start_date, "end_date": end_date, "code": code });
                $("#"+action).html(html);
                if (action == "species_table")
                    speciesTableEvents();
            }
            catch (error) {
                alert(error);
                console.log(error);
            }
        });
        $('body').off().on("click",".ebpActions", async function(ev) {
            ev.preventDefault();
            try {
                const action = $(this).data("f");
                const method = $(this).data("method");
                const code = $(this).data("code");
                const definition = $(this).parent().find('.view-definition').val();
                let conf = true;
                if (action == 'init_dropSchemaAndTables') {
                    conf = confirm('Are you sure? All ebp data will be lost!!!');
                }

                if (conf) {
                    const data = await $.get("ajax",{ "m": "ebp", "action": action, "method" : method, "code": code, "definition": definition});
                    if (data)
                        showDialog(data);
                }
            }
            catch (error) {
                console.log(error);
            }
        });
        $("#prepareEventsForm").off().on("submit", async function(ev) {
            ev.preventDefault();
            const formData = new FormData(this);
            for (var pair of formData.entries()) {
                console.log(pair[0] + ': ' + pair[1]);
            }
        });
    });

    function speciesTableEvents() {
        $("table#ebpSpeciesTable td:nth-child(3)").on("click", async function(ev) {
            ev.preventDefault();
            try {
                const list = await $.getJSON("ajax",{"m": "ebp", "action": "species_getLocalList"});
                let html = "<select class=\'taxonAssign\'><option disabled hidden selected> Select species ... </option>";
                $.each(list,function(k,v) {
                    html += "<option data-id=\'" + v.taxon_id + "\'>"+v.word+"</option>";
                });
                html += "</select>";
                $(this).html(html);
                $(this).off();
                $(".taxonAssign").on("change", async function(ev) {
                    const local_id = $(this).find("option:selected").data("id");
                    const ebp_id = $(this).parent().next().html();
                    const resp = await $.post("ajax",{"m":"ebp", "action": "species_setLocalId", "local_id": local_id ,"ebp_id": ebp_id });
                    if (showDialog(resp) == "success") {
                        $(this).parent().html(local_id);
                        $("#showSpeciesTable").trigger("click");
                    }

                });
            }
            catch (error) {
                console.log(error);
            }
        });
    }

    function showDialog(data) { 
        const retval = jsendp(data);
        let txt = "";
        if (retval["status"]=="error") 
            txt = retval["message"];
        else if (retval["status"]=="fail")
            txt = "FAIL: "+retval["data"];
        else if (retval["status"]=="success") {
            txt = "SUCCESS: " + retval["data"];
            $(this).attr("disabled","disabled");
        }
        $( "#dialog" ).text(txt);
        var isOpen = $( "#dialog" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog" ).dialog( "open" ); 

        return retval["status"];
    }
    function pgFormatDate(date) {
        /* Via http://stackoverflow.com/questions/3605214/javascript-add-leading-zeroes-to-date */
        function zeroPad(d) {
            return ("0" + d).slice(-2)
        }

        var parsed = new Date(date)

        return [parsed.getUTCFullYear(), zeroPad(parsed.getMonth() + 1), zeroPad(parsed.getDate())].join("-");
    }
}
