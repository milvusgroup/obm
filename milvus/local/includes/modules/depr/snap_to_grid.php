<?php
class snap_to_grid {
    function  geom_column($p) {

        $t = PROJECTTABLE.'.'.$_SESSION['st_col']['GEOM_C'];
        /*$ts = 'snap_geometry';

        $grp = 0;
        $Tid = 0;
        if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='') $grp = $_SESSION['Tgroups'];
        if (isset($_SESSION['Tid'])) $Tid = $_SESSION['Tid'];*/

        /*$t = sprintf('
            CASE 
                WHEN access_to_data IN (1,2) AND (((ARRAY['.$Tid.'] && owners)=FALSE AND (ARRAY['.$grp.'] && groups)=FALSE) OR groups IS NULL) 
                    THEN NULL 
                ELSE %1$s
                END',$t,$ts);*/
        $params = preg_split('/;/',$p);
        if (count($params)==2) {
            $t = sprintf("ST_AsText(ST_SnapToGrid(%s,%f,%f))",$t,$params[0],$params[1]);
        } else {
            $t = sprintf("ST_AsText(ST_SnapToGrid(%s,0.13,0.09))",$t);
        }

        return $t;
    }
    function geom_column_join($p) {
        global $ID;

        $grp = 0;
        $Tid = 0;
        if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='') $grp = $_SESSION['Tgroups'];
        if (isset($_SESSION['Tid'])) $Tid = $_SESSION['Tid'];

        $cmd = sprintf('CREATE UNLOGGED TABLE IF NOT EXISTS temporary_tables.rules_%1$s_%2$s (row_id integer,rule integer);
                         DELETE FROM temporary_tables.rules_%1$s_%2$s;
                         INSERT INTO temporary_tables.rules_%1$s_%2$s
                            SELECT obm_id,
                            CASE 
                                WHEN access_to_data=2 AND (((ARRAY[%2$s] && owners)=FALSE AND (ARRAY[%3$s] && groups)=FALSE) OR groups IS NULL) 
                                    THEN 3 
                                WHEN access_to_data=1 AND (((ARRAY[%2$s] && owners)=FALSE AND (ARRAY[%3$s] && groups)=FALSE) OR groups IS NULL) 
                                    THEN 2 
                                ELSE 1
                            END
                            FROM %1$s
                            LEFT JOIN %1$s_rules ON ("table"=\'%1$s\' AND obm_id=row_id)
                            WHERE sensitive IS NULL OR sensitive=FALSE OR (sensitive=TRUE AND (%2$s=ANY(owners) OR groups && ARRAY[%3$s]))',PROJECTTABLE,$Tid,$grp);

        $res = pg_query($ID,$cmd);

        $t = sprintf('CASE 
                           WHEN rule=3 
                                THEN snap_geometry 
                           WHEN rule=2 
                                THEN NULL 
                           ELSE snap_geometry
                       END');
        return $t;
    }
    function rules_join($p) {
        $Tid = 0;
        if (isset($_SESSION['Tid'])) $Tid = $_SESSION['Tid'];

        $rules_join = sprintf('LEFT JOIN temporary_tables.rules_%1$s_%2$s ON (obm_id=row_id)',PROJECTTABLE,$Tid);

        return $rules_join;
    }
}
/*
                WHEN access_to_data=2 AND (((ARRAY['.$_SESSION['Tid'].'] && owners)=FALSE AND (ARRAY['.$grp.'] && groups)=FALSE) OR groups IS NULL) 
                    THEN ST_SnapToGrid(%1$s,0.13,0.09) 
                WHEN access_to_data=1 AND (((ARRAY['.$_SESSION['Tid'].'] && owners)=FALSE AND (ARRAY['.$grp.'] && groups)=FALSE) OR groups IS NULL) 
                    THEN NULL 
                ELSE %1$s 
*/
?>
