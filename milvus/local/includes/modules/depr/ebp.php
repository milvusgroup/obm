<?php
/**
 * EuroBirdPortal data provision module 
 *
 * 
 * @author Bóné Gábor <gabor.bone@milvus.ro>
 * @version 1.0
 */
class ebp {
    var $apiURL = 'https://api.eurobirdportal.org/';
    var $error = '';
    var $retval;
    var $token;
    var $credentials;

    function __construct($action = null, $params = null) {
        global $BID;
/*        if (!$this->update_db())
            $this->error = 'update_db error!';
 */
        $res = pg_query($BID,"SELECT email FROM projects WHERE project_table='".PROJECTTABLE."'");
        $row = pg_fetch_assoc($res);
        $this->project_email = $row['email'];
        $this->credentials = getenv('OB_LIB_DIR') . 'modules/private/ebp/ebp.json';


        if (!isset($_SESSION['ebp_token']))
            $this->getToken();
        else
            $this->token = $_SESSION['ebp_token'];

        if ($action)
            $this->retval = $this->$action($params);

    }

    public function getMenuItem() {
        return ['label' => 'EuroBirdPortal', 'url' => 'ebp' ];
    }

    public function adminPage() {

        global $BID, $ID;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

//        $js = (file_exists(getenv('PROJECT_DIR').'modules/private/ebp/ebp.js')) ? file_get_contents(getenv('PROJECT_DIR').'modules/private/ebp/ebp.js') : '';
        $css = (file_exists(getenv('OB_LIB_DIR').'modules/private/ebp/ebp.css')) ? '<link rel="stylesheet" href="'.$protocol . '://' . URL . '/includes/modules/private/ebp/ebp.css?rev='.rev( getenv('OB_LIB_DIR').'modules/private/ebp/ebp.css').'" type="text/css" />' : '';
        
        $out = "";//"<script> $js </script>";
        
        $out .= $css;
        $out .= '<div id="ebp">';
        if (!grst(PROJECTTABLE,'master')) {
            $out .= '<p>'. str_no_access_to_this_function."!</p></div>";
            return $out;
        }

        $dis = (!$this->existsSchemaAndTables()) ?  '' : 'disabled' ;
        $out .= '<main>
  <input id="tab1" class="tabInput" type="radio" name="tabs" checked>
  <label for="tab1">Species</label>

  <input id="tab2" class="tabInput" type="radio" name="tabs">
  <label for="tab2">Protocols</label>

  <input id="tab3" class="tabInput" type="radio" name="tabs">
  <label for="tab3">Prepare data</label>

  <input id="tab4" class="tabInput" type="radio" name="tabs">
  <label for="tab4">DataProvision</label>

  <input id="tab5" class="tabInput" type="radio" name="tabs">
  <label for="tab5">Settings</label>
  <section id="content1">
        
    <h5> Download EBP species list and connect with your species list</h5> 
    <button data-f="getEbpSpeciesList" class="pure-button ebpActions"> Download & connect </button>
    <h5> Show species table </h5>
    <button data-f="speciesTable" class="pure-button ebpTables" > Show </button>
<hr>
    <h5> Download the complete species list and connect with your species list</h5> 
    <button data-f="getCompleteSpeciesList" class="pure-button ebpActions"> Download & connect </button>

    <div id="speciesTable"></div>
  </section>

  <section id="content2">
        <div><button data-f="protocolsTable" class="pure-button ebpTables">Get protocols</button></div>
        <div><button data-f="postProtocols" data-method="POST" class="pure-button ebpActions">Post protocols</button></div>
        <div><button data-f="deleteProtocols" class="pure-button ebpActions">DELETE uploaded protocols</button></div>
        <div id="protocolsTable"></div>
  </section>

  <section id="content3">
        <div><form id="prepareEventsForm"><div><label for="start_date">Start date</label><input type="date" id="start_date"> </div><div><label for="start_date">Start date</label><input type="date" id="end_date"></div><div><button data-f="prepareEvents" class="pure-button ebpTables" type="submit">Prepare Events</button></div></form></div>
        <div id="prepareEvents"></div>
  </section>

  <section id="content4">
        <div><button data-f="dataProvision" class="pure-button ebpTables"> Data provision </button></div>
        <div id="dataProvision"></div>
  </section>

  <section id="content5">
        <div><button data-f="createEbpTables" class="pure-button ebpActions" '. $dis .'> Create EBP tables </button></div>
  </section>
</main>';

        $out .= '</div> <!-- #ebp -->';
        return $out;
    }

    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    public function profileSection() {
        return [
            'label' => '',
            'fa' => '',
            'href' => '',
            'data_url' => '',
            'help' => ""
        ];
    }
    public function ajax($request) {
        global $ID;

        if (isset($request['action'])) {

            $action = $request['action'];
            echo $this->$action($request);
        }
    }

    public function print_js($request) {

        $js = (file_exists(getenv('PROJECT_DIR').'modules/private/ebp/ebp.js')) ? file_get_contents(getenv('OB_LIB_DIR').'modules/private/ebp/ebp.js') : '';
        $this->retval = $js;
        return;

    }

    private function setLocalId($request) {
        global $ID;
        $local_id = quote(preg_replace('/\D/','',$request['local_id']));
        $ebp_id = quote(preg_replace('/\D/','',$request['ebp_id']));
        $cmd = sprintf("UPDATE ebp.species SET local_id = %s WHERE ebp_id = %s;", $local_id, $ebp_id);
        if ($this->query($ID, $cmd))
            return common_message('success','local_id updated');
        else
            common_message('fail','query error');

    }

    private function prepareEvents( $args = [] ) {
        global $ID;
        $defaults = [
            'start_date' => date("Y-m-d", strtotime("yesterday")),
            'end_date' => date("Y-m-d", strtotime("yesterday")),

        ];
        foreach ($defaults as $key => $value) {
            if (!isset($args[$key]))
                $args[$key] = $value;
        }

        $recordsQuery = sprintf("
            SELECT  
                protocol_code as protocol_id,
                CASE 
                    WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || '_' || g.etrs10_name
                    ELSE CASE WHEN pt.list_id IS NULL THEN pt.obm_uploading_id || '_' || pt.method ELSE pt.obm_uploading_id || '.' || pt.list_id || '_' || pt.method END
                END as event_id,
                CASE 
                    WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN 'C'
                    WHEN (pt.comlete_list = 1) THEN 'L'
                    WHEN (pt.comlete_list = 0 AND protocol_code IS NOT NULL) THEN 'F'
                END as data_type,
                date,
                CASE 
                    WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
                    ELSE make_interval(mins := duration)
                END as duration,
                CASE 
                    WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
                    ELSE make_interval(mins := time_of_start)
                END as time,
                pt.obm_geometry as geometry,
                pt.number,
                s.observer_ids AS observer_ids_array,
                g.etrs10_name as etrs_code,
                CASE
                    WHEN (u.observer_name IS NOT NULL AND regexp_split_to_array(pt.observers,E',\\s*') @> ARRAY[u.observer_name::text] ) THEN 
                        u.observer_id
                    ELSE
                        (SELECT search_id FROM milvus_search WHERE word LIKE (regexp_split_to_array(observers,E',\\s*'))[1]::text)
                END as first_observer,
                bc.breeding_code as breeding_code,
                CASE 
                    WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
                    ELSE 
                        CASE
                            WHEN status_of_individuals = 'str_overflying' THEN 'Y'
                            ELSE 'N'
                        END
                END as flying_over,
                CASE
                    WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || '_' || g.etrs10_name || '_' || tx.ebp_id
                    ELSE 'obm_id_' || pt.obm_id::varchar
                END as record_id,
                tx.ebp_id as species_code,
                tx.taxon_id as local_taxon_id
                
            FROM public.%1\$s pt 
            LEFT JOIN (SELECT * FROM system.uploadings up LEFT JOIN (SELECT search_id AS observer_id, word AS observer_name FROM milvus_search WHERE subject = 'observer') as fooo ON observer_name = uploader_name) u
                ON pt.obm_uploading_id = u.id 
            LEFT JOIN ebp.protocol_connect pc 
                ON (
                    method = obm_method AND 
                    (national_program = obm_national_program OR (national_program IS NULL AND obm_national_program IS NULL)) AND 
                    (local_program = obm_local_program OR (local_program IS NULL AND obm_local_program IS NULL))
                ) 
            LEFT JOIN ebp.breeding_codes_connect bc ON pt.status_of_individuals = bc.local_code
            LEFT JOIN public.milvus_qgrids g 
                ON g.data_table = 'milvus' AND pt.obm_id = g.row_id
            LEFT JOIN milvus_search_connect s
                ON s.data_table = '%1\$s' AND pt.obm_id = s.row_id
            LEFT JOIN (SELECT taxon_id, ebp_id, word FROM milvus_taxon tax LEFT JOIN ebp.species ebps ON tax.taxon_id = ebps.local_id) as tx 
                ON tx.word = pt.species 
            WHERE 
                (u.uploading_date::date BETWEEN %2\$s AND %3\$s) AND 
                protocol_code IS DISTINCT FROM 'excluded' AND 
                method IS NOT NULL AND
                comlete_list IS NOT NULL AND 
                g.etrs10_name IS NOT NULL", PROJECTTABLE, quote($args['start_date']), quote($args['end_date']));
        

        $cmd = sprintf("
    SELECT event_id, protocol_id, data_type, date, duration, time, location_mode, sum(records) as records, sum(observer) as observer,
            CASE 
                WHEN (data_type = 'C') THEN (SELECT name FROM shared.etrs10 g WHERE ST_Intersects(g.geometry, centroid))
                ELSE ST_AsText(centroid)
            END as location,
            CASE 
                WHEN (data_type = 'C') THEN NULL
                ELSE (SELECT radius::integer FROM ST_MinimumBoundingRadius(ST_Transform(geometryc, 31700)))
            END as radius,
            1 as state
    FROM 
        (
        SELECT event_id, protocol_id, data_type, date, duration, time, 
            CASE
                WHEN (data_type = 'C') THEN count(DISTINCT first_observer) --array_length(array_agg(DISTINCT observer_ids),1)
                ELSE max(first_observer)
            END as observer,
            CASE 
                WHEN (data_type = 'C')  THEN 'A'
                ELSE 'E'
            END as location_mode,
            ST_Collect(geometry) as geometryc,
            ST_Centroid(ST_Collect(geometry)) as centroid,
            CASE 
                WHEN (data_type = 'C') THEN count(DISTINCT (local_taxon_id, first_observer))
                ELSE count(DISTINCT local_taxon_id)
            END as records
            
        FROM 
            (SELECT *, unnest(observer_ids_array) as observer_ids FROM ( %s ) as bar) as foo
        GROUP BY event_id, protocol_id, data_type, date, duration, time
) as bar GROUP BY event_id, protocol_id, data_type, date, duration, time, location_mode, radius, geometryc, centroid ;", 

                $recordsQuery);
        //debug($cmd, __FILE__, __LINE__);

        if ($results = $this->query($ID, $cmd)) {
            $events = [];
            while ($row = pg_fetch_assoc($results[0])) 
                $events[] = $row;

            $events = $this->filter_unique_data($events,'event_id');

            if (count($events['filtered'])) {
                pg_query($ID, sprintf("DELETE FROM ebp.events WHERE event_id IN (%s);", implode(",",array_map('quote',array_column($events['filtered'],'event_id')))));

                pg_query($ID, "COPY ebp.events (event_id, event, start_date, end_date, state) FROM stdin;");
                foreach ($events['filtered'] as $r) {
                    if (! $this->test_event($r)) {
                        $events['errors'][] = $r;
                        continue;
                    }
                    unset($r['uploading_date']);
                    $line = "{$r['event_id']}\t". json_encode($r,JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE) . "\t".$args['start_date']."\t".$args['end_date']."\t1\n";
                    //debug($line,__FILE__,__LINE__);
                    pg_put_line($ID, $line);
                }
                pg_put_line($ID, "\\.\n");
                pg_end_copy($ID);
            }





            // Records
            $cmd = sprintf("
            SELECT 
                max(breeding_code) as breeding_code, 
                event_id, 
                flying_over, 
                record_id, 
                count(*) as records_of_species, 
                species_code,
                max(number) as count
            FROM (%s) as foo WHERE species_code IS NOT NULL 
            GROUP BY event_id, flying_over, record_id, species_code
            ", $recordsQuery);
            //debug($cmd, __FILE__,__LINE__);
            if ($results = $this->query($ID, $cmd)) {
            $records = [];
            $eif = array_column($events['filtered'],'event_id');
            while ($row = pg_fetch_assoc($results[0]))
                if (in_array($row['event_id'],$eif))
                    $records[] = $row;
            }

            $records = $this->filter_unique_data($records, 'record_id');
            if (count($records['filtered'])) {
                pg_query($ID, sprintf("DELETE FROM ebp.records WHERE record_id IN (%s);", implode(",",array_map('quote',array_column($records['filtered'],'record_id')))));

                pg_query($ID, "COPY ebp.records (record_id, record, state) FROM stdin;");
                foreach ($records['filtered'] as $r) {
/*                    if (! $this->test_record($r)) {
                        $records['errors'][] = $r;
                        continue;
                    }
 */
                    $line = "{$r['record_id']}\t". json_encode($r,JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE) . "\t1\n";
                    //debug($line,__FILE__,__LINE__);
                    pg_put_line($ID, $line);
                }
                pg_put_line($ID, "\\.\n");
                pg_end_copy($ID);
            }







            // Results
            $out = 'Sorok száma szűrés után: ' . count($events['filtered']) . '<br>';
            $out .= '<pre>'.json_encode($events['filtered'], JSON_PRETTY_PRINT).'</pre>';
            $out .= 'Hibás események száma:' . count($events['errors']) . '<br>';

            if (count($events['errors']))
                $out .= '<pre>'.json_encode($events['errors'], JSON_PRETTY_PRINT).'</pre>';
            $out .=  '</br>';
            $out .= 'Adatok száma szűrés után: ' . count($records['filtered']) . '</br>';
            $out .= 'Hibás adatok szama: ' . count($records['errors']) . '</br>';

            if (count($records['errors'])) { 
                $out .= '';
                $out .= '<pre>' . json_encode($records['errors'], JSON_PRETTY_PRINT) . '</pre>';
            }

            echo $out; 
        }
        else
            echo 'query error';


    }

    private function dataProvision() {
        global $ID;

        $dataProvision = [
            'mode' => 'T',
            'partner_source' => 'RO2_OBM',
        ];
        $events = [];
        $records = [];
        $cmd = [
            "SELECT * FROM ebp.events WHERE state = 1;",
            "SELECT * FROM ebp.records WHERE state = 1;"
        ];

        if (!$res = $this->query($ID, $cmd)) {
            $this->error = 'dataProvision query error!';
            return ;
        }
        
        while ($row = pg_fetch_assoc($res[0])) {
            $events[] = json_decode($row['event']);
            $dates[] = ['start_date' => $row['start_date'], 'end_date' => $row['end_date']];
        }
        for ($i = 0, $l = count($events); $i < $l; $i++) 
            $events[$i]->observer = '"'.$events[$i]->observer.'"';
            
        $start_date = min(array_column($dates, 'start_date'));
        $end_date = max(array_column($dates,'end_date'));
        while ($row = pg_fetch_assoc($res[1])) {
            $records[] = json_decode($row['record']);
        }

        $dataProvision['events'] = $events;
        $dataProvision['records'] = $records;
        $dataProvision['start_date'] = $start_date;
        $dataProvision['end_date'] = $end_date;


        echo '<pre>'; 
        var_export(json_encode($dataProvision,JSON_PRETTY_PRINT));
        echo '</pre>'; 
        $authorization = "Authorization: Bearer {$this->token}";
        $fp = fopen(dirname(__FILE__).'/errorlog.txt', 'a+');
        $handle = curl_init($this->apiURL . 'data/');
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($dataProvision, JSON_NUMERIC_CHECK));
        curl_setopt($handle, CURLINFO_HEADER_OUT, true);

        $output = curl_exec($handle);
        log_action($output,__FILE__,__LINE__);

        if (curl_error($handle)) {
            $err = curl_error($handle);
            log_action($err,__FILE__,__LINE__);
            return common_message('fail',$err);
        }

        curl_close($handle);

    }

    private function filter_unique_data($data,$unique_id) {
            $eif = array_count_values(array_column($data,$unique_id));

            $filtered = array_filter($data, function($row) use ($eif, $unique_id) {
                return ($eif[$row[$unique_id]] === 1);
            });

            $errors = array_filter($data, function($row) use ($eif, $unique_id) {
                return ($eif[$row[$unique_id]] > 1);
            });

            return compact('filtered','errors');
    }
    private function test_event($e) {
        $fields = ['event_id','data_type','date','time','location_mode','location','protocol_id','radius','duration','records','observer','protocol_id','state'];

        foreach ($fields as $field) {
            if (!array_key_exists($field, $e)) {
                log_action("$field field is missing",__FILE__,__LINE__);
                return false;
            }
        }
 
        if ($e['data_type'] === 'F') {
            if ($e['protocol_id'] === null) {
                log_action('protocol_id is null with fixed list',__FILE__,__LINE__);
                return false;
            }
            if (!in_array($e['location_mode'],['E','D'])) {
                log_action('location mode not E or D for fixed list',__FILE__,__LINE__);
                return false;
            }
        }
        if ($e['data_type'] === 'L') {
            if (!in_array($e['location_mode'],['E','D'])) {
                log_action('location mode not E or D for complete list',__FILE__,__LINE__);
                return false;
            }
        }
        elseif ($e['data_type'] === 'C') {
            if ($e['location_mode'] !== 'A') {
                log_action('location mode not A for casual record',__FILE__,__LINE__);
                return false;
            }
        }
        if ($e['location_mode'] == 'A') {
            if ($e['time'] !== null || $e['radius'] !== null || $e['duration'] !== null) {
                log_action('time, radius or duration not null whit aggregated data',__FILE__,__LINE__);
                return false;
            }
            if (!preg_match('/^10kmE\d{3}N\d{3}$/',$e['location'],$m)) {
                log_action('etrs code pattern do not match',__FILE__,__LINE__);
                return false;
            }
        }
        elseif ($e['location_mode'] == 'E') {
            if (!preg_match('/^POINT ?\(/',$e['location'],$m)) {
                log_action('etrs code pattern do not match',__FILE__,__LINE__);
                return false;
            }
        }
        return true;
    }


    private function prepareData($q) {
        global $ID;

    }
    private function speciesTable() {
        global $ID;

        $res = $this->query($ID,"SELECT * FROM ebp.species ORDER BY latin;");
        $species = [];
        while($row = pg_fetch_assoc($res[0]))
            $species[] = $row;

        $tbl = new createTable();
        $tbl->def(['tid'=>'ebpSpeciesTable','tclass'=>'resultstable']);
        $tbl->addHeader(['latin','english','local_id','ebp_id']);
        foreach ($species as $sp) {
            $tbl->addRows([
                $sp['latin'],
                $sp['english'],
                $sp['local_id'],
                $sp['ebp_id']
            ]);
        }
        return $tbl->printOut();
    }

    private function protocols( $args = [] ) {
        global $ID;

        $defaults = [
            'url' => 'protocols/',
            'method' => 'GET',
            'code' => null,
            'return' => 'html',
        ];

        foreach ($defaults as $key => $value) {
            if (!isset($args[$key]))
                $args[$key] = $value;
        }

        if ($args['method'] == 'POST') 
            $url = $args['url'];
        else
            $url = ($args['code']) ? $args['url'] . $args['code'] : $args['url'];

        $authorization = "Authorization: Bearer {$this->token}";
        $fp = fopen(dirname(__FILE__).'/errorlog.txt', 'a+');
        $handle = curl_init($this->apiURL . $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, TRUE);
//        curl_setopt($handle, CURLOPT_VERBOSE, true);
//        curl_setopt($handle, CURLOPT_FILE, $fp); 
        if ($args['method'] == 'POST') {

            if ($results = $this->query($ID,sprintf("SELECT citation,end_year,protocol_code,title,description,fixed_list_tags,link,ongoing,protocol_details,website,ebp_data_structure,geographic_coverage,method,project_type,start_year FROM ebp.protocols WHERE protocol_code = %s;", quote($args['code'])))) {
                if (!$protocol = pg_fetch_object($results[0])) {
                    $this->error = 'protocol not found';
                    return;
                }
            }
            $protocol->ongoing = ($protocol->ongoing === 'f') ? false : true;
            if ($protocol->end_year === 0) $protocol->end_year = null;
            $protocol = json_encode($protocol, JSON_NUMERIC_CHECK);

            curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $protocol);
        }
        elseif ($args['method'] == 'DELETE') {
           curl_setopt($handle, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        curl_setopt($handle, CURLINFO_HEADER_OUT, true);

        $output = curl_exec($handle);

        if (curl_error($handle)) {
            $err = curl_error($handle);
            log_action($err,__FILE__,__LINE__);
            return common_message('fail',$err);
        }

        curl_close($handle);

        $protocols = json_decode($output);

        fclose($fp);
        
        if ($args['return'] == 'html')
            return common_message('ok',$output);
        elseif ($args['return'] == 'array')
            return $protocols;
    }

    private function deleteProtocols() {

        $protocols = call_user_func([$this,'getProtocols']);

        $currentProtocolCodes = array_column($protocols['current'],'protocol_code');
        $uploadedProtocolCodes = array_column($protocols['uploaded'],'protocol_code');

        $newProtocols = array_diff($currentProtocolCodes,$uploadedProtocolCodes);

        $results = [];
        foreach ($uploadedProtocolCodes as $upc) {
            $results[] = $this->protocols([
                'url' => 'protocols/',
                'method' => 'DELETE',
                'code' => $upc,
                'return' => 'html'
            ]);
        }
        print json_encode($results,JSON_PRETTY_PRINT);
        return;
    }
    private function postProtocols() {

        $protocols = call_user_func([$this,'getProtocols']);

        $currentProtocolCodes = array_column($protocols['current'],'protocol_code');
        $uploadedProtocolCodes = array_column($protocols['uploaded'],'protocol_code');

        $newProtocols = array_diff($currentProtocolCodes,$uploadedProtocolCodes);

        $results = [];
        foreach ($protocols['current'] as $sp) {
            if (in_array($sp['protocol_code'], $newProtocols)) {
                $results[] = $this->protocols([
                    'url' => 'protocols/',
                    'method' => 'POST',
                    'code' => $sp['protocol_code'],
                    'return' => 'html'
                ]);
            }
        }
        print json_encode($results,JSON_PRETTY_PRINT);
        return;
    }

    private function getProtocols() {
        global $ID;
        $handle = curl_init();
        
        $authorization = "Authorization: Bearer {$this->token}";
        curl_setopt($handle, CURLOPT_URL, "https://api.eurobirdportal.org/protocols/");
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        $info = curl_getinfo($handle);

        $output = curl_exec($handle);
        curl_close($handle);

        $uploadedProtocols = [];
        foreach (json_decode($output) as $prot) {
            $uploadedProtocols[$prot->protocol_code] = $prot;
        }

        $currentProtocols = [];

        if ($results = $this->query($ID,"SELECT * FROM ebp.protocols ORDER BY protocol_code;")) {
            while ($row = pg_fetch_assoc($results[0])) 
                $currentProtocols[$row['protocol_code']] = $row;
        }

        return ['current' => $currentProtocols, 'uploaded' => $uploadedProtocols];
    }
    private function protocolsTable($args = []) {

        $protocols = call_user_func([$this,'getProtocols']);

        $currentProtocolCodes = array_column($protocols['current'],'protocol_code');
        $uploadedProtocolCodes = array_column($protocols['uploaded'],'protocol_code');

        $newProtocols = array_diff($currentProtocolCodes,$uploadedProtocolCodes);

        $tbl = new createTable();
        $tbl->def(['tid'=>'protocolsTable','tclass'=>'resultstable']);
$protocolColumns = ['citation','end_year','protocol_code','title','description','fixed_list_tags','link','ongoing','protocol_details','website','ebp_data_structure','geographic_coverage','method','project_type','start_year'];
        $tbl->addHeader(array_merge(['status'],$protocolColumns));
        foreach ($protocols['uploaded'] as $sp) {
            $cols = [sprintf('<button class="pure-button button-sm button-warning ebpActions" data-f="protocols" data-method="DELETE" data-code="%s"> delete </button>',$sp->protocol_code)];
            foreach($protocolColumns as $col) 
                $cols[] = $sp->$col;
            $tbl->addRows($cols);
        }
        foreach ($protocols['current'] as $sp) {
            if (in_array($sp['protocol_code'], $newProtocols)) {
                $cols = [sprintf('<button class="pure-button button-sm ebpActions" data-f="protocols" data-method="POST" data-code="%s"> upload </button>',$sp['protocol_code']) ];
                foreach($protocolColumns as $col) 
                    $cols[] = $sp[$col];
                $tbl->addRows($cols);
            }
        }
        return $tbl->printOut();
    }
    private function getLocalSpeciesList() {
        global $ID;

        $cmd = sprintf("SELECT taxon_id, word FROM %s_taxon ORDER BY word;", PROJECTTABLE);
        if ($results = $this->query($ID, $cmd)) {
            $list = [['taxon_id' => 0, 'word' => 'N/A']];
            while ($row = pg_fetch_assoc($results[0]))
                $list[] = $row;
            return json_encode($list);
        }
    }

    private function getCompleteSpeciesList () {
        global $ID;
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, "https://api.eurobirdportal.org/species/");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($handle);
        curl_close($handle);

        $species = json_decode($output);
        pg_query($ID, 'TRUNCATE "ebp"."all_species";');
        pg_query($ID, 'COPY "ebp"."all_species" ("ebp_id", "latin", "english") FROM STDIN');

        foreach ($species as $sp) {
            pg_put_line($ID, "{$sp->species_id}\t{$sp->latin}\t{$sp->english}\n");
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

        $cmd = [
            sprintf('UPDATE ebp.all_species SET local_id = t.taxon_id FROM %s_taxon t WHERE latin = t.word;',PROJECTTABLE),
            "SELECT count(*) c FROM ebp.all_species WHERE local_id IS NULL;"
        ];
        if ($results = $this->query($ID, $cmd)) {
            $state = pg_fetch_assoc($results[1]);
            return common_message('success', "{$state['c']} taxons are not assigned");
        }
        else
            common_message('fail','query error');

        return common_message('success','ok');
    }
    private function getEbpSpeciesList () {
        global $ID;
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, "https://api.eurobirdportal.org/species/ebp");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($handle);
        curl_close($handle);

        $species = json_decode($output);
        pg_query($ID, 'TRUNCATE "ebp"."species";');
        pg_query($ID, 'COPY "ebp"."species" ("ebp_id", "latin", "english") FROM STDIN');

        foreach ($species as $sp) {
            pg_put_line($ID, "{$sp->species_id}\t{$sp->latin}\t{$sp->english}\n");
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://api.eurobirdportal.org/breeding_code/");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($handle);
        curl_close($handle);

        pg_query($ID, 'TRUNCATE "ebp"."breeding_codes";');
        pg_query($ID, 'COPY "ebp"."breeding_codes" ("breeding_code", "breeding_code_text") FROM STDIN');

        foreach (json_decode($output) as $bc) {
            pg_put_line($ID, "{$bc->breeding_code}\t{$bc->breeding_code_text}\n");
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);


        $this->assignTaxonIds();


        pg_query($ID, "TRUNCATE ebp.breeding_codes_connect;");
        if (file_exists('ebp/breeding_codes.sql')) {
            $cmd = file_get_contents('ebp/breeding_codes.sql');
            pg_query($ID, $cmd);
        }

        return common_message('success','ok');
    }
    private function assignTaxonIds() {
        global $ID;

        $cmd = [
            sprintf('UPDATE ebp.species SET local_id = t.taxon_id FROM %s_taxon t WHERE latin = t.word;',PROJECTTABLE),
            "SELECT count(*) c FROM ebp.species WHERE local_id IS NULL;"
        ];
        if ($results = $this->query($ID, $cmd)) {
            $state = pg_fetch_assoc($results[1]);
            return common_message('success', "{$state['c']} taxons are not assigned");
        }
        else
            common_message('fail','query error');
    }

    private function getToken() {
        if (!file_exists($this->credentials)) {
            $this->error = 'ebp credetials file missing';
            return common_message('fail',$this->error);
        }
        if (!$cr = json_decode(file_get_contents($this->credentials))) {
            $this->error = 'ebp credentials read error';
            return common_message('fail',$this->error);
        }


        $fields = [
            "grant_type" => "password",
            "username" => $cr->username,
            "password" => $cr->password,
            "scope" => 'api'
        ];


        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://api.eurobirdportal.org/oauth/token");
        curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($handle, CURLOPT_USERPWD, "$cr->clientID:$cr->clientSecret");
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($handle, CURLINFO_HEADER_OUT, true);

        $result=curl_exec($handle);
        $status_code = curl_getinfo($handle, CURLINFO_HTTP_CODE);   //get status code
        curl_close ($handle);

        if ($status_code != '200') {
            $this->error = $result;
            return common_message('fail', "HTTP response: $status_code");
        }

        $result = json_decode($result);
        $this->token = $result->access_token;
        $_SESSION['ebp_token'] = $this->token;

        return common_message('ok'); 

    }

    private function existsSchemaAndTables() {
        if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
            die("Unsuccessful connect to GIS database.");

        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.schemata WHERE catalog_name = 'gisdata' AND schema_name = 'ebp');");
        $res = $this->query($GID,$cmd);
        $result = pg_fetch_assoc($res[0]);

        if ($result['exists'] === 'f')
            return false;
        else
            return true;
    }


    private function createEbpTables() {
        global $ID;
        if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
            die("Unsuccessful connect to GIS database.");

        if ($this->existsSchemaAndTables())
            return common_message('fail','schema already exists');

        $cmd = [
            'CREATE SCHEMA "ebp";',
            sprintf('GRANT USAGE, CREATE ON SCHEMA ebp TO %s;', gisdb_user)
        ];

        if (!$this->query($GID, $cmd))
            return common_message('fail','query error');

        $cmd = [
            'CREATE TABLE "ebp"."species" ( "local_id" integer, "ebp_id" integer NOT NULL, "latin" character varying NOT NULL, "english" character varying, CONSTRAINT "species_ebp_id" UNIQUE ("ebp_id")) WITH (oids = false);',
            'CREATE SEQUENCE ebp.protocols_obm_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;',
            'CREATE TABLE "ebp"."protocols" ( "obm_id" integer DEFAULT nextval(\'ebp.protocols_obm_id_seq\') NOT NULL, "protocol_code" character varying NOT NULL, "title" character varying NOT NULL, "project_type" character varying NOT NULL, "method" character(1) NOT NULL, "website" character varying, "description" character varying, "protocol_details" character varying, "ebp_data_structure" character varying NOT NULL, "citation" character varying NOT NULL, "id_gbif" character varying, "geographic_coverage" character varying NOT NULL, "start_year" character varying NOT NULL, "end_year" character varying, "ongoing" character varying NOT NULL, "link" character varying, "fixed_list_tags" text NOT NULL, CONSTRAINT "protocols_obm_id" PRIMARY KEY ("obm_id"), CONSTRAINT "protocols_protocol_code" UNIQUE ("protocol_code")) WITH (oids = false);',
            'CREATE TABLE "ebp"."events" ( "event_id" character varying NOT NULL, "event" json NOT NULL, "state" integer NOT NULL, "start_date" date, "end_date" date, CONSTRAINT "events_event_id" UNIQUE ("event_id")) WITH (oids = false);',
            'CREATE TABLE "ebp"."protocol_connect" ( "obm_method" character varying(128), "obm_national_program" character varying(128), "obm_local_program" character varying(128), "comments" character varying(128), "protocol_code" character varying(128)) WITH (oids = false);',
            'CREATE TABLE "ebp"."records" ( "record_id" character varying NOT NULL, "record" json NOT NULL, "state" integer, CONSTRAINT "records_record_id" UNIQUE ("record_id")) WITH (oids = false);',
            'CREATE TABLE "ebp"."breeding_codes" ( "breeding_code" integer NOT NULL, "breeding_code_text" character varying NOT NULL) WITH (oids = false);',
            'CREATE TABLE "ebp"."breeding_codes_connect" ( "breeding_code" integer NOT NULL, "local_code" character varying NOT NULL) WITH (oids = false);',
        ];


        // 'CREATE TABLE "ebp"."etrs_intersections" ( "row_id" integer, "etrs_geom" public.geometry(Polygon,4326), "etrs_name" character varying) WITH (oids = false);'

        if (file_exists('ebp/protocols.sql')) {
            $cmd[] = file_get_contents('ebp/protocols.sql');

        }

        if (!$this->query($ID, $cmd))
            return common_message('fail','query error');
        else
            return common_message('success','schema and tables created');

    }
    private function query($db,$cmd) {
        if (gettype($cmd) === "string")
            $cmd = [$cmd];

        pg_query($db, "BEGIN;");
        $cmdStr = implode('',$cmd);
        if (pg_send_query($db, $cmdStr)) {
            $results = [];
            for ($i = 0, $l = count($cmd); $i < $l; $i++) {
                $res = pg_get_result($db);
                $results[] = $res;
                $state = pg_result_error($res);
                if ($state != '') {
                    $this->error = $state;
                    pg_query($db,'ROLLBACK;');
                    return false;
                }
            }
            pg_query($db,'COMMIT;');
            return $results;
        }
        else {
            $this->error = "pg_send_query failed!";
            return false;
        }
    }
}

?>
