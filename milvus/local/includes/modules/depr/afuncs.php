<?php
/* Milvus-TIR 
 * program-gyűjtőkód webes feltöltő formon.
 * default values modul
 * */

if (session_status() == PHP_SESSION_NONE) {
    debug('private_afuncs',__FILE__,__LINE__);
        session_start();
}

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once('custom_default_modules.php');

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'languages.php');

if (isset($_POST['pga'])) {
    require('box_filter.php');

    // PROGRAM ajax
    if ($_POST['pga'] == 'program') {
        global $ID; 
        $rules = box_filter::sensitivity_check('milvus_grouping_codes');
        $cmd = sprintf("SELECT t.obm_id as id,gykod FROM milvus_program p LEFT JOIN milvus_grouping_codes t ON (p.obm_id=t.program) %s WHERE program=%d %s ORDER BY gykod",$rules['join'], $_POST['program'], $rules['filter']);
        //$cmd = sprintf("SELECT array_to_string(array_agg(gykod ORDER BY gykod),',') AS gy FROM milvus_programs p LEFT JOIN milvus_gykodok gyk ON (p.id=gyk.program) WHERE program=%d ORDER BY gy",$_POST['program']);
        $res = pg_query($ID,$cmd);
        if ($res and pg_num_rows($res)) {
            while ($row = pg_fetch_assoc($res)) {
                $gy[$row['gykod']] = $row['id'];
            }
            echo json_encode($gy);
        } else
            echo json_encode(array());
    }
    
    // GYUJTOKOD ajax
    if ($_POST['pga'] == 'gyujtokod') {
        global $ID; 

        $rules = box_filter::sensitivity_check('milvus_grouping_codes');
        $cmd = sprintf("SELECT obm_id as id, alapegyseg FROM milvus_sampling_units t %s WHERE gykod_id=%s %s ORDER BY alapegyseg",$rules['join'], quote($_POST['gykod']), $rules['filter']);
        //$cmd = sprintf("SELECT array_to_string(array_agg(alapegyseg ORDER BY alapegyseg),',') AS a FROM milvus_gykodok gy LEFT JOIN milvus_alapegysegek ae ON gy.id=ae.gykod_id WHERE gy.gykod=%s AND gy.program = %s ORDER BY a",quote($_POST['gykod']),quote($_POST['program']) );

        $res = pg_query($ID,$cmd);
        if ($res and pg_num_rows($res)) {
            while ($row = pg_fetch_assoc($res)) {
                $a[$row['alapegyseg']] = $row['id'];
            }
            echo json_encode($a);
        } else
            echo json_encode(array());
    }

    // ALAPEGYSEG ajax
    if ($_POST['pga'] == 'alapegyseg') {
        global $ID; 
        $cmd = sprintf("SELECT ST_AsText(obm_geometry) as g FROM milvus_sampling_units WHERE obm_id=%s",quote($_POST['egys']));
        $res = pg_query($ID,$cmd);
        if ($res and pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            echo $row['g'];
        } else
            echo '';
    }
}

if (isset($_GET['form'])) {
    global $BID;
    $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id = %s",quote($_GET['form']));
    $res = pg_query($BID,$cmd);

    header("Content-type: application/json");
    if ($res and pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        echo json_encode($row);
    } else
        echo json_encode(array());
}

if (isset($_REQUEST['m']) && ($_REQUEST['m'] == 'download_restricted')) {
    switch($_SERVER['REQUEST_METHOD']) {
    case 'GET': 
        $request = &$_GET; 
        break;
    case 'POST': 
        $request = &$_POST; 
        break;
    default:
    }

    $modules = modules($_SESSION['current_query_table']);
    $mki = array_search('download_restricted', array_column($modules, 'module_name'));
    if ($mki!==false) {
        $cdmodule = new cDefModules();
        $cdmodule->download_restricted('ajax',$request);
    }
}

if (isset($_REQUEST['m']) && ($_REQUEST['m'] == 'ebp')) {
    switch($_SERVER['REQUEST_METHOD']) {
    case 'GET': 
        $request = &$_GET; 
        break;
    case 'POST': 
        $request = &$_POST; 
        break;
    default:
    }

    $modules = modules($_SESSION['current_query_table']);
    $mki = array_search('ebp', array_column($modules, 'module_name'));
    if ($mki!==false) {
        $cdmodule = new cDefModules();
        $cdmodule->ebp('ajax',$request);
    }
}

?>
