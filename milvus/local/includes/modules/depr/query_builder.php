<?php
function transform_geometry($p) {
    $t = PROJECTTABLE.'.'.$_SESSION['st_col']['GEOM_C'];
    //$t = $_SESSION['st_col']['GEOM_C'];
    //
    if (!rst('acc')) {
        $params = preg_split('/;/',$p);
        if (count($params)==2) {
            $t = sprintf("ST_SnapToGrid(%s,%f,%f)",$t,$params[0],$params[1]);
        } else if (count($params)==1) {
            $t = sprintf("ST_SnapToGrid(%s,%f)",$t,$params[0]);
        } else {
            $t = sprintf("ST_SnapToGrid(%s,0.13,0.09)",$t);
        }
    }
    return $t;
}

    function query_builder($mc,$post_val) {
        global $where;
        $w = '';
        $specials = array('exact_from','exact_mfrom','exact_mto','exact_to','exact_date','obm_uploading_date','obm_uploader_user','obm_id');
        $api_columns = array('obm_uploading_id'=>'uploads');

        
        //if (array_key_exists($mc,dbcolist('array'))) {
        if (array_key_exists($mc,array_merge($api_columns,dbcolist('array')))) {
            $ja = json_decode($post_val,true);
            $v = implode(',',array_map('quote',$ja));
            if($v=='NULL') return; 

            /* mi ez qform SESSION változó???*/     
            if (isset($_SESSION['qform'])) {
                if (isset($_SESSION['qform'][$mc])) $mc = $_SESSION['qform'][$mc];
                    $w = "$mc IN ($v)";
            } else {
                $w = "\"$mc\" IN ($v)";
            }
            return $w;
        }
        if(!isset($_SESSION['st_col']['DATE_C'])) {
            log_action("st_col DATE_C not defined");
            return;
        }
        // special search for date ranges 
        if (in_array($mc,$specials)) {
            /*qids_exact_date	["2016-01-13"]
              qids_exact_from	["2016-01-13"]
              qids_exact_mfrom ["01-19"]
              qids_exact_mto	 ["01-28"]
              qids_exact_to	["2016-01-22"] */

            //ez megint egy erőltett dolog és nincs dokumentálva, hogy csak az első oszlopra működik!!!
            //MEG KELL VÁLTOZTATNI!!!!
            $DATE_C = $_SESSION['st_col']['DATE_C'][0];
            //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$DATE_C,$m))
            //    $DATE_C = $m[2];

            $ja = json_decode($post_val,true);

            $v = implode(',',array_map('quote',$ja));

            if($v==NULL) {
                if (isset($_SESSION['exact_date_from']) and $mc == 'exact_to') $v = sprintf("'%s'",date("Y-m-d"));
                elseif (isset($_SESSION['exact_mdate_from']) and $mc == 'exact_mto') $v = sprintf("'%s'",date("m-d"));
                else
                    return; 
            }
            if ($mc == 'exact_date' and $v!='NULL') {
                $w = "\"$DATE_C\"=$v";
            }
            elseif ($mc == 'exact_from' and $v!='NULL') {
                $_SESSION['exact_date_from'] = $v;
                return;
            }
            elseif ($mc == 'exact_to' and $v!='NULL') {
                $ef = $_SESSION['exact_date_from'];
                unset($_SESSION['exact_date_from']);
                //$ef = array_pop($where);
                $w = "($DATE_C BETWEEN $ef AND $v)";
            }
            elseif ($mc == 'exact_mfrom' and $v!='NULL') {
                $_SESSION['exact_mdate_from'] = $v;
                return;
            }
            elseif ($mc == 'exact_mto' and $v!='NULL') {
                $start = $_SESSION['exact_mdate_from'];
                unset($_SESSION['exact_mdate_from']);
                $m = array();
                if(preg_match('/([0-9]{2})-([0-9]{2})/',$start,$m)) {
                    $start_month = $m[1];
                    $start_day = $m[2];
                    $m = array();
                    if (preg_match('/([0-9]{2})-([0-9]{2})/',$v,$m)){
                        $end_month = $m[1];
                        $end_day = $m[2];
                        $month = array();
                        foreach(range($start_month,$end_month) as $i){
                            /*(extract(month from "datum_tol")=3 AND extract(day from "datum_tol")>2) OR
                            (extract(month from "datum_tol")=4) OR
                            (extract(month from "datum_tol")=5 AND extract(day from "datum_tol")<5)*/
                            if($i==$start_month) {
                                $day_filter = "AND extract(day from \"$DATE_C\")>$start_day";
                            } elseif($i==$end_month) {
                                $day_filter = "AND extract(day from \"$DATE_C\")<$end_day";
                            } else {
                                $day_filter = "";
                            }
                            $month[] = "(extract(month from \"$DATE_C\")=$i $day_filter)";
                        }
                        $w = "(".implode(" OR ",$month).")";
                    }
                }
            } elseif ($mc == 'obm_uploading_date' and $v!='NULL') {
                //only works if in the map file int he query_layer's query the uploading is LEFT JOINED
                $w = "date_trunc('day',uploading_date)=$v";
            } elseif ($mc == 'obm_uploader_user' and $v!='NULL') {
                //only works if in the map file int he query_layer's query the uploading is LEFT JOINED
                $w = "uploader_id=$v";
            } elseif ($mc == 'obm_id' and $v!='NULL') {
                //obm_id query : map apply text query over spatial query
                $w = sprintf("\"obm_id\" IN (%s)",implode(',',$_SESSION['wfs_array']['id']));
            }
        }
        return $w;
    }
?>
