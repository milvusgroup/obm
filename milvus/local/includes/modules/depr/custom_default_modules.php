<?php
class cDefModules {
    /* Download restriction
     *
     * */
    function download_restricted ($action,$params) {
        if(file_exists(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/download_restricted.php')) {
            if( php_check_syntax(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/download_restricted.php','e')) {
                include_once(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/download_restricted.php');
            } else {
                $error[] = str_module_include_error.': '.$e;
            }
        } else {
            include_once(getenv('PROJECT_DIR').'modules/download_restricted.php');
        }

        $mf = new download_restricted($action,$params);

        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }

    function ebp ($action,$params) {
        if(file_exists(getenv('PROJECT_DIR').'modules/ebp.php')) {
            if( php_check_syntax(getenv('PROJECT_DIR').'modules/ebp.php','e')) {
                include_once(getenv('PROJECT_DIR').'modules/ebp.php');
            } else {
                $error[] = str_module_include_error.': '.$e;
            }
        } else {
            include_once(getenv('PROJECT_DIR').'modules/ebp.php');
        }

        $mf = new ebp($action,$params);

        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else 
            return $mf->retval;
    }
}
