<?php
/* It is an empty function
 * It used as a private function in Milvus
 *
 * */
class extra_params {
    function add_params ($p,$input) {
        //log_action("extra_params teszt",__FILE__,__LINE__);
        return $input;
    }

    //nincs default változata
    //összetett kitett mező kezelés a feltöltő formban
    //lásd milvus_get_default_value

    function format_input($p='',$col,$insert_allowed=0) {
        global $BID,$ID,$des,$sn;
        $list = '';
        $def_modsz = "";
        if ($col=='grouping_code') 
            return '-';
        if ($col=='sampling_unit') 
            return '-';
        if ($col=='local_grouping_code') 
            return '-';
        if ($col=='national_grouping_code') 
            return '-';
        if ($col=='local_sampling_unit') 
            return '-';
        if ($col=='national_sampling_unit') 
            return '-';
        if ($col=='xlocal_program') {

            $cmd = "SELECT default_value,list[1] as list FROM project_forms_data WHERE \"column\"='method' AND form_id='{$insert_allowed}'";
            $res = pg_query($BID,$cmd);
            if ($row = pg_fetch_assoc($res)) {
                if ($row['default_value'] == '_list')
                    $def_modsz =  $row['list'];
                else
                    $def_modsz = $row['default_value'];
            }

            $cmd = sprintf("SELECT mp.obm_id AS id,nev,helyi_program,orszagos FROM milvus_program mp LEFT JOIN milvus_method mm ON modszer=mm.obm_id WHERE orszagos=FALSE AND name='$def_modsz' ORDER BY nev");
            $res = pg_query($ID,$cmd);

            // default program - 1 value
            $cmd = "SELECT list[1] as list FROM project_forms_data WHERE \"column\"='local_program' AND default_value='_list' AND form_id='{$insert_allowed}'";
            $res2 = pg_query($BID,$cmd);
            if ($row2 = pg_fetch_assoc($res2)) {
                $def_program = $row2['list'];
            } 
            else {
                $def_program = '';
            }

            $d = array();
            $disabled='';
            while ($row = pg_fetch_assoc($res)) {
                $translated = $row['nev'];
                if ($def_program!='' and $def_program == $row['nev']) {
                    //only one row!
                    $d = array();
                    $translated = $def_program;
                    $disabled="disabled='true'";
                }

                if ( defined($row['nev'])) 
                    $translated = constant($row['nev']);
                
                $d[] = "<option value='{$row['nev']}' data-val='{$row['id']}'>$translated</option>";
                
                // do not load the other values if there was a default one
                if ($def_program!='' and $def_program == $row['nev']) {
                    break;
                }
            }
            $cmd = sprintf('SELECT description,"column" FROM project_forms_data WHERE "column" IN (\'local_grouping_code\',\'local_sampling_unit\') AND form_id=%d',$insert_allowed);
            $res = pg_query($BID,$cmd);
            $gykod_comm = '';
            $ekod_comm = '';
            while ($row = pg_fetch_assoc($res)) {

                if ($row['column'] == 'local_grouping_code') {
                    $gykod_comm = $row['description'];
                    if ( defined($row['description'])) 
                        $gykod_comm = constant($row['description']);
                }
                elseif ($row['column'] == 'local_sampling_unit') {
                    $ekod_comm = $row['description'];
                    if ( defined($row['description'])) 
                        $ekod_comm = constant($row['description']);
                }
            }

            $list = sprintf('<div class="table"><div><label>'.$sn.' '.$des.'</label><select id="default-%1$s" class="default milvus-l-program" style="min-width:120px" $disabled><option></option>%2$s</select> &nbsp; <i class="fa fa-hand-o-right fa-lg" style="color:#666"></i></div><div><label>%3$s (%5$s)</label><select class="default milvus-l-gykod" id="default-local_grouping_code" style="min-width:120px"></select> &nbsp; <i class="fa fa-hand-o-right fa-lg" style="color:#666"></i></div><div><label>%4$s (%6$s)</label><select class="default milvus-l-egys" id="default-local_sampling_unit" style="min-width:120px"></select></div></div>',$col,implode("",$d),t(str_gykodok),t(str_alapegysegek),$gykod_comm,$ekod_comm);
            if (count($d)==1) 
                $list .= "<script>$(document).ready(function(){
                $('.mezok').on('change','#default-local_program',function(){program_change($(this));});
                $('#default-local_program > option:eq(1)').prop('selected',true).trigger('change');</script>";
        }
        elseif($col=='xnational_program') {
            $cmd = "SELECT default_value,list[1] as list FROM project_forms_data WHERE \"column\"='method' AND form_id='{$insert_allowed}'";
            $res = pg_query($BID,$cmd);
            if($row = pg_fetch_assoc($res)) {
                if ($row['default_value'] == '_list')
                    $def_modsz =  $row['list'];
                else
                    $def_modsz = $row['default_value'];
            }

            $cmd = sprintf("SELECT mp.obm_id as id,nev,helyi_program,orszagos FROM milvus_program mp LEFT JOIN milvus_method mm ON modszer=mm.obm_id WHERE orszagos=TRUE AND name='$def_modsz' ORDER BY nev");
            $res = pg_query($ID,$cmd);

            // default program - 1 value
            $cmd = "SELECT list[1] as list FROM project_forms_data WHERE \"column\"='national_program' AND default_value='_list' AND form_id='{$insert_allowed}'";
            $res2 = pg_query($BID,$cmd);
            if($row2 = pg_fetch_assoc($res2)) {
                $def_program = $row2['list'];
            } else {
                $def_program = '';
            }
            $d = array();
            $rid = '';
            $disabled='';
            while($row = pg_fetch_assoc($res)){
                $translated = $row['nev'];
                if ($def_program!='' and $def_program == $row['nev']) {
                    //only one row!
                    $d = array();
                    $translated = $def_program;
                    $disabled="disabled='true'";
                }

                if( defined($row['nev'])) $translated = constant($row['nev']);
                $d[] = "<option value='{$row['nev']}' data-val='{$row['id']}'>$translated</option>";
                $rid = $row['id'];

                // do not load the other values if there was a default one
                if ($def_program!='' and $def_program == $row['nev']) {
                    break;
                }
            }
            $cmd = sprintf('SELECT description,"column" FROM project_forms_data WHERE "column" IN (\'national_grouping_code\',\'national_sampling_unit\') AND form_id=%d',$insert_allowed);
            $res = pg_query($BID,$cmd);
            $gykod_comm = '';
            $ekod_comm = '';
            while ($row = pg_fetch_assoc($res)) {

                if ($row['column'] == 'national_grouping_code') {
                    $gykod_comm = $row['description'];
                    if ( defined($row['description'])) $gykod_comm = constant($row['description']);
                }
                elseif ($row['column'] == 'national_sampling_unit') {
                    $ekod_comm = $row['description'];
                    if ( defined($row['description'])) $ekod_comm = constant($row['description']);
                }
            }
            $list = sprintf('<div class="table"><div><label>'.$sn.' '.$des.'</label><select id="default-%1$s" class="default milvus-n-program" style="min-width:120px" $disabled><option></option>%2$s</select> &nbsp; <i class="fa fa-hand-o-right fa-lg" style="color:#666"></i></div><div><label>%3$s (%5$s)</label><select class="default milvus-n-gykod" id="default-national_grouping_code" style="min-width:120px"></select> &nbsp; <i class="fa fa-hand-o-right fa-lg" style="color:#666"></i></div><div><label>%4$s (%6$s)</label><select class="default milvus-n-egys" id="default-national_sampling_unit" style="min-width:120px"></select></div></div>',$col,implode('',$d),t(str_gykodok),t(str_alapegysegek),$gykod_comm,$ekod_comm);
            if (count($d)==1) $list .= "<script>$(document).ready(function(){
                $('.mezok').on('change','#default-national_program',function(){program_change($(this));});
                $('#default-national_program > option:eq(1)').prop('selected',true).trigger('change');});</script>";
        }

        elseif($col=='program') {
/*            $cmd = "SELECT default_value,list[1] as list FROM project_forms_data WHERE \"column\"='method' AND form_id='{$insert_allowed}'";
            $res = pg_query($BID,$cmd);
            if($row = pg_fetch_assoc($res)) {
                log_action($row,__FILE__,__LINE__);

                if ($row['default_value'] == '_list')
                    $def_modsz =  $row['list'];
                else
                    $def_modsz = $row['default_value'];
            }
 */
            #$cmd = sprintf("SELECT id,nev,helyi_program,orszagos FROM milvus_programs WHERE orszagos=TRUE ORDER BY nev");
            //            $cmd = sprintf("SELECT mp.id,nev,helyi_program,orszagos FROM milvus_programs mp LEFT JOIN milvus_methods mm ON modszer=mm.id WHERE name='$def_modsz' ORDER BY nev");

            // _rules JOIN and sensitive check
            if (!isset($_SESSION['Tid']))
                $tid = 0;
            else
                $tid = $_SESSION['Tid'];
            if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
                $tgroups = 0;
            else
                $tgroups = $_SESSION['Tgroups'];

            if (!grst(PROJECTTABLE,4) and $_SESSION['st_col']['RESTRICT_C']) {
                $rules_filter = " AND ( (sensitivity=3 AND ARRAY[$tgroups] && read) ";
                $rules_filter .= " OR sensitivity = 0 ) ";
                $rules_join = sprintf("LEFT JOIN %s_rules r ON (id=r.row_id AND data_table = 'milvus_program')",PROJECTTABLE);
            } else {
                $rules_filter = '';
                $rules_join = '';
            }

            $cmd = sprintf('SELECT mp.obm_id as id, nev, helyi_program, orszagos FROM milvus_program mp %s  ORDER BY nev',$rules_join,$rules_filter);
            $res = pg_query($ID,$cmd);

            // default program - 1 value
            $cmd = "SELECT list[1] as list FROM project_forms_data WHERE \"column\"='program' AND default_value='_list' AND form_id='{$insert_allowed}'";
            $res2 = pg_query($BID,$cmd);
            if($row2 = pg_fetch_assoc($res2)) {
                $def_program = $row2['list'];
            } else {
                $def_program = '';
            }
            $d = array();
            $rid = '';
            $disabled='';
            while($row = pg_fetch_assoc($res)){
                $translated = $row['nev'];
                if ($def_program!='' and $def_program == $row['nev']) {
                    //only one row!
                    $d = array();
                    $translated = $def_program;
                    $disabled="disabled='true'";
                }

                if( defined($row['nev'])) $translated = constant($row['nev']);
                $d[] = "<option value='{$row['nev']}' data-val='{$row['id']}'>$translated</option>";
                $rid = $row['id'];

                // do not load the other values if there was a default one
                if ($def_program!='' and $def_program == $row['nev']) {
                    break;
                }
            }
            $cmd = sprintf('SELECT description,"column" FROM project_forms_data WHERE "column" IN (\'grouping_code\',\'sampling_unit\') AND form_id=%d',$insert_allowed);
            $res = pg_query($BID,$cmd);
            $gykod_comm = '';
            $ekod_comm = '';
            while ($row = pg_fetch_assoc($res)) {

                if ($row['column'] == 'grouping_code') {
                    $gykod_comm = $row['description'];
                    if ( defined($row['description'])) $gykod_comm = constant($row['description']);
                }
                elseif ($row['column'] == 'sampling_unit') {
                    $ekod_comm = $row['description'];
                    if ( defined($row['description'])) $ekod_comm = constant($row['description']);
                }
            }
            $list = sprintf('<div class="table"><div><label>'.$sn.' '.$des.'</label><select id="default-n-%1$s" class="default milvus-n-program" style="min-width:120px" $disabled><option></option>%2$s</select> &nbsp; <i class="fa fa-hand-o-right fa-lg" style="color:#666"></i></div><div><label>%3$s (%5$s)</label><select class="default milvus-n-gykod" id="default-national_grouping_code" style="min-width:120px"></select> &nbsp; <i class="fa fa-hand-o-right fa-lg" style="color:#666"></i></div><div><label>%4$s (%6$s)</label><select class="default milvus-n-egys" id="default-national_sampling_unit" style="min-width:120px"></select></div></div>',$col,implode('',$d),t(str_gykodok),t(str_alapegysegek),$gykod_comm,$ekod_comm);
            if (count($d)==1) $list .= "<script>$(document).ready(function(){
                $('.mezok').on('change','#default-national_program',function(){program_change($(this));});
                $('#default-national_program > option:eq(1)').prop('selected',true).trigger('change');});</script>";
        }
        return $list;
    }
}
?>
