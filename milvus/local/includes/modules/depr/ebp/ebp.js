$(document).ready(function() {
    $(".prepareAllData").off().on("click", async function(ev) {
        ev.preventDefault();
        let conf = confirm('Biztos?');
        if (conf) {
            try {
                const div = $('#prepareAllData');
                var start = new Date("2010-01-01");
                var end = new Date();
                end = end.setDate(end.getDate() - 1);
                div.html('<ul>');

                var d = new Date(start);
                while(d <= end){
                    pgdate = pgFormatDate(d);
                    const html = await $.get("ajax_milvus",{ "m": "ebp", "action": "prepareEvents", "start_date": pgdate, "end_date": pgdate });
                    div.append(html);
                    var newDate = d.setDate(d.getDate() + 1);
                    d = new Date(newDate);
                }
                div.append('</ul>');
            }
            catch (error) {
                alert(error);
                console.log(error);
            }
        }
    });

    $(".ebpTables").off().on("click", async function(ev) {
        ev.preventDefault();
        try {
            const action = $(this).data("f");
            let mode = null;
            let code = null;
            if (action == 'dataProvision') {
                mode = $(this).data('mode');
            }
            else if (action == 'audit') {
                code = $(this).data('code');
            }


            const html = await $.get("ajax_milvus",{ "m": "ebp", "action": action, "mode": mode ,"code": code});
            $("#"+action).html(html);
            if (action == "speciesTable")
                speciesTableEvents();
        }
        catch (error) {
            alert(error);
            console.log(error);
        }
    });
    $('body').off().on("click",".ebpActions", async function(ev) {
        ev.preventDefault();
        try {
            const action = $(this).data("f");
            const method = $(this).data("method");
            const code = $(this).data("code");
            const data = await $.get("ajax_milvus",{ "m": "ebp", "action": action, "method" : method, "code": code});
            if (data)
                showDialog(data);
        }
        catch (error) {
            console.log(error);
        }
    });

    $("#prepareEventsForm").off().on("submit", async function(ev) {
        ev.preventDefault();
        const formData = new FormData(this);

        let date = (formData.get('date') == '') ? null : pgFormatDate(formData.get('date'));
        let q_by = formData.get('q_by');
        const html = await $.get("ajax_milvus",{ "m": "ebp", "action": 'prepareEvents', "date": date, "q_by": q_by });
        $("#prepareEvents").html(html);

    });
});

function speciesTableEvents() {
    $("table#ebpSpeciesTable td:nth-child(3)").on("click", async function(ev) {
        ev.preventDefault();
        try {
            const list = await $.getJSON("ajax_milvus",{"m": "ebp", "action": "getLocalSpeciesList"});
            let html = "<select class=\'taxonAssign\'><option disabled hidden selected> Select species ... </option>";
            $.each(list,function(k,v) {
                html += "<option data-id=\'" + v.taxon_id + "\'>"+v.word+"</option>";
            });
            html += "</select>";
            $(this).html(html);
            $(this).off();
            $(".taxonAssign").on("change", async function(ev) {
                const local_id = $(this).find("option:selected").data("id");
                const ebp_id = $(this).parent().next().html();
                const resp = await $.post("ajax_milvus",{"m":"ebp", "action": "setLocalId", "local_id": local_id ,"ebp_id": ebp_id });
                if (showDialog(resp) == "success") {
                    $(this).parent().html(local_id);
                    $("#showSpeciesTable").trigger("click");
                }

            });
        }
        catch (error) {
            console.log(error);
        }
    });
}

function showDialog(data) { 
    const retval = jsendp(data);
    let txt = "";
    if (retval["status"]=="error") 
        txt = retval["message"];
    else if (retval["status"]=="fail")
        txt = "FAIL: "+retval["data"];
    else if (retval["status"]=="success") {
        txt = "SUCCESS: " + retval["data"];
        $(this).attr("disabled","disabled");
    }
    $( "#dialog" ).text(txt);
    var isOpen = $( "#dialog" ).dialog( "isOpen" );
    if(!isOpen) $( "#dialog" ).dialog( "open" ); 

    return retval["status"];
}
function pgFormatDate(date) {
    /* Via http://stackoverflow.com/questions/3605214/javascript-add-leading-zeroes-to-date */
    function zeroPad(d) {
        return ("0" + d).slice(-2)
    }

    var parsed = new Date(date)

    return [parsed.getUTCFullYear(), zeroPad(parsed.getMonth() + 1), zeroPad(parsed.getDate())].join("-");
}
