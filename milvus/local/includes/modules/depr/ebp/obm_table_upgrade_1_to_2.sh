#!/bin/bash

# export obm 1.0 tables
pg_dump gisdata -t milvus > milvus.sql
pg_dump gisdata -t milvus_alapegysegek > milvus_alapegysegek.sql
pg_dump gisdata -t milvus_criticaldata_geom > milvus_criticaldata_geom.sql
pg_dump gisdata -t milvus_form_group > milvus_form_group.sql
pg_dump gisdata -t milvus_grid > milvus_grid.sql
pg_dump gisdata -t milvus_gykodok > milvus_gykodok.sql
pg_dump gisdata -t milvus_habitat > milvus_habitat.sql
pg_dump gisdata -t milvus_hidden_geometry > milvus_hidden_geometry.sql
pg_dump gisdata -t milvus_history > milvus_history.sql
pg_dump gisdata -t milvus_megfigyelok > milvus_megfigyelok.sql
pg_dump gisdata -t milvus_methods > milvus_methods.sql
pg_dump gisdata -t milvus_programs > milvus_programs.sql
pg_dump gisdata -t milvus_roviditesek > milvus_roviditesek.sql
pg_dump gisdata -t milvus_taxon > milvus_taxon.sql
pg_dump gisdata -t uploadings > uploadings.sql

#biomaps
pg_dump biomaps -t users > users.sql
pg_dump biomaps -t users --data-only > users_data.sql
awk '{ if ($1 > 88 ) print $0 }' users_data.sql > users_new_data.sql
psql -p 5433 -d biomaps <  users_new_data.sql

# drop obm 2.0 tables
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_alapegysegek CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_criticaldata_geom CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_form_group CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_grid CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_gykodok CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_habitat CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_hidden_geometry CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_history CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_megfigyelok CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_methods CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_programs CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_roviditesek CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS milvus_taxon CASCADE;"
psql -p 5433 -d gisdata -c "DROP table IF EXISTS uploadings CASCADE;"

#import obm 1.0 tables
psql -d gisdata -p 5433 < milvus.sql
psql -d gisdata -p 5433 < milvus_alapegysegek.sql
psql -d gisdata -p 5433 < milvus_criticaldata_geom.sql
psql -d gisdata -p 5433 < milvus_form_group.sql
psql -d gisdata -p 5433 < milvus_grid.sql
psql -d gisdata -p 5433 < milvus_gykodok.sql
psql -d gisdata -p 5433 < milvus_habitat.sql
psql -d gisdata -p 5433 < milvus_hidden_geometry.sql
psql -d gisdata -p 5433 < milvus_history.sql
psql -d gisdata -p 5433 < milvus_megfigyelok.sql
psql -d gisdata -p 5433 < milvus_methods.sql
psql -d gisdata -p 5433 < milvus_programs.sql
psql -d gisdata -p 5433 < milvus_roviditesek.sql
psql -d gisdata -p 5433 < milvus_taxon.sql

# uplodings - add 3 columns

#Újra kell számolni a feltöltések számát feltöltésenként

psql -d gisdata -p 5433 < uploadings.sql

psql -p 5433 -d gisdata -c "ALTER TABLE uploadings ADD COLUMN project_table character varying(64);" 
psql -p 5433 -d gisdata -c "ALTER TABLE uploadings ADD COLUMN owner integer[];" 
psql -p 5433 -d gisdata -c "ALTER TABLE uploadings ADD COLUMN project character varying(32);" 

# rename columns
cat columns.txt | awk -F , '{print "ALTER TABLE milvus RENAME COLUMN " $1 " TO " $2 ";"}' | awk '{print "psql -p 5433 -d gisdata -c \""$0"\""}' | bash 

#replace truncated geometries from the milvus_hidden_geometry table
#letiltottam a hidden_geom_triggert!
psql -p 5433 -d gisdata -c "UPDATE milvus SET obm_geometry = data_geom FROM milvus_hidden_geometry where obm_id = data_id;"

#drop columns
psql -p 5433 -d gisdata -c "ALTER TABLE milvus DROP COLUMN adat_status;"
psql -p 5433 -d gisdata -c "ALTER TABLE milvus DROP COLUMN elohely_1a;"
psql -p 5433 -d gisdata -c "ALTER TABLE milvus DROP COLUMN elohely_1b;"
psql -p 5433 -d gisdata -c "ALTER TABLE milvus DROP COLUMN elohely_3;"
psql -p 5433 -d gisdata -c "ALTER TABLE milvus DROP COLUMN source_db;"
psql -p 5433 -d gisdata -c "ALTER TABLE milvus DROP COLUMN source_db_rid;"

# add new columns
psql -p 5433 -d gisdata -c "ALTER TABLE milvus ADD COLUMN exact_time time;"
psql -p 5433 -d gisdata -c "ALTER TABLE milvus ADD COLUMN precision_of_count_list varchar(32);"
psql -p 5433 -d gisdata -c "ALTER TABLE milvus ADD COLUMN surveyed_geometry geometry;"

# milvus_taxon lang

psql -p 5433 -d gisdata -c "ALTER TABLE milvus_taxon ALTER COLUMN lang TYPE varchar(16);"

# biomaps_db_upgrades
pg_dump biomaps -t project_metaname --data-only > project_metaname.sql

psql -p 5433 -d biomaps -c "DELETE FROM project_forms WHERE project_table='milvus';"

# save mammalia
/usr/pgsql-9.4/bin/pg_dump biomaps -p 5433 -t project_forms --data-only > project_forms_obm2.sql
/usr/pgsql-9.4/bin/pg_dump biomaps -p 5433 -t project_forms_data --data-only > project_forms_data_obm2.sql

#sed -i -e 's/34/41/' project_forms_obm2.sql 
#sed -i -e 's/34/41/' project_forms_data_obm2.sql 
#sed -i -e 's/35/42/' project_forms_obm2.sql 
#sed -i -e 's/35/42/' project_forms_data_obm2.sql 
#sed -i -e 's/36/43/' project_forms_obm2.sql 
#sed -i -e 's/36/43/' project_forms_data_obm2.sql 
#sed -i -e 's/37/44/' project_forms_obm2.sql 
#sed -i -e 's/37/44/' project_forms_data_obm2.sql 
#sed -i -e 's/38/45/' project_forms_obm2.sql 
#sed -i -e 's/38/45/' project_forms_data_obm2.sql 
#sed -i -e 's/39/46/' project_forms_obm2.sql 
#sed -i -e 's/39/46/' project_forms_data_obm2.sql 
#sed -i -e 's/40/47/' project_forms_obm2.sql 
#sed -i -e 's/40/47/' project_forms_data_obm2.sql 
#sed -i -e 's/{}/\\N/' project_forms_data_obm2.sql 

# dump obm 1.0 forms, forms_data
pg_dump biomaps -t project_forms > project_forms.sql
pg_dump biomaps -t project_forms_data > project_forms_data.sql

# drop obm 2.0 forms, forms_data
psql -d biomaps -p 5433 -c "DROP TABLE IF EXISTS project_forms CASCADE;"
psql -d biomaps -p 5433 -c "DROP TABLE IF EXISTS project_forms_data;"

# import obm 1.0 forms
psql -d biomaps -p 5433 < project_forms.sql
psql -d biomaps -p 5433 < project_forms_data.sql

# add obm 2.0 new column
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data ALTER COLUMN \"count\" TYPE numeric[] USING \
CASE WHEN length='max' THEN array[0,\"count\"]::numeric[]\
        WHEN length='min' THEN array[\"count\",0]::numeric[]\
                WHEN length='exactly' THEN array[\"count\",\"count\"]::numeric[]\
                        ELSE array[]::numeric[]\
END;"

psql -p 5433 -d biomaps -c "ALTER TABLE project_forms ADD COLUMN srid character varying[];" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms ADD COLUMN data_owners integer[];" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms ADD COLUMN data_groups integer[];" 

psql -p 5433 -d biomaps -c "UPDATE project_forms_data SET length='minmax' WHERE \"length\"!='nochceck';"
psql -p 5433 -d biomaps -c "UPDATE project_forms_data SET length='nocheck' WHERE \"length\"='nochceck';"

psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data ADD COLUMN api_params text;" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data ADD COLUMN relation character varying(255);" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data ADD COLUMN regexp character varying(128);" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data ADD COLUMN spatial geometry;" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data ADD COLUMN pseudo_columns text;" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data ADD COLUMN custom_function character varying(32);" 
psql -p 5433 -d biomaps -c "ALTER TABLE project_forms_data RENAME COLUMN \"length\" TO \"control\";"


# update column names in forms_data
cat columns.txt | awk -F , '{print "UPDATE project_forms_data SET \\\"column\\\"=\x27"$2"\x27 WHERE \\\"column\\\"=\x27"$1"\x27;"}'  | awk '{print "psql -p 5433 -d biomaps -c \""$0"\""}' | bash

#utólag írtam hozzá, mert nem működött a megfigyelő autocomplete (G)
psql -p 5433 -d biomaps -c "UPDATE project_forms_data SET genlist = 'milvus.observers' WHERE genlist = 'milvus.megfigyelok';" 
psql -p 5433 -d gisdata -c "DROP TRIGGER milvus_titkos_geom_update ON milvus;" 

# restore mammalia
psql -d biomaps -p 5433 < project_forms_obm2.sql
psql -d biomaps -p 5433 < project_forms_data_obm2.sql

# database columns
cat columns.txt | awk -F , '{print "UPDATE project_metaname SET column_name=\x27"$2"\x27 WHERE project_table=\x27milvus\x27 AND column_name=\x27"$1"\x27"}' | awk '{print "psql -p 5433 -d biomaps -c \""$0"\""}' | bash

psql -p 5433 -d gisdata -c "UPDATE uploadings SET \"group\" = ARRAY[uploader_id] WHERE \"group\" = '{}';"

# milvus_rules tábla feltöltése
psql -p 5433 -d gisdata -c "TRUNCATE milvus_rules;"
psql -p 5433 -d gisdata -c "INSERT INTO milvus_rules (data_table,row_id,read,write,sensitivity) (select 'milvus',m.obm_id,u.\"group\",ARRAY[u.uploader_id],m.access_to_data FROM milvus m LEFT JOIN uploadings u ON m.obm_uploading_id = u.id);"
psql -p 5433 -d gisdata -c "CREATE TRIGGER milvus_rules BEFORE INSERT ON milvus FOR EACH ROW EXECUTE PROCEDURE milvus_rules_f();"


#modulook működéséhez szükséges változtatások
# header names 
#psql -d biomaps -p 5433 -c "update header_names set f_order_columns = '{\"milvus\":{\"obm_id\":\"2\",\"list_id\":\"3\",\"method\":\"4\",\"method_details_species_aimed\":\"5\",\"date\":\"6\",\"date_until\":\"7\",\"time_of_start\":\"8\",\"time_of_end\":\"9\",\"duration\":\"10\",\"observers\":\"11\",\"national_program\":\"12\",\"national_grouping_code\":\"13\",\"national_sampling_unit\":\"14\",\"local_program\":\"15\",\"local_grouping_code\":\"16\",\"local_sampling_unit\":\"17\",\"comlete_list\":\"18\",\"list_id\":\"19\",\"optics_used\":\"20\",\"habitat_state\":\"21\",\"ice_cover\":\"22\",\"disturbance\":\"23\",\"colony_type\":\"24\",\"colony_place\":\"25\",\"colony_survey_method\":\"26\",\"snow_depth\":\"27\",\"visibility\":\"28\",\"wind_force\":\"29\",\"wind_speed_kmh\":\"30\",\"wind_speed_kmh\":\"31\",\"wind_direction\":\"32\",\"cloud_cover\":\"33\",\"precipitation\":\"34\",\"temperature\":\"35\",\"atmospheric_pressure\":\"36\",\"comments_general\":\"37\",\"comments_observation\":\"38\",\"obm_geometry\":\"39\",\"number_max\":\"40\",\"observers\":\"41\",\"wind_direction\":\"42\",\"visibility\":\"43\",\"optics_used\":\"44\",\"comments_general\":\"45\",\"habitat_state\":\"46\",\"obm_geometry\":\"47\",\"local_program\":\"48\",\"colony_type\":\"49\",\"colony_place\":\"50\",\"colony_survey_method\":\"51\",\"atmospheric_pressure\":\"52\",\"national_grouping_code\":\"53\",\"national_sampling_unit\":\"54\",\"flight_direction\":\"55\",\"nest_type\":\"56\",\"habitat_id\":\"57\",\"habitat_2\":\"58\",\"response_type\":\"59\",\"migration_intensity\":\"60\",\"migration_type\":\"61\",\"secondary_time\":\"62\",\"nest_place\":\"63\",\"comments_observation\":\"64\",\"local_grouping_code\":\"65\",\"local_sampling_unit\":\"66\",\"altitude\":\"67\",\"flight_altitude\":\"68\",\"secondary_time\":\"69\",\"observed_unit\":\"70\",\"species\":\"71\",\"method_details_species_aimed\":\"72\",\"number\":\"73\",\"number_max\":\"74\",\"number_max\":\"75\",\"gender\":\"76\",\"age\":\"77\",\"timing_of_reaction\":\"78\",\"count_precision\":\"79\",\"status_of_individuals\":\"80\",\"cause_of_death\":\"81\",\"nest_status\":\"82\",\"nest_place\":\"83\",\"nest_type\":\"84\",\"nest_condition\":\"85\",\"distance_m\":\"86\",\"distance_code\":\"87\",\"habitat_state\":\"88\",\"habitat_id\":\"89\",\"habitat_2\":\"90\",\"habitat_2\":\"91\",\"map_code\":\"92\",\"timing_of_reaction\":\"93\",\"response_type\":\"94\",\"in_flight_sitting\":\"95\",\"flight_direction\":\"96\",\"flight_altitude\":\"97\",\"migration_intensity\":\"98\",\"migration_type\":\"99\",\"number_of_eggs\":\"100\",\"number_of_nestlings\":\"101\",\"obm_files_id\":\"102\",\"comments_observation\":\"103\",\"data_owner\":\"104\",\"access_to_data\":\"105\"}}' where f_table_name = 'milvus';"

# modules 
#psql -d biomaps -p 5433 -c "update modules set params = '{obm_taxon,obm_datum,method,gender,age,extra_orszagos_program,extra_helyi_program,comlete_list,observed_unit,obm_uploading_date,obm_uploader_user}' where project_table = 'milvus' and module_name = 'box_filter';"
#psql -d biomaps -p 5433 -c "update modules set file = '{interface.php}' where project_table = 'milvus' and module_name = 'box_filter';"
