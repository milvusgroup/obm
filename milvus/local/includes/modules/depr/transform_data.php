<?php
class transform_data {
    function text_transform($p,$text,$col,$data_id) {
            global $BID;
            $params = preg_split('/;/',$p);
            #Array("modszer:shunt","hely:geom");
            $pmi = array();
            foreach($params as $pm) {
                $c = preg_split('/:/',$pm);
                $pmi[$c[0]] = $c[1];
            }
            //geometry to link
            $i = array_search('geom',$pmi);
            if ($i !== false and $i == $col) {
                $cmd = "SELECT st_AsGeoJSON('$text'::geometry,15,0) as gjson, st_AseWKT('$text'::geometry) as wkt";
                /* Ha lenne a pontnak neve és van json type (psql 9.3<)
                 * $cmd = "SELECT row_to_json(fc) FROM (SELECT 'Feature' As type, 
                          ST_AsGeoJSON('$text')::json As geometry, 
                          row_to_json((SELECT l FROM (SELECT 'valami' AS name) As l)) As properties) As fc";*/
                $res = pg_query($BID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                                //{"type":"Point","coordinates":[18.767587411656972,47.785560590815244]}
                    
                    //$geojson = '{"type":"Feature","geometry": {"type": "Point","coordinates": [125.6, 10.1]},"properties": {"name": "Dinagat Islands"}}';
                    $wkt = preg_replace('/SRID=\d+;/','',$row['wkt']);
                    $w = strtolower(preg_replace('/[^MUPOINTLGESYR]/','',$wkt));
                    return "<a href='' target='_blank' alt='".addslashes($row['gjson'])."' title='{$row['wkt']}' id='$data_id' class='coord_query'>$w</a>";
                }
            }
            //geometry to wkt
            $i = array_search('geom_nolink',$pmi);
            if ($i !== false and $i == $col) {
                $cmd = "SELECT st_AsGeoJSON('$text'::geometry,15,0) as gjson, st_AseWKT('$text'::geometry) as wkt";
                $res = pg_query($BID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    $wkt = preg_replace('/SRID=\d+;/','',$row['wkt']);
                    $w = strtolower(preg_replace('/[^MUPOINTLGESYR]/','',$wkt));
                    return "$w";
                }
            }

            $i = array_search('idotartam',$pmi);
            if ($i !== false and $i == $col) {
                $m = $text%60;
                $h = floor($text/60);
                return sprintf("%0d:%0d",$h,$m);
            }
            $i = array_search('kezdes',$pmi);
            if ($i !== false and $i == $col) {
                $m = $text%60;
                $h = floor($text/60);
                return sprintf("%0d:%0d",$h,$m);
            }
    	    $i = array_search('uplid',$pmi);
            if ($i !== false and $i == $col) {
               return sprintf('<a href=\'?history=upload&id=%1$d\' target=\'_blank\'>%1$d</a>',$text);
            }
    
    	    $i = array_search('date_yearonly',$pmi);
            if ($i !== false and $i == $col) {
               return sprintf('%d',preg_replace('/^(\d+).+/','$1',$text));
            }
    	    $i = array_keys($pmi,'translate');
            if ($i !== false and in_array($col,$i)) {
                $translated = $text;
                if( preg_match('/^str_/',$text) and defined($text)) $translated = constant($text);
                return sprintf('%s',$translated);
            }
    
    
            //only testing
            #$i = array_search('shunt',$pmi);
            #if ($i !== false and $i == $col) {
            #    return "shunt";
            #}
            // currently it works only with point geometry:
            // maps.js: $('body').on('click',".coord_query",function(e){
            return $text;
    }
}
