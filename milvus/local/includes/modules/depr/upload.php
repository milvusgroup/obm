<?php
// create choose list
function milvus_get_list() {
    global $ID,$BID;

    /*$q = '';
    if ($login_group=='')
        $q .= "<br>Nyílvános adatfeltöltés";*/

    $_SESSION['upload']['question'] = "<br>".t(str_kitolto_iv_valasztasa_modszer_szerint).":";

    $li = array();
    $list = form_choose_list();
    $g = array('ZZZZ'=>array());
    $c_order = array(0);
   
    foreach ($list as $form_id) {
        $row = form_element($form_id);

        $f = array('<button class="pure-button button-href pure-button-disabled" disabled><i class="fa fa-table"></i></button>','<button class="pure-button button-href pure-button-disabled" disabled><i class="fa fa-upload"></i></button>'); 
        foreach(explode('|',$row['ft']) as $ft) {
            if($ft=='web') {
                $f[0] = sprintf('<a href="?form=%1$d&type=%2$s" title="%3$s" class="pure-button button-href"><i class="fa fa-table"></i></a>',$row['form_id'],'web',str_upload_from_web);
            }
            elseif($ft=='file') {
                $f[1] = sprintf('<a href="?form=%1$d&type=%2$s" title="%3$s" class="pure-button button-href"><i class="fa fa-upload"></i></a>',$row['form_id'],'file',str_upload_from_file);
            }
        }
        $label = $row['form_name'];
        if (preg_match('/^str_/',$label))
            if (defined($label))
                $label = constant($label);

        $l = sprintf('<span>%2$s</span><span style="font-size:15px">%1$s</span>',$label,implode(' ',$f));

        //
        $cmd = "SELECT name,c_order FROM milvus_form_group WHERE $form_id = ANY(form_id)";
        $res = pg_query($ID,$cmd);
        if(pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            if (!isset($g[$r['name']])) {
                $g[$r['name']] = array();
                $c_order[] = $r['c_order'];
            }
            $g[$r['name']][] = $l;
        } else {
           $g['ZZZZ'][] = $l; 
        }

        //$li[] = sprintf('<h2>'.$g.'<a href="?form=%1$d&type=web">%2$s</a>  <span style="font-size:80%4$s">(%3$s)</span></h2>',$row['form_id'],$row['form_name'],implode(' ',$f),'%');
    }
    $g_sort = array();
    $count = count($c_order);
    $g_keys = array_keys($g);
    for($n=0;$n<$count;$n++) {
        #0 2 4 3 1
        $sort = $c_order[$n];
        $g_sort[$sort] = $g_keys[$n];
    }
    ksort($g_sort);
    $gk = array();
    foreach($g_sort as $n) {
        $gk[$n] = $g[$n];
    }
    $g = $gk;
    $list = "<ul id='formMenu'>";
    foreach($g as $k=>$v) {
        $sor = "<li>";
        $label = $k;
        if (preg_match('/^str_/',$label))
            if (defined($label))
                $label = constant($label);

        if ($k!='ZZZZ') $sor .= "<h3><i class='fa fa-caret-right fa-fw'></i> $label</h3><ul class='noJS'>";
        foreach ($v as $ls) {
            if ($k!='ZZZZ')
                $sor .= "<li>$ls</li>";
            else
                $sor .= "<li style='padding-left:10px'>$ls</li>";
        }
        if ($k!='ZZZZ') $sor .= "</ul>";
        $sor .= "</li>";

        $list .= $sor;
    }
    $list .= "</ul>";
    return array($list);
}
?>
