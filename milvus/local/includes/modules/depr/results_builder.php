<?php

// check a column is available in the enabled column list if not return 2
// if it is in the list return true
// !!! WARNING !!! It has reverse logic !!! because of pratical reasons
// It is called in the interface.php in data_sheet_loader()
// It is called in here, titkos_adat()
//
function milvus_secret_columns($p,$column) {
    // Called in results_builder.php
    $params = preg_split('/;/',$p);
    $pmi = array();
    foreach($params as $pm) {
        $c = preg_split('/:/',$pm);
        $pmi[] = $c[0];
    }
    $params = $pmi;

    if (!in_array($column,$params)) 
        return 2;
    else 
        return false;
}
function titkos_adat($row,$columns) {
    # Called in results_builder.php before calling any printout method
    # It removes all columns which is not allowed to see
    #
    # if(isset($_SESSION['st_col']['RESTRICT_C']) and $_SESSION['st_col']['RESTRICT_C']!='' and $row[$_SESSION['st_col']['RESTRICT_C']] == 't' and !rst('mod',$row['id']))
    
    if(isset($row['access_to_data']) and $row['access_to_data'] == '1' and !rst('mod',$row['id'])) {
        foreach($columns as $i) {
            if (milvus_secret_columns('species;upid;uploader_name;the_geom;id;observers;data_owner;method;national_program',$i)) {
                unset($row[$i]);
            }
        }
    } elseif(isset($row['access_to_data']) and $row['access_to_data'] == '2' and !rst('mod',$row['id'])) {
        foreach($columns as $i) {
            if (milvus_secret_columns('species;upid;uploader_name;the_geom;id;observers;data_owner;method;national_program;date',$i)) {
                unset($row[$i]);
            }
            if (isset($row[$i]) and $i=='date') {
                $row[$i] = transform_data($row[$i],'date','date:date_yearonly');
            }
        }
    } elseif(isset($row['access_to_data']) and $row['access_to_data'] == '3' and !rst('mod',$row['id'])) {
        //elvileg nincs ilyen opció
        foreach($columns as $i) {
            unset($row[$i]);
        }
    }
    return $row;
}
function milvus_column_list() {
    //should contains C_ID as id!!!
	return "gid as id";
}
function milvus_column_names() {
    return array();
}
function milvus_join_extension() {
    # valami...
    return " LEFT JOIN milvus_programs v ON v.id=orszagos_program LEFT JOIN ved_kat_faj_con con ON con.faj=v.faj ";
}


?>
