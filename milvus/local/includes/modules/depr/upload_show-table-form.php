<?php
/* add extra parameters to input/select fields, depending on column's id
 *
 * */
function extra_params($p) {
    $text = "";
    //if ($p=='titkos') $text = "disabled='true'";
    return $text;
}
/* triggered columns in upload form
 *
 **/
/*function column_trigger($p) {
        $params = preg_split('/;/',$p);
        $pmi = array();
        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);
            $pmi[$c[0]] = $c[1];
        }

        $text = "";
        if (isset($_SESSION['st_col']['PHOTO_ID_C']) and $_SESSION['st_col']['PHOTO_ID_C']!='')
            $text .= "var photo_id_column='{$_SESSION['st_col']['PHOTO_ID_C']}';";
        if (isset($_SESSION['st_col']['GEOM_C']) and $_SESSION['st_col']['GEOM_C']!='')
            $text .= "var geom_id_column='{$_SESSION['st_col']['GEOM_C']}';";
        if (isset($_SESSION['st_col']['DATE_C']) and $_SESSION['st_col']['DATE_C'][0]!='')
            $text .= "var date_column='{$_SESSION['st_col']['DATE_C'][0]}';";

        #$i = array_search('geom',$pmi);
        #if ($i !== false and $i == $col) {
        foreach ($pmi as $k=>$v) {
            $text .= "var $k='$v';";
        
        }
        $text .= '
        $(document).ready(function() {
            var egyseg_idx;
            var faj_idx;
            var titkos_idx;
            var fajok = ["Aquila heliaca", "Aquila chrysaetos", "Falco cherrug", "Falco peregrinus", "Bubo bubo", "Otis tarda", "Tetrao tetrix"];

            //oszlop azon osítók
            $("#upload-data-table thead").find("li").each(function(){ 
                if(typeof egyseg_column !== "undefined" && $(this).attr("id")==egyseg_column) {
                    egyseg_idx = $(this).closest("th").index()+1;
                }
                if(typeof faj_column !== "undefined" && $(this).attr("id")==faj_column) {
                    faj_idx = $(this).closest("th").index()+1;
                }
                if(typeof titkos_column !== "undefined" && $(this).attr("id")==titkos_column) {
                    titkos_idx = $(this).closest("th").index()+1;
                }
            });
            //Kitett mezők
            if( $("#default-unitate_de_masura").val()=="str_nest_cavity" || $("#default-unitate_de_masura").val()=="str_colony" ) {
                if ($("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").length>0) {
                    $("#upload-data-table tbody").find(".cf-"+titkos_idx).val("1");
                }
                $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
                $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
            }
            $(".mezok").on("change","#default-unitate_de_masura",function(){
                if ($(this).val()=="nest_cavity" || $(this).val()=="str_nest_cavity" || $(this).val()=="str_colony"){
                    if ($("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").length>0) {
                        $("#upload-data-table tbody").find(".cf-"+titkos_idx).val("1");
                    }
                    $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
                    $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
                } else {
                    if ($("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=0]").length>0) {
                        $("#upload-data-table tbody").find(".cf-"+titkos_idx).val("0");
                    }
                    $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",true);
                    $("#upload-data-table tbody").find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",true);

                }
            });

            //tábla
            $("#upload-data-table").on("change focusout",".cf-"+faj_idx,function(){
                var rix = $(this).closest("tr").index();
                var a=$.inArray($(this).val(),fajok);
                if(a==-1) {
                    var e = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+egyseg_idx).val();
                    if (e!="str_nest_cavity" && e != "str_colony") {
                        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx)[0].selectedIndex = 0;
                        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).prop("selectedIndex", 0);
                        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).attr("selectedIndex", 0);
                    }
                }
            });
            $("#upload-data-table").on("change focusout",".cf-"+egyseg_idx,function(){
                var rix = $(this).closest("tr").index();
                if ($(this).val()!="str_nest_cavity" && $(this).val()!="str_colony") {
                    var a=$.inArray( $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+faj_idx).val(),fajok);
                    if (a==-1) {
                        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx)[0].selectedIndex = 0;
                        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).prop("selectedIndex", 0);
                        $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).attr("selectedIndex", 0);
                    }
                }

            });
            $("#upload-data-table").on("focus",".cf-"+titkos_idx,function(){
                var rix = $(this).closest("tr").index();
                var Rfaj = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+faj_idx).val();
                var Regy = $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+egyseg_idx).val();

                $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",true);
                $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",true);
                
                var a=$.inArray(Rfaj,fajok);
                if(a!=-1) {
                    $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
                    $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
                } 
                if (Regy == "str_nest_cavity" || Regy == "str_colony") {
                    $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").prop("disabled",false);
                  $("#upload-data-table tbody").find("tr").eq(rix).find(".cf-"+titkos_idx).children("option[value=1]").attr("disabled",false);
                } 
            });
        });';
        return $text;
}
 */

function milvus_get_default_value($p='',$col,$insert_allowed=0) {
    global $BID,$ID,$des,$sn;
    $list = '';
/*    $_SESSION['upload']['script'] = '<script>
$(document).ready(function() {
    $(".mezok").on("change",".milvus-l-program",function(){program_change($(this));});
    $(".mezok").on("change",".milvus-n-program",function(){program_change($(this));});
    $(".mezok").on("change",".milvus-n-gykod",function(){gykod_change($(this));});
    $(".mezok").on("change",".milvus-l-gykod",function(){gykod_change($(this));});
    $(".mezok").on("change",".milvus-n-egys",function(){egys_change($(this));});
    $(".mezok").on("change",".milvus-l-egys",function(){egys_change($(this));});
});
function program_change(w) {
    var element = w.find("option:selected");
    var val = element.data("val");
    //var val=w.val();
    var id=w.attr("id");
    var widgetId=id.substring(id.indexOf("-")+1,id.indexOf("-")+2);
    $.post("ajax_milvus", {"program":val },
        function(data){
            $(".milvus-"+widgetId+"-gykod").html("<option></option>");
            $(".milvus-"+widgetId+"-egys").html("<option></option>");
            var j = {
                gy:null,
            };
            if (data != "") {
                j = JSON.parse(data);
            }
            var gy = new Array("");
            if (j["gy"] != null)
                gy = j["gy"].split(",");
            for (var i = 0; i < gy.length; i++) {
                $(".milvus-"+widgetId+"-gykod").append("<option>"+gy[i]+"</option>");
            }
            $(".milvus-"+widgetId+"-gykod").append("<option>*</option>");
            $(".milvus-"+widgetId+"-gykod").trigger("change");
    });
}
function gykod_change(w) {
    var val=w.val();
    if (w.hasClass("milvus-l-gykod")) widgetId = "l";
    else widgetId = "n";
    var prog = $("#default-national_program").find("option:selected").data("val")
    $.post("ajax_milvus", {"gykod":val,"programm":prog },
        function(data){
            $(".milvus-"+widgetId+"-egys").html("<option></option>");
            var j = JSON.parse(data);
            var a = new Array("");
            if (j["a"] != null)
                a  = j["a"].split(",");
            var gyks = ".milvus-"+widgetId+"-egys";
            for (var i = 0; i < a.length; i++) {
                $(gyks).append("<option>"+a[i]+"</option>");
            }
    });
}
function egys_change(w) {
    var val=w.val();
    if (w.hasClass("milvus-l-egys")) widgetId = "l";
    else widgetId = "n";
    pos = $("#obm_geometry").closest("th").index();
    $.post("ajax_milvus", {"egys":val },
        function(data){
            $("#upload-data-table thead tr:eq(2)").find("th").eq(1+(+pos)).find("input").val(data);
            var input = $("#upload-data-table thead tr:eq(2)").find("th").eq(1+(+pos)).find("input");
            fillfunction(input,"fill");
    });
}

</script>';*/
    $def_modsz = "";
    if($col=='local_grouping_code') return '-';
    if($col=='national_grouping_code') return '-';
    if($col=='local_sampling_unit') return '-';
    if($col=='national_sampling_unit') return '-';
    if($col=='local_program') {
        
        $cmd = "SELECT default_value,list[1] as list FROM project_forms_data WHERE \"column\"='method' AND form_id='{$insert_allowed}'";
        $res = pg_query($BID,$cmd);
        if($row = pg_fetch_assoc($res)) {
            if ($row['default_value'] == '_list')
                $def_modsz =  $row['list'];
            else
                $def_modsz = $row['default_value'];
        }

        $cmd = sprintf("SELECT mp.id,nev,helyi_program,orszagos FROM milvus_programs mp LEFT JOIN milvus_methods mm ON modszer=mm.id WHERE orszagos=FALSE AND name='$def_modsz' ORDER BY nev");
        $res = pg_query($ID,$cmd);

        // default program - 1 value
        $cmd = "SELECT list[1] as list FROM project_forms_data WHERE \"column\"='local_program' AND default_value='_list' AND form_id='{$insert_allowed}'";
        $res2 = pg_query($BID,$cmd);
        if($row2 = pg_fetch_assoc($res2)) {
            $def_program = $row2['list'];
        } else {
            $def_program = '';
        }

        $d = array();
        $disabled='';
        while($row = pg_fetch_assoc($res)){
            $translated = $row['nev'];
            if ($def_program!='' and $def_program == $row['nev']) {
                //only one row!
                $d = array();
                $translated = $def_program;
                $disabled="disabled='true'";
            }

            if( defined($row['nev'])) $translated = constant($row['nev']);
            $d[] = "<option value='{$row['nev']}' data-val='{$row['id']}'>$translated</option>";
            // do not load the other values if there was a default one
            if ($def_program!='' and $def_program == $row['nev']) {
                break;
            }
        }
        $list = sprintf("<div class='table'><div><label>$sn $des</label><select id='default-%s' class='default milvus-l-program' style='min-width:120px' $disabled><option></option>%s</select> &nbsp; <i class='fa fa-hand-o-right fa-lg' style='color:#666'></i></div><div><label>%s</label><select class='default milvus-l-gykod' id='default-local_grouping_code' style='min-width:120px'></select> &nbsp; <i class='fa fa-hand-o-right fa-lg' style='color:#666'></i></div><div><label>%s</label><select class='default milvus-l-egys' id='default-local_sampling_unit' style='min-width:120px'></select></div></div>",$col,implode('',$d),t(str_gykodok),t(str_alapegysegek));
        if (count($d)==1) $list .= "<script>$(document).ready(function(){
            $('.mezok').on('change','#default-local_program',function(){program_change($(this));});
            $('#default-local_program > option:eq(1)').prop('selected',true).trigger('change');</script>";
    }
    elseif($col=='national_program') {
        $cmd = "SELECT default_value,list[1] as list FROM project_forms_data WHERE \"column\"='method' AND form_id='{$insert_allowed}'";
        $res = pg_query($BID,$cmd);
        if($row = pg_fetch_assoc($res)) {
            if ($row['default_value'] == '_list')
                $def_modsz =  $row['list'];
            else
                $def_modsz = $row['default_value'];
        }

        #$cmd = sprintf("SELECT id,nev,helyi_program,orszagos FROM milvus_programs WHERE orszagos=TRUE ORDER BY nev");
        $cmd = sprintf("SELECT mp.id,nev,helyi_program,orszagos FROM milvus_programs mp LEFT JOIN milvus_methods mm ON modszer=mm.id WHERE orszagos=TRUE AND name='$def_modsz' ORDER BY nev");
        $res = pg_query($ID,$cmd);

        // default program - 1 value
        $cmd = "SELECT list[1] as list FROM project_forms_data WHERE \"column\"='national_program' AND default_value='_list' AND form_id='{$insert_allowed}'";
        $res2 = pg_query($BID,$cmd);
        if($row2 = pg_fetch_assoc($res2)) {
            $def_program = $row2['list'];
        } else {
            $def_program = '';
        }
        $d = array();
        $rid = '';
        $disabled='';
        while($row = pg_fetch_assoc($res)){
            $translated = $row['nev'];
            if ($def_program!='' and $def_program == $row['nev']) {
                //only one row!
                $d = array();
                $translated = $def_program;
                $disabled="disabled='true'";
            }

            if( defined($row['nev'])) $translated = constant($row['nev']);
            $d[] = "<option value='{$row['nev']}' data-val='{$row['id']}'>$translated</option>";
            $rid = $row['id'];

            // do not load the other values if there was a default one
            if ($def_program!='' and $def_program == $row['nev']) {
                break;
            }
        }
        $list = sprintf("<div class='table'><div><label>$sn $des</label><select id='default-%s' class='default milvus-n-program' style='min-width:120px' $disabled><option></option>%s</select> &nbsp; <i class='fa fa-hand-o-right fa-lg' style='color:#666'></i></div><div><label>%s</label><select class='default milvus-n-gykod' id='default-national_grouping_code' style='min-width:120px'></select> &nbsp; <i class='fa fa-hand-o-right fa-lg' style='color:#666'></i></div><div><label>%s</label><select class='default milvus-n-egys' id='default-national_sampling_unit' style='min-width:120px'></select></div></div>",$col,implode('',$d),t(str_gykodok),t(str_alapegysegek));
        if (count($d)==1) $list .= "<script>$(document).ready(function(){
            $('.mezok').on('change','#default-national_program',function(){program_change($(this));});
            $('#default-national_program > option:eq(1)').prop('selected',true).trigger('change');});</script>";
    }
    return $list;
}
?>
