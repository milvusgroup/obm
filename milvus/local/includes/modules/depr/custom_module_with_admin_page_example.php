<?php
/**
 * Example module with admin page
 *
 * An example for creating modules with custom admin page. 
 *
 * @author Bóné Gábor <gabor.bone@milvus.ro>
 */

class custom_module_with_admin_page_example {

    /** Returns the label and the url of the custom admin page.  */
    public function getMenuItem() {
        return ['label' => 'Custom module example','url' => __CLASS__];
    }

    /** Creates the html of the admin page */
    public function adminPage() {
        return "<div>Custom module admin page</div>";
    }
}
?>
