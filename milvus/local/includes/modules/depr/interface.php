<?php
function box_filter ($p) {
    global $ID,$BID;
    $t = new table_row_template();
    $s = new select_input_template();

    $params = preg_split('/;/',$p);

    $pm_labels = array();

    if (count($params)) {
        $t->cell(mb_convert_case(str_text_filters, MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();

        $first = 0;
        $options = array();
        foreach ($params as $pm) {
            if ($first==0) {
                $first++;
                continue;
            } else
                $first++;

            $pm = preg_split('/:/',$pm);
            $pm_labels[$pm[0]]=$pm[0];
            $label = $pm[0];
            // should query the translation - automatism not work if the users give strange names
            $cmd = sprintf("SELECT unnest(main_table) FROM projects WHERE project_table='%s'",PROJECTTABLE);
            #log_action($cmd);
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $tables = "'".implode("','", array_column(pg_fetch_all($res), 'unnest'))."'";

                $cmd = sprintf("SELECT short_name FROM project_metaname WHERE project_table IN (%s) AND column_name='%s'",$tables,$pm[0]);
                $res = pg_query($BID,$cmd);
                if (pg_num_rows($res)) {
                    $row = pg_fetch_assoc($res);
                    if (defined($row['short_name'])) {
                        $pm_labels[$pm[0]] = constant($row['short_name']);
                        $label = constant($row['short_name']);
                    }
                }
            }

            $options[] = "<input type='checkbox' class='filter-sh-advopt' value='$pm[0]'> $label<br>";
        }
        $options = implode('',$options);
        $t->cell("<div>".str_options.":<br><div class='pure-u-1' style='height:100px;overflow-y:scroll;'>$options</div></div>",2,'shadow');

        $t->row();
    }


    #$s->set_style("width:100%");
    #$s->new_menu('name_of_the_institute',institute,$ID); 
    #$s->set_style("");
    #sprintf("<tr><td class='title' style='vertical-align:top'>%s: </td><td colspan=3>%s</td></tr>",$s->name,$s->menu);
    $_SESSION['qform'] = array();
    $first = 0;
    foreach($params as $pm) {
        
        $pm = preg_split('/:/',$pm);

//fajkeresés         
        if ($pm[0]=='obm_taxon') {
            if ($first==0)
                $def_hidden = '';
            else
                $def_hidden = 'def-hidden';
            $first++;

            $t->cell(str_taxon_filter,2,'title center');
            $t->row("text-options_obm_taxon $def_hidden");
            $t->cell(str_searchstring,1,'title');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='taxon_sim' value=''>",1,'content');
            $t->row("text-options_obm_taxon $def_hidden");
            $t->cell("".str_taxon_names.":<div id='tsellist'></div>",2,'title');
            $t->row("text-options_obm_taxon $def_hidden");
            $t->cell("<select name='trgm' multiple id='taxon_trgm' style='width:100%;height:60px'></select>",2,'content');
            $t->row("text-options_obm_taxon $def_hidden");
            $t->cell("<input type='radio' id='onematch' name='match' style='vertical-align:bottom'> ".str_one_match."<br>
            <input type='radio' id='allmatch' checked name='match' style='vertical-align:bottom'> ".str_all_match."<br>
            <div style='text-align:center'><button class='button-secondary button-small pure-button' id='speciesplus'><i class='fa-plus fa'></i> ".str_speciesplus."</button></div>",2,'content');
            $t->row("text-options_obm_taxon $def_hidden");
            $first++;
        } 
//dátum szűrő
        elseif($pm[0]=='obm_datum') {
            if ($first==0)
                $def_hidden = '';
            else
                $def_hidden = 'def-hidden';
            $first++;

            $t->cell(str_datum_filter,2,'title center');
            $t->row("text-options_obm_datum $def_hidden");
            $t->cell(str_exact_date,1,'title');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_date' class='qf' value=''>",1,'content');
            $t->row("text-options $def_hidden");
            $t->cell(str_date_period,2,'title');
            $t->row("text-options_obm_datum $def_hidden");
            $t->cell(t(str_from),1,'content');
            $t->cell(t(str_to),1,'content');
            $t->row("text-options_obm_datum $def_hidden");
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_from' class='qf' value=''>",1,'content');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_to' class='qf' value=''>",1,'content');
            $t->row("text-options_obm_datum $def_hidden");
            $t->cell(str_range_period,2,'title');
            $t->row("text-options_obm_datum $def_hidden");
            $t->cell(t(str_from),1,'content');
            $t->cell(t(str_to),1,'content');
            $t->row("text-options_obm_datum $def_hidden");
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_mfrom' class='qf' value=''>",1,'content');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_mto' class='qf' value=''>",1,'content');
            $t->row("text-options_obm_datum $def_hidden");
        } 
        
//Feltöltés dátuma

        elseif($pm[0]=='obm_uploading_date') {
            if ($first==0)
                $def_hidden = '';
            else
                $def_hidden = 'def-hidden';
            $first++;

            $t->cell(str_upload_date,1,'title');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='obm_uploading_date' class='qf' value=''>",1,'content');
            $t->row("text-options_obm_uploading_date $def_hidden");
        } 

//Feltöltő

        elseif($pm[0]=='obm_uploader_user') {
            if ($first==0)
                $def_hidden = '';
            else
                $def_hidden = 'def-hidden';
            $first++;

            $upl_user_box = "<select id='obm_uploader_user' class='qf'><option></option>";
            $cmd = "SELECT DISTINCT uploader_name AS un,uploader_id FROM uploadings u LEFT JOIN ".PROJECTTABLE." d ON (d.obm_uploading_id=u.id) ORDER BY uploader_name";
            $res = pg_query($ID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                $upl_user_box .= "<option value='{$row['uploader_id']}'>{$row['un']}</option>";
            }
            $upl_user_box .="</select>";

            //array_splice($params,$i,1);
            $t->cell(str_uploader,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row("text-options_obm_uploader_user $def_hidden");
        } 

//Országos program

        elseif($pm[0]=='extra_orszagos_program') {
            $upl_user_box = "<select id='orszagos_program' class='milvus-o-program qf'><option></option>";
            $cmd = "SELECT DISTINCT nev AS un,modszer,id FROM milvus_programs u WHERE orszagos=TRUE ORDER BY nev";
            $res = pg_query($ID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                    $label = $row['un'];
                        if (defined($row['un']))
                                    $label = constant($row['un']);
                            $upl_user_box .= "<option value='{$row['un']}' data-val='{$row['id']}'>$label</option>";
                        }
            $upl_user_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_orszagos_program,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('text-options_extra_orszagos_program def-hidden');
        }

//Helyi program
        
        elseif($pm[0]=='extra_helyi_program') {
            $upl_user_box = "<select id='helyi_program' class='milvus-h-program qf'><option></option>";
            $cmd = "SELECT DISTINCT nev AS un,modszer,id FROM milvus_programs u WHERE orszagos=FALSE ORDER BY nev";
            $res = pg_query($ID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                $label = $row['un'];
                if (defined($row['un']))
                    $label = constant($row['un']);
                $upl_user_box .= "<option value='{$row['un']}' data-val='{$row['id']}'>$label</option>";
            }
            $upl_user_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_helyi_program,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('text-options_extra_helyi_program def-hidden');
        }

//Gyűjtőkód
        elseif ($pm[0]=='extra_d_gyujtokod') {
            $upl_user_box = "<select id='gyujtokod' class='milvus-d-gykod qf'><option></option>";
            $upl_user_box .="</select>";
            array_splice($params,$i,1);
            $t->cell(str_gyujtokod,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('text-options_extra_d_gyujtokod def-hidden');
        }

//Alapegység
        elseif($pm[0]=='extra_d_egysegkod') {
            $upl_user_box = "<select id='egysegkod' class='milvus-d-egys qf'><option></option>";
            $upl_user_box .="</select>";
            array_splice($params,$i,1);
            $t->cell(str_egysegkod,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('text-options_extra_d_egysegkod def-hidden');
        }
 
        else {
            if ($first==0)
                $def_hidden = '';
            else
                $def_hidden = 'def-hidden';
            $first++;

            if(isset($pm[1])) {
                $format = $pm[1];
                if (isset($pm[2]))
                    $_SESSION['qform'][$pm[0]] = $pm[2];
            }
            else
                $format = '';
            $pm = $pm[0];

            $label = $pm;
            if (isset($pm_labels[$pm]))
                $label = $pm_labels[$pm];
            

            //betöltött select mező cimkéje
            $t->cell($label,1,'title');
            //betöltött select mező
            $s->new_menu($pm,$format); 
            //betöltött szelkció tartalma
            $t->cell($s->menu,1,'content');
            //a teljes sor létrehozása
            $t->row("text-options_$pm $def_hidden");
        }
    }

    $html = sprintf("<input type='hidden' id='taxon_id'>
        <table class='mapfb'>
        %s
        <tr><td colspan=2 class='title'><button class='button-success button-xlarge pure-button' id='sendQuery'><i class='fa-search fa'></i> ".str_query."</button> <button id='clear-filters' class='button-small pure-button button-href' style='float:right'><i class='fa fa-circle-o-notch'></i> reset</button></td></tr>
        </table>
        <div style='font-size:0.8em' id='responseText'></div>",$t->printOut());
    return $html;
    }
?>
