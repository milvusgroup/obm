<?php
    /* Taxon and other filters
     *
     * */
function box_filter($p) {
        global $ID;
        $t = new table_row_template();
        $s = new select_input_template();

        $params = preg_split('/;/',$p);
echo '<script>
$(document).ready(function() {
    $("body").on("change","#helyi_program",function(){program_change($(this));});
    $("body").on("change","#orszagos_program",function(){program_change($(this));});
    $("body").on("change",".milvus-d-gykod",function(){gykod_change($(this));});
});
function program_change(w) {
    var element = w.find("option:selected");
    var val = element.data("val");
    var id=w.attr("id");
    var wId=id.substring(0,id.indexOf("_"));
    var widgetId = "d";
    if (wId == "helyi") {
        $("#orszagos_program").val("");
        $("#orszagos_program > option:eq(0)").prop("selected",true);
        $(".milvus-"+widgetId+"-gykod").attr("id","helyi_gyujtokod");
        $(".milvus-"+widgetId+"-egys").attr("id","helyi_egysegkod");
    } else {
        $("#helyi_program").val("");
        $("#helyi_program > option:eq(0)").prop("selected",true);
        $(".milvus-"+widgetId+"-gykod").attr("id","orszagos_gyujtokod");
        $(".milvus-"+widgetId+"-egys").attr("id","orszagos_egysegkod");
    }
    $.post("ajax_milvus", {"program":val },
        function(data){
            $(".milvus-"+widgetId+"-gykod").html("<option></option>");
            $(".milvus-"+widgetId+"-egys").html("<option></option>");
            var j = {
                gy:null,
            };
            if (data != "") {
                j = JSON.parse(data);
            }
            var gy = new Array("");
            if (j["gy"] != null)
                gy = j["gy"].split(",");
            for (var i = 0; i < gy.length; i++) {
                $(".milvus-"+widgetId+"-gykod").append("<option>"+gy[i]+"</option>");
            }
            $(".milvus-"+widgetId+"-gykod").append("<option>*</option>");
            $(".milvus-"+widgetId+"-gykod").trigger("change");
    });
}
function gykod_change(w) {
    var val=w.val();
    //if (w.hasClass("extra-helyi-gykod")) widgetId = "h";
    //if (w.attr("id") == "extra-helyi-gykod") widgetId = "helyi";
    //else widgetId = "orszagos";
    var widgetId = "d";
    var prog = $("#orszagos_program").find("option:selected").data("val")
    $.post("ajax_milvus", {"gykod":val,"programm":prog },
        function(data){
            $(".milvus-"+widgetId+"-egys").html("<option></option>");
            var j = JSON.parse(data);
            var a = new Array("");
            if (j["a"] != null)
                a  = j["a"].split(",");
            for (var i = 0; i < a.length; i++) {
                $(".milvus-"+widgetId+"-egys").append("<option>"+a[i]+"</option>");
            }
    });
}
</script>';

        $i = array_search('obm_taxon',$params);
        if ($i !== false) {
            array_splice($params,$i,1);
            $t->cell(mb_convert_case(str_text_filters, MB_CASE_UPPER, "UTF-8"),2,'title center');
            $t->row();
            $t->cell(str_taxon_filter,2,'title center');
            $t->row();
            $t->cell(str_searchstring,1,'title');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='taxon_sim' value=''>",1,'content');
            $t->row();
            $t->cell("".str_taxon_names.":<div id='tsellist'></div>",2,'title');
            $t->row();
            $t->cell("<select name='trgm' multiple id='taxon_trgm' style='width:100%;height:60px'></select>",2,'content');
            $t->row();
            $t->cell("<input type='radio' id='onematch' name='match' style='vertical-align:bottom'> ".str_one_match."<br>
            <input type='radio' id='allmatch' checked name='match' style='vertical-align:bottom'> ".str_all_match."<br>
            <div style='text-align:center'><button class='button-secondary button-small pure-button' id='speciesplus'><i class='fa-plus fa'></i> ".str_speciesplus."</button></div>",2,'content');
            $t->row();
        }

        if (count($params)) {
            $t->cell("<div><button class='pure-button button-href pure-u-1' id='filter-sh-advopt'>".str_more_options."...</button></div>",2,'shadow');
            $t->row();
        }

        $i = array_search('obm_datum',$params);
        if ($i !== false) {
            array_splice($params,$i,1);
            $t->cell(str_datum_filter,2,'title center');
            $t->row('advanced-options def-hidden');
            $t->cell(str_exact_date,1,'title');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_date' class='qf' value=''>",1,'content');
            $t->row('advanced-options def-hidden');
            $t->cell(str_date_period,2,'title');
            $t->row('advanced-options def-hidden');
            $t->cell(t(str_from),1,'content');
            $t->cell(t(str_to),1,'content');
            $t->row('advanced-options def-hidden');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_from' class='qf' value=''>",1,'content');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='exact_to' class='qf' value=''>",1,'content');
            $t->row('advanced-options def-hidden');
            $t->cell(str_range_period,2,'title');
            $t->row('advanced-options def-hidden');
            $t->cell(t(str_from),1,'content');
            $t->cell(t(str_to),1,'content');
            $t->row('advanced-options def-hidden');
            $t->cell("<input style='width:100%;font-size:15px' type='text' class='qf' id='exact_mfrom' value=''>",1,'content');
            $t->cell("<input style='width:100%;font-size:15px' type='text' class='qf' id='exact_mto' value=''>",1,'content');
            $t->row('advanced-options def-hidden');
            
            $t->cell('',2,'shadow');
            $t->row('advanced-options def-hidden');
            $t->cell(str_other_filters,2,'title center');
            $t->row('advanced-options def-hidden');
        } else {
            $t->cell('',2,'shadow');
            $t->row('advanced-options def-hidden');
            $t->cell(str_other_filters,2,'title center');
            $t->row('advanced-options def-hidden');
        
        }
        $i = array_search('obm_uploading_date',$params);
        if ($i !== false) {
            array_splice($params,$i,1);
            $t->cell(str_upload_date,1,'title');
            $t->cell("<input style='width:100%;font-size:15px' type='text' id='obm_uploading_date' class='qf' value=''>",1,'content');
            $t->row('advanced-options def-hidden');
        }
        $i = array_search('obm_uploader_user',$params);
        if ($i !== false) {
            $upl_user_box = "<select id='obm_uploader_user' class='qf'><option></option>";
            $cmd = "SELECT DISTINCT uploader_name AS un,uploader_id FROM uploadings u LEFT JOIN ".PROJECTTABLE." d ON (d.uploading_id=u.id) ORDER BY uploader_name";
            $res = pg_query($ID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                $upl_user_box .= "<option value='{$row['uploader_id']}'>{$row['un']}</option>";
            }
            $upl_user_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_uploader,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('advanced-options def-hidden');
        }
        $i = array_search('extra_orszagos_program',$params);
        if ($i !== false) {
            $upl_user_box = "<select id='orszagos_program' class='milvus-o-program qf'><option></option>";
            $cmd = "SELECT DISTINCT nev AS un,modszer,id FROM milvus_programs u WHERE orszagos=TRUE ORDER BY nev";
            $res = pg_query($ID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                $label = $row['un'];
                if (defined($row['un']))
                    $label = constant($row['un']);
                $upl_user_box .= "<option value='{$row['un']}' data-val='{$row['id']}'>$label</option>";
            }
            $upl_user_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_orszagos_program,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('advanced-options def-hidden');
        }
        $i = array_search('extra_helyi_program',$params);
        if ($i !== false) {
            $upl_user_box = "<select id='helyi_program' class='milvus-h-program qf'><option></option>";
            $cmd = "SELECT DISTINCT nev AS un,modszer,id FROM milvus_programs u WHERE orszagos=FALSE ORDER BY nev";
            $res = pg_query($ID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                $label = $row['un'];
                if (defined($row['un']))
                    $label = constant($row['un']);
                $upl_user_box .= "<option value='{$row['un']}' data-val='{$row['id']}'>$label</option>";
            }
            $upl_user_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_helyi_program,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('advanced-options def-hidden');
        }
        $i = array_search('extra_d_gyujtokod',$params);
        if ($i !== false) {
            $upl_user_box = "<select id='gyujtokod' class='milvus-d-gykod qf'><option></option>";
            $upl_user_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_gyujtokod,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('advanced-options def-hidden');
        }
        $i = array_search('extra_d_egysegkod',$params);
        if ($i !== false) {
            $upl_user_box = "<select id='egysegkod' class='milvus-d-egys qf'><option></option>";
            $upl_user_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_egysegkod,1,'title');
            $t->cell($upl_user_box,1,'content');
            $t->row('advanced-options def-hidden');
        }
        $i = array_search('teljes_lista',$params);
        if ($i !== false) {
            $t_box = "<select id='teljes_lista' class='qf'><option></option>";
            $t_box .="<option value='0'>".str_no."</option><option value=1>".str_yes."</option>";
            $t_box .="</select>";

            array_splice($params,$i,1);
            $t->cell(str_teljes_lista,1,'title');
            $t->cell($t_box,1,'content');
            $t->row('advanced-options def-hidden');
        }

        #$s->set_style("width:100%");
        #$s->new_menu('name_of_the_institute',institute,$ID); 
        #$s->set_style("");
        #sprintf("<tr><td class='title' style='vertical-align:top'>%s: </td><td colspan=3>%s</td></tr>",$s->name,$s->menu);
        $_SESSION['qform'] = array();
        foreach($params as $pm) {
            $format = '';
            $pm = preg_split('/:/',$pm);
            if(isset($pm[1])) {
                $format = $pm[1];
                if (isset($pm[2]))
                    $_SESSION['qform'][$pm[0]] = $pm[2];
            }
            $pm = $pm[0];

            $label = $pm;
            #$clab = sprintf("str_cn_%s",$pm);
            $clab = sprintf("str_%s",$pm);
            if (defined($clab))
                $label = constant($clab);

            $t->cell($label,1,'title');
            $s->new_menu($pm,$format); 
            $t->cell($s->menu,1,'content');
            $t->row('advanced-options def-hidden');
        }

        $html = sprintf("<input type='hidden' id='taxon_id'>
            <table class='mapfb'>
            %s
            <tr><td colspan=2 class='title'><button class='button-success button-xlarge pure-button' id='sendQuery'><i class='fa-search fa'></i> ".str_query."</button>
<button id='clear-filters' class='button-small pure-button button-href' style='float:right'><i class='fa fa-circle-o-notch'></i> reset</button>
</td></tr>
            </table>
            <div style='font-size:0.8em' id='responseText'></div>",$t->printOut());
        return $html;

}

?>
