<?php
if ($path[0] === 'monitoring') {
    $load = 'view.php';
    $chunks['includes'] = 'monitoring';
    if (isset($path[1]) && in_array($path[1], ['haris','glaucidium','raptor','strix'])) {
        $chunks['subpage'] = $path[1];
    }
}
if ($path[0] === 'help') {
    $load = 'view.php';
    $chunks['includes'] = 'help';
    if (isset($path[1]) && in_array($path[1], ['tutoriale', 'tutoriale_video'])) {
        $chunks['subpage'] = $path[1];
    }
}
if (in_array('ajax_milvus', $path)) {
    $load = '../local/includes/private/afuncs.php';
}
if (in_array($path[0], ['terms', 'privacy', 'cookies'])) {
    $load = 'view.php';
    $chunks['includes'] = $path[0];
}
