<?php
# that is the mapserver wrapper!
# this env-var was set in .htaccess

# Options -Indexes
# RewriteEngine On
# RewriteCond %{QUERY_STRING} !MAP=PMAP [NC]
# RewriteRule (.*) /mapserv [R=301,L]
# SetEnvIf Request_URI "(.*)" ACCESS=public
# <Files *.map>
#    Order Deny,Allow
#    Deny from all
#    #Allow from 127.0.0.1
# </Files>

//ini_set("log_errors", 1);
//ini_set("error_log", "/tmp/php-error.log");

session_start();

require_once(getenv('OB_LIB_DIR').'db_funcs.php');

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

// $_SESSION['current_query_table'] = 'openherpmaps_atlas';
// 
// 
// // If login session expired
// if (!isset($_SESSION['token'])) {
//     require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
//     st_col('default_table');
// }
// # project switch check
// if (isset($_SESSION['current_query_table'])) {
//     if (!preg_match('/^'.PROJECTTABLE.'/',$_SESSION['current_query_table'])) {
//         require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
//     }
// }

/* Use cache 
 * 
 * XCache not works with php-7 
 *
 * Every SQL query should be cached here!
 *
 * */

$MAP = realpath('./urban_atlas.map');

$XML_TYPES = array('GetCapabilities','DescribeFeatureType','GetFeature','GetFeatureInfo','GetSchemaExtension');
$IMG_TYPES = array('GetMap','GetLegendGraphic');

$MAPSERVER = (defined('MAPSERVER')) ? MAPSERVER."?" : "http://localhost/cgi-bin/mapserv?";

$referer = '';

if ($MAP!=''){
    $extent = '';

    //will be processed in .htaccess
    //maybe it is used only in public access: to change the real path to dummy path
    $map = "&ACCESS=$MAP";

    // parse QUERY_STRING
    parse_str($_SERVER['QUERY_STRING'], $HttpQuery);

    //assembling SQL query string using ACCESS level text filters and spatial filters and subfilters
    //wms layers param is the reference
    $query = '';

    if (isset($HttpQuery['layers'])) {
        $HttpQuery['LAYERS'] = $HttpQuery['layers'];
        $HttpQuery['BBOX'] = $HttpQuery['bbox'];
        $HttpQuery['CNAME'] = $HttpQuery['cname'];
    }

    $filter = "";
    switch ($HttpQuery['CNAME']) {
        case 'species_count':
            $tbl = 'milvus.atlas_species_count';
            break;

        case 'list_count':
            $tbl = 'milvus.atlas_list_count';
            break;

        case 'species_map':
            $tbl = 'milvus.atlas_species_map';
            if (!isset($HttpQuery['species'])) {
                exit;
            }
            $filter = sprintf(' WHERE species = %s', quote(preg_replace('/[^a-z ]/i','',$HttpQuery['species'])));
        
        default:
            
            break;
    }

    $query = sprintf("SELECT obm_id, obm_geometry , 0 AS selected FROM %s %s", $tbl, $filter);

    //log_action( $query,__FILE__,__LINE__);

    //push query and assemble the new url
    $HttpQuery['query'] = $query;
    $mapserver_get_string = http_build_query($HttpQuery);
    $mapserver_get_string = preg_replace('/TILE=X%2BY%2BZ/','TILE=X+Y+Z',$mapserver_get_string);

    # Assemble the final mapserver GET url with forcing real BBOX
    $url = $mapserver_get_string.$extent.$map;

    // Close session write
    session_write_close();

    /* we don't print the output xml if has refered from the web apps... */
    if (isset($_SERVER['HTTP_REFERER']))
        $referer = implode("/",array_slice(preg_split("/\//",$_SERVER['HTTP_REFERER']), 2,-1));

    /* show xml response in local query
     * This called from the maps.js->#customLayer".click function
     * when somebody load a custom shp file as a new Layer */
    if (isset($HttpQuery['showXML'])) {
        $referer = ''; 
    }

    // Automatic authentication by using _SESSION login parameters
    /*if (isset($_SESSION['Tth'])) {
        //currently it is not works, because I don't know how deal with php password_hash crypt compatible passwords
        //and it causes segmentation faults
        //...
        $auth = sprintf("Authorization: Basic %s",base64_encode("{$_SESSION['Tmail']}:{$_SESSION['Tth']}"));
        $opts = array('http'=>array('method'=>'GET','header'=>$auth));
        $context = stream_context_create($opts);
        $fp = fopen($MAPSERVER.$url, 'r', false, $context);
    } else { */

    // POST not works, POST-XML?
    //$opts = array('http'=>array('method'=>'POST','header'=>"Content-type: application/x-www-form-urlencoded\r\n",'content'=>$mapserver_get_string));
    //$context = stream_context_create($opts);
    //$fp = fopen($MAPSERVER, 'r', false, $context);
    $fp = fopen($MAPSERVER.$url, 'r', false);
    //}

    if (isset($HttpQuery['REQUEST'])) {
        if (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $XML_TYPES))!==false) $type = 'xml';
        elseif (array_search(strtolower($HttpQuery['REQUEST']), array_map('strtolower', $IMG_TYPES))!==false) $type = 'image';
        else $type = 'unknown';
    } else {
        # skip_processing: paging...
        $type = '';
    }

    //close pg here
    //pg_close($ID);

    if ($type=='xml') {
        # xml output
        $ses = session_id();

        # rewrite the wfs:FeatureCollection xsi:schemaLocation
        //$content = preg_replace("/http:\/\/localhost\/cgi-bin\/mapserv/ius","http://localhost{$_SERVER['PHP_SELF']}/",$content);
        $e = 0;

        $list = glob(OB_TMP."conts_$ses*.xml");
        natsort($list);
        $file = array_pop($list);
        $m = array();
        if (preg_match('/(\d)+\.xml/',$file,$m)) {
            $e = $m[1];
        }
        while (file_exists(OB_TMP."conts_$ses-$e.xml")) {
            $e++;
        }
        $filename=OB_TMP."conts_$ses-$e.xml";
        $dest = fopen($filename, 'w');
        if ($fp===false) {
            error_log("WFS XML READ ERROR");
            exit;
        }
        if ($dest===false) {
            error_log("WFS XML WRITE ERROR");
            exit;
        }

        if(!stream_copy_to_stream($fp,$dest)) {
            error_log( "ERROR WHILE STREAMING XML DATA TO FILENAME: $filename\n" );
            exit;
        }

        //file_put_contents("/mnt/data/tmp/conts_$ses-$e.xml", $content);
        /* we don't print the output xml if has refered from the web apps... 
         * if we don't want to draw with the setHTML() */
        if (URL!=$referer) {
            header('Content-type: text/xml');
            $handle = fopen($filename, "rb");
            echo fread($handle, filesize($filename));
            //echo $content;
        }
        exit;
    } elseif($type=='image') {
        if (is_resource($fp)) {

            if (isset($HttpQuery['FORMAT'])) $format = $HttpQuery['FORMAT'];
            else $format = 'image/png';
            header("Content-type: $format");
            echo stream_get_contents($fp);
            //echo $content;
        } else {
            log_action('Mapserver does not available',__FILE__,__LINE__);
        }
        exit;
    } else {
        echo stream_get_contents($fp);
        //echo $content;
        exit;
    }
} else {
    print "No map defined.";
    exit;
}

#xdebug_stop_trace();


?>
