<?php 
$path = $argv[1];
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'/local_vars.php.inc');
require_once($path.'/includes/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");

require_once($path.'/includes/default_modules.php');
require_once($path.'/includes/modules/validation.php');
require_once($path.'/includes/admin_pages/jobs/observado_dupla_feltoltesek.php');

job_log('observado_dupla_feltoltesek started');

if (!class_exists('observado_dupla_feltoltesek')) {
    job_log("class observado_dupla_feltoltesek does not exists\n");
    exit();
}
if (!method_exists('observado_dupla_feltoltesek','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
call_user_func(['observado_dupla_feltoltesek','run']);

?>
