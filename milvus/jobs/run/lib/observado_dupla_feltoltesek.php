<?php
class observado_dupla_feltoltesek extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        global $ID;
/*
        //checking jobs table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '%s_jobs_results' AND column_name = '%s');", PROJECTTABLE, __CLASS__);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('ALTER TABLE "public"."%1$s_jobs_results" ADD COLUMN %2$s %3$s;', PROJECTTABLE, __CLASS__, 'jsonb' );
        }
        if (!empty($cmd1) && !self::query($ID,$cmd1)) {
            debug(__CLASS__ . ' init failed', __FILE__,__LINE__);
            return;
        }
 */
        debug('observado_dupla_feltoltesek initialized',__FILE__,__LINE__);
        return true;
    }

    public function get_results() {
        global $ID;
        
        $cmd[] = 'SELECT * FROM (
            SELECT DISTINCT \'<a href="https://openbirdmaps.ro/index.php?history=upload&id=\' || result || \'" target="_blank"> \' || result || \'</a>\' as uploading_id, count(*) as count, uploader_name, uploading_date 
            FROM milvus_jobs_results LEFT JOIN system.uploadings ON result::integer = id 
            WHERE data_table = \'milvus\' AND test_name = \'observado_dupla_feltoltesek\' GROUP BY uploading_id, uploader_name, uploading_date ORDER BY uploading_id DESC) foo WHERE count % 20 = 0;';
        
        if (!$res = query($ID, $cmd)) {
            return "ERROR: query error";
        }
        
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader(['uploading_id','count', 'uploader_name','uploading_date']);
        
        for ($i = 0, $l = count($cmd); $i < $l; $i++) {
            while ($row = pg_fetch_assoc($res[$i])) {
                $tbl->addRows($row);
            }
        }

        return $tbl->printOut();
    }

    static function run() {
        global $path, $ID;
        
        $cmd[] = "WITH 
            multi as (
                SELECT DISTINCT obm_uploading_id, source_id FROM (
                    SELECT obm_uploading_id, source_id, count(*) from milvus m 
                    LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id  
                    WHERE project_table = 'milvus' AND source_id IS NOT NULL 
                    GROUP BY source_id, obm_uploading_id
                ) as foo WHERE count > 1
            ), 
            new AS (
                SELECT obm_id, m.obm_uploading_id FROM milvus m 
                LEFT JOIN multi ON m.obm_uploading_id = multi.obm_uploading_id AND m.source_id = multi.source_id 
                WHERE multi.obm_uploading_id IS NOT NULL
            ),
            old AS (
                SELECT row_id FROM milvus_jobs_results WHERE data_table = 'milvus' AND test_name = 'observado_dupla_feltoltesek'
            )
            SELECT obm_id as new, row_id as corrected, new.obm_uploading_id as uplid
            FROM new
            FULL JOIN old ON obm_id = row_id
            WHERE row_id IS NULL OR obm_id IS NULL;";
            
        if (!$res = query($ID, $cmd)) {
            job_log(__CLASS__.' error: query error');
            return;
        }
        if (!$results = pg_fetch_all($res[0])) {
            return;
        }
        $news = array_filter($results, function($row) {
            return $row['new'] !== null;
        });
        $new_uplids = [];
        foreach ($news as $row) {
            $new_uplids[$row['new']] = $row['uplid'];
        }
        $new_ids = array_column($news, 'new');
        
        unset($news);
        
        $corrected = array_values(array_filter(array_column($results, 'corrected'), function($id) {
            return ($id !== null);
        }));
        
        $cmd = [];
        if (count($corrected)) {
            $cmd[] = sprintf("DELETE FROM %s_jobs_results WHERE data_table = %s AND row_id IN (%s) AND test_name = '%s' AND result LIKE %s;",PROJECTTABLE, quote($col->table), implode(',',$corrected),__CLASS__, quote($col->column . '%'));
        }
        
        if (count($new_ids)) {
            $insertValues = [];
            foreach ($new_ids as $id) {
                $insertValues[] = sprintf("('milvus',%s,'%s',%s)", $id, __CLASS__, $new_uplids[$id]);
            }
            
            $cmd[] = sprintf("INSERT INTO %s_jobs_results (data_table, row_id, test_name, result) VALUES %s ;", PROJECTTABLE, implode( ',' , $insertValues ) );
        }
        
        if (count($cmd)) {
            query($ID, $cmd);
        }
    }

}
?>
