<?php
class linnaeus_job extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        log_action('linnaeus_job initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID;
        $jobs = parent::getJobParams(__CLASS__);
        $data_table = (isset($jobs->data_table)) ? $jobs->data_table : PROJECTTABLE;
        
        if (isset($jobs->src_column)) {
            L_Job::species_name_validation($jobs->src_column, $data_table);
        }
        if (isset($jobs->count_observations)) {
            L_Job::count_observations($data_table);
        }
        if (isset($jobs->refreshMatViews)) {
            L_Job::refreshMatViews();
        }
        if (isset($jobs->vacuumTaxonTable)) {
            L_Job::vacuumTaxonTable();
        }
        
        if (isset($jobs->match_terms)) {
            $modules = new modules();
            $modules->set_modules();
            $l_params = $modules->_include('linnaeus', 'get_params');
            L_Job::match_terms($l_params['managed_columns'] ?? null, $data_table);
        }
        
    }
}

/**
 * Job methods for the linnaeus module
 */
class L_Job
{
    
    static function species_name_validation($source_lang, $data_table) {
        global $ID;
        
        if (!in_array($source_lang, getColumnsOfTables())) {
            job_log(__CLASS__ . ' ERROR: Invalid column name: ' . $source_lang);
            return;
        }
        
        $st_col = st_col('default_table', 'array', 0);
        $languages = TaxonManager::taxon_columns();
        
        $cmd = [];
        foreach ($languages as $taxon_lang) {
            // IMPORTANT!!! source species column can not be modified
            if ($taxon_lang === $source_lang) {
                continue;
            }
            // Updating the automatic species columns based on the taxon_table
            $c = sprintf(
                getSQL('species_name_validation', getenv('OB_LIB_DIR') . 'modules/linnaeus.sql'), 
                $source_lang, 
                $st_col['SPECIES_C_SCI'], 
                $taxon_lang, 
                PROJECTTABLE,
                $data_table
            );
                    
            $cmd[$taxon_lang . '_validate'] = $c;
            
        }
        
        $start = microtime(true);
        if (!$res = query($ID, $cmd)) {
            job_log(pg_last_error($ID));
            exit;
        }
        
        $aff_rows = pg_affected_rows($res[0]);
        $duration = round(microtime(true) - $start, 2);
        job_log(__CLASS__ . "::" . __FUNCTION__ . ": Validált fajnevek száma: $aff_rows. Végrehajtási idő: $duration mp");
        
    }
    
    static function count_observations($data_table) {
        global $ID;
        
        $st_col = st_col('default_table', 'array', 0);
        $languages = TaxonManager::taxon_columns();
        
        $cmd = [];
        foreach ($languages as $taxon_lang) {
            // recalculating the taxon_db column of the taxon_table
            $cmd[$taxon_lang . '_count'] = sprintf(
                getSQL('count_observations', getenv('OB_LIB_DIR') . 'modules/linnaeus.sql'),
                PROJECTTABLE,
                "\"$taxon_lang\"",
                quote($taxon_lang),
                $data_table
            );
        }
        $start = microtime(true);
        if (!$res = query($ID, $cmd)) {
            job_log(pg_last_error($ID));
            exit;
        }
        
        $aff_rows = array_reduce($res, function ($sum, $r) {
            $sum += pg_affected_rows($r);
            return $sum;
        });
        $duration = round(microtime(true) - $start, 2);
        job_log(__CLASS__ . "::" . __FUNCTION__ . ": Végrehajtási idő: $duration mp");
    }
    
    static function refreshMatViews() {
        global $ID;
        $st_col = st_col('default_table', 'array', 0);
        
        if (!count($st_col['NATIONAL_C'])) {
            return;
        }
        $start = microtime(true);
        $cmd = array_values(array_map(function($col) use ($st_col) {
            return sprintf(" REFRESH MATERIALIZED VIEW temporary_tables.%s_taxon_%s_%s; ", PROJECTTABLE, $col, $st_col['SPECIES_C_SCI']);
        }, $st_col['NATIONAL_C']));
        query($ID, $cmd);
        $duration = round(microtime(true) - $start, 2);
        job_log(__CLASS__. "::" . __FUNCTION__ . ": MatView frissítés: $duration mp");
    }
    
    static function vacuumTaxonTable() {
        global $ID;
        
        $start = microtime(true);
        $cmd = sprintf("VACUUM %s_taxon;", PROJECTTABLE);
        pg_query($ID, $cmd);
        $duration = round(microtime(true) - $start, 2);
        job_log(__CLASS__ . "::" . __FUNCTION__ . ": Vacuum: $duration mp");
    }
    
    static function match_terms($columns, $data_table = PROJECTTABLE) {
        global $ID;
        $thisrun = date("Y-m-d H:i:s");
        $thisrunstatus = 'ok';
        if (!$columns) {
            job_log("linnaeus param: 'managed_columns' not defined");
            return;
        }
        if (!$lastrun = self::last_run('match_terms')) {
            $thisrunstatus = 'failed';
        }
        $start = microtime(true);
        
        $aff_rows = 0;
        if ($lastrun) {
            foreach ($columns as $col) {
                if (!$col['multiterm']) {
                    $cmd = sprintf(
                        getSQL('match_terms_singleterm', getenv('OB_LIB_DIR') . 'modules/linnaeus.sql'),
                        "{$col['name']}_ids",
                        PROJECTTABLE,
                        quote($data_table),
                        quote($col['name']),
                        quote($lastrun),
                        $col['name'],
                        $data_table
                    );
                }
                else {
                    $cmd = sprintf(
                        getSQL('match_terms_multiterm', getenv('OB_LIB_DIR') . 'modules/linnaeus.sql'),
                        "{$col['name']}_ids",
                        PROJECTTABLE,
                        quote($data_table),
                        quote($col['name']),
                        quote($lastrun),
                        $col['name'],
                        $data_table
                    );
                }
                if (!$res = pg_query($ID, $cmd)) {
                    $thisrunstatus = 'failed';
                    job_log(__CLASS__."::".__FUNCTION__.": " . pg_result_error($res) );
                    continue;
                }
                $aff_rows += pg_affected_rows($res);
            }
        }
        if ($thisrunstatus == 'ok') {
            $res = pg_query($ID, sprintf(getSQL('reset_updated_at', getenv('OB_LIB_DIR') . 'modules/linnaeus.sql'), PROJECTTABLE));
        }
        $duration = round(microtime(true) - $start, 2);
        job_log(__CLASS__."::".__FUNCTION__.": $thisrunstatus <$thisrun>, duration <$duration>, affected_rows <$aff_rows>" );
    }
    
    static private function last_run($job, $n = 1) {
        global $path;
        $data = shell_exec("grep $job " . $path . "jobs/event.log | tail -n $n | head -n 1");
        if (!$data) {
            job_log('TODO - mi legyen, ha még nem futott?');
            return false;
        }
        elseif ($n > 30) {
            job_log('There were more than 3000 failed job runs. Please check the problem!');
            return false;
        }
        elseif (!preg_match('/ok <([0-9-: ]*)>/', $data, $out)) {
            return self::last_run($job, $n+1);
        }
        else {
            return $out[1];
        }
        
    }
}

?>
