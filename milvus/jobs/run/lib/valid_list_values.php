<?php
class valid_list_values extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        global $ID;
/*
        //checking jobs_jobs table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '%s_jobs_results' AND column_name = '%s');", PROJECTTABLE, __CLASS__);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('ALTER TABLE "public"."%1$s_jobs_results" ADD COLUMN %2$s %3$s;', PROJECTTABLE, __CLASS__, 'jsonb' );
        }
        if (!empty($cmd1) && !self::query($ID,$cmd1)) {
            debug(__CLASS__ . ' init failed', __FILE__,__LINE__);
            return;
        }
 */
        debug('valid_list_values initialized',__FILE__,__LINE__);
        return true;
    }

    public function get_results() {
        global $ID;
        $params = parent::getJobParams(__CLASS__);
        $rules = [];
        $cmd = [];
        foreach($params as $col) {
            $cmd[] = sprintf('WITH ids AS (SELECT row_id FROM %1$s_jobs_results WHERE data_table = %2$s AND test_name = \'%3$s\' AND result LIKE %4$s) SELECT DISTINCT \'%6$s\' as table, \'%5$s\' as column, %5$s as invalid_value, count(*) as count from %6$s, ids where obm_id = ids.row_id GROUP BY %5$s ORDER BY "table", "column", invalid_value;', PROJECTTABLE, quote($col->table), __CLASS__, quote($col->column . '%'), $col->column, $col->table);
        }
        if (!$res = query($ID, $cmd)) {
            return "ERROR: query error";
        }
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader(['data_table','column','invalid_value','count']);
        for ($i = 0, $l = count($cmd); $i < $l; $i++) {
            while ($row = pg_fetch_assoc($res[$i])) {
                $tbl->addRows($row);
            }
        }

        return $tbl->printOut();
    }

    static function run() {
        global $path, $ID;

        $start = microtime(true);

        $params = parent::getJobParams(__CLASS__);
        $rules = [];
        foreach($params as $col) {
            $cmd = [];
            $valid_values = array_column(self::prepareOptions($col->options),'value');

            $vv = implode(',', array_map( function($el) {
                return "'$el'";
            } , $valid_values));

            $cmd[] = sprintf('
                WITH
                    new AS (
                        SELECT obm_id FROM %1$s WHERE %4$s NOT IN (%5$s)
                    ),
                    old AS (
                        SELECT row_id FROM %2$s_jobs_results WHERE data_table = %3$s AND test_name = \'%6$s\' AND result LIKE %7$s
                    )
                    SELECT obm_id as new, row_id as corrected
                    FROM new
                    FULL JOIN old ON obm_id = row_id
                    WHERE row_id IS NULL OR obm_id IS NULL;',
                $col->table,
                PROJECTTABLE,
                quote($col->table),
                $col->column,
                $vv,
                __CLASS__,
                quote($col->column.'%')
            );

            if (!$res = query($ID, $cmd)) {
                job_log(__CLASS__.' error: query error');
                continue;
            }
            if (!$results = pg_fetch_all($res[0])) {
                continue;
            }

            $new_ids = array_values(array_filter(array_column($results, 'new'), function($id) {
                return ($id !== null);
            }));
            $corrected = array_values(array_filter(array_column($results, 'corrected'), function($id) {
                return ($id !== null);
            }));

            $cmd = [];
            if (count($corrected)) {
                $cmd[] = sprintf("DELETE FROM %s_jobs_results WHERE data_table = %s AND row_id IN (%s) AND test_name = '%s' AND result LIKE %s;",PROJECTTABLE, quote($col->table), implode(',',$corrected),__CLASS__, quote($col->column . '%'));
            }

            if (count($new_ids)) {
                $insertValues = [];
                foreach ($new_ids as $id) {
                    $insertValues[] = sprintf("(%s,%s,'%s',%s)", quote($col->table), $id, __CLASS__, quote($col->column));
                }

                $cmd[] = sprintf("INSERT INTO %s_jobs_results (data_table, row_id, test_name, result) VALUES %s ;", PROJECTTABLE, implode( ',' , $insertValues ) );
            }

            if (count($cmd)) {
                query($ID, $cmd);
            }
        }
        $duration = round(microtime(true) - $start, 2);
        $duration = ($duration >= 60) ? floor($duration / 60) . " perc " . ($duration % 60) . " mp" : "$duration mp";
        job_log(__CLASS__. ": $duration");
    }

    static function prepareOptions($j) {
        global $ID;

        $preFilter = "1 = 1";
        if (isset($j->preFilterColumn) and $j->preFilterColumn != '') {
            $preFilter = prepareFilter($j->preFilterColumn, $j->preFilterValue, $j->preFilterRelation ??     null);
        }

        $schema = (isset($j->optionsSchema) && $j->optionsSchema !== '') ? $j->optionsSchema : 'public';
        $table = $j->optionsTable;
        $valueColumn = $j->valueColumn;
        $labelColumn = (isset($j->labelColumn) and $j->labelColumn!='' ) ? $j->labelColumn : $j->valueColumn;

        $cmd = sprintf( 'SELECT DISTINCT %1$s AS value, %2$s AS label FROM %3$s.%4$s WHERE %5$s ORDER BY %1$s', $valueColumn, $labelColumn, $schema, $table, $preFilter);
        return ($res = query($ID,$cmd)) ? pg_fetch_all($res[0]) : [];
    }

}
?>
