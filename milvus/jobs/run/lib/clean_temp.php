<?php
class clean_temp extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('clean_jobs initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID;

        job_log("running clean_jobs module");
        $params = parent::getJobParams(__CLASS__);
        
        extract((array)$params);

        // Read variables
        $limit = isset($query_limit) ? $query_limit : 2000; // how much data will be droped at once
        $create_drop_funcs = isset($create_funcs) ? $create_funcs : FALSE;
 
        // drop orphaned imports
        $cmd = "DELETE FROM system.temp_index 
                WHERE table_name IN ( 
                    SELECT table_name FROM system.temp_index 
                        LEFT JOIN system.imports ON table_name=file 
                    WHERE table_name LIKE '". PROJECTTABLE ."%' AND project_table IS NULL AND temp_index.datum<NOW() - INTERVAL '1 DAY' 
                    LIMIT $limit)";
        $result = pg_query($ID,$cmd);


        $cmd = 'CREATE OR REPLACE FUNCTION drop_table(name TEXT)
                  RETURNS void AS
                $BODY$
                DECLARE statement TEXT;
                BEGIN
                statement := \'DROP TABLE temporary_tables.\' || name;
                EXECUTE statement;
                END;
                $BODY$
                  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
                  COST 100;';
        if ($create_drop_funcs)
            $result = pg_query($ID,$cmd);

        // drop empty tables;
        // ANALYZE TABLES FIRST!!!
        $cmd ='DO $$
            DECLARE
                tab RECORD;
                schemaName VARCHAR := \'temporary_tables\';
            BEGIN
                for tab in (select t.relname::varchar AS table_name
                    FROM pg_class t
                    JOIN pg_namespace n ON n.oid = t.relnamespace
                    WHERE t.relkind = \'r\' and n.nspname::varchar = schemaName
                    order by 1)
                LOOP
                    RAISE NOTICE \'ANALYZE %1.%2\', schemaName, tab.table_name;
                    EXECUTE format(\'ANALYZE %I.%I\', schemaName, tab.table_name); 
                end loop;
            end
        $$;';

        job_log($cmd, __FILE__, __LINE__);
        pg_query($ID,$cmd);

        $cmd = "SELECT drop_table(relname)
                FROM pg_stat_user_tables
                WHERE n_live_tup = 0 AND schemaname='temporary_tables';";
        $result = pg_query($ID,$cmd);

    }
}
?>
