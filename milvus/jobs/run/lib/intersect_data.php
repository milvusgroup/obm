<?php
class intersect_data extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('intersect_data initialized', __FILE__, __LINE__);
        return true;
    }

    public function get_results() {
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        $cmdpart = [];
        $tblHeader = ['total rows'];
        foreach ($params as $col => $opts) {
            $cmdpart[] = "count(CASE WHEN $col IS NOT NULL THEN 1 END) AS $col";
            $tblHeader[] = $col;
        }
        $cmd = sprintf("SELECT count(*) AS total, %s FROM %s_qgrids;", implode(', ', $cmdpart), PROJECTTABLE);

        if (!$res = query($ID,$cmd))
            return 'ERROR: query error';


        $results = pg_fetch_assoc($res[0]);

        $t = [$results['total']];
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader($tblHeader);

        foreach ($params as $col => $opts) {
            $t[] = $results[$col]. ' ('. round($results[$col] * 100 / $t[0],2) .'%)';
        }

        $tbl->addRows($t);

        return $tbl->printOut();
    }


    static function run() {
        global $ID;
        $start = microtime(true);
        $params = parent::getJobParams(__CLASS__);
        $cmd = [];
        $limit = round(1000 / count((array)$params));
        foreach ($params as $col => $opts) {
            $filter = (isset($opts->filterColumn) && isset($opts->filterValue))
                ? "g.{$opts->filterColumn} = '{$opts->filterValue}'"
                : "1 = 1";

            $transform = (isset($opts->srid))
                ? sprintf("ST_Transform(rqg.centroid,%s)", $opts->srid)
                : "rqg.centroid";

            $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.%2$s FROM %9$s_qgrids rqg LEFT JOIN %3$s.%4$s g ON ST_Intersects(%6$s,%7$s) WHERE rqg.%1$s IS NULL AND %5$s ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = rows.%2$s FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;',
                $col,
                $opts->valueColumn,
                $opts->schema,
                $opts->table,
                $filter,
                $transform,
                $opts->geomColumn,
                $limit,
                PROJECTTABLE
            );
            if ($opts->valueColumn !== 'geometry') {
                $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id FROM %9$s_qgrids rqg WHERE %1$s IS NULL AND NOT EXISTS (SELECT 1 FROM %3$s.%4$s g WHERE ST_Intersects(%6$s,%7$s) AND %5$s) ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = \'NA\' FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;',
                    $col,
                    $opts->valueColumn,
                    $opts->schema,
                    $opts->table,
                    $filter,
                    $transform,
                    $opts->geomColumn,
                    $limit,
                    PROJECTTABLE
                );
            }
        }
        query($ID, $cmd);
        $duration = round(microtime(true) - $start, 2);
        $duration = ($duration >= 60) ? floor($duration / 60) . " perc " . ($duration % 60) . " mp" : "$duration mp";
        job_log(__CLASS__. ": $duration");
            
        $start = microtime(true);
        $cmd = sprintf("VACUUM %s_qgrids;", PROJECTTABLE);
        pg_query($ID, $cmd);
        $duration = round(microtime(true) - $start, 2);
        job_log(__CLASS__ . ": Vacuum: $duration mp");
        
    }
}
?>
