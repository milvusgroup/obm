<?php 
$path = $argv[1];
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'/local_vars.php.inc');
require_once($path.'/includes/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");

require_once($path.'/includes/common_pg_funcs.php');
require_once($path.'/includes/postgres_functions.php');
require_once($path.'/includes/default_modules.php');
require_once($path.'/includes/admin_pages/jobs.php');
require_once($path.'/jobs/run/lib/valid_list_values.php');

if (!class_exists('valid_list_values')) {
    job_log("class valid_list_values does not exists\n");
    exit();
}
if (!method_exists('valid_list_values','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
call_user_func(['valid_list_values','run']);

?>
