<?php
$path = $argv[1];

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'/local_vars.php.inc');
require_once($path.'/includes/job_functions.php');

putenv("OB_LIB_DIR={$path}includes/");


if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once( $path . 'local/includes/modules/ebp.php');
require_once( $path . 'local/includes/modules/ebp/ebp_classes.php');

$sp = ebp_data::standardProvision(['mode' => 'S']);

echo "\n";
?>
