CREATE TRIGGER milvus_projects_rules BEFORE INSERT OR UPDATE OR DELETE ON public.milvus_projects FOR EACH ROW EXECUTE PROCEDURE public.milvus_general_rules_f('projects');
