CREATE TRIGGER "milvus_program_refresh_mvs" AFTER INSERT OR UPDATE OR DELETE ON "milvus_project_programs" FOR EACH STATEMENT EXECUTE PROCEDURE milvus_refresh_materialized_views();
CREATE TRIGGER "milvus_gc_refresh_mvs" AFTER INSERT OR UPDATE OR DELETE ON "milvus_grouping_codes" FOR EACH STATEMENT EXECUTE PROCEDURE milvus_refresh_materialized_views();
CREATE TRIGGER "milvus_su_refresh_mvs" AFTER INSERT OR UPDATE OR DELETE ON "milvus_sampling_units" FOR EACH STATEMENT EXECUTE PROCEDURE milvus_refresh_materialized_views();
-- https://redmine.milvus.ro/issues/1542
CREATE TRIGGER "milvus_refresh_list_mvs" AFTER INSERT OR UPDATE OR DELETE ON "openbirdmaps"."list_validation" FOR EACH STATEMENT EXECUTE PROCEDURE milvus_refresh_list_matviews();

