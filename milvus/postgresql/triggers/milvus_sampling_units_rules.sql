CREATE TRIGGER milvus_sampling_units_rules BEFORE INSERT OR UPDATE OR DELETE ON public.milvus_sampling_units FOR EACH ROW EXECUTE PROCEDURE public.milvus_general_rules_f('sampling_units');
