CREATE TRIGGER milvus_fieldwork_rules BEFORE INSERT OR UPDATE OR DELETE ON public.milvus_fieldwork FOR EACH ROW EXECUTE PROCEDURE public.milvus_general_rules_f('fieldwork');
