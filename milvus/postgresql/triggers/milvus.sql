CREATE TRIGGER file_connection AFTER INSERT ON public.milvus FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();
CREATE TRIGGER history_update AFTER DELETE OR UPDATE ON public.milvus FOR EACH ROW EXECUTE PROCEDURE history_milvus();
CREATE TRIGGER milvus_qgrid_ai AFTER INSERT OR UPDATE ON public.milvus FOR EACH ROW EXECUTE PROCEDURE milvus_update_qgrids('milvus');
CREATE TRIGGER milvus_rules BEFORE INSERT OR DELETE OR UPDATE ON public.milvus FOR EACH ROW EXECUTE PROCEDURE milvus_rules_f();
CREATE TRIGGER rules_milvus AFTER INSERT OR DELETE OR UPDATE ON public.milvus FOR EACH ROW EXECUTE PROCEDURE rules_milvus();
CREATE TRIGGER taxon_update_milvus_b BEFORE INSERT ON public.milvus FOR EACH ROW EXECUTE PROCEDURE update_milvus_taxonlist();
CREATE TRIGGER taxon_update_milvus_a AFTER UPDATE OR DELETE ON public.milvus FOR EACH ROW EXECUTE PROCEDURE update_milvus_taxonlist();

