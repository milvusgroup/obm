CREATE TRIGGER milvus_pairs_rules BEFORE INSERT OR UPDATE OR DELETE ON public.milvus_pairs FOR EACH ROW EXECUTE PROCEDURE public.milvus_general_rules_f();
