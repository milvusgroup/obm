CREATE TRIGGER milvus_grouping_codes_rules BEFORE INSERT OR UPDATE OR DELETE ON public.milvus_grouping_codes FOR EACH ROW EXECUTE PROCEDURE public.milvus_general_rules_f('grouping_codes');
