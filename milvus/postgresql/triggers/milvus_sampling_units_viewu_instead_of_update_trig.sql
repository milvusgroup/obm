CREATE TRIGGER milvus_sampling_units_viewu_instead_of_update_trig
INSTEAD OF UPDATE ON milvus_sampling_units_view
FOR EACH ROW EXECUTE PROCEDURE milvus_sampling_units_view_update_func();
