CREATE TRIGGER "milvus_taxon_meta_fill" BEFORE INSERT OR UPDATE ON "milvus_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "milvus_search_meta_fill" BEFORE INSERT OR UPDATE ON "milvus_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
