CREATE TRIGGER observations_milvus BEFORE INSERT OR UPDATE ON system.uploadings FOR EACH ROW EXECUTE PROCEDURE system.observations_milvus();
