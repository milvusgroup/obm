CREATE TRIGGER "milvus_qgrids_trigger" BEFORE DELETE OR INSERT OR UPDATE ON milvus.milvus FOR EACH ROW EXECUTE PROCEDURE public.milvus_update_qgrids();
CREATE TRIGGER "milvus_threats_qgrids_trigger" BEFORE DELETE OR INSERT OR UPDATE ON public.milvus_threats FOR EACH ROW EXECUTE PROCEDURE public.milvus_update_qgrids();

