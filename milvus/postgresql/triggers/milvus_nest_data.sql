CREATE TRIGGER milvus_nest_data_rules BEFORE INSERT OR UPDATE OR DELETE ON public.milvus_nest_data FOR EACH ROW EXECUTE PROCEDURE public.milvus_general_rules_f('nest_data');
