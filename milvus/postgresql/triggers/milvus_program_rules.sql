CREATE TRIGGER milvus_program_rules BEFORE INSERT or UPDATE OR DELETE ON public.milvus_program FOR EACH ROW EXECUTE PROCEDURE public.milvus_general_rules_f('program');
