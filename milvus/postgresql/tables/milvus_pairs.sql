ALTER TABLE milvus_pairs ADD COLUMN project varchar(128) NULL;
ALTER TABLE milvus_pairs ADD COLUMN program varchar(128) NULL;
ALTER TABLE milvus_pairs ADD COLUMN grouping_code varchar(128) NULL;
ALTER TABLE milvus_pairs ADD COLUMN sampling_unit varchar(128) NULL;
ALTER TABLE milvus_pairs ADD COLUMN species varchar(128) NULL;
ALTER TABLE milvus_pairs ADD COLUMN min integer NULL;
ALTER TABLE milvus_pairs ADD COLUMN max integer NULL;
ALTER TABLE milvus_pairs ADD COLUMN pair_status varchar(32) NULL;
ALTER TABLE milvus_pairs ADD COLUMN observation_ids integer[] NULL;
ALTER TABLE milvus_pairs ADD COLUMN imported_at timestamp with time zone NULL;

INSERT INTO milvus_pairs (project, program, grouping_code, sampling_unit, species, min, max, obm_geometry, pair_status, observation_ids, imported_at)
SELECT project, program, grouping_code, sampling_unit, species, min, max, geometry as obm_geometry, status as pair_status, array_agg(obm_id) as observation_ids, now() as imported_at
FROM openbirdmaps.raptor_pair p 
LEFT JOIN openbirdmaps.raptor_pair_observation po ON po.pair_id = p.id
LEFT JOIN openbirdmaps.raptor_survey_session s ON s.id = p.session_id
LEFT JOIN openbirdmaps.raptor_period pr ON pr.id = s.period_id
WHERE status != 'str_not_pair'
GROUP BY project, program, grouping_code, sampling_unit, species, min, max, geometry, status ;

INSERT INTO milvus_rules (data_table, row_id, read, write, sensitivity)
SELECT 'milvus_pairs', obm_id, '{84, 322}', '{84}', 3 FROM milvus_pairs;