CREATE TABLE openbirdmaps.raptor_pair (
    id SERIAL,
    session_id integer NOT NULL,
    sampling_unit varchar NOT NULL,
    species varchar NOT NULL,
    min integer NOT NULL,
    max integer NOT NULL,
    status varchar NOT NULL,
    geometry geometry NULL,
    CONSTRAINT fk_session FOREIGN KEY(session_id) REFERENCES openbirdmaps.raptor_survey_session(id)
);

ALTER TABLE ONLY openbirdmaps.raptor_pair
    ADD CONSTRAINT raptor_pair_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON openbirdmaps.raptor_pair TO milvus_admin;
GRANT ALL on SEQUENCE openbirdmaps.raptor_pair_id_seq to milvus_admin ;