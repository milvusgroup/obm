CREATE TABLE openbirdmaps.raptor_pair_observation (
    pair_id integer NOT NULL,
    obm_id integer NOT NULL,
    CONSTRAINT fk_pair FOREIGN KEY(pair_id) REFERENCES openbirdmaps.raptor_pair(id) ON DELETE CASCADE,
    CONSTRAINT fk_observation FOREIGN KEY(obm_id) REFERENCES public.milvus(obm_id) ON DELETE CASCADE
);

GRANT ALL ON openbirdmaps.raptor_pair_observation TO milvus_admin;