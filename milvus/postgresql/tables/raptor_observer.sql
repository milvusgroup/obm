CREATE TABLE openbirdmaps.raptor_observer (
    id SERIAL,
    session_id integer NOT NULL,
    sampling_unit varchar NULL,
    observer integer NOT NULL,
    CONSTRAINT fk_session FOREIGN KEY(session_id) REFERENCES openbirdmaps.raptor_survey_session(id)
);
ALTER TABLE ONLY openbirdmaps.raptor_observer
    ADD CONSTRAINT raptor_observer_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON openbirdmaps.raptor_observer TO milvus_admin;
GRANT ALL on SEQUENCE openbirdmaps.raptor_observer_id_seq to milvus_admin;

