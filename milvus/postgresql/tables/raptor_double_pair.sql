CREATE TABLE openbirdmaps.raptor_double_pair (
    double_id integer NOT NULL,
    pair_id integer NOT NULL,
    CONSTRAINT fk_double FOREIGN KEY(double_id) REFERENCES openbirdmaps.raptor_double(id) ON DELETE CASCADE,
    CONSTRAINT fk_pair FOREIGN KEY(pair_id) REFERENCES openbirdmaps.raptor_pair(id) ON DELETE CASCADE
);

GRANT ALL ON openbirdmaps.raptor_double_pair TO milvus_admin;