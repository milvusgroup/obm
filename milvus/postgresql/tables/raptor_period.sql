CREATE TABLE openbirdmaps.raptor_period (
    id SERIAL,
    period_name varchar NOT NULL,
    project varchar NOT NULL,
    program varchar NOT NULL,
    date_from date NOT NULL,
    date_until date NOT NULL
);
ALTER TABLE ONLY openbirdmaps.raptor_period
    ADD CONSTRAINT raptor_period_id_pkey PRIMARY KEY (id);

GRANT ALL ON openbirdmaps.raptor_period TO milvus_admin;
GRANT ALL on SEQUENCE openbirdmaps.raptor_period_id_seq to milvus_admin ;

ALTER TABLE openbirdmaps.raptor_period ADD COLUMN species_list varchar[];
