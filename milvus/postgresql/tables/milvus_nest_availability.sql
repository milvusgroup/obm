CREATE TABLE milvus.milvus_nest_availability (
    row_id integer NOT NULL,
    start_date date,
    end_date date,
    actual boolean DEFAULT TRUE
);
CREATE UNIQUE INDEX milvus_nest_availability_unique_actual_per_id ON milvus.milvus_nest_availability (row_id) WHERE actual = TRUE;

ALTER TABLE milvus.milvus_nest_availability OWNER TO milvus_admin;

INSERT INTO milvus.milvus_nest_availability (row_id, start_date, end_date) SELECT obm_id, first_date, destroyed_date FROM milvus.milvus_nest_data;

CREATE OR REPLACE FUNCTION milvus.milvus_nest_availability_set_actual () RETURNS TRIGGER AS 
$$
BEGIN
    IF tg_op = 'INSERT' THEN
        UPDATE milvus.milvus_nest_availability SET actual = FALSE WHERE row_id = new.row_id AND actual = TRUE;
        RETURN NEW;
    END IF;
END;
$$ 
LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;

CREATE TRIGGER milvus_nest_availability_set_actual_tr BEFORE INSERT ON milvus.milvus_nest_availability FOR EACH ROW EXECUTE PROCEDURE milvus.milvus_nest_availability_set_actual();