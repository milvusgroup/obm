WITH match as (
    SELECT species_id,
        latin as ebp_latin,
        taxon_id as m_taxon_id
    FROM milvus_taxon
        INNER JOIN ebp.ebp_taxonomy ON case
            WHEN word in ('Test', 'waterbirds', 'null', 'str_other_value_specify_in_notes', 'Fészek nélküli állvány') THEN species_id = 0
            WHEN word = 'Motacilla flava superciliaris' THEN species_id = 103822349
            when word like '%/%' then species_id = 9004
            when word like '%spec.' then species_id = 9004
            when word like '% x %' then species_id = 9003
            ELSE word = latin OR word = english
        end
    WHERE latin is NOT NULL
)
INSERT INTO milvus_taxon (word, taxon_id, status, lang)
SELECT distinct species_id,
    taxon_id, 'accepted', 'ebp_species_id'
FROM milvus_taxon
    LEFT JOIN match ON m_taxon_id = taxon_id
WHERE
species_id != 0 and 
species_id is not null;

--CREATE TABLE ebp.ebp_taxonomy (
--    species_id BIGINT PRIMARY KEY,
--    latin VARCHAR NOT NULL,
--    subspecies_id INTEGER,
--    subspecies VARCHAR,
--    english VARCHAR,
--    paleartic BOOLEAN NOT NULL
--);