CREATE TABLE openbirdmaps.raptor_survey_session (
    id SERIAL,
    period_id integer NOT NULL,
    grouping_code varchar NOT NULL,
    responsible integer NULL,
    finished boolean NOT NULL DEFAULT false,
    cmnt text NULL,
    CONSTRAINT fk_period FOREIGN KEY(period_id) REFERENCES openbirdmaps.raptor_period(id)
);
ALTER TABLE ONLY openbirdmaps.raptor_survey_session
    ADD CONSTRAINT raptor_survey_session_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON openbirdmaps.raptor_survey_session TO milvus_admin;
GRANT ALL on SEQUENCE openbirdmaps.raptor_survey_session_id_seq to milvus_admin ; 