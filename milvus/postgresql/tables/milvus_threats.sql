ALTER TABLE milvus_threats ADD COLUMN "obm_access" integer;
ALTER TABLE milvus_threats ADD COLUMN "date" date;
ALTER TABLE milvus_threats ADD COLUMN "site" character varying;
ALTER TABLE milvus_threats ADD COLUMN "observers" character varying;
ALTER TABLE milvus_threats ADD COLUMN "x" numeric;
ALTER TABLE milvus_threats ADD COLUMN "y" numeric;
ALTER TABLE milvus_threats ADD COLUMN "impact_type" character varying;
ALTER TABLE milvus_threats ADD COLUMN "impact_l1" character varying;
ALTER TABLE milvus_threats ADD COLUMN "impact_l2" character varying;
ALTER TABLE milvus_threats ADD COLUMN "impact_l3" character varying;
ALTER TABLE milvus_threats ADD COLUMN "impact_l4" character varying;
ALTER TABLE milvus_threats ADD COLUMN "impact_custom" character varying;
ALTER TABLE milvus_threats ADD COLUMN "impact_intensity" character varying;
ALTER TABLE milvus_threats ADD COLUMN "time" time without time zone;
ALTER TABLE milvus_threats ADD COLUMN "observations" character varying;
ALTER TABLE milvus_threats ADD COLUMN "threatened_species_group" character varying;
ALTER TABLE milvus_threats ADD COLUMN "management_measure" character varying;


-- add missing qgrids data from milvus table
INSERT INTO milvus_qgrids (row_id, data_table, original) SELECT obm_id, 'milvus_threats', obm_geometry FROM public.milvus_threats;

UPDATE milvus_qgrids SET etrs10_geom = st_transform(foo.geometry,4326), etrs10_name = foo.name
FROM (
       SELECT d.obm_id,k.geometry,k.name FROM milvus_threats d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,st_transform(k.geometry,4326)))
) as foo
WHERE row_id=foo.obm_id AND data_table = 'milvus_threats';


-- add missing qgrids data from openherpmaps table
INSERT INTO openherpmaps_qgrids (row_id, data_table, original) SELECT obm_id, 'openherpmaps_threats', obm_geometry FROM public.openherpmaps_threats;

UPDATE openherpmaps_qgrids SET etrs10_geom = st_transform(foo.geometry,4326), etrs10_name = foo.name
FROM (
       SELECT d.obm_id,k.geometry,k.name FROM openherpmaps_threats d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,st_transform(k.geometry,4326)))
) as foo
WHERE row_id=foo.obm_id AND data_table = 'openherpmaps_threats';

UPDATE openherpmaps_qgrids SET centroid = ST_Centroid(original), judet = NULL, comuna = NULL, sci = NULL, spa = NULL, altit = NULL, corine_code = NULL, country = NULL, npa = NULL, unitate_de_relief = NULL WHERE data_table = 'openherpmaps_threats';