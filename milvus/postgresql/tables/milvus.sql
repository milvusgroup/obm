-- ALTER TABLE milvus ADD COLUMN pair_id integer NULL;
-- ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN pair_id integer NULL;
-- ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN pair_id integer NULL;

-- ALTER TABLE milvus ADD COLUMN species_valid character varying (64) NULL;
-- ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN species_valid character varying (64) NULL;
-- ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN species_valid character varying (64) NULL;

-- ALTER TABLE milvus ALTER COLUMN habitat_id TYPE character varying(255);
-- ALTER TABLE temporary_tables.milvus_obm_upl ALTER COLUMN habitat_id TYPE character varying(255);
-- ALTER TABLE temporary_tables.milvus_obm_obsl ALTER COLUMN habitat_id TYPE character varying(255);

-- ALTER TABLE milvus ADD COLUMN obm_access smallint NOT NULL DEFAULT 0;
-- ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN obm_access smallint NOT NULL DEFAULT 0;
-- ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN obm_access smallint NOT NULL DEFAULT 0;

ALTER TABLE milvus ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE milvus ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE milvus ADD COLUMN species_en character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN species_en character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN species_en character varying (64) NULL;
ALTER TABLE milvus ADD COLUMN species_euring character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN species_euring character varying (64) NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN species_euring character varying (64) NULL;


ALTER TABLE milvus ALTER COLUMN response_type TYPE varchar (128);
ALTER TABLE temporary_tables.milvus_obm_obsl ALTER COLUMN response_type TYPE varchar (128);
ALTER TABLE temporary_tables.milvus_obm_upl ALTER COLUMN response_type TYPE varchar (128);

ALTER TABLE milvus ALTER COLUMN comments_general TYPE text;
ALTER TABLE temporary_tables.milvus_obm_obsl ALTER COLUMN comments_general TYPE text;
ALTER TABLE temporary_tables.milvus_obm_upl ALTER COLUMN comments_general TYPE text;
ALTER TABLE milvus ALTER COLUMN comments_observation TYPE text;
ALTER TABLE temporary_tables.milvus_obm_obsl ALTER COLUMN comments_observation TYPE text;
ALTER TABLE temporary_tables.milvus_obm_upl ALTER COLUMN comments_observation TYPE text;

ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN uncertain_identification boolean NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN uncertain_identification boolean NULL;
ALTER TABLE temporary_tables.milvus_failed_uploads ADD COLUMN uncertain_identification boolean NULL;

ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN colony_code character varying (32) NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN end_time time without time zone NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN start_time time without time zone NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN obs_duration time without time zone NULL;

ALTER TABLE milvus ALTER COLUMN precipitation TYPE varchar (255);
ALTER TABLE temporary_tables.milvus_obm_obsl ALTER COLUMN precipitation TYPE varchar (255);
ALTER TABLE temporary_tables.milvus_obm_upl ALTER COLUMN precipitation TYPE varchar (255);

ALTER TABLE temporary_tables.milvus_obm_obsl ADD COLUMN taxon_id integer NULL;
ALTER TABLE temporary_tables.milvus_obm_upl ADD COLUMN taxon_id integer NULL;
ALTER TABLE temporary_tables.milvus_failed_uploads ADD COLUMN taxon_id integer NULL;