BEGIN;
    ALTER TABLE milvus_project_programs ADD COLUMN project_id integer NOT NULL;
    ALTER TABLE milvus_project_programs ADD COLUMN program_id integer NOT NULL;
    ALTER TABLE "milvus_project_programs" ADD CONSTRAINT "milvus_project_programs_project_id_program_id" UNIQUE ("project_id", "program_id");
COMMIT;
