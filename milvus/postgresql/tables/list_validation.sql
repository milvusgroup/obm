-- https://redmine.milvus.ro/issues/1542
CREATE TABLE "list_validation" (
  "id" serial NOT NULL,
  "observation_list_id" character varying NOT NULL,
  "validation" character varying NOT NULL
);
