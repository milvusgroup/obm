-- BEGIN;
--     ALTER TABLE milvus_projects ADD COLUMN name character varying NOT NULL;
--     ALTER TABLE "milvus_projects" ADD CONSTRAINT "milvus_projects_name" UNIQUE ("name");
-- COMMIT;
BEGIN;
    ALTER TABLE milvus_projects ADD COLUMN obm_access integer NOT NULL DEFAULT 0;
    ALTER TABLE milvus_projects ADD COLUMN active boolean NOT NULL DEFAULT TRUE;
COMMIT;
