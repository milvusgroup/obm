CREATE TABLE openbirdmaps.raptor_double (
    id SERIAL,
    session_id integer NOT NULL,
    min integer NOT NULL,
    max integer NOT NULL,
    status varchar NOT NULL,
    CONSTRAINT fk_session FOREIGN KEY(session_id) REFERENCES openbirdmaps.raptor_survey_session(id)
);

ALTER TABLE ONLY openbirdmaps.raptor_double
    ADD CONSTRAINT raptor_double_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON openbirdmaps.raptor_double TO milvus_admin;
GRANT ALL on SEQUENCE openbirdmaps.raptor_double_id_seq to milvus_admin;