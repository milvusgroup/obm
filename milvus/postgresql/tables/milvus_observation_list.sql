CREATE TABLE public.milvus_observation_list (
    id integer DEFAULT nextval('public.milvus_observation_id_seq'::regclass) NOT NULL,
    oidl character varying(40),
    uploading_id integer,
    obsstart timestamp without time zone,
    obsend timestamp without time zone,
    nulllist boolean,
    started_at timestamp without time zone,
    finished_at timestamp without time zone,
    obm_observation_list_id integer
);

ALTER TABLE ONLY public.milvus_observation_list
    ADD CONSTRAINT milvus_observation_pkey PRIMARY KEY (id);
