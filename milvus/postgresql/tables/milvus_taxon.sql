-- ALTER TABLE milvus_taxon ADD COLUMN wid SERIAL;
ALTER TABLE "milvus_taxon"
ADD CONSTRAINT "milvus_taxon_word_lang" UNIQUE ("word", "lang"),
DROP CONSTRAINT "milvus_taxon_metaword_key";

