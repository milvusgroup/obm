EXPLAIN ANALYZE
SELECT  
    obm_id,
    qgrids."centroid" AS obm_geometry,
    'admin' AS rules, 
    CASE WHEN GeometryType(obm_geometry) = 'POINT' THEN 0 ELSE 1 END as geomtype,
    CASE WHEN write && ARRAY[31,1302,1758,1260,336,1700,1782,1403,1368,1930,1812,322,1469,1379,1265,1775,1684] THEN 1 ELSE 0 END AS writable,
    1 AS selected
FROM milvus
LEFT JOIN milvus_taxon ON species_hu=word AND lang = 'species_hu'  
LEFT JOIN milvus_taxonmeta txm ON milvus_taxon.taxon_id = txm.taxon_id
LEFT JOIN system.milvus_linnaeus linn ON (linn.row_id = obm_id)
LEFT JOIN milvus_qgrids qgrids ON (obm_id = qgrids.row_id)
LEFT JOIN milvus.milvus_rules data_rules ON (data_rules.data_table='milvus' AND obm_id=data_rules.row_id)
WHERE milvus_taxon.taxon_id IN (133);


EXPLAIN ANALYZE
SELECT  obm_id,qgrids."centroid" AS obm_geometry,'admin' AS rules,
CASE WHEN write && ARRAY[31,1302,1758,1260,336,1700,1782,1403,1368,1930,1812,322,1469,1379,1265,1775,1684] THEN 1 ELSE 0 END AS writable
,1 AS selected
FROM milvus
LEFT JOIN milvus_taxon ON species_hu=word AND lang = 'species_hu'  LEFT JOIN milvus_taxonmeta txm ON milvus_taxon.taxon_id = txm.taxon_id
LEFT JOIN system.milvus_linnaeus linn ON (linn.row_id = obm_id)
LEFT JOIN milvus_qgrids qgrids ON (obm_id = qgrids.row_id)
LEFT JOIN milvus_rules data_rules ON (data_rules.data_table='milvus' AND obm_id=data_rules.row_id)
WHERE milvus_taxon.taxon_id IN (133)


EXPLAIN ANALYZE
SELECT  obm_id,qgrids."centroid" AS obm_geometry,'admin' AS rules,
CASE WHEN write && ARRAY[31,1302,1758,1260,336,1700,1782,1403,1368,1930,1812,322,1469,1379,1265,1775,1684] THEN 1 ELSE 0 END AS writable
,1 AS selected
FROM milvus
LEFT JOIN milvus_taxon ON species_hu=word AND lang = 'species_hu'  LEFT JOIN milvus_taxonmeta txm ON milvus_taxon.taxon_id = txm.taxon_id
LEFT JOIN system.milvus_linnaeus linn ON (linn.row_id = obm_id)
LEFT JOIN milvus_qgrids qgrids ON (obm_id = qgrids.row_id)
LEFT JOIN milvus_rules data_rules ON (data_rules.data_table='milvus' AND obm_id=data_rules.row_id)
WHERE milvus_taxon.taxon_id IN (133)



EXPLAIN ANALYZE
WITH "centroid" (geom) AS (SELECT ST_MakeEnvelope(19.4895346773549,44.0895048473317,29.5953168172793,48.0004333439715,4326)) SELECT  obm_id,qgrids."centroid" AS obm_geometry,'admin' AS rules, 
CASE WHEN GeometryType(obm_geometry) = 'POINT' THEN 0 ELSE 1 END as geomtype,
CASE WHEN write && ARRAY[31,1302,1758,1260,336,1700,1782,1403,1368,1930,1812,322,1469,1379,1265,1775,1684] THEN 1 ELSE 0 END AS writable
,1 AS selected
FROM milvus
LEFT JOIN milvus_taxon ON species_hu=word AND lang = 'species_hu'  LEFT JOIN milvus_taxonmeta txm ON milvus_taxon.taxon_id = txm.taxon_id
LEFT JOIN system.milvus_linnaeus linn ON (linn.row_id = obm_id)
LEFT JOIN milvus_qgrids qgrids ON (obm_id = qgrids.row_id)
LEFT JOIN milvus_rules data_rules ON (data_rules.data_table='milvus' AND obm_id=data_rules.row_id)
WHERE GeometryType(qgrids."centroid") IN ('POINT')   AND milvus_taxon.taxon_id IN (133)


DROP VIEW milvus_rules;
ALTER 

EXPLAIN ANALYZE
SELECT 
    milvus.obm_id,
    ST_AsText(qgrids."centroid") as obm_geometry,
    uploadings.uploading_date,
    uploadings.uploader_name,
    uploadings.form_id as uformid,
    uploadings.uploader_id,
    milvus.obm_validation as data_eval,
    uploadings.id as uploading_id,
    milvus.obm_modifier_id,
    ARRAY_TO_STRING(data_rules.read,',') as obm_read,
    ARRAY_TO_STRING(data_rules.write,',') as obm_write,
    data_rules.sensitivity as obm_sensitivity,
    milvus.list_id,
    milvus.obm_observation_list_id,
    milvus.method,
    milvus.method_details_species_aimed,
    milvus.date,
    milvus.date_until,
    milvus.time_of_start,
    milvus.time_of_end,
    milvus.duration,
    milvus.comlete_list,
    milvus.precision_of_count_list,
    milvus.observers,
    milvus.projekt,
    milvus.national_program,
    milvus.national_grouping_code,
    milvus.national_sampling_unit,
    milvus.local_program,
    milvus.local_grouping_code,
    milvus.local_sampling_unit,
    milvus.colony_code,
    milvus.surveyed_geometry,
    milvus.samplin_unit_coverage,
    milvus.disturbance,
    milvus.snow_depth,
    milvus.wind_force,
    milvus.wind_speed_kmh,
    milvus.wind_direction,
    milvus.cloud_cover,
    milvus.precipitation,
    milvus.temperature,
    milvus.visibility,
    milvus.atmospheric_pressure,
    milvus.ice_cover,
    milvus.habitat_state,
    milvus.optics_used,
    milvus.colony_type,
    milvus.colony_place,
    milvus.colony_survey_method,
    milvus.comments_general,
    milvus.nest_id,
    milvus.x,
    milvus.y,
    milvus.altitude,
    milvus.exact_time,
    milvus.secondary_time,
    milvus.observed_unit,
    milvus.species_valid,
    milvus.species,
    milvus.species_ro,
    milvus.species_hu,
    milvus.species_en,
    milvus.species_euring,
    milvus.uncertain_identification,
    milvus.active_abandoned,
    milvus.number,
    milvus.number_max,
    milvus.gender,
    milvus.age,
    milvus.count_precision,
    milvus.status_of_individuals,
    milvus.cause_of_death,
    milvus.nest_status,
    milvus.nest_place,
    milvus.nest_type,
    milvus.nest_condition,
    milvus.distance_m,
    milvus.distance_code,
    milvus.habitat_id,
    milvus.habitat_2,
    milvus.map_code,
    milvus.timing_of_reaction,
    milvus.response_type,
    milvus.in_flight_sitting,
    milvus.flight_direction,
    milvus.flight_altitude,
    milvus.migration_intensity,
    milvus.migration_type,
    milvus.number_of_eggs,
    milvus.number_of_nestlings,
    milvus.nestling_age,
    milvus.nr_dead_egg,
    milvus.nr_dead_nestling,
    milvus.type_place_roost_colony,
    milvus.type_number_trees,
    milvus.threats,
    milvus.obm_files_id,
    milvus.comments_observation,
    milvus.data_owner,
    milvus.access_to_data,
    milvus.non_human_observer,
    milvus.source,
    milvus.source_id,
    qgrids.country,
    qgrids.judet,
    qgrids.comuna,
    qgrids.etrs10_name,
    qgrids.spa,
    qgrids.sci,
    fj.filename 
FROM milvus 
LEFT JOIN system.uploadings ON ("milvus".obm_uploading_id=uploadings.id) 
LEFT JOIN milvus_qgrids qgrids ON (milvus.obm_id = qgrids.row_id)
LEFT JOIN milvus_rules data_rules ON (data_rules.data_table='milvus' AND milvus.obm_id=data_rules.row_id) 
LEFT JOIN ( 
    SELECT string_agg(reference, ', ') as filename, conid 
    FROM system.file_connect fc 
    LEFT JOIN system.files f ON fc.file_id = f.id 
    WHERE f.project_table = 'milvus' AND f.data_table = 'milvus' GROUP BY conid
) as fj ON obm_files_id = fj.conid 
WHERE "milvus".obm_id IN (
    SELECT obm_id FROM temporary_tables.temp_milvus_a8b34ddbc91ff6a82e71e10e7f3e31d3
    );
