CREATE OR REPLACE FUNCTION process_left_behind_obslist_parts(obs varchar, dt date)
RETURNS void
LANGUAGE plpgsql
as
$$
DECLARE
    lid varchar;
    mn integer;
    uplcnt integer;
    list_record integer;
BEGIN
    SELECT count(*) INTO mn FROM temporary_tables.milvus_obm_obsl WHERE observers = obs AND date = dt;
    RAISE NOTICE '%', 'lemaradt adatok száma: ' || mn;
    SELECT DISTINCT metadata->>'observation_list_id' INTO lid FROM system.uploadings WHERE id IN (SELECT obm_uploading_id FROM temporary_tables.milvus_obm_obsl WHERE observers = obs AND date = dt);
    SELECT count(*) INTO uplcnt FROM milvus WHERE obm_observation_list_id = lid;
    RAISE NOTICE '%', 'feltöltött adatok száma: ' || uplcnt;
    -- SELECT uploading_date, uploader_id, uploader_name, description, access, "group", project, project_table, form_id, metadata->>'observation_list_start', metadata->>'observation_list_end', metadata->>'id', metadata->>'app_version', metadata->>'measurements_num' FROM system.uploadings WHERE metadata::jsonb ? 'measurements_num' AND metadata->>'observation_list_id' = lid INTO list_record;
    SELECT count(*) INTO list_record FROM system.uploadings WHERE metadata::jsonb ? 'measurements_num' AND metadata->>'observation_list_id' = lid;

    RAISE NOTICE '%', lid;
    RAISE NOTICE '%', list_record;
    
    if (list_record = 1) THEN
        INSERT INTO system.uploadings ( uploading_date, metadata, uploader_id, uploader_name, description, access, "group", project_table, project, form_id) 
        SELECT 
          uploading_date,
          json_build_object('id', metadata->>'id', 'app_version', metadata->>'app_version', 'observation_list_id', metadata->>'observation_list_id', 'observation_list_start', metadata->>'observation_list_start', 'observation_list_end', metadata->>'observation_list_end', 'measurements_num', mn),
          uploader_id,
          uploader_name,
          'API upload',
          0,
          "group",
          'temporary_tables.milvus_obm_obsl',
          'milvus',
          form_id
        FROM system.uploadings WHERE metadata::jsonb ? 'measurements_num' AND metadata->>'observation_list_id' = lid ;
    ELSIF (list_record = 0) THEN
        WITH uplinf as (
            SELECT min(uploading_date) as uploading_date, metadata->>'app_version' as app_version, metadata->>'observation_list_id' as observation_list_id, min(metadata->>'started_at') as observation_list_start, max(metadata->>'finished_at') as observation_list_end, count(*) as measurements_num , uploader_id, uploader_name, "group", form_id FROM system.uploadings WHERE metadata->>'observation_list_id' = lid GROUP BY metadata->>'app_version', metadata->>'observation_list_id', uploader_id, uploader_name, "group", form_id
        )
        INSERT INTO system.uploadings ( uploading_date, metadata, uploader_id, uploader_name, description, access, "group", project_table, project, form_id) 
        SELECT 
            uploading_date,
            json_build_object('app_version', app_version, 'observation_list_id', observation_list_id, 'observation_list_start', observation_list_start, 'observation_list_end', observation_list_end, 'measurements_num', measurements_num),
            uploader_id,
            uploader_name,
            'API upload',
            0,
            "group",
            'temporary_tables.milvus_obm_obsl',
            'milvus',
            form_id
        FROM uplinf;
    END IF;
END;
$$


DO $$DECLARE r record;
BEGIN
    FOR r IN SELECT DISTINCT observers, date FROM temporary_tables.milvus_obm_obsl
    LOOP
        EXECUTE 'SELECT process_left_behind_obslist_parts(' || quote_literal(r.observers) || ',' || quote_literal(r.date) || ')';
    END LOOP;
END$$;

INSERT INTO system.uploadings ( uploading_date, metadata, uploader_id, uploader_name, description, access, "group", project_table, project, form_id) 
VALUES 
(
  'UPLDT', 
  '{"id":"M-ID","app_version":"M-AV","observation_list_id":"LID","observation_list_start":LS,"observation_list_end":LE,"measurements_num":MN}',
  UPLRID,
  'ULPRNM',
  'API upload',
  0,
  'GRP',
  'temporary_tables.milvus_obm_obsl',
  'milvus',
  FRMID
);




SELECT DISTINCT observers, date FROM temporary_tables.milvus_obm_obsl;
SELECT count(*) FROM temporary_tables.milvus_obm_obsl WHERE observers = 'Kovács Zsolt' AND date = '2023-04-09';
SELECT DISTINCT metadata->>'observation_list_id' FROM system.uploadings WHERE id IN (SELECT obm_uploading_id FROM temporary_tables.milvus_obm_obsl WHERE observers = 'Kovács Zsolt' AND date = '2023-04-09');
SELECT count(*) FROM milvus WHERE obm_observation_list_id = 'fb994afb-ee54-481d-ae69-121374af1ab9';
SELECT uploading_date, uploader_id, uploader_name, description, access, "group", project, project_table, form_id, metadata->>'observation_list_start', metadata->>'observation_list_end', metadata->>'id', metadata->>'app_version', metadata->>'measurements_num' FROM system.uploadings WHERE metadata::jsonb ? 'measurements_num' AND metadata->>'observation_list_id' = 'fb994afb-ee54-481d-ae69-121374af1ab9';

INSERT INTO system.uploadings ( uploading_date, metadata, uploader_id, uploader_name, description, access, "group", project_table, project, form_id) 
SELECT 
  uploading_date,
  json_build_object('id', metadata->>'id', 'app_version', metadata->>'app_version', 'observation_list_id', metadata->>'observation_list_id', 'observation_list_start', metadata->>'observation_list_start', 'observation_list_end', metadata->>'observation_list_end', 'measurements_num', 31),
  uploader_id,
  uploader_name,
  'API upload',
  0,
  "group",
  'temporary_tables.milvus_obm_obsl',
  'milvus',
  form_id
FROM system.uploadings WHERE metadata::jsonb ? 'measurements_num' AND metadata->>'observation_list_id' = 'fb994afb-ee54-481d-ae69-121374af1ab9' ;
