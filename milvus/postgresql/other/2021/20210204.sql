UPDATE milvus_taxonmeta SET rank = 'other_taxon' WHERE taxon_id IN (1383, 1378, 1382, 1380, 1375) AND rank IS NULL;
UPDATE milvus_taxonmeta SET rank = 'other_taxon' WHERE rank IS NULL;

SELECT *, count(*) OVER() AS full_count 
FROM milvus 
INNER JOIN temporary_tables.temp_milvus_e6325611bf04fb18bf1bc4dfff956ffd t ON (milvus.obm_id=t.obm_id) 
LEFT JOIN milvus_qgrids as qgrids ON ("milvus".obm_id=qgrids.row_id) 
WHERE ST_DWithin(
    st_transform(milvus.obm_geometry,3857), 
    st_geomfromtext($qyfnJKEVbFHwcDSk$POINT(3120453.046308 5484220.775238)$qyfnJKEVbFHwcDSk$,3857), 
    357.603429699
) 
LIMIT 30;

WITH isvalid as (
    select obm_id, ST_IsValidReason(obm_geometry) miert from milvus where st_isvalid(obm_geometry) = false
)
UPDATE milvus SET obm_geometry = st_makevalid(obm_geometry) FROM isvalid WHERE milvus.obm_id = isvalid.obm_id AND isvalid.miert LIKE 'Self-intersection';

update milvus set obm_geometry = jo.obm_geometry from milvus jo WHERE jo.obm_id = 769087 AND jo.obm_uploading_id = milvus.obm_uploading_id and milvus.obm_uploading_id = 26991;
update milvus set obm_geometry = jo.obm_geometry from milvus jo WHERE jo.obm_id = 949343 AND jo.obm_uploading_id = milvus.obm_uploading_id and milvus.obm_uploading_id = 52477;



select obm_id from milvus where geometrytype(obm_geometry) = 'POINT' AND st_x(obm_geometry) IN (select min(st_x(obm_geometry)) from milvus where geometrytype(obm_geometry) = 'POINT');

SELECT *, count(*) OVER() AS full_count FROM milvus INNER JOIN temporary_tables.temp_milvus_e8a1812d7b427f6bc654953ba5b31061 t ON (milvus.obm_id=t.obm_id) LEFT JOIN milvus_qgrids as qgrids ON ("milvus".obm_id=qgrids.row_id) 
            WHERE ST_DWithin(st_transform(milvus.obm_geometry,3857), st_geomfromtext($VtOnSFihygEILfQT$POINT(2821043.5935651 5797282.9503077)$VtOnSFihygEILfQT$,3857), 65603.664968066) 
            ORDER BY ST_Distance(milvus.obm_geometry,st_transform(st_geomfromtext($VtOnSFihygEILfQT$POINT(2821043.5935651 5797282.9503077)$VtOnSFihygEILfQT$,900913),4326)) LIMIT 30;
            


-- minden népi név minden adatbázisból

CREATE OR REPLACE VIEW shared.combined_taxon_list AS
WITH 
ohm AS (
    SELECT sci.word as latin, ro.word as romana, hu.word as magyar, en.word as english, 'OHM' as project, sci.taxon_id
    FROM openherpmaps_taxon sci
    LEFT JOIN openherpmaps_taxon ro ON ro.taxon_id = sci.taxon_id AND ro.status = 'accepted' AND ro.lang = 'specia_ro'
    LEFT JOIN openherpmaps_taxon hu ON hu.taxon_id = sci.taxon_id AND hu.status = 'accepted' AND hu.lang = 'specia_hu'
    LEFT JOIN openherpmaps_taxon en ON en.taxon_id = sci.taxon_id AND en.status = 'accepted' AND en.lang = 'specia_en'
    WHERE sci.status = 'accepted' AND sci.lang = 'specia_sci'
),
omm AS (
    SELECT sci.word as latin, ro.word as romana, hu.word as magyar, en.word as english, 'OMM' as project, sci.taxon_id
    FROM mammalia_taxon sci
    LEFT JOIN mammalia_taxon ro ON ro.taxon_id = sci.taxon_id AND ro.status = 'accepted' AND ro.lang = 'species_ro'
    LEFT JOIN mammalia_taxon hu ON hu.taxon_id = sci.taxon_id AND hu.status = 'accepted' AND hu.lang = 'species_hu'
    LEFT JOIN mammalia_taxon en ON en.taxon_id = sci.taxon_id AND en.status = 'accepted' AND en.lang = 'species_en'
    WHERE sci.status = 'accepted' AND sci.lang = 'species_sci'
),
obm AS (
    SELECT sci.word as latin, ro.word as romana, hu.word as magyar, en.word as english, 'OBM' as project, sci.taxon_id
    FROM milvus_taxon sci
    LEFT JOIN milvus_taxon ro ON ro.taxon_id = sci.taxon_id AND ro.status = 'accepted' AND ro.lang = 'species_ro'
    LEFT JOIN milvus_taxon hu ON hu.taxon_id = sci.taxon_id AND hu.status = 'accepted' AND hu.lang = 'species_hu'
    LEFT JOIN milvus_taxon en ON en.taxon_id = sci.taxon_id AND en.status = 'accepted' AND en.lang = 'species_en'
    WHERE sci.status = 'accepted' AND sci.lang = 'species_valid'
)
SELECT * FROM obm
UNION
SELECT * FROM ohm
UNION
SELECT * FROM omm
ORDER BY project, latin ;

-- observer_user modul
SELECT u.id, u.username, u.institute, u.email, pr.role_id FROM project_roles pr LEFT JOIN users u ON u.id = pr.user_id WHERE pr.project_table = 'teszt' AND pr.user_id IS NOT NULL;