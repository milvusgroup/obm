SELECT uploader_id, count(*) FROM (
  SELECT DISTINCT uploader_id, uploader_name FROM system.uploadings WHERE project = 'milvus' GROUP BY uploader_id, uploader_name
 ) foo
GROUP BY uploader_id
HAVING count(*) > 1;

--  uploader_id | count
-- -------------+-------
--           65 |     2
--         1263 |     3
--           88 |     2
--           40 |     3
--           74 |     2
--          356 |     2
--           41 |     2

SELECT DISTINCT uploader_name, uploader_id FROM system.uploadings WHERE uploader_id IN (65, 1263, 88, 40, 74, 356, 41) AND project = 'milvus' ORDER BY uploader_id;

SELECT uploader_name FROM system.uploadings WHERE uploader_id = 40 AND project = 'milvus' ORDER BY id DESC LIMIT 1;
--     uploader_name
-- -----------------------
--  Ölvedi Szilárd-Zsolt
-- (1 row)



SELECT uploader_name FROM system.uploadings WHERE uploader_id = 65 AND project = 'milvus' ORDER BY id DESC LIMIT 1;
--     uploader_name
-- -----------------------
--  Toncean Florin-Costin
-- (1 row)



SELECT uploader_name FROM system.uploadings WHERE uploader_id = 1263 AND project = 'milvus' ORDER BY id DESC LIMIT 1;
-- uploader_name
-- ---------------
--  Szabó Csilla
-- (1 row)



SELECT uploader_name FROM system.uploadings WHERE uploader_id = 88 AND project = 'milvus' ORDER BY id DESC LIMIT 1;
--   uploader_name
-- -------------------
--  Belényessy Sándor
-- (1 row)



SELECT uploader_name FROM system.uploadings WHERE uploader_id = 74 AND project = 'milvus' ORDER BY id DESC LIMIT 1;
--     uploader_name
-- -----------------------
--  Baltag Emanuel Stefan
-- (1 row)



SELECT uploader_name FROM system.uploadings WHERE uploader_id = 356 AND project = 'milvus' ORDER BY id DESC LIMIT 1;
--      uploader_name
-- -------------------------
--  Alexandru Cătălin Birău
-- (1 row)



SELECT uploader_name FROM system.uploadings WHERE uploader_id = 41 AND project = 'milvus' ORDER BY id DESC LIMIT 1;
--   uploader_name
-- --------------------
--  András-Attila Nagy
-- (1 row)


BEGIN;
UPDATE system.uploadings SET uploader_name = 'Ölvedi Szilárd-Zsolt' WHERE uploader_id = 40 AND uploader_name != 'Ölvedi Szilárd-Zsolt' AND project = 'milvus';
UPDATE system.uploadings SET uploader_name = 'Toncean Florin-Costin' WHERE uploader_id = 65 AND uploader_name != 'Toncean Florin-Costin' AND project = 'milvus';
UPDATE system.uploadings SET uploader_name = 'Szabó Csilla' WHERE uploader_id = 1263 AND uploader_name != 'Szabó Csilla' AND project = 'milvus';
UPDATE system.uploadings SET uploader_name = 'Belényessy Sándor' WHERE uploader_id = 88 AND uploader_name != 'Belényessy Sándor' AND project = 'milvus';
UPDATE system.uploadings SET uploader_name = 'Baltag Emanuel Stefan' WHERE uploader_id = 74 AND uploader_name != 'Baltag Emanuel Stefan' AND project = 'milvus';
UPDATE system.uploadings SET uploader_name = 'Alexandru Cătălin Birău' WHERE uploader_id = 356 AND uploader_name != 'Alexandru Cătălin Birău' AND project = 'milvus';
UPDATE system.uploadings SET uploader_name = 'András-Attila Nagy' WHERE uploader_id = 41 AND uploader_name != 'András-Attila Nagy' AND project = 'milvus';
ROLLBACK;