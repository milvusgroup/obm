SELECT 'cp ' || replace(replace(replace(reference, ' ', '\ '), '(', '\('), ')', '\)') || ' /tmp/' FROM system.files WHERE project_table = 'milvus' AND reference LIKE '%+%';
SELECT 'mv ' || replace(replace(replace(reference, ' ', '\ '), '(', '\('), ')', '\)') || ' ' || replace(replace(replace(replace(reference, '+', '_'), ' ', '_'), '(', '_'), ')', '_') as corrected FROM system.files WHERE project_table = 'milvus' AND reference LIKE '%+%';

-- SELECT string_agg(coalesce({'_','_'}[array_position(['+',' '], c)],c),'') FROM regexp_split_to_table('asdf+asdf asdf=asdf','') g(c);

-- mégsem cserélek ki mindent, mert nem merem
CREATE OR REPLACE FUNCTION "rename_filename" ("filename" text) RETURNS text LANGUAGE plpgsql AS $$
DECLARE 
    invalid_chars varchar[];
    c varchar(2);
BEGIN
    invalid_chars := ARRAY['+', ' ', '(', ')'];
    FOREACH c IN ARRAY invalid_chars 
    LOOP
        filename = replace(filename, c, '_');
    END LOOP;
    return filename;
END;
$$;

'

SELECT id, reference, rename_filename(reference) as renamed FROM system.files WHERE project_table = 'milvus' AND reference LIKE '%+%';
BEGIN;
UPDATE system.files SET reference = rename_filename(reference) WHERE project_table = 'milvus' AND reference LIKE '%+%';
END;

DROP FUNCTION rename_filename(text);

-- backup
-- 12267 | Track_26 + 29 (III.)_2020-06-20.gpx                       | Track_26___29__III.__2020-06-20.gpx
-- 12800 | track_patrat 033+038_2020_observatii nocturne.gpx         | track_patrat_033_038_2020_observatii_nocturne.gpx
-- 12801 | track_patrat_033+038_2020_verificarea accesibilitatii.gpx | track_patrat_033_038_2020_verificarea_accesibilitatii.gpx
-- 15312 | KL95_04 + LL26_03.gpx                                     | KL95_04___LL26_03.gpx
-- 16571 | Jidvei+Secase-05-06.06.2020.gdb                           | Jidvei_Secase-05-06.06.2020.gdb
-- 16553 | track 84+108 (10.10.2020) zi.gpx                          | track_84_108__10.10.2020__zi.gpx
-- 16554 | track 84+108 (11.10.2020) noapte.gpx                      | track_84_108__11.10.2020__noapte.gpx
-- 16566 | Monitorizare huhurezi 77+106+113.gpx                      | Monitorizare_huhurezi_77_106_113.gpx
