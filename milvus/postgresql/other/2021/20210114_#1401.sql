INSERT INTO milvus (
    obm_uploading_id, obm_validation, obm_comments, obm_modifier_id, date, date_until, observers, method, time_of_start, time_of_end, 
    duration, comlete_list, wind_force, wind_direction, precipitation, snow_depth, visibility, temperature, optics_used, comments_general, 
    species, number, number_max, observed_unit, count_precision, gender, age, habitat_state, obm_geometry, national_program, local_program, 
    colony_type, colony_place, colony_survey_method, disturbance, atmospheric_pressure, altitude, access_to_data, national_grouping_code, 
    national_sampling_unit, ice_cover, cloud_cover, wind_speed_kmh, flight_direction, cause_of_death, nest_type, nest_status, nest_condition, 
    distance_m, distance_code, habitat_id, habitat_2, map_code, timing_of_reaction, response_type, in_flight_sitting, flight_altitude, 
    migration_intensity, migration_type, secondary_time, status_of_individuals, nest_place, comments_observation, list_id, obm_files_id, 
    local_grouping_code, local_sampling_unit, x, y, data_owner, number_of_eggs, number_of_nestlings, method_details_species_aimed, exact_time, 
    precision_of_count_list, surveyed_geometry, source, source_id, non_human_observer, nest_id, samplin_unit_coverage, active_abandoned, 
    type_number_trees, type_place_roost_colony, threats, nestling_age, nr_dead_egg, nr_dead_nestling, projekt, obm_observation_list_id, 
    colony_code, start_time, end_time, obs_duration, pair_id, species_valid, obm_access, species_hu, species_ro, species_en, species_euring
    )
SELECT 
    obm_uploading_id, obm_validation, obm_comments, obm_modifier_id, date, date_until, observers, method, time_of_start, time_of_end, 
    duration, comlete_list, wind_force, wind_direction, precipitation, snow_depth, visibility, temperature, optics_used, comments_general, 
    species, number, number_max, observed_unit, count_precision, gender, age, habitat_state, obm_geometry, national_program, local_program, 
    colony_type, colony_place, colony_survey_method, disturbance, atmospheric_pressure, altitude, access_to_data, national_grouping_code, 
    national_sampling_unit, ice_cover, cloud_cover, wind_speed_kmh, flight_direction, cause_of_death, nest_type, nest_status, nest_condition, 
    distance_m, distance_code, habitat_id, habitat_2, map_code, timing_of_reaction, response_type, in_flight_sitting, flight_altitude, 
    migration_intensity, migration_type, secondary_time, status_of_individuals, nest_place, comments_observation, list_id, obm_files_id, 
    local_grouping_code, local_sampling_unit, x, y, data_owner, number_of_eggs, number_of_nestlings, method_details_species_aimed, exact_time, 
    precision_of_count_list, surveyed_geometry, source, source_id, non_human_observer, nest_id, samplin_unit_coverage, active_abandoned, 
    type_number_trees, type_place_roost_colony, threats, nestling_age, nr_dead_egg, nr_dead_nestling, projekt, obm_observation_list_id, 
    colony_code, start_time, end_time, obs_duration, pair_id, species_valid, obm_access, species_hu, species_ro, species_en, species_euring
FROM temporary_tables.milvus_obm_obsl WHERE observers = 'Komáromi István';

DELETE FROM temporary_tables.milvus_obm_obsl WHERE observers = 'Komáromi István';

BEGIN;
UPDATE system.uploadings SET project_table = 'milvus' WHERE id = 65527;
UPDATE system.uploadings SET project_table = 'milvus' WHERE id = 65528;