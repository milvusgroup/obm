BEGIN;
INSERT INTO translations (scope, project, lang, const, translation) VALUES
('local', 'milvus', 'hu', 'str_ioc_order', 'taxonómiai sorrend'), 
('local', 'milvus', 'en', 'str_ioc_order', 'taxonomical order'), 
('local', 'milvus', 'ro', 'str_ioc_order', 'ordin taxonomic'), 

('local', 'milvus', 'hu', 'str_hybrid', 'hibrid'), 
('local', 'milvus', 'en', 'str_hybrid', 'hybrid'), 
('local', 'milvus', 'ro', 'str_hybrid', 'hibrid'), 

('local', 'milvus', 'hu', 'str_other_taxon', 'egyéb taxon'), 
('local', 'milvus', 'en', 'str_other_taxon', 'other taxon'), 
('local', 'milvus', 'ro', 'str_other_taxon', 'alt taxon'), 

('local', 'milvus', 'hu', 'str_non_taxon', 'nem taxon'), 
('local', 'milvus', 'en', 'str_non_taxon', 'not a taxon'), 
('local', 'milvus', 'ro', 'str_non_taxon', 'nu este taxon'), 

('local', 'milvus', 'hu', 'str_subspecies', 'alfaj'), 
('local', 'milvus', 'en', 'str_subspecies', 'subspecies'), 
('local', 'milvus', 'ro', 'str_subspecies', 'subspecie'), 

('local', 'milvus', 'hu', 'str_waterbirds', 'vízimadarak'), 
('local', 'milvus', 'en', 'str_waterbirds', 'water birds'), 
('local', 'milvus', 'ro', 'str_waterbirds', 'păsări acvatice'), 

('local', 'milvus', 'hu', 'str_raptors', 'ragadozómadarak'), 
('local', 'milvus', 'en', 'str_raptors', 'raptors'), 
('local', 'milvus', 'ro', 'str_raptors', 'păsări răpitoare'), 

('local', 'milvus', 'hu', 'str_owls', 'baglyok'), 
('local', 'milvus', 'en', 'str_owls', 'owls'), 
('local', 'milvus', 'ro', 'str_owls', 'strigiforme');

COMMIT;