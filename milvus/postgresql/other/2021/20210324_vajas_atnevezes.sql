--\copy (SELECT * FROM milvus WHERE national_program = 'str_corfru_national_survey' AND projekt = 'POIM Național Păsări 2020-2021') TO './corfru_backup.csv' CSV HEADER;
BEGIN;

WITH 
su AS (
 SELECT msu.*, geometry
 FROM
     shared.etrs10
 INNER JOIN milvus_sampling_units_mv msu ON milvus_code = sampling_unit
 WHERE program = 'str_corfru_national_survey'
),
suj as (
 SELECT
     obm_id,
     ST_Transform(obm_geometry, 31700) as geom,
     species,
     observers,
     observed_unit,
     status_of_individuals,
     national_grouping_code, 
     colony_code,
     SUBSTR(su.sampling_unit, 1, 2) as grouping_code, 
     sampling_unit || '_' || LPAD((ROW_NUMBER() OVER (PARTITION BY sampling_unit ORDER BY colony_code))::varchar, 2, '0') as new_colony_code,
     national_sampling_unit, 
     su.sampling_unit
 FROM
     milvus m
 LEFT JOIN su ON ST_Intersects (m.obm_geometry, su.geometry)
 WHERE
     national_program = 'str_corfru_national_survey' AND 
     projekt = 'POIM Național Păsări 2020-2021' AND
     species != 'test'
),
fbi as (
 SELECT DISTINCT national_grouping_code, national_sampling_unit
 FROM milvus
 WHERE
     national_program = 'str_corfru_national_survey' AND 
     projekt = 'Farmland Bird Index 2015'
),
judit_varjai as (
 SELECT obm_id FROM suj WHERE species = 'Corvus corone cornix' AND observers LIKE 'Veres-Szászka Judit%'
),
atnevezes_elokeszites as (
 SELECT 
     obm_id,
     grouping_code, 
     sampling_unit, 
     colony_code, 
     new_colony_code
 FROM suj
 LEFT JOIN fbi ON sampling_unit = fbi.national_sampling_unit
 WHERE 
     suj.obm_id NOT IN (SELECT obm_id FROM judit_varjai) AND
     sampling_unit IS NOT NULL AND
     -- suj.colony_code != new_colony_code AND
     fbi.national_sampling_unit IS NULL
)

-- 2. minden adathoz hozzárendelni a gyűjtő- (MILVUS50) és egység-kódokat (MILVUS10)
UPDATE milvus m SET 
    national_sampling_unit = suj.sampling_unit,
    national_grouping_code = suj.grouping_code
FROM suj 
WHERE
    m.obm_id = suj.obm_id AND
    m.national_sampling_unit IS DISTINCT FROM suj.sampling_unit;


-- 1. ami négyzeten kívűl van, azt áttenni alkalmi megfigyelésre
UPDATE milvus m SET 
    method = 'str_occasional_observation',
    national_program = NULL
FROM suj 
WHERE
    m.obm_id = suj.obm_id AND
    suj.sampling_unit IS NULL;


-- 4. C13-as költési kódot minden varjú adathoz
UPDATE milvus m SET 
    status_of_individuals = 'str_ad_incubating_or_on_occupied_nest'
FROM suj 
WHERE
    m.obm_id = suj.obm_id AND
    suj.species != 'null' AND suj.status_of_individuals IS NULL;


-- 3. atnevezés - csak az új négyzetek
UPDATE milvus m SET 
 colony_code = ae.new_colony_code
FROM atnevezes_elokeszites ae
WHERE
 m.obm_id = ae.obm_id;



-- az érintetlen kolónianevek ellenőrzése:
WITH matccs as (
SELECT obm_id, projekt, national_program, national_grouping_code, national_sampling_unit, colony_code, regexp_match(colony_code,'^[A-Z]{2}[0-9]{2}_[0-9]{2,3}$') as starts_with_b
FROM milvus
WHERE
    national_program = 'str_corfru_national_survey' AND 
    projekt = 'POIM Național Păsări 2020-2021' AND
    species != 'test'
),
nem_match as (
    SELECT * FROM matccs WHERE starts_with_b IS NULL
),
su_nem_matccs as (
    SELECT *, national_sampling_unit || right(colony_code, length(colony_code) - 4) as colony_code_corrected FROM matccs WHERE LEFT(colony_code, 4) != national_sampling_unit
)
UPDATE milvus m SET colony_code = UPPER(m.colony_code) FROM nem_match nm WHERE m.obm_id = nm.obm_id AND LENGTH(m.colony_code) = 7;
UPDATE milvus m SET colony_code = 'OF23_103' WHERE m.obm_id = 1194523;
UPDATE milvus m SET colony_code = 'OF32_02' WHERE m.obm_id = 1193947;
UPDATE milvus m SET colony_code = 'FB41_02' WHERE m.obm_id = 1082518;
UPDATE milvus m SET colony_code = LEFT(m.colony_code, 4) || '_0' || RIGHT(m.colony_code,1) FROM nem_match nm WHERE m.obm_id = nm.obm_id;
UPDATE milvus m SET colony_code = colony_code_corrected FROM su_nem_matccs nm WHERE m.obm_id = nm.obm_id;
TABLE su_nem_matccs;

ROLLBACK;


-- távolság mátrix
WITH 
minden as (
    SELECT obm_id, ST_Transform(obm_geometry, 31700) as obm_geometry, projekt, national_program, national_grouping_code, national_sampling_unit, colony_code, species, observers 
    FROM milvus
    WHERE
        national_program = 'str_corfru_national_survey' AND 
        projekt = 'POIM Național Păsări 2020-2021' AND
        species != 'test'
),
judit_varjai as (
    SELECT obm_id FROM minden WHERE species = 'Corvus corone cornix' AND observers LIKE 'Veres-Szászka Judit%'
),
d as (
    SELECT * FROM minden
    WHERE
        obm_id NOT IN (SELECT obm_id FROM judit_varjai)
    ),
distances as (
SELECT d1.colony_code, d2.colony_code, ST_Distance(d1.obm_geometry, d2.obm_geometry) as dist
FROM d as d1 
CROSS JOIN d as d2 
WHERE 
    d1.national_sampling_unit = d2.national_sampling_unit AND
    d1.colony_code != d2.colony_code
ORDER BY dist
)
SELECT * FROM distances WHERE dist > 0;


-- távolság mátrix FBI - POIM
WITH 
poim_minden as (
    SELECT obm_id, ST_Transform(obm_geometry, 31700) as obm_geometry, projekt, national_program, national_grouping_code, national_sampling_unit, colony_code, species, observers 
    FROM milvus
    WHERE
        national_program = 'str_corfru_national_survey' AND 
        projekt = 'POIM Național Păsări 2020-2021' AND
        species != 'test'
),
judit_varjai as (
    SELECT obm_id FROM poim_minden WHERE species = 'Corvus corone cornix' AND observers LIKE 'Veres-Szászka Judit%'
),
poim as (
    SELECT * FROM poim_minden
    WHERE
        obm_id NOT IN (SELECT obm_id FROM judit_varjai)
),
fbi as (
    SELECT obm_id, ST_Transform(obm_geometry, 31700) as obm_geometry, projekt, national_program, national_grouping_code, national_sampling_unit, colony_code, species, observers 
    FROM milvus
    WHERE
        national_program = 'str_corfru_national_survey' AND 
        projekt = 'Farmland Bird Index 2015' AND
        species != 'test'
),
distances as (
    SELECT d1.colony_code as colony_code_POIM, ST_Distance(d1.obm_geometry, d2.obm_geometry) as dist, d2.colony_code as colony_code_FBI
    FROM poim as d1 
    CROSS JOIN fbi as d2 
    WHERE 
        d1.national_sampling_unit = d2.national_sampling_unit
    ORDER BY colony_code_POIM, dist, colony_code_FBI
)
SELECT * FROM distances WHERE dist > 0;


