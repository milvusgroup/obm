-- WITH poim_varju_id AS (
--     select obm_id 
--     from milvus 
--     where 
--         national_program = 'str_corfru_national_survey' AND 
--         projekt = 'POIM Național Păsări 2020-2021' 
-- )
-- SELECT DISTINCT "read" || 322
-- FROM milvus_rules mr
-- LEFT JOIN poim_varju_id on obm_id = row_id
-- WHERE 
--     data_table = 'milvus' AND 
--     obm_id IS NOT NULL AND
--     NOT "read" @> ARRAY[322] ;
     
WITH poim_varju_id AS (
    select obm_id 
    from milvus 
    where 
        national_program = 'str_corfru_national_survey' AND 
        projekt = 'POIM Național Păsări 2020-2021' 
)
UPDATE milvus_rules mr
SET "read" = "read" || 322
FROM poim_varju_id
WHERE 
    obm_id = row_id AND
    data_table = 'milvus' AND 
    obm_id IS NOT NULL AND
    NOT "read" @> ARRAY[322] ;
    
-- osszes 
WITH poim_varju_id AS (
    select obm_id 
    from milvus 
    where 
        access_to_data IN (1,2,3)
)
UPDATE milvus_rules mr
SET "read" = "read" || 322
FROM poim_varju_id
WHERE 
    obm_id = row_id AND
    data_table = 'milvus' AND 
    obm_id IS NOT NULL AND
    NOT "read" @> ARRAY[322] ;
