SELECT obm_id, national_sampling_unit FROM milvus WHERE obm_id IN (1318572, 1318571, 1318564, 1318570, 1318566, 1318563);
SELECT obm_geometry FROM milvus_sampling_units WHERE alapegyseg = 'ER70_09';
BEGIN;
UPDATE milvus SET obm_geometry = '0101000020E6100000D5F73E7117F23540C7B6EAFA689E4640' WHERE obm_id IN (1318572, 1318571, 1318564, 1318570, 1318566, 1318563);

-- Feladat #1534
SELECT national_sampling_unit FROM milvus WHERE obm_id IN (1319580, 1319581, 1319582, 1319583, 1319584, 1319585, 1319586, 1319587, 1319588, 1319589, 1319590, 1319591, 1319592, 1319593, 1319594, 1319595, 1319596, 1319597);
BEGIN;
UPDATE milvus SET national_sampling_unit = 'FQ29_04' WHERE obm_id IN (1319580, 1319581, 1319582, 1319583, 1319584, 1319585, 1319586, 1319587, 1319588, 1319589, 1319590, 1319591, 1319592, 1319593, 1319594, 1319595, 1319596, 1319597);


SELECT record_type FROM milvus_fieldwork WHERE obm_id IN (11177, 11178, 11236, 11239, 11252, 11253);
UPDATE milvus_fieldwork SET record_type = 'str_sampling_unit_status' WHERE obm_id IN (11177, 11178, 11236, 11239, 11252, 11253);

-- Feladat #1535
SELECT national_sampling_unit FROM milvus WHERE obm_id IN (1313556, 1313557, 1313558, 1313559, 1313560, 1313561, 1313562, 1313563, 1313564, 1313565, 1313566, 1313567, 1313568);
BEGIN;
UPDATE milvus SET national_sampling_unit = 'FR78_02' WHERE obm_id IN (1313556, 1313557, 1313558, 1313559, 1313560, 1313561, 1313562, 1313563, 1313564, 1313565, 1313566, 1313567, 1313568);


SELECT FROM milvus where national_sampling_unit = 'LK15_01' AND date = '2021-07-04';
BEGIN;
UPDATE milvus SET national_sampling_unit = 'LK15_13' where national_sampling_unit = 'LK15_01' AND date = '2021-07-04';

-- #1533
SELECT national_sampling_unit, method, exact_time, (time_of_start / 60 || ':' || mod(time_of_start, 60)) as time  FROM milvus WHERE date = '2021-07-01' AND observers = 'Liviu Cernisov' ORDER BY exact_time;

-- ft41_05
BEGIN;
WITH ft41_05 as (
    SELECT DISTINCT method, national_program, national_grouping_code, national_sampling_unit, time_of_start, time_of_end, duration, obm_observation_list_id FROM milvus WHERE date = '2021-07-01' AND observers = 'Liviu Cernisov' AND exact_time::time BETWEEN '09:21' AND '12:21' AND method != 'str_occasional_observation'
)
UPDATE milvus m SET
    method = ft41_05.method,
    national_program = ft41_05.national_program,
    national_grouping_code = ft41_05.national_grouping_code,
    national_sampling_unit = ft41_05.national_sampling_unit,
    time_of_start = ft41_05.time_of_start,
    time_of_end = ft41_05.time_of_end,
    duration = ft41_05.duration,
    obm_observation_list_id = ft41_05.obm_observation_list_id
FROM ft41_05
WHERE  
    m.date = '2021-07-01' AND 
    m.observers = 'Liviu Cernisov' AND 
    m.exact_time::time BETWEEN '09:21' AND '12:21' AND 
    m.method = 'str_occasional_observation';
    
-- ft41_03
BEGIN;
WITH ft41_03 as (
    SELECT DISTINCT method, national_program, national_grouping_code, national_sampling_unit, time_of_start, time_of_end, duration, obm_observation_list_id FROM milvus WHERE date = '2021-07-01' AND observers = 'Liviu Cernisov' AND exact_time::time BETWEEN '12:59' AND '15:59' AND method != 'str_occasional_observation'
)
UPDATE milvus m SET
    method = ft41_03.method,
    national_program = ft41_03.national_program,
    national_grouping_code = ft41_03.national_grouping_code,
    national_sampling_unit = ft41_03.national_sampling_unit,
    time_of_start = ft41_03.time_of_start,
    time_of_end = ft41_03.time_of_end,
    duration = ft41_03.duration,
    obm_observation_list_id = ft41_03.obm_observation_list_id
FROM ft41_03
WHERE  
    m.date = '2021-07-01' AND 
    m.observers = 'Liviu Cernisov' AND 
    m.exact_time::time BETWEEN '12:59' AND '15:59' AND 
    m.method = 'str_occasional_observation';
    

-- Lécci cseréld fel a KK92_03 nak feltöltött pont adatait KK92_05-re. Az én hibám volt ezennel. Svilen adatai 
SELECT FROM milvus where national_sampling_unit = 'KK92_03';
BEGIN;
UPDATE milvus SET national_sampling_unit = 'KK92_05' where national_sampling_unit = 'KK92_03';

-- Feladat #1538
SELECT FROM milvus where national_sampling_unit = 'ES30_06';
SELECT DISTINCT national_grouping_code, national_sampling_unit FROM milvus WHERE observers LIKE '%Gahura Vladimir%';
UPDATE milvus SET national_grouping_code = 'FS30', national_sampling_unit = 'FS30_06' WHERE national_sampling_unit = 'ES30_06' AND observers LIKE '%Gahura Vladimir%';

-- Feladat #1537
DELETE FROM milvus_fieldwork WHERE obm_id = 11405;

-- Feladat #1537 - elmaradás
SELECT national_sampling_unit, exact_time, observers FROM milvus WHERE obm_id IN (1322021, 1322018, 1322020, 1322023, 1322017, 1322019, 1322022);
BEGIN; 
UPDATE milvus SET national_sampling_unit = 'FR04_02' WHERE obm_id IN (1322021, 1322018, 1322020, 1322023, 1322017, 1322019, 1322022);

-- Feladat #1549
UPDATE milvus SET national_sampling_unit = 'LM26_02' WHERE obm_id IN (1345979,1345980,1345981,1345982,1345983,1345984,1345985,1345986,1345987,1345988,1345989,1345990,1345991,1345992,1345993,1345994,1345995,1345996,1345997,1346001);
UPDATE milvus SET national_sampling_unit = 'MM15_13' WHERE obm_id IN (1342372,1342375,1342378,1342381,1342383,1342385,1342387,1342389,1342391,1342394);
SELECT DISTINCT observers, date, national_sampling_unit FROM milvus WHERE national_grouping_code = 'MM15' AND date > '2021-01-01' ;
SELECT DISTINCT observers, date, national_sampling_unit FROM milvus WHERE national_grouping_code = 'MM40' AND date > '2021-01-01' ;


select m.obm_id, national_program, exact_time, observers, date, national_sampling_unit, ST_Distance(st_transform(m.obm_geometry, 31700), st_transform(su.obm_geometry, 31700))
from milvus m
LEFT JOIN milvus_sampling_units su ON m.national_sampling_unit = su.alapegyseg
WHERE alapegyseg = 'KN98_11' AND date > '2021-01-01' ORDER BY exact_time;

-- Feladat #1551
UPDATE milvus SET national_grouping_code = 'LN86', national_sampling_unit = 'LN86_01' WHERE obm_id IN (SELECT obm_id FROM milvus WHERE date BETWEEN '2020-07-01' AND '2020-09-30' AND national_sampling_unit = 'LM86_01' AND observers = 'Ifrim Alexandru');  

-- Feladat #1549
BEGIN;
UPDATE milvus 
SET 
    national_grouping_code = 'MM40', 
    national_sampling_unit = 'MM40_04', 
    date = '2021-07-25',
    obm_observation_list_id = obm_observation_list_id || '_MM40_04',
    time_of_start = NULL,
    time_of_end = NULL
WHERE obm_id IN (1345653, 1345654, 1345655, 1345656, 1345657, 1345658, 1345659, 1345660, 1345661, 1345662, 1345663, 1345664, 1345665, 1345666, 1345667, 1345668, 1345669, 1345670, 1345671, 1345672, 1345673, 1345674, 1345675, 1345676, 1345677, 1345747, 1345748, 1345749, 1345751, 1345752);

UPDATE milvus 
SET 
    national_grouping_code = 'MM40', 
    national_sampling_unit = 'MM40_06', 
    date = '2021-07-26',
    obm_observation_list_id = obm_observation_list_id || '_MM40_06',
    time_of_start = NULL,
    time_of_end = NULL
WHERE obm_id IN (1345697, 1345698, 1345699, 1345700, 1345701, 1345702, 1345703, 1345704, 1345705, 1345706, 1345707, 1345708, 1345709, 1345710, 1345711, 1345712, 1345713, 1345714, 1345715, 1345716, 1345717, 1345718, 1345719, 1345720, 1345722);

UPDATE milvus 
SET 
    national_grouping_code = 'MM40', 
    national_sampling_unit = 'MM40_07', 
    date = '2021-07-25',
    obm_observation_list_id = obm_observation_list_id || '_MM40_07',
    time_of_start = NULL,
    time_of_end = NULL
WHERE obm_id IN (1345678, 1345680, 1345681, 1345683, 1345684, 1345685, 1345686, 1345687, 1345688, 1345689, 1345690, 1345691, 1345692, 1345693, 1345694, 1345695, 1345696);

UPDATE milvus 
SET 
    national_grouping_code = 'MM40', 
    national_sampling_unit = 'MM40_09', 
    date = '2021-07-26',
    obm_observation_list_id = obm_observation_list_id || '_MM40_09',
    time_of_start = NULL,
    time_of_end = NULL
WHERE obm_id IN (1345679, 1345682, 1345721, 1345723, 1345724, 1345725, 1345726, 1345727, 1345728, 1345729, 1345730, 1345731, 1345732, 1345733);


UPDATE milvus 
SET 
    national_program = NULL,
    national_grouping_code = NULL, 
    national_sampling_unit = NULL, 
    obm_observation_list_id = NULL,
    method = 'str_occasional_observation'
WHERE obm_id IN (1345650, 1345651, 1345746);

-- Feladat #1557
UPDATE milvus SET national_sampling_unit = 'FT21_01' WHERE obm_id IN ( 1340738, 1340739, 1340740, 1340741, 1340742, 1340743, 1340744, 1340745 );
