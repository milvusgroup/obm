\copy (SELECT * FROM milvus_grouping_codes where program_name = 'str_corgar_nest_monitoring') TO '~/Documents/milvus/szalakota_gc_backup.csv' CSV HEADER;
\copy (SELECT * FROM milvus_sampling_units where program_nev = 'str_corgar_nest_monitoring') TO '~/Documents/milvus/szalakota_su_backup.csv' CSV HEADER;

SELECT count(*) FROM milvus WHERE national_program = 'str_corgar_nest_monitoring';


DELETE FROM milvus_grouping_codes where program_name = 'str_corgar_nest_monitoring';
DELETE FROM milvus_sampling_units where program_nev = 'str_corgar_nest_monitoring';
UPDATE milvus SET national_grouping_code = 'Vest', national_sampling_unit = NULL WHERE national_program = 'str_corgar_nest_monitoring';
UPDATE milvus_nest_data SET grouping_code = 'Vest', sampling_unit = NULL WHERE program = 'str_corgar_nest_monitoring';

UPDATE milvus m
SET national_sampling_unit = judet 
FROM milvus_qgrids g 
WHERE 
    g.data_table = 'milvus' AND
    m.national_program = 'str_corgar_nest_monitoring' AND
    g.row_id = m.obm_id;
    
WITH gsn as (
    Select obm_id, name from milvus_nest_data nd LEFT JOIN shared.romania_administrative_units a on st_intersects(a.geometry_4326, nd.obm_geometry) WHERE a.natlevel = '2ndOrder' AND nd.program = 'str_corgar_nest_monitoring'
    )
UPDATE milvus_nest_data nd 
SET sampling_unit = gsn.name
FROM gsn
WHERE 
    nd.obm_id = gsn.obm_id;