select * from milvus_terms where subject = 'observers';
\copy (select * from milvus_terms where subject = 'observers') TO '/tmp/milvus_terms_observers_backup.csv' CSV HEADER;
delete from milvus_terms where subject = 'observers';

INSERT INTO milvus_terms (term_id, data_table, subject, term, status, taxon_db)
SELECT (search_id - 702) as term_id, data_table, 'observers', word, status, taxon_db FROM milvus_search WHERE subject = 'observer';   