\copy (SELECT ST_AsText(obm_geometry), * FROM temporary_tables.milvus_obm_obsl) TO '/tmp/temporary_tables_milvus_obm_obsl.csv' CSV HEADER; 
\copy (SELECT * FROM system.uploadings WHERE project_table = 'temporary_tables.milvus_obm_obsl') TO '/tmp/system_uploadings_temporay_backup.csv' CSV HEADER;

-- 1. ahol a measurements_num = 0 ott nincs adat, manuálisan javítom a táblát.
BEGIN;
UPDATE system.uploadings SET project_table = 'milvus' WHERE id IN (select uploading_id from openbirdmaps.uploadings_metadata where data_table = 'temporary_tables.milvus_obm_obsl' AND measurements_num::integer = 0);

-- 2. azok az adatok, ahol a measurements_num nem talál a valós számmal
WITH lists as (
    SELECT DISTINCT observation_list_id FROM openbirdmaps.uploadings_metadata WHERE uploading_id IN (SELECT obm_uploading_id FROM temporary_tables.milvus_obm_obsl)
),
mn as (
    SELECT uploading_id, observation_list_id, measurements_num FROM openbirdmaps.uploadings_metadata WHERE measurements_num IS NOT NULL AND observation_list_id IN (SELECT observation_list_id FROM lists)
)
SELECT mn.uploading_id, md.observation_list_id, mn.measurements_num, count(*) 
FROM openbirdmaps.uploadings_metadata md
LEFT join mn ON mn.observation_list_id = md.observation_list_id
WHERE md.measurements_num IS NULL AND md.observation_list_id IN (SELECT observation_list_id FROM lists) GROUP BY md.observation_list_id, mn.measurements_num, mn.uploading_id;
-- minta lista-rekord létrehozásra, adminerben
INSERT INTO "uploadings" ("uploading_date", "uploader_id", "uploader_name", "validation", "collectors", "description", "access", "group", "project_table", "owner", "project", "form_id", "metadata")
SELECT '2021-07-08 16:31:12.450678', '60', 'Simó Imre', NULL, '', 'API upload', '0', '{322,60}', 'milvus', '{60}', 'milvus', '763', '{"id":"cac11678-575c-4bab-85ec-7b16fd431c3dx","app_version":"OBM_mobile-r4_v1.8.15","observation_list_id":"6c8d2fa7-9bf7-4047-9dd0-5cd997b5cded","observation_list_start":1625737141799,"observation_list_end":1625741320736,"measurements_num":16}'
FROM "uploadings"
WHERE "id" = '346205' AND (("id" = '346205'));

-- 3. failed uploads
WITH upl AS (
    SELECT * FROM system.uploadings WHERE
    project = 'milvus' AND
    project_table IN ('temporary_tables.milvus_obm_obsl', 'temporary_tables.milvus_failed_uploads')
)
SELECT
    l.id AS uploading_id,
    l.uploader_id,
    l.uploader_name,
    l.uploading_date,
    l.metadata->>'observation_list_id' as observation_list_id,
    l.metadata->>'observation_list_start' as observation_list_start,
    l.metadata->>'observation_list_end' as observation_list_end,
    l.metadata->>'observation_list_null_record' as observation_list_null_record,
    l.metadata->>'measurements_num' as measurements_num,
    string_agg(o.id::text,',') as observation_list_elements
FROM upl l LEFT JOIN upl o ON l.metadata->>'observation_list_id' = o.metadata->>'observation_list_id'
WHERE
    l.metadata::jsonb ? 'measurements_num' AND
    NOT o.metadata::jsonb ? 'measurements_num'
GROUP BY l.id, observation_list_id, observation_list_start, observation_list_end, observation_list_null_record, measurements_num, l.uploader_id, l.uploader_name, l.uploading_date
ORDER BY uploading_id;

-- valamiert nem mennek at a failed tablabol az adatok
select from milvus where obm_uploading_id = 380521;
BEGIN; 
UPDATE system.uploadings u SET project_table = 'temporary_tables.milvus_failed_uploads' WHERE metadata->>'observation_list_id' IN (SELECT DISTINCT metadata->>'observation_list_id' FROM system.uploadings WHERE id IN (SELECT DISTINCT obm_uploading_id FROM temporary_tables.milvus_failed_uploads));

-- milvus_obm_upl
SELECT * FROM system.uploadings WHERE id IN (SELECT DISTINCT obm_uploading_id FROM temporary_tables.milvus_obm_upl);
