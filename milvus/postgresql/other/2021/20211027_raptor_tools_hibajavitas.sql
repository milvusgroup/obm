BEGIN;
WITH asdas as (
SELECT obm_id, pair_id, pr.id, pr.oldid, (national_sampling_unit = sampling_unit) su_eq, (lg.species_valid = pr.species) sp_eq
FROM milvus As lg   
LEFT JOIN openbirdmaps.raptor_period p ON
    lg.projekt = p.project AND
    lg.national_program = p.program AND 
    lg."date" BETWEEN p.date_from AND p.date_until
LEFT JOIN openbirdmaps.raptor_pair pr ON pr.oldid = pair_id
WHERE 
    p.id = 1 AND 
    pair_id IS NOT NULL
)
UPDATE milvus SET pair_id = asdas.id FROM asdas WHERE milvus.obm_id = asdas.obm_id;
COMMIT;


WITH asdas as (
SELECT obm_id, pair_id, pr.id, pr.oldid, (national_sampling_unit = sampling_unit) su_eq, (lg.species_valid = pr.species) sp_eq
FROM milvus As lg   
LEFT JOIN openbirdmaps.raptor_period p ON
    lg.projekt = p.project AND
    lg.national_program = p.program AND 
    lg."date" BETWEEN p.date_from AND p.date_until
LEFT JOIN openbirdmaps.raptor_pair pr ON pr.oldid = pair_id
WHERE 
    p.id = 1 AND 
    pair_id IS NOT NULL
)
SELECT * FROM asdas;
