SELECT max(form_id), form_name FROM project_forms WHERE project_table = 'milvus' GROUP BY form_name;

-- surveyed_geometry hozzáadása
BEGIN;
WITH active_forms as (
    (SELECT form_id,form_name,active,published, published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms p1 WHERE destination_table = 'milvus' AND project_table='milvus' AND published IS NULL ORDER BY form_name)
    UNION
    (SELECT t1.form_id,t1.form_name,t1.active,t1.published, t1.published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms t1 JOIN ( SELECT form_name,MAX(published) AS MAXDATE FROM project_forms WHERE destination_table = 'milvus' AND project_table='milvus' GROUP BY form_name ) t2 ON t1.form_name = t2.form_name AND t1.published = t2.MAXDATE WHERE destination_table = 'milvus' AND t1.project_table='milvus')
    ORDER BY form_id
)
INSERT INTO project_forms_data (form_id, "column", "description", "type", 
    "control", "obl", "fullist", "default_value", "count", "api_params", 
    "column_label")
SELECT 
    f.form_id, 'surveyed_geometry' as col, 
    'str_place_observation_point' as descr, 'point' as typ, 
    'nocheck' as control, 2 as obl, 0 as fullist,
    '_auto_geometry' as default_value, '{}' as count,
    '{}' as api_params, 'str_observers_location' as column_label
FROM project_forms f LEFT JOIN project_forms_data fd ON f.form_id = fd.form_id AND "column" = 'surveyed_geometry'
WHERE 
    project_table = 'milvus' AND 
    f.form_id IN (SELECT form_id FROM active_forms) AND fd.form_id IS NULL AND 
    form_type = '{"api"}';
 
-- minden geometria mező pont típusúra alakítása.
BEGIN;
UPDATE project_forms_data fd SET "type" = 'point' FROM project_forms f WHERE f.form_id = fd.form_id AND project_table = 'milvus' AND "column" = 'obm_geometry';
COMMIT;
BEGIN;
UPDATE project_forms_data fd SET "type" = 'point' FROM project_forms f WHERE f.form_id = fd.form_id AND project_table = 'milvus' AND "column" = 'surveyed_geometry' AND "type" = 'wkt';
COMMIT;

-- a dátum nem once 
BEGIN;
WITH active_forms as (
    (SELECT form_id,form_name,active,published, published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms p1 WHERE destination_table = 'milvus' AND project_table='milvus' AND published IS NULL ORDER BY form_name)
    UNION
    (SELECT t1.form_id,t1.form_name,t1.active,t1.published, t1.published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms t1 JOIN ( SELECT form_name,MAX(published) AS MAXDATE FROM project_forms WHERE destination_table = 'milvus' AND project_table='milvus' GROUP BY form_name ) t2 ON t1.form_name = t2.form_name AND t1.published = t2.MAXDATE WHERE destination_table = 'milvus' AND t1.project_table='milvus')
    ORDER BY form_id
),
oncedatumos_formok as (
    SELECT 
        f.form_id
    FROM project_forms f LEFT JOIN project_forms_data fd ON f.form_id = fd.form_id
    WHERE 
        f.form_id IN (SELECT form_id FROM active_forms) AND
        project_table = 'milvus' AND 
        "column" = 'date' AND
        form_type = '{"api"}' AND
        api_params = '["once"]'
)
UPDATE project_forms_data fd SET api_params = '{""}' FROM oncedatumos_formok o 
WHERE fd.form_id = o.form_id AND 
    "column" = 'date' AND
    api_params = '["once"]';
COMMIT;

-- megfigyelők: accepted vagy undefined prefilter 
BEGIN;
WITH active_forms as (
    (SELECT form_id,form_name,active,published, published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms p1 WHERE destination_table = 'milvus' AND project_table='milvus' AND published IS NULL ORDER BY form_name)
    UNION
    (SELECT t1.form_id,t1.form_name,t1.active,t1.published, t1.published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms t1 JOIN ( SELECT form_name,MAX(published) AS MAXDATE FROM project_forms WHERE destination_table = 'milvus' AND project_table='milvus' GROUP BY form_name ) t2 ON t1.form_name = t2.form_name AND t1.published = t2.MAXDATE WHERE destination_table = 'milvus' AND t1.project_table='milvus')
    ORDER BY form_id
),
ld as (
    SELECT list_definition FROM project_forms_data WHERE form_id = 534 AND "column" = 'observers'
    ),
formid_col as (
    SELECT 
    fd.form_id, fd."column"
    FROM project_forms as f 
    LEFT JOIN project_forms_data fd ON f.form_id = fd.form_id
    LEFT JOIN ld ON 1 = 1
    WHERE 
        f.form_id IN (SELECT form_id FROM active_forms) AND
        project_table = 'milvus' AND 
        "column" = 'observers' AND
        (form_type = '{"api"}' OR form_type = '{"web"}')
    )
UPDATE project_forms_data SET list_definition = ld.list_definition FROM ld WHERE (form_id, "column") IN (SELECT form_id, "column" FROM formid_col);
COMMIT;

-- a 'pár' ivart kivenni a listából 
BEGIN;
WITH active_forms as (
    (SELECT form_id,form_name,active,published, published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms p1 WHERE destination_table = 'milvus' AND project_table='milvus' AND published IS NULL ORDER BY form_name)
    UNION
    (SELECT t1.form_id,t1.form_name,t1.active,t1.published, t1.published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms t1 JOIN ( SELECT form_name,MAX(published) AS MAXDATE FROM project_forms WHERE destination_table = 'milvus' AND project_table='milvus' GROUP BY form_name ) t2 ON t1.form_name = t2.form_name AND t1.published = t2.MAXDATE WHERE destination_table = 'milvus' AND t1.project_table='milvus')
    ORDER BY form_id
),
ld as (
    SELECT '{"optionsTable": "milvus_terms", "valueColumn": "term", "preFilterColumn": ["data_table", "subject", "status"], "preFilterValue": ["milvus", "gender", "accepted"]}'::json as list_definition
    ),
formid_col as (
    SELECT 
        fd.form_id, fd."column"
    FROM project_forms as f 
    LEFT JOIN project_forms_data fd ON f.form_id = fd.form_id
    LEFT JOIN ld ON 1=1
    WHERE 
        f.form_id IN (SELECT form_id FROM active_forms) AND
        project_table = 'milvus' AND 
        "column" = 'gender' AND
        (form_type = '{"api"}' OR form_type = '{"web"}') AND
        fd.form_id != 842
)
UPDATE project_forms_data SET list_definition = ld.list_definition FROM ld WHERE (form_id, "column") IN (SELECT form_id, "column" FROM formid_col);
COMMIT;

-- ellenőrizni, hogy a Feltöltő ki van-e pipálva a hozzáférésnél 
BEGIN;
WITH active_forms as (
    (SELECT form_id,form_name,active,published, published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms p1 WHERE destination_table = 'milvus' AND project_table='milvus' AND published IS NULL ORDER BY form_name)
    UNION
    (SELECT t1.form_id,t1.form_name,t1.active,t1.published, t1.published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms t1 JOIN ( SELECT form_name,MAX(published) AS MAXDATE FROM project_forms WHERE destination_table = 'milvus' AND project_table='milvus' GROUP BY form_name ) t2 ON t1.form_name = t2.form_name AND t1.published = t2.MAXDATE WHERE destination_table = 'milvus' AND t1.project_table='milvus')
    ORDER BY form_id
),
formid_col as (
    SELECT 
        *
    FROM project_forms as f 
    WHERE 
        f.form_id IN (SELECT form_id FROM active_forms) AND
        project_table = 'milvus' AND 
        (form_type = '{"api"}' OR form_type = '{"web"}')
)
SELECT * FROM formid_col;
COMMIT;


-- webformon visszateszem a wkt geometriatipust
BEGIN;
WITH active_forms as (
    (SELECT form_id,form_name,active,published, published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms p1 WHERE project_table='milvus' AND published IS NULL ORDER BY form_name)
    UNION
    (SELECT t1.form_id,t1.form_name,t1.active,t1.published, t1.published_form_id, array_to_string("form_type",'|') as form_type FROM project_forms t1 JOIN ( SELECT form_name,MAX(published) AS MAXDATE FROM project_forms WHERE project_table='milvus' GROUP BY form_name ) t2 ON t1.form_name = t2.form_name AND t1.published = t2.MAXDATE WHERE t1.project_table='milvus')
    ORDER BY form_id
),
formid_col as (
    SELECT 
        fd.form_id, fd."column", f.form_name, "type"
    FROM project_forms as f 
    LEFT JOIN project_forms_data fd ON f.form_id = fd.form_id
    WHERE 
        f.form_id IN (SELECT form_id FROM active_forms) AND
        "column" = 'obm_geometry' AND
        (form_type = '{"web"}') 
)
UPDATE project_forms_data SET "type" = 'wkt' WHERE (form_id, "column") IN (SELECT form_id, "column" FROM formid_col);
COMMIT;
ROLLBACK;
