SELECT 
    DISTINCT uploader_name, national_program, projekt, extract(year from date)
FROM milvus m
LEFT JOIN system.uploadings u ON u.id = m.obm_uploading_id
LEFT JOIN openbirdmaps.raptor_period p ON
    m.projekt = p.project AND
    m.national_program = p.program AND 
    m."date" BETWEEN p.date_from AND p.date_until 
WHERE 
    m.species_valid IN ( SELECT sp FROM unnest(p.species_list) as t(sp) )  AND
    p.id = 3 AND
    number > 1;

-- Balla Dani által talált hibával bevezetett adatok.
WITH asdf as (
    SELECT 
        session_id, sampling_unit, species, min, max, status, geometry, array_agg(obm_id) as obm_ids
    FROM openbirdmaps.raptor_pair p
    LEFT JOIN openbirdmaps.raptor_pair_observation po ON p.id = po.pair_id
    LEFT JOIN openbirdmaps.raptor_survey_session ss ON p.session_id = ss.id
    WHERE ss.period_id = 3 
    GROUP BY session_id, sampling_unit, species, min, max, status, geometry, pair_id
    )
SELECT DISTINCT session_id, sampling_unit, species, min, max, status, geometry, obm_ids, count(*)
FROM asdf
GROUP BY session_id, sampling_unit, species, min, max, status, geometry, obm_ids
HAVING count(*) > 1
ORDER BY sampling_unit;

\copy (WITH asdf as ( SELECT session_id, sampling_unit, species, min, max, status, geometry, array_agg(obm_id) as obm_ids FROM openbirdmaps.raptor_pair p LEFT JOIN openbirdmaps.raptor_pair_observation po ON p.id = po.pair_id LEFT JOIN openbirdmaps.raptor_survey_session ss ON p.session_id = ss.id WHERE ss.period_id = 3 GROUP BY session_id, sampling_unit, species, min, max, status, geometry, pair_id ) SELECT DISTINCT session_id, sampling_unit, species, min, max, status, geometry, obm_ids, count(*) FROM asdf GROUP BY session_id, sampling_unit, species, min, max, status, geometry, obm_ids HAVING count(*) > 1 ORDER BY sampling_unit) TO '/tmp/balladani_hibaja.sql' CSV;

WITH minids as (
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 340 AND sampling_unit = 'EQ39_01' AND species = 'Circaetus gallicus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000139C20081580354070EB6E9E8A8D4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 340 AND sampling_unit = 'EQ39_03' AND species = 'Buteo buteo' AND min = 0 AND max = 1 AND status = 'str_possible_pair' AND geometry = '0101000020E6100000A224FE0A14803540EE3FDC1CC18A4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 340 AND sampling_unit = 'EQ39_03' AND species = 'Ciconia nigra' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000CAF26135177A3540B2B23E6498874640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 341 AND sampling_unit = 'EQ66_04' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000CC1B50042BD43540D36B4615E46A4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 280 AND sampling_unit = 'ER48_03' AND species = 'Buteo buteo' AND min = 0 AND max = 1 AND status = 'str_possible_pair' AND geometry = '0101000020E61000005E6FBEB8A4903540C222C638ADF84640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 280 AND sampling_unit = 'ER48_03' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000004BD9A0313E8F35409990A9976CF84640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 280 AND sampling_unit = 'ER48_14' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000B0D6E1CCD5943540175CA638A4F24640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 233 AND sampling_unit = 'ER73_06' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E610000040391EF46B003640CC03D763C2BD4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 297 AND sampling_unit = 'ES92_02' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000D2BE7F14F14536408F03A26C79214740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 357 AND sampling_unit = 'ET74_04' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000004D247FCD450B3640422B3F8FE2AF4740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 328 AND sampling_unit = 'FS30_04' AND species = 'Pernis apivorus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000001E016266ADB136403CEA1081A80E4740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 333 AND sampling_unit = 'FS32_02' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000004DD0A04D00BF364089D47CE1EA1D4740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 253 AND sampling_unit = 'FT85_01' AND species = 'Accipiter nisus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000A0DD7F36026C37401CE8717DF7B94740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 253 AND sampling_unit = 'FT85_05' AND species = 'Falco subbuteo' AND min = 0 AND max = 0 AND status = 'str_not_pair' AND geometry = '0101000020E610000050B62012C07D374019F29DD260B44740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 302 AND sampling_unit = 'GS04_04' AND species = 'Pernis apivorus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000009073CFDF22A937409A3B389872374740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 302 AND sampling_unit = 'GS04_09' AND species = 'Accipiter nisus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000F999614002BE37406F086294B8314740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 292 AND sampling_unit = 'LJ67_02' AND species = 'Ciconia nigra' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000218BDEF0D54E3940B48F315166FF4540' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 303 AND sampling_unit = 'LM02_02' AND species = 'Accipiter nisus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000010000C07A7F384094C128FDF21C4740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 303 AND sampling_unit = 'LM02_08' AND species = 'Accipiter nisus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000010000C0157B38400A15663D55244740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 304 AND sampling_unit = 'LM20_06' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000D02E4CFDE5D33840B89E1B4364074740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 331 AND sampling_unit = 'ML42_08' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000003DE89F01714B3A4060DCCE3D32AE4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 338 AND sampling_unit = 'ML73_06' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000F0D43037F0AB3A406BF1C11C11BF4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 338 AND sampling_unit = 'ML73_06' AND species = 'Hieraaetus pennatus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E610000032F401E1C0AC3A401C687D4BF2BE4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 334 AND sampling_unit = 'ML93_02' AND species = 'Pernis apivorus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000003F43BE9FA8FA3A40CAE9BDC1AFBD4640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 310 AND sampling_unit = 'MN24_10' AND species = 'Ciconia nigra' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000BAD4FE8AD9083A40A1AAB65984B04740'
    )
DELETE FROM openbirdmaps.raptor_pair WHERE id IN (SELECT minid FROM minids);

WITH minids as (
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 280 AND sampling_unit = 'ER48_03' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000004BD9A0313E8F35409990A9976CF84640' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 297 AND sampling_unit = 'ES92_02' AND species = 'Accipiter nisus' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000005CDE88BDD94036404917EA2B44254740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 297 AND sampling_unit = 'ES92_02' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000D2BE7F14F14536408F03A26C79214740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 333 AND sampling_unit = 'FS32_12' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000000100009052CA36407CB5CD7E9F234740' UNION
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 237 AND sampling_unit = 'GQ07_04' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E6100000BB634E232FA0374003D39ECF36764640' 
    )
DELETE FROM openbirdmaps.raptor_pair WHERE id IN (SELECT minid FROM minids);

WITH minids as (
    SELECT min(id) as minid FROM openbirdmaps.raptor_pair WHERE session_id = 280  AND sampling_unit = 'ER48_17' AND species = 'Buteo buteo' AND min = 1 AND max = 1 AND status = 'str_certain_pair' AND geometry = '0101000020E61000001575B0C556A6354056573FE004F14640'
)
DELETE FROM openbirdmaps.raptor_pair WHERE id IN (SELECT minid FROM minids);

-- TT teszt négyzet törlése
DELETE FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'TT86';
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 234;


\copy (SELECT ST_AsText(obm_geometry) as wkt, * FROM milvus WHERE national_program = 'str_raptor_monitoring' AND projekt = 'POIM Național Păsări 2020-2021') to '/tmp/ragadozoadatok_poim.csv' CSV HEADER; 