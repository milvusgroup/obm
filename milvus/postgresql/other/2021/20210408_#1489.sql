SELECT DISTINCT count(*) FROM temporary_tables.milvus_obm_obsl;

SELECT count(*) FROM system.uploadings WHERE project = 'milvus' AND project_table = 'temporary_tables.milvus_obm_obsl';

SELECT DISTINCT metadata->>'observation_list_id', metadata->>'measurements_num', count(*) 
FROM system.uploadings 
WHERE 
    project = 'milvus' AND project_table = 'temporary_tables.milvus_obm_obsl'
GROUP BY metadata->>'observation_list_id',metadata->>'measurements_num' ;

SELECT * FROM system.uploadings WHERE metadata->>'observation_list_id' = '58645614-289e-4e7d-803d-2a961fd544e7';
SELECT FROM milvus WHERE obm_observation_list_id = '58645614-289e-4e7d-803d-2a961fd544e7';

SELECT DISTINCT observers FROM temporary_tables.milvus_obm_obsl;

SELECT id, metadata FROM system.uploadings WHERE metadata->>'observation_list_id' = 'cf1b0358-cad7-4151-9780-fef6a00763aa' AND metadata->>'measurements_num' = '46';

SELECT id, metadata->>'observation_list_id', metadata->>'measurements_num', count(*) FROM system.uploadings WHERE id IN (SELECT obm_uploading_id FROM temporary_tables.milvus_obm_obsl) GROUP BY metadata->>'observation_list_id', metadata->>'measurements_num', id;


BEGIN;
INSERT INTO system.uploadings (
  uploading_date, 
  metadata,
  uploader_id,
  uploader_name,
  description,
  access,
  "group",
  project_table,
  project,
  form_id
) 
VALUES 
(
  NOW(), 
  '{"id": "2f67c4d5-611d-448b-a98f-e00dda1f6736", "app_version": "OBM_mobile-r4_v1.8.4", "measurements_num": 1, "observation_list_id": "635d8b17-eaa2-432c-8e3f-64aa08b340f1", "observation_list_end": 1605819447441, "observation_list_start": 1605817954533}',
  42,
  'Bărbos Lőrinc',
  'API upload',
  0,
  '{322,42}',
  'temporary_tables.milvus_obm_obsl',
  'milvus',
  719
);


INSERT INTO system.uploadings (
  uploading_date, 
  metadata,
  uploader_id,
  uploader_name,
  description,
  access,
  "group",
  project_table,
  project,
  form_id
) 
VALUES 
(
  NOW(), 
  '{"id": "65645dd9-0ac4-49d7-abcc-bf11b27f673d", "app_version": "OBM_mobile-r4_v1.8.4", "measurements_num": 1, "observation_list_id": "e554e941-3042-4475-b0f1-2fc0fbfe8a7e", "observation_list_end": 1605814541457, "observation_list_start": 1605813160290}',
  42,
  'Bărbos Lőrinc',
  'API upload',
  0,
  '{322,42}',
  'temporary_tables.milvus_obm_obsl',
  'milvus',
  719
);

INSERT INTO system.uploadings (
  uploading_date, 
  metadata,
  uploader_id,
  uploader_name,
  description,
  access,
  "group",
  project_table,
  project,
  form_id
) 
VALUES 
(
  NOW(), 
  '{"id":"05734319-eba9-47db-b225-74555447dd77","app_version":"OBM_mobile-r4_v1.8.15","observation_list_id":"11e32cb3-e182-4450-91d0-22c88c2f97a8","observation_list_start":1614842941984,"observation_list_end":1614855034507,"measurements_num":2}',
  45,
  'Kis Réka-Beáta',
  'API upload',
  0,
  '{322,45}',
  'temporary_tables.milvus_obm_obsl',
  'milvus',
  142
);


207218 | 2021-03-05 10:55:20.251809 |          45 | Kis Réka-Beáta |            |            | API upload  |      0 | {322,45} | milvus        | {45}  | milvus  |     142 | 
