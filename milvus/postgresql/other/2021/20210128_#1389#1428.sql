DROP TABLE tmp.euring;
CREATE TABLE tmp.euring ();
ALTER TABLE tmp.euring ADD COLUMN "code" integer;
ALTER TABLE tmp.euring ADD COLUMN "name" text;
ALTER TABLE tmp.euring ADD COLUMN "date_updated" text;

-- az euring.csv-t a birdring R csomagból exportáltam ki
\copy tmp.euring FROM '/tmp/euring.csv' CSV HEADER;

SELECT DISTINCT taxon_id, word 
FROM milvus_taxon 
WHERE
    lang = 'species_valid' AND
    status = 'accepted' AND
    taxon_id NOT IN (
    SELECT DISTINCT taxon_id FROM milvus_taxon WHERE word IN (
        SELECT name FROM tmp.euring
    )
);
\copy ( SELECT DISTINCT taxon_id, word FROM milvus_taxon WHERE lang = 'species_valid' AND status = 'accepted' AND taxon_id NOT IN ( SELECT DISTINCT taxon_id FROM milvus_taxon WHERE word IN ( SELECT name FROM tmp.euring ) ) ) TO '/tmp/euring_missing.csv' CSV HEADER;

    
\copy ( WITH talal as (SELECT DISTINCT taxon_id, name as euring_name, code FROM milvus_taxon LEFT JOIN tmp.euring ON word = name WHERE name IS NOT NULL) SELECT DISTINCT milvus_taxon.taxon_id, word, euring_name, code FROM milvus_taxon LEFT JOIN talal ON talal.taxon_id = milvus_taxon.taxon_id WHERE lang = 'species_valid' AND status = 'accepted') to '/tmp/euring_joined.csv' CSV HEADER;

BEGIN;
WITH talal as (
    SELECT DISTINCT taxon_id, name as euring_name, code 
    FROM milvus_taxon 
    LEFT JOIN tmp.euring ON word = name WHERE name IS NOT NULL
)
-- INSERT INTO milvus_taxon (taxon_id, word, lang, status)
SELECT DISTINCT 
    milvus_taxon.taxon_id, word, code as word, 'euring' as lang, 'accepted' as status 
FROM milvus_taxon 
LEFT JOIN talal ON talal.taxon_id = milvus_taxon.taxon_id 
WHERE euring_name IS NOT NULL AND lang = 'species_valid' AND status = 'accepted' AND code = 1574;
-- volt már euring oszlop, csak nem vettem észre
update milvus_taxon set lang = 'species_euring' where lang = 'euring';
COMMIT;

-- a species_name_validation job parancsa, csak nem futott le egyben
BEGIN;
WITH rows AS (
SELECT DISTINCT f.species, foo.valid_word 
FROM milvus f 
LEFT JOIN (
    SELECT 
      t.word as src_word, 
      CASE WHEN tt.word IS NULL 
        THEN fb.word 
        ELSE tt.word 
      END as valid_word 
    FROM milvus_taxon t 
    LEFT JOIN 
      milvus_taxon tt ON t.taxon_id = tt.taxon_id AND tt.status = 'accepted'
    LEFT JOIN -- fallback lang is the scientific name
      milvus_taxon fb ON t.taxon_id = fb.taxon_id AND fb.lang = 'species_valid' AND fb.status = 'accepted'
    WHERE 
      tt.lang = 'species_euring' AND tt.word IS NOT NULL
) foo ON f.species = foo.src_word 
WHERE f.species_euring IS DISTINCT FROM foo.valid_word
LIMIT 10)
UPDATE milvus ff SET species_euring = rows.valid_word 
FROM rows 
WHERE rows.species = ff.species AND ff.species_euring IS DISTINCT FROM rows.valid_word;
COMMIT;




-- az alfajoknak magyar, román és angol nevet kell adjak
WITH asdf as (
    SELECT taxon_id, word as wl FROM milvus_taxon
),
dt as (
    SELECT 
        asdf.taxon_id as parent_id, 
        milvus_taxon.taxon_id, 
        wl parent_name, 
        word subspecies_name
    FROM milvus_taxon, asdf 
    WHERE 
        word LIKE asdf.wl || ' %' AND 
        word NOT LIKE '%/%' AND 
        word NOT LIKE '%spec.' AND 
        asdf.taxon_id != milvus_taxon.taxon_id AND 
        word NOT LIKE '% x %' AND 
        lang = 'species_valid' AND 
        wl != 'Poecile'
),
alfajnevek as (
    SELECT dt.parent_id, tx.word, dt.taxon_id, tx.word || ' (' || replace(dt.subspecies_name, dt.parent_name || ' ', '') || ')' as alfajnev
    FROM dt
    LEFT JOIN milvus_taxon tx  ON tx.taxon_id = dt.parent_id AND tx.lang = 'species_en'
    LEFT JOIN milvus_taxon tx2 ON tx2.taxon_id = dt.taxon_id AND tx2.lang = 'species_en'
    WHERE tx2.word IS NULL
    )
INSERT INTO milvus_taxon (word, taxon_id, status, lang)
SELECT alfajnev, taxon_id, 'accepted', 'species_en' FROM alfajnevek;
    

-- alfaj status beállítása
UPDATE milvus_taxonmeta SET parent_id = dt.parent_id, rank = 'subspecies' FROM dt WHERE milvus_taxonmeta.taxon_id = dt.taxon_id;

