WITH asdf as (
    SELECT DISTINCT 
        date, 
        time_of_start, 
        observers, 
        national_program, 
        national_grouping_code, 
        national_sampling_unit 
    FROM milvus m
    LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id
    WHERE 
        obm_observation_list_id LIKE '20210928%' AND 
        national_program IS NOT NULL AND
        national_grouping_code IS NOT NULL AND
        national_sampling_unit IS NOT NULL AND
        observers IS NOT NULL AND
        time_of_start IS NOT NULL AND
        comlete_list = 0 AND
        (u.description != 'API upload' OR u.description IS NULL)
),
list as (
    SELECT ROW_NUMBER() OVER() as rn, *
    FROM asdf    
    )
UPDATE milvus m
SET obm_observation_list_id = '20210928-' || rn -- encode((m.date::varchar || m.time_of_start || m.national_program || m.national_grouping_code || m.national_sampling_unit)::bytea, 'base64')
FROM list
WHERE 
    obm_observation_list_id IS NULL AND 
    m.date = list.date AND
    m.observers = list.observers AND 
    (m.time_of_start IS NOT DISTINCT FROM list.time_of_start) AND 
    (m.national_program IS NOT DISTINCT FROM list.national_program) AND 
    (m.national_grouping_code IS NOT DISTINCT FROM list.national_grouping_code) AND 
    (m.national_sampling_unit IS NOT DISTINCT FROM list.national_sampling_unit) AND
    m.comlete_list = 0;

WITH list as (
    SELECT DISTINCT 
        obm_uploading_id,
        list_id,
        CASE WHEN list_id IS NULL THEN obm_uploading_id::varchar ELSE obm_uploading_id || '_' || list_id END as obmolid
    FROM milvus m
    LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id
    WHERE 
        obm_observation_list_id IS NULL AND 
        comlete_list = 1 AND
        (u.description != 'API upload' OR u.description IS NULL)
)
UPDATE milvus m
SET obm_observation_list_id = obmolid
FROM list
WHERE 
    m.obm_observation_list_id IS NULL AND 
    comlete_list = 1 AND
    m.obm_uploading_id = list.obm_uploading_id AND
    m.list_id IS NOT DISTINCT FROM list.list_id;
        
    
SELECT DISTINCT 
    obm_uploading_id,
    u.description,
    CASE WHEN list_id IS NULL THEN obm_uploading_id::varchar ELSE obm_uploading_id || '_' || list_id END as obmolid
FROM milvus m
LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id
WHERE 
    obm_observation_list_id IS NULL AND 
    comlete_list = 1 ;


WITH list as (
    SELECT DISTINCT 
        obm_id,
        date, 
        time_of_start, 
        observers, 
        national_program, 
        national_grouping_code, 
        national_sampling_unit, obm_observation_list_id
    FROM milvus m
    LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id
    WHERE 
        national_program IS NOT NULL AND
        national_grouping_code IS NOT NULL AND
        national_sampling_unit IS NOT NULL AND
        observers IS NOT NULL AND
        time_of_start IS NOT NULL AND
        comlete_list = 0 AND
        (u.description != 'API upload' OR u.description IS NULL)
)
UPDATE milvus SET obm_observation_list_id = NULL FROM list WHERE list.obm_id = milvus.obm_id AND milvus.obm_observation_list_id LIKE 'Mj%';
select obm_observation_list_id, mtc FROM list;

WITH list as (
    SELECT DISTINCT 
        obm_uploading_id,
        list_id,
        CASE WHEN list_id IS NULL THEN obm_uploading_id::varchar ELSE obm_uploading_id || '_' || list_id END as obmolid
    FROM milvus m
    LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id
    WHERE 
        obm_observation_list_id IS NULL AND 
        comlete_list = 1 AND
        u.description = 'API upload'
)
UPDATE milvus m
SET comlete_list = 0
FROM list
WHERE 
    m.obm_observation_list_id IS NULL AND 
    comlete_list = 1 AND
    m.obm_uploading_id = list.obm_uploading_id AND
    m.list_id IS NOT DISTINCT FROM list.list_id;
