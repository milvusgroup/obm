-- István lekérdezése:
WITH missing AS (
    SELECT row_id, uploader_id, (ARRAY[uploader_id] && read) as test
    FROM public.milvus_rules
    LEFT JOIN (
        SELECT uploader_id, obm_id FROM public.milvus JOIN system.uploadings ON obm_uploading_id=id
    ) AS up_id ON obm_id=row_id
    WHERE data_table='milvus'
    GROUP BY row_id, uploader_id, read
    HAVING ARRAY[uploader_id] && read = FALSE
)
select count(*) from missing;
SELECT * FROM public.milvus_rules RIGHT JOIN missing ON milvus_rules.row_id=missing.row_id;


-- Istvane modositva
WITH up_id as (
    SELECT uploader_id, obm_id FROM public.milvus JOIN system.uploadings ON obm_uploading_id=id
), 
miss AS (
    SELECT row_id, uploader_id, data_table, (ARRAY[uploader_id] && read) as test
    FROM public.milvus_rules
    LEFT JOIN up_id ON obm_id=row_id AND data_table='milvus'
    WHERE data_table='milvus'
)


SELECT 
    u.id, u.uploader_id, r.*
FROM miss
LEFT JOIN milvus m ON m.obm_id = miss.row_id
LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id
LEFT JOIN milvus_rules r ON r.row_id = m.obm_id AND r.data_table = 'milvus'
WHERE 
    miss.test = false
ORDER BY u.id;

UPDATE milvus_rules mr
SET "read" = "read" || uploader_id
FROM miss
WHERE 
    miss.row_id = mr.row_id AND
    mr.data_table = 'milvus' AND 
    miss.test = false;