-- MN46
SELECT * FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'MN46';
SELECT * FROM openbirdmaps.raptor_pair WHERE session_id = 146;
SELECT * FROM openbirdmaps.raptor_double WHERE session_id = 146;
SELECT * FROM openbirdmaps.raptor_double_pair WHERE double_id = 244;

DELETE FROM openbirdmaps.raptor_double_pair WHERE double_id = 244;
DELETE FROM openbirdmaps.raptor_double WHERE session_id = 146;

SELECT * FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 146);
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 146;

-- KL83
SELECT * FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'KL83';
SELECT * FROM openbirdmaps.raptor_double WHERE session_id = 267;
SELECT * FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 267);
DELETE FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 267);
DELETE FROM openbirdmaps.raptor_double WHERE session_id = 267;
SELECT * FROM openbirdmaps.raptor_pair WHERE session_id = 267;
SELECT * FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 267);
DELETE FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 267);
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 267;

-- LL29
SELECT * FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'LL29';
SELECT * FROM openbirdmaps.raptor_double WHERE session_id = 270;
SELECT * FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 270);
DELETE FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 270);
DELETE FROM openbirdmaps.raptor_double WHERE session_id = 270;
SELECT * FROM openbirdmaps.raptor_pair WHERE session_id = 270;
SELECT * FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 270);
DELETE FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 270);
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 270;

-- LP60
SELECT * FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'LP60';
SELECT * FROM openbirdmaps.raptor_double WHERE session_id = 210;
SELECT * FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 210);
DELETE FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 210);
DELETE FROM openbirdmaps.raptor_double WHERE session_id = 210;
SELECT * FROM openbirdmaps.raptor_pair WHERE session_id = 210;
SELECT * FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 210);
DELETE FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 210);
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 210;

-- ML66
SELECT * FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'ML66';
SELECT * FROM openbirdmaps.raptor_double WHERE session_id = 272;
SELECT * FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 272);
DELETE FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 272);
DELETE FROM openbirdmaps.raptor_double WHERE session_id = 272;
SELECT * FROM openbirdmaps.raptor_pair WHERE session_id = 272;
SELECT * FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 272);
DELETE FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 272);
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 272;

-- MN39
SELECT * FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'MN39';
SELECT * FROM openbirdmaps.raptor_double WHERE session_id = 158;
SELECT * FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 158);
DELETE FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 158);
DELETE FROM openbirdmaps.raptor_double WHERE session_id = 158;
SELECT * FROM openbirdmaps.raptor_pair WHERE session_id = 158;
SELECT * FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 158);
DELETE FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 158);
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 158;

-- PK87
SELECT * FROM openbirdmaps.raptor_survey_session WHERE grouping_code = 'PK87';
SELECT * FROM openbirdmaps.raptor_double WHERE session_id = 281;
SELECT * FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 281);
DELETE FROM openbirdmaps.raptor_double_pair WHERE double_id IN (SELECT id FROM openbirdmaps.raptor_double WHERE session_id = 281);
DELETE FROM openbirdmaps.raptor_double WHERE session_id = 281;
SELECT * FROM openbirdmaps.raptor_pair WHERE session_id = 281;
SELECT * FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 281);
DELETE FROM openbirdmaps.raptor_pair_observation WHERE pair_id IN (SELECT id FROM openbirdmaps.raptor_pair WHERE session_id = 281);
DELETE FROM openbirdmaps.raptor_pair WHERE session_id = 281;
