BEGIN;

-- gyujtokodok kitoltese
BEGIN;
WITH rows AS ( 
    SELECT m.obm_id, g.name 
    FROM milvus m 
    LEFT JOIN shared.romania_administrative_units g ON ST_Intersects(m.obm_geometry, g.geometry_4326) 
    WHERE m.national_program = 'str_national_white_stork_census' AND m.date > '2020-03-01' AND m.national_grouping_code IS NULL AND g.natlevel = '2ndOrder' ORDER BY obm_id DESC
) 
UPDATE milvus m SET national_grouping_code = rows.name 
FROM rows 
WHERE rows.obm_id = m.obm_id;
COMMIT;

--alapegyseg kitoltese
WITH rows AS ( 
    SELECT m.obm_id, g.name 
    FROM milvus m 
    LEFT JOIN shared.romania_administrative_units g ON ST_Intersects(m.obm_geometry, g.geometry_4326) 
    WHERE m.national_program = 'str_national_white_stork_census' AND m.date > '2020-06-01' AND m.national_sampling_unit IS NULL AND g.natlevel = '3rdOrder' ORDER BY obm_id DESC
) 
UPDATE milvus m SET national_sampling_unit = rows.name 
FROM rows 
WHERE rows.obm_id = m.obm_id;

--ellenorzes
SELECT observers, national_program, national_grouping_code, national_sampling_unit FROM milvus WHERE national_program = 'str_national_white_stork_census' AND date > '2020-06-01';

--commitra kell cserelni, ha minden rendben
ROLLBACK;


WITH pairs as (
    SELECT 
        obm_id as pair_id, 
        grouping_code, 
        species, 
        dtype, 
        data 
    FROM milvus_metadata 
    WHERE 
        project = 'POIM Național Păsări 2020-2021' AND
        program = 'str_raptor_monitoring' AND 
        grouping_code IN ($NfLzkWsYMKcTJbZo$PL80$NfLzkWsYMKcTJbZo$,$ZywFLKvGhSDejaHT$EQ59$ZywFLKvGhSDejaHT$) AND 
        dtype = 'pair_connect' AND data->>'pair_status' IN ('str_certain_pair', 'str_possible_pair')
), 
doubles AS (
    SELECT 
        obm_id as double_id, 
        grouping_code, 
        species, 
        d.value::integer as pair_id, 
        data->>'double_status' as double_status, 
        data->>'max_pairs' as max_pairs 
    FROM milvus_metadata, jsonb_array_elements(milvus_metadata.data->'obm_ids') as d 
    WHERE 
        project = 'POIM Național Păsări 2020-2021' AND
        program = 'str_raptor_monitoring' AND 
        grouping_code IN ($NfLzkWsYMKcTJbZo$PL80$NfLzkWsYMKcTJbZo$,$ZywFLKvGhSDejaHT$EQ59$ZywFLKvGhSDejaHT$) AND 
        dtype = 'double_pairs'
), 
not_doubles as (
    SELECT 
        pairs.grouping_code, 
        pairs.species, 
        count(*) as min, 
        count(*) as max 
    FROM pairs 
    LEFT JOIN doubles on pairs.pair_id = doubles.pair_id 
    WHERE double_id IS NULL 
    GROUP BY pairs.grouping_code, pairs.species)
SELECT * FROM doubles;
