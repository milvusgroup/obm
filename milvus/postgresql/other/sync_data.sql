SELECT max(obm_id) FROM milvus;
-- %s/1663433/1685043/
SELECT max(obm_id) FROM milvus_qgrids;
-- s///
SELECT max(id) FROM system.uploadings;
-- s/682186/708063/

\copy (SELECT * FROM system.uploadings WHERE id > 708063) to '/tmp/uploadings.csv' CSV HEADER;
-- scp /tmp/uploadings.csv boneg@db:/tmp/
\copy (SELECT * FROM milvus.milvus WHERE obm_id > 1685043) to '/tmp/milvus.csv' CSV HEADER;
-- scp /tmp/milvus.csv boneg@db:/tmp/
COPY system.uploadings FROM '/tmp/uploadings.csv' CSV HEADER;
SELECT * FROM milvus WHERE obm_id > 
