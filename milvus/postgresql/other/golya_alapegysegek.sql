
WITH gc AS (
    SELECT
        gykod,
        obm_id
    FROM milvus_grouping_codes
    WHERE program = 14
),
comuna as (
    select name, substring(localid, '1.([0-9]*).[0-9]*')::integer as cod_judet from shared.romania_administrative_units where natlevel = '3rdOrder'
),
judete as (
    select name, natcode from shared.romania_administrative_units where natlevel = '2ndOrder'
)

INSERT INTO milvus_sampling_units (alapegyseg, gykod_id, gykod_nev, program, program_nev, obm_uploading_id, obm_access)
SELECT 
    comuna.name as alapegyseg, 
    gc.obm_id as gykod_id, 
    gc.gykod as gykod_nev,
    14 as program, 
    'str_national_white_stork_census' as program_name, 
    60682 as obm_uploading_id, 
    0 as obm_access
FROM comuna 
LEFT JOIN judete ON comuna.cod_judet = judete.natcode
LEFT JOIN gc ON gc.gykod = judete.name;


