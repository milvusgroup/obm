WITH a as (
    SELECT * FROM milvus WHERE obm_uploading_id = 390695
    ),
b as (
    SELECT * FROM milvus WHERE obm_uploading_id = 390696
    ),
tst as (
    SELECT a.species, b.species, a.date = b.date as test2, st_equals(a.obm_geometry, b.obm_geometry) as test FROM a LEFT JOIN b ON a.obm_id = b.obm_id - 131
    )
SELECT DISTINCT test2 FROM tst;

BEGIN;
DELETE FROM milvus WHERE obm_uploading_id = 390696;
COMMIT;


WITH x as (
    SELECT DISTINCT obm_uploading_id, observers, date, count(*) c FROM milvus m LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id WHERE description != 'API upload' GROUP BY obm_uploading_id, observers, date
    )
SELECT obm_uploading_id, observers, date, c, count(*) FROM x GROUP BY obm_uploading_id, observers, date, c HAVING count(*) > 1;
SELECT DISTINCT description FROM system.uploadings WHERE description LIKE 'AP%';