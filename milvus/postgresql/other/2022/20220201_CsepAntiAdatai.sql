WITH list_ids as (
    select distinct metadata->>'observation_list_id' list_id
    from temporary_tables.milvus_failed_uploads fu 
    LEFT JOIN system.uploadings u ON fu.obm_uploading_id = u.id 
    where observers = 'Csép Antal'
    )
SELECT metadata from system.uploadings WHERE metadata->>'observation_list_id' IN (
    SELECT list_id FROM list_ids
);

select distinct method, national_program, extract(year from date), count(*) from milvus where observers LIKE '%Csép Antal%' AND extract(year from date) = 2022 GROUP BY national_program, extract(year from date), method;