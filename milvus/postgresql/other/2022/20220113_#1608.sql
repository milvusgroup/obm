SELECT 
    projekt, count(*) 
FROM milvus m 
LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id 
WHERE extract(year from date) = 2021 
GROUP BY projekt;

-- OpenBirdMaps
SELECT 
    uploader_name, count(*) c
FROM milvus m 
LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id 
WHERE 
    extract(year from date) = 2021 AND
    (projekt IS NULL OR projekt = 'str_voluntary_scheme')
GROUP BY uploader_name
ORDER BY c DESC;
\copy (SELECT uploader_name, count(*) c FROM milvus m LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id WHERE extract(year from date) = 2021 AND (projekt IS NULL OR projekt = 'str_voluntary_scheme') GROUP BY uploader_name ORDER BY c DESC) to '/tmp/feltoltott_adatok_emberenkent.csv';

-- OpenHerpMaps
SELECT 
    uploader_name, count(*) c
FROM openherpmaps m 
LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id 
WHERE 
    extract(year from data) = 2021
GROUP BY uploader_name
ORDER BY c DESC;

\copy (SELECT uploader_name, count(*) c FROM openherpmaps m LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id WHERE extract(year from data) = 2021 GROUP BY uploader_name ORDER BY c DESC)  to '/tmp/OHM_feltoltott_adatok_emberenkent.csv'

-- OpenMammalMaps
SELECT 
    uploader_name, count(*) c
FROM mammalia m 
LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id 
WHERE 
    extract(year from date) = 2021
GROUP BY uploader_name
ORDER BY c DESC;

\copy (SELECT uploader_name, count(*) c FROM mammalia m LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id WHERE extract(year from date) = 2021 GROUP BY uploader_name ORDER BY c DESC) to '/tmp/OMM_feltoltott_adatok_emberenkent.csv';
