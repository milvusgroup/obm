-- updating missing notes from filenames
BEGIN;
WITH d as (
SELECT 
    obm_id,
    file_id, 
    sampling_unit,
    notes,
    date,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM milvus_fieldwork
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'str_carpathia_restauration_cbm' AND 
    record_type = 'str_photo' AND
    obm_files_id IS NOT NULL AND
    notes IS NULL AND
    reference LIKE '%EST%' AND reference NOT LIKE '%WEST%'
)
UPDATE milvus_fieldwork fw SET notes = 'Est' FROM d WHERE fw.obm_id = d.obm_id;

WITH d as (
SELECT 
    obm_id,
    file_id, 
    sampling_unit,
    notes,
    date,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM milvus_fieldwork
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'str_carpathia_restauration_cbm' AND 
    record_type = 'str_photo' AND
    obm_files_id IS NOT NULL AND
    notes IS NULL AND
    reference LIKE '%WEST%'
)
UPDATE milvus_fieldwork fw SET notes = 'Vest' FROM d WHERE fw.obm_id = d.obm_id;

WITH d as (
SELECT 
    obm_id,
    file_id, 
    sampling_unit,
    notes,
    date,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM milvus_fieldwork
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'str_carpathia_restauration_cbm' AND 
    record_type = 'str_photo' AND
    obm_files_id IS NOT NULL AND
    notes IS NULL AND
    reference LIKE '%NORD%'
)
UPDATE milvus_fieldwork fw SET notes = 'Nord' FROM d WHERE fw.obm_id = d.obm_id;

WITH d as (
SELECT 
    obm_id,
    file_id, 
    sampling_unit,
    notes,
    date,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM milvus_fieldwork
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'str_carpathia_restauration_cbm' AND 
    record_type = 'str_photo' AND
    obm_files_id IS NOT NULL AND
    notes IS NULL AND
    reference LIKE '%SUD%'
)
UPDATE milvus_fieldwork fw SET notes = 'Sud' FROM d WHERE fw.obm_id = d.obm_id;;
COMMIT;


-- copying files
WITH d as (
SELECT 
    obm_id,
    file_id, 
    sampling_unit,
    notes,
    date,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM milvus_fieldwork
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'str_carpathia_restauration_cbm' AND 
    record_type = 'str_photo' AND
    obm_files_id IS NOT NULL
)
SELECT 'cp /home/gabor/obm/obm-composer/local/milvus/local/attached_files/' || reference || ' /tmp/carpathia_cbm/' || sampling_unit || '_' || notes || '_' || date || '.jpg' FROM d;

\copy (WITH d as ( SELECT obm_id, file_id, sampling_unit, notes, date, SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x, SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y, reference FROM milvus_fieldwork LEFT JOIN system.file_connect fc ON obm_files_id = conid LEFT JOIN system.files f ON f.id = fc.file_id where program = 'str_carpathia_restauration_cbm' AND record_type = 'str_photo' AND obm_files_id IS NOT NULL ) SELECT 'cp /home/gabor/obm/obm-composer/local/milvus/local/attached_files/' || reference || ' /tmp/carpathia_cbm/' || sampling_unit || '_' || notes || '_' || date || '.jpg' FROM d) to '/tmp/cbm.sh' CSV;


-- 
WITH d as (
SELECT 
    obm_id,
    file_id, 
    sampling_unit,
    notes,
    date,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM milvus_fieldwork
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'str_carpathia_restauration_cbm' AND 
    record_type = 'str_photo' AND
    obm_files_id IS NOT NULL
)
SELECT DISTINCT sampling_unit, notes, date, count(*) FROM d GROUP BY sampling_unit, notes, date HAVING count(*) > 1;
