SELECT  
            pt.obm_id,
            u.uploading_date::date,
            CASE 
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || '_' || g.etrs10_name
                ELSE 
                    CASE 
                        WHEN pt.list_id IS NULL THEN pt.obm_uploading_id || '_' || pt.method 
                        ELSE pt.obm_uploading_id || '.' || pt.list_id || '_' || pt.method 
                    END
            END as event_id,
            date,
            s.observers_ids AS observer_ids_array,
            CASE
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || '_' || g.etrs10_name || '_' || tx.ebp_id
                ELSE CASE 
                    WHEN pt.list_id IS NULL
                        THEN pt.obm_uploading_id || '_' || pt.method || '_' || tx.ebp_id 
                        ELSE pt.obm_uploading_id || '.' || pt.list_id || '_' || pt.method || '_' || tx.ebp_id  
                    END
            END as record_id
            
        FROM public.milvus pt 
        LEFT JOIN (
            SELECT * FROM system.uploadings up 
            LEFT JOIN (SELECT term_id AS observer_id, term AS observer_name FROM public.milvus_terms WHERE subject = 'observers') as fooo ON observer_name = uploader_name
        ) u ON pt.obm_uploading_id = u.id 
        LEFT JOIN ebp.protocol_connect pc 
            ON (
                method = obm_method AND 
                (national_program = obm_national_program OR (national_program IS NULL AND obm_national_program IS NULL)) AND 
                (local_program = obm_local_program OR (local_program IS NULL AND obm_local_program IS NULL))
            ) 
        LEFT JOIN ebp.breeding_codes_connect bc ON pt.status_of_individuals = bc.local_code
        LEFT JOIN public.milvus_qgrids g ON g.data_table = 'milvus' AND pt.obm_id = g.row_id
        LEFT JOIN system.milvus_linnaeus s ON pt.obm_id = s.row_id
        LEFT JOIN (SELECT taxon_id, ebp_id, word FROM public.milvus_taxon tax LEFT JOIN ebp.species ebps ON tax.taxon_id = ebps.local_id) as tx 
            ON tx.word = pt.species 
        WHERE 
            protocol_code IS DISTINCT FROM 'excluded' AND 
            pt.date >= '2010-01-01' AND 
            pt.observed_unit = 'str_individuals' AND 
            method IS NOT NULL AND
            comlete_list IS NOT NULL AND 
            g.etrs10_name IS NOT NULL AND u.uploading_date::date > '2023-02-01' AND u.uploading_date::date <= '2023-03-02'
