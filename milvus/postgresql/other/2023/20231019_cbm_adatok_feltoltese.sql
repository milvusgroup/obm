INSERT INTO milvus (obm_uploading_id, source_id, national_program, method, observers, date, obm_access, wind_force, exact_time, species, number, gender, comments_observation, national_grouping_code, national_sampling_unit, comments_general, distance_m, projekt, obm_geometry, surveyed_geometry, status_of_individuals, observed_unit, comlete_list, data_owner, source, duration, distance_code, list_id, time_of_start)
SELECT
839223, source_id::integer, program, method, observers, date::date, access, wind_speed, exact_time::time, species, num, ivar, comment, gykod, su, comment_general, distance_m::numeric, projekt, st_geomfromtext(wkt, 4326), st_geomfromtext(observer_pozition, 4326), status_ind_for, observed_unit, comlete_list, data_owner, sourse, duration, distance_cat, list_id, split_part(kezd, ':', 1)::numeric * 60 + split_part(kezd, ':', 2)::numeric
FROM tmp.cbm2022;

INSERT INTO system.uploadings (uploading_date, uploader_id, uploader_name, description, access, "group", project_table, owner, project, form_id)
VALUES (NOW(), 84, 'Milvus Group', 'Adat felvitel fájlból: ''cbm2022.csv''', 0, '{322,84}', 'milvus', '{84}', 'milvus', 62);



with r  as (
    select distinct term_id, role_id from milvus_terms where subject = 'observers' and role_id is not null and role_id > 0
)
update milvus_terms mr SET role_id = r.role_id from r where mr.subject = 'observers' and mr.term_id = r.term_id AND mr.role_id = 0;




WITH obs_ids as (
    select distinct row_id, unnest(observers_ids) as observer_id from system.milvus_linnaeus left join milvus on row_id = obm_id where obm_uploading_id = 839223
),
role_ids as (
    select distinct l.row_id, t.role_id
    from obs_ids l
    left join milvus_terms t on term_id = observer_id
),
role_ids_agg as (
    SELECT row_id, array_agg(role_id) as rids FROM role_ids WHERE role_id > 0 group by row_id
)
update milvus_rules set read = read || rids from role_ids_agg where milvus_rules.row_id = role_ids_agg.row_id;





select observers
