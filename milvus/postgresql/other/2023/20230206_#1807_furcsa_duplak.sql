-- obm_comment átírása a látható comments_observation mezőbe
BEGIN;
with asdf as (select obm_id, unnest(obm_comments) obmc from milvus where obm_comments is not null) 
UPDATE milvus SET comments_observation = coalesce(comments_observation || ' / ' || obmc, obmc) from asdf WHERE asdf.obm_id = milvus.obm_id;
COMMIT;

-- max számok javítása: a másolatoknál 1-re, az eredetinél a 1 + (max_number - eredeti egyedszam)
SELECT obm_id, number, number_max, obm_comments from milvus WHERE number_max IS NOT NULL AND obm_comments IS NOT NULL AND obm_comments != '{}';
-- LibreOfficeban raktam össze a javítást, mert egyszerűbb volt.
BEGIN;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1049079;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1049080;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049090;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049091;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049155;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049161;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049163;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049162;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049207;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 3 to 1', number_max = 1 WHERE obm_id = 1049209;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 3 to 1', number_max = 1 WHERE obm_id = 1049210;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049208;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1049252;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1049251;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049299;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049387;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1049388;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049440;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049838;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049839;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049837;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049842;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1054236;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1054250;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1054251;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1055818;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1052474;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1039496;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1039503;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049829;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1042231;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1053099;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049836;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1047090;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049828;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 3', number_max = 3 WHERE obm_id = 1038422;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 3', number_max = 3 WHERE obm_id = 1055763;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1049835;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 2 to 1', number_max = 1 WHERE obm_id = 1053096;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 2', number_max = 2 WHERE obm_id = 1034301;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 2', number_max = 2 WHERE obm_id = 1044424;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1040874;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 3 to 1', number_max = 1 WHERE obm_id = 1039497;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 0 to 1', number_max = 1 WHERE obm_id = 1034251;
    UPDATE milvus SET comments_observation = comments_observation || ' / number_max changed from 4 to 1', number_max = 1 WHERE obm_id = 1039223;

