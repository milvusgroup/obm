BEGIN;
    CREATE SCHEMA IF NOT EXISTS milvus;
    ALTER TABLE milvus SET SCHEMA milvus;
    CREATE VIEW public.milvus AS
        SELECT 
    -- COLUMNS FROM THE MAIN TABLE
            d.obm_id,
            d.obm_uploading_id,
            d.obm_validation,
            d.obm_comments,
            d.obm_modifier_id,
            d.date,
            d.date_until,
            d.observers,
            d.method,
            d.time_of_start,
            d.time_of_end,
            d.duration,
            d.comlete_list,
            d.wind_force,
            d.wind_direction,
            d.precipitation,
            d.snow_depth,
            d.visibility,
            d.temperature,
            d.optics_used,
            d.comments_general,
            d.species,
            d.number,
            d.number_max,
            d.observed_unit,
            d.count_precision,
            d.gender,
            d.age,
            d.habitat_state,
            d.obm_geometry,
            d.national_program,
            d.local_program,
            d.colony_type,
            d.colony_place,
            d.colony_survey_method,
            d.disturbance,
            d.atmospheric_pressure,
            d.altitude,
            d.access_to_data,
            d.national_grouping_code,
            d.national_sampling_unit,
            d.ice_cover,
            d.cloud_cover,
            d.wind_speed_kmh,
            d.flight_direction,
            d.cause_of_death,
            d.nest_type,
            d.nest_status,
            d.nest_condition,
            d.distance_m,
            d.distance_code,
            d.habitat_id,
            d.habitat_2,
            d.map_code,
            d.timing_of_reaction,
            d.response_type,
            d.in_flight_sitting,
            d.flight_altitude,
            d.migration_intensity,
            d.migration_type,
            d.secondary_time,
            d.status_of_individuals,
            d.nest_place,
            d.comments_observation,
            d.list_id,
            d.obm_files_id,
            d.local_grouping_code,
            d.local_sampling_unit,
            d.x,
            d.y,
            d.data_owner,
            d.number_of_eggs,
            d.number_of_nestlings,
            d.method_details_species_aimed,
            d.exact_time,
            d.precision_of_count_list,
            d.surveyed_geometry,
            d.source,
            d.source_id,
            d.non_human_observer,
            d.nest_id,
            d.samplin_unit_coverage,
            d.active_abandoned,
            d.type_number_trees,
            d.type_place_roost_colony,
            d.threats,
            d.nestling_age,
            d.nr_dead_egg,
            d.nr_dead_nestling,
            d.projekt,
            d.obm_observation_list_id,
            d.colony_code,
            d.start_time,
            d.end_time,
            d.obs_duration,
            d.pair_id,
            d.obm_access,
            vf2.word as species_valid,
            vf3.word as species_hu,
            vf5.word as species_ro,
            vf4.word as species_en,
            vf6.word as species_euring,
            d.uncertain_identification
    -- COLUMNS FROM THE JOINED TABLE
    -- MAIN TABLE REFERENCE
        FROM milvus.milvus d
    -- JOIN STATEMENT
        LEFT JOIN milvus_taxon vf1 ON d.species::text = vf1.word::text
        LEFT JOIN milvus.milvus_taxon_valid vf2 ON vf1.taxon_id = vf2.taxon_id
        LEFT JOIN milvus.milvus_taxon_hu vf3 ON vf1.taxon_id = vf3.taxon_id
        LEFT JOIN milvus.milvus_taxon_en vf4 ON vf1.taxon_id = vf4.taxon_id
        LEFT JOIN milvus.milvus_taxon_ro vf5 ON vf1.taxon_id = vf5.taxon_id
        LEFT JOIN milvus.milvus_taxon_euring vf6 ON vf1.taxon_id = vf6.taxon_id;
    
    DROP RULE IF EXISTS milvus_ins ON milvus;
    CREATE RULE milvus_ins AS ON INSERT TO public.milvus
        DO INSTEAD
        INSERT INTO milvus.milvus (obm_uploading_id,obm_validation,obm_comments,obm_modifier_id,date,date_until,observers,method,time_of_start,time_of_end,duration,comlete_list,wind_force,wind_direction,precipitation,snow_depth,visibility,temperature,optics_used,comments_general,species,number,number_max,observed_unit,count_precision,gender,age,habitat_state,obm_geometry,national_program,local_program,colony_type,colony_place,colony_survey_method,disturbance,atmospheric_pressure,altitude,access_to_data,national_grouping_code,national_sampling_unit,ice_cover,cloud_cover,wind_speed_kmh,flight_direction,cause_of_death,nest_type,nest_status,nest_condition,distance_m,distance_code,habitat_id,habitat_2,map_code,timing_of_reaction,response_type,in_flight_sitting,flight_altitude,migration_intensity,migration_type,secondary_time,status_of_individuals,nest_place,comments_observation,list_id,obm_files_id,local_grouping_code,local_sampling_unit,x,y,data_owner,number_of_eggs,number_of_nestlings,method_details_species_aimed,exact_time,precision_of_count_list,surveyed_geometry,source,source_id,non_human_observer,nest_id,samplin_unit_coverage,active_abandoned,type_number_trees,type_place_roost_colony,threats,nestling_age,nr_dead_egg,nr_dead_nestling,projekt,obm_observation_list_id,colony_code,start_time,end_time,obs_duration,pair_id,obm_access,uncertain_identification) 
        VALUES (NEW.obm_uploading_id,NEW.obm_validation,NEW.obm_comments,NEW.obm_modifier_id,NEW.date,NEW.date_until,NEW.observers,NEW.method,NEW.time_of_start,NEW.time_of_end,NEW.duration,NEW.comlete_list,NEW.wind_force,NEW.wind_direction,NEW.precipitation,NEW.snow_depth,NEW.visibility,NEW.temperature,NEW.optics_used,NEW.comments_general,NEW.species,NEW.number,NEW.number_max,NEW.observed_unit,NEW.count_precision,NEW.gender,NEW.age,NEW.habitat_state,NEW.obm_geometry,NEW.national_program,NEW.local_program,NEW.colony_type,NEW.colony_place,NEW.colony_survey_method,NEW.disturbance,NEW.atmospheric_pressure,NEW.altitude,NEW.access_to_data,NEW.national_grouping_code,NEW.national_sampling_unit,NEW.ice_cover,NEW.cloud_cover,NEW.wind_speed_kmh,NEW.flight_direction,NEW.cause_of_death,NEW.nest_type,NEW.nest_status,NEW.nest_condition,NEW.distance_m,NEW.distance_code,NEW.habitat_id,NEW.habitat_2,NEW.map_code,NEW.timing_of_reaction,NEW.response_type,NEW.in_flight_sitting,NEW.flight_altitude,NEW.migration_intensity,NEW.migration_type,NEW.secondary_time,NEW.status_of_individuals,NEW.nest_place,NEW.comments_observation,NEW.list_id,NEW.obm_files_id,NEW.local_grouping_code,NEW.local_sampling_unit,NEW.x,NEW.y,NEW.data_owner,NEW.number_of_eggs,NEW.number_of_nestlings,NEW.method_details_species_aimed,NEW.exact_time,NEW.precision_of_count_list,NEW.surveyed_geometry,NEW.source,NEW.source_id,NEW.non_human_observer,NEW.nest_id,NEW.samplin_unit_coverage,NEW.active_abandoned,NEW.type_number_trees,NEW.type_place_roost_colony,NEW.threats,NEW.nestling_age,NEW.nr_dead_egg,NEW.nr_dead_nestling,NEW.projekt,NEW.obm_observation_list_id,NEW.colony_code,NEW.start_time,NEW.end_time,NEW.obs_duration,NEW.pair_id,NEW.obm_access,NEW.uncertain_identification) RETURNING
         obm_id::int,obm_uploading_id,obm_validation,obm_comments,obm_modifier_id,date,date_until,observers,method,time_of_start,time_of_end,duration,comlete_list,wind_force,wind_direction,precipitation,snow_depth,visibility,temperature,optics_used,comments_general,species,number,number_max,observed_unit,count_precision,gender,age,habitat_state,obm_geometry,national_program,local_program,colony_type,colony_place,colony_survey_method,disturbance,atmospheric_pressure,altitude,access_to_data,national_grouping_code,national_sampling_unit,ice_cover,cloud_cover,wind_speed_kmh,flight_direction,cause_of_death,nest_type,nest_status,nest_condition,distance_m,distance_code,habitat_id,habitat_2,map_code,timing_of_reaction,response_type,in_flight_sitting,flight_altitude,migration_intensity,migration_type,secondary_time,status_of_individuals,nest_place,comments_observation,list_id,obm_files_id,local_grouping_code,local_sampling_unit,x,y,data_owner,number_of_eggs,number_of_nestlings,method_details_species_aimed,exact_time,precision_of_count_list,surveyed_geometry,source,source_id,non_human_observer,nest_id,samplin_unit_coverage,active_abandoned,type_number_trees,type_place_roost_colony,threats,nestling_age,nr_dead_egg,nr_dead_nestling,projekt,obm_observation_list_id,colony_code,start_time,end_time,obs_duration,pair_id,obm_access,NULL::character varying,NULL::character varying,NULL::character varying,NULL::character varying,NULL::character varying,uncertain_identification;

    DROP RULE IF EXISTS milvus_upd ON milvus;
    CREATE RULE milvus_upd AS ON UPDATE TO public.milvus
        DO INSTEAD
        UPDATE milvus.milvus SET
            obm_uploading_id = NEW.obm_uploading_id,obm_validation = NEW.obm_validation,obm_comments = NEW.obm_comments,obm_modifier_id = NEW.obm_modifier_id,date = NEW.date,date_until = NEW.date_until,observers = NEW.observers,method = NEW.method,time_of_start = NEW.time_of_start,time_of_end = NEW.time_of_end,duration = NEW.duration,comlete_list = NEW.comlete_list,wind_force = NEW.wind_force,wind_direction = NEW.wind_direction,precipitation = NEW.precipitation,snow_depth = NEW.snow_depth,visibility = NEW.visibility,temperature = NEW.temperature,optics_used = NEW.optics_used,comments_general = NEW.comments_general,species = NEW.species,number = NEW.number,number_max = NEW.number_max,observed_unit = NEW.observed_unit,count_precision = NEW.count_precision,gender = NEW.gender,age = NEW.age,habitat_state = NEW.habitat_state,obm_geometry = NEW.obm_geometry,national_program = NEW.national_program,local_program = NEW.local_program,colony_type = NEW.colony_type,colony_place = NEW.colony_place,colony_survey_method = NEW.colony_survey_method,disturbance = NEW.disturbance,atmospheric_pressure = NEW.atmospheric_pressure,altitude = NEW.altitude,access_to_data = NEW.access_to_data,national_grouping_code = NEW.national_grouping_code,national_sampling_unit = NEW.national_sampling_unit,ice_cover = NEW.ice_cover,cloud_cover = NEW.cloud_cover,wind_speed_kmh = NEW.wind_speed_kmh,flight_direction = NEW.flight_direction,cause_of_death = NEW.cause_of_death,nest_type = NEW.nest_type,nest_status = NEW.nest_status,nest_condition = NEW.nest_condition,distance_m = NEW.distance_m,distance_code = NEW.distance_code,habitat_id = NEW.habitat_id,habitat_2 = NEW.habitat_2,map_code = NEW.map_code,timing_of_reaction = NEW.timing_of_reaction,response_type = NEW.response_type,in_flight_sitting = NEW.in_flight_sitting,flight_altitude = NEW.flight_altitude,migration_intensity = NEW.migration_intensity,migration_type = NEW.migration_type,secondary_time = NEW.secondary_time,status_of_individuals = NEW.status_of_individuals,nest_place = NEW.nest_place,comments_observation = NEW.comments_observation,list_id = NEW.list_id,obm_files_id = NEW.obm_files_id,local_grouping_code = NEW.local_grouping_code,local_sampling_unit = NEW.local_sampling_unit,x = NEW.x,y = NEW.y,data_owner = NEW.data_owner,number_of_eggs = NEW.number_of_eggs,number_of_nestlings = NEW.number_of_nestlings,method_details_species_aimed = NEW.method_details_species_aimed,exact_time = NEW.exact_time,precision_of_count_list = NEW.precision_of_count_list,surveyed_geometry = NEW.surveyed_geometry,source = NEW.source,source_id = NEW.source_id,non_human_observer = NEW.non_human_observer,nest_id = NEW.nest_id,samplin_unit_coverage = NEW.samplin_unit_coverage,active_abandoned = NEW.active_abandoned,type_number_trees = NEW.type_number_trees,type_place_roost_colony = NEW.type_place_roost_colony,threats = NEW.threats,nestling_age = NEW.nestling_age,nr_dead_egg = NEW.nr_dead_egg,nr_dead_nestling = NEW.nr_dead_nestling,projekt = NEW.projekt,obm_observation_list_id = NEW.obm_observation_list_id,colony_code = NEW.colony_code,start_time = NEW.start_time,end_time = NEW.end_time,obs_duration = NEW.obs_duration,pair_id = NEW.pair_id,obm_access = NEW.obm_access,uncertain_identification = NEW.uncertain_identification 
    WHERE
         obm_id = NEW.obm_id;
    
    DROP RULE IF EXISTS milvus_del ON milvus;
    CREATE RULE milvus_del AS ON DELETE TO public.milvus
        DO INSTEAD
        DELETE FROM milvus.milvus 
        WHERE
         obm_id = OLD.obm_id;
    
    ALTER SCHEMA milvus OWNER TO milvus_admin;
    ALTER VIEW public.milvus OWNER TO milvus_admin;
COMMIT;

BEGIN;

    DROP RULE IF EXISTS milvus_upd ON milvus;
    ALTER TABLE milvus.milvus ALTER COLUMN obm_access SET NULL;
    CREATE RULE milvus_upd AS ON UPDATE TO public.milvus
        DO INSTEAD
        UPDATE milvus.milvus SET
            obm_uploading_id = NEW.obm_uploading_id,obm_validation = NEW.obm_validation,obm_comments = NEW.obm_comments,obm_modifier_id = NEW.obm_modifier_id,date = NEW.date,date_until = NEW.date_until,observers = NEW.observers,method = NEW.method,time_of_start = NEW.time_of_start,time_of_end = NEW.time_of_end,duration = NEW.duration,comlete_list = NEW.comlete_list,wind_force = NEW.wind_force,wind_direction = NEW.wind_direction,precipitation = NEW.precipitation,snow_depth = NEW.snow_depth,visibility = NEW.visibility,temperature = NEW.temperature,optics_used = NEW.optics_used,comments_general = NEW.comments_general,species = NEW.species,number = NEW.number,number_max = NEW.number_max,observed_unit = NEW.observed_unit,count_precision = NEW.count_precision,gender = NEW.gender,age = NEW.age,habitat_state = NEW.habitat_state,obm_geometry = NEW.obm_geometry,national_program = NEW.national_program,local_program = NEW.local_program,colony_type = NEW.colony_type,colony_place = NEW.colony_place,colony_survey_method = NEW.colony_survey_method,disturbance = NEW.disturbance,atmospheric_pressure = NEW.atmospheric_pressure,altitude = NEW.altitude,access_to_data = NEW.access_to_data,national_grouping_code = NEW.national_grouping_code,national_sampling_unit = NEW.national_sampling_unit,ice_cover = NEW.ice_cover,cloud_cover = NEW.cloud_cover,wind_speed_kmh = NEW.wind_speed_kmh,flight_direction = NEW.flight_direction,cause_of_death = NEW.cause_of_death,nest_type = NEW.nest_type,nest_status = NEW.nest_status,nest_condition = NEW.nest_condition,distance_m = NEW.distance_m,distance_code = NEW.distance_code,habitat_id = NEW.habitat_id,habitat_2 = NEW.habitat_2,map_code = NEW.map_code,timing_of_reaction = NEW.timing_of_reaction,response_type = NEW.response_type,in_flight_sitting = NEW.in_flight_sitting,flight_altitude = NEW.flight_altitude,migration_intensity = NEW.migration_intensity,migration_type = NEW.migration_type,secondary_time = NEW.secondary_time,status_of_individuals = NEW.status_of_individuals,nest_place = NEW.nest_place,comments_observation = NEW.comments_observation,list_id = NEW.list_id,obm_files_id = NEW.obm_files_id,local_grouping_code = NEW.local_grouping_code,local_sampling_unit = NEW.local_sampling_unit,x = NEW.x,y = NEW.y,data_owner = NEW.data_owner,number_of_eggs = NEW.number_of_eggs,number_of_nestlings = NEW.number_of_nestlings,method_details_species_aimed = NEW.method_details_species_aimed,exact_time = NEW.exact_time,precision_of_count_list = NEW.precision_of_count_list,surveyed_geometry = NEW.surveyed_geometry,source = NEW.source,source_id = NEW.source_id,non_human_observer = NEW.non_human_observer,nest_id = NEW.nest_id,samplin_unit_coverage = NEW.samplin_unit_coverage,active_abandoned = NEW.active_abandoned,type_number_trees = NEW.type_number_trees,type_place_roost_colony = NEW.type_place_roost_colony,threats = NEW.threats,nestling_age = NEW.nestling_age,nr_dead_egg = NEW.nr_dead_egg,nr_dead_nestling = NEW.nr_dead_nestling,projekt = NEW.projekt,obm_observation_list_id = NEW.obm_observation_list_id,colony_code = NEW.colony_code,start_time = NEW.start_time,end_time = NEW.end_time,obs_duration = NEW.obs_duration,pair_id = NEW.pair_id,obm_access = NEW.obm_access,uncertain_identification = NEW.uncertain_identification 
    WHERE
         obm_id = NEW.obm_id;
