-- create table tmp.new_rau as (
--        select id, name, natlevel, judet, (st_dump(geometry)).*
--        from shared.romania_administrative_units
-- );
-- create index tmp.new_rau_geom_gist on tmp.new_rau using gist(geom);

-- adds the new geom column that will contain simplified geoms
-- alter table tmp.new_rau add column simple_geom geometry(POLYGON, 31700);

-- select CreateTopology('topo1',31700,0);

-- select ST_CreateTopoGeo('topo1',ST_Collect(geometry)) from shared.romania_administrative_units WHERE natlevel = '2ndOrder';

CREATE OR REPLACE FUNCTION simplify_administrative_units(jdt varchar) RETURNS void AS $$
BEGIN
        execute format('SELECT CreateTopology(%L,31700,0);', 'topo1');
        execute format('SELECT ST_CreateTopoGeo(%L,ST_MakeValid(ST_Collect(geometry))) from shared.romania_administrative_units WHERE natlevel = ''3rdOrder'' AND judet = %L', 'topo1', jdt);
        execute format('SELECT CreateTopology(%L,31700,0);', 'topo2');
        execute format('select ST_CreateTopoGeo(%L, geom)
            from (
                select st_makevalid(ST_Collect(st_simplifyPreserveTopology(geom, 250))) as geom from %s.edge_data
            ) as foo;','topo2', 'topo1');

        execute format('with simple_face as (
               select st_transform(st_getFaceGeometry(%L, face_id),4326) as geom
               from %s.face
               where face_id > 0
            ) update shared.romania_administrative_units d set simple_geom = sf.geom
            from simple_face sf
            where 
                d.natlevel = ''3rdOrder'' AND judet = %L AND
                st_intersects(st_centroid(d.geometry_4326), sf.geom)', 'topo2', 'topo2', jdt);
        execute format('select dropTopology(%L);', 'topo1');
        execute format('select dropTopology(%L);', 'topo2');
END
$$ LANGUAGE plpgsql;


select simplify_administrative_units('ILFOV');
select simplify_administrative_units('TELEORMAN');
select simplify_administrative_units('VRANCEA');
select simplify_administrative_units('BRAILA');
select simplify_administrative_units('MURES');
select simplify_administrative_units('IASI');

select simplify_administrative_units('BRASOV');
select simplify_administrative_units('CALARASI');
select simplify_administrative_units('COVASNA');
select simplify_administrative_units('DÂMBOVITA');
select simplify_administrative_units('HARGHITA');
select simplify_administrative_units('GORJ');
select simplify_administrative_units('SUCEAVA');
select simplify_administrative_units('GALATI');
select simplify_administrative_units('TULCEA');
SELECT ST_CreateTopoGeo('tulcea_topo1',st_makevalid(ST_Collect(geometry))) from shared.romania_administrative_units WHERE natlevel = '3rdOrder' AND judet = 'TULCEA' AND id IN ( 995,949,3081,2621,3051,2622,3036,3037,3045,3046,3050,3038,3039,3040,3053,3054,3055,3073,3052,3032,3033,3034,3035,3058,3059,3060, 3061,3062,3063,3028,3029,3030,3031,3064,3065,3066,3067,3041, 3042,3043,3044,3071,3047,3048,3049, 3056,3057,3082, 3083,3084, 3102);
SELECT ST_CreateTopoGeo('tulcea_topo1',st_makevalid(ST_Collect(geometry))) from shared.romania_administrative_units WHERE natlevel = '3rdOrder' AND judet = 'TULCEA' AND id IN ( 995,949,2621,3051,2622,3036,3037,3045,3046,3050,3038,3039,3040,3053,3054,3055,3073,3052,3032,3033,3034,3035,3058,3059,3060, 3061,3062,3063,3028,3029,3030,3031,3064,3065,3066,3067,3041, 3042,3043,3044,3071,3047,3048,3049, 3056,3057,3082, 3083,3084, 3102);

select simplify_administrative_units('CARAS-SEVERIN');
select simplify_administrative_units('BUCURESTI');
select simplify_administrative_units('ARGES');
select simplify_administrative_units('TIMIS');
select simplify_administrative_units('CONSTANTA');
select simplify_administrative_units('OLT');
select simplify_administrative_units('NEAMT');
select simplify_administrative_units('IALOMITA');
select simplify_administrative_units('SATU-MARE');
select simplify_administrative_units('SALAJ');
select simplify_administrative_units('MEHEDINTI');
select simplify_administrative_units('CLUJ');
select simplify_administrative_units('ALBA');
select simplify_administrative_units('VÂLCEA');
select simplify_administrative_units('BOTOSANI');
select simplify_administrative_units('BUZAU');
select simplify_administrative_units('BIHOR');
select simplify_administrative_units('SIBIU');
select simplify_administrative_units('MARAMURES');
select simplify_administrative_units('DOLJ');
select simplify_administrative_units('HUNEDOARA');
select simplify_administrative_units('VASLUI');
select simplify_administrative_units('GIURGIU');
select simplify_administrative_units('ARAD');
select simplify_administrative_units('PRAHOVA');
select simplify_administrative_units('BACAU');

select simplify_administrative_units('BISTRITA-NASAUD');
