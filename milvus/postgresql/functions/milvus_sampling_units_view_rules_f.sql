CREATE OR REPLACE FUNCTION "milvus_sampling_units_view_rules_f" () RETURNS trigger LANGUAGE plpgsql AS $$

DECLARE

 tbl varchar;
 sens integer DEFAULT 0;

BEGIN

        tbl := 'sampling_units_view';
        IF tg_op = 'INSERT' THEN

            sens := new."obm_access";


            execute format('INSERT INTO milvus_rules ("data_table","row_id","write","read","sensitivity") 
                            SELECT ''milvus_%s'',%L,ARRAY[uploader_id],%L,%L FROM uploadings WHERE id = %L',tbl,new.obm_id,(SELECT uploader_id || "group" FROM uploadings WHERE id = new.obm_uploading_id),sens,new.obm_uploading_id);

            RETURN new; 

        ELSIF tg_op = 'UPDATE' AND OLD."obm_access" != NEW."obm_access" THEN

            sens := new."obm_access";
        
            execute format('UPDATE milvus_rules SET sensitivity = %L WHERE data_table = ''milvus_%s'' AND row_id = %L',sens,tbl,NEW.obm_id);

            RETURN new; 

        ELSIF tg_op = 'DELETE' THEN

            execute format('DELETE FROM milvus_rules WHERE data_table = ''milvus_%s'' AND row_id = %L',tbl,old.obm_id);
            RETURN old;

        ELSE

            RETURN new;

        END IF;

END; 
$$;

