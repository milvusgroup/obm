CREATE OR REPLACE FUNCTION milvus_missing_obm_observation_list_id () RETURNS void LANGUAGE plpgsql AS $$
BEGIN
    WITH list as (
        SELECT DISTINCT 
            obm_uploading_id,
            list_id,
            CASE WHEN list_id IS NULL THEN obm_uploading_id::varchar ELSE obm_uploading_id || '_' || list_id END as obmolid
        FROM milvus m
        LEFT JOIN system.uploadings u ON m.obm_uploading_id = u.id
        WHERE 
            obm_observation_list_id IS NULL AND 
            comlete_list = 1 AND
            (u.description != 'API upload' OR u.description IS NULL)
    )
    UPDATE milvus m
    SET obm_observation_list_id = obmolid
    FROM list
    WHERE 
        m.obm_observation_list_id IS NULL AND 
        comlete_list = 1 AND
        m.obm_uploading_id = list.obm_uploading_id AND
        m.list_id IS NOT DISTINCT FROM list.list_id;
END;
$$;
