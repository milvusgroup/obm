CREATE OR REPLACE FUNCTION "update_milvus_taxonlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
tbl varchar(64) = TG_TABLE_NAME;
BEGIN
    IF tg_op = 'INSERT' THEN
        PERFORM add_term(NEW.species, tbl, 'species','milvus','taxon'::varchar);
        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  OLD.species != NEW.species THEN
            PERFORM add_term(NEW.species, tbl, 'species','milvus','taxon'::varchar);
        END IF;

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        PERFORM remove_term(OLD.species, tbl, 'species','milvus','taxon');

        RETURN OLD;

    END IF;                                                                                         
END $$;
