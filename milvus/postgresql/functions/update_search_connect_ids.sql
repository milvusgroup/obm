CREATE OR REPLACE FUNCTION "update_search_connect_ids" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    term varchar(32);
BEGIN
    IF tg_op = 'UPDATE' AND NEW.search_id != OLD.search_id THEN
        EXECUTE format( 'UPDATE milvus_search_connect SET %1$s_ids = array_replace(%1$s_ids,%2$L,%3$L) WHERE %1$s_ids @> ARRAY[%2$s];', NEW.subject, OLD.search_id, NEW.search_id);
    END IF;
    RETURN NEW;
END;
$$;
