CREATE OR REPLACE FUNCTION "public"."milvus_general_rules_f" () RETURNS trigger AS
$$

DECLARE

 tbl varchar;
 project varchar;
 sens integer DEFAULT 0;
 upl RECORD;
 
BEGIN
     project := 'milvus';
     tbl := TG_TABLE_NAME;

     
     IF (TG_OP = 'DELETE') THEN
         EXECUTE format('DELETE FROM %s_rules WHERE data_table = %L AND row_id = %L', project, tbl, old.obm_id);
         
         RETURN OLD;

     ELSIF (TG_OP = 'UPDATE') THEN 
        IF (OLD."obm_access" != NEW."obm_access") THEN
         sens := new.obm_access;
         EXECUTE format('UPDATE %s_rules SET sensitivity = %L WHERE data_table = %L AND row_id = %L', project, sens, tbl, NEW.obm_id);
        END IF;
         RETURN NEW;
         
     ELSIF (TG_OP = 'INSERT') THEN
         sens := new.obm_access;
        SELECT "group" as rd, owner as wr, CASE WHEN array_position(owner, 322) IS NULL THEN owner || 322 ELSE owner END as dl INTO upl FROM system.uploadings WHERE uploadings.id=NEW.obm_uploading_id;
         EXECUTE format('INSERT INTO %s_rules (data_table,row_id,read,write,download,sensitivity) VALUES (%L, %L, %L, %L, %L, %L);', project, tbl, NEW.obm_id, upl.rd, upl.wr, upl.dl, sens);
         
         RETURN NEW;
     END IF;
 END

$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;


