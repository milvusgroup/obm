CREATE OR REPLACE FUNCTION system.observations_milvus()
RETURNS TRIGGER AS
$$
DECLARE
   oid character varying(40);
   list_id integer;
BEGIN
    IF tg_op = 'INSERT' THEN
        oid := new.metadata->'observation_list_id';
        IF (oid IS NOT NULL) THEN
            SELECT obm_observation_list_id INTO list_id FROM milvus_observation_list WHERE oidl = oid LIMIT 1;
            IF list_id IS NULL THEN
                list_id := nextval('milvus_obm_observation_list_id');
            INSERT INTO milvus_observation_list (oidl,uploading_id,obsstart,obsend,nulllist,started_at,finished_at, obm_observation_list_id)
                SELECT new.metadata->>'observation_list_id',new.id,to_timestamp((new.metadata->>'observation_list_start')::bigint/1000),to_timestamp((new.metadata->>'observation_list_end')::bigint/1000),(new.metadata->>'observation_list_null_record')::boolean,to_timestamp((new.metadata->>'started_at')::bigint/1000),to_timestamp((new.metadata->>'finished_at')::bigint/1000, list_id);
        END IF;
        RETURN new;
    END IF;
    IF tg_op = 'UPDATE' THEN
        oid := new.metadata->'observation_list_id';
        IF (oid IS NOT NULL) THEN
            INSERT INTO milvus_observation_list (oidl,uploading_id,obsstart,obsend,nulllist,started_at,finished_at)
                SELECT new.metadata->>'observation_list_id',new.id,to_timestamp((new.metadata->>'observation_list_start')::bigint/1000),to_timestamp((new.metadata->>'observation_list_end')::bigint/1000),(new.metadata->>'observation_list_null_record')::boolean,to_timestamp((new.metadata->>'started_at')::bigint/1000),to_timestamp((new.metadata->>'finished_at')::bigint/1000);
        END IF;
        RETURN new;
    END IF;
END
$$
LANGUAGE plpgsql;
