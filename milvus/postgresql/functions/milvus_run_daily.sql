CREATE OR REPLACE FUNCTION milvus_run_daily () RETURNS void LANGUAGE plpgsql AS $$
DECLARE r record;
BEGIN
    -- megfigyelők gyakoriságának számolása a linnaeus tábla alapján
    ALTER TABLE milvus_terms DISABLE TRIGGER milvus_l_sync_terms_linnaeus_tr ;
    UPDATE milvus_terms s SET taxon_db = foo.c FROM ( SELECT unnest(observers_ids) as observer_id, count(*) as c FROM system.milvus_linnaeus GROUP BY observer_id) foo WHERE term_id = foo.observer_id AND s.data_table = 'milvus' AND s.subject = 'observers' AND s.status != 'misspelled';
    ALTER TABLE milvus_terms ENABLE TRIGGER milvus_l_sync_terms_linnaeus_tr ;

    -- observation list id automatikus kitöltése abban az esetben, amikor ez üres. Pl. fájl vagy webes feltöltéskor
    ALTER TABLE milvus.milvus DISABLE TRIGGER milvus_linnaeus_sync_rows; 
    PERFORM milvus_missing_obm_observation_list_id();
    ALTER TABLE milvus.milvus ENABLE TRIGGER milvus_linnaeus_sync_rows; 

    -- hibás listák ellenőrzéséhez kialakított viewk lásd: https://redmine.milvus.ro/issues/1542
    REFRESH MATERIALIZED VIEW openbirdmaps.uploadings_long_lists;
    REFRESH MATERIALIZED VIEW openbirdmaps.uploadings_old_lists;

    DELETE FROM milvus WHERE species_valid = 'Test data';

    FOR r IN SELECT DISTINCT observers, date FROM temporary_tables.milvus_obm_obsl
    LOOP
        EXECUTE 'SELECT process_left_behind_obslist_parts(' || quote_literal(r.observers) || ',' || quote_literal(r.date) || ')';
    END LOOP;
    
END;
$$;
