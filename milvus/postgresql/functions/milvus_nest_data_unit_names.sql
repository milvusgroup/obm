CREATE OR REPLACE FUNCTION "milvus_nest_data_unit_names" () RETURNS trigger AS 
$$
DECLARE
	program varchar(128);
	gykod varchar(128); 
	alapegyseg varchar(128);

BEGIN

    IF tg_op = 'INSERT' THEN
		IF NEW.program_id IS NOT NULL THEN
			EXECUTE format('SELECT nev FROM milvus_program WHERE obm_id = %L',NEW.program_id) INTO NEW.program;
		END IF;

		IF NEW.gc_id IS NOT NULL THEN
			EXECUTE format('SELECT gykod FROM milvus_grouping_codes WHERE obm_id = %L',NEW.gc_id) INTO NEW.grouping_code;
		END IF;

		IF NEW.su_id IS NOT NULL THEN
			EXECUTE format('SELECT alapegyseg FROM milvus_sampling_units WHERE obm_id = %L',NEW.su_id) INTO NEW.sampling_unit;
		END IF;

        RETURN new; 

    ELSIF tg_op = 'UPDATE' THEN 
		IF NEW.program_id IS DISTINCT FROM OLD.program_id THEN
			EXECUTE format('SELECT nev FROM milvus_program WHERE obm_id = %L',NEW.program_id) INTO NEW.program;
		END IF;

		IF NEW.gc_id IS DISTINCT FROM OLD.gc_id THEN
			EXECUTE format('SELECT gykod FROM milvus_grouping_codes WHERE obm_id = %L',NEW.gc_id) INTO NEW.grouping_code;
		END IF;

		IF NEW.su_id IS DISTINCT FROM OLD.su_id THEN
			EXECUTE format('SELECT alapegyseg FROM milvus_sampling_units WHERE obm_id = %L',NEW.su_id) INTO NEW.sampling_unit;
		END IF;

        RETURN new; 

    ELSIF tg_op = 'DELETE' THEN

        RETURN old;

    ELSE

        RETURN new;

    END IF;

END $$ 
LANGUAGE plpgsql 

