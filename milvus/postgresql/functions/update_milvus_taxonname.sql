CREATE OR REPLACE FUNCTION "update_milvus_taxonname" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF tg_op = 'UPDATE' AND new.word != old.word AND new.lang != 'species' 
    THEN
      EXECUTE FORMAT('
        UPDATE milvus 
        SET 
          %1$I = %2$L, 
          obm_modifier_id = %3$L 
        WHERE milvus.species IN (
          SELECT word FROM milvus_taxon WHERE taxon_id = %4$L
        );', new.lang, new.word, new.modifier_id, new.taxon_id);
    END IF; 
    RETURN new; 
END $$;

-- DROP FUNCTION update_milvus_taxonname ;
