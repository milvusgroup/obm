CREATE OR REPLACE FUNCTION "update_milvus_search_obm_id" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF tg_op = 'INSERT' THEN
        new.obm_id = new.search_id;

        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        new.obm_id = new.search_id;

        RETURN NEW;

    ELSE
        RETURN NULL;

    END IF;
END
$$;
