CREATE OR REPLACE FUNCTION "milvus_refresh_list_matviews" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW openbirdmaps.uploadings_long_lists;
    REFRESH MATERIALIZED VIEW openbirdmaps.uploadings_old_lists;
    RETURN NULL;
END
$$;
