CREATE FUNCTION public.requestebpdata(prov_mode character varying, upl_from date DEFAULT '1741-09-22'::date, upl_until date DEFAULT '1811-09-08'::date, year integer DEFAULT NULL::integer, month integer DEFAULT NULL::integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
    retval integer; 
    wh varchar;
BEGIN
    IF prov_mode = 'standard' THEN
        IF upl_from = '1741-09-22' OR upl_until = '1811-09-08'
            THEN RETURN NULL;
        END IF;
        wh := format('u.uploading_date::date > %L AND u.uploading_date::date <= %L',upl_from, upl_until);
    ELSIF prov_mode = 'bulk' THEN
        IF year IS NULL OR month IS NULL
            THEN RETURN NULL;
        END IF;
        wh := format('extract(''year'' FROM pt.date) = %L AND extract(''month'' FROM pt.date) = %L',year, month);
        EXECUTE format('DELETE FROM ebp.records_raw pt WHERE %s;', wh);
    END IF;

EXECUTE 'WITH rows AS (
    INSERT INTO ebp.records_raw
        SELECT  
            pt.obm_id,
            u.uploading_date::date,
            COALESCE(protocol_code, '''') as protocol_id,
            CASE 
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || ''_'' || g.etrs10_name
                ELSE 
                    CASE 
                        WHEN pt.list_id IS NULL THEN pt.obm_uploading_id || ''_'' || pt.method 
                        ELSE pt.obm_uploading_id || ''.'' || pt.list_id || ''_'' || pt.method 
                    END
            END as event_id,
            CASE 
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN ''C''
                WHEN (pt.comlete_list = 1) THEN ''L''
                WHEN (pt.comlete_list = 0 AND protocol_code IS NOT NULL) THEN ''F''
            END as data_type,
            date,
            CASE 
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
                ELSE round((duration::numeric/60),2)
            END as duration,
            CASE 
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
                ELSE make_interval(mins := time_of_start)
            END as time,
            pt.obm_geometry as geometry,
            pt.number,
            s.observers_ids AS observer_ids_array,
            g.etrs10_name as etrs_code,
            CASE
                WHEN (u.observer_name IS NOT NULL AND regexp_split_to_array(pt.observers,E'',\\s*'') @> ARRAY[u.observer_name::text] ) THEN u.observer_id
                ELSE (SELECT term_id FROM milvus_terms WHERE subject = ''observers'' AND term LIKE (regexp_split_to_array(observers,E'',\\s*''))[1]::text LIMIT 1)
            END as first_observer,
            bc.breeding_code as breeding_code,
            CASE 
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN NULL
                ELSE 
                    CASE
                        WHEN status_of_individuals = ''str_overflying'' THEN ''Y''
                        ELSE ''N''
                    END
            END as flying_over,
            CASE
                WHEN (pt.comlete_list = 0 AND protocol_code IS NULL) THEN date || ''_'' || g.etrs10_name || ''_'' || tx.ebp_id
                ELSE CASE WHEN pt.list_id IS NULL THEN pt.obm_uploading_id || ''_'' || pt.method || ''_'' || tx.ebp_id ELSE pt.obm_uploading_id || ''.'' || pt.list_id || ''_'' || pt.method || ''_'' || tx.ebp_id  END
            END as record_id,
            tx.ebp_id as species_code,
            tx.taxon_id as local_taxon_id,
            ''new''::varchar as status
            
        FROM public.milvus pt 
        LEFT JOIN (SELECT * FROM system.uploadings up LEFT JOIN (SELECT term_id AS observer_id, term AS observer_name FROM public.milvus_terms WHERE subject = ''observers'') as fooo ON observer_name = uploader_name) u
            ON pt.obm_uploading_id = u.id 
        LEFT JOIN ebp.protocol_connect pc 
            ON (
                method = obm_method AND 
                (national_program = obm_national_program OR (national_program IS NULL AND obm_national_program IS NULL)) AND 
                (local_program = obm_local_program OR (local_program IS NULL AND obm_local_program IS NULL))
            ) 
        LEFT JOIN ebp.breeding_codes_connect bc ON pt.status_of_individuals = bc.local_code
        LEFT JOIN public.milvus_qgrids g 
            ON g.data_table = ''milvus'' AND pt.obm_id = g.row_id
        LEFT JOIN system.milvus_linnaeus s
            ON pt.obm_id = s.row_id
        LEFT JOIN (SELECT taxon_id, ebp_id, word FROM public.milvus_taxon tax LEFT JOIN ebp.species ebps ON tax.taxon_id = ebps.local_id) as tx 
            ON tx.word = pt.species 
        WHERE 
            protocol_code IS DISTINCT FROM ''excluded'' AND 
            pt.date >= ''2010-01-01'' AND 
            pt.observed_unit = ''str_individuals'' AND 
            method IS NOT NULL AND
            comlete_list IS NOT NULL AND 
            g.etrs10_name IS NOT NULL AND ' || wh || ' RETURNING 1) SELECT count(*) c FROM rows;' INTO retval;
    RETURN retval;     
END $$;

