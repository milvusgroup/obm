-- This function is creating a uniqe (obm_uploading_id, list_id) pair based on the date, time_of_start, observers, grouping_codes, sampling_units of the observations
-- It prepares data to be uploadable to ebp, but the results should be verified.

CREATE OR REPLACE FUNCTION "update_list_id" ("upl_id" integer) RETURNS void LANGUAGE plpgsql AS $$
DECLARE
lid integer;
foo RECORD;
BEGIN
    PERFORM setval('listid',0);
    FOR foo IN SELECT DISTINCT date, time_of_start, observers, local_program, local_grouping_code, local_sampling_unit, national_program, national_grouping_code, national_sampling_unit FROM milvus WHERE obm_uploading_id = upl_id AND list_id IS NULL
    LOOP
        SELECT nextval('listid') INTO lid;
        UPDATE milvus SET list_id = lid WHERE obm_uploading_id = upl_id AND obm_id IN (SELECT obm_id FROM milvus m WHERE 
            m.date = foo.date AND
            (m.time_of_start = foo.time_of_start OR (m.time_of_start IS NULL AND foo.time_of_start IS NULL)) AND 
            m.observers = foo.observers AND 
            (m.national_program IS NOT DISTINCT FROM foo.national_program) AND 
            (m.national_grouping_code IS NOT DISTINCT FROM foo.national_grouping_code) AND 
            (m.national_sampling_unit IS NOT DISTINCT FROM foo.national_sampling_unit) AND
            (m.local_program IS NOT DISTINCT FROM foo.local_program) AND
            (m.local_grouping_code IS NOT DISTINCT FROM foo.local_grouping_code) AND
            (m.local_sampling_unit IS NOT DISTINCT FROM foo.local_sampling_unit ));
    END LOOP;
END;
$$;
