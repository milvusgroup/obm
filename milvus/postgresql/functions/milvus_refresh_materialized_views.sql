CREATE OR REPLACE FUNCTION "milvus_refresh_materialized_views" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW milvus_sampling_units_mv;
    REFRESH MATERIALIZED VIEW milvus_grouping_codes_mv;
    REFRESH MATERIALIZED VIEW milvus_program_mv;
    RETURN NULL;
END
$$;
