CREATE OR REPLACE FUNCTION "public"."milvus_update_qgrids" () RETURNS trigger AS 
$$

DECLARE
 tbl varchar;
 geom geometry;
 centr geometry;
 etrs RECORD;
BEGIN
        tbl := TG_TABLE_NAME;
        IF tg_op = 'INSERT' THEN

            geom := new."obm_geometry";
			centr := ST_Centroid(geom);

            SELECT geometry,name INTO etrs FROM shared.etrs10 g WHERE ST_Intersects(centr, g.geometry);

            execute format('INSERT INTO %s_qgrids ("data_table", "row_id", "original", "etrs10", "etrs10_name", "centroid") VALUES (%L,%L,%L,%L,%L,%L);', tbl, tbl, new.obm_id, geom, etrs.geometry, etrs.name, centr );

            RETURN new; 

        ELSIF tg_op = 'UPDATE' AND (ST_Equals(OLD."obm_geometry", NEW."obm_geometry") = FALSE) THEN

            geom := new."obm_geometry";
			centr := ST_Centroid(geom);

            SELECT geometry, name INTO etrs FROM shared.etrs10 g WHERE ST_Intersects(centr, g.geometry);

            execute format('UPDATE %s_qgrids set "original" = %L, "etrs10" = %L, etrs10_name = %L, "centroid" = %L, sci = NULL, spa = NULL, judet = NULL, comuna = NULL WHERE data_table = %L AND row_id = %L;', tbl, geom, etrs.geometry, etrs.name, centr, tbl, new.obm_id );

            RETURN new; 

        ELSIF tg_op = 'DELETE' THEN
            EXECUTE format('DELETE FROM %s_qgrids WHERE data_table = %L AND row_id = %L;',tbl,tbl,OLD.obm_id);
            RETURN old;
        ELSE
            RETURN new;
        END IF;
END; 
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
