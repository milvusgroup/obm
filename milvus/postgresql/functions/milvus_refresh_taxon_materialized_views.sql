CREATE OR REPLACE FUNCTION "milvus_refresh_taxon_materialized_views" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW milvus.milvus_taxon_valid;
    REFRESH MATERIALIZED VIEW milvus.milvus_taxon_en;
    REFRESH MATERIALIZED VIEW milvus.milvus_taxon_ro;
    REFRESH MATERIALIZED VIEW milvus.milvus_taxon_hu;
    REFRESH MATERIALIZED VIEW milvus.milvus_taxon_euring;
    RETURN NULL;
END
$$;
