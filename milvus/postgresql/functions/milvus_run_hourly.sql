CREATE OR REPLACE FUNCTION milvus_run_hourly () RETURNS void LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW milvus_observation_lists;
END;
$$;
