CREATE OR REPLACE FUNCTION "milvus_multiply_observation" (com bool default false) RETURNS void LANGUAGE plpgsql AS $$
DECLARE
foo RECORD;
BEGIN
    FOR foo IN
    WITH target_species as (
        SELECT DISTINCT word 
        FROM milvus_taxon 
        WHERE taxon_id IN (
            SELECT taxon_id 
            FROM milvus_taxon 
            WHERE word IN (
                'Ciconia nigra','Pernis apivorus','Milvus migrans','Haliaeetus albicilla',
                'Circaetus gallicus','Circus aeruginosus','Circus pygargus','Accipiter gentilis',
                'Accipiter nisus','Accipiter brevipes','Buteo buteo','Buteo rufinus','Aquila pomarina',
                'Aquila heliaca','Aquila chrysaetos','Aquila pennata','Falco tinnunculus',
                'Falco vespertinus','Falco subbuteo','Falco cherrug','Falco peregrinus'
            )
        )
    )
    SELECT * FROM milvus, target_species 
    WHERE 
        national_program = 'str_raptor_monitoring' AND 
        date > '2020-06-01' AND 
        species = word AND 
        pair_id IS NULL AND 
        number > 1
    LOOP
        IF com = false THEN 
            RAISE NOTICE '% - % - % - % - % - %', foo.national_grouping_code, foo.national_sampling_unit, foo.date, foo.observers, foo.species, foo.number;
        ELSE
            RAISE NOTICE '% - % - % - % - % - % - %', foo.obm_id, foo.national_grouping_code, foo.national_sampling_unit, foo.date, foo.observers, foo.species, foo.number;
            UPDATE milvus SET number = 1, obm_comments = ARRAY['record multiplied, number of observations modified from ' || foo.number || ' to 1'] where obm_id = foo.obm_id;
            FOR i IN 1..(foo.number - 1) LOOP
                RAISE NOTICE 'insert';
                INSERT INTO milvus (obm_comments, obm_uploading_id,date,date_until,observers,method,time_of_start,time_of_end,duration,comlete_list,wind_force,
                wind_direction,precipitation,snow_depth,visibility,temperature,optics_used,comments_general,species,number,
                number_max,observed_unit,count_precision,gender,age,habitat_state,obm_geometry,national_program,local_program,
                colony_type,colony_place,colony_survey_method,disturbance,atmospheric_pressure,altitude,access_to_data,
                national_grouping_code,national_sampling_unit,ice_cover,cloud_cover,wind_speed_kmh,flight_direction,
                cause_of_death,nest_type,nest_status,nest_condition,distance_m,distance_code,habitat_id,habitat_2,map_code,
                timing_of_reaction,response_type,in_flight_sitting,flight_altitude,migration_intensity,migration_type,
                secondary_time,status_of_individuals,nest_place,comments_observation,list_id,obm_files_id,
                local_grouping_code,local_sampling_unit,x,y,data_owner,number_of_eggs,number_of_nestlings,
                method_details_species_aimed,exact_time,precision_of_count_list,surveyed_geometry,
                source,source_id,non_human_observer,nest_id,samplin_unit_coverage,active_abandoned,
                type_number_trees,type_place_roost_colony,threats,nestling_age,nr_dead_egg,nr_dead_nestling,
                projekt,obm_observation_list_id,colony_code,start_time,end_time,obs_duration,pair_id) 
                VALUES (ARRAY['record generated automaticaly by multiplying record #' || foo.obm_id || ' and setting number of observed birds to 1'],
                foo.obm_uploading_id, foo.date, foo.date_until, foo.observers, foo.method, foo.time_of_start, foo.time_of_end, foo.duration, foo.comlete_list, foo.wind_force, 
                foo.wind_direction, foo.precipitation, foo.snow_depth, foo.visibility, foo.temperature, foo.optics_used, foo.comments_general, foo.species, 1, 
                foo.number_max, foo.observed_unit, foo.count_precision, foo.gender, foo.age, foo.habitat_state, foo.obm_geometry, foo.national_program, foo.local_program, 
                foo.colony_type, foo.colony_place, foo.colony_survey_method, foo.disturbance, foo.atmospheric_pressure, foo.altitude, foo.access_to_data, 
                foo.national_grouping_code, foo.national_sampling_unit, foo.ice_cover, foo.cloud_cover, foo.wind_speed_kmh, foo.flight_direction, 
                foo.cause_of_death, foo.nest_type, foo.nest_status, foo.nest_condition, foo.distance_m, foo.distance_code, foo.habitat_id, foo.habitat_2, foo.map_code, 
                foo.timing_of_reaction, foo.response_type, foo.in_flight_sitting, foo.flight_altitude, foo.migration_intensity, foo.migration_type, 
                foo.secondary_time, foo.status_of_individuals, foo.nest_place, foo.comments_observation, foo.list_id, foo.obm_files_id, 
                foo.local_grouping_code, foo.local_sampling_unit, foo.x, foo.y, foo.data_owner, foo.number_of_eggs, foo.number_of_nestlings, 
                foo.method_details_species_aimed, foo.exact_time, foo.precision_of_count_list, foo.surveyed_geometry, 
                foo.source, foo.source_id, foo.non_human_observer, foo.nest_id, foo.samplin_unit_coverage, foo.active_abandoned, 
                foo.type_number_trees, foo.type_place_roost_colony, foo.threats, foo.nestling_age, foo.nr_dead_egg, foo.nr_dead_nestling, 
                foo.projekt, foo.obm_observation_list_id, foo.colony_code, foo.start_time, foo.end_time, foo.obs_duration, foo.pair_id
                );
            END LOOP;
        END IF;
    END LOOP;
END;
$$;
