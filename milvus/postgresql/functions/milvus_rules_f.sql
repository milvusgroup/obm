CREATE OR REPLACE FUNCTION "public"."milvus_rules_f" () RETURNS trigger AS
$$

DECLARE

 project varchar;
 tbl varchar;
 sens integer DEFAULT 0;
 ext integer DEFAULT 0;
 upl RECORD;

BEGIN

    project := 'milvus';
    tbl := TG_TABLE_NAME;
    
    IF tg_op = 'INSERT' THEN

        sens := new.access_to_data;
        SELECT "group" as rd, owner as wr, CASE WHEN array_position(owner, 322) IS NULL THEN owner || 322 ELSE owner END as dl INTO upl FROM system.uploadings WHERE uploadings.id=NEW.obm_uploading_id;

        IF (new.access_to_data = 1) THEN
            ext := 1;
            sens := 3;
        END IF;

        EXECUTE format('INSERT INTO %s_rules ("data_table",row_id,read,write,download,sensitivity) VALUES (%L, %L, %L, %L, %L, %L);', project, tbl, NEW.obm_id, upl.rd, upl.wr, upl.dl, sens);

        IF (sens = 3) THEN
            EXECUTE format('INSERT INTO %s_rules_extension ("data_table","row_id","rule_value") VALUES (%L,%L,%L);', project, tbl, new.obm_id, ext);
        END IF;

        RETURN new;

    ELSIF tg_op = 'UPDATE' AND OLD.access_to_data != NEW.access_to_data THEN

        sens := new.access_to_data;

        IF (new.access_to_data = 1) THEN
            ext := 1;
            sens := 3;
        END IF;

        execute format('UPDATE %s_rules SET sensitivity = %L WHERE row_id = %L AND data_table = %L',project, sens, NEW.obm_id, tbl);

        IF (sens = 3) THEN
            EXECUTE format('UPDATE milvus_rules_extension SET rule_value = %1$L WHERE data_table = %2$L AND row_id = %3$L; INSERT INTO milvus_rules_extension ("data_table","row_id","rule_value") SELECT %2$L,%3$L,%1$L WHERE NOT EXISTS (SELECT 1 FROM milvus_rules_extension WHERE "data_table" = %2$L AND "row_id" = %3$L)', ext, 'milvus', new.obm_id);
        END IF;

        IF (old.access_to_data = 1) THEN
            IF (new.access_to_data = 3) THEN
                execute format('UPDATE milvus_rules_extension SET rule_value = 0 WHERE data_table = %L AND row_id = %L','milvus',new.obm_id);
            ELSE
                execute format('DELETE FROM milvus_rules_extension WHERE data_table = %L AND row_id = %L','milvus',new.obm_id);
            END IF;
        END IF;

        RETURN new;

    ELSIF tg_op = 'DELETE' THEN

        execute format('DELETE FROM "milvus_rules" WHERE "data_table" = %L AND "row_id" = %L ','milvus',old.obm_id);
        RETURN old;

    ELSE

        RETURN new;

    END IF;

END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
