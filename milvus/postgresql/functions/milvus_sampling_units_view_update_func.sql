CREATE OR REPLACE FUNCTION "milvus_sampling_units_view_update_func" () RETURNS trigger LANGUAGE plpgsql AS $$

BEGIN

        IF tg_op = 'UPDATE' THEN

            RAISE NOTICE '%', NEW.obm_id;
            UPDATE milvus_sampling_units SET 
                obm_geometry = NEW.obm_geometry,
                alapegyseg = NEW.alapegyseg, 
                gykod_id = NEW.gykod_id, 
                program = NEW.program
            WHERE obm_id = NEW.obm_id;

            RETURN NEW;

        ELSE

            RETURN new;

        END IF;

END; 
$$;


