DROP INDEX complete_list_idx;
DROP INDEX datum_index;
DROP INDEX method_idx;
DROP INDEX milvus_geom_idx;
DROP INDEX species_idx;
DROP INDEX status_of_individuals_idx;
DROP INDEX uploding_id_idx;

CREATE INDEX milvus_complete_list_idx ON milvus USING btree (comlete_list);
CREATE INDEX milvus_datum_index ON milvus USING btree (date);
CREATE INDEX milvus_method_idx ON milvus USING btree (method);
CREATE INDEX milvus_geom_idx ON milvus USING gist (obm_geometry);
CREATE INDEX milvus_species_idx ON milvus USING btree (species);
CREATE INDEX milvus_status_of_individuals_idx ON milvus USING btree (status_of_individuals);
CREATE INDEX milvus_uploding_id_idx ON milvus USING btree (obm_uploading_id);
