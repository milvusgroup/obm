CREATE SEQUENCE public.milvus_obm_observation_list_id
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
