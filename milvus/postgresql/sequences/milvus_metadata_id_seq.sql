CREATE SEQUENCE public.milvus_metadata_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

ALTER SEQUENCE milvus_metadata_id_seq OWNER TO milvus_admin;
--DROP SEQUENCE milvus_metadata_id_seq;