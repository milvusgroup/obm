DROP MATERIALIZED VIEW public.milvus_observation_lists;
CREATE MATERIALIZED VIEW public.milvus_observation_lists AS
WITH m_agg as (
    SELECT 
        obm_observation_list_id, 
        u.uploader_name,
        national_program as program,
        national_grouping_code as grouping_code,
        national_sampling_unit as sampling_unit,
        ST_MakeLine(obm_geometry ORDER BY u.metadata->>'finished_at') as obm_geometry, 
        string_agg(DISTINCT species_valid, ', ') as species_list, 
        count(*) as no_of_observations,
        max(access_to_data) as obm_access
    FROM milvus m 
    LEFT JOIN system.uploadings u ON obm_uploading_id = u.id 
    WHERE obm_observation_list_id IS NOT NULL
    GROUP BY obm_observation_list_id, national_program, national_grouping_code, national_sampling_unit, u.uploader_name
)
SELECT 
-- COLUMNS FROM THE MAIN TABLE
    d.id as obm_id,
    COALESCE(
        vf.tracklog_line_geom,
        vf2.obm_geometry
    ) as obm_geometry,
    d.uploading_id as obm_uploading_id,
    NULL::numeric as obm_validation,
    NULL::integer as obm_modifier_id,
    NULL::varchar as obm_files_id,
    vf2.uploader_name,
    d.obsstart::date as date_start,
    d.obsstart::time as time_start,
    d.obsend::date as date_end,
    d.obsend::time as time_end,
    d.obsend - d.obsstart as duration,
    d.oidl as observation_list_id,
-- COLUMNS FROM THE JOINED TABLE
    vf2.species_list,
    vf2.no_of_observations,
    vf2.program,
    vf2.grouping_code,
    vf2.sampling_unit,
    vf.trackname,
    vf2.obm_access
-- MAIN TABLE REFERENCE
FROM public.milvus_observation_list d
-- A JOIN STATEMENT
LEFT JOIN public.milvus_tracklogs vf ON (d.oidl = vf.observation_list_id)
LEFT JOIN m_agg vf2 ON vf2.obm_observation_list_id = d.oidl ;

ALTER MATERIALIZED VIEW milvus_observation_lists owner to milvus_admin ;


-- SELECT obm_id, obm_geometry, 0 as writable %selected%
-- FROM %F%milvus_observation_lists m%F%
--     %uploading_join%
--     %morefilter%
-- WHERE %geometry_type% %envelope% %qstr%
