ALTER TABLE milvus_rules SET SCHEMA milvus;

CREATE OR REPLACE VIEW public.milvus_rules AS 
SELECT data_table, row_id, owners, groups, rights, read, write, download, sensitivity FROM milvus.milvus_rules
UNION
SELECT 
    'milvus_observation_lists' as data_table,
    obm_id as row_id,
    NULL::integer[] as owners,
    NULL::integer[] as groups,
    NULL::integer[] as rights,
    u."group" as read,
    u."owner" as write,
    CASE WHEN array_position(owner, 322) IS NULL THEN owner || 322 ELSE owner END as download,
    obm_access::varchar as sensitivity
FROM milvus_observation_lists l 
LEFT JOIN system.uploadings u ON l.obm_uploading_id = u.id;

GRANT ALL on public.milvus_rules TO milvus_admin;

CREATE RULE milvus_rules_ins AS ON INSERT TO public.milvus_rules
    DO INSTEAD
    INSERT INTO milvus.milvus_rules (data_table, row_id, owners, groups, rights, read, write, download, sensitivity) VALUES (
        NEW."data_table",
        NEW.row_id,
        NULL,
        NULL,
        NULL,
        NEW.read,
        NEW.write,
        NEW.download,
        NEW.sensitivity
    );

CREATE RULE milvus_rules_upd AS ON UPDATE TO public.milvus_rules
    DO INSTEAD
    UPDATE milvus.milvus_rules
       SET 
         read = NEW.read,
         write = NEW.write,
         download = NEW.download,
         sensitivity = NEW.sensitivity
     WHERE data_table = OLD.data_table AND row_id = OLD.row_id;

CREATE RULE milvus_rules_del AS ON DELETE TO public.milvus_rules
    DO INSTEAD
    DELETE FROM milvus.milvus_rules
     WHERE data_table = OLD.data_table AND row_id = OLD.row_id;
