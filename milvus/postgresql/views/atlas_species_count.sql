DROP MATERIALIZED VIEW milvus.atlas_species_count;
CREATE MATERIALIZED VIEW milvus.atlas_species_count as 
WITH rw as (
    SELECT su.obm_id, su.obm_geometry, su.gykod_nev as grouping_code, su.alapegyseg as sampling_unit, count(distinct m.species_valid) as c
    FROM milvus_sampling_units su
    LEFT JOIN milvus_qgrids qg ON su.alapegyseg = qg.urban_atlas
    LEFT JOIN milvus m ON qg.data_table = 'milvus' AND qg.row_id = m.obm_id 
    WHERE program_nev = 'str_urban_atlas' 
    GROUP BY su.gykod_nev, su.alapegyseg, su.obm_geometry, su.obm_id
),
mx as (select grouping_code, max(c) FROM rw GROUP BY grouping_code)
SELECT obm_id, obm_geometry, rw.grouping_code, sampling_unit, c,
case WHEN c = 0 THEN 0 else round(c/(mx.max::numeric / 10)) end as color
FROM rw 
LEFT JOIN mx ON mx.grouping_code = rw.grouping_code;
--GROUP BY obm_id, obm_geometry, rw.grouping_code, sampling_unit;
GRANT SELECT ON milvus.atlas_species_count TO milvus_admin;
