DROP VIEW milvus_project_programs_view;
CREATE OR REPLACE VIEW milvus_project_programs_view AS
SELECT p.*, pr.obm_id as project_id, pr.name as project_name
FROM milvus_project_programs pp
LEFT JOIN milvus_program p ON pp.program_id = p.obm_id
LEFT JOIN milvus_projects pr ON pp.project_id = pr.obm_id;
