WITH
observations AS (
    SELECT oidl, uploading_id FROM milvus_observation_list WHERE obsstart IS NULL AND obsend IS NULL
),
lists AS (
    SELECT oidl, obsstart, obsend, nulllist FROM milvus_observation_list WHERE obsstart IS NOT NULL AND obsend IS NOT NULL
)
SELECT * FROM lists LEFT JOIN observations ON observations.oidl = lists.oidl;
