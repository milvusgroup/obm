--
-- Name: milvus milvus_del; Type: RULE; Schema: public; Owner: milvus_admin
--

CREATE RULE milvus_del AS
    ON DELETE TO public.milvus DO INSTEAD  DELETE FROM milvus.milvus
  WHERE (milvus.obm_id = old.obm_id);


--
-- Name: milvus milvus_ins; Type: RULE; Schema: public; Owner: milvus_admin
--

CREATE RULE milvus_ins AS
    ON INSERT TO public.milvus DO INSTEAD  INSERT INTO milvus.milvus (obm_uploading_id, obm_validation, obm_comments, obm_modifier_id, date, date_until, observers, method, time_of_start, time_of_end, duration, comlete_list, wind_force, wind_direction, precipitation, snow_depth, visibility, temperature, optics_used, comments_general, species, number, number_max, observed_unit, count_precision, gender, age, habitat_state, obm_geometry, national_program, local_program, colony_type, colony_place, colony_survey_method, disturbance, atmospheric_pressure, altitude, access_to_data, national_grouping_code, national_sampling_unit, ice_cover, cloud_cover, wind_speed_kmh, flight_direction, cause_of_death, nest_type, nest_status, nest_condition, distance_m, distance_code, habitat_id, habitat_2, map_code, timing_of_reaction, response_type, in_flight_sitting, flight_altitude, migration_intensity, migration_type, secondary_time, status_of_individuals, nest_place, comments_observation, list_id, obm_files_id, local_grouping_code, local_sampling_unit, x, y, data_owner, number_of_eggs, number_of_nestlings, method_details_species_aimed, exact_time, precision_of_count_list, surveyed_geometry, source, source_id, non_human_observer, nest_id, samplin_unit_coverage, active_abandoned, type_number_trees, type_place_roost_colony, threats, nestling_age, nr_dead_egg, nr_dead_nestling, projekt, obm_observation_list_id, colony_code, start_time, end_time, obs_duration, pair_id, obm_access, uncertain_identification)
  VALUES (new.obm_uploading_id, new.obm_validation, new.obm_comments, new.obm_modifier_id, new.date, new.date_until, new.observers, new.method, new.time_of_start, new.time_of_end, new.duration, new.comlete_list, new.wind_force, new.wind_direction, new.precipitation, new.snow_depth, new.visibility, new.temperature, new.optics_used, new.comments_general, new.species, new.number, new.number_max, new.observed_unit, new.count_precision, new.gender, new.age, new.habitat_state, new.obm_geometry, new.national_program, new.local_program, new.colony_type, new.colony_place, new.colony_survey_method, new.disturbance, new.atmospheric_pressure, new.altitude, new.access_to_data, new.national_grouping_code, new.national_sampling_unit, new.ice_cover, new.cloud_cover, new.wind_speed_kmh, new.flight_direction, new.cause_of_death, new.nest_type, new.nest_status, new.nest_condition, new.distance_m, new.distance_code, new.habitat_id, new.habitat_2, new.map_code, new.timing_of_reaction, new.response_type, new.in_flight_sitting, new.flight_altitude, new.migration_intensity, new.migration_type, new.secondary_time, new.status_of_individuals, new.nest_place, new.comments_observation, new.list_id, new.obm_files_id, new.local_grouping_code, new.local_sampling_unit, new.x, new.y, new.data_owner, new.number_of_eggs, new.number_of_nestlings, new.method_details_species_aimed, new.exact_time, new.precision_of_count_list, new.surveyed_geometry, new.source, new.source_id, new.non_human_observer, new.nest_id, new.samplin_unit_coverage, new.active_abandoned, new.type_number_trees, new.type_place_roost_colony, new.threats, new.nestling_age, new.nr_dead_egg, new.nr_dead_nestling, new.projekt, new.obm_observation_list_id, new.colony_code, new.start_time, new.end_time, new.obs_duration, new.pair_id, new.obm_access, new.uncertain_identification)
  RETURNING milvus.obm_id,
    milvus.obm_uploading_id,
    milvus.obm_validation,
    milvus.obm_comments,
    milvus.obm_modifier_id,
    milvus.date,
    milvus.date_until,
    milvus.observers,
    milvus.method,
    milvus.time_of_start,
    milvus.time_of_end,
    milvus.duration,
    milvus.comlete_list,
    milvus.wind_force,
    milvus.wind_direction,
    milvus.precipitation,
    milvus.snow_depth,
    milvus.visibility,
    milvus.temperature,
    milvus.optics_used,
    milvus.comments_general,
    milvus.species,
    milvus.number,
    milvus.number_max,
    milvus.observed_unit,
    milvus.count_precision,
    milvus.gender,
    milvus.age,
    milvus.habitat_state,
    milvus.obm_geometry,
    milvus.national_program,
    milvus.local_program,
    milvus.colony_type,
    milvus.colony_place,
    milvus.colony_survey_method,
    milvus.disturbance,
    milvus.atmospheric_pressure,
    milvus.altitude,
    milvus.access_to_data,
    milvus.national_grouping_code,
    milvus.national_sampling_unit,
    milvus.ice_cover,
    milvus.cloud_cover,
    milvus.wind_speed_kmh,
    milvus.flight_direction,
    milvus.cause_of_death,
    milvus.nest_type,
    milvus.nest_status,
    milvus.nest_condition,
    milvus.distance_m,
    milvus.distance_code,
    milvus.habitat_id,
    milvus.habitat_2,
    milvus.map_code,
    milvus.timing_of_reaction,
    milvus.response_type,
    milvus.in_flight_sitting,
    milvus.flight_altitude,
    milvus.migration_intensity,
    milvus.migration_type,
    milvus.secondary_time,
    milvus.status_of_individuals,
    milvus.nest_place,
    milvus.comments_observation,
    milvus.list_id,
    milvus.obm_files_id,
    milvus.local_grouping_code,
    milvus.local_sampling_unit,
    milvus.x,
    milvus.y,
    milvus.data_owner,
    milvus.number_of_eggs,
    milvus.number_of_nestlings,
    milvus.method_details_species_aimed,
    milvus.exact_time,
    milvus.precision_of_count_list,
    milvus.surveyed_geometry,
    milvus.source,
    milvus.source_id,
    milvus.non_human_observer,
    milvus.nest_id,
    milvus.samplin_unit_coverage,
    milvus.active_abandoned,
    milvus.type_number_trees,
    milvus.type_place_roost_colony,
    milvus.threats,
    milvus.nestling_age,
    milvus.nr_dead_egg,
    milvus.nr_dead_nestling,
    milvus.projekt,
    milvus.obm_observation_list_id,
    milvus.colony_code,
    milvus.start_time,
    milvus.end_time,
    milvus.obs_duration,
    milvus.pair_id,
    milvus.obm_access,
    NULL::character varying AS "varchar",
    NULL::character varying AS "varchar",
    NULL::character varying AS "varchar",
    NULL::character varying AS "varchar",
    NULL::character varying AS "varchar",
    milvus.uncertain_identification,
    NULL::integer AS "integer";


--
-- Name: milvus milvus_upd; Type: RULE; Schema: public; Owner: milvus_admin
--

CREATE RULE milvus_upd AS
    ON UPDATE TO public.milvus DO INSTEAD  UPDATE milvus.milvus SET obm_uploading_id = new.obm_uploading_id, obm_validation = new.obm_validation, obm_comments = new.obm_comments, obm_modifier_id = new.obm_modifier_id, date = new.date, date_until = new.date_until, observers = new.observers, method = new.method, time_of_start = new.time_of_start, time_of_end = new.time_of_end, duration = new.duration, comlete_list = new.comlete_list, wind_force = new.wind_force, wind_direction = new.wind_direction, precipitation = new.precipitation, snow_depth = new.snow_depth, visibility = new.visibility, temperature = new.temperature, optics_used = new.optics_used, comments_general = new.comments_general, species = new.species, number = new.number, number_max = new.number_max, observed_unit = new.observed_unit, count_precision = new.count_precision, gender = new.gender, age = new.age, habitat_state = new.habitat_state, obm_geometry = new.obm_geometry, national_program = new.national_program, local_program = new.local_program, colony_type = new.colony_type, colony_place = new.colony_place, colony_survey_method = new.colony_survey_method, disturbance = new.disturbance, atmospheric_pressure = new.atmospheric_pressure, altitude = new.altitude, access_to_data = new.access_to_data, national_grouping_code = new.national_grouping_code, national_sampling_unit = new.national_sampling_unit, ice_cover = new.ice_cover, cloud_cover = new.cloud_cover, wind_speed_kmh = new.wind_speed_kmh, flight_direction = new.flight_direction, cause_of_death = new.cause_of_death, nest_type = new.nest_type, nest_status = new.nest_status, nest_condition = new.nest_condition, distance_m = new.distance_m, distance_code = new.distance_code, habitat_id = new.habitat_id, habitat_2 = new.habitat_2, map_code = new.map_code, timing_of_reaction = new.timing_of_reaction, response_type = new.response_type, in_flight_sitting = new.in_flight_sitting, flight_altitude = new.flight_altitude, migration_intensity = new.migration_intensity, migration_type = new.migration_type, secondary_time = new.secondary_time, status_of_individuals = new.status_of_individuals, nest_place = new.nest_place, comments_observation = new.comments_observation, list_id = new.list_id, obm_files_id = new.obm_files_id, local_grouping_code = new.local_grouping_code, local_sampling_unit = new.local_sampling_unit, x = new.x, y = new.y, data_owner = new.data_owner, number_of_eggs = new.number_of_eggs, number_of_nestlings = new.number_of_nestlings, method_details_species_aimed = new.method_details_species_aimed, exact_time = new.exact_time, precision_of_count_list = new.precision_of_count_list, surveyed_geometry = new.surveyed_geometry, source = new.source, source_id = new.source_id, non_human_observer = new.non_human_observer, nest_id = new.nest_id, samplin_unit_coverage = new.samplin_unit_coverage, active_abandoned = new.active_abandoned, type_number_trees = new.type_number_trees, type_place_roost_colony = new.type_place_roost_colony, threats = new.threats, nestling_age = new.nestling_age, nr_dead_egg = new.nr_dead_egg, nr_dead_nestling = new.nr_dead_nestling, projekt = new.projekt, obm_observation_list_id = new.obm_observation_list_id, colony_code = new.colony_code, start_time = new.start_time, end_time = new.end_time, obs_duration = new.obs_duration, pair_id = new.pair_id, obm_access = new.obm_access, uncertain_identification = new.uncertain_identification
  WHERE (milvus.obm_id = new.obm_id);

