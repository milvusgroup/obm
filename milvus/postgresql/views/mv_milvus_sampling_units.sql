DROP MATERIALIZED VIEW milvus_sampling_units_mv;
CREATE MATERIALIZED VIEW milvus_sampling_units_mv AS
SELECT ROW_NUMBER() OVER () uid, project, project_id, program, program_id, grouping_code, grouping_code_id, sampling_unit, sampling_unit_id FROM 
(
SELECT
    proj.name AS project,
    proj.obm_id as project_id,
    pr.nev AS program,
    pr.obm_id as program_id,
    gc.gykod as grouping_code,
    gc.obm_id as grouping_code_id,
    su.alapegyseg as sampling_unit,
    su.obm_id as sampling_unit_id
   FROM milvus_projects as proj
   LEFT JOIN milvus_project_programs pp ON proj.obm_id = pp.project_id
   LEFT JOIN milvus_program pr ON pp.program_id = pr.obm_id
   LEFT JOIN milvus_grouping_codes gc ON pr.obm_id = gc.program
   LEFT JOIN milvus_sampling_units su ON gc.obm_id = su.gykod_id
) as foo;

ALTER MATERIALIZED VIEW milvus_sampling_units_mv OWNER TO milvus_admin;
