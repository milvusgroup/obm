DROP TABLE milvus_tracklogs; 
DROP SEQUENCE milvus_tracklogs_id_seq;
DROP VIEW milvus_tracklogs;
CREATE OR REPLACE VIEW milvus_tracklogs AS
SELECT 
    tracklog_id, user_id, start_time, end_time, trackname, observation_list_id, tracklog_line_geom
FROM 
    system.tracklogs WHERE project = 'milvus';
    
GRANT SELECT on milvus_tracklogs TO milvus_admin;
