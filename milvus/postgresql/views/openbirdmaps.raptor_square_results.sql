CREATE OR REPLACE VIEW openbirdmaps.raptor_square_results as 
WITH 
doubles as (
    SELECT 
        per.period_name,
        d.id as double_id, 
        s.grouping_code,
        p.species,
        p.id as pair_id, 
        d.status as double_status, 
        d.min,
        d.max
    FROM openbirdmaps.raptor_double d
    LEFT JOIN openbirdmaps.raptor_double_pair dp ON  dp.double_id = d.id
    LEFT JOIN openbirdmaps.raptor_pair p ON dp.pair_id = p.id
    LEFT JOIN openbirdmaps.raptor_survey_session s ON p.session_id = s.id
    LEFT JOIN openbirdmaps.raptor_period per ON per.id = s.period_id
),
pairs as (
    SELECT 
        per.period_name,
        p.id as pair_id, 
        s.grouping_code, 
        p.species, 
        p.min, 
        p.max
    FROM openbirdmaps.raptor_pair p
    LEFT JOIN openbirdmaps.raptor_survey_session s ON s.id = p.session_id
    LEFT JOIN openbirdmaps.raptor_period per ON per.id = s.period_id
    LEFT JOIN doubles dbl on p.id = dbl.pair_id
    WHERE 
        dbl.pair_id IS NULL AND
        p.status IN ('str_certain_pair', 'str_possible_pair')
),
unionn as (
    SELECT double_id as id, period_name, grouping_code, species, min, max FROM doubles
    UNION 
    SELECT pair_id as id, period_name, grouping_code, species, min, max FROM pairs
) SELECT period_name, grouping_code, species, sum(min) as min, sum(max) as max FROM unionn GROUP by period_name, grouping_code, species;

GRANT SELECT ON openbirdmaps.raptor_square_results TO milvus_admin;
GRANT SELECT ON openbirdmaps.raptor_square_results TO istvan_kovacs_milvus_ro;

\copy (SELECT * FROM openbirdmaps.raptor_square_results) TO '/tmp/ragadozofelmeres_eredmenyek.csv' CSV HEADER;