-- https://redmine.milvus.ro/issues/1542
DROP MATERIALIZED VIEW openbirdmaps.uploadings_old_lists;
CREATE MATERIALIZED VIEW openbirdmaps.uploadings_old_lists AS 
WITH list_dates as (
    SELECT DISTINCT date, obm_observation_list_id FROM milvus WHERE obm_observation_list_id IS NOT NULL
    )
SELECT * FROM (
    SELECT 
        observation_list_end - observation_list_start as list_duration,
        ld.date data_date,
        CASE 
            WHEN observation_list_start::date = observation_list_end::date AND observation_list_start::date > ld.date THEN observation_list_start - ld.date
            ELSE NULL
        END as old_list,
        um.*
    FROM openbirdmaps.uploadings_metadata um
    LEFT JOIN list_dates ld ON obm_observation_list_id = observation_list_id
    WHERE 
        observation_list_start IS NOT NULL AND
        observation_list_end IS NOT NULL
    ) as foo
WHERE old_list IS NOT NULL
ORDER BY uploading_date DESC;

GRANT ALL ON openbirdmaps.uploadings_old_lists TO milvus_admin;

