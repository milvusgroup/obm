-- https://redmine.milvus.ro/issues/1542
DROP MATERIALIZED VIEW openbirdmaps.uploadings_long_lists;
CREATE MATERIALIZED VIEW openbirdmaps.uploadings_long_lists AS 
SELECT foo.*, v.validation FROM (
    SELECT 
        observation_list_end - observation_list_start as list_duration,
        um.*
    FROM openbirdmaps.uploadings_metadata um
    WHERE 
        observation_list_start IS NOT NULL AND
        observation_list_end IS NOT NULL
    ) as foo
LEFT JOIN openbirdmaps.list_validation v ON foo.observation_list_id = v.observation_list_id
WHERE observation_list_start::date != observation_list_end::date AND list_duration > '03:00:00'
ORDER BY uploading_date DESC;

GRANT ALL ON openbirdmaps.uploadings_long_lists TO milvus_admin;
