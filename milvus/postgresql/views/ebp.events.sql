DROP VIEW IF EXISTS "ebp"."events";
CREATE VIEW "ebp"."events" AS SELECT bar.event_id,
    bar.protocol_id,
    bar.data_type,
    bar.date,
    bar.duration,
    bar."time",
    bar.location_mode,
    sum(bar.records) AS records,
    (sum(bar.observer))::character varying AS observer,
        CASE
            WHEN (bar.data_type = 'C'::text) THEN (( SELECT g.name
               FROM shared.etrs10 g
              WHERE public.st_intersects(g.geometry, bar.centroid)))::text
            ELSE public.st_astext(bar.centroid)
        END AS location,
        CASE
            WHEN (bar.data_type = 'C'::text) THEN NULL::integer
            ELSE ( SELECT (st_minimumboundingradius.radius)::integer AS radius
               FROM public.st_minimumboundingradius(public.st_transform(bar.geometryc, 31700)) st_minimumboundingradius(center, radius))
        END AS radius,
    1 AS state
   FROM ( SELECT foo.event_id,
            foo.protocol_id,
            foo.data_type,
            min(foo.date) as date, -- over midnight lists
            foo.duration,
            foo."time",
            CASE
                WHEN (foo.data_type = 'C'::text) THEN count(DISTINCT foo.first_observer)
                ELSE (max(foo.first_observer))::bigint
            END AS observer,
            CASE
                WHEN (foo.data_type = 'C'::text) THEN 'A'::text
                ELSE 'E'::text
            END AS location_mode,
            public.st_collect(foo.geometry) AS geometryc,
            public.st_centroid(public.st_collect(foo.geometry)) AS centroid,
                CASE
                    WHEN (foo.data_type = 'C'::text) THEN count(DISTINCT ROW(foo.local_taxon_id, foo.first_observer))
                    ELSE count(DISTINCT foo.local_taxon_id)
                END AS records
           FROM ( SELECT bar_1.obm_id,
                    bar_1.uploading_date,
                    bar_1.protocol_id,
                    bar_1.event_id,
                    bar_1.data_type,
                    bar_1.date,
                    bar_1.duration,
                    bar_1."time",
                    bar_1.geometry,
                    bar_1.number,
                    bar_1.observer_ids_array,
                    bar_1.etrs_code,
                    bar_1.first_observer,
                    bar_1.breeding_code,
                    bar_1.flying_over,
                    bar_1.record_id,
                    bar_1.species_code,
                    bar_1.local_taxon_id,
                    bar_1.status,
                    unnest(bar_1.observer_ids_array) AS observer_ids
                   FROM ebp.records_raw bar_1
                  WHERE ((bar_1.status)::text = ANY (ARRAY[('new'::character varying)::text, ('modified'::character varying)::text]))) foo
          GROUP BY foo.event_id, foo.protocol_id, foo.data_type, foo.duration, foo."time") bar
  GROUP BY bar.event_id, bar.protocol_id, bar.data_type, bar.date, bar.duration, bar."time", bar.location_mode,
        CASE
            WHEN (bar.data_type = 'C'::text) THEN NULL::integer
            ELSE ( SELECT (st_minimumboundingradius.radius)::integer AS radius
               FROM public.st_minimumboundingradius(public.st_transform(bar.geometryc, 31700)) st_minimumboundingradius(center, radius))
        END, bar.geometryc, bar.centroid;
ALTER VIEW ebp.events OWNER TO milvus_admin;