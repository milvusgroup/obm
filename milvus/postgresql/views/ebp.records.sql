DROP VIEW IF EXISTS "ebp"."records";
CREATE VIEW "ebp"."records" AS 
SELECT 
    max(foo.breeding_code) AS breeding_code,
    foo.event_id,
    CASE
        WHEN (length(string_agg(DISTINCT lower(foo.flying_over), ''::text)) = 2) THEN 'N'::text
        ELSE string_agg(DISTINCT foo.flying_over, ''::text)
    END AS flying_over,
    foo.record_id,
    CASE
        WHEN (foo.data_type = 'C'::text) THEN count(*)
        ELSE (1)::bigint
    END AS records_of_species,
    foo.species_code,
    sum(foo.number) AS count
   FROM ebp.records_raw foo
  WHERE ((foo.species_code IS NOT NULL) AND ((foo.status)::text = ANY (ARRAY[('new'::character varying)::text, ('modified'::character varying)::text])))
  GROUP BY foo.event_id, foo.record_id, foo.species_code, foo.data_type;
ALTER VIEW ebp.records OWNER TO ebp;