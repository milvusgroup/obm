DROP MATERIALIZED VIEW milvus_program_mv;
CREATE MATERIALIZED VIEW milvus_program_mv AS
SELECT ROW_NUMBER() OVER () uid, project, project_id, program, program_id FROM 
(
SELECT
    proj.name AS project,
    proj.obm_id as project_id,
    pr.nev AS program,
    pr.obm_id as program_id
   FROM milvus_projects as proj
   LEFT JOIN milvus_project_programs pp ON proj.obm_id = pp.project_id
   LEFT JOIN milvus_program pr ON pp.program_id = pr.obm_id
) as foo;

ALTER MATERIALIZED VIEW milvus_program_mv OWNER TO milvus_admin;
