DROP VIEW "openbirdmaps"."raptor_administration";
CREATE OR REPLACE VIEW "openbirdmaps"."raptor_administration" AS
SELECT 
    id, period_id, grouping_code, responsible, array_to_json(array_agg(observer)) as observer, finished, no_of_points
FROM (
    SELECT 
        s.id,
        s.period_id,
        s.grouping_code,
        s.responsible,
        o.observer,
        s.finished,
        count(*) as no_of_points
    FROM openbirdmaps.raptor_survey_session s
    LEFT JOIN openbirdmaps.raptor_period p ON s.period_id = p.id
    LEFT JOIN openbirdmaps.raptor_observer o ON s.id = o.session_id
    LEFT JOIN milvus_sampling_units_mv su ON su.project = p.project AND su.program = p.program AND su.grouping_code = s.grouping_code
    GROUP BY s.id, s.period_id, s.grouping_code, s.responsible, o.observer, s.finished
) as foo
GROUP BY id, period_id, grouping_code, responsible, finished, no_of_points;

GRANT ALL on "openbirdmaps"."raptor_administration" TO milvus_admin;