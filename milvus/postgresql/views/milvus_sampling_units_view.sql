CREATE OR REPLACE VIEW milvus_sampling_units_view AS
SELECT su.*, gc.gykod as grouping_code_name, p.nev as program_name FROM milvus_sampling_units su
LEFT JOIN milvus_grouping_codes gc ON su.gykod_id = gc.obm_id
LEFT JOIN milvus_program p ON su.program = p.obm_id;
