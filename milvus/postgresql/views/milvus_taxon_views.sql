-- sci
DROP MATERIALIZED VIEW milvus.milvus_taxon_valid;
CREATE MATERIALIZED VIEW milvus.milvus_taxon_valid AS
SELECT DISTINCT ON (milvus_taxon.taxon_id) milvus_taxon.taxon_id,
    milvus_taxon.word,
    milvus_taxon.status
   FROM milvus_taxon
  WHERE milvus_taxon.lang = 'species_valid' AND milvus_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON milvus.milvus_taxon_valid (taxon_id);

GRANT SELECT ON milvus.milvus_taxon_valid TO public; 
ALTER MATERIALIZED VIEW milvus.milvus_taxon_valid OWNER TO milvus_admin;


-- hu
DROP MATERIALIZED VIEW milvus.milvus_taxon_hu;
CREATE MATERIALIZED VIEW milvus.milvus_taxon_hu AS
SELECT DISTINCT ON (milvus_taxon.taxon_id) milvus_taxon.taxon_id,
    milvus_taxon.word,
    milvus_taxon.status
   FROM milvus_taxon
  WHERE milvus_taxon.lang = 'species_hu' AND milvus_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON milvus.milvus_taxon_hu (taxon_id);

GRANT SELECT ON milvus.milvus_taxon_hu TO public; 
ALTER MATERIALIZED VIEW milvus.milvus_taxon_hu OWNER TO milvus_admin;


-- ro
DROP MATERIALIZED VIEW milvus.milvus_taxon_ro;
CREATE MATERIALIZED VIEW milvus.milvus_taxon_ro AS
SELECT DISTINCT ON (milvus_taxon.taxon_id) milvus_taxon.taxon_id,
    milvus_taxon.word,
    milvus_taxon.status
   FROM milvus_taxon
  WHERE milvus_taxon.lang = 'species_ro' AND milvus_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON milvus.milvus_taxon_ro (taxon_id);

GRANT SELECT ON milvus.milvus_taxon_ro TO public; 
ALTER MATERIALIZED VIEW milvus.milvus_taxon_ro OWNER TO milvus_admin;


-- en
DROP MATERIALIZED VIEW milvus.milvus_taxon_en;
CREATE MATERIALIZED VIEW milvus.milvus_taxon_en AS
SELECT DISTINCT ON (milvus_taxon.taxon_id) milvus_taxon.taxon_id,
    milvus_taxon.word,
    milvus_taxon.status
   FROM milvus_taxon
  WHERE milvus_taxon.lang = 'species_en' AND milvus_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON milvus.milvus_taxon_en (taxon_id);

GRANT SELECT ON milvus.milvus_taxon_en TO public; 
ALTER MATERIALIZED VIEW milvus.milvus_taxon_en OWNER TO milvus_admin;


-- euring
DROP MATERIALIZED VIEW milvus.milvus_taxon_euring;
CREATE MATERIALIZED VIEW milvus.milvus_taxon_euring AS
SELECT DISTINCT ON (milvus_taxon.taxon_id) milvus_taxon.taxon_id,
    milvus_taxon.word,
    milvus_taxon.status
   FROM milvus_taxon
  WHERE milvus_taxon.lang = 'species_euring' AND milvus_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON milvus.milvus_taxon_euring (taxon_id);

GRANT SELECT ON milvus.milvus_taxon_euring TO public; 
ALTER MATERIALIZED VIEW milvus.milvus_taxon_euring OWNER TO milvus_admin;

