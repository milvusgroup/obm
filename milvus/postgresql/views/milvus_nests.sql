BEGIN;
CREATE SCHEMA IF NOT EXISTS milvus;
ALTER TABLE milvus_nest_data SET SCHEMA milvus;

CREATE OR REPLACE VIEW public.milvus_nest_data AS
SELECT 
    -- COLUMNS FROM THE MAIN TABLE
    d.obm_id,
    d.obm_geometry,
    d.obm_uploading_id,
    d.obm_validation,
    d.obm_comments,
    d.obm_modifier_id,
    d.nest_name,
    d.geometry_accuracy,
    d.observer,
    d.grouping_code,
    d.sampling_unit,
    vf.start_date as first_date,
    vf.end_date AS destroyed_date,
    d.natural_artificial,
    d.nest_type,
    d.nest_place,
    d.nest_height,
    d.tree_species,
    d.trunk_diameter,
    d.exposure,
    d.protected_from_sun,
    d.protected_from_rain,
    d.distance_from_top_cliff,
    d.pylon_type,
    d.vegetation_type,
    d.nest_place_details,
    d.built_by_species,
    d.obm_access,
    d.obm_files_id,
    d.program_id,
    d.gc_id,
    d.su_id,
    d.program,
    d.nest_notes,
    d.observer_destroyed_nest,
    d.destruction_cause,
    d.altitude,
    d.x,
    d.y,
    d.address,
    string_agg(coalesce(vf2.start_date::varchar,''::varchar) || ' - ' || coalesce(vf2.end_date::varchar, ''::varchar), ' | ')::varchar as nest_availability
-- MAIN TABLE REFERENCE
FROM milvus.milvus_nest_data d
-- A JOIN STATEMENT
LEFT JOIN milvus.milvus_nest_availability vf ON (d.obm_id = vf.row_id AND actual = TRUE)
LEFT JOIN milvus.milvus_nest_availability vf2 ON (d.obm_id = vf2.row_id)
GROUP BY
    d.obm_id, d.obm_geometry, d.obm_uploading_id, d.obm_validation, d.obm_comments, d.obm_modifier_id, d.nest_name, 
    d.geometry_accuracy, d.observer, d.grouping_code, d.sampling_unit, vf.start_date, vf.end_date, d.natural_artificial, 
    d.nest_type, d.nest_place, d.nest_height, d.tree_species, d.trunk_diameter, d.exposure, d.protected_from_sun, 
    d.protected_from_rain, d.distance_from_top_cliff, d.pylon_type, d.vegetation_type, d.nest_place_details, 
    d.built_by_species, d.obm_access, d.obm_files_id, d.program_id, d.gc_id, d.su_id, d.program, d.nest_notes, 
    d.observer_destroyed_nest, d.destruction_cause, d.altitude, d.x, d.y, d.address
;
    
DROP RULE IF EXISTS milvus_nest_data_ins ON milvus_nest_data;
CREATE RULE milvus_nest_data_ins AS ON INSERT TO public.milvus_nest_data
    DO INSTEAD
    INSERT INTO milvus.milvus_nest_data (
        obm_geometry,obm_uploading_id,obm_validation,obm_comments,obm_modifier_id,nest_name,geometry_accuracy,observer,
        grouping_code,sampling_unit,first_date,destroyed_date,natural_artificial,nest_type,nest_place,nest_height,tree_species,
        trunk_diameter,exposure,protected_from_sun,protected_from_rain,distance_from_top_cliff,pylon_type,vegetation_type,
        nest_place_details,built_by_species,obm_access,obm_files_id,program_id,gc_id,su_id,program,nest_notes,
        observer_destroyed_nest,destruction_cause,altitude,x,y,address) 
    VALUES (
        NEW.obm_geometry,NEW.obm_uploading_id,NEW.obm_validation,NEW.obm_comments,NEW.obm_modifier_id,NEW.nest_name,
        NEW.geometry_accuracy,NEW.observer,NEW.grouping_code,NEW.sampling_unit,NEW.first_date,NEW.destroyed_date,
        NEW.natural_artificial,NEW.nest_type,NEW.nest_place,NEW.nest_height,NEW.tree_species,NEW.trunk_diameter,
        NEW.exposure,NEW.protected_from_sun,NEW.protected_from_rain,NEW.distance_from_top_cliff,NEW.pylon_type,
        NEW.vegetation_type,NEW.nest_place_details,NEW.built_by_species,NEW.obm_access,NEW.obm_files_id,NEW.program_id,
        NEW.gc_id,NEW.su_id,NEW.program,NEW.nest_notes,NEW.observer_destroyed_nest,NEW.destruction_cause,NEW.altitude,
        NEW.x,NEW.y,NEW.address
    ) RETURNING
        obm_id::int,obm_geometry,obm_uploading_id,obm_validation,obm_comments,obm_modifier_id,nest_name,geometry_accuracy,
        observer,grouping_code,sampling_unit,NULL::date,NULL::date,natural_artificial,nest_type,nest_place,nest_height,
        tree_species,trunk_diameter,exposure,protected_from_sun,protected_from_rain,distance_from_top_cliff,pylon_type,
        vegetation_type,nest_place_details,built_by_species,obm_access,obm_files_id,program_id,gc_id,su_id,program,nest_notes,
        observer_destroyed_nest,destruction_cause,altitude,x,y,address,NULL::varchar;
        
DROP RULE IF EXISTS milvus_nest_data_upd ON milvus_nest_data;
CREATE RULE milvus_nest_data_upd AS ON UPDATE TO public.milvus_nest_data
    DO INSTEAD
    UPDATE milvus.milvus_nest_data SET
        obm_geometry = NEW.obm_geometry,obm_uploading_id = NEW.obm_uploading_id,obm_validation = NEW.obm_validation,
        obm_comments = NEW.obm_comments,obm_modifier_id = NEW.obm_modifier_id,nest_name = NEW.nest_name,
        geometry_accuracy = NEW.geometry_accuracy,observer = NEW.observer,grouping_code = NEW.grouping_code,
        sampling_unit = NEW.sampling_unit,first_date = NEW.first_date,destroyed_date = NEW.destroyed_date,
        natural_artificial = NEW.natural_artificial,nest_type = NEW.nest_type,nest_place = NEW.nest_place,
        nest_height = NEW.nest_height,tree_species = NEW.tree_species,trunk_diameter = NEW.trunk_diameter,
        exposure = NEW.exposure,protected_from_sun = NEW.protected_from_sun,protected_from_rain = NEW.protected_from_rain,
        distance_from_top_cliff = NEW.distance_from_top_cliff,pylon_type = NEW.pylon_type,
        vegetation_type = NEW.vegetation_type,nest_place_details = NEW.nest_place_details,
        built_by_species = NEW.built_by_species,obm_access = NEW.obm_access,obm_files_id = NEW.obm_files_id,
        program_id = NEW.program_id,gc_id = NEW.gc_id,su_id = NEW.su_id,program = NEW.program,nest_notes = NEW.nest_notes,
        observer_destroyed_nest = NEW.observer_destroyed_nest,destruction_cause = NEW.destruction_cause,altitude = NEW.altitude,
        x = NEW.x,y = NEW.y,address = NEW.address 
    WHERE
         obm_id = NEW.obm_id;

DROP RULE IF EXISTS milvus_nest_data_del ON milvus_nest_data;
CREATE RULE milvus_nest_data_del AS ON DELETE TO public.milvus_nest_data
    DO INSTEAD
    DELETE FROM milvus.milvus_nest_data 
    WHERE
        obm_id = OLD.obm_id;

CREATE OR REPLACE FUNCTION milvus.milvus_nest_data_add_availability () RETURNS TRIGGER AS
$$
DECLARE
    av RECORD;
BEGIN
    IF tg_op = 'INSERT' THEN
        INSERT INTO milvus.milvus_nest_availability (row_id, start_date, end_date) VALUES (new.obm_id, new.first_date, new.destroyed_date);
        RETURN NEW;
    ELSIF tg_op = 'UPDATE' AND (
            new.first_date IS DISTINCT FROM old.first_date OR
            new.destroyed_date IS DISTINCT FROM old.destroyed_date ) 
        THEN
            UPDATE milvus.milvus_nest_availability SET start_date = new.first_date, end_date = new.destroyed_date WHERE row_id = new.obm_id AND actual = TRUE;
            RETURN NEW;
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
CREATE TRIGGER milvus_nest_data_add_availability_tr AFTER INSERT OR UPDATE ON milvus.milvus_nest_data FOR EACH ROW EXECUTE PROCEDURE milvus.milvus_nest_data_add_availability();

ALTER VIEW public.milvus_nest_data OWNER TO milvus_admin;