DROP VIEW tmp.erdeifules_monitoring_2016;
CREATE OR REPLACE VIEW tmp.erdeifules_monitoring_2016 AS
SELECT milvus.obm_id,
    public.st_centroid(milvus.obm_geometry) AS geom,
    milvus.species,
    milvus.number
   FROM public.milvus
  WHERE (((milvus.national_program)::text = 'str_wintering_long_eared_owl_monitoring_program'::text) AND ((milvus.date >= '2015-11-01'::date) AND (milvus.date <= '2016-02-15'::date)));
GRANT ALL ON tmp.erdeifules_monitoring_2016 TO milvus_admin;

DROP VIEW tmp.erdeifules_monitoring_2017;
CREATE OR REPLACE VIEW tmp.erdeifules_monitoring_2017 AS
SELECT milvus.obm_id,
    public.st_centroid(milvus.obm_geometry) AS geom,
    milvus.species,
    milvus.number
   FROM public.milvus
  WHERE (((milvus.national_program)::text = 'str_wintering_long_eared_owl_monitoring_program'::text) AND ((milvus.date >= '2016-11-01'::date) AND (milvus.date <= '2017-02-15'::date)));
GRANT ALL ON tmp.erdeifules_monitoring_2017 TO milvus_admin;

DROP VIEW tmp.erdeifules_monitoring_2018;
CREATE OR REPLACE VIEW tmp.erdeifules_monitoring_2018 AS
SELECT milvus.obm_id,
    public.st_centroid(milvus.obm_geometry) AS geom,
    milvus.species,
    milvus.number
   FROM public.milvus
  WHERE (((milvus.national_program)::text = 'str_wintering_long_eared_owl_monitoring_program'::text) AND ((milvus.date >= '2017-11-01'::date) AND (milvus.date <= '2018-02-15'::date)));
GRANT ALL ON tmp.erdeifules_monitoring_2018 TO milvus_admin;


DROP VIEW tmp.erdeifules_monitoring_2019;
CREATE OR REPLACE VIEW tmp.erdeifules_monitoring_2019 AS
SELECT milvus.obm_id,
    public.st_centroid(milvus.obm_geometry) AS geom,
    milvus.species,
    milvus.number
FROM public.milvus
WHERE (((milvus.national_program)::text = 'str_wintering_long_eared_owl_monitoring_program'::text) AND ((milvus.date >= '2018-11-01'::date) AND (milvus.date <= '2019-02-15'::date)));
GRANT ALL ON tmp.erdeifules_monitoring_2019 TO milvus_admin;

DROP VIEW tmp.erdeifules_monitoring_2020;
CREATE OR REPLACE VIEW tmp.erdeifules_monitoring_2020 AS
SELECT milvus.obm_id,
    public.st_centroid(milvus.obm_geometry) AS geom,
    milvus.species_valid,
    milvus.species_hu,
    milvus.species_ro,
    milvus.species_en,
    CASE WHEN number IS NULL THEN 0 ELSE number END as number
FROM public.milvus
WHERE (
    ((milvus.national_program)::text = 'str_wintering_long_eared_owl_monitoring_program'::text) AND 
    (
        (milvus.date >= '2019-11-01'::date) AND 
        (milvus.date <= '2020-02-15'::date)
    )
);
GRANT ALL ON tmp.erdeifules_monitoring_2020 TO milvus_admin;

DROP VIEW tmp.erdeifules_monitoring_2021;
CREATE OR REPLACE VIEW tmp.erdeifules_monitoring_2021 AS
SELECT milvus.obm_id,
    public.st_centroid(milvus.obm_geometry) AS geom,
    milvus.species_valid,
    milvus.species_hu,
    milvus.species_ro,
    milvus.species_en,
    CASE WHEN number IS NULL THEN 0 ELSE number END as number
FROM public.milvus
WHERE (
    ((milvus.national_program)::text = 'str_wintering_long_eared_owl_monitoring_program'::text) AND 
    (
        (milvus.date >= '2020-11-01'::date) AND 
        (milvus.date <= '2021-02-15'::date)
    )
);
GRANT ALL ON tmp.erdeifules_monitoring_2021 TO milvus_admin;

DROP VIEW openbirdmaps.erdeifules_monitoring_2022;
CREATE OR REPLACE VIEW openbirdmaps.erdeifules_monitoring_2022 AS
SELECT milvus.obm_id,
    public.st_centroid(milvus.obm_geometry) AS geom,
    milvus.species_valid,
    milvus.species_hu,
    milvus.species_ro,
    milvus.species_en,
    CASE WHEN number IS NULL THEN 0 ELSE number END as number
FROM public.milvus
WHERE (
    ((milvus.national_program)::text = 'str_wintering_long_eared_owl_monitoring_program'::text) AND 
    (
        (milvus.date >= '2021-11-01'::date) AND 
        (milvus.date <= '2022-02-15'::date)
    )
);
