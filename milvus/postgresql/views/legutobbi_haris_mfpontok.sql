DROP VIEW tmp.legutobbi_haris_mfpontok;
CREATE OR REPLACE VIEW tmp.legutobbi_haris_mfpontok AS
WITH foo AS (
         SELECT milvus.national_sampling_unit,
            milvus.national_grouping_code,
            max(milvus.date) AS d
           FROM public.milvus
          WHERE (((milvus.national_program)::text = 'str_nocturnal_monitoring_program'::text) AND (milvus.date > '2016-01-01'::date))
          GROUP BY milvus.national_sampling_unit, milvus.national_grouping_code
        )
 SELECT ae.alapegyseg,
    ae.obm_geometry,
    bar.national_sampling_unit,
    bar.date,
        CASE
            WHEN (bar.time_of_start IS NOT NULL) THEN to_char((format('%s minute'::text, bar.time_of_start))::interval, 'HH24:MI'::text)
            ELSE NULL::text
        END AS time_of_start,
    baz.gykod AS national_grouping_code
   FROM ((public.milvus_sampling_units ae
     LEFT JOIN ( SELECT DISTINCT milvus.national_sampling_unit,
            milvus.date,
            milvus.time_of_start
           FROM public.milvus
          WHERE (((milvus.national_sampling_unit)::text, milvus.date) IN ( SELECT foo.national_sampling_unit,
                    foo.d
                   FROM foo))) bar ON (((ae.alapegyseg)::text = (bar.national_sampling_unit)::text)))
     LEFT JOIN ( SELECT milvus_grouping_codes.obm_id,
            milvus_grouping_codes.gykod
           FROM public.milvus_grouping_codes
          WHERE ((milvus_grouping_codes.gykod)::text IN ( SELECT DISTINCT foo.national_grouping_code
                   FROM foo))) baz ON ((ae.gykod_id = baz.obm_id)))
  WHERE ((ae.program = 9) AND (baz.obm_id IS NOT NULL))
  ORDER BY baz.gykod, bar.date,
        CASE
            WHEN (bar.time_of_start IS NOT NULL) THEN to_char((format('%s minute'::text, bar.time_of_start))::interval, 'HH24:MI'::text)
            ELSE NULL::text
        END;
        
GRANT ALL ON tmp.legutobbi_haris_mfpontok TO milvus_admin;
