DROP VIEW openbirdmaps.uploadings_metadata;
CREATE OR REPLACE VIEW openbirdmaps.uploadings_metadata AS 
SELECT
    id as uploading_id,
    uploading_date,
    uploader_name,
    project_table as data_table,
    metadata->>'id' as mid,
    metadata->>'app_version' as app_version,
    to_timestamp((metadata->>'started_at')::bigint/1000) as started_at,
    to_timestamp((metadata->>'finished_at')::bigint/1000) as finished_at,
    metadata->>'form_version' as form_version,
    metadata->>'gps_accuracy' as gps_accuracy,
    metadata->>'observation_list_id' as observation_list_id,
    to_timestamp((metadata->>'observation_list_start')::bigint/1000) as observation_list_start,
    to_timestamp((metadata->>'observation_list_end')::bigint/1000) as observation_list_end,
    metadata->>'observation_list_null_record' as observation_list_null_record,
    metadata->>'measurements_num' as measurements_num,
    metadata->'measurement_track_log' as measurement_track_log,
    metadata->'observation_list_track_log' as observation_list_track_log,
    metadata->'recorded_session_tracklog' as recorded_session_tracklog,
    metadata->'tracklog_info' as tracklog_info
FROM system.uploadings
WHERE project = 'milvus' AND metadata IS NOT NULL;

