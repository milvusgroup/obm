<?php
$ref =  sprintf('%s://%s',$protocol,URL);
$hidden = (get_option('project_title') == 'hide');
$title_rgb = (defined("TITLE_RGB")) ? constant('TITLE_RGB') : '159,198,81';
$titlew = mb_strlen($OB_project_title)*27;

if (file_exists(getenv('PROJECT_DIR').'local/styles/app/images/logo.png'))
    $logo = 'local/styles/app/images/logo.png';
else
    $logo = STYLE_PATH.'/images/logo.png';

?>

<div id='maintenance'><?php echo $maintenance_message ?></div>
<div id='bheader'>
    <div class='htitle'>
        <p>
            <a href="<?= $protocol ?>://<?= URL ?>" >
                <img src="<?php echo $protocol ?>://<?php echo URL.STYLE_PATH ?>/images/logo.png" class="main-logo-img">
                <?php if (!$hidden): ?>
                    <svg
                        width="100%"
                        height="50"
                        viewBox="0 0 <?= $titlew ?> 50"
                        preserveAspectRatio="xMinYMid meet"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                    >
                        <text x="10" y="38" font-size="48" fill=rgb(<?= $title_rgb ?>) ><?= $OB_project_title ?></text>
                    </svg>
                <?php endif; ?>
            </a>
        </p>
    </div>
<!--Register / Login -->
<?php

    $doclang = ($_SESSION['LANG'] == 'hu') ? 'hu' : 'en';

    if (isset($_SESSION['Tcrypt'])) $user = $_SESSION['Tcrypt'];
    else $user = 0;

    $profile = "profile/$user/";

    if (!isset($_SESSION['Tid'])) {

        $profile_menu = "
    <li style='border-bottom:1px solid lightgray'><a href='".$ref."/?login' id='login'><span class='navlabel'>".t(str_login)."</span></a></li>
    <li><a href='https://openbiomaps.org/documents/$doclang/'><span class='navlabel'>".t(str_documentation)."</span></a></li>
    <li><a href='".$ref."/index.php?database'><span class='navlabel'>".t(str_db_summary)."</span></a></li>
    <li><a href='$protocol://".OB_DOMAIN."/projects/'><span class='navlabel'>".t(str_other_databases)."</span></a></li>";
        $message_out = "";
        $profile_title = str_login;

    }
    else {

        $M = new Messenger();
        $profile_title = $_SESSION['Tname']; 
        $unread = $M->get_total_unread_number();
        $message_out = ($unread > 0) 
            ? "<a href='$ref/boat/?messages' style='position:relative' id='header-unread-envelope' title='".sprintf(str_you_have_new_message,$unread)."'><i class='fa fa-envelope'></i> 
    <span style='position: absolute;
    right: 0;
    z-index: 1;height:16px;
    display:inline-block;
    color: white;
    top: 20px;
    border-radius: 8px;
    padding: 1px 5px;
    background: #83781b;
    font-size: 11px;
    line-height: 12px;
    font-weight: 600;'>$unread</span></a>&nbsp;" : "";
            
    $profile_menu = "
    <li class='nohover' style='font-weight:bold;border-bottom: 1px solid lightgray;text-align: center;'><span style='line-height: 3rem;'>{$_SESSION['Tname']}</span></li>
    <li><a href='$ref/$profile'><span class='navlabel'>".t(str_settings)."</span></a></li>
    <li><a href='".$ref."/index.php?database'><span class='navlabel'>".t(str_db_summary)."</span></a></li>
    <li style='border-bottom: 1px solid lightgray;'><a href='".$ref."/index.php?feedback' id='feedback'><span class='navlabel'>".t(str_feedback)."</span></a></li>
    <li><a href='https://openbiomaps.org/documents/$doclang/'><span class='navlabel'>".t(str_documentation)."</span></a></li>
    <li><a href='$protocol://".OB_DOMAIN."/projects/'><span class='navlabel'>".t(str_other_databases)."</span></a></li>
    <li style='border-top: 1px solid lightgray;'><a href='".$ref."/index.php?logout' id='logout'><span class='navlabel'>".t(str_logout)." <i class='fa fa-sign-out fa-lg'></i></span></a></li>";

    }
?>
    <!-- Navigation -->    
    <div id='header-menu-div'>
    <div id='nav'>
     <ul class='topnav tablink'>
        <li><a href='<?php echo $ref ?>/upload/'><div class='navlabel'><i class='fa fa-upload fa-fw'></i> <span class='navlabel-text'><?php echo t(str_upload) ?></span></div></a></li>
        <li><a href='<?php echo $ref ?>/map/'><div class='navlabel'><i class='fa fa-globe fa-fw'></i> <span class='navlabel-text'><?php echo t(str_map) ?></span></div></a></li>
        <li><?php echo $message_out ?></li>
        <li><a href='#' class='to' title="<?php echo $profile_title ?>"><?php

    if (!isset($_SESSION['Tid']))
        echo "<i class='fa fa-navicon'></i> <span class='navlabel-text'>".t(str_login)."</span>";
    else
        echo "<img src='$protocol://".URL."/includes/avatar.php?size=32&value={$_SESSION['Tmail']}&bg=225,225,225' style='vertical-align:top;margin-top:-10px;border:1px dashed #dadada;border-radius:8px'>";

    ?><i class='fa fa-angle-down fa-fw fa-lg'></i></a>
          <ul class='subnav'>
                <?php echo $profile_menu ?>
          </ul></li>
    <?php if (count(LANGUAGES) > 1): ?>
            
        <li><a href='#' class='to'><span class="flag-icon flag-icon-<?= $_SESSION['LANG']; ?>"></span><i class='fa fa-angle-down fa-fw fa-lg'></i></a>
            <ul class="subnav">
            <?php
                
                foreach (LANGUAGES as $L=>$label) {
                    //if ( file_exists(sprintf("%slanguages/%2s.php",getenv('PROJECT_DIR'),$L))) 
                        echo "<li><a href='?lang=$L'><span class='flag-icon flag-icon-$L'></span>$label</a></li>";
                }
            ?>
            </ul>
        </li>
        
    <?php endif; ?>
        
    </ul></div>
    </div>
</div>
