import os
import psycopg2
from psycopg2.extras import execute_batch
from dotenv import load_dotenv


# Load the environment variables from .env file
load_dotenv()

def sync_tables (source_table, dest_table, source_table_schema = 'public', dest_table_schema = 'public', source_table_id = 'obm_id', dest_table_id = 'obm_id') :
    # Retrieve the connection string variables from environment
    source_host = os.getenv('SOURCE_HOST')
    source_port = os.getenv('SOURCE_PORT')
    source_db = os.getenv('SOURCE_DB')
    source_user = os.getenv('SOURCE_USER')
    source_password = os.getenv('SOURCE_PASSWORD')

    dest_host = os.getenv('DEST_HOST')
    dest_port = os.getenv('DEST_PORT')
    dest_db = os.getenv('DEST_DB')
    dest_user = os.getenv('DEST_USER')
    dest_password = os.getenv('DEST_PASSWORD')

    # Connect to the source database
    source_conn = psycopg2.connect(host=source_host, port=source_port, dbname=source_db,
                                   user=source_user, password=source_password)
    source_cursor = source_conn.cursor()

    # Connect to the destination database
    dest_conn = psycopg2.connect(host=dest_host, port=dest_port, dbname=dest_db,
                                 user=dest_user, password=dest_password)
    dest_cursor = dest_conn.cursor()

    # Fetch the maximum timestamp or incremental ID from the destination table
    dest_cursor.execute(f"SELECT COALESCE(MAX({dest_table_id}), 0) FROM {dest_table_schema}.{dest_table}")
    last_sync_value = dest_cursor.fetchone()[0]

    # Fetch the column names of the source table
    source_cursor.execute("SELECT column_name FROM information_schema.columns WHERE table_schema = %s AND table_name= %s", (source_table_schema, source_table))
    source_columns = [column[0] for column in source_cursor.fetchall()]

    # Fetch the column names of the destination table
    dest_cursor.execute("SELECT column_name FROM information_schema.columns WHERE table_schema=%s AND table_name=%s", (dest_table_schema, dest_table))
    dest_columns = [column[0] for column in dest_cursor.fetchall()]

    # Check for differences in column sets
    source_only_columns = set(source_columns) - set(dest_columns)
    dest_only_columns = set(dest_columns) - set(source_columns)

    if source_only_columns or dest_only_columns:
        error_message = "Columns mismatch between source_table and dest_table.\n"
        if source_only_columns:
            error_message += f"Columns in source_table but not in dest_table: {source_only_columns}\n"
        if dest_only_columns:
            error_message += f"Columns in dest_table but not in source_table: {dest_only_columns}\n"
        raise ValueError(error_message)

    # Identify the columns that exist in both tables
    common_columns = list(set(source_columns).intersection(dest_columns))
    if source_table == 'uploadings':
        common_columns.remove('metadata')

    # Fetch the modified or new rows from the source table
    source_cursor.execute("SELECT \"" + "\", \"".join(common_columns) + f"\" FROM {source_table_schema}.{source_table} WHERE {source_table_id} > %s", (last_sync_value,))
    source_data = source_cursor.fetchall()

    # Build the INSERT query with column names
    placeholders = ', '.join(['%s'] * len(common_columns))
    insert_query = f"INSERT INTO {dest_table_schema}.{dest_table} (\"" + "\", \"".join(common_columns) + f"\") VALUES ({placeholders})"

    # Insert the modified or new rows into the destination table in batches
    batch_size = 1000
    for i in range(0, len(source_data), batch_size):
        batch = source_data[i:i+batch_size]
        execute_batch(dest_cursor, insert_query, batch)

    # Commit the changes
    dest_conn.commit()

    # Close the cursors and connections
    source_cursor.close()
    source_conn.close()
    dest_cursor.close()
    dest_conn.close()


sync_tables('milvus', 'milvus', 'milvus', 'public')
sync_tables('uploadings', 'uploadings', 'system', 'system', 'id', 'id')
sync_tables('milvus_qgrids', 'milvus_qgrids', 'public', 'public', 'row_id', 'row_id')
sync_tables('milvus_projects', 'milvus_projects')
sync_tables('milvus_method', 'milvus_method')
sync_tables('milvus_program', 'milvus_program')
sync_tables('milvus_project_programs', 'milvus_project_programs')
sync_tables('milvus_grouping_codes', 'milvus_grouping_codes')
sync_tables('milvus_sampling_units', 'milvus_sampling_units')
