<?php
class incomplete_observation_lists extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('incomplete_observation_lists initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID;
        
        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing');
            return;
        }

        foreach ($params as $table => $options) {
            extract((array)$options);
            if (!isset($mail_to) || !is_numeric($mail_to) || !isset($diff_tolerance) || !isset($days_offset)) {
                job_log('vm parameters missing');
                return;
            }
            //check is the messages module is included
            if ( ! class_exists('Messenger')) {
                job_log('Messenger class not included!');
                return;
            }
            if ( ! class_exists('Role') ) {
                job_log('Messenger class not included!');
                return;
            }
            $m = new Messenger();
            $role = new Role($mail_to);
            
            $mt = new MessageTemplate('obs_list_not_complete');
            $dict = [
                'user_name' => 'Pista', 
                'list_id' => 'asdrew-sfaswr-a24dsf-wrlk123', 
            ];
            
            job_log( $mt->get('subject','en',  $dict) );
            job_log( $mt->get('message','en',  $dict) );
            return;
            
            
            
            $recipients = $role->get_members();
            
            $tmptable = "{$table}_obm_obsl";
            
            //get the unprocessed lists with observations
            $lists = observation_lists::get_unprocessed_lists( $table );
            if ($lists === 'error') {
                job_log('observation_lists failed 2');
                return;
            }
            //get the unprocessed null lists
            $nlists = observation_lists::get_unprocessed_null_lists( $table );
            if ($nlists === 'error') {
                job_log('observation_lists failed 3');
                return;
            }

            $lists = array_merge($lists, $nlists);
            
            foreach ($lists as $list) {
                $upl_date = new DateTime($list['uploading_date']);
                if ($upl_date->diff(new DateTime(),true)->days < (int)$days_offset) {
                    continue;
                }

                // Number of list records counted by the mobile app
                $app_count = (int)$list['measurements_num'];
                
                // Number of list records uploaded (the difference can be due to deleted record after saving the list or network error??)
                $uploaded_count = count(explode(',',$list['observation_list_elements']));
                
                $diff = $app_count - $uploaded_count;
                
                
                if ( $diff === 0 ) {
                    // this case is processed by the observation_lists job
                    continue;
                }
                elseif ($diff <= (int)$diff_tolerance) {
                    // if the difference is smaller than the tolerance set in the parameters the list will be processed, but a notification is sent
                    $cmd = sprintf("UPDATE system.uploadings SET metadata = jsonb_set(metadata::jsonb,ARRAY['measurements_num'],'%d'::jsonb)::json where id = %d;", 
                        $uploaded_count,
                        $list['uploading_id']
                    );
                    if ($res = pg_query($ID,$cmd)) {
                        $message = "observation list {$list['observation_list_id']} not uploaded completly, but processed";
                        foreach ($recipients as $to) {
                            $m->send_system_message($to, $message, "Dear {$to->name}", $message, true);
                        }
                    }
                }
                else {
                    $subject_contains = $list['observation_list_id'];
                    $limit = 1;
                    $notifications_sent = $m->get_system_messages( compact('subject_contains', 'limit') );
        
                    if (count($notifications_sent) === 0) {
                        $message = "observation list {$list['observation_list_id']} not uploaded completly";
                        foreach ($recipients as $to) {
                            $m->send_system_message($to, $message, "Dear {$to->name}", $message, true);
                        }
                    }
                }
            }
        }
    }
}
?>
