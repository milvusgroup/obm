<?php
/**
  * The purpose of this job is to keep the main_table and the taxon table in syncron
  **/
class species_name_validation extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('species_name_validation initialized', __FILE__, __LINE__);
        return true;
    }

    public function get_results() {
        global $ID;
        
        $params = parent::getJobParams(__CLASS__);
        return '<pre> TODO </pre>';
    }

    static function run() {
        global $ID;
        
        $params = parent::getJobParams(__CLASS__);
        
        if (!isset($params->src_column)) {
          job_log(__CLASS__ . ' ERROR: Please define the "src_column" parameter - the source species column!');
          return;
        }
        if (!in_array($params->src_column, getColumnsOfTables())) {
          job_log(__CLASS__ . ' ERROR: Invalid column name: ' . $params->src_column);
          return;
        }
        
        $source_lang = $params->src_column;
        
        $st_col = st_col('default_table', 'array', 0);
        
        $languages = [
          $st_col['SPECIES_C_SCI'] => $st_col['SPECIES_C_SCI']
        ];
        if ($st_col['SPECIES_C'] !== $st_col['SPECIES_C_SCI']) {
          $languages[$st_col['SPECIES_C']] = $st_col['SPECIES_C'];
        }
        foreach ($st_col['ALTERN_C'] as $col) {
          $languages[$col] = $col;
        }
        
        $cmd = [];
        foreach ($languages as $taxon_lang) {
          // IMPORTANT!!! source species column can not be modified
          if ($taxon_lang === $source_lang) {
            continue;
          }
          // Updating the automatic species columns based on the taxon_table
          $c = sprintf("WITH rows AS (
                    SELECT DISTINCT f.%1\$s, foo.valid_word 
                    FROM %4\$s f 
                    LEFT JOIN (
                        SELECT 
                          t.word as src_word, 
                          CASE WHEN tt.word IS NULL 
                            THEN fb.word 
                            ELSE tt.word 
                          END as valid_word 
                        FROM %4\$s_taxon t 
                        LEFT JOIN 
                          %4\$s_taxon tt ON t.taxon_id = tt.taxon_id AND tt.status = 'accepted'
                        LEFT JOIN -- fallback lang is the scientific name
                          %4\$s_taxon fb ON t.taxon_id = fb.taxon_id AND fb.lang = '%2\$s' AND fb.status = 'accepted'
                        WHERE 
                          tt.lang = '%3\$s' AND tt.word IS NOT NULL
                    ) foo ON f.%1\$s = foo.src_word 
                    WHERE f.%3\$s IS DISTINCT FROM foo.valid_word) -- AND foo.valid_word IS NOT NULL) 
                UPDATE %4\$s ff SET %3\$s = rows.valid_word 
                FROM rows 
                WHERE rows.%1\$s = ff.%1\$s AND ff.%3\$s IS DISTINCT FROM rows.valid_word;
          ", $source_lang, $st_col['SPECIES_C_SCI'], $taxon_lang, PROJECTTABLE);
          
          $cmd[$taxon_lang . '_validate'] = $c;
          
          if (isset($params->count_observations)) {
              // recalculating the taxon_db column of the taxon_table
              $cmd[$taxon_lang . '_count'] = sprintf("WITH counts AS (
                  SELECT word, CASE WHEN c IS NULL THEN 0 ELSE c END as c from %1\$s_taxon 
                  LEFT JOIN (SELECT %2\$s, count(*) as c FROM %1\$s GROUP BY %2\$s) foo ON %2\$s = word WHERE lang = %3\$s
                  ) 
                  UPDATE %1\$s_taxon t SET taxon_db = counts.c FROM counts WHERE t.word = counts.word AND t.lang = %3\$s;",
                  PROJECTTABLE,
                  "\"$taxon_lang\"",
                  quote($taxon_lang)
              );
          }
        }
        
        if (!$res = query($ID, $cmd)) {
            job_log(pg_last_error($ID));
            exit;
        }

        $aff_rows = pg_affected_rows($res[0]);
        job_log("Validált fajnevek száma: $aff_rows");
        
        if (isset($params->refreshMatViews) && count($st_col['NATIONAL_C']) > 0) {
            $cmd = array_values(array_map(function($col) use ($st_col) {
                return sprintf("REFRESH MATERIALIZED VIEW temporary_tables.%s_taxon_%s_%s;", PROJECTTABLE, $col, $st_col['SPECIES_C_SCI']);
            }, $st_col['NATIONAL_C']));
            query($ID, $cmd);
        }
        
        if (isset($params->vacuumTaxonTable)) {
            $cmd = sprintf("VACUUM %s_taxon;", PROJECTTABLE);
            pg_query($ID, $cmd);
        }
        exit;
    }
}
?>
