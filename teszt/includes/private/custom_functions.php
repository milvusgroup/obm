<?php 
function custom_script_tags() {
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $url = URL;
    $out = "<script type='text/javascript' src='$protocol://$url/js/private/jquery.multi-select.js'> </script>";
    $out .= "<script type='text/javascript' src='$protocol://$url/js/private/jquery.quicksearch.js'> </script>";
    
    echo $out;
}
?>