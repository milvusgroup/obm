if (typeof etr === "undefined") {
    var etr = ""
}

$(document).ready(function() {
    var l = new Vue({
        el: '#hrsz_query_multiselect',
        data () {
            return {
                function_name: 'hrsz_query',
                speedup_filter: 'on',
                spatial_function: 'within',
                parent_value: '',
                child_value: '',
                etr: ''
            }
        },
        template: `
        <div>
            <input id='judet' class='qfautocomplete qfc' placeholder='Megye' data-etr='SLGXBnh9C3i2TUdSNZ0HfY6bVgEfXCM+5cmue3LL4EtiP8nQLhmTAEya7RkuC1uZ' data-all_options='on' v-model="parent_value">
            <select id="name" data='custom_polygon_id' class='qfc custom_polygon_id' style='width:100%' multiple='multiple' size=6></select>
            <multiselect v-model="value" :options="options" :multiple="true" :close-on-select="false" :clear-on-select="false" :preserve-search="true" placeholder="Pick some" label="name" track-by="name" :preselect-first="true">
                <template slot="selection" slot-scope="{ values, search, isOpen }"><span class="multiselect__single" v-if="values.length &amp;&amp; !isOpen">{{ values.length }} options selected</span></template>
            </multiselect>
        </div>
        `
    });
    skip_loadQueryMap_customBox = 1;
    /*
    var cpv_id = $("#select_with_'.$this->function_name.'_polygon").closest(".mapfb").find(".custom_polygon_id").attr("id");
    $("#'.$this->id.'").multiselect({
        searchField: true,
        availableListPosition: "left",
        splitRatio: 0.5,
    });
    let polygon_ids = [];
    $("#'. $this->id.'").bind("multiselectChange", async function(ev, item){
        if (item.selected) {
            item.optionElements.forEach( function (element) {
                polygon_ids.push(element.dataset.id);
            });
        }
        else {
            item.optionElements.forEach( function (element) {
                polygon_ids.splice(polygon_ids.indexOf(element.dataset.id))
            });
        }
        drawLayer.destroyFeatures();
        
        etr = $("#select_with_'.$this->function_name.'_polygon").data("etr");
        console.log(polygon_ids);
        let polygon_wkts = await $.post("ajax", {getWktGeometries: polygon_ids, custom_table:etr});
        polygon_wkts = JSON.parse(polygon_wkts);
        Object.keys(polygon_wkts).forEach(function(k){
            // draw polygon on map
            drawPolygonFromWKT(JSON.stringify(polygon_wkts[k]),1);
        });
    });
    $("#mapfilters").on("click", "#'.$this->function_name.'_info", function() {
        $( "#dialog" ).text("'.get_option( $this->function_name . '_info').'");
        $( "#dialog" ).dialog( "open" );
    });
    $("#"+cpv_id).on("nested_data_appended", function () {
        $("#'.$this->id.'").multiselect("refresh");
    })
    */
});

// this function name will be automaticall processed by its  name

function custom_hrsz_query () {
    var qids = new Array();
    var qval = new Array();
    $(".qfc").each(function(){
        var stri = "";
        var stra = new Array();
        var qid=$(this).attr("id");
        if ( $(this).prop("type") == "text" ) {
            stra.push($(this).val());
        } else if ($(this).prop("type") == "button" ) {
            stra.push(($(this).find("i").hasClass("fa-toggle-on")) ? "on" : "");
        } else {
            $("#" + qid + " option:selected").each(function () {
                stra.push($(this).val());
            });
        }
        if (stra.length) {
            qids.push(qid);
            qval.push(JSON.stringify(stra));
        }
    });
    
    var myVar = { geom_selection:"custom_polygons",spatial_function:$("#spatial_function").val(),custom_table:etr }
    
    for (i=0;i<qids.length;i++) {
        myVar["qids_" + qids[i]] = qval[i];
    }
    
    return myVar;
}
