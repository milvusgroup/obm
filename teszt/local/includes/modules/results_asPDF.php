<?php

/**
 * Class results_asPDF_CM
 * @author yourname
 */
class results_asPDF_CM {
    /**
     * undocumented function
     *
     * @return void
     */
    public static function prepare_report_fisa_de_evidenta($de)
    {
        $st_col = $_SESSION['st_col'];
        $trimesters = [
            [ 'no' => 4, 'from' => new DateTime((date('Y') - 1). '-10-01'), 'until' => new DateTime((date('Y') - 1). '-12-31') ],
            [ 'no' => 1, 'from' => new DateTime(date('Y'). '-01-01'), 'until' => new DateTime(date('Y'). '-03-31') ],
            [ 'no' => 2, 'from' => new DateTime(date('Y'). '-04-01'), 'until' => new DateTime(date('Y'). '-06-30') ],
            [ 'no' => 3, 'from' => new DateTime(date('Y'). '-07-01'), 'until' => new DateTime(date('Y'). '-09-30') ]
        ];
        $trim = $trimesters[ceil(date('m') / 3) - 1];

        $data = $de->as_array();

        $events = self::load_rehab_events($trim['from']->format('Y-m-d'), $trim['until']->format('Y-m-d'));
        $patient_ids = array_unique(array_column($events, 'patient_id'));

        return [
            'error' => false,
            'species_no' => count(array_unique(array_column($data, $st_col['SPECIES_C']))),
            'indiv_no' => array_sum(array_column($data, $st_col['NUM_IND_C'])),
            'intrari' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_entry');
            })),
            'decedati' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_death');
            })),
            'eutanasie' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_euthanasia');
            })),
            'eliberat' => count(array_filter($events, function ($ev) {
                return ($ev['event'] === 'str_released');
            })),
            'trimester' => $trim['no'],
            'from' => $trim['from']->format('d.m.Y.'),
            'until' => $trim['until']->format('d.m.Y.'),
            'year' => (date('m') < 4) ? date('Y') - 1 : date('Y'),
            'collaborators' => [
                'Asociația Vets4Wild',
                'ACDB (Asociația pentru Conservarea Diversității Biologice)',
                'FundațiaVisul Luanei - Luana’s dream',
                'IDNR (Iniţiativa "Drepneaua neagră" România)',
                'USAMV Cluj Napoca',
                'Asociația Prietenii Berzelor',
            ],
            'patient_list' => self::load_rehab_patient_list($patient_ids),
            'phone_calls' => self::count_phone_calls($trim['from']->format('Y-m-d'), $trim['until']->format('Y-m-d')),
            'location_data' => self::load_rehab_locations($patient_ids)
        ];
    }

    /**
     * loads the rehab events between two dates
     *
     * @return array
     */
    private static function load_rehab_events($from, $until)
    {
        global $ID;
        $cmd = sprintf("SELECT * FROM %s_events WHERE date BETWEEN %s AND %s", PROJECTTABLE, quote($from), quote($until));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return [];
        }
        return (pg_num_rows($res) == 0) ? [] : pg_fetch_all($res);
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function load_rehab_patient_list($ids) {
        global $ID;
        $st_col = $_SESSION['st_col'];
        $cmd = sprintf("SELECT ROW_NUMBER() OVER() as nr, %1\$s as sci_name, %2\$s as rom_name, count(*) as count FROM %3\$s WHERE obm_id IN (%4\$s) GROUP BY %1\$s, %2\$s",$st_col['SPECIES_C_SCI'], $st_col['NATIONAL_C']['ro'], PROJECTTABLE, implode(',', $ids));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return [];
        }
        return (pg_num_rows($res) == 0) ? [] : pg_fetch_all($res);
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function load_rehab_locations($ids) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT count(DISTINCT finding_county) as counties, count(DISTINCT finding_location) as locations FROM %s WHERE obm_id IN (%s)", PROJECTTABLE, implode(',', $ids));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return [];
        }
        return (pg_num_rows($res) == 0) ? [] : pg_fetch_assoc($res);
    }
    

    /**
     * undocumented function
     *
     * @return integer
     */
    public function count_phone_calls($from, $until) {
        global $ID;
        $cmd = sprintf("SELECT sum(call_count) as cnt FROM %s_phone_calls WHERE date BETWEEN %s AND %s;", PROJECTTABLE, quote($from), quote($until));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return [];
        }
        if (pg_num_rows($res) == 0) {
            return 0;
        }
        else {
            $results = pg_fetch_assoc($res);
            return (int)$results['cnt'];
        }
    }
    
}
