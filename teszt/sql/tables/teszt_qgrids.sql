CREATE TABLE teszt_qgrids ( row_id integer NOT NULL, original geometry, etrs10_geom geometry, etrs10_name character varying );
ALTER TABLE ONLY teszt_qgrids ADD CONSTRAINT teszt_qgrids_pkey PRIMARY KEY (row_id);
INSERT INTO teszt_qgrids (row_id) SELECT obm_id FROM teszt;
UPDATE teszt_qgrids SET 
    etrs10_geom = foo.geometry,
    etrs10_name = foo.name
FROM (
   SELECT d.obm_id,k.geometry, k.name FROM teszt d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,k.geometry))
) as foo
WHERE row_id=foo.obm_id;


ALTER TABLE teszt_qgrids ADD COLUMN megye varchar NULL;
ALTER TABLE teszt_qgrids ADD COLUMN kozseg varchar NULL;
