CREATE SEQUENCE public.teszt_observation_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;

CREATE TABLE public.teszt_observation_list (
    id integer DEFAULT nextval('public.teszt_observation_id_seq'::regclass) NOT NULL,
    oidl character varying(40),
    uploading_id integer,
    obsstart timestamp without time zone,
    obsend timestamp without time zone,
    nulllist boolean
);

ALTER TABLE ONLY public.teszt_observation_list
    ADD CONSTRAINT teszt_observation_pkey PRIMARY KEY (id);
