CREATE OR REPLACE FUNCTION "update_teszt_taxonmeta_table" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF tg_op = 'INSERT' THEN
        INSERT INTO teszt_taxonmeta (taxon_id) VALUES (new.taxon_id) ON CONFLICT DO NOTHING;
        RETURN new;
    ELSIF tg_op = 'UPDATE' AND old.taxon_id != new.taxon_id THEN
        INSERT INTO teszt_taxonmeta (taxon_id) VALUES (new.taxon_id) ON CONFLICT DO NOTHING;
        DELETE FROM teszt_taxonmeta WHERE taxon_id = old.taxon_id AND NOT EXISTS (SELECT 1 FROM teszt_taxon WHERE taxon_id = old.taxon_id);
        RETURN new;
    ELSIF tg_op = 'DELETE' THEN
        DELETE FROM teszt_taxonmeta WHERE taxon_id = old.taxon_id AND NOT EXISTS (SELECT 1 FROM teszt_taxon WHERE taxon_id = old.taxon_id);
        RETURN old;
    END IF;
    RETURN new;
END; 
$$;
