CREATE OR REPLACE FUNCTION "update_teszt_taxonlist" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF tg_op = 'INSERT' THEN
        INSERT INTO teszt_taxon (word,lang,modifier_id)
            SELECT new.terepi_fajnev, 'terepi_fajnev', new.obm_modifier_id
            WHERE NOT EXISTS (
                    SELECT taxon_id FROM teszt_taxon WHERE new.terepi_fajnev = word
            ) AND new.terepi_fajnev IS NOT NULL; 
        RETURN new;
    ELSIF tg_op = 'UPDATE' THEN
        INSERT INTO teszt_taxon (word,lang,modifier_id)
            SELECT new.terepi_fajnev, 'terepi_fajnev', new.obm_modifier_id
            WHERE NOT EXISTS (
                    SELECT taxon_id FROM teszt_taxon WHERE new.terepi_fajnev = word
            ) AND new.terepi_fajnev IS NOT NULL; 
        RETURN new;
    END IF;
END; 
$$;
