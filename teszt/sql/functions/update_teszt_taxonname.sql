CREATE OR REPLACE FUNCTION "update_teszt_taxonname" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF (
      tg_op = 'UPDATE' AND (
        new.word != old.word 
        --OR new.lang != old.lang 
        --OR new.status != old.status
      ) AND 
      new.lang != 'terepi_fajnev' 
      -- AND new.status = 'accepted'
    )
    THEN
      EXECUTE FORMAT('
        UPDATE teszt 
        SET 
          %1$I = %2$L, 
          obm_modifier_id = %3$L 
        WHERE teszt.terepi_fajnev IN (
          SELECT word FROM teszt_taxon WHERE taxon_id = %4$L
        );', new.lang, new.word, new.modifier_id, new.taxon_id);
    END IF; 
    RETURN new; 
END $$;

DROP FUNCTION update_teszt_taxonname ;
