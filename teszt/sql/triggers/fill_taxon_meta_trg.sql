CREATE TRIGGER "teszt_search_fill_meta" BEFORE INSERT OR UPDATE ON "teszt_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "teszt_search_fill_meta" BEFORE INSERT OR UPDATE ON "teszt_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
