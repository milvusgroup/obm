CREATE TRIGGER "teszt_name_update" AFTER UPDATE ON "teszt_taxon" FOR EACH ROW EXECUTE PROCEDURE update_teszt_taxonname();
DROP TRIGGER "teszt_name_update" ON "teszt_taxon";