<?php
class ohm_mainpage_functions extends mainpage_functions {

    public function stat_species($use_taxon_db=FALSE,$stat_type=array('frequent'),$addlinks=true) {

        $table = obm_cache('get',"stat_species",'','',FALSE);
        if ($this->force_update_cache) $table = false;
        if ($table !== false ) 
            return $table;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        global $ID;
        $sum = "0";
        $count = "";
        $order = "cnt";
        $where = "";

        $st_col = st_col($this->table,'array');
        $qtable = "";
        if ($this->table!='PROJECTTABLE') {
            $t = preg_replace('/'.PROJECTTABLE.'_/','',$this->table);
            $qtable = "&qtable=$t";
        }
        $SPECIES_C = $st_col['SPECIES_C'];

        $count = "count($SPECIES_C) AS cnt";

        if ($SPECIES_C!='') {

            $table = "";

            if (in_array('frequent',$stat_type)) {
                $list = array();
                $cmd = "SELECT $SPECIES_C as f, $count FROM ".$this->table." $where GROUP BY $SPECIES_C ORDER BY $order DESC LIMIT 6";
                debug($cmd,__FILE__,__LINE__);
                $result = pg_query($ID,$cmd);
                while($row=pg_fetch_assoc($result)) {
                    if ($addlinks)
                        $link = '<a href="'.$protocol.'://'.URL.'/?query='.$SPECIES_C.':'.$row['f'].''.$qtable.'" target="_blank">'.$row['f'].'</a>';
                    else
                        $link = $row['f'];
                    $list[] = "<div class='tbl-cell button-small'>$link</div><div class='tbl-cell button-small' style='text-align:right'>".number_format($row['cnt'])."</div>";
                }
                $table .= "<div class='tbl pure-u-1'><div class='tbl-h' style='white-space:nowrap;text-decoration:underline'>".str_most_frequent_species.":</div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";
            }

            $time = $this->stat_species_cache * 60;
            obm_cache('set',"stat_species",$table,$time,FALSE);
            return $table;
        }
    }
}
    $mf = new ohm_mainpage_functions();
?>
<h2><?php echo t(str_project_statistics)?></h2>

<div class='leftbox boxborder'><h3><?php echo t(str_members)?></h3>
    <ul class='boxul'>
    <li class='blarge'><?php echo $mf->count_members(); ?></li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_uploads)?></h3>
    <ul class='boxul'>
        <li class='blarge'><?php $mf->count_uploads_cache = 5;echo $mf->count_uploads(); ?></li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_data)?></h3>
    <ul class='boxul'>
        <li class='blarge'><?php $mf->table = PROJECTTABLE;echo $mf->count_data(); ?></li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_species)?></h3>
    <ul class='boxul'>
        <li class='blarge'><a href='?specieslist'><?php $mf->count_species_cache = 5; echo $mf->count_species(true)?></a></li>
    </ul>
</div>

<div class='leftbox boxborder'><h3><?php echo t(str_species_stat)?></h3>
    <ul class='boxul'>
        <li><?php echo $mf->stat_species()?></li>
    </ul>
</div>
