<?php

/**
 * Class results_asPDF_CM
 * @author yourname
 */
class results_asPDF_CM {
    /**
     * undocumented function
     *
     * @return void
     */
    public static function prepare_report_fisa_de_teren_specii($de)
    {
        $st_col = $_SESSION['st_col'];
        $data = $de->as_array();
        
        try {
            $lists = self::convert_data($data);
            return compact('lists');
            
        } catch (DataPreparationException $e) {
            return [
                "error" => $e->getMessage()
            ];
        }
    }

    private static function convert_data($data) {

        $list_columns = [
            'data' => ['method' => 'reduce', 'fun' =>  'unique'],
            'observator' => ['method' => 'reduce', 'fun' =>  'unique'],
            'timp_incepere' => ['method' => 'reduce', 'fun' =>  'unique'],
            'email' => ['method' => 'get', 'fun' => 'email'],
            'qgrids.sci' => ['method' => 'reduce', 'fun' =>  'concat'],
            'qgrids.npa' => ['method' => 'reduce', 'fun' =>  'concat'],
            'qgrids.unitate_de_relief' => ['method' => 'reduce', 'fun' =>  'concat'],
            'obm_observation_list_id' => ['method' => 'reduce', 'fun' =>  'unique'],
            'tracklog' => ['method' => 'get', 'fun' => 'track_info'],
            'durata_lista' => ['method' => 'reduce', 'fun' => 'unique'],
            'qgrids.comuna' => ['method' => 'reduce', 'fun' =>  'concat'],
            'qgrids.etrs1_name' => ['method' => 'reduce', 'fun' =>  'concat'],
            'tip_metodologie' => ['method' => 'reduce', 'fun' => 'unique'],
            'habitat_terestru' => ['method' => 'reduce', 'fun' =>  'concat'],
            'habitat_terestru2' => ['method' => 'reduce', 'fun' =>  'concat'],
            'habitat_terestru3' => ['method' => 'reduce', 'fun' =>  'concat'],
            'habitat_acvatic' => ['method' => 'reduce', 'fun' =>  'concat'],
            'habitat_acvatic2' => ['method' => 'reduce', 'fun' =>  'concat'],
            'habitat_acvatic3' => ['method' => 'reduce', 'fun' =>  'concat'],
            'alt_tip_de_habitat' => ['method' => 'reduce', 'fun' =>  'concat'],
            'species_list' => ['method' => 'prepare', 'fun' => 'species_list'],
            'observatii' => ['method' => 'reduce', 'fun' =>  'concat'],
            'filename' => ['method' => 'reduce', 'fun' =>  'concat'],
            'altitudine' => ['method' => 'reduce', 'fun' => 'unique'],
            'temperatura_aer' => ['method' => 'reduce', 'fun' => 'unique'],
            'lungime' => ['method' => 'reduce', 'fun' => 'unique'],
            'temperatura_apa' => ['method' => 'reduce', 'fun' => 'unique'],
            'latime' => ['method' => 'reduce', 'fun' => 'unique'],
            'ph' => ['method' => 'reduce', 'fun' => 'unique'],
            'adancime' => ['method' => 'reduce', 'fun' => 'unique'],
            'ec' => ['method' => 'reduce', 'fun' => 'unique'],
            'tds' => ['method' => 'reduce', 'fun' => 'unique'],
        ];
        $prepared_lists = [];

        foreach (array_unique(array_column($data, 'obm_observation_list_id')) as $list_id) {
            $converted = [];

            foreach ($list_columns as $col => $opt) {

                $met = "{$opt['method']}_{$opt['fun']}";
                if (!method_exists(__CLASS__, $met)) {
                    throw new DataPreparationException("$met function is not implemented");
                }
                $data_filtered = array_values(array_filter($data, function ($row) use ($list_id) {
                    return ($row['obm_observation_list_id'] === $list_id);
                }));
                $res = self::$met($data_filtered, $col);
                
                $converted[str_replace('.', '_', $col)] = $res;
            }
            $prepared_lists[] = $converted;
        }

        return $prepared_lists; 
    }

    private static function reduce_unique ($data, $col) {
        $col_data = array_column($data, $col);
        if (count(array_unique($col_data)) > 1) {
            throw new DataPreparationException("$col column has multiple values!");
        }
        return array_values(array_unique($col_data))[0] ?? "";
    }

    private static function reduce_concat ($data, $col, $sep = ", ") {
        $col_data = array_column($data, $col);
        return implode($sep, array_values(array_filter(array_unique($col_data))));
    }

    private static function get_email ($data, $col) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT email FROM users WHERE username = %s AND project_table = '%s'", quote(self::reduce_unique($data, 'uploader_name')), PROJECTTABLE);
        if (!$res = pg_query($ID, $cmd)) {
            throw new DataPreparationException('query error');
        }
        $results = pg_fetch_assoc($res);

        return $results['email'];
    }

    private static function get_track_info ($data, $col) {
        global $ID;
        $observation_list_id = self::reduce_unique($data, 'obm_observation_list_id');
        $cmd = sprintf("
            SELECT 
                ST_AsText(ST_StartPoint(tracklog_line_geom)) as start_point, 
                ST_AsText(ST_EndPoint(tracklog_line_geom)) as end_point,
                ROUND(ST_Area(ST_Buffer(ST_Transform(tracklog_line_geom, 3844), 3))::numeric, 2) as surveyed_surface
            FROM %s_tracklogs WHERE observation_list_id = %s;", 
            PROJECTTABLE, 
            quote($observation_list_id)
        );
        if (!$res = pg_query($ID, $cmd)) {
            throw new DataPreparationException('query error');
        }
        $results = pg_fetch_assoc($res);

        return $results;
    }

    private static function prepare_species_list ($data, $col) {
        return array_map(function ($row) {
            extract($row);
            return compact( 'specia_sci', 'numar_exemplar', 'adult', 'mascul', 'femela',
                    'subadult', 'metamorf', 'juvenil', 'larve', 'ponta', 'roadkill',
            );
        }, $data);
    }
}

class DataPreparationException extends Exception {
}
?>
