DROP MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_valid;
CREATE MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_valid AS
SELECT DISTINCT ON (openherpmaps_taxon.taxon_id) openherpmaps_taxon.taxon_id,
    openherpmaps_taxon.word,
    openherpmaps_taxon.status
   FROM openherpmaps_taxon
  WHERE openherpmaps_taxon.lang = 'specia_sci' AND openherpmaps_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON openherpmaps.openherpmaps_taxon_valid (taxon_id);

GRANT SELECT ON openherpmaps.openherpmaps_taxon_valid TO public; 
ALTER MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_valid OWNER TO openherpmaps_admin;
