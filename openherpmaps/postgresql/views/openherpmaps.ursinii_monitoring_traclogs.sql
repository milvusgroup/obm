DROP MATERIALIZED VIEW openherpmaps.ursinii_monitoring_tracklogs;
CREATE MATERIALIZED VIEW openherpmaps.ursinii_monitoring_tracklogs AS
WITH props AS (
    SELECT 
        user_id, start_time, end_time, tracklog_id, observation_list_id, 
        json_array_elements((tracklog_geom->>'features')::json) AS props 
    FROM system.tracklogs 
    WHERE project = 'openherpmaps' AND trackname LIKE '%ursinii%'
),
vertices AS (
    SELECT 
        user_id, start_time, end_time,
        tracklog_id,
        observation_list_id, 
        props->>'type' AS type,
        ((props->>'properties')::json)->>'timestamp' AS timestamp,
        (((props->>'properties')::json)->>'accuracy')::numeric AS accuracy,
        ((props->>'geometry')::json)->>'type' AS geomtype,
        st_geomfromtext('POINT(' || (((((props->>'geometry')::json)->>'coordinates')::json)->>0)::varchar || ' ' || (((((props->>'geometry')::json)->>'coordinates')::json)->>1)::varchar || ')', 4326) AS geom
    FROM props
)
SELECT
    user_id, start_time, end_time,
    tracklog_id,
    observation_list_id,     
    row_number() over(partition by tracklog_id order by timestamp) AS v_id,
    timestamp,
    accuracy,
    geom,
    grouping_code,
    sampling_unit
FROM vertices v
LEFT JOIN openherpmaps_sampling_units su ON st_intersects(st_transform(st_buffer(geom_3844,10),4326), geom)
LEFT JOIN openherpmaps_grouping_codes gc ON su.grouping_code_id = gc.obm_id
WHERE type = 'Feature' AND geomtype = 'Point' AND accuracy < 10
ORDER BY tracklog_id, v_id;
GRANT ALL ON openherpmaps.ursinii_monitoring_tracklogs TO openherpmaps_admin;