CREATE OR REPLACE VIEW openherpmaps_roadkill_qgrids AS
SELECT * FROM openherpmaps_qgrids WHERE row_id IN (
    SELECT obm_id FROM openherpmaps_roadkill
    );
ALTER VIEW openherpmaps_roadkill_qgrids OWNER TO openherpmaps_admin;

