DROP MATERIALIZED VIEW openherpmaps.ursinii_monitoring_tracklog_summary;
CREATE MATERIALIZED VIEW openherpmaps.ursinii_monitoring_tracklog_summary AS
WITH cte AS (
    SELECT
        tracklog_id, extract(year from start_time) as year, start_time, end_time, user_id, geom, sampling_unit, grouping_code,
        v_id - LAG(v_id) OVER (PARTITION BY tracklog_id ORDER BY v_id) AS v_id_diff,
        LAG(geom) OVER (PARTITION BY tracklog_id ORDER BY v_id) AS prev_geometry,
        substring(timestamp, 1, 10)::integer::abstime::timestamp - LAG(substring(timestamp, 1, 10)::integer::abstime::timestamp) OVER (PARTITION BY tracklog_id ORDER BY v_id) AS time_diff
    FROM
        openherpmaps.ursinii_monitoring_tracklogs
    WHERE
        sampling_unit IS NOT NULL
),
cte2 as (
    SELECT
        tracklog_id, year, start_time, end_time, user_id, sampling_unit, grouping_code,
        SUM(CASE WHEN v_id_diff = 1 THEN time_diff ELSE '0 seconds'::interval END) AS total_time_in_square,
        round(SUM(
            CASE
                WHEN prev_geometry IS NOT NULL AND v_id_diff = 1
                THEN ST_Distance(geom::geography, prev_geometry::geography)
                ELSE 0
            END
        )::numeric, 2) AS total_length_in_square,
        count(*) as number_of_track_vertices
    FROM
        cte
    GROUP BY
        tracklog_id, year, start_time, end_time, user_id, sampling_unit, grouping_code
)
SELECT 
    *,
    CASE WHEN
        total_time_in_square < '15 minutes'::interval OR
        total_length_in_square < 800
        THEN TRUE
        ELSE FALSE
    END as erroneous
FROM cte2;
GRANT ALL ON openherpmaps.ursinii_monitoring_tracklog_summary TO openherpmaps_admin;