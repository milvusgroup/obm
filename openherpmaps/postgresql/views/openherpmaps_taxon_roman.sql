DROP MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_roman;
CREATE MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_roman AS
SELECT DISTINCT ON (openherpmaps_taxon.taxon_id) openherpmaps_taxon.taxon_id,
    openherpmaps_taxon.word,
    openherpmaps_taxon.status
   FROM openherpmaps_taxon
  WHERE openherpmaps_taxon.lang = 'specia_ro' AND openherpmaps_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON openherpmaps.openherpmaps_taxon_roman (taxon_id);

GRANT SELECT ON openherpmaps.openherpmaps_taxon_roman TO public; 
ALTER MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_roman OWNER TO openherpmaps_admin;
