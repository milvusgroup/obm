DROP TABLE openherpmaps_tracklogs; 
DROP SEQUENCE openherpmaps_tracklogs_id_seq;
DROP VIEW openherpmaps_tracklogs;
CREATE OR REPLACE VIEW openherpmaps_tracklogs AS
SELECT 
    tracklog_id, user_id, start_time, end_time, trackname, observation_list_id, tracklog_line_geom
FROM 
    system.tracklogs WHERE project = 'openherpmaps';
    
GRANT SELECT on openherpmaps_tracklogs TO openherpmaps_admin;
