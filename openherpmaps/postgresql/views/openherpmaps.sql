DROP VIEW public.openherpmaps CASCADE;
CREATE VIEW public.openherpmaps AS
    SELECT 
-- COLUMNS FROM THE MAIN TABLE
        d.obm_id,
        d.obm_geometry,
        d.obm_datum,
        d.obm_uploading_id,
        d.obm_validation,
        d.obm_comments,
        d.obm_modifier_id,
        d.obm_files_id,
        d.data,
        d.specia,
        d.numar_exemplar,
        d.unitate,
        d.site,
        d.punct_gps,
        d.observator,
        d.observatii,
        d.x,
        d.y,
        d.obm_access,
        d.nr_eveniment,
        d.ora,
        d.localitate,
        d.toponim,
        d.nr_cadrat,
        d.vremea,
        d.temperatura_aer,
        d.temperatura_apa,
        d.varietate,
        d.adult,
        d.mascul,
        d.femela,
        d.amplex,
        d.subadult,
        d.juvenil,
        d.metamorf,
        d.larve,
        d.ponta,
        d.tip_metodologie,
        d.habitat_terestru,
        d.foto_habitat_terestru,
        d.habitat_acvatic,
        d.lungime,
        d.latime,
        d.adancime,
        d.foto_habitat_acvatic,
        d.alte_fotografii,
        d.habitat_acvatic2,
        d.habitat_acvatic3,
        d.habitat_terestru2,
        d.habitat_terestru3,
        d.alt_tip_de_habitat,
        d.ph,
        d.ec,
        d.tds,
        d.localizare_reusita,
        d.cantec,
        d.email,
        d.atlas_access,
        d.roadkill,
        d.identificare_cu_dubii,
        d.data_incompleta,
        d.altitudine,
        d.literature,
        d.doi,
        vf2.word as specia_sci,
        vf3.word as specia_hu,
        vf5.word as specia_ro,
        vf4.word as specia_en,
        d.timp_incepere,
        d.timp_terminare,
        d.durata_lista,
        d.obm_observation_list_id,
        d.precision_of_count,
        d.carcass,
        d.dna_sample,
        d.distance,
        d.piele_naparlita,
        d.identificabil,
        d.odor,
        d.artropod,
        d.rozator,
        d.exact_time,
        d.surveyed_geometry,
        d.program,
        d.grouping_code,
        d.sampling_unit
-- COLUMNS FROM THE JOINED TABLE
-- MAIN TABLE REFERENCE
    FROM openherpmaps.openherpmaps d
    LEFT JOIN openherpmaps_taxon vf1 ON d.specia::text = vf1.word::text
    LEFT JOIN openherpmaps.openherpmaps_taxon_valid vf2 ON vf1.taxon_id = vf2.taxon_id
    LEFT JOIN openherpmaps.openherpmaps_taxon_magyar vf3 ON vf1.taxon_id = vf3.taxon_id
    LEFT JOIN openherpmaps.openherpmaps_taxon_angol vf4 ON vf1.taxon_id = vf4.taxon_id
    LEFT JOIN openherpmaps.openherpmaps_taxon_roman vf5 ON vf1.taxon_id = vf5.taxon_id;
-- JOIN STATEMENT
-- LEFT JOIN X vf ON (d.X = vf.X);

ALTER VIEW public.openherpmaps OWNER TO openherpmaps_admin;

--  openherpmaps_roadkill depends on view openherpmaps
--  openherpmaps_roadkill_qgrids depends on view openherpmaps_roadkill
--  openherpmaps.capreolus_user_species_list_specia_sci depends on view openherpmaps
--  openherpmaps.capreolus_project_species_list_specia_sci depends on view openherpmaps
--  openherpmaps.capreolus_user_species_list_specia_ro depends on view openherpmaps
--  openherpmaps.capreolus_project_species_list_specia_ro depends on view openherpmaps
--  openherpmaps.capreolus_user_species_list_specia_hu depends on view openherpmaps
--  openherpmaps.capreolus_project_species_list_specia_hu depends on view openherpmaps
--  openherpmaps.capreolus_user_species_list_specia_en depends on view openherpmaps
--  openherpmaps.capreolus_project_species_list_specia_en depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary0_specia_sci depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary0_specia_ro depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary0_specia_hu depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary0_specia_en depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary_specia_sci depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary_specia_ro depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary_specia_hu depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary_specia_en depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary2_specia_sci depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary2_specia_ro depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary2_specia_hu depends on view openherpmaps
--  openherpmaps.capreolus_observation_summary2_specia_en depends on view openherpmaps