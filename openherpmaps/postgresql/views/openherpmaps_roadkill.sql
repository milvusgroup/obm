-- DROP VIEW openherpmaps_roadkill CASCADE;
CREATE OR REPLACE VIEW openherpmaps_roadkill AS
SELECT * FROM openherpmaps WHERE roadkill IS NOT NULL AND roadkill > 0;
ALTER VIEW openherpmaps_roadkill OWNER TO  openherpmaps_admin;

