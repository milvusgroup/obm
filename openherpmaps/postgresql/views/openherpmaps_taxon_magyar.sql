DROP MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_magyar;
CREATE MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_magyar AS
SELECT DISTINCT ON (openherpmaps_taxon.taxon_id) openherpmaps_taxon.taxon_id,
    openherpmaps_taxon.word,
    openherpmaps_taxon.status
   FROM openherpmaps_taxon
  WHERE openherpmaps_taxon.lang = 'specia_hu' AND openherpmaps_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON openherpmaps.openherpmaps_taxon_magyar (taxon_id);

GRANT SELECT ON openherpmaps.openherpmaps_taxon_magyar TO public; 
ALTER MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_magyar OWNER TO openherpmaps_admin;
