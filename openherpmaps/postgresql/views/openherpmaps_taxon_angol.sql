DROP MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_angol;
CREATE MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_angol AS
SELECT DISTINCT ON (openherpmaps_taxon.taxon_id) openherpmaps_taxon.taxon_id,
    openherpmaps_taxon.word,
    openherpmaps_taxon.status
   FROM openherpmaps_taxon
  WHERE openherpmaps_taxon.lang = 'specia_en' AND openherpmaps_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON openherpmaps.openherpmaps_taxon_angol (taxon_id);

GRANT SELECT ON openherpmaps.openherpmaps_taxon_angol TO public; 
ALTER MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_angol OWNER TO openherpmaps_admin;
