CREATE OR REPLACE FUNCTION "update_openherpmaps_qgrids" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
 tbl varchar;
 geom geometry;
 centroid geometry;
 etrs1 RECORD;
 etrs10 RECORD;
BEGIN
        tbl := TG_TABLE_NAME;
        IF tg_op = 'INSERT' THEN

            geom := new."obm_geometry";
            centroid := ST_Centroid(geom);

            SELECT geometry,name INTO etrs10 FROM shared.etrs10 g WHERE ST_Intersects(centroid, g.geometry);
            EXECUTE format('SELECT geometry, cellcode as name FROM shared.etrs1 g WHERE etrs10_code = %L AND ST_Intersects(%L, g.geometry);', etrs10.name, centroid) INTO etrs1;

            execute format('INSERT INTO %s_qgrids ("data_table","row_id","original","etrs10_geom",etrs10_name,"etrs1_geom",etrs1_name,centroid) VALUES (%L,%L,%L,%L,%L,%L,%L,%L);',tbl,tbl,new.obm_id,new.obm_geometry,etrs10.geometry, etrs10.name, etrs1.geometry, etrs1.name, centroid);

            RETURN new; 

        ELSIF tg_op = 'UPDATE' AND ST_Equals(OLD."obm_geometry",NEW."obm_geometry") = FALSE THEN

            geom := new."obm_geometry";
            centroid := ST_Centroid(geom);
            
            SELECT geometry,name INTO etrs10 FROM shared.etrs10 g WHERE ST_Intersects(centroid, g.geometry);
            EXECUTE format('SELECT geometry, cellcode as name FROM shared.etrs1 g WHERE etrs10_code = %L AND ST_Intersects(%L, g.geometry);', etrs10.name, centroid) INTO etrs1;

            execute format('UPDATE %s_qgrids set "original" = %L, "etrs10_geom" = %L, etrs10_name = %L, "etrs1_geom" = %L, etrs1_name = %L, centroid = %L, judet = NULL, comuna = NULL, sci = NULL, spa = NULL, altit = NULL, corine_code = NULL, country = NULL, npa = NULL, unitate_de_relief = NULL WHERE data_table = %L AND row_id = %L;',tbl,new.obm_geometry,etrs10.geometry,etrs10.name,etrs1.geometry,etrs1.name,centroid,tbl,new.obm_id);
            RETURN new; 

        ELSIF tg_op = 'DELETE' THEN
            EXECUTE format('DELETE FROM %s_qgrids WHERE data_table = %L AND row_id = %L;',tbl,tbl,OLD.obm_id);
            RETURN old;
        ELSE
            RETURN new;
        END IF;
END; 
$$;
