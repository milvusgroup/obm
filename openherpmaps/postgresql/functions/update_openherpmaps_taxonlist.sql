CREATE OR REPLACE FUNCTION "update_openherpmaps_taxonlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
observer RECORD;
tbl varchar(64) = TG_TABLE_NAME;
sid integer;
sids integer[] DEFAULT '{}';
term varchar(128);
BEGIN
    IF tg_op = 'INSERT' THEN
        term := NEW.specia;
        PERFORM add_term(term, tbl, 'specia','openherpmaps','taxon'::varchar);
        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  OLD.specia != NEW.specia THEN
            PERFORM add_term(NEW.specia, tbl, 'specia','openherpmaps','taxon'::varchar);
        END IF;

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        -- azt hiszem ez már nem szükséges, mert a kézi karbantartás miatt nem kell törölni semmit
        -- PERFORM remove_term(OLD.specia, tbl, 'specia','openherpmaps','taxon');

        RETURN OLD;

    END IF;                                                                                         
END $$;

--INSERT INTO openherpmaps_search_connect (data_table, row_id, observer_ids) SELECT 'openherpmaps', foo.obm_id, array_agg(search_id) FROM (SELECT obm_id, unnest(regexp_split_to_array(concat_ws(',',observator),E'[,;]\\s*')) as names FROM openherpmaps WHERE obm_id > 221) as foo LEFT JOIN openherpmaps_search o ON names = word GROUP by foo.obm_id;

--UPDATE openherpmaps_search_connect sc SET species_ids = ARRAY[search_id] FROM (SELECT search_id, b.obm_id FROM openherpmaps b LEFT JOIN openherpmaps_search tx ON b.specia = tx.word AND b.specia IS NOT NULL WHERE b.obm_id > 221) as t WHERE sc.row_id = t.obm_id;
