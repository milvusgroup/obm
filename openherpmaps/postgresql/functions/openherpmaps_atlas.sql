Begin;

WITH foo as (
    SELECT g.geometry, g.cellcode, o.species, count(*) as nr_observations 
    FROM openherpmaps o LEFT JOIN shared.etrs10 g ON ST_Contains(g.geometry, ST_Centroid(o.obm_geometry))
    WHERE o.atlas_access = 1)
INSERT INTO openherpmaps_atlas (obm_geometry ,cellcode, species, nr_observations) (SELECT * FROM foo)
ON CONFLICT ON CONSTRAINT openherpmaps_atlas_cellcode_species
DO UPDATE SET nr_observations = EXCLUDED.nr_observations;

    ROLLBACK;
