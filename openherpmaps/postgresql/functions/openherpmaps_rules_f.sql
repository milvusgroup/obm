CREATE OR REPLACE FUNCTION "openherpmaps_rules_f" () RETURNS trigger LANGUAGE plpgsql AS $$

DECLARE

 sens integer DEFAULT 0;
 ext integer DEFAULT 0;
 proj varchar DEFAULT 'openherpmaps';

BEGIN

    IF tg_op = 'INSERT' THEN

        sens := new.obm_access;

        execute format('INSERT INTO %1$s_rules ("data_table","row_id","write","read","sensitivity") SELECT %1$L,%2$L,ARRAY[uploader_id],%3$L,%4$L FROM system.uploadings WHERE id = %5$L',proj,new.obm_id,(SELECT uploader_id || "group" FROM system.uploadings WHERE id = new.obm_uploading_id),sens,new.obm_uploading_id);

        RETURN new;

    ELSIF tg_op = 'UPDATE' AND OLD.obm_access != NEW.obm_access THEN

        sens := new.obm_access;

        execute format('UPDATE %1$s_rules SET sensitivity = %2$L WHERE row_id = %3$L AND data_table = %1$L',proj,sens,NEW.obm_id);

        RETURN new;

    ELSIF tg_op = 'DELETE' THEN

        execute format('DELETE FROM "%1$s_rules" WHERE "data_table" = %1$L AND "row_id" = %2$L ',proj,OLD.obm_id);
        RETURN old;

    ELSE

        RETURN new;

    END IF;

END;
$$;
