CREATE OR REPLACE FUNCTION "openherpmaps_refresh_taxon_materialized_views" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_valid;
    REFRESH MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_magyar;
    REFRESH MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_roman;
    REFRESH MATERIALIZED VIEW openherpmaps.openherpmaps_taxon_angol;
    RETURN NULL;
END
$$;
