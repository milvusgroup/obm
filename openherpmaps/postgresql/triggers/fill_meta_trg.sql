CREATE TRIGGER "openherpmaps_search_fill_meta" BEFORE INSERT OR UPDATE ON "openherpmaps_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "openherpmaps_taxon_fill_meta" BEFORE INSERT OR UPDATE ON "openherpmaps_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
