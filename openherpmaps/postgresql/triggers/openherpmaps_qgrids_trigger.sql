CREATE TRIGGER "openherpmaps_qgrids_trigger" BEFORE DELETE OR INSERT OR UPDATE ON openherpmaps.openherpmaps FOR EACH ROW EXECUTE PROCEDURE public.update_openherpmaps_qgrids();
CREATE TRIGGER "openherpmaps_threats_qgrids_trigger" BEFORE DELETE OR INSERT OR UPDATE ON public.openherpmaps_threats FOR EACH ROW EXECUTE PROCEDURE public.update_openherpmaps_qgrids();

