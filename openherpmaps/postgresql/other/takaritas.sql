select count(*) --FROM (
    --select distinct obm_geometry, specia, observator, count(*) c 
    FROM openherpmaps 
    WHERE obm_id NOT IN (
        select minid FROM (
            select distinct min(obm_id) as minid, obm_geometry, specia, observator, data, count(*) as c 
            from openherpmaps 
            group by obm_geometry, specia, observator, data) as foo 
        WHERE foo.c > 1
        UNION
        SELECT obm_id as minid FROM (
            SELECT distinct min(obm_id) as minid, obm_geometry, specia, observator, data, count(*) as c 
            from openherpmaps 
            group by obm_geometry, specia, observator, data) as foo 
        WHERE foo.c = 1
    )
--    group by obm_geometry, specia, observator) as baz 
--WHERE baz.c > 1
;
