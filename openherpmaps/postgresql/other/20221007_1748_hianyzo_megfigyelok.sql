SELECT uploader_name FROM system.uploadings WHERE id IN (
    SELECT obm_uploading_id FROM openherpmaps WHERE observator IS NULL
);

UPDATE openherpmaps SET observator = uploader_name FROM system.uploadings WHERE obm_uploading_id = id AND observator IS NULL;
