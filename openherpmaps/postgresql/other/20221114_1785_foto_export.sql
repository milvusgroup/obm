\copy (
    WITH d as (
SELECT 
    obm_id,
    file_id, 
    reference
FROM openherpmaps ohm
LEFT JOIN system.uploadings u ON u.id = ohm.obm_uploading_id
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    data > '2022-01-01' AND
    obm_files_id IS NOT NULL AND
    u.uploader_name = 'Sos Tibor'
)
SELECT '/home/gabor/obm/obm-composer/local/openherpmaps/local/attached_files/' || reference FROM d
) TO '/tmp/ohm_st_2022.sh' CSV;

