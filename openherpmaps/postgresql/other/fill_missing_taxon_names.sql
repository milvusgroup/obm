--BEGIN; 
--INSERT INTO openherpmaps_taxon (taxon_id, word, lang, status)
(SELECT taxon_id, word, 'specia_hu' as lang, 'accepted' as status FROM openherpmaps_taxon where status = 'accepted' AND lang = 'specia_sci' AND taxon_id IN ( select taxon_id from openherpmaps_taxon GROUP BY taxon_id HAVING count(*) = 1 )
UNION
SELECT taxon_id, word, 'specia_ro' as lang, 'accepted' as status FROM openherpmaps_taxon where status = 'accepted' AND lang = 'specia_sci' AND taxon_id IN ( select taxon_id from openherpmaps_taxon GROUP BY taxon_id HAVING count(*) = 1 )
UNION
SELECT taxon_id, word, 'specia_en' as lang, 'accepted' as status FROM openherpmaps_taxon where status = 'accepted' AND lang = 'specia_sci' AND taxon_id IN ( select taxon_id from openherpmaps_taxon GROUP BY taxon_id HAVING count(*) = 1 ))
ORDER BY taxon_id;
--COMMIT;