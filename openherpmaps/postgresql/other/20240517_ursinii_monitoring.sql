WITH cte AS (
    SELECT
        tracklog_id,
        v_id - LAG(v_id) OVER (PARTITION BY tracklog_id ORDER BY v_id) AS prev_v_id,
        substring(timestamp, 1, 10)::integer::abstime::timestamp,
        LAG(substring(timestamp, 1, 10)::integer::abstime::timestamp) OVER (PARTITION BY tracklog_id ORDER BY v_id),
        substring(timestamp, 1, 10)::integer::abstime::timestamp - LAG(substring(timestamp, 1, 10)::integer::abstime::timestamp) OVER (PARTITION BY tracklog_id ORDER BY v_id) AS time_diff
    FROM
        openherpmaps.ursinii_monitoring_tracklogs
    WHERE
        sampling_unit = 'T2' AND 
        extract(year from start_time) = 2024 AND
        tracklog_id = '636ad286-5eff-4e66-8a33-100a891d8d32'
)
SELECT * FROM cte;
--SELECT
--    tracklog_id, start_time, end_time, user_id,
--    SUM(time_diff) AS total_time_in_square,
--    count(*) as number_of_track_vertices
--FROM
--    cte
--GROUP BY
--    tracklog_id, start_time, end_time, user_id;