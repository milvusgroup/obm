UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 3 FROM openherpmaps_taxon tx WHERE tx.word = 'Salamandra salamandra' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 6 FROM openherpmaps_taxon tx WHERE tx.word = 'Triturus sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 9 FROM openherpmaps_taxon tx WHERE tx.word = 'Triturus cristatus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 12 FROM openherpmaps_taxon tx WHERE tx.word = 'Triturus dobrogicus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 15 FROM openherpmaps_taxon tx WHERE tx.word = 'Ichthyosaura alpestris' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 18 FROM openherpmaps_taxon tx WHERE tx.word = 'Lissotriton sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 21 FROM openherpmaps_taxon tx WHERE tx.word = 'Lissotriton vulgaris' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 24 FROM openherpmaps_taxon tx WHERE tx.word = 'Lissotriton vulgaris vulgaris' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 27 FROM openherpmaps_taxon tx WHERE tx.word = 'Lissotriton vulgaris ampelensis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 30 FROM openherpmaps_taxon tx WHERE tx.word = 'Lissotriton montandoni' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 33 FROM openherpmaps_taxon tx WHERE tx.word = 'Bombina sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 36 FROM openherpmaps_taxon tx WHERE tx.word = 'Bombina bombina' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 39 FROM openherpmaps_taxon tx WHERE tx.word = 'Bombina variegata' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 42 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelobates sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 45 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelobates fuscus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 48 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelobates balcanicus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 51 FROM openherpmaps_taxon tx WHERE tx.word = 'Bufo/Bufotes sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 54 FROM openherpmaps_taxon tx WHERE tx.word = 'Bufo bufo' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 57 FROM openherpmaps_taxon tx WHERE tx.word = 'Bufotes viridis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 60 FROM openherpmaps_taxon tx WHERE tx.word = 'Hyla sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 63 FROM openherpmaps_taxon tx WHERE tx.word = 'Hyla cf. Arborea' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 66 FROM openherpmaps_taxon tx WHERE tx.word = 'Hyla cf. Orientalis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 69 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 72 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana temporaria' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 75 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana arvalis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 78 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana arvalis arvalis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 81 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana arvalis wolterstorffi' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 84 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana dalmatina' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 87 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelophylax sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 90 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelophylax ridibundus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 93 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelophylax lessonae' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 96 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelophylax kl. esculentus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 99 FROM openherpmaps_taxon tx WHERE tx.word = 'Testudo sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 102 FROM openherpmaps_taxon tx WHERE tx.word = 'Testudo hermanni boettgeri' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 105 FROM openherpmaps_taxon tx WHERE tx.word = 'Testudo graeca ibera' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 108 FROM openherpmaps_taxon tx WHERE tx.word = 'Emys orbicularis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 111 FROM openherpmaps_taxon tx WHERE tx.word = 'Caretta caretta' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 114 FROM openherpmaps_taxon tx WHERE tx.word = 'Pseudemys concinna' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 117 FROM openherpmaps_taxon tx WHERE tx.word = 'Trachemys ssp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 120 FROM openherpmaps_taxon tx WHERE tx.word = 'Trachemys scripta' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 123 FROM openherpmaps_taxon tx WHERE tx.word = 'Trachemys scripta scripta' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 126 FROM openherpmaps_taxon tx WHERE tx.word = 'Trachemys scripta elegans' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 129 FROM openherpmaps_taxon tx WHERE tx.word = 'Graptemys pseudogeographica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 132 FROM openherpmaps_taxon tx WHERE tx.word = 'Pelodiscus sinensis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 135 FROM openherpmaps_taxon tx WHERE tx.word = 'Eremias arguta deserti' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 138 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 141 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta viridis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 144 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta diplochondrodes dobrogica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 147 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta agilis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 150 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta agilis agilis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 153 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta agilis chersonensis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 156 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta agilis euxinica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 159 FROM openherpmaps_taxon tx WHERE tx.word = 'Zootoca vivipara' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 162 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 165 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis muralis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 168 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis muralis muralis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 171 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis muralis maculiventris' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 174 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis tauricus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 177 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis siculus campestris' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 180 FROM openherpmaps_taxon tx WHERE tx.word = 'Darevskia praticola hungarica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 183 FROM openherpmaps_taxon tx WHERE tx.word = 'Ablepharus kitaibelii stepaneki' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 186 FROM openherpmaps_taxon tx WHERE tx.word = 'Anguis colchica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 189 FROM openherpmaps_taxon tx WHERE tx.word = 'Mediodactylus danilewskii' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 192 FROM openherpmaps_taxon tx WHERE tx.word = 'Eryx jaculus turcicus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 195 FROM openherpmaps_taxon tx WHERE tx.word = 'Dolichophis caspius' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 198 FROM openherpmaps_taxon tx WHERE tx.word = 'Elaphe sauromates' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 201 FROM openherpmaps_taxon tx WHERE tx.word = 'Zamenis longissimus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 204 FROM openherpmaps_taxon tx WHERE tx.word = 'Natrix sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 207 FROM openherpmaps_taxon tx WHERE tx.word = 'Natrix natrix' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 210 FROM openherpmaps_taxon tx WHERE tx.word = 'Natrix tessellata' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 213 FROM openherpmaps_taxon tx WHERE tx.word = 'Coronella austriaca' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 216 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera sp.' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 219 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ursinii' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 222 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ursinii rakosiensis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 225 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ursinii moldovica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 228 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera berus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 231 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera berus berus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 234 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera (berus) nikolskii' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 237 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera berus cf. nikolskii' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 240 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ammodytes' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 243 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ammodytes ammodytes' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET taxonomical_order = 246 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ammodytes montandoni' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';


UPDATE openherpmaps_taxonmeta SET taxonomical_order = 2147483647 WHERE taxonomical_order IS NULL;

SELECT word, taxon_id FROM openherpmaps_taxon tx WHERE tx.word IN ('Lissotriton vulgaris', 'Rana arvalis', 'Trachemys scripta', 'Lacerta agilis', 'Podarcis muralis', 'Vipera ursinii', 'Vipera berus', 'Vipera ammodytes'); 

UPDATE openherpmaps_taxonmeta tm SET parent_id = 287 FROM openherpmaps_taxon tx WHERE tx.word = 'Lissotriton vulgaris vulgaris' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 287 FROM openherpmaps_taxon tx WHERE tx.word = 'Lissotriton vulgaris ampelensis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 305 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana arvalis arvalis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 305 FROM openherpmaps_taxon tx WHERE tx.word = 'Rana arvalis wolterstorffi' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 320 FROM openherpmaps_taxon tx WHERE tx.word = 'Trachemys scripta scripta' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 320 FROM openherpmaps_taxon tx WHERE tx.word = 'Trachemys scripta elegans' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 328 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta agilis agilis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 328 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta agilis chersonensis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 328 FROM openherpmaps_taxon tx WHERE tx.word = 'Lacerta agilis euxinica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 334 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis muralis muralis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 334 FROM openherpmaps_taxon tx WHERE tx.word = 'Podarcis muralis maculiventris' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 352 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ursinii rakosiensis' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 352 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ursinii moldavica' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 355 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera berus berus' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 355 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera (berus) nikolskii' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 359 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ammodytes ammodytes' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';
UPDATE openherpmaps_taxonmeta tm SET parent_id = 359 FROM openherpmaps_taxon tx WHERE tx.word = 'Vipera ammodytes montandoni' AND tm.taxon_id = tx.taxon_id AND tx.status = 'accepted' AND tx.lang = 'specia_sci';


-- az alfajoknak magyar, román és angol nevet kell adjak
WITH asdf as (
    SELECT wid, taxon_id, word as wl FROM openherpmaps_taxon
),
dt as (
    SELECT 
        asdf.wid as parent_wid,
        asdf.taxon_id as parent_id, 
        openherpmaps_taxon.taxon_id, 
        openherpmaps_taxon.wid as subs_wid,
        wl parent_name, 
        word subspecies_name
    FROM openherpmaps_taxon, asdf 
    WHERE 
        word LIKE asdf.wl || ' %' AND 
        word NOT LIKE '%/%' AND 
        word NOT LIKE '%cf.%' AND 
        word NOT LIKE '%ssp.' AND 
        asdf.taxon_id != openherpmaps_taxon.taxon_id AND 
        word NOT LIKE '% x %' AND 
        word NOT LIKE 'Testudo%' AND 
        lang = 'specia_'
),
alfajnevek as (
    SELECT dt.parent_wid, tx.word, dt.subs_wid, tx.word || ' (' || replace(dt.subspecies_name, dt.parent_name || ' ', '') || ')' as alfajnev, subspecies_name
    FROM dt
    LEFT JOIN openherpmaps_taxon tx  ON tx.taxon_id = dt.parent_id AND tx.lang = 'specia_sci'
    LEFT JOIN openherpmaps_taxon tx2 ON tx2.taxon_id = dt.taxon_id AND tx2.lang = 'specia_sci'
    )
UPDATE openherpmaps_taxon SET word = alfajnevek.subspecies_name FROM alfajnevek WHERE wid = subs_wid;
