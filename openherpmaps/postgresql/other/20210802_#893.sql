-- #839

select DISTINCT observator from openherpmaps o 
left join system.uploadings u on u.id = o.obm_uploading_id 
where observator like 'Hegyeli Zsolt' and uploader_id = 482;

UPDATE openherpmaps SET 
    obm_uploading_id = 369952 
WHERE obm_id IN (
    select obm_id from openherpmaps o 
    left join system.uploadings u on u.id = o.obm_uploading_id 
    where observator like 'Hegyeli Zsolt' and uploader_id = 482
);



UPDATE openherpmaps SET 
    obm_uploading_id = 369953
WHERE obm_id IN (
    select obm_id from openherpmaps o 
    left join system.uploadings u on u.id = o.obm_uploading_id 
    where observator like 'Kovács István%' and uploader_id = 482
);


UPDATE openherpmaps SET 
    obm_uploading_id = 369954
WHERE obm_id IN (
    select obm_id from openherpmaps o 
    left join system.uploadings u on u.id = o.obm_uploading_id 
    where observator like 'Boné%' and uploader_id = 482
);

UPDATE openherpmaps SET 
    obm_uploading_id = 369955
WHERE obm_id IN (
    select obm_id from openherpmaps o 
    left join system.uploadings u on u.id = o.obm_uploading_id 
    where observator like '%Moldov%' and uploader_id = 482
);

