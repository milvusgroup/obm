WITH target_lang as (    
    SELECT * FROM openherpmaps_taxon WHERE lang = 'specia_hu' AND status = 'accepted'
),
fallback_lang as (
    SELECT * FROM openherpmaps_taxon WHERE lang = 'specia_sci' AND status = 'accepted'
)
SELECT src.word, src.lang, trg.word, trg.lang, fb.word, fb.lang 
FROM openherpmaps_taxon src                      
LEFT JOIN 
     target_lang trg ON src.taxon_id = trg.taxon_id
LEFT JOIN
    fallback_lang fb ON src.taxon_id = fb.taxon_id
ORDER BY src.word;