BEGIN;

--CREATE TABLE openherpmaps_qgrids ( 
--    row_id integer NOT NULL, 
--    data_table varchar NOT NULL,
--    original geometry,
--    etrs10_geom geometry,
--    etrs10_name varchar (64),
--    PRIMARY KEY (row_id, data_table)
--);

--ALTER TABLE openherpmaps_qgrids ADD COLUMN etrs1_geom geometry;
--ALTER TABLE openherpmaps_qgrids ADD COLUMN etrs1_name varchar(13);
ALTER TABLE openherpmaps_qgrids ADD COLUMN utm10_geom geometry;
ALTER TABLE openherpmaps_qgrids ADD COLUMN utm10_name varchar(13);
ALTER TABLE openherpmaps_qgrids ADD COLUMN utm50_geom geometry;
ALTER TABLE openherpmaps_qgrids ADD COLUMN utm50_name varchar(13);
ALTER TABLE openherpmaps_qgrids ADD COLUMN centroid geometry;
ALTER TABLE openherpmaps_qgrids ADD COLUMN country varchar(64);

--INSERT INTO openherpmaps_qgrids (row_id, data_table, original) SELECT obm_id, 'openherpmaps', obm_geometry FROM openherpmaps;

--UPDATE openherpmaps_qgrids SET etrs10_geom = st_transform(foo.geometry,4326), etrs10_name = foo.name
--FROM (
--       SELECT d.obm_id,k.geometry,k.name FROM openherpmaps d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,st_transform(k.geometry,4326)))
--) as foo
--WHERE row_id=foo.obm_id AND data_table = 'openherpmaps';

--UPDATE openherpmaps_qgrids SET etrs1_geom = foo.geometry, etrs1_name = foo.name
--FROM (
--       SELECT d.obm_id,k.geometry,k.cellcode as name FROM openherpmaps d LEFT JOIN shared."etrs1" k ON (st_within(d.obm_geometry,k.geometry))
--) as foo
--WHERE row_id=foo.obm_id AND data_table = 'openherpmaps';

--CREATE TRIGGER "openherpmaps_qgrids_trigger" BEFORE INSERT OR UPDATE OR DELETE ON "public"."openherpmaps" FOR EACH ROW EXECUTE PROCEDURE update_openherpmaps_qgrids();

COMMIT;

ALTER TABLE openherpmaps_qgrids ADD COLUMN judet varchar;
ALTER TABLE openherpmaps_qgrids ADD COLUMN comuna varchar;
ALTER TABLE openherpmaps_qgrids ADD COLUMN sci varchar;
ALTER TABLE openherpmaps_qgrids ADD COLUMN spa varchar;
ALTER TABLE openherpmaps_qgrids ADD COLUMN altit numeric;
ALTER TABLE openherpmaps_qgrids ADD COLUMN corine_code varchar(3);

ALTER TABLE openherpmaps_qgrids ADD COLUMN npa varchar;
ALTER TABLE openherpmaps_qgrids ADD COLUMN unitate_de_relief varchar;

CREATE INDEX openherpmaps_qgrids_centroid_idx ON openherpmaps_qgrids USING GIST (centroid);

WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.name FROM openherpmaps_qgrids rqg LEFT JOIN shared.romania_administrative_units g ON ST_Intersects(rqg.centroid,geometry_4326) WHERE rqg.judet IS NULL AND g.natlevel = '2ndOrder' ORDER BY row_id DESC ) UPDATE openherpmaps_qgrids qg SET judet = rows.name FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;
WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.name FROM openherpmaps_qgrids rqg LEFT JOIN shared.romania_administrative_units g ON ST_Intersects(rqg.centroid,geometry_4326) WHERE rqg.comuna IS NULL AND g.natlevel = '3rdOrder' ORDER BY row_id DESC ) UPDATE openherpmaps_qgrids qg SET comuna = rows.name FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;
WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.nume FROM openherpmaps_qgrids rqg LEFT JOIN shared.arii_protejate_20170829 g ON ST_Intersects(rqg.centroid,geometry) WHERE rqg.spa IS NULL AND g.tip_ap = 'Arie de protecție specială avifaunistică' ORDER BY row_id DESC ) UPDATE openherpmaps_qgrids qg SET spa = rows.nume FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;
WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.nume FROM openherpmaps_qgrids rqg LEFT JOIN shared.arii_protejate_20170829 g ON ST_Intersects(rqg.centroid,geometry) WHERE rqg.sci IS NULL AND g.tip_ap = 'Sit de importanță comunitară' ORDER BY row_id DESC ) UPDATE openherpmaps_qgrids qg SET sci = rows.nume FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;


INSERT INTO openherpmaps_qgrids (row_id, data_table, original) SELECT obm_id, 'openherpmaps_threats', obm_geometry FROM public.openherpmaps_threats;

UPDATE openherpmaps_qgrids SET original = foo.obm_geometry, centroid = st_centroid(foo.obm_geometry), etrs10_geom = st_transform(foo.geometry,4326), etrs10_name = foo.name, judet = NULL, comuna = NULL, sci = NULL, spa = NULL
FROM (
       SELECT d.obm_id, d.obm_geometry, k.geometry,k.name FROM openherpmaps_threats d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,st_transform(k.geometry,4326)))
) as foo
WHERE row_id=foo.obm_id AND data_table = 'openherpmaps_threats';
