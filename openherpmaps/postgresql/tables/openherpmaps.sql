ALTER TABLE openherpmaps ADD COLUMN specia_sci character varying (64) NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN specia_sci character varying (64) NULL;
ALTER TABLE openherpmaps ADD COLUMN specia_hu character varying (64) NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN specia_hu character varying (64) NULL;
ALTER TABLE openherpmaps ADD COLUMN specia_ro character varying (64) NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN specia_ro character varying (64) NULL;
ALTER TABLE openherpmaps ADD COLUMN specia_en character varying (64) NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN specia_en character varying (64) NULL;

ALTER TABLE openherpmaps ADD COLUMN timp_incepere timestamp NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN timp_incepere timestamp NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN timp_incepere timestamp NULL;

ALTER TABLE openherpmaps ADD COLUMN timp_terminare timestamp NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN timp_terminare timestamp NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN timp_terminare timestamp NULL;

ALTER TABLE openherpmaps ADD COLUMN durata_lista timestamp NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN durata_lista timestamp NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN durata_lista timestamp NULL;
ALTER TABLE openherpmaps ALTER COLUMN durata_lista TYPE time;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ALTER COLUMN durata_lista TYPE time;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ALTER COLUMN durata_lista TYPE time;

ALTER TABLE openherpmaps ALTER COLUMN doi TYPE character varying;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN doi character varying NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN doi character varying NULL;

ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN literature text NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN literature text NULL;

ALTER TABLE openherpmaps ADD COLUMN obm_observation_list_id character varying (32) NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN obm_observation_list_id character varying (32) NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN obm_observation_list_id character varying (32) NULL;

ALTER TABLE openherpmaps ALTER COLUMN obm_observation_list_id TYPE character varying (64);
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ALTER COLUMN obm_observation_list_id TYPE character varying (64);
ALTER TABLE temporary_tables.openherpmaps_obm_upl ALTER COLUMN obm_observation_list_id TYPE character varying (64);

ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN precision_of_count character varying (32);
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN precision_of_count character varying (32);

ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN carcass character varying (32);
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN carcass character varying (32);

ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN dna_sample character varying (32);
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN dna_sample character varying (32);

ALTER TABLE openherpmaps ALTER COLUMN odor TYPE integer;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN odor character varying NULL;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN odor character varying NULL;

ALTER TABLE openherpmaps.openherpmaps ADD COLUMN project varchar;
ALTER TABLE temporary_tables.openherpmaps_obm_obsl ADD COLUMN project varchar;
ALTER TABLE temporary_tables.openherpmaps_obm_upl ADD COLUMN project varchar;