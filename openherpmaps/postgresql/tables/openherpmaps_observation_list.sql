CREATE SEQUENCE public.openherpmaps_observation_list_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

CREATE TABLE public.openherpmaps_observation_list (
    id integer DEFAULT nextval('public.openherpmaps_observation_list_id_seq'::regclass) NOT NULL,
    oidl character varying(40),
    uploading_id integer,
    obsstart timestamp without time zone,
    obsend timestamp without time zone,
    nulllist boolean,
    started_at timestamp without time zone,
    finished_at timestamp without time zone,
    obm_observation_list_id integer
);

ALTER TABLE ONLY public.openherpmaps_observation_list
    ADD CONSTRAINT openherpmaps_observation_pkey PRIMARY KEY (id);
