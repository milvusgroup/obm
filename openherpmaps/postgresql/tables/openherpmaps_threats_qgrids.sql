BEGIN;
       CREATE TABLE "public"."openherpmaps_threats_qgrids" (
       "row_id" integer NOT NULL,
       "data_table" character varying NOT NULL,
       "original" geometry,
       "etrs10_geom" geometry,
       "etrs10_name" character varying(64),
       "etrs1_geom" geometry,
       "etrs1_name" character varying(13),
       "centroid" geometry,
       "judet" character varying,
       "comuna" character varying,
       "sci" character varying,
       "spa" character varying,
       "npa" character varying,
       CONSTRAINT "openherpmaps_threats_qgrids_pkey" PRIMARY KEY ("row_id", "data_table")
       ) WITH (oids = false);

       CREATE INDEX "openherpmaps_threats_qgrids_centroid_idx" ON "public"."openherpmaps_threats_qgrids" USING btree ("centroid");

       COMMENT ON COLUMN "public"."openherpmaps_threats_qgrids"."original" IS 'Original';

       COMMENT ON COLUMN "public"."openherpmaps_threats_qgrids"."etrs10_geom" IS 'ETRS 10x10 km';

       COMMENT ON COLUMN "public"."openherpmaps_threats_qgrids"."etrs1_geom" IS 'ETRS 1x1 km';

       INSERT INTO openherpmaps_threats_qgrids (row_id, data_table, original) SELECT obm_id, 'openherpmaps_threats', obm_geometry FROM openherpmaps_threats;
       UPDATE openherpmaps_threats_qgrids SET centroid = ST_Centroid(original);
       UPDATE openherpmaps_threats_qgrids SET etrs10_geom = st_transform(foo.geometry,4326), etrs10_name = foo.name
       FROM (
              SELECT d.obm_id,k.geometry,k.name FROM openherpmaps_threats d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,st_transform(k.geometry,4326)))
       ) as foo
       WHERE row_id=foo.obm_id AND data_table = 'openherpmaps_threats';

       UPDATE openherpmaps_threats_qgrids SET etrs1_geom = foo.geometry, etrs1_name = foo.name
       FROM (
              SELECT d.obm_id,k.geometry,k.cellcode as name FROM openherpmaps_threats d LEFT JOIN shared."etrs1" k ON (st_within(d.obm_geometry,k.geometry))
       ) as foo
       WHERE row_id=foo.obm_id AND data_table = 'openherpmaps_threats';
       GRANT ALL ON openherpmaps_threats_qgrids TO openherpmaps_admin;
COMMIT;