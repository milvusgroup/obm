CREATE RULE openherpmaps_del AS
    ON DELETE TO public.openherpmaps DO INSTEAD  DELETE FROM openherpmaps.openherpmaps
  WHERE (openherpmaps.obm_id = old.obm_id);
