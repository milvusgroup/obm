<?php
class ursinii_refresh_matviews extends job_module
{

    public function __construct($mtable)
    {
        parent::__construct(__CLASS__, $mtable);
    }

    public function init($params, $pa)
    {
        debug('ursinii_refresh_matviews initialized', __FILE__, __LINE__);
        return true;
    }

    static function run()
    {
        global $ID;
        job_log("running ursinii_refresh_matviews module");
        $cmd = "refresh materialized view openherpmaps.ursinii_monitoring_tracklogs; refresh materialized view openherpmaps.ursinii_monitoring_tracklog_summary;";
        pg_query($ID, $cmd);

        $cmd = sprintf("comment on materialized view openherpmaps.ursinii_monitoring_tracklogs IS 'last_refreshed %s'", date("Y-m-d H:i:s"));
        pg_query($ID, $cmd);
        job_log("ursinii_refresh_matviews finished");
    }
}
