<?php
class find_the_duplicates extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        global $ID;
        debug('find_the_duplicates initialized',__FILE__,__LINE__);
        return true;
    }

    public function get_results() {
        global $ID;
        return '';
    }

    static function run() {
        global $path, $ID;

        require_once($path . '/../includes/common_pg_funcs.php');
        
        $params = parent::getJobParams(__CLASS__);

        foreach($params as $ftd) {
            $table_columns = getColumnsOfTables($ftd->table);
            $columns = array_filter($ftd->columns,function ($col) use ($table_columns) {
                return (in_array($col, $table_columns));
            });

            //"SELECT ROW_NUMBER() OVER () rn, unnest(array_agg(obm_id)) as ids FROM openherpmaps group BY obm_geometry, specia, data, numar_exemplar, observator, data HAVING count(*) > 1 ORDER BY rn, ids;"
            $cmd = sprintf("SELECT obm_id, ROW_NUMBER() OVER (PARTITION BY (%s) ORDER BY obm_id DESC) rm FROM %s;", implode(',',$columns), $ftd->table);
   /*         
            $vv = implode(',', array_map( function($el) { 
                return "'$el'";
            } , $valid_values));
            
            $cmd[] = sprintf('
                WITH 
                    new AS ( 
                        SELECT obm_id FROM %1$s WHERE %4$s NOT IN (%5$s)
                    ), 
                    old AS (
                        SELECT row_id FROM %2$s_validation_results WHERE data_table = %3$s AND test_name = \'%6$s\' AND result LIKE %7$s
                    ) 
                    SELECT obm_id as new, row_id as corrected 
                    FROM new 
                    FULL JOIN old ON obm_id = row_id 
                    WHERE row_id IS NULL OR obm_id IS NULL;',
                $col->table, 
                PROJECTTABLE, 
                quote($col->table), 
                $col->column, 
                $vv, 
                __CLASS__, 
                quote($col->column.'%')
            );

            if (!$res = parent::query($ID, $cmd)) {
                job_log(__CLASS__.' error: query error');
                continue;
            }
            if (!$results = pg_fetch_all($res[0])) {
                continue;
            }

            $new_ids = array_values(array_filter(array_column($results, 'new'), function($id) {
                return ($id !== null);
            }));
            $corrected = array_values(array_filter(array_column($results, 'corrected'), function($id) {
                return ($id !== null);
            }));

            $cmd = [];
            if (count($corrected)) {
                $cmd[] = sprintf("DELETE FROM %s_validation_results WHERE data_table = %s AND row_id IN (%s) AND test_name = '%s' AND result LIKE %s;",PROJECTTABLE, quote($col->table), implode(',',$corrected),__CLASS__, quote($col->column . '%'));
            }

            if (count($new_ids)) {
                $insertValues = [];
                foreach ($new_ids as $id) {
                    $insertValues[] = sprintf("(%s,%s,'%s',%s)", quote($col->table), $id, __CLASS__, quote($col->column));
                }

                $cmd[] = sprintf("INSERT INTO %s_validation_results (data_table, row_id, test_name, result) VALUES %s ;", PROJECTTABLE, implode( ',' , $insertValues ) );
            }

            if (count($cmd)) {
                parent::query($ID, $cmd);
            }
    */
        }
    }


}
?>
