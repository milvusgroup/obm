<?php ?>

<div id='footer'>
    
    <div class="pure-g" id="footer-1st-row">
        <div class="pure-u-1 pure-u-sm-1-2">
                <ul>
                    <li><a href='https://<?php echo OB_PROJECT_DOMAIN ?>/?database' class='fl'> <?php echo t(str_summary) ?></a></li>
                    <li><a target='_blank' href='https://<?php echo OB_PROJECT_DOMAIN ?>/help/' class='fl'><?php echo t(str_help) ?></a></li>
                </ul>
        </div>
        <div class="pure-u-1 pure-u-sm-1-2 contrib">
            <a href='https://milvus.ro' class='fl_customb' target='_blank'><div class='inset' style='background-image:url(https://<?php echo URL.STYLE_PATH ?>/images/milvus-logo.png)'></div></a>
            <a href='https://unideb.hu' class='fl_customb' target='_blank'><div class='inset' style='background-image:url(https://<?php echo URL.STYLE_PATH ?>/images/unideb_logo.png)'></div></a>
            <a href='http://openbiomaps.org' class='fl_customb' target='_blank'><div class='inset' style='width:110px;background-image:url(https://<?php echo URL.STYLE_PATH ?>/images/obm_logo.png)'></div></a> 
        </div>
    </div>
    <hr>
    <div class="pure-g" id="footer-2nd-row">
        <div class="pure-u-1-2 pure-u-md-5-6 policies">
            <ul>
            <li><a href='https://<?php echo OB_PROJECT_DOMAIN ?>/privacy/' class='fl_custom'><?= t(str_privacy_policy_sajat); ?></a></li>
                <li><a href='https://<?php echo OB_PROJECT_DOMAIN ?>/terms/' class='fl_custom'><?= t(str_terms_and_conditions); ?></a></li>
                <li><a href='https://<?php echo OB_PROJECT_DOMAIN ?>/cookies/' class='fl_custom'><?= t(str_cookie_policy); ?></a></li>
            </ul>
        </div>
        <div class="pure-u-1-2 pure-u-md-1-6 social">
            <ul>
                <li>
                    <div id="fLang">
                        <ul>
                            <li><a href='https://www.facebook.com/openbirdmaps/' target="_blank" class='fl_custom'><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href='https://www.youtube.com/playlist?list=PLI4ZtBQ9maumpk4la5UabVfni30j6MuLe' class='fl_custom' target="_blank"><i class="fa fa-youtube-play"></i></a></li> 
                        </ul>
                    </div>
                <li style="margin-left: 1em; margin-top: 1em;">
                    <div id="fLang">
                        <ul>
                            <li><a href='?lang=ro' class='fl_custom'> <span class='flag-icon flag-icon-ro'></span> RO</a></li>
                            <li><a href='?lang=hu' class='fl_custom'> <span class='flag-icon flag-icon-hu'></span> HU</a></li>
                            <li><a href='?lang=en' class='fl_custom'> <span class='flag-icon flag-icon-en'></span> EN</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div><!--/footer-->
