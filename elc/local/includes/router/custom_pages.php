<?php

if (in_array('ajax_elc', $path)) {
    $load = '../local/includes/private/afuncs_elc.php';
}
if (in_array($path[0], ['terms', 'privacy', 'cookies'])) {
    $load = 'view.php';
    $chunks['includes'] = $path[0];
}

if ($path[0] === 'birdetect') {
    $load = 'view.php';
    $chunks['includes'] = 'birdetect';
}
if ($path[0] === 'monitoring') {
    $load = 'view.php';
    $chunks['includes'] = 'monitoring';
    if (isset($path[1]) && in_array($path[1], ['raptor'])) {
        $chunks['subpage'] = $path[1];
    }
}
