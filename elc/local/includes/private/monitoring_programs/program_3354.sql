-- save_square_info_insert
WITH ins as (
    INSERT INTO elc.raptor_survey_session (period_id, grouping_code, responsible) VALUES (%d, %s, %d) RETURNING id
)
INSERT INTO elc.raptor_observer (session_id, observer) SELECT id, unnest(ARRAY[%s]::integer[]) FROM ins;

-- save_square_info_update
UPDATE elc.raptor_survey_session SET responsible = %1$d WHERE id = %2$d;
DELETE FROM elc.raptor_observer WHERE session_id = %2$d;
INSERT INTO elc.raptor_observer (session_id, observer) SELECT %2$d, unnest(ARRAY[%3$s]::integer[]);

-- get_gcs
SELECT DISTINCT s.id, s.grouping_code, s.finished
FROM elc.raptor_survey_session s
LEFT JOIN elc.raptor_observer o ON s.id = o.session_id
WHERE 
    period_id = %1$d AND 
    ( responsible = %2$d OR observer = %2$d );

-- unoccupiedGcs_selector
SELECT gc.grouping_code 
FROM elc_grouping_codes gc 
LEFT JOIN elc.raptor_period p ON split_part(gc.program, ':', 1) = p.project AND gc.program = p.program
LEFT JOIN elc.raptor_survey_session s ON s.grouping_code = gc.grouping_code AND s.period_id = p.id
WHERE p.id = %d AND s.responsible IS NULL ORDER BY gc.grouping_code;

-- get_observed_species_list_pair_tool
SELECT DISTINCT species_valid as species 
FROM elc m
LEFT JOIN elc.raptor_period p ON
    m.projekt = p.project AND
    m.program = p.program AND 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.species_valid IN ( SELECT sp FROM unnest(p.species_list) as t(sp) )  
WHERE
    p.id = %d AND
    m.grouping_code = %s AND 
    m.sampling_unit = %s AND 
    GeometryType(m.obm_geometry) = 'POINT';
        
-- get_observed_species_list_double_tool
SELECT DISTINCT species_valid as species 
FROM elc m
LEFT JOIN elc.raptor_period p ON
    m.projekt = p.project AND
    m.program = p.program AND 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.species_valid IN ( SELECT sp FROM unnest(p.species_list) as t(sp) )  
WHERE
    p.id = %d AND
    m.grouping_code = %s AND 
    GeometryType(m.obm_geometry) = 'POINT';

-- get_observations
SELECT row_to_json(fc) as geojson
FROM ( 
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
    FROM (
        SELECT 
            'Feature' As type,
            ST_AsGeoJSON(lg.obm_geometry)::json As geometry,
            row_to_json((
                SELECT l FROM (
                    SELECT obm_id, observers, date, exact_time, species_valid as species, 
                    CASE WHEN observed_previously = 'str_possible' THEN 0 ELSE number END as number,
                    CASE WHEN observed_previously = 'str_possible' THEN 1 ELSE number_max END as number_max,
                        age,
                        gender, status_of_individuals, comments_observation, pair_id 
                    ) as l
            )) As properties
        FROM elc As lg   
        LEFT JOIN elc.raptor_period p ON
            lg.projekt = p.project AND
            lg.program = p.program AND 
            lg."date" BETWEEN p.date_from AND p.date_until
        WHERE 
            p.id = %d AND
            lg.grouping_code = %s AND 
            lg.sampling_unit = %s AND 
            lg.species_valid = %s AND
            lg.observed_previously IN ('str_no', 'str_possible') AND
            GeometryType(obm_geometry) = 'POINT'
    ) As f 
)  As fc;

-- get_results
WITH 
doubles as (
    SELECT 
        d.id as double_id, 
        s.grouping_code,
        p.species,
        p.id as pair_id, 
        d.status as double_status, 
        d.min,
        d.max
    FROM elc.raptor_double d
    LEFT JOIN elc.raptor_double_pair dp ON  dp.double_id = d.id
    LEFT JOIN elc.raptor_pair p ON dp.pair_id = p.id
    LEFT JOIN elc.raptor_survey_session s ON p.session_id = s.id
    WHERE 
        s.period_id = %1$d AND 
        s.grouping_code IN (%2$s)
),
pairs as (
    SELECT 
        p.id as pair_id, 
        s.grouping_code, 
        p.species, 
        p.min, 
        p.max
    FROM elc.raptor_pair p
    LEFT JOIN elc.raptor_survey_session s ON s.id = p.session_id
    LEFT JOIN doubles dbl on p.id = dbl.pair_id
    WHERE 
        dbl.pair_id IS NULL AND
        s.period_id = %1$d AND
        s.grouping_code IN (%2$s) AND 
        p.status IN ('str_certain_pair', 'str_possible_pair')
),
unionn as (
    SELECT double_id as id, grouping_code, species, min, max FROM doubles
    UNION 
    SELECT pair_id as id, grouping_code, species, min, max FROM pairs
) SELECT grouping_code, species, sum(min) as min, sum(max) as max FROM unionn GROUP by grouping_code, species;

-- get_results_2
SELECT 
    grouping_code as grouping_code, 
    sampling_unit as sampling_unit, 
    species_valid as species, 
    count(*) as total,
    count(pair_id) as evaluated,
    count(DISTINCT pair_id) as no_of_pairs
FROM elc m
LEFT JOIN elc.raptor_period p ON 
    m.projekt = p.project AND
    m.program = p.program AND 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.species_valid IN ( SELECT sp FROM unnest(p.species_list) as t(sp) )  
WHERE 
    p.id = %d AND
    grouping_code IN (%s) AND
    observed_previously IN ('str_no', 'str_possible') AND
    GeometryType(obm_geometry) = 'POINT' 
GROUP BY grouping_code, sampling_unit, species_valid
ORDER BY grouping_code, sampling_unit, species_valid;

-- get_results_3
SELECT grouping_code FROM elc.raptor_survey_session WHERE period_id = %d AND grouping_code IN (%s) AND finished = 'true';

-- save_pair_status
WITH ins1 as (    
    INSERT INTO elc.raptor_pair (session_id, sampling_unit, species, min, max, status, geometry)
    VALUES (%d, %s, %s, %d, %d, %s , ST_GeometryFromText(%s, 4326)) RETURNING id
),
ins2 as (
    INSERT INTO elc.raptor_pair_observation (pair_id, obm_id) 
    SELECT id, unnest(ARRAY[%s]) FROM ins1
)
SELECT id as pair_id FROM ins1;

-- is_pair_tool_finished
SELECT count(*) as unevaluated 
FROM elc m
LEFT JOIN elc.raptor_period p ON 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.program = p.program AND
    m.projekt = p.project
WHERE 
    p.id = %d AND
    grouping_code = %s AND
    species_valid = %s AND
    observed_previously IN ('str_no', 'str_possible') AND
    GeometryType(obm_geometry) = 'POINT' AND 
    pair_id IS NULL;

-- get_pairs
SELECT 
    row_to_json(fc) as geojson
FROM ( 
    SELECT 
        'FeatureCollection' As type, 
        array_to_json(array_agg(f)) As features
    FROM (
        SELECT 
            'Feature' As type, 
            ST_AsGeoJSON(p.geometry)::json As geometry,
            row_to_json(
                (
                    SELECT l FROM (
                        SELECT 
                            p.id as obm_id, 
                            p.species, 
                            s.grouping_code, 
                            p.sampling_unit,
                            p.status,
                            p.min,
                            p.max,
                            d.id as double_id, 
                            d.status as double_status
                    ) as l
                )
            ) As properties
        FROM elc.raptor_pair As p
        LEFT JOIN elc.raptor_survey_session s ON p.session_id = s.id
        LEFT JOIN elc.raptor_double_pair dp ON p.id = dp.pair_id
        LEFT JOIN elc.raptor_double d ON dp.double_id = d.id
        WHERE 
            s.period_id = %d AND
            s.grouping_code = %s AND 
            p.species = %s 
    ) As f 
)  As fc;

-- save_double_status
WITH mx AS (
    SELECT max("min") as min_pairs from elc.raptor_pair where id IN (%s)
),
ins AS (
    INSERT INTO elc.raptor_double (session_id, min, max, status) SELECT %d, min_pairs, %d, %s FROM mx RETURNING id
)
INSERT INTO elc.raptor_double_pair (double_id, pair_id) SELECT id, unnest(ARRAY[%s]) FROM ins;

-- get_sampling_unit_geom
SELECT row_to_json(fc) as geojson
FROM ( 
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
    FROM (
        SELECT 'Feature' As type
        , ST_AsGeoJSON(su.sampling_unit_geometry)::json As geometry
        , row_to_json((SELECT l FROM (SELECT 
            sampling_unit_id, 
            sampling_unit ) as l
        )) As properties
        
        FROM elc_sampling_units As su
        LEFT JOIN elc.raptor_period p ON p.program = su.program 
        WHERE
        p.id = %d AND
        su.grouping_code = %s %s AND
        GeometryType(su.sampling_unit_geometry) = 'POINT'
    ) As f 
)  As fc;
