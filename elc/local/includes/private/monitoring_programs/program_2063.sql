-- get_gcs
SELECT DISTINCT grouping_code 
FROM public.elc d
LEFT JOIN elc.rapmig_period p ON d.projekt = p.project AND d.program = p.program
WHERE 
    p.id = %1$d AND
    d."date" BETWEEN p.date_from AND p.date_until 

-- get_observed_species_list_double_tool
WITH sp_list as (
    SELECT unnest(p.species_list) as sp FROM elc.rapmig_period p WHERE id = %1$d 
    )
SELECT DISTINCT species_valid as species 
FROM public.elc m
LEFT JOIN elc.rapmig_period p ON
    m.program = p.program AND 
    m.projekt = p.project AND
    m."date" BETWEEN p.date_from AND p.date_until 
LEFT JOIN sp_list ON sp = m.species_valid
WHERE
    p.id = %1$d AND
    sp IS NOT NULL AND
    m.grouping_code = %2$s AND 
    GeometryType(m.obm_geometry) = 'POINT';

-- get_observations
SELECT row_to_json(fc) as geojson
FROM ( 
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
    FROM (
        SELECT 
            'Feature' As type,
            ST_AsGeoJSON(lg.obm_geometry)::json As geometry,
            row_to_json((
                SELECT l FROM (
                    SELECT lg.obm_id, observers, date, exact_time, extract(epoch from exact_time)::integer/60 as time_in_minutes, species_valid as species, number, age,
                        gender, status_of_individuals, comments_observation, d.id as double_id, d.status as double_status, sampling_unit_id,
                        CASE flight_direction 
                            WHEN 'str_S_N' THEN 0
                            WHEN 'str_SW_NE' THEN 45
                            WHEN 'str_W_E' THEN 90
                            WHEN 'str_NW_SE' THEN 135
                            WHEN 'str_N_S' THEN 180
                            WHEN 'str_NE_SW' THEN 225
                            WHEN 'str_E_W' THEN 270
                            WHEN 'str_SE_NW' THEN 315
                        ELSE -1
                        END as flight_direction
                    ) as l
            )) As properties
        FROM elc As lg   
        LEFT JOIN elc.rapmig_period p ON
            lg.projekt = p.project AND
            lg.program = p.program AND 
            lg."date" BETWEEN p.date_from AND p.date_until
        LEFT JOIN elc.rapmig_double_observation dbo ON dbo.obm_id = lg.obm_id
        LEFT JOIN elc.rapmig_double d ON d.id = dbo.double_id
        LEFT JOIN elc_sampling_units su ON lg.program = su.program AND lg.grouping_code = su.grouping_code AND lg.sampling_unit = su.sampling_unit
        WHERE 
            p.id = %d AND
            lg.grouping_code = %s AND 
            lg.species_valid = %s AND
            GeometryType(obm_geometry) = 'POINT'
            %s
    ) As f 
)  As fc;

-- get_results
WITH sp_l as (
    SELECT unnest(species_list) as species FROM elc.rapmig_period WHERE id = %1$d
    )
SELECT DISTINCT 
    grouping_code, sampling_unit, species_valid as species, sum(number) as no_ind, count(*) as no_obs,
    CASE WHEN sp_l.species IS NULL THEN 0 ELSE 1 END as is_raptor 
FROM public.elc d
LEFT JOIN elc.rapmig_period p ON d.projekt = p.project AND d.program = p.program
LEFT JOIN sp_l ON d.species_valid = sp_l.species
WHERE 
    p.id = %1$d AND
    (sp_l.species IS NOT NULL OR d.status_of_individuals IN ('str_02_in_active_migration')) AND
    d.date BETWEEN %2$s AND %3$s
GROUP BY grouping_code, sampling_unit, species_valid, sp_l.species;

-- get_results_2
WITH sp_l as (
    SELECT unnest(species_list) as species FROM elc.rapmig_period WHERE id = %1$d
    ),
joined_data as (
    SELECT 
        species_valid as species, number, rd.id as double_id, min, max, status
    FROM public.elc d
    LEFT JOIN elc.rapmig_period p ON d.projekt = p.project AND d.program = p.program
    LEFT JOIN elc.rapmig_double_observation rdo ON rdo.obm_id = d.obm_id
    LEFT JOIN elc.rapmig_double rd ON rdo.double_id = rd.id
    LEFT JOIN sp_l ON d.species_valid = sp_l.species
    WHERE 
        p.id = %1$d AND
        sp_l.species IS NOT NULL AND
        d.date BETWEEN %2$s AND %3$s
    ),
doubles_excluded as (
    SELECT DISTINCT double_id, species, min, max FROM joined_data where double_id IS NOT NULL
    ),
unioned as (
    SELECT species, min, max from doubles_excluded
    UNION ALL
    SELECT species, number as min, number as max FROM joined_data WHERE double_id IS NULL
    )
SELECT species, sum(min) as min, sum(max) as max FROM unioned GROUP BY species;

-- save_pair_status
WITH ins1 as (    
    INSERT INTO elc.rapmig_pair (session_id, sampling_unit, species, min, max, status, geometry)
    VALUES (%d, %s, %s, %d, %d, %s , ST_GeometryFromText(%s, 4326)) RETURNING id
),
ins2 as (
    INSERT INTO elc.rapmig_pair_observation (pair_id, obm_id) 
    SELECT id, unnest(ARRAY[%s]) FROM ins1
)
SELECT id as pair_id FROM ins1;

-- is_pair_tool_finished
SELECT count(*) as unevaluated 
FROM elc m
LEFT JOIN elc.rapmig_period p ON 
    m."date" BETWEEN p.date_from AND p.date_until AND 
    m.program = p.program AND
    m.projekt = p.project
WHERE 
    p.id = %d AND
    grouping_code = %s AND
    species_valid = %s AND
    GeometryType(obm_geometry) = 'POINT' AND 
    pair_id IS NULL;

-- get_pairs
SELECT 
    row_to_json(fc) as geojson
FROM ( 
    SELECT 
        'FeatureCollection' As type, 
        array_to_json(array_agg(f)) As features
    FROM (
        SELECT 
            'Feature' As type, 
            ST_AsGeoJSON(p.geometry)::json As geometry,
            row_to_json(
                (
                    SELECT l FROM (
                        SELECT 
                            p.id as obm_id, 
                            p.species, 
                            s.grouping_code, 
                            p.sampling_unit,
                            p.status,
                            p.min,
                            p.max,
                            d.id as double_id, 
                            d.status as double_status
                    ) as l
                )
            ) As properties
        FROM elc.rapmig_pair As p
        LEFT JOIN elc.rapmig_survey_session s ON p.session_id = s.id
        LEFT JOIN elc.rapmig_double_pair dp ON p.id = dp.pair_id
        LEFT JOIN elc.rapmig_double d ON dp.double_id = d.id
        WHERE 
            s.period_id = %d AND
            s.grouping_code = %s AND 
            p.species = %s 
    ) As f 
)  As fc;

-- save_double_status
WITH ins AS (
    INSERT INTO elc.rapmig_double (min, max, status) VALUES (%d, %d, %s) RETURNING id
)
INSERT INTO elc.rapmig_double_observation (double_id, obm_id) SELECT id, unnest(ARRAY[%s]) FROM ins RETURNING double_id;

-- get_sampling_unit_geom
SELECT row_to_json(fc) as geojson
FROM ( 
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
    FROM (
        SELECT 'Feature' As type
        , ST_AsGeoJSON(su.sampling_unit_geometry)::json As geometry
        , row_to_json((SELECT l FROM (SELECT 
            sampling_unit_id, 
            sampling_unit ) as l
        )) As properties
        
        FROM elc_sampling_units As su
        LEFT JOIN elc.rapmig_period p ON p.program = su.program 
        WHERE
        p.id = %d AND
        su.grouping_code = %s AND
        GeometryType(su.sampling_unit_geometry) = 'POINT'
    ) As f 
)  As fc;
