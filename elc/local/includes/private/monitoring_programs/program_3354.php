<?php 
/**
* 
*/
class Program_3354 extends Program
{
    public static $pid = 3354;
    public static $def_first_letter = 'c';
    public static $from_date = '2020-01-01';
    public static $until_date = '2020-12-31';
    public static $pair_states = ['str_certain_pair', 'str_possible_pair', 'str_not_pair', 'str_overfly', 'str_unknown'];
    public static $double_states = ['str_certain', 'str_possible'];
    public static $program = 'Dobrogea de Nord: Monitorizarea păsărilor rapitoare';
    public static $project = 'Dobrogea de Nord';
    public static $projec_admin_group = 1350;
    public static $su_table = 'elc_sampling_units';
    public static $gc_pattern = '/^[a-zA-Z0-9 \-_]+$/u';
    public static $su_pattern = '/^[0-9]{2}b?$/';
    public static $program_col = 'program';
    public static $grouping_code_col = 'grouping_code';
    public static $sampling_unit_col = 'sampling_unit';

    private static $periods = null;
    public static $period_id = null;

    public static function menu_items() {
        if (!isset($_SESSION['Tid'])) {
            return json_encode([]);
        }
        $menu = ['Main page', 'Pair tool', 'Dupla tool'];
        $user = new User($_SESSION['Tid']);
        if ($user->is_member_of(self::$projec_admin_group)) {
            $menu[] = 'Administration';
        }
        return json_encode($menu);
    }
    
    public static function administration($request) {
        if (!isset($_SESSION['Tid'])) {
            return "Please log in!";
        }
        ob_start();
        include("program_".self::$pid."_administration.php");
        $AdminClass = "Program_".self::$pid."_admin";
        
        $pu = new Role('project members');
        $AdminClass::$felhasznalok = $pu->get_members();

        self::set_periods($request['period_id'] ?? null);
        
        $first_letters = $AdminClass::get_first_letters(self::$period_id);
        $fl = $request['first_letter'] ?? $first_letters[0] ?? null;
        
        $negyzetek = ($fl) ? $AdminClass::get_administration_data($fl, self::$period_id) : [];
        
        ?>
        <style media="screen">
            .fs-wrap {
                width: 500px;
                position: relative;
            }
            .fs-dropdown {
                width: 500px;
            }
        </style>
        <h3>Felmérési időszakok</h3>
        <form class="pure-form" method="post">
            <?= self::period_selector(); ?>
        </form>
        <h4>Felelősök</h4>
        <div class="first_letters">
            <?php foreach ($first_letters as $fl): ?>
                <button type="button" name="button" class="fl-button pure-button button-small"><?= $fl ?></button>
            <?php endforeach; ?>
        </div>
        <form class="pure-form">
            
            <table class="pure-table pure-table-striped raptor_monitoring-administration">
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 35%;">
                    <col span="1" style="width: 40%;">
                    <col span="1" style="width: 5%;">
                </colgroup>
                <thead>
                    <tr>
                        <th>négyzet</th>
                        <th>pontok száma</th>
                        <th>felelős</th>
                        <th>felmérők</th>
                        <th>mentés</th>
                    </tr>
                </thead> 
                <tbody>
                    <?php foreach ($negyzetek as $n): extract($n); ?>
                        <tr data-sq="<?= $negyzet ?>" data-metaid="<?= $metaid ?>">
                            <td><?= $negyzet ?>
                                <?php if ($finished === 't'): ?>
                                    <i class="fa fa-check square-finished" style="color: #00A35A; font-size: 14px;"></i>
                                    <i class="fa fa-times reopen-square" title="Reopen square"></i>
                                <?php endif; ?>
                            </td>
                            <td><?= $pontok_szama ?></td>
                            <td><?= $AdminClass::felelos($felelos); ?> </td> 
                            <td><?= $AdminClass::felmerok($felmerok) ; ?></td>
                            <td> <button type="button" name="button" class="pure-button save_square_info button-success"> <i class="fa fa-floppy-o"></i> </button> </td>
                        </tr>
                    <?php endforeach; ?>   
                </tbody>
            </table>
        </form>
        <div class="first_letters">
            <?php foreach ($first_letters as $fl): ?>
                <button type="button" name="button" class="fl-button pure-button button-small"><?= $fl ?></button>
            <?php endforeach; ?>
        </div>
        <h4>Új négyzet hozzáadáasa</h4>
        <form id="new_square_form" class="pure-form"  method="post">
            <table class="pure-table pure-table-striped raptor_monitoring-administration">
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 35%;">
                    <col span="1" style="width: 40%;">
                    <col span="1" style="width: 5%;">
                </colgroup>
                <thead>
                    <tr> <th>négyzet <?= self::$period_id ?></th> <th>pontok száma</th> <th>felelős</th> <th>felmérők</th> <th>mentés</th> </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $AdminClass::unoccupiedGcs_selector(self::$period_id); ?> </td>
                        <td> <span class="no_of_points"></span> </td>
                        <td><?= $AdminClass::felelos(null); ?> </td> 
                        <td><?= $AdminClass::felmerok([]) ; ?></td>
                        <td> <button type="button" name="button" class="pure-button add_new_square button-success"> <i class="fa fa-floppy-o"></i> </button> </td>
                    </tr>
                </tbody>
            </table>
        </form>
        <?php
        return ob_get_clean();
    }
    
    public static function dupla_tool($req) {
        if (!isset($_SESSION['Tid'])) {
            return "Please log in";
        }

        self::set_periods($req['period_id'] ?? null);
        $gcs = self::get_gcs(self::$period_id, true);
        ob_start();
        ?>
        <form class="pure-form p<?=self::$pid ?>-menu">
            
            <?= self::period_selector(); ?>
            
            <select id="grouping_code" class="" name="">
                <option value="" selected disabled><?= t('str_square_code') ?></option>
                <?php foreach ($gcs as $si => $gc): ?>
                    <option data-si="<?= $si ?>" value="<?= $gc ?>"><?= $gc ?></option>
                <?php endforeach; ?>
            </select>
            <select id="species" name="">
                <option value="" selected disabled><?= t('str_species') ?></option>
            </select>
        </form>
        <div id="map">
            
        </div>
        <?php
        return ob_get_clean();
    }
    
    public static function pair_tool($req) {
        if (!isset($_SESSION['Tid'])) {
            return str_please_log_in_or_refresh_the_page;
        }
        self::set_periods($req['period_id'] ?? null);
        $gcs = self::get_gcs(self::$period_id, true);
        ob_start();
        ?>
        <form class="p<?= self::$pid ?>-menu pure-form">
            
            <?= self::period_selector(); ?>
            
            <select id="grouping_code" class="" name="">
                <option value="" selected disabled><?= t('str_square_code') ?></option>
                <?php foreach ($gcs as $si => $gc): ?>
                    <option data-si="<?= $si ?>" value="<?= $gc ?>"><?= $gc ?></option>
                <?php endforeach; ?>
            </select>
            <select id="sampling_unit" name="">
                <option value="" selected disabled><?= t('str_observation_point_name') ?></option>
            </select>
            <select id="species" name="">
                <option value="" selected disabled><?= t('str_species') ?></option>
            </select>
            <span class="obs_num"></span>
        </form>
        <div id="map">
            
        </div>
        <?php
        return ob_get_clean();
    }
    
    public static function main_page($req = null) {
        if (!isset($_SESSION['Tid'])) {
            return str_please_log_in_or_refresh_the_page;
        }
        ob_start();
        self::set_periods($req['period_id'] ?? null);
        $data = self::get_results();
        ?>
        <div class="container">
            <form class="pure-form p<?=self::$pid ?>-menu">
                <?= self::period_selector(); ?>
            </form>
            
            <?php foreach ($data['pairs'] as $square => $points): ?>
                
                <h2 style="text-align: center;"><?= $square; ?> </h2>
                <div class="tool-results">
                    <div class="pair_tool_results">
                        <h3><?= str_pair_tool_results ?></h3>
                        <p> <i class="fa fa-info-circle"></i> <?= str_pair_tool_additional_information ?></p>
                        <table class="pure-table pure-table-horizontal" style="text-align: center;">
                            <thead style="background: #817F75;">
                                <tr>
                                    <th><?= str_species ?></th>
                                    <th><?= str_number_of_observations_evaluated ?></th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php foreach ($points as $p => $s): ?>
                                    <tr><td colspan="3" style="background: #cdfff9;"> <strong> <?= $p; ?> </strong> </td> </tr>
                                    <?php foreach ($s as $row): ?>
                                        <tr>
                                            <td>
                                                <i><?= $row['species'] ?></i>
                                                <?php if ($row['evaluated'] === $row['total']): ?>
                                                    <i class="fa fa-check" style="color: #00A35A; font-size: 14px;"></i>
                                                <?php endif; ?>
                                            </td> 
                                            <td style="text-align: center;"><?= $row['evaluated'] . ' / ' . $row['total'] ?></td>
                                        </tr>
                                    <?php endforeach; ?>   
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="double_tool_results">
                        <h3><?= str_double_tool_results ?></h3>
                        <p> <i class="fa fa-info-circle"></i> <?= str_double_tool_additional_information ?></p>
                        <table class="pure-table pure-table-striped">
                            <thead style="background: #817F75;">
                                <tr>
                                    <th><?= str_species ?></th>
                                    <th><?= str_min ?></th>
                                    <th><?= str_max ?></th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php if (count($data['square_info'])): ?>
                                    <?php foreach (array_filter($data['square_info'], function($r) use ($square) {return ($r['grouping_code'] == $square);}) as $row): ?>
                                        <tr>
                                            <td><em><?= $row['species'] ?></em></td> 
                                            <td style="text-align: center;"><?= $row['min'] ?></td>
                                            <td style="text-align: center;"><?= $row['max'] ?></td>
                                        </tr>
                                    <?php endforeach; ?>   
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <br>
                        <?php if (!in_array($square, $data['finished_squares'])): ?>
                            <form class="finish-square pure-form">
                                <h3><?= str_finish_raptor_sqaure ?></h3>
                                <p><?= str_finish_raptor_sqaure_info ?></p>
                                <input type="hidden" name="square_code" value="<?= $square ?>">
                                <textarea name="finish_square_comment" rows="5" placeholder="<?= str_finish_raptor_sqaure_comment_placeholder ?>"></textarea>
                                <button type="submit" name="button" class="pure-button button-xlarge button-error">
                                    <i class="fa fa-flag-checkered" aria-hidden="true"></i> <?= str_finish_raptor_sqaure_btn ?>
                                </button>
                            </form>
                        <?php endif; ?>
                    </div>
                </div>
                
                <hr>
            <?php endforeach; ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public static function reopen_square($request) {
        global $ID;
        debug($request,__FILE__,__LINE__);
        
        $cmd = sprintf("UPDATE elc.raptor_survey_session SET finished = 'false' WHERE period_id = %d AND grouping_code = %s;", (int)$request['period'], quote($request['sq']));
        if (!$res = pg_query( $ID, $cmd ) ) {
            log_action('save error');
            return common_message('error', 'save error');
        }
        return common_message('ok','ok');
    }
    
    private static function get_gcs($period, $finished_excluded = false) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        
        if (!isset($_SESSION['Tid'])) {
            return [];
        }
        $cmd = sprintf(getSQL('get_gcs', $sql_file), $period, $_SESSION['Tid']);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query_error', __FILE__, __LINE__);
            return [];
        }
        $results = (pg_num_rows($res)) ? pg_fetch_all($res) : [];
        if ($finished_excluded) {
            $results = array_values(array_filter($results, function($gc) {
                return ($gc['finished'] !== 't');
            }));
        }
        return array_combine(
            array_column($results, 'id'),
            array_column($results, 'grouping_code')
        );
    }
    
    public static function get_sampling_units($request) {
        
        self::set_periods($request['period_id'] ?? null);
        /*
        if (!isset($request['gc']) || !preg_match(self::$gc_pattern, $request['gc'])) {
            return '';
        }
        */
        $alapegyseg_parameterek = [ 
            'list_definition' => '{
                "optionsTable": "'.self::$su_table.'",
                "valueColumn": "sampling_unit",
                "labelColumn": "sampling_unit",
                "preFilterColumn": [
                    "program",
                    "grouping_code"
                ],
                "preFilterValue": [
                    "'. self::$program . '",
                    "'.$request['gc'].'"
                ]
            }',
            'list' => "",
            'type' => "",
            'control' => "",
            'custom_function' => "",
            'default_value' => "",
        ];
        
        $s = new upload_select_list($alapegyseg_parameterek);
        return '<option value="" selected disabled>' . t('str_observation_point_name') . '</option>' . implode('',$s->get_options());
    }
    
    public static function get_sampling_unit_geom($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        /*
        if (!isset($request['gc']) || !preg_match(self::$gc_pattern, $request['gc'])) {
            return '';
        }
        */
        $su_cmd = "";
        if (isset($request['su'])) {
            /* 
            if (!preg_match(self::$su_pattern, $request['su'])) {
                return '';
            }
            */
            $su_cmd = "AND sampling_unit = '{$request['su']}'";
        }
        
        $cmd = sprintf(getSQL("get_sampling_unit_geom", $sql_file), 
            (int)$request['period'],
            quote($request['gc']),
            $su_cmd
        );
        if (! $res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        $mfp = pg_fetch_row($res);
        return common_message('ok',$mfp);
    }
    
    public static function get_observed_species_list($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        /*
        if (!isset($request['gc']) || !preg_match(self::$gc_pattern, $request['gc'])) {
            return common_message('error', 'gc pattern does not match');
        }
        */
        if (isset($request['su'])) { 
            /*
            if (!preg_match(self::$su_pattern, $request['su'])) {
                return common_message('error', 'su pattern does not match');
            }
            */
            $cmd = sprintf(getSQL("get_observed_species_list_pair_tool", $sql_file),
                (int)$request['period'],
                quote($request['gc']), 
                quote($request['su'])
            );
        }
        else {
            $cmd = sprintf(getSQL("get_observed_species_list_double_tool", $sql_file),
                (int)$request['period'],
                quote($request['gc'])
            );
        }
        
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        if (pg_num_rows($res) == 0) {
            return common_message('fail', str_no_observations_found);
        }
        $results = pg_fetch_all($res);
        $species = array_map(function($s) {
            return "<option value='{$s['species']}'>{$s['species']}</option>";
        }, $results);
        array_unshift($species, '<option value="" selected disabled>Species</option>');
        return common_message('ok',implode('', $species));
            
    }
    
    public static function get_observations($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        /*
        if (!isset($request['gc']) || !preg_match(self::$gc_pattern, $request['gc'])) {
            return '';
        }
        if (!isset($request['su']) || !preg_match(self::$su_pattern, $request['su'])) {
            return '';
        }
        */
        if (!preg_match('/^[a-zA-z ]+$/', $request['species'])) {
            return '';
        }
        
        $cmd = sprintf(
            getSQL(__FUNCTION__, $sql_file),
            (int)$request['period'],
            quote($request['gc']),
            quote($request['su']),
            quote($request['species'])
        );
                
        if (! $res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        $data = pg_fetch_row($res);
        return common_message('ok',$data);
    }
    
    public static function get_results() {
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        global $ID;
        $gcs = self::get_gcs(self::$period_id);
        $cmd = sprintf(getSQL(__FUNCTION__, $sql_file),
            self::$period_id,
            implode(',',array_map('quote',$gcs))
        );
        if (! $res = pg_query($ID, $cmd)) {
            return false;
        }
        $square_info = (pg_num_rows($res)) ? pg_fetch_all($res) : [];
        
        $from_date = self::$from_date;
        $until_date = self::$until_date;
        
        $cmd = sprintf(getSQL("get_results_2", $sql_file),
            self::$period_id,
            implode(',',array_map('quote',$gcs))
        );
        if (! $res = pg_query($ID, $cmd)) {
            return false;
        }
        
        $data = pg_fetch_all($res);
        $pairs = [];
        foreach ($data as $row) {
            $pairs[$row['grouping_code']][$row['sampling_unit']][] = $row;
        }
        
        $cmd = sprintf(getSQL("get_results_3", $sql_file),
            self::$period_id,
            implode(',',array_map('quote',$gcs))
        );
        if (! $res = pg_query($ID, $cmd)) {
            return false;
        }
        
        $finished_squares = (pg_num_rows($res) > 0) ? array_column(pg_fetch_all($res),'grouping_code') : [];
        
        return compact('pairs', 'square_info', 'finished_squares');
        
    }
    
    public static function is_pair_tool_finished($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        
        $cmd = sprintf(getSQL(__FUNCTION__, $sql_file),
            (int)$request['period'],
            quote($request['gc']),
            quote($request['species'])
        );
        if (! $res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        $evaluaton = pg_fetch_assoc($res);
        return ($evaluaton['unevaluated'] == 0);
    }
    
    public static function get_pairs($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        /*
        if (!isset($request['gc']) || !preg_match(self::$gc_pattern, $request['gc'])) {
            return '';
        }
        */
        if (!preg_match('/^[a-zA-z ]+$/', $request['species'])) {
            return '';
        }
        //if (!self::is_pair_tool_finished($request)) {
        //    return common_message('fail', str_finish_the_pair_tool_first);
        //}
        
        $cmd = sprintf(
            getSQL(__FUNCTION__, $sql_file),
            (int)$request['period'],
            quote($request['gc']),
            quote($request['species'])
        );
            
        if (! $res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        $data = pg_fetch_row($res);
        return common_message('ok',$data);
    }
    
    public static function get_pair_status_selector() {
        return self::status_selector('pair');
    }
    
    public static function get_double_status_selector() {
        return self::status_selector('double');
    }
    
    private static function status_selector($what) {
        $id = "{$what}_status_selector";
        $btn_id = "save_$what";
        $arr = "{$what}_states";
        ob_start();
        ?>
        <form class="pure-form as_vertical_flex">
                <label for="<?= $id ?>"> <?= t("str_{$id}_label"); ?> </label>
                <select id="<?= $id ?>">
                    <option value="" selected disabled><?= str_please_choose ?></option>
                    <?php foreach (self::$$arr as $st): ?>
                        <option value="<?= $st ?>"><?= t($st) ?></option>
                    <?php endforeach; ?>
                </select>
            <?php if ($what === 'double'): ?>
                <label for="max_pairs"> <?= t("str_max_pairs_label"); ?> </label> 
                <input type="number" id="max_pairs" name="max_pairs" min="1" disabled>
            <?php endif; ?>
            <button type="button" id="<?= $btn_id ?>" class="pure-button button-success"> <i class="fa fa-floppy-o"></i> </button>
        </form>
        <?php
        
        return ob_get_clean();
    }

    public static function save_pair_status($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        
        $validation_failed = 'Invalid %s provided!';
        if (!isset($request['si']) && !is_numeric($request['si'])) {
            return common_message('error', sprintf($validation_failed, 'survey_session id'));
        }
        /*
        if (!isset($request['su']) || !preg_match(self::$su_pattern, $request['su'])) {
            return common_message('error', sprintf($validation_failed, 'sampling unit'));
        }
        */
        if (!preg_match('/^[a-zA-z ]+$/', $request['sp'])) {
            return common_message('error', sprintf($validation_failed, 'species'));
        }
        if (!in_array($request['status'], self::$pair_states)) {
            return common_message('error', sprintf($validation_failed, 'status'));
        }
        if (array_reduce($request['ids'], function($valid, $id) { return $valid + !is_numeric($id); }, 0) > 0) {
            return common_message('error', sprintf($validation_failed, 'ids'));
        }
        if (!preg_match('/^POINT ?\([0-9\.]+ [0-9\.]+\)$/', $request['pair_geom'])) {
            return common_message('error', sprintf($validation_failed, 'geometry'));
        }
        
        $obm_ids = implode(',', array_map(function($id) {return (int)$id; }, $request['ids']));
        $min = ($request['status'] == 'str_certain_pair') ? 1 : 0;
        $max = ($request['status'] == 'str_not_pair') ? 0 : 1;
        
        $cmd = sprintf(getSQL('save_pair_status', $sql_file), 
            (int)$request['si'], quote($request['su']), quote($request['sp']), 
            $min, $max, quote($request['status']), quote($request['pair_geom']),
            $obm_ids
        );
        
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error', 'pair staus save query error');
        }
        $result = pg_fetch_assoc($res);
        $id = $result['pair_id'];
        $cmd = sprintf("UPDATE %s SET pair_id = %d WHERE obm_id IN (%s);", 
            PROJECTTABLE,
            $id,
            implode(',',$request['ids'])
        );
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error', 'pair staus save query error');
        }
        return common_message('ok',$id);
    }
    
    public static function unpair($request) {
        global $ID;
        $validation_failed = 'Invalid %s provided!';
        if (!is_numeric($request['pair_id'])) {
            return common_message('error', sprintf($validation_failed, 'pair_id'));
        }
        $pair_id = (int)$request['pair_id'];
        
        // Getting the id of the eventual double connections.
        $cmd = sprintf("SELECT double_id FROM elc.raptor_double_pair WHERE pair_id = %d;", $pair_id);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('unpair query error', __FILE__,__LINE__);
            return common_message('fail', "Pair removal failed.");
        }
        // Removing the double connections
        while ($row = pg_fetch_assoc($res)) {
            if (!self::delete_meta('double', $row['double_id'])) {
                return common_message('fail', "Pair removal failed.");
            }
        }        
        // Removing the pair connection
        if (!self::delete_meta('pair', $pair_id)) {
            return common_message('fail', "Pair removal failed.");
        }

        // Updating the mai table's pair_id column
        $cmd = sprintf("UPDATE %s SET pair_id = NULL WHERE pair_id = %d;", PROJECTTABLE, (int)$request['pair_id']);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('unpair query error', __FILE__,__LINE__);
            return common_message('fail', "Pair removal failed.");
        }
        return common_message('ok','ok');
    }
 
    public static function undouble($request) {
        global $ID;
        
        $validation_failed = 'Invalid %s provided!';
        if (!is_numeric($request['double_id'])) {
            return common_message('error', sprintf($validation_failed, 'double_id'));
        }
        $cmd = sprintf("SELECT pair_id FROM elc.raptor_double_pair WHERE double_id = %d;" , (int)$request['double_id']);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('undouble query error',__FILE__,__LINE__);
            return common_message('fail', "undouble query error");
        }
        $pair_ids = array_column(pg_fetch_all($res), 'pair_id');
        
        return (!self::delete_meta('double', (int)$request['double_id'])) 
            ? common_message('fail', "Double removal failed.")
            : common_message('ok', $pair_ids);
    }
    
    private static function delete_meta($what, $id) {
        global $ID;
        if (!in_array($what, ['pair', 'double'])) {
            return false;
        }
        $cmd = sprintf("DELETE FROM elc.raptor_$what WHERE id = %d;", $id);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('delete_meta query error',__FILE__,__LINE__);
            return false;
        }
        return true;
        
    }
    
    public static function save_double_status($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        $validation_failed = 'Invalid %s provided!';
        if (!isset($request['si']) && !is_numeric($request['si'])) {
            return common_message('error', sprintf($validation_failed, 'survey_session id'));
        }
        if (!is_numeric($request['max'])) {
            return common_message('error', sprintf($validation_failed, 'max pair number'));
        }
        if (!in_array($request['status'], self::$double_states)) {
            return common_message('error', sprintf($validation_failed, 'status'));
        }
        if (array_reduce($request['ids'], function($valid, $id) { return $valid + !is_numeric($id); }, 0) > 0) {
            return common_message('error', sprintf($validation_failed, 'ids'));
        }
        $pair_ids = array_map(function($id) {return (int)$id; }, $request['ids']);
        
        $pair_ids_str = implode(',', $pair_ids);
        $cmd = sprintf(getSQL(__FUNCTION__, $sql_file),
            $pair_ids_str,
            (int)$request['si'],
            (int)$request['max'],
            quote($request['status']),
            $pair_ids_str
        );
        
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error', 'pair status save query error');
        }
        $result = pg_fetch_assoc($res);
        $id = $result['obm_id'];
    
        return common_message('ok',$id);
    }
    
    public static function finish_raptor_sqaure($request) {
        global $ID;
        $finish_failed = 'Invalid %s provided!';
        if (!isset($request['period_id']) && !is_numeric($request['period_id'])) {
            return common_message('error', sprintf($finish_failed, 'period_id'));
        }
        /*
        if (!isset($request['square_code']) || !preg_match(self::$gc_pattern, $request['square_code'])) {
            return common_message('error', sprintf($finish_failed, 'square_code'));
        }
        */
        $data = [
            'comment' => $request['finish_square_comment'],
        ];
        $cmd = sprintf("UPDATE elc.raptor_survey_session SET finished = true, cmnt = %s WHERE period_id = %d AND grouping_code = %s;", 
            quote($request['finish_square_comment']),  
            (int)$request['period_id'],
            quote($request['square_code'])
        );
        
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error', 'finish square query error');
        }
        return common_message('ok','ok');
    }
    
    public static function get_translations() {
        $translations = dbcolist('array');
        $translations = array_merge($translations, [
            'str_n_observations_selected' => t('str_n_observations_selected'),
            'str_reset' => t('str_reset'),
            'str_please_select_at_least_two_pairs' => t('str_please_select_at_least_two_pairs'),
            'str_select_pair_status' => t('str_select_pair_status'),
            'str_please_select_at_least_one_observation' => t('str_please_select_at_least_one_observation'),
            'str_finish_raptor_sqaure_confirm' => t('str_finish_raptor_sqaure_confirm'),
            'str_finish_raptor_sqaure_success' => t('str_finish_raptor_sqaure_success'),
            'str_no_certain_or_possible_pairs' => t('str_no_certain_or_possible_pairs'),
            'str_number_of_observations_evaluated' => t('str_number_of_observations_evaluated')
        ]);
        return json_encode($translations);
    }
    
    public static function set_periods($period_id = null) {
        global $ID;
        $res = pg_query($ID, "SELECT * FROM elc.raptor_period ORDER BY id DESC;");
        self::$periods = pg_fetch_all($res);
        self::$period_id = $period_id ?? self::$periods[0]['id'] ?? null;
        if (self::$period_id) {
            $period = array_values(array_filter(self::$periods, function ($p) {
                return ($p['id'] === self::$period_id);
            }));
            if (count($period)) {
                $period = $period[0];
                self::$program = $period['program'];
                self::$project = $period['project'];
                self::$from_date = $period['date_from'];
                self::$until_date = $period['date_until'];
            }
        }
    }
    
    public static function period_selector() {
        ob_start();
        if (!self::$periods) {
            self::set_periods();
        }
        $periods = self::$periods;
        ?>
        <select class="period_select" name="period">
            <?php foreach ($periods as $period): ?>
                <option value="<?= $period['id'] ?>" <?= ($period['id'] === self::$period_id) ? "selected" : "" ?> > <?= $period['period_name'] ?></option>
            <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
}

?>
