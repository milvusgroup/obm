<?php 
/**
* 
*/
class Program_2063 extends Program
{
    public static $pid = 2063;
    public static $pair_states = ['str_certain_pair', 'str_possible_pair', 'str_not_pair', 'str_overfly', 'str_unknown'];
    public static $double_states = ['str_certain', 'str_possible'];
    public static $su_table = 'elc_sampling_units';
    public static $gc_pattern = '/^[a-zA-Z0-9 \-_]+$/u';
    public static $su_pattern = '/^[0-9]{2}b?$/';
    public static $program_col = 'program';
    public static $grouping_code_col = 'grouping_code';
    public static $sampling_unit_col = 'sampling_unit';
    public static $sql_file = __DIR__ . '/program_2063.sql';

    private static $periods = null;
    public static $period_id = null;

    public static $felhasznalok = [];
    
    public static function menu_items() {
        if (!isset($_SESSION['Tid'])) {
            return json_encode([]);
        }
        $menu = ['Main page', 'Dupla tool'];
        $user = new User($_SESSION['Tid']);
        return json_encode($menu);
    }
    
    public static function dupla_tool($req = null) {
        if (!isset($_SESSION['Tid'])) {
            return "Please log in";
        }

        self::set_periods($req['period_id'] ?? null);
        $gcs = self::get_gcs(self::$period_id, true);
        ob_start();
        ?>
        <form id="dupla-tool-form" class="pure-form p16-menu">
            <div>
                <?= self::period_selector(); ?>
                
                <select id="grouping_code" class="" name="gc">
                    <option value="" selected disabled><?= t('str_square_code') ?></option>
                    <?php foreach ($gcs as $si => $gc): ?>
                        <option data-si="<?= $si ?>" value="<?= $gc ?>"><?= $gc ?></option>
                    <?php endforeach; ?>
                </select>
                <select id="species" class="load_observations" name="species">
                    <option value="" selected disabled><?= t('str_species') ?></option>
                </select>
            </div>
            <div>
                <select id="date" class="load_observations" name="date">
                    <option value="" selected disabled><?= t('str_date') ?></option>
                </select>
                <div class="slider-wrapper">
                    <div id="slider-timespan">
                        <div id="custom-handle-timespan" class="ui-slider-handle"></div>
                    </div>
                </div>
                <input type="number" hidden class="time-window" id="timespan" name="timespan" value="20">
                <div id="time-slider-wrapper" class="slider-wrapper">
                    <div id="slider-time">
                        <div id="custom-handle-time" class="ui-slider-handle"></div>
                    </div>
                </div>
                <input type="number" hidden class="time-window" id="time" name="time" value="540">
            </div>
        </form>
        <div id="map"> </div>
        <?php
        return ob_get_clean();
    }
    
    public static function main_page($req = null) {
        if (!isset($_SESSION['Tid'])) {
            return str_please_log_in_or_refresh_the_page;
        }
        if (isset($req['date_from']) && !preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $req['date_from'])) {
            unset($req['date_from']);
        }
        if (isset($req['date_until']) && !preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $req['date_until'])) {
            unset($req['date_until']);
        }
        ob_start();
        
        self::set_periods($req['period_id'] ?? null);
        $d = self::get_results($req);
        $data = $d['raw'];
        $sampling_units = array_unique(array_column($data, 'sampling_unit'));
        
        $species_list = array_unique(array_column($data, 'species'));
        
        $data_restructured = [];
        foreach ($sampling_units as $su) {
            foreach ($species_list as $sp) {
                $df = array_values(array_filter($data, function ($row) use ($su, $sp) {
                    return ($row['sampling_unit'] === $su) && ($row['species'] === $sp);
                }));
                $data_restructured[$su][$sp] = ["no_ind" => $df[0]['no_ind'] ?? null, "no_obs" => $df[0]['no_obs'] ?? null];
            }
        }
        ?>
        <form id="rapmig_mainpage_params" class="pure-form p16-menu">
            <?= self::period_selector(); ?>
            <input value="<?= $req['date_from'] ?? '' ?>" type="text" class="datepicker" name="date_from">
            <input value="<?= $req['date_until'] ?? '' ?>" type="text" class="datepicker" name="date_until">
        </form>
        <div id="rapmig-main-page" class="container">
            <h3> <?= t('str_target_species') ?> </h3>
            <?php 
                $species_list = array_unique(array_column(array_filter($data, function ($row) {
                    return $row['is_raptor'] === '1';
                }), 'species'));
                sort($species_list);
                self::generate_main_page_table($sampling_units, $species_list, $data_restructured, $d['doubles_excluded']);
            ?>
            <br>
            <h3><?= t('str_other_species') ?></h3>
            <?php
            $species_list = array_unique(array_column(array_filter($data, function ($row) {
                return $row['is_raptor'] === '0';
            }), 'species'));
            sort($species_list);
            self::generate_main_page_table($sampling_units, $species_list, $data_restructured);
             ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    private function generate_main_page_table($sampling_units, $species_list, $data, $final_total = []) {
        $total_su = ['ind' => [], 'obs' => [], 'final' => ['min' => 0, 'max' => 0]];
        ?>
        <table class="rapmig_mainpage_table pure-table pure-table-bordered">
            <thead>
                <tr>
                    <th rowspan="2"><?= t('str_species') ?></th>
                    <?php foreach ($sampling_units as $su): ?>
                        <th colspan="2"><?= $su ?></th>
                    <?php endforeach; ?>
                    <th rowspan="2"><?= t('str_total_ind') ?></th>
                    <th rowspan="2"><?= t('str_total_filtered') ?></th>
                    <th rowspan="2"><?= t('str_total_obs') ?></th>
                </tr>
                <tr>
                    <?php foreach ($sampling_units as $su): ?>
                        <th>ind</th>
                        <th>obs</th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($species_list as $sp): 
                    $total_ind = 0;
                    $total_obs = 0;
                ?>
                    <tr>
                        <td class="spn"><em><?= $sp ?></em></td>
                        <?php foreach ($sampling_units as $su): 
                            $no_ind = (int)$data[$su][$sp]['no_ind'];
                            $no_obs = (int)$data[$su][$sp]['no_obs'];
                            $total_ind += $no_ind;
                            $total_obs += $no_obs;
                            if (!isset($total_su['ind'][$su])) {
                                $total_su['ind'][$su] = 0;
                                $total_su['obs'][$su] = 0;
                            }
                            $total_su['ind'][$su] += $no_ind;
                            $total_su['obs'][$su] += $no_obs;
                        ?>
                            <td><strong><?= ($no_ind) ? $no_ind : "" ?></strong></td>
                            <td><?= ($no_obs) ? $no_obs : "" ?></td>
                        <?php endforeach; ?>
                        <td class="text-red"><?= $total_ind ?></td>
                        <td><strong class="text-red">
                            <?php 
                                if (isset($final_total[$sp])) {
                                    $total_su['final']['min'] += $final_total[$sp]['min'];
                                    $total_su['final']['max'] += $final_total[$sp]['max'];
                                    if ($final_total[$sp]['min'] === $final_total[$sp]['max']) {
                                        echo $final_total[$sp]['min'];
                                    }
                                    else {
                                        echo "{$final_total[$sp]['min']} - {$final_total[$sp]['max']}";
                                    }
                                }
                                else {
                                    echo "";
                                } 
                                ?>
                        </stong></td>
                        <td class="text-red"><?= $total_obs ?></td>
                    </tr>
                <?php endforeach; 
                    $total_ind = $total_obs = 0;
                ?>
                <tr>
                    <td><strong><?= t('str_total') ?></strong></td>
                    <?php foreach ($sampling_units as $su): 
                        $total_ind += $total_su['ind'][$su];
                        $total_obs += $total_su['obs'][$su];
                    ?>
                        <td class='text-red'><?= $total_su['ind'][$su] ?></td>
                        <td class='text-red'><?= $total_su['obs'][$su] ?></td>
                    <?php endforeach; ?>
                    <td><strong class='text-red'><?= $total_ind ?></strong></td>
                    <td><strong class='text-red'><?= ($total_su['final']['min'] === $total_su['final']['max']) ? $total_su['final']['min'] : "{$total_su['final']['min']} - {$total_su['final']['max']}" ?></strong></td>
                    <td><strong class='text-red'><?= $total_obs ?></strong></td>
                </tr>
            </tbody>
        </table>
        <?php
    }
    
    private static function extract_and_sort_species_list($data) {
        debug($raptor_list, __FILE__, __LINE__);
        $non_raptor_data = array_filter($data, function ($row) {
            return $row['is_raptor'] === '0';
        });
        $non_raptor_list = array_unique(array_column($non_raptor_data, 'species'));
        sort($non_raptor_list);
        debug($non_raptor_list, __FILE__, __LINE__);
        return array_merge($raptor_list, $non_raptor_list);
    }
    
    private static function get_gcs($period) {
        global $ID;
        
        if (!isset($_SESSION['Tid'])) {
            return [];
        }
        $cmd = sprintf(getSQL('get_gcs', self::$sql_file), $period); 
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query_error:' . $cmd, __FILE__, __LINE__);
            return [];
        }
        $results = (pg_num_rows($res)) ? pg_fetch_all($res) : [];
        return array_column($results, 'grouping_code');
    }
    
    public static function get_sampling_unit_geom($request) {
        global $ID;
        
        $cmd = sprintf(getSQL(__FUNCTION__, self::$sql_file), 
            (int)$request['period'],
            quote($request['gc'])
        );
        if (! $res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        $mfp = pg_fetch_row($res);
        return common_message('ok',$mfp);
    }
    
    public static function get_observed_species_list($request) {
        global $ID;
        $cmd = sprintf(getSQL("get_observed_species_list_double_tool", self::$sql_file),
            (int)$request['period'],
            quote($request['gc'])
        );
        
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        if (pg_num_rows($res) == 0) {
            return common_message('fail', str_no_observations_found);
        }
        $results = pg_fetch_all($res);
        $species = array_map(function($s) {
            return "<option value='{$s['species']}'>{$s['species']}</option>";
        }, $results);
        array_unshift($species, '<option value="" selected disabled>Species</option>');
        return common_message('ok',implode('', $species));
            
    }
    
    public static function get_observations($request) {
        global $ID;
        if (!preg_match('/^[a-zA-z ]+$/', $request['species'])) {
            return '';
        }
        $date_clause = ($request['date']) ? "AND lg.date = " . quote($request['date']) : "";
        $cmd = sprintf(
            getSQL(__FUNCTION__, self::$sql_file),
            (int)$request['period'],
            quote($request['gc']),
            quote($request['species']),
            $date_clause
        );
                
        if (! $res = pg_query($ID, $cmd)) {
            return common_message('error','query error');
        }
        $data = pg_fetch_row($res);
        return common_message('ok',$data);
    }
    
    public static function get_results($req) {
        global $ID;
        $date_from = $req['date_from'] ?? self::$periods[self::$period_id]['date_from'];
        $date_until = $req['date_until'] ?? self::$periods[self::$period_id]['date_until'];
        
        $cmd = sprintf(
            getSQL(__FUNCTION__, self::$sql_file), 
            self::$period_id,
            quote($date_from),
            quote($date_until)
        ); 
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query_error:' . $cmd, __FILE__, __LINE__);
            return [];
        }
        $results = (pg_num_rows($res)) ? pg_fetch_all($res) : [];
        
        $cmd = sprintf(
            getSQL('get_results_2', self::$sql_file), 
            self::$period_id,
            quote($date_from),
            quote($date_until)
        ); 
        if (!$res2 = pg_query($ID, $cmd)) {
            log_action('query_error:' . $cmd, __FILE__, __LINE__);
            return [];
        }
        
        $results2 = [];
        while ($row = pg_fetch_assoc($res2)) {
            $results2[$row['species']] = [
                'min' => $row['min'],
                'max' => $row['max'],
            ];
        }
        
        return [
            "raw" => $results,
            "doubles_excluded" => $results2
        ];
    }
    
    public static function get_double_status_toolbox() {
        $id = "double_status_selector";
        $btn_id = "save_double";
        ob_start();
        ?>
        <form class="pure-form as_vertical_flex">
                <label for="<?= $id ?>"> <?= t("str_{$id}_label"); ?> </label>
                <select id="<?= $id ?>">
                    <option value="" selected disabled><?= str_please_choose ?></option>
                    <?php foreach (self::$double_states as $st): ?>
                        <option value="<?= $st ?>"><?= t($st) ?></option>
                    <?php endforeach; ?>
                </select>
                <label for="max_number"> <?= t("str_max_number_label"); ?> </label> 
                <input type="number" id="max_number" name="max_number" min="1" disabled>
            <button type="button" id="<?= $btn_id ?>" class="pure-button button-success"> <i class="fa fa-floppy-o"></i> </button>
        </form>
        <?php
        return ob_get_clean();
    }

    public static function undouble($request) {
        global $ID;
        
        $validation_failed = 'Invalid %s provided!';
        if (!is_numeric($request['double_id'])) {
            return common_message('error', sprintf($validation_failed, 'double_id'));
        }
        
        $id = (int)$request['double_id'];
        $cmd = sprintf("SELECT obm_id FROM elc.rapmig_double_observation WHERE double_id = %d;" , $id);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('undouble query error',__FILE__,__LINE__);
            return common_message('fail', "undouble query error");
        }
        $obm_ids = array_column(pg_fetch_all($res), 'obm_id');
        
        $cmd = sprintf("DELETE FROM elc.rapmig_double WHERE id = %d;", $id);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('delete_meta query error',__FILE__,__LINE__);
            return common_message('fail', "Double removal failed.");
        }
        return common_message('ok', $obm_ids);
    }
    
    public static function save_double_status($request) {
        global $ID;
        $validation_failed = 'Invalid %s provided!';
        if (!isset($request['si']) && !is_numeric($request['si'])) {
            return common_message('error', sprintf($validation_failed, 'survey_session id'));
        }
        if (!is_numeric($request['max'])) {
            return common_message('error', sprintf($validation_failed, 'max pair number'));
        }
        if (!in_array($request['status'], self::$double_states)) {
            return common_message('error', sprintf($validation_failed, 'status'));
        }
        if (array_reduce($request['ids'], function($valid, $id) { return $valid + !is_numeric($id); }, 0) > 0) {
            return common_message('error', sprintf($validation_failed, 'ids'));
        }
        $pair_ids = array_map(function($id) {return (int)$id; }, $request['ids']);
        
        $pair_ids_str = implode(',', $pair_ids);
        $cmd = sprintf(getSQL(__FUNCTION__, self::$sql_file),
            (int)$request['min'],
            (int)$request['max'],
            quote($request['status']),
            $pair_ids_str
        );
        
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error', 'pair status save query error');
        }
        $result = pg_fetch_assoc($res);
        $id = $result['double_id'];
    
        return common_message('ok',$id);
    }
    
    public static function get_translations() {
        $translations = dbcolist('array');
        $translations = array_merge($translations, [
            'str_n_observations_selected' => t('str_n_observations_selected'),
            'str_reset' => t('str_reset'),
            'str_please_select_at_least_two_pairs' => t('str_please_select_at_least_two_pairs'),
            'str_select_pair_status' => t('str_select_pair_status'),
            'str_please_select_at_least_one_observation' => t('str_please_select_at_least_one_observation'),
            'str_finish_rapmig_sqaure_confirm' => t('str_finish_rapmig_sqaure_confirm'),
            'str_finish_rapmig_sqaure_success' => t('str_finish_rapmig_sqaure_success'),
            'str_no_certain_or_possible_pairs' => t('str_no_certain_or_possible_pairs'),
            'str_number_of_observations_evaluated' => t('str_number_of_observations_evaluated')
        ]);
        return json_encode($translations);
    }
    
    public static function set_periods($period_id = null) {
        global $ID;
        $res = pg_query($ID, "SELECT * FROM elc.rapmig_period ORDER BY id DESC;");
        self::$periods = [];
        self::$period_id = $period_id;
        if (pg_num_rows($res)) {
            $results = pg_fetch_all($res);
            self::$period_id = $period_id ?? $results[0]['id'];
            self::$periods = array_combine(array_column($results, 'id'), $results);
        } 
    }
    
    public static function period_selector() {
        ob_start();
        if (!self::$periods) {
            self::set_periods();
        }
        $periods = self::$periods;
        ?>
        <select class="period_select" name="period">
            <?php foreach ($periods as $period): ?>
                <option 
                    value="<?= $period['id'] ?>" 
                    <?= ($period['id'] === self::$period_id) ? "selected" : "" ?> 
                    data-date_from="<?= $period['date_from'] ?>"
                    data-date_until="<?= $period['date_until'] ?>"
                > 
                    <?= $period['period_name'] ?>
                </option>
            <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
}

?>
