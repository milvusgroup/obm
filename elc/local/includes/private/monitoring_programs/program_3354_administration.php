<?php 
/**
 * 
 */
class Program_3354_admin
{
    public static $felhasznalok = [];
    

    public static function get_first_letters($period_id) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT SUBSTRING(grouping_code, 1, 1) as fl from elc.raptor_survey_session where period_id = %d ORDER BY fl;", $period_id);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('get_first_letters query error', __FILE__,__LINE__);
            return false;
        }
        return (pg_num_rows($res)) ? array_column(pg_fetch_all($res), 'fl') : [];
    }
    
    public static function get_administration_data($first_letter, $period_id) {
        global $ID;
        $first_letter = substr($first_letter, 0, 1);
        $cmd = sprintf("SELECT * FROM elc.raptor_administration WHERE period_id = %d AND grouping_code LIKE '%s%%' ORDER BY grouping_code;", $period_id, $first_letter);
        if (!$res = pg_query($ID, $cmd)) {
            return false;
        }
        $results = (pg_num_rows($res) === 0) ? [] : pg_fetch_all($res);
        return array_map(function($r) {
            return [
                'metaid' => $r['id'],
                'negyzet' => $r['grouping_code'],
                'pontok_szama' => $r['no_of_points'],
                'felelos' => new User($r['responsible']),
                'felmerok' => array_map(function($fm) {
                    return (is_numeric($fm)) ? new User($fm) : "";
                }, json_decode($r['observer'])),
                'finished' => $r['finished']
            ];
        }, $results);
    }
    
    public static function felmero_select($felmerok, $multi = false) {
        ob_start();
        ?>
        <select class="felmero_select<?= ($multi) ? '_multi' : '' ?>" name="<?= ($multi) ? "felmerok[]" : "felelos"; ?>" <?= ($multi) ? "multiple" : ""; ?>>
            <?php if (!$multi): ?>
                <option value=""></option>
            <?php endif; ?>
            <?php foreach (self::$felhasznalok as $fh): ?>
                <option value="<?= $fh->user_id ?>" <?= (in_array($fh->user_id, $felmerok)) ? "selected" : "" ?> > <?= $fh->name ?></option>
            <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
    
    public static function felmerok($arr) {
        $ids =  array_map(function($a) { 
            return ($a instanceof User) ? $a->user_id : (string)$a; 
        }, $arr);
        return self::felmero_select($ids, true);
    }
    
    public static function felelos($felelos) {
        $id = ($felelos instanceof User) ? $felelos->user_id :  (string)$felelos ; 
        return self::felmero_select([$id]);
    }
    
    public static function save_square_info($request) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        
        $period = (int)$request['period'];
        $felelos = (is_numeric($request['felelos'])) ? (int)$request['felelos'] : null;
        $felmerok = (isset($request['felmerok'])) ? array_values(array_filter($request['felmerok'], function($f) { return (is_numeric($f)); } )) : [];
        $felmerok = implode(', ', $felmerok);
        $metaid = $request['metaid'] ?? null;
        
        $cmd = ($metaid) 
            ? sprintf(getSQL('save_square_info_update', $sql_file), $felelos, (int)$metaid, $felmerok)
            : sprintf(getSQL('save_square_info_insert', $sql_file), $period, quote($request['sq']), $felelos, $felmerok);
        
        if (!$res = pg_query( $ID, $cmd ) ) {
            log_action('save error');
            return common_message('error', 'save error');
        }
        return common_message('ok','ok');
    }
    
    public static function add_new_square($request) {
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        debug($request);
    }
    
    public static function unoccupiedGcs_selector($period_id) {
        global $ID;
        $sql_file = getenv('PROJECT_DIR') . 'local/includes/private/monitoring_programs/program_3354.sql';
        
        ob_start();
        $cmd = sprintf(getSQL(__FUNCTION__, $sql_file), $period_id);
        $res = pg_query($ID, $cmd);
        $unoccupied = (pg_num_rows($res)) ? array_column(pg_fetch_all($res), 'grouping_code') : [];
        ?>
        <select class="sq" name="sq">
            <option selected disabled><?= str_please_choose ?></option>
            <?php foreach ($unoccupied as $sq): ?>
                <option> <?= $sq ?></option>
            <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
    
    public static function get_no_of_points($request) {
        global $ID;
        self::set_periods($request['period_id'] ?? null);
        if (!isset($request['gc'])) {
            return common_message('fail', 'invalid request');
        }
        $cmd = sprintf("SELECT count(*) as c 
                FROM elc_sampling_units su
                LEFT JOIN elc.raptor_period p ON su.project = p.project AND su.program = p.program
                WHERE p.id = %d AND su.grouping_code = %s;
        ", self::$period_id, quote($request['gc']));
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error', 'query_error');
        }
        $results = pg_fetch_assoc($res);
        return common_message('ok', $results['c']);
        
    }
}

?>
