<?php 
/**
* 
*/
require_once(getenv('OB_LIB_DIR'). '/private/page.php');
class BirdetectPage extends Page
{
    function __construct() {
        $session = $_GET['session'] ?? null;
        $record = $_GET['record'] ?? null;
        $this->content = $this->prepare_content($session, $record);
    }
    
    static function ajax($request) {
        $action = $request['action'];
        if (method_exists(__CLASS__, $request['action'])) {
            return self::$action($request);
        }
    }
    
    private function get_stylesheet() {
        $css = file_get_contents(getenv('OB_LIB_DIR').'/private/birdetect.css');
        return $css;
    }
    
    private function get_scripts() {
        $css = file_get_contents(getenv('OB_LIB_DIR').'/private/birdetect.js');
        return $css;
    }
    
    private function prepare_content($session, $record) {
        $wav = $this->select_wav_file($session, $record);
        if (!isset($_SESSION['Tid'])) {
          return 'Please log in';
        }
        $admin_roles = [1409,567,559];
        ob_start();
        ?>
        <link rel="stylesheet" href="http://milvus.openbiomaps.org/projects/elc/vendor/leaflet/leaflet.css">
        <style media="screen">
        <?= $this->get_stylesheet(); ?>
        </style>
        <script type="text/javascript">
        const wav = <?= json_encode($wav); ?>;
        </script>
        <?php if (isset($wav['filename'])): ?>
            <header>
                <h3 id="record_info" data-record_id="<?= $wav['record_id'] ?>"><?= $wav['device'] ?> - <?= $wav['filename'] ?> - <?= $wav['recorded_at'] ?></h3>
            </header>
            <main id="birdetect" class="main">
                
                <div class="controls">
                    <!--    <button type="button" id="bd-save" class="pure-button button-warning" title="Save regions"> <i class="fa fa-floppy-o"></i> </button> -->
                    <button type="button" id="bd-load_reduced" title="Load noise reduced version" class="pure-button pure-button-primary">
                        <i class="fa fa-microphone-slash"></i>
                    </button>
                    <div class="slidercontainer">
                        <button type="button" id="bd-zoom-out" class="pure-button button-secondary"><i class="fa fa-search-minus"></i></button>
                        <input type="range" min="1" max="100" value="50" id="bd-zoom">
                        <button type="button" id="bd-zoom-in" class="pure-button button-secondary"><i class="fa fa-search-plus"></i></button>
                    </div>
                    <button type="button" id="finish_and_load" title="Finish & Load another recording" class="pure-button button-warning">
                        <i class="fa fa-floppy-o"></i> <i class="fa fa-folder-open-o"></i> 
                    </button>
                    <button type="button" id="bd-open-browser" class="pure-button button-secondary" title="Open file browser"> <i class="fa fa-folder-open-o"></i> </button>
                </div>
                <div id="timeline"></div>
                <div id="waveform"></div>
                <div id="spectrogram"></div>
                <form id="bd-form" class="pure-form" method="post">
                    <fieldset>
                        <input type="hidden" id="bd-event_id" name="event_id">
                        <input type="hidden" id="bd-start" name="start">
                        <input type="hidden" id="bd-end" name="end">
                        <button type="button" class="pure-button pure-button-primary mr" id="bd-play" title="Play track"><i class="fa fa-play-circle-o"></i></button>
                        <button type="button" class="pure-button pure-button-primary" id="previous-region" title="Previous region"><i class="fa fa-step-backward"></i></button>
                        <button type="button" class="pure-button pure-button-primary" id="play-region" title="Play region"><i class="fa fa-play"></i></button>
                        <input type="text" id="bd-tag" name="tag">
                        <button type="submit" class="pure-button pure-button-primary" name="bd-save-event" title="Save region"><i class="fa fa-floppy-o"></i></button>
                        <button type="button" class="pure-button pure-button-primary mr" id="next-region" title="Next region"><i class="fa fa-step-forward"></i></button>
                        <button type="button" class="pure-button button-error" id="delete-region" title="Delete region"><i class="fa fa-trash-o"></i></button>
                        <button type="button" class="pure-button pure-button-primary" id="add-region" title="Add region"><i class="fa fa-plus-square-o"></i></button>
                    </fieldset>
                </form>
                <div id="waveform2"></div>
                <div id="spectrogram2"></div>
                <div class="notification"> </div>
                <div id="bd-browser" class="pure-g">
                    <div class="pure-u-1-6 sessions"> </div>
                    <div class="pure-u-2-3 records">
                        <?php if (in_array($_SESSION['Trole_id'], $admin_roles)) : ?>
                            <div class="records-filter">
                                <input type="text" placeholder="tag_category">
                            </div>
                        <?php endif; ?>
                        <div class="records-list pure-g"> </div>
                    </div>
                    <div class="pure-u-1-6 info"> </div>
                </div>
            </main>
            <footer class="footer"></footer>
        <?php elseif ($wav === 'no more records found'): ?>
            <header>
                <h3>No more records to listen to</h3>
            </header>
        <?php endif; ?>
        <?php if (in_array($_SESSION['Trole_id'], $admin_roles)) : ?>
            <div class="admin">
                <button type="button" id="bd-reload" class="pure-button button-secondary" title="Reload file"> <i class="fa fa-refresh"></i> </button>
                <button type="button" class="pure-button button-error do" id="import_records"> <i class="fa fa-exclamation-triangle"></i> Import records </button>
                <button type="button" class="pure-button button-error do" id="import_detection_file"> <i class="fa fa-exclamation-triangle"></i> import detection file</button>
            </div>
        <?php endif; ?>
        <script type="text/javascript">
        <?= $this->get_scripts(); ?>
        </script>
        <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/elc/vendor/leaflet/leaflet.js"> </script>
        <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/elc/vendor/wavesurfer/wavesurfer.js"> </script>
        <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/elc/vendor/wavesurfer/wavesurfer.timeline.js"></script>
        <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/elc/vendor/wavesurfer/wavesurfer.regions.js"></script>
        <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/elc/vendor/wavesurfer/wavesurfer.minimap.js"></script>
        <script type="text/javascript" src="http://milvus.openbiomaps.org/projects/elc/vendor/wavesurfer/wavesurfer.spectrogram.js"></script>
        <?php 
        $out = ob_get_clean();
        return $out;
    }
    
    private function select_wav_file($session, $record) {
        global $ID;
        
        if (!is_null($session) && !is_null($record)) {
            $cmd = sprintf("SELECT r.obm_id as record_id, \"path\", name as device, filename, recorded_at FROM elc_am_record r INNER JOIN elc_am_session s ON r.session_id = s.obm_id WHERE r.filename = %s AND s.name = %s LIMIT 1;", quote($record), quote($session));
            if (! $res = pg_query($ID, $cmd)) {
                return 'query error';
            }
            return pg_fetch_assoc($res);
        }
        else {
            $cmd = sprintf("SELECT r.obm_id as record_id, \"path\", name as device, filename, recorded_at FROM elc_am_record r INNER JOIN elc_am_session s ON r.session_id = s.obm_id WHERE detection_file IS NOT NULL AND listener = %d AND listening_finished = false LIMIT 1;", $_SESSION['Trole_id']);
        }
        // select if there is unfinished listening
        if (! $res = pg_query($ID, $cmd)) {
            return 'query error';
        }
        if (pg_num_rows($res) != 0) {
            return pg_fetch_assoc($res);
        }
        $cmd = sprintf("WITH selected AS (SELECT obm_id FROM elc_am_record WHERE detection_file IS NOT NULL AND listener IS NULL AND listening_finished = false LIMIT 1) UPDATE elc_am_record r SET listener = %d FROM selected, elc_am_session s WHERE r.obm_id = selected.obm_id AND r.session_id = s.obm_id RETURNING selected.obm_id as record_id, s.path, s.name as device, r.filename, r.recorded_at;", $_SESSION['Trole_id']);
        if (! $res = pg_query($ID, $cmd)) {
            return 'query error';
        }
        if (pg_num_rows($res) != 0) {
            return pg_fetch_assoc($res);
        }
        return 'no more records found';
        
    }
    
    private static function finish_and_load($request) {
        global $ID;
        $cmd = sprintf("UPDATE elc_am_record SET listening_finished = true WHERE listener = %d AND listening_finished = false;", $_SESSION['Trole_id']);
        if (! $res = pg_query($ID, $cmd )) {
            return common_message('error','query error');
        }
        return common_message('ok','ok');
    }
    
    private static function import_records() {
        global $ID;
        $cmd = 'SELECT "path" || name as dir, obm_id, name FROM elc_am_session WHERE "path" IS NOT NULL;';
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error','import_detection_file query error');
        }
        if (pg_num_rows($res) == 0) {
            return common_message('fail', 'No session paths are defined');
        }
        $dirs = pg_fetch_all($res);
        foreach ($dirs as $d) {
            $dir = getenv('PROJECT_DIR') . $d['dir'];
            if (!file_exists($dir)) {
                return common_message('fail', "$dir folder is missing");
            }
            $cmd = "INSERT INTO elc_am_record (session_id, filename, recorded_at) VALUES ";
            $values = [];
            foreach (new DirectoryIterator($dir) as $fileInfo) {
                if ($fileInfo->isDot()) continue;    
                if (strtolower($fileInfo->getExtension()) !== 'wav') continue;
                $record_name = substr($fileInfo->getBasename(), 0, 8);
                $values[] = "({$d['obm_id']}, '". $fileInfo->getBasename() ."', to_timestamp(hex_to_int(substring('$record_name',1, 8))))";
            }
            $cmd .= implode(',', $values);
            $cmd .= ' ON CONFLICT DO NOTHING;';
            if (!$res = query($ID, $cmd)) {
                return common_message('error','import_detection_file query error');
            }
        }
        return common_message('ok', 'ok');
    }
        
    private static function import_detection_file() {
        global $ID;
        $cmd = 'SELECT "path" || name as dir, obm_id, name FROM elc_am_session WHERE "path" IS NOT NULL;';
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error','import_detection_file query error');
        }
        if (pg_num_rows($res) == 0) {
            return common_message('fail', 'No session paths are defined');
        }
        $dirs = pg_fetch_all($res);
        $warnings = [];
        foreach ($dirs as $d) {
            $dir = getenv('PROJECT_DIR') . $d['dir'];
            $wd = $dir . "/_work";
            if (!file_exists($dir)) {
                return common_message('fail', "$dir folder is missing");
            }
            if (!file_exists($wd)) {
                debug("$wd does not exist!", __FILE__,__LINE__);
                continue;
            }
            foreach (new DirectoryIterator($wd) as $fileInfo) {
                if ($fileInfo->isDot()) continue;    
                if (strtolower($fileInfo->getExtension()) !== 'json') continue;
                
                $record_name = substr($fileInfo->getBasename(), 0, 8);
                
                $cmd = sprintf("SELECT r.obm_id FROM elc_am_record r LEFT JOIN elc_am_event e ON r.obm_id = e.record_id WHERE e.record_id IS NULL AND session_id = %d AND filename = %s;", $d['obm_id'], quote($record_name . '.WAV') );
                
                if (!$res = pg_query($ID, $cmd)) {
                    return common_message('error','import_detection_file query error');
                }
                if (pg_num_rows($res) == 0) {
                    //$warnings[] = $fileInfo->getFilename(). ': no record entry or events already imported';
                    continue;
                }
                $record_id = pg_fetch_assoc($res)['obm_id'];
                
                $dps = json_decode(file_get_contents($fileInfo->getPathname()));
                
                /*
                if ( !isset($dps->device_name) || $dps->device_name !== $d['name'] ) {
                    return common_message('error', "{$d['name']}: Json error: device_name missing or does not match");
                } 
                
                if (!isset($dps->timestamp) || $dps->timestamp !== $record_name) {
                    return common_message('error', 'Json error: record_name missing or does not match');
                }
                if (!isset($dps->detection_points) || count($dps->detection_points) === 0 ) {
                    return common_message('error', 'Json error: detection_points missing or empty');
                }
                */
                if (!count($dps)) {
                    continue;
                }
                $icmd = "INSERT INTO elc_am_event (record_id, start, duration) VALUES ";
                $icmd .= implode(',', array_map(function($dp) use ($record_id) {
                    return  "($record_id, $dp->start, 5)";
                }, $dps));
                $icmd .= ' ON CONFLICT DO NOTHING;';
                $cmd = [
                    $icmd, 
                    sprintf("UPDATE elc_am_record SET detection_file = %s WHERE session_id = %d AND filename = %s;", quote($record_name . '.json'), $d['obm_id'], quote($record_name . '.WAV')),
                ];
                if (!$res = query($ID, $cmd)) {
                    return common_message('error','import_detection_file query error');
                }
            }
        }
        if (count($warnings) > 0) {
            return common_message('warning', implode('</br>', $warnings));
        }
        return common_message('ok', 'ok');
    }
    
    private static function get_detection_points($request) {
        global $ID;
        $session = $request['device'];
        $record = $request['record'];
        
        $cmd = sprintf('SELECT start, start + duration as "end", obm_id as event_id, tag FROM elc_audiomoth WHERE name = %s AND filename = %s;', quote($session), quote($record));
        //debug($cmd,__FILE__,__LINE__);
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error','import_detection_file query error');
        }
        $data = [];
        while ($row = pg_fetch_assoc($res)) {
            $row['data'] = [
                'tag' => $row['tag'],
                'event_id' => $row['event_id'],
            ];
            unset($row['tag']);
            unset($row['obm_id']);
            $data[] = $row;
        }
        return common_message('ok',json_encode($data));
        
    }
    
    private static function get_sessions($request) {
        global $ID;
        
        if (!$res = pg_query($ID, "SELECT DISTINCT session_id, name FROM elc_audiomoth ORDER BY name")) {
            return common_message('error', 'get_sessions query error');
        }
        
        $sessions = pg_fetch_all($res);
        
        return common_message('ok', json_encode($sessions));
    }
    
    private static function get_records($request) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT 
                            filename, 
                            listening_finished, 
                            priority, 
                            recorded_at, 
                            count(*) as total, 
                            string_agg(DISTINCT(tag_category), ', ' ORDER BY tag_category ASC) as tag_categories,
                            sum(CASE WHEN tag IS NULL THEN 0 ELSE 1 END) as tagged 
                        FROM elc_audiomoth 
                        WHERE session_id = %d 
                        GROUP BY filename, listening_finished, recorded_at, priority;", 
            (int)$request['session']
        );
        if (!$res = pg_query($ID, $cmd)) {
            return common_message('error', 'get_sessions query error');
        }
        
        $records = pg_fetch_all($res);
        
        return common_message('ok', json_encode($records));
    }
    
    private static function save_event($request) {
        global $ID;
        if (!isset($_SESSION['Trole_id'])) {
            return common_message('error', 'Please sign in!');
        }
        $cmd = '';
        if ( isset($request['event_id']) && is_numeric($request['event_id']) ) {
            $cmd = sprintf("UPDATE elc_am_event SET start = %s, duration = %s, tag = %s, user_rid = %d WHERE obm_id = %d RETURNING obm_id", quote($request['start']), quote($request['end'] - $request['start']), quote($request['tag']), $_SESSION['Trole_id'], $request['event_id'] );
        }
        elseif (isset($request['record_id']) && is_numeric($request['record_id'])) {
            $cmd = sprintf("INSERT INTO elc_am_event (record_id, start, duration, tag, user_rid) VALUES (%d, %s, %s, %s, %s) RETURNING obm_id;", (int)$request['record_id'], quote($request['start']), quote($request['end'] - $request['start']), quote($request['tag']), $_SESSION['Trole_id']);
        }
        if ($cmd) {
            if (!$res = pg_query($ID, $cmd)) {
                return common_message('error','save_event query error');
            }
            $result = pg_fetch_assoc($res);
            return common_message('ok',$result['obm_id']);
        }
        return common_message('fail','event_id missing or not numeric');
    }
    
    private static function delete_event($request) {
       global $ID;
       if (!isset($_SESSION['Trole_id'])) {
          return common_message('error', 'Please sign in!');
       }
       if ( isset($request['event_id']) && is_numeric($request['event_id']) ) {
          $cmd = sprintf('DELETE FROM elc_am_event WHERE obm_id = %d;', (int)$request['event_id']);
          if (!$res = pg_query($ID, $cmd)) {
            return common_message('error','save_event query error');
          }
          return common_message('ok','ok');
       }
       return common_message('fail','event_id missing or not numeric');
    }
    
    private function tag_options() {
        return [
            'macskabagoly',
            'uráli bagoly',
            'patak',
            'eső',
        ];
    }
}
?>