<?php 
/**
 * 
 */
require_once(getenv('PROJECT_DIR').'local/includes/private/page.php');
class DocumentationPage extends Page
{
    function __construct($program = '') {
        $this->content = $this->prepare_content();
    }
    
    
    private function prepare_content($selected) {
        global $protocol;
        ob_start();
        ?>

<ul>
<li><p style="margin-bottom: 0in; line-height: 100%">elc</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_am_event</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_am_record</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_am_session</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_am_tags</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_audiomoth</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_custom_filetype</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_grouping_codes</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_habitat</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_history</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_jobs</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_jobs_results</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_metadata</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_nest_data</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_nests</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_observation_list</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_pairs</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_photos</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_programs</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_projects</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_rules</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_sampling_units</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_search</p>
	<ul>
		<li><p style="margin-bottom: 0in; line-height: 100%">obm_id		integer
		Auto Increment	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">obm_geometry	geometry
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">obm_uploading_id	integer
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">obm_validation	numeric
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">obm_comments	text[]
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">obm_modifier_id	integer
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">data_table		character
		varying NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">subject			character
		varying NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">meta			character
		varying NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">word			character
		varying NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">status			character
		varying NULL [unknown]	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">frequency		integer
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">taxon_db		integer
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">parent_id		integer
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">altitude		double
		precision NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">habitat			character
		varying(128) NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">corine_code		character
		varying(3) NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">search_id		integer
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">ord			integer
		NULL	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">active			boolean
		NULL [true]	</p>
		<ul>
			<li><p style="margin-bottom: 0in; line-height: 100%">when subject
			= project → true = ongoing project, false = completed project</p>
			<li><p style="margin-bottom: 0in; line-height: 100%">when subject
			= sampling_unit → true = valid sampling unit, false = archived
			sampling unit</p>
		</ul>
		<li><p style="margin-bottom: 0in; line-height: 100%">points_on_field	boolean
		NULL [false]	</p>
		<li><p style="margin-bottom: 0in; line-height: 100%">comments		text
		NULL</p>
	</ul>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_search_connect</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_tagged</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_taxon</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_terms</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_threats</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_threats_mv</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_tracklogs</p>
	<li><p style="margin-bottom: 0in; line-height: 100%">elc_users</p>
</ul>
        <?php 
        $out = ob_get_clean();
        return $out;
    }
    
}

?>

