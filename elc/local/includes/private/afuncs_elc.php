<?php
session_start();
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'languages.php');
if (isset($_GET['serie_inel'])) {
    global $ID;
    $si = preg_replace('/[^a-zA-Z]+/','',$_GET['serie_inel']);

    $cmd = sprintf('WITH last as (SELECT max(obm_id) as maxid FROM elc WHERE serie_inel = %s AND inel IS NOT NULL) SELECT inel FROM elc, last WHERE elc.obm_id = last.maxid;', quote($si));

    $res = pg_query($ID, $cmd);
    $result = pg_fetch_assoc($res);

    echo $result['inel'];
    exit;
}

if (isset($_REQUEST['page'])) {
    if (!in_array($_REQUEST['page'], ['monitoring', 'birdetect'] ) ) {
        echo common_message('error', 'invalid request');
        exit;
    }
    
    $p = "{$_REQUEST['page']}Page";
    require_once(getenv('PROJECT_DIR').'local/includes/private/page.php');
    require_once(getenv('PROJECT_DIR')."local/includes/private/{$_REQUEST['page']}.php");
    
    $resp = $p::ajax($_REQUEST);
    echo $resp;
    exit;
}

if (isset($_GET['tags'])) {
    global $ID;
    header('Content-Type: application/json');  // <-- header declaration
    if (!$res = pg_query($ID, "SELECT devices FROM elc_tagged LIMIT 1;")) {
        echo common_message('error', 'query error');
        exit();
    }
    $devices = pg_fetch_row($res);
    echo $devices[0];
    exit();
}

?>
