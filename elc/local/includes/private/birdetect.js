var wavesurfer
var wavesurfer2
const ajx = 'ajax_elc'
let selected = {}
const taggedColor = 'rgba(4, 167, 119, 0.2)'
const untaggedColor = 'rgba(247, 92, 3, 0.2)'

document.addEventListener('DOMContentLoaded', async function () {
  var GLOBAL_ACTIONS = {
    play: function () {
      wavesurfer.playPause()
      $('#bd-play i').toggleClass('fa-play-circle-o').toggleClass('fa-pause-circle-o')
    },
    back: function () {
      wavesurfer.skipBackward()
    },
    forth: function () {
      wavesurfer.skipForward()
    },
    play2: function () {
      wavesurfer2.playPause()
    },
    back2: function () {
      wavesurfer2.skipBackward()
    },
    forth2: function () {
      wavesurfer2.skipForward()
    },
    deleteRegion () {
      if (selected.region) {
        deleteRegion()
      }
    },
    'toggle-mute': function () {
      wavesurfer.toggleMute()
    }
  }

  document.addEventListener('keydown', function (e) {
    var map = {
      87: 'play',
      81: 'back',
      69: 'forth',
      83: 'play2',
      65: 'back2',
      68: 'forth2',
      46: 'deleteRegion'
    }
    var action = map[e.keyCode]
    const targetTag = e.target.tagName.toUpperCase()
    if (action in GLOBAL_ACTIONS && targetTag !== 'INPUT' && targetTag !== 'SELECT') {
      if (document === e.target || document.body === e.target) {
        e.preventDefault()
      }
      GLOBAL_ACTIONS[action](e)
    }
  })

  if ($('#waveform').length) {
    const colorMap = await $.getJSON('/projects/elc/includes/private/hot.json')
    await loadWave(wav, colorMap)
    $('#bd-play').on('click', function (ev) {
      GLOBAL_ACTIONS.play()
    })

    $('#bd-browser').dialog({
      modal: true,
      autoOpen: false,
      maxHeight: 600,
      minWidth: 1100,
      buttons: {
        Ok () {
          $(this).dialog('close')
      }
      },
      minWidth: 1200
    })
    $('#bd-browser').on('dialogclose', () => $(this).find('.records').html(''))

    $('#bd-reload').on('click', function () {
      localStorage.removeItem('regions')
      wavesurfer.destroy()
      loadWave(wav, colorMap)
    })
    $('#bd-load_reduced').on('click', function () {
      localStorage.removeItem('regions')
      wavesurfer.destroy()
      loadWave(wav, colorMap, true)
    })

    $('#play-region').on('click', playSelectedRegion)
    $('#previous-region').on('click', previousRegion)
    $('#next-region').on('click', nextRegion)
    $('#delete-region').click(deleteRegion)
    $('#add-region').click(addRegion)

    $('#bd-form').on('submit', async function (ev) {
      ev.preventDefault()
      try {
        const data = new FormData(this)
        // new region
        if (!data.get('event_id')) {
          data.append('record_id', $('#record_info').data('record_id'))
          wavesurfer.disableDragSelection()
        }
        data.append('page', 'birdetect')
        data.append('action', 'save_event')
        if (data.get('tag') === '') {
          throw 'Please tag the sound before saving!'
        }
        let resp = await $.ajax({
          url: ajx,
          data: data,
          processData: false,
          contentType: false,
          type: 'POST'
        })
        resp = JSON.parse(resp)
        if (resp.status !== 'success') {
          throw resp.message
        }
        selected.region.update({
          color: taggedColor,
          data: {
            event_id: resp.data,
            tag: data.get('tag')
          }
        });
        saveRegions(wav.device + '/' + wav.filename)
        nextRegion()
      } catch (e) {
        console.log(e)
        $('#dialog').html(e)
        $('#dialog').dialog('open')
      }
    })

    $('#bd-tag').on('change', function () {
      selected.unsaved = true
    })

    $('#finish_and_load').on('click', async function (ev) {
      ev.preventDefault()
      try {
        if (selected.unsaved) {
          throw 'Please save your work first!'
        }
        const untagged = Object.keys(wavesurfer.regions.list).filter(function (id) {
          var region = wavesurfer.regions.list[id]
          return (!region.data.tag || region.data.tag === '')
        })
        if (!window.confirm('Are you sure you want to skip the unfinished recording?')) {
          return
        }

        const action = $(this).prop('id')
        const resp = await $.getJSON(ajx, {
          page: 'birdetect',
          action: action
        });
        if (resp.status !== 'success') {
          throw resp.message
        }
        location.reload()
        return false
      } catch (e) {
        $('#dialog').html(e)
        $('#dialog').dialog('open')
      }
    })

    $('#bd-open-browser').click(async function () {
      try {
        const resp = await $.getJSON(ajx, {
          page: 'birdetect',
          action: 'get_sessions'
        })
        if (resp.status !== 'success') {
          throw resp.message
        }
        let sessions = JSON.parse(resp.data)
        sessions = sessions.map((s) => {
          return '<li class="show-records" data-sid="' + s.session_id + '"><i class="fa fa-folder"></i>  ' + s.name + '</li>'
        })
        $('#bd-browser .sessions').html('<ul>' + sessions.join('') + '</ul>')
        $('#bd-browser').dialog('open')
      } catch (e) {
        console.log(e)
        const d = $('#dialog')
        d.html(e)
        d.dialog('open')
      }
    })

    $('#bd-browser .sessions').on('click', '.show-records', async function () {
      try {
        $('.active').removeClass('active')
        $('.records-filter input').val('')
        const sid = $(this).data('sid')
        const resp = await $.getJSON(ajx, {
          page: 'birdetect',
          action: 'get_records',
          session: sid
        })
        if (resp.status !== 'success') {
          throw resp.message
        }
        const records = JSON.parse(resp.data)
        showRecordsList(records)
        $(this).addClass('active')
        $('.records-filter').show()
        $('.records-filter input').on('input', function() {
            const records_filtered = (this.value !== '') 
                ? records.filter(r => r.tag_categories && r.tag_categories.toLowerCase().includes(this.value.toLowerCase()))
                : records;
            showRecordsList(records_filtered)
        })
      } catch (e) {
        console.log(e)
      }
    })

    $('#bd-browser .records-list').on('click', 'li', async function () {
      const session = $('.show-records.active').text()
      const record = $(this).find('.filename').text()
      window.location.href = 'http://milvus.openbiomaps.org/projects/elc/birdetect?session=' + session + '&record=' + record
    })
  }

  $('.do').on('click', async function (ev) {
    ev.preventDefault()
    try {
      const action = $(this).prop('id')
      const resp = await $.getJSON(ajx, {
        page: 'birdetect',
        action: action
      })
      if (resp.status !== 'success') {
        throw resp.message
      }
    } catch (e) {
      $('#dialog').html(e)
      $('#dialog').dialog('open')
    }
  })

  function showRecordsList(records) {
        const uls = {
          0: [], 1: [], 2: [], 3: []
        }
        for (var i = 0; i < records.length; i++) {
          const done = records[i].tagged / records[i].total
          const cls = (done === 0) 
            ? (Number(records[i].priority) === 1) 
              ? 'priority'
              : 'none' 
            : (done === 1) 
              ? 'done' 
              : 'partial'
          const lock = records[i].listening_finished === 't' ? ' <i class="fa fa-lock"></i> ' : ''
          uls[i % 4].push('<li class="' + cls + '">' +
            '<span class="filename">' + records[i].filename + '</span>' +
            '</li>')
        }
        $('#bd-browser .records-list').html('')
        Object.keys(uls).forEach(function (k) {
          $('#bd-browser .records-list').append('<ul class="pure-u-1-4">' + uls[k].join('') + '</ul>')
        })
        $('.filename').hover(
          function () {
              const rec = records.filter((r) => r.filename === $(this).html())[0]
              const inf = "<div class='infocontent'>" + 
                Object.keys(rec).map(k => "<strong>" + k + "</strong>: " + rec[k] ).join('</br>') +
                "</div>"
              $("#bd-browser>.info").html(inf)
          },
        )
  }
  
  function nextRegion () {
    const next = getNextRegionId()
    if (next) {
      if (selectRegion(wavesurfer.regions.list[next])) {
        playSelectedRegion()
      }
    }
  }

  function previousRegion () {
    const prev = getPreviousRegionId()
    if (prev) {
      if (selectRegion(wavesurfer.regions.list[prev])) {
        playSelectedRegion()
      }
    }
  }

  function playSelectedRegion () {
    selected.region.play()
    $('#bd-form input[name="tag"]').focus()
  }

  async function deleteRegion () {
    if (window.confirm('Are you sure?')) {
      try {
        let resp = await $.post(ajx, {
          page: 'birdetect',
          action: 'delete_event',
          event_id: selected.region.data.event_id
        })
        resp = JSON.parse(resp)
        if (resp.status !== 'success') {
          throw resp.message
        }
        selected.region.remove()
        saveRegions(wav.device + '/' + wav.filename)
      } catch (e) {
        console.log(e)
        $('#dialog').html(e)
        $('#dialog').dialog('open')
      }
    }
  }

  function addRegion () {
    wavesurfer.enableDragSelection({
      drag: true,
      resize: true,
      color: '#bac345'
    })
  }

  function loadWave (wav, colorMap) {
    wavesurfer = WaveSurfer.create({
      container: '#waveform',
      waveColor: 'violet',
      height: 128,
      progressColor: 'purple',
      minPxPerSec: 5,
      backend: 'WebAudio',
      plugins: [
        WaveSurfer.regions.create(),
        WaveSurfer.timeline.create({
          container: '#timeline'
        }),
        /*WaveSurfer.spectrogram.create({
          container: '#spectrogram',
          labels: true,
          colorMap: colorMap
        })*/
      ]
    })
    wavesurfer2 = WaveSurfer.create({
      container: '#waveform2',
      waveColor: 'violet',
      height: 128,
      progressColor: 'purple',
      minPxPerSec: 5,
      backend: 'WebAudio',
      plugins: [
        /*WaveSurfer.spectrogram.create({
          container: '#spectrogram2',
          labels: true,
          colorMap: colorMap
        })*/
      ]
    })

    wavesurfer.regions.updateOrder = function () {
      const rgs = Object.keys(wavesurfer.regions.list).map(function (id) {
        var region = wavesurfer.regions.list[id]
        return [id, region.start]
      })
      rgs.sort(function (a, b) {
        return a[1] - b[1]
      })

      wavesurfer.regions.order = rgs.map((r) => r[0])
    }
    const wavfile = wav.path + wav.device + '/' + wav.filename
    const reduced = wav.path + wav.device + '/_work/' + wav.filename + '_reduced.wav'
    const alien_sonogram = wav.path + wav.device + '/_work/images/' + wav.filename + '_reduced.wav.jpg'
    wavesurfer.load(wavfile)
    wavesurfer2.load(reduced)

    $("#spectrogram2").html('<img src="'+alien_sonogram+'" />');
    wavesurfer.on('ready', function () {
      try {
        if (localStorage.regions && localStorage.wav && localStorage.wav === wav.device + '/' + wav.filename) {
          loadRegions(JSON.parse(localStorage.regions))
        } else {
          const request = wavesurfer.util.fetchFile({
            responseType: 'json',
            url: ajx + '?page=birdetect&action=get_detection_points&device=' + wav.device + '&record=' + wav.filename
          })
          request.on('success', function (data) {
            if (data.status !== 'success') {
              throw data.message
            }
            loadRegions(JSON.parse(data.data))
            saveRegions(wav.device + '/' + wav.filename)
          })
          request.on('error', (e) => {
            throw 'Detection file can not be found.';
            console.log(e)
          })
        }
      } catch (e) {
        $('#dialog').html(e)
        $('#dialog').dialog('open')
      }
    })
    wavesurfer.on('region-click', (region, e) => {
      if (!selected.region || selected.region.data.event_id != region.data.event_id) {
        selectRegion(region)
      }
    })

    wavesurfer.on('region-dblclick', function (region, e) {
      e.stopPropagation()
      // Play on click, loop on shift click
      e.shiftKey ? region.playLoop() : region.play()
      $('#bd-form input[name="tag"]').focus()
    })
    wavesurfer.on('region-update-end', () => {
      updateFormTimes()
      selected.unsaved = true
    })

    var slider = document.querySelector('#bd-zoom')
    slider.value = wavesurfer.params.minPxPerSec
    slider.min = wavesurfer.params.minPxPerSec
    slider.max = 500

    document.querySelector('#bd-zoom-in').addEventListener('click', function () { zoom('in') })
    document.querySelector('#bd-zoom-out').addEventListener('click', function () { zoom('out') })


    const zoomEvt = new CustomEvent('bd-zoom-evt')
    const zoom = function (dir) {
      slider.value = (dir === 'out') ? Number(slider.value) - 20 : Number(slider.value) + 20
      slider.dispatchEvent(zoomEvt)
    }

    $(slider).on('input', function () {
      wavesurfer.zoom(Number(this.value))
    })
    $(slider).on('bd-zoom-evt', function () {
      wavesurfer.zoom(Number(this.value))
    })

    // set initial zoom to match slider value
    wavesurfer.zoom(slider.value)

    $('wave').on('wheel', function (ev) {
      ev.preventDefault()
      const dir = (ev.deltaY < 0) ? 'in' : 'out'
      zoom(dir)
    })
  }

  function updateForm () {
    if (!selected.region) {
      $('#bd-form input').val('');
      return;
    }
    $('#bd-event_id').val(selected.region.data.event_id);
    $('#bd-tag').val(selected.region.data.tag);

    updateFormTimes();
  }

  function updateFormTimes () {
    $('#bd-start').val(selected.region.start.toFixed(4));
    $('#bd-end').val(selected.region.end.toFixed(4));
  }

  function selectRegion (region) {
    if (selected.region && !deselectRegions()) {
      return false;
    }
    region.element.setAttribute('data-region-highlight', true);
    selected['region'] = region;
    selected['undo'] = {
      start: region.start,
      end: region.end            
    }
    selected.region.update( { drag: true, resize: true });
    $('#bd-form').css('display', 'flex');
    updateForm();

    return true;
  }

  function getSelectedRegionId () {
    const sel_id = Object.keys(wavesurfer.regions.list).find((id) => {
      r = wavesurfer.regions.list[id]
      return r.element.dataset.hasOwnProperty('regionHighlight') 
    })
    return sel_id
  }

  function getPreviousRegionId () {
    const previousIdx = wavesurfer.regions.order.indexOf(getSelectedRegionId()) - 1;
    if (previousIdx == -1) {
      return false;
    }
    return wavesurfer.regions.order[previousIdx];
  }

  function getNextRegionId () {
    const nextIdx = wavesurfer.regions.order.indexOf(getSelectedRegionId()) + 1;
    if (nextIdx == -1) {
      return false;
    }
    return wavesurfer.regions.order[nextIdx];
  }

  function deselectRegions () {
    const undo = (selected.unsaved) ? confirm('There are unsaved changes! Do you want to proceed?') : false;
    if (!selected.unsaved || undo) {
      selected.region.update( { drag: false, resize: false });
      if (undo) {
        selected.region.update( { start: selected.undo.start, end: selected.undo.end });
      }
      selected = {};
      $('.wavesurfer-region[data-region-highlight="true"]').removeAttr('data-region-highlight');
      return true
    }
    return false
  }

  /**
  * Save annotations to localStorage.
  */
  function saveRegions (w) {
    const rgs = Object.keys(wavesurfer.regions.list).map(function(id) {
      var region = wavesurfer.regions.list[id];
      return {
        start: region.start,
        end: region.end,
        attributes: region.attributes,
        data: region.data
      };
    })
    localStorage.regions = JSON.stringify(rgs);
    localStorage.wav = w;
    
    if (selected.unsaved) {
      selected.unsaved = false;
    }
  }

  /**
  * Disabling the drag and resize properties of all regions
  */
  function disableRegionDragResize () {
    Object.keys(wavesurfer.regions.list).forEach(function(id) {
      wavesurfer.regions.list[id].update({ drag: false, resize: false });
    })
  }

  /**
  * Load regions from localStorage.
  */
  function loadRegions (regions) {
    regions.forEach(function(region) {
      if (region.start <= wavesurfer.getDuration()) {
        region.color = region.data.tag ? taggedColor : untaggedColor;
        wavesurfer.addRegion(region);
      }
    });
    wavesurfer.regions.updateOrder();
    disableRegionDragResize();
  }
})

