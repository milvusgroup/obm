$(document).ready(function() {
    //declaration of t for translations
    var t = []
    $('#monitoring_program_selector').on('change', async function(ev) {
        try {
            const page_selector = $('#monitoring_page_selector');
            page_selector.html('<option selected disabled> Please choose </option>');
            $('main').html('');
            
            const prog_id = $(this).find(':selected').val();
            let paramters = {
                page: 'monitoring',
                program: prog_id,
                action: 'main_page'
            }
            let resp = await $.get(ajx, paramters);
            $('main').html(resp);
            $('main').trigger(`main_page_${prog_id}_page_loaded`);
            
            // requesting the menu items
            paramters['action'] = 'menu_items'
            resp = await $.getJSON(ajx, paramters);
            resp.forEach(function(el) {
                page_selector.append(dashboard_menu_item(el));
            })
            
            
        } catch (e) {
            console.log(e);
        } 
    })
    $('#monitoring_page_selector').on('change', async function(ev) {
        try {
            
            const prog_id = $('#monitoring_program_selector').find(':selected').val();
            const action = $(this).find('option:selected').html().toLowerCase().split(' ').join('_');
            const paramters = {
                page: 'monitoring',
                program: prog_id,
                action: action
            };
            const resp = await $.get(ajx, paramters);
            $('main').html(resp);
            $('main').trigger(`${action}_${prog_id}_page_loaded`);
            
        } catch (e) {
            console.log(e);
        } 
    })

    const raptor_id = 3354;
    const ajx = obj.url + 'ajax_elc';
    
    var period = () => $('.period_select').find('option:selected').val()    
    
    $('main').on('main_page_3354_page_loaded', async function() {
        var t = await $.getJSON(ajx, {
            page: 'monitoring',
            program: raptor_id,
            action: 'get_translations'
        });
        
        $('.finish-square').on('submit', async function(event) {
            event.preventDefault();
            if (confirm(t.str_finish_raptor_sqaure_confirm)) {
                try {
                    const fd = new FormData(this);
                    fd.append('page', 'monitoring');
                    fd.append('program', raptor_id);
                    fd.append('period_id', period());
                    fd.append('action', 'finish_raptor_sqaure');
                    let resp = await $.ajax({
                        url : ajx,
                        type : "POST",
                        data : fd,
                        processData: false,
                        contentType: false,
                        cache: false,
                    });
                    resp = JSON.parse(resp);
                    if (resp.status == 'success') {
                        $('.finish-square').hide();
                        throw t.str_finish_raptor_sqaure_success
                    }
                    else {
                        throw resp.message;
                    }
                } catch (e) {
                    $('#dialog').html(e);
                    $('#dialog').dialog('open');
                    
                } 
            }
        })
        $('.period_select').on('change', async function(ev) {
            try {
                const paramters = {
                    page: 'monitoring',
                    program: raptor_id,
                    action: 'main_page',
                    period_id: period()
                };
                const resp = await $.get(ajx, paramters);
                $('main').html(resp);
                $('main').trigger(`main_page_${raptor_id}_page_loaded`);
            } catch (e) {
                console.log(e);
            } 
        })
    });
    
    $('main').on('administration_3354_page_loaded', async function() {
        $('.felmero_select_multi').fSelect({
            placeholder:'&nbsp;',
            searchText:'Search for user name',
            numDisplayed: 5,
            showSearch:true
        });
        
        $('.felmero_select').on('change', mark_unsaved);
        
        $('.fs-checkbox i').on('click', mark_unsaved);
        
        function mark_unsaved(e) {
            const row = $(e.target).closest('tr');
            row.find('.save_square_info').removeClass('button-success').addClass('button-warning');
        }
        
        $('.fl-button').on('click', async function(ev) {
            try {
                
                const paramters = {
                    page: 'monitoring',
                    program: raptor_id,
                    action: 'administration',
                    first_letter: $(this).html(),
                    period_id: period()
                };
                const resp = await $.get(ajx, paramters);
                $('main').html(resp);
                $('main').trigger(`administration_${raptor_id}_page_loaded`);
            } catch (e) {
                console.log(e);
            } 
        })
        
        
        $('.period_select').on('change', async function(ev) {
            try {
                const paramters = {
                    page: 'monitoring',
                    program: raptor_id,
                    action: 'administration',
                    period_id: period()
                };
                const resp = await $.get(ajx, paramters);
                $('main').html(resp);
                $('main').trigger(`administration_${raptor_id}_page_loaded`);
            } catch (e) {
                console.log(e);
            } 
        })
        
        $('.sq').on('change', async function () {
            try {
                const paramters = {
                    page: 'monitoring',
                    program: raptor_id,
                    action: 'get_no_of_points',
                    gc: this.selectedOptions[0].value
                };
                const resp = await $.getJSON(ajx, paramters);
                if (resp.status !== 'success') {
                    throw new Error(resp.message)
                }
                $('#new_square_form .no_of_points').html(resp.data);
            } catch (e) {
                console.log(e);
            } 
        })
        
        $('.add_new_square').on('click', async function (ev) {
            try {
                const form = document.querySelector('#new_square_form')
                const fd = new FormData(form)
                fd.append('page', 'monitoring')
                fd.append('program', raptor_id)
                fd.append('action', 'save_square_info')
                fd.append('period', period())
                if (!fd.get('sq')) {
                    throw 'Please select a square!'
                }
                let resp = await $.ajax({
                    url: ajx,
                    data: fd,
                    processData: false,
                    contentType: false,
                    type: 'POST'
                })
                resp = JSON.parse(resp)
                if (resp.status !== 'success') {
                    throw resp.message
                }
                $('.period_select').trigger('change')
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            } 
        })
        
        $('.save_square_info').on('click', async function(ev) {
            try {
                const row = $(this).parent().parent();
                const sq = row.data('sq');
                const metaid = row.data('metaid');
                const felelos = row.find('.felmero_select option:selected').val();
                const felmerok = [];
                row.find('.felmero_select_multi option:selected').each( function (i) {
                    felmerok.push(this.value);
                });
                
                const paramters = {
                    page: 'monitoring',
                    program: raptor_id,
                    action: 'save_square_info',
                    period: period(),
                    q: sq,
                    metaid: metaid,
                    felelos: felelos,
                    felmerok: felmerok
                }
                const r = await $.post(ajx, paramters);
                const resp = JSON.parse(r);
                if (resp.status !== 'success') {
                    throw r.message;
                }
                row.find('.save_square_info').addClass('button-success').removeClass('button-warning');
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            } 
        })
        
        $(".reopen-square").click(async function() {
          try {
            if (!confirm('Biztos újranyitod a négyzetet?')) {
              return;
            }
            const row = $(this).parent().parent();
            const sq = row.data('sq');
            const paramters = {
              page: 'monitoring',
              program: raptor_id,
              action: 'reopen_square',
              period: period(),
              sq: sq
            }
            const r = await $.post(ajx, paramters);
            const resp = JSON.parse(r);
            if (resp.status !== 'success') {
                throw r.message;
            }
            row.find('.square-finished').remove();
            row.find('.reopen-square').remove();
          } catch (e) {
            console.log(e);
          } 
        })
    })

    var toolbox = L.control();
    toolbox.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'toolbox');
        L.DomEvent.disableClickPropagation(this._div);
        this.update();
        return this._div;
    };
    
    var info = L.control({position: 'bottomright'});
    
    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        L.DomEvent.disableClickPropagation(this._div);
        this.update();
        return this._div;
    };
    
    // method that we will use to update the control based on feature properties passed
    info.update = function (props) {
        const props_list = props ? Object.keys(props).reduce((str, k) => {
            k_transl = (k in t) ? t[k] : k;
            return str + k_transl + ": <b>" + props[k] + "</b><br>"
        }, "") : "";
        this._div.innerHTML = (props ? props_list : '');
    };
    
    const done_color = '#FBFBFF';
    const selected_color = "#F7B32B";
    
    
    var markerOptions = {
        mfp: { radius: 8, fillColor: "#E7F59E", color: "#000", weight: 1, opacity: 1, fillOpacity: 0.8, bubblingMouseEvents: false },
        pair_data: { radius: 8, fillColor: "#077187", color: "#000", weight: 1, opacity: 1, fillOpacity: 0.8, bubblingMouseEvents: false },
        base: { radius: 8, color: "#000", weight: 1, opacity: 1, fillOpacity: 1, bubblingMouseEvents: false },
        selected: { radius: 8, color: "#000", fillColor: selected_color, weight: 1, opacity: 1, fillOpacity: 1, bubblingMouseEvents: false },
        done: { radius: 8, color: done_color, weight: 2, opacity: 1, fillOpacity: 0, bubblingMouseEvents: false },
        done_selected: { radius: 8, color: selected_color, weight: 2, opacity: 1, fillOpacity: 0, bubblingMouseEvents: false },
    };

    $('main').on('dupla_tool_3354_page_loaded', async function() {
        var colors =  Array.apply(null, Array(70)).map(c => getRandomColor())
        var t = await $.getJSON(ajx, {
            page: 'monitoring',
            program: raptor_id,
            action: 'get_translations'
        });
        var pair_double_selector = await $.get(ajx, {
            page: 'monitoring',
            program: raptor_id,
            action: 'get_double_status_selector'
        });
        var selected_observations = [];
        var map = L.map('map').setView([46, 25], 7);
        L.esri.basemapLayer('Imagery').addTo(map);
        L.esri.basemapLayer('ImageryLabels').addTo(map);
        
        var o_points = L.featureGroup().addTo(map);
        
        info.addTo(map);
        
        toolbox.update = function (props) {
            const sol = selected_observations.length;
            if (sol <= 1) {
                this._div.innerHTML = "<div class='n_observations'>" + t.str_please_select_at_least_two_pairs + "</div>";
            }
            else {
                this._div.innerHTML = "<div class='n_observations'>" + sol + ' ' + t.str_n_observations_selected + "</div>" + pair_double_selector;
                
                var max_pairs = $('.toolbox input[name="max_pairs"]');
                const max = observations.getLayers().filter(o => selected_observations.indexOf(o.feature.properties.obm_id) > -1).reduce((sum, o) => sum + o.feature.properties.max, 0);
                max_pairs.prop('min', 2);
                max_pairs.prop('max', max);
            }
        };
        toolbox.undouble = function (double_id) {
            let html = t.str_reset + " <button class='pure-button button-error' id='undouble' data-double_id='" + double_id + "'> <i class='fa fa-unlink'> </i> </button>";
            this._div.innerHTML = html;
        }
        toolbox.addTo(map);
        
        $('.period_select').on('change', async function(ev) {
            try {
                const paramters = {
                    page: 'monitoring',
                    program: raptor_id,
                    action: 'dupla_tool',
                    period_id: period()
                };
                const resp = await $.get(ajx, paramters);
                $('main').html(resp);
                $('main').trigger(`dupla_tool_${raptor_id}_page_loaded`);
            } catch (e) {
                console.log(e);
            } 
        })
        
	var su_color = {}
        $('#grouping_code').on('change', async function() {
            try {
                o_points.clearLayers();
                points.clearLayers();
                const gyk = $(this).find('option:selected').val();
                let paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    period: period(),
                    action: 'get_sampling_unit_geom',
                    gc: gyk,
                }
                let resp = await $.getJSON(ajx, paramters);
                const r = JSON.parse(resp.data);
               	var cc = 0 
                o_points.addLayer(L.geoJSON(r, {
                    pointToLayer: function (feature, latlng) {
			cc++
                        const pn = Number(feature.properties.sampling_unit.substr(-2));
			su_color[feature.properties.sampling_unit] = cc
                        var redMarker = L.ExtraMarkers.icon({
                            icon: 'fa-number',
                            markerColor: colors[cc],
                            svg: true,
                            number: feature.properties.sampling_unit
                        });
                        return L.marker(latlng, {icon: redMarker});
                    },
                }));
                
                const fitBoundsOptions = {
                    maxZoom: 13
                };
                map.fitBounds(o_points.getBounds(), fitBoundsOptions);
                
                paramters.action = 'get_observed_species_list';
                paramters.period = period()
                resp = await $.getJSON(ajx, paramters);
                if (resp.status !== 'success') {
                    $('#species').html('');
                    throw resp.message;
                }
                $('#species').html(resp.data);
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            } 
        });
        
        var points = L.markerClusterGroup({
            spiderfyDistanceMultiplier: 4
        });
        var observations;
        
        $('#species').on('change', async function() {
            try {
                points.clearLayers();
                const gyk = $('#grouping_code').find('option:selected').val();
                const sp = $(this).find('option:selected').val();
                const paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: 'get_pairs',
                    period: period(),
                    gc: gyk,
                    species: sp
                };
                const resp = await $.getJSON(ajx, paramters);
                if (resp.status !== 'success') {
                    throw resp.message;
                }
                const r = JSON.parse(resp.data);
                if (r.features === null) {
                    throw t.str_no_certain_or_possible_pairs
                }
                observations = L.geoJSON(r, {
                    onEachFeature: onEachObservation,
                    pointToLayer: function (feature, latlng) {
                        let mo = markerOptions.base;
                        if (feature.properties.double_status !== null) {
                            mo = markerOptions.done;
                        }
                        else {
                            mo.fillColor = colors[su_color[feature.properties.sampling_unit]];
                            
                        }
                        return L.circleMarker(latlng, mo);
                    }
                });
                points.addLayer(observations);
                map.addLayer(points);
            }
            catch(e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        });
        
        $('.toolbox').on('change', '#double_status_selector' ,function() {
            if (this.value == 'str_certain') {
                $('#max_pairs').prop('value', 1);
            }
            $('#max_pairs').prop('disabled', (this.value == 'str_certain'));
        });
        
        $('.toolbox').on('click','#undouble', async function() {
            try {
                const double_id = $(this).data('double_id');
                if (!double_id) {
                    throw 'Double id missing';
                }
                
                const paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: "undouble",
                    double_id: double_id,
                }
                let resp = await $.post(ajx, paramters);
                resp = JSON.parse(resp);
                if (resp.status == 'success') {
                    resp.data = resp.data.map(x => Number(x));
                    observations.getLayers().forEach(o => {
                        const pair_id = o.feature.properties.obm_id;
                        o.feature.properties.double_id = null;
                        o.feature.properties.double_status = null;
                        if (resp.data.indexOf(pair_id) > -1) {
                            let mo = markerOptions.base;
                            mo.fillColor = colors[su_color[feature.properties.sampling_unit]];
                            o.setStyle(mo);
                            o.off('click');
                            o.on({ click: select_observation });
                        }
                    });
                    toolbox.update();
                }
                else {
                    throw resp.message;
                }
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        })
        
        $('.toolbox').on('click','#save_double', async function() {
            try {
                const stat = $('#double_status_selector option:selected').val();
                const max_pairs = $('#max_pairs');
                if (!stat) {
                    throw 'Please select status';
                }
                if (Number(max_pairs.val()) < 1 || max_pairs.val() > max_pairs.prop('max')) {
                    throw 'Please give the maximum number of possible pairs, but not more than ' + max_pairs.prop('max');
                }
                if (selected_observations.length < 2) {
                    throw 'Please select at least two pairs'
                }
                const si = $('#grouping_code').find('option:selected').data('si');
                
                const paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: "save_double_status",
                    si: si,
                    ids: selected_observations,
                    status: stat,
                    max: max_pairs.val()
                }
                let resp = await $.post(ajx, paramters);
                resp = JSON.parse(resp);
                if (resp.status === 'success') {
                    const connected_ids = selected_observations;
                    selected_observations = [];
                    toolbox.update();
                    observations.getLayers().forEach(o => {
                        const obm_id = o.feature.properties.obm_id;
                        if (connected_ids.indexOf(obm_id) > -1) {
                            o.feature.properties.double_id = resp.data;
                            o.feature.properties.double_status = stat;
                            o.setStyle(markerOptions.done);
                            o.off('click');
                            o.on({click: undouble_observation});
                        }
                    });
                }
                else {
                    throw resp.message;
                }
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        })

        function onEachObservation(feature, layer) {
            let actions = {
                mouseover: highlightFeature,
            };
            actions.click = (feature.properties.double_status === null) ? select_observation : undouble_observation;
            layer.on(actions);
        }
        
        function highlightFeature(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
        }
        
        function undouble_observation(e) {
            unselect_observations();
            const layer = e.target;
            info.update(layer.feature.properties);
            const double_id = layer.feature.properties.double_id;
            toolbox.undouble(double_id);
        }

        function select_observation(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
            var obm_id = e.target.feature.properties.obm_id;
            var index = selected_observations.indexOf(obm_id);
            if (index > -1) {
                selected_observations.splice(index, 1);
                let mo = markerOptions.base;
                mo.fillColor = colors[su_color[e.target.feature.properties.sampling_unit]];
                layer.setStyle(mo)
                
            }
            else {
                selected_observations.push(layer.feature.properties.obm_id);
                layer.setStyle(markerOptions.selected);
            }
            toolbox.update();
        }
        function unselect_observations() {
            selected_observations = [];
            observations.getLayers().forEach(o => {
                if (o.feature.properties.double_id == null) {
                    let mo = markerOptions.base;
                    mo.fillColor = colors[su_color[o.feature.properties.sampling_unit]];
                    o.setStyle(mo);
                }
            });
            toolbox.update();
            
        }
    });
    
    $('main').on('pair_tool_3354_page_loaded', async function() {
        var selected_observations = [];
        var pair_status_selector = await $.get(ajx, {
            page: 'monitoring',
            program: raptor_id,
            action: 'get_pair_status_selector'
        });
        t = await $.getJSON(ajx, {
            page: 'monitoring',
            program: raptor_id,
            action: 'get_translations'
        });
        
        var map = L.map('map').setView([46, 25], 7);
        L.esri.basemapLayer('Imagery').addTo(map);
        L.esri.basemapLayer('ImageryLabels').addTo(map);
            
        info.addTo(map);
        
        toolbox.update = function (props) {
            let html = "<div class='n_observations'>" + selected_observations.length + ' ' + t.str_n_observations_selected + "</div>";
            this._div.innerHTML = selected_observations.length ? html + pair_status_selector : html;
        };
        toolbox.unpair = function (pair_id) {
            let html = t.str_reset + " <button class='pure-button button-error' id='unpair' data-pair_id='" + pair_id + "'> <i class='fa fa-unlink'> </i> </button>";
            this._div.innerHTML = html;
        }
        toolbox.addTo(map);
        
        
        var o_points = L.featureGroup().addTo(map);
        
        $('.period_select').on('change', async function(ev) {
            try {
                const paramters = {
                    page: 'monitoring',
                    program: raptor_id,
                    action: 'pair_tool',
                    period_id: period()
                };
                const resp = await $.get(ajx, paramters);
                $('main').html(resp);
                $('main').trigger(`pair_tool_${raptor_id}_page_loaded`);
            } catch (e) {
                console.log(e);
            } 
        })
        
        $('#grouping_code').on('change', async function() {
            try {
                selected_observations = [];
                toolbox.update();
                const gyk = $(this).find('option:selected').val();
                const paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: 'get_sampling_units',
                    period_id: period(),
                    gc: gyk,
                }
                const resp = await $.get(ajx, paramters);
                $('#sampling_unit').html(resp);
                reset_centroid();
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            } 
        });
        
        
        $('#sampling_unit').on('change', async function() {
            try {
                selected_observations = [];
                toolbox.update();
                o_points.clearLayers();
                points.clearLayers();
                
                const gyk = $('#grouping_code').find('option:selected').val();
                const su = $(this).find('option:selected').val();
                let paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: 'get_observed_species_list',
                    period: period(),
                    gc: gyk,
                    su: su
                };
                let resp = await $.getJSON(ajx, paramters);
                if (resp.status !== 'success') {
                    throw resp.message;
                }
                $('#species').html(resp.data);
                
                paramters.action = 'get_sampling_unit_geom';
                resp = await $.getJSON(ajx, paramters);
                const r = JSON.parse(resp.data);
                
                o_points.addLayer(L.geoJSON(r, {
                    pointToLayer: function (feature, latlng) {
                        return L.circleMarker(latlng, markerOptions.mfp);
                    },
                }));
                
                const fitBoundsOptions = {
                    maxZoom: 13
                };
                map.fitBounds(o_points.getBounds(), fitBoundsOptions);
                reset_centroid();
            }
            catch(e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        });
        
        var points = L.markerClusterGroup({
            spiderfyDistanceMultiplier: 4
        }).addTo(map);
        var observations;

        $('#species').on('change', async function() {
            try {
                selected_observations = [];
                toolbox.update();
                points.clearLayers();
                const gyk = $('#grouping_code').find('option:selected').val();
                const su = $('#sampling_unit').find('option:selected').val();
                const sp = $(this).find('option:selected').val();
                const paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: 'get_observations',
                    period: period(),
                    gc: gyk,
                    su: su,
                    species: sp
                };
                const resp = await $.getJSON(ajx, paramters);
                if (resp.status !== 'success') {
                    throw resp.message;
                }
                const r = JSON.parse(resp.data);
                
                observations = L.geoJSON(r, {
                    onEachFeature: onEachObservation,
                    pointToLayer: function (feature, latlng) {
                        const mo = (feature.properties.pair_id !== null) ? markerOptions.done : markerOptions.pair_data;
                        return L.circleMarker(latlng, mo);
                    }
                });
                points.addLayer(observations);
                reset_centroid();
                count_observations();
                //points
            
            }
            catch(e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        });
        
        $('.toolbox').on('click','#save_pair', async function() {
            try {
                const stat = $('#pair_status_selector option:selected').val();
                if (!stat) {
                    throw t.str_select_pair_status;
                }
                if (!selected_observations.length) {
                    throw t.str_please_select_at_least_one_observation
                }
                const gyk = $('#grouping_code').find('option:selected');
                const su = $('#sampling_unit').find('option:selected').val();
                const sp = $('#species').find('option:selected').val();
                const pair_latlng = centroid.getLayers()[0].getLatLng();
                const pair_geom = "POINT(" + pair_latlng.lng + " " + pair_latlng.lat + ")";
                
                const paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: "save_pair_status",
                    si: gyk.data('si'),
                    su: su,
                    sp: sp,
                    ids: selected_observations,
                    status: stat,
                    pair_geom: pair_geom
                }
                let resp = await $.post(ajx, paramters);
                resp = JSON.parse(resp);
                if (resp.status === 'success') {
                    const connected_ids = selected_observations;
                    selected_observations = [];
                    toolbox.update();
                    reset_centroid();
                    observations.getLayers().forEach(o => {
                        const obm_id = o.feature.properties.obm_id;
                        if (connected_ids.indexOf(obm_id) > -1) {
                            o.feature.properties.pair_id = resp.data;
                            o.feature.properties.pair_status = stat;
                            o.setStyle(markerOptions.done);
                            o.off('click');
                            o.on({ click: unpair_observation });
                        }
                    });
                    count_observations();
                }
                else {
                    throw resp.message;
                }
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        })
        
        $('.toolbox').on('click','#unpair', async function() {
            try {
                const pair_id = $(this).data('pair_id');
                if (!pair_id) {
                    throw 'pair id missing';
                }
                
                const paramters = {
                    page: "monitoring",
                    program: raptor_id,
                    action: "unpair",
                    pair_id: pair_id,
                }
                let resp = await $.post(ajx, paramters);
                resp = JSON.parse(resp);
                if (resp.status == 'success') {
                    observations.getLayers().forEach(o => {
                        if (o.feature.properties.pair_id == pair_id) {
                            o.feature.properties.pair_id = null;
                            o.setStyle(markerOptions.pair_data);
                            o.off('click');
                            o.on({ click: select_observation });
                        }
                    });
                    toolbox.update();
                    count_observations();
                }
                else {
                    throw resp.message;
                }
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        })
        var selection = L.featureGroup().addTo(map);
        var centroid = L.featureGroup().addTo(map);
        var latlngs = [];
        
        function count_observations() {
            const layers = observations.getLayers();
            const c = layers.reduce((evaluated, o) => {
                return (o.feature.properties.pair_id !== null) ? evaluated + 1 : evaluated;
            },0);
            $('.obs_num').html( `${String(c)} / ${layers.length}  -  <span class="glossary"><i class="fa fa-info-circle"></i><span class="glossary-definition">  ${t.str_number_of_observations_evaluated}</span></span>`) ;
            
        }
        function onEachObservation(feature, layer) {
            let actions = {
                mouseover: highlightFeature,
            };
            actions.click = (feature.properties.pair_id === null) ? select_observation : unpair_observation;
            layer.on(actions);
        }
        
        function unpair_observation(e) {
            unselect_observations();
            const layer = e.target;
            info.update(layer.feature.properties);
            const pair_id = layer.feature.properties.pair_id;
            toolbox.unpair(pair_id);
        }
        
        function highlightFeature(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
        }
        
        function select_observation(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
            var obm_id = e.target.feature.properties.obm_id;
            var index = selected_observations.indexOf(obm_id);
            if (index > -1) {
                selected_observations.splice(index, 1);
                layer.setStyle(markerOptions.pair_data)
                latlngs = latlngs.filter(l => !l.equals(layer.getLatLng()));
                
            }
            else {
                selected_observations.push(layer.feature.properties.obm_id);
                layer.setStyle(markerOptions.selected);
                latlngs.push(layer.getLatLng());
            }
            update_centroid()
            toolbox.update();
        }
        
        function unselect_observations() {
            selected_observations = [];
            observations.getLayers().forEach(o => {
                if (o.feature.properties.pair_id == null) {
                    o.setStyle(markerOptions.pair_data);
                }
            });
            toolbox.update();
        }

        function update_centroid() {
            centroid.clearLayers();
            if (latlngs.length) {
                var bounds = L.latLngBounds(latlngs);
                centroid.addLayer( L.marker(bounds.getCenter(), { draggable: true }) );
            }
        }
        function reset_centroid() {
            latlngs = [];
            centroid.clearLayers();
        }
    })
    
    // RAPTOR MIGRATION
    
    const rapmig_id = 2063;
    
    $('main').on(`main_page_${rapmig_id}_page_loaded`, async function() {
        $('.period_select').on('change', async function(ev) {
            try {
                const paramters = {
                    page: 'monitoring',
                    program: rapmig_id,
                    action: 'main_page',
                    period_id: period()
                };
                const resp = await $.get(ajx, paramters);
                $('main').html(resp);
                $('main').trigger(`main_page_${rapmig_id}_page_loaded`);
            } catch (e) {
                console.log(e);
            } 
        })
        $( ".datepicker" ).datepicker({
            minDate: new Date($('.period_select').find('option:selected').data('date_from')), 
            maxDate: new Date($('.period_select').find('option:selected').data('date_until')),
            dateFormat:'yy-mm-dd',
            onClose: async function () {
                try {
                    const form = document.querySelector("#rapmig_mainpage_params");
                    const fd = new FormData(form);
                    fd.append('page', 'monitoring');
                    fd.append('program', rapmig_id);
                    fd.append('action', 'main_page');
                    
                    let resp = await $.ajax({
                        url : ajx,
                        type : "POST",
                        data : fd,
                        processData: false,
                        contentType: false,
                        cache: false,
                    });
                    $('main').html(resp);
                    $('main').trigger(`main_page_${rapmig_id}_page_loaded`);
                } catch (e) {
                    console.log(e);
                } 
            }
        });
    });

    $('main').on('dupla_tool_2063_page_loaded', async function() {
        var colors = ["678d58","81d2c7","01BAEF","d66e64","cc0808","D5B886","9b5de5","419d78","f9e71d","8e3b46","fc9f5b","842103","c879ff","f43316","320a28","ff57bb","392f5a","f9e5d6","4e4187","3772ff","000501","c5d86d","603711","684b4b","242038"];
        
        var t = await $.getJSON(ajx, {
            page: 'monitoring',
            program: rapmig_id,
            action: 'get_translations'
        });
        
        var pair_double_selector = await $.get(ajx, {
            page: 'monitoring',
            program: rapmig_id,
            action: 'get_double_status_toolbox'
        });
        
        var selected_observations = [];
        const selected_ids = () => selected_observations.map(o => o.obm_id)
        const selected_numbers = () => selected_observations.map(o => o.number)
        const max_of_selected_numbers = () => selected_numbers().reduce((a, b) => Math.max(a, b), 0)
        const sum_of_selected_numbers = () => selected_numbers().reduce((a, b) => a + b, 0)
        
        var map = L.map('map').setView([46, 25], 7);
        
        L.esri.basemapLayer('Imagery').addTo(map);
        L.esri.basemapLayer('ImageryLabels').addTo(map);
        
        var o_points = L.featureGroup().addTo(map);
        
        var info = L.control({position: 'bottomright'});
        
        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
            L.DomEvent.disableClickPropagation(this._div);
            this.update();
            return this._div;
        };
        
        // method that we will use to update the control based on feature properties passed
        info.update = function (props) {
            let html = "";
            if (props) {
                const props_list = Object.keys(props).reduce((str, k) => {
                    k_transl = (k in t) ? t[k] : k;
                    return str + k_transl + ": <b>" + props[k] + "</b><br>"
                }, "");
                const ang = 42 + Number(props.flight_direction)
                const img = (props.flight_direction >= 0) ? `<img style="transform:rotate(${ang}deg);" src="http://milvus.openbiomaps.org/projects/elc/includes/private/arrow.svg">` : "";
                html = `<div>${props_list}</div><div>${img}</div>`
            }
            
            this._div.innerHTML = html;
        };
    
        info.addTo(map);
        
        toolbox.update = function (props) {
            const sol = selected_observations.length;
            if (sol <= 1) {
                this._div.innerHTML = "<div class='n_observations'>" + t.str_please_select_at_least_two_pairs + "</div>";
            }
            else {
                this._div.innerHTML = "<div class='n_observations'>" + sol + ' ' + t.str_n_observations_selected + "</div>" + pair_double_selector;
                
                var max_number = $('.toolbox input[name="max_number"]');
                max_number.prop('min', max_of_selected_numbers());
                max_number.prop('max', sum_of_selected_numbers());
            }
        };
        toolbox.undouble = function (double_id) {
            let html = t.str_reset + " <button class='pure-button button-error' id='undouble' data-double_id='" + double_id + "'> <i class='fa fa-unlink'> </i> </button>";
            this._div.innerHTML = html;
        }
        toolbox.addTo(map);
        
        var points = L.markerClusterGroup({
            spiderfyDistanceMultiplier: 4
        });
        var observations;
        
        var handleTs = $( "#custom-handle-timespan" );
        var handleT = $( "#custom-handle-time" );
        
        function onEachObservation(feature, layer) {
            let actions = {
                mouseover: highlightFeature,
            };
            actions.click = (feature.properties.double_status === null) ? select_observation : undouble_observation;
            layer.on(actions);
        }
        
        function highlightFeature(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
        }
        
        function undouble_observation(e) {
            unselect_observations();
            const layer = e.target;
            info.update(layer.feature.properties);
            const double_id = layer.feature.properties.double_id;
            toolbox.undouble(double_id);
        }

        function select_observation(e) {
            var layer = e.target;
            info.update(layer.feature.properties);
            var obm_id = e.target.feature.properties.obm_id;
            var index = selected_ids().indexOf(obm_id);
            if (index > -1) {
                selected_observations.splice(index, 1);
            }
            else {
                selected_observations.push(layer.feature.properties);
            }
            focusOnTimeWindow();
            toolbox.update();
        }
        
        function unselect_observations() {
            selected_observations = [];
            focusOnTimeWindow();
            toolbox.update();
        }
        
        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }

        function extractObservationDates(observations) {
            let dates = observations.features.map(o => o.properties.date).filter(onlyUnique).sort();
            let html = '<option value="" selected disabled>Date</option>'
            html += dates.map(d => `<option value="${d}">${d}</option>`).join("")
            $('#date').html(html)
        }
        
        function focusOnTimeWindow () {
            if (!observations) {
                return
            }
            const fd = new FormData(document.querySelector("#dupla-tool-form"));
            observations.getLayers().forEach(o => {
                const props = o.feature.properties
                const selected = (selected_ids().indexOf(props.obm_id) > -1)
                if (props.double_id) {
                    let mo = selected ? markerOptions.done_selected : markerOptions.done
                    o.setStyle(mo)
                }
                else {
                    let mo = markerOptions.base;
                    const t = fd.get('time')
                    const ts = fd.get('timespan')
                    const opacity = o.feature.properties.time_in_minutes >= Number(t) && o.feature.properties.time_in_minutes < Number(t)+Number(ts) ? "ff" : "55" 
                    mo.fillColor = selected ? selected_color + opacity : '#' + colors[o.feature.properties.sampling_unit_id % 24] + opacity;
                    o.setStyle(mo);
                }
            });
        }
        
        
        $('.period_select').on('change', async function(ev) {
            try {
                const paramters = {
                    page: 'monitoring',
                    program: rapmig_id,
                    action: 'dupla_tool',
                    period_id: period()
                };
                const resp = await $.get(ajx, paramters);
                $('main').html(resp);
                $('main').trigger(`dupla_tool_${rapmig_id}_page_loaded`);
            } catch (e) {
                console.log(e);
            } 
        })
        
        $('#grouping_code').on('change', async function() {
            try {
                o_points.clearLayers();
                points.clearLayers();
                const gyk = $(this).find('option:selected').val();
                let paramters = {
                    page: "monitoring",
                    program: rapmig_id,
                    period: period(),
                    action: 'get_sampling_unit_geom',
                    gc: gyk,
                }
                let resp = await $.getJSON(ajx, paramters);
                const r = JSON.parse(resp.data);
                
                o_points.addLayer(L.geoJSON(r, {
                    pointToLayer: function (feature, latlng) {
                        const pn = Number(feature.properties.sampling_unit_id);
                        var redMarker = L.ExtraMarkers.icon({
                            icon: 'fa-number',
                            markerColor: '#' + colors[pn % 24] + "55",
                            svg: true,
                            number: feature.properties.sampling_unit
                        });
                        return L.marker(latlng, {icon: redMarker});
                    },
                }));
                
                const fitBoundsOptions = {
                    maxZoom: 13
                };
                map.fitBounds(o_points.getBounds(), fitBoundsOptions);
                
                paramters.action = 'get_observed_species_list';
                paramters.period = period()
                resp = await $.getJSON(ajx, paramters);
                if (resp.status !== 'success') {
                    $('#species').html('');
                    throw resp.message;
                }
                $('#species').html(resp.data);
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            } 
        });
        
        $('.load_observations').on('change', async function() {
            selected_observations = [];
            try {
                points.clearLayers();
                const id = $(this).prop('id')
                
                const form = document.querySelector("#dupla-tool-form");
                const fd = new FormData(form);
                fd.append('page', 'monitoring');
                fd.append('program', rapmig_id);
                fd.append('action', 'get_observations');
                if (id  === 'species') {
                    fd.set('date', '');
                }
                
                let resp = await $.ajax({
                    url : ajx,
                    type : "POST",
                    data : fd,
                    processData: false,
                    contentType: false,
                    cache: false,
                });
                resp = JSON.parse(resp);
                
                if (resp.status !== 'success') {
                    throw resp.message;
                }
                const r = JSON.parse(resp.data);
                if (r.features === null) {
                    throw t.str_no_certain_or_possible_pairs
                }
                
                if (id  === 'species') {
                    extractObservationDates(r)
                }
                
                observations = L.geoJSON(r, {
                    onEachFeature: onEachObservation,
                    pointToLayer: function (feature, latlng) {
                        let mo = markerOptions.base;
                        return L.circleMarker(latlng, mo);
                    }
                });
                
                focusOnTimeWindow();
                points.addLayer(observations);
                map.addLayer(points);
            }
            catch(e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        });
        
        $( "#slider-timespan" ).slider({
            min: 20,
            max: 120,
            step: 10,
            create: function() {
                handleTs.text( $( this ).slider( "value" ) );
            },
            slide: function( event, ui ) {
                handleTs.text( ui.value );
                $('#timespan').val(ui.value)
                focusOnTimeWindow()
            },
        });
        
        $( "#slider-time" ).slider({
            min: 540,
            max: 1080,
            step: 10,
            create: function() {
                const v = $( this ).slider( "value" )
                const mod = v % 60
                const time = Math.floor(v / 60) + ':' + ((mod === 0) ? '00' : mod)
                handleT.text( time );
            },
            slide: function( event, ui ) {
                const mod = ui.value % 60
                const time = Math.floor(ui.value / 60) + ':' + ((mod === 0) ? '00' : mod)
                handleT.text( time );
                $('#time').val(ui.value)
                focusOnTimeWindow()
            },
        });
        
        $('.toolbox').on('change', '#double_status_selector' ,function() {
            if (this.value == 'str_certain') {
                $('#max_number').prop('value', max_of_selected_numbers());
            }
            $('#max_number').prop('disabled', (this.value == 'str_certain'));
        });
        
        $('.toolbox').on('click','#save_double', async function() {
            try {
                const stat = $('#double_status_selector option:selected').val();
                const max_number = $('#max_number');
                if (!stat) {
                    throw 'Please select status';
                }
                if (Number(max_number.val()) < 1 || Number(max_number.val()) > Number(max_number.prop('max'))) {
                    throw 'Please give the maximum number of possible pairs, but not more than ' + max_number.prop('max');
                }
                if (selected_observations.length < 2) {
                    throw 'Please select at least two pairs'
                }
                const si = $('#grouping_code').find('option:selected').data('si');
                
                const paramters = {
                    page: "monitoring",
                    program: rapmig_id,
                    action: "save_double_status",
                    si: si,
                    ids: selected_ids(),
                    status: stat,
                    min: max_of_selected_numbers(),
                    max: max_number.val()
                }
                let resp = await $.post(ajx, paramters);
                resp = JSON.parse(resp);
                if (resp.status === 'success') {
                    toolbox.update();
                    observations.getLayers().forEach(o => {
                        const obm_id = o.feature.properties.obm_id;
                        if (selected_ids().indexOf(obm_id) > -1) {
                            o.feature.properties.double_id = resp.data;
                            o.feature.properties.double_status = stat;
                            o.off('click');
                            o.on({click: undouble_observation});
                        }
                    });
                    selected_observations = [];
                    focusOnTimeWindow()
                }
                else {
                    throw resp.message;
                }
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        })

        $('.toolbox').on('click','#undouble', async function() {
            try {
                const double_id = $(this).data('double_id');
                if (!double_id) {
                    throw 'Double id missing';
                }
                
                const paramters = {
                    page: "monitoring",
                    program: rapmig_id,
                    action: "undouble",
                    double_id: double_id,
                }
                let resp = await $.post(ajx, paramters);
                resp = JSON.parse(resp);
                if (resp.status == 'success') {
                    resp.data = resp.data.map(x => Number(x));
                    observations.getLayers().forEach(o => {
                        const obm_id = o.feature.properties.obm_id;
                        if (resp.data.indexOf(obm_id) > -1) {
                            o.feature.properties.double_id = null;
                            o.feature.properties.double_status = null;
                            o.off('click');
                            o.on({ click: select_observation });
                        }
                    });
                    focusOnTimeWindow();
                    toolbox.update();
                }
                else {
                    throw resp.message;
                }
            } catch (e) {
                $('#dialog').html(e);
                $('#dialog').dialog('open');
            }
        })
        
    });
})

function dashboard_menu_item(name) {
    return '<option>'+ name + '</option>';
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
