<?php 
/**
 * 
 */
require_once(getenv('PROJECT_DIR').'local/includes/private/page.php');
class MonitoringPage extends Page
{
    private $available_programs = [
      'raptor' => 3354,
      'raptor_migration' => 2063,
    ];
    function __construct($program = '') {
        $content = "";
        $id = null;
        $page = "";
        if (in_array($program, array_keys($this->available_programs))) {
            $id = $this->available_programs[$program];
            require_once(getenv('PROJECT_DIR')."local/includes/private/monitoring_programs/program_$id.php");
            $class = "Program_$id";
            $page = 'main_page';
            if ( method_exists($class, $page)) {
                $content = $class::$page();
            }
        }
        
        $selected = compact('content', 'id', 'page');
        $this->content = $this->prepare_content($selected);
    }
    
    private function get_monitoring_programmes() {
        /*
        global $ID;
        $cmd = sprintf("SELECT obm_id, word as name FROM elc_search WHERE parent_id = 41;");
        if (!$res = pg_query($ID, $cmd)) {
            log_action('elc_program query error', __FILE__,__LINE__);
            return false;
        };
        $programs = [];
        while ($row = pg_fetch_assoc($res)) {
            $programs[$row['obm_id']] = defined($row['name']) ? constant($row['name']) : $row['name'];
        }
        return $programs;
        */
        return [
            3354 => 'Raptor survey',
            2063 => 'Raptor migration'
        ];
    }
    
    private function monitoring_program_selector($id) {
        ob_start();
        ?>
        <select id="monitoring_program_selector">
            <option disabled selected> <?= str_please_choose ?></option>
            <?php foreach ($this->get_monitoring_programmes() as $key => $value): ?>
                <option value="<?= $key ?>" <?= ($id == $key) ? "selected" : ""; ?>><?= $value ?></option>
            <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
    
    private function monitoring_page_selector($id) {
        $menus = [];
        if (!is_null($id)) {
            require_once(getenv('PROJECT_DIR')."local/includes/private/monitoring_programs/program_$id.php");
            $class = "Program_$id";
            $method = 'menu_items';
            $menus = (method_exists($class, $method)) ? json_decode($class::$method()) : [];
        }
        ob_start();
        ?>
        <select id="monitoring_page_selector">
            <option disabled selected> <?= str_please_choose ?></option>
            <?php foreach ($menus as $m): ?>
                <option <?= ($m == 'Main page') ? 'selected' : "" ?>><?= $m ?></option>
            <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
    static function ajax($request) {
        if (isset($request['program']) && is_numeric($request['program'])) {
            $p = (int)$request['program'];
            $filename = getenv('PROJECT_DIR')."local/includes/private/monitoring_programs/program_$p.php";
            if (!file_exists($filename)) {
                log_action("$filename does not exist!");
                return false;
            }
            require_once($filename);
            $class = "Program_$p";
            $method = $request['action'];
            if ( method_exists($class, $method)) {
                return $class::$method($request);
            }
            $filename = getenv('PROJECT_DIR')."local/includes/private/monitoring_programs/program_{$p}_administration.php";
            if (!file_exists($filename)) {
                return false;
            }
            require_once($filename);
            $class = "Program_{$p}_admin";
            if ( method_exists($class, $method)) {
                return $class::$method($request);
            }
            return Program::$method($request);
        }
        return false;
    }
    
    private function get_stylesheet() {
        $css = file_get_contents(getenv('PROJECT_DIR').'local/includes/private/monitoring.css');
        return $css;
    }
    private function get_scripts() {
        $css = file_get_contents(getenv('PROJECT_DIR').'local/includes/private/monitoring.js');
        return $css;
    }
    
    private function prepare_content($selected) {
        global $protocol;
        ob_start();

        ?>
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/leaflet.css">
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet-markercluster/MarkerCluster.css">
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet-markercluster/MarkerCluster.Default.css">
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet-extra-markers/css/leaflet.extra-markers.min.css">
        <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/fSelect.css?rev=<?php echo rev('css/fSelect.css'); ?>" type="text/css" />
        <style media="screen">
            <?= $this->get_stylesheet(); ?>
        </style>
        <header class="header pure-form">
            <?= $this->monitoring_program_selector($selected['id']) ?>
            <?= $this->monitoring_page_selector($selected['id']) ?>
        </header>
        <main class="main"><?= $selected['content'] ?></main>
        <footer class="footer"></footer>
        <script type="text/javascript">
            <?= $this->get_scripts(); ?>
            <?php if ($selected['page'] !== ''): ?>
                $(document).ready(() => { $('main').trigger('<?= $selected['page'] ?>_page_loaded') } );
            <?php endif; ?>
        </script>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/fSelect.js?rev=<?php echo rev('js/fSelect.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet/leaflet.js"> </script>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet-markercluster/leaflet.markercluster.js"> </script>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/leaflet-extra-markers/js/leaflet.extra-markers.min.js"> </script>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/vendor/esri-leaflet/esri-leaflet.js"> </script>
        <?php 
        $out = ob_get_clean();
        return $out;
    }
    
}

/**
 * 
 */
class Program
{
    public static function menu_items() {
        return json_encode([]);
    }
}

?>
