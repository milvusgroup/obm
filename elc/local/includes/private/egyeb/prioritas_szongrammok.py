from os import walk


def main():
    txt_dir = '/home/gabor/Documents/motolla/ecologic/retyezat/audiomoth/priori'
    sql_file = open('/tmp/priority.sql', 'a')
    print("BEGIN;", file=sql_file)
    for dirpath, dirnames, filenames in walk(txt_dir):
        for filename in filenames:
            if filename.endswith('.txt'):
                wavs = read_wav_filename(dirpath + '/' + filename)
                session = filename[0:-4]
                print(session)
                cmd = "UPDATE elc_am_record r SET priority = 1 FROM elc_am_session s WHERE s.obm_id = r.session_id AND s.name = '" + session + "' AND r.filename IN (" + wavs + ");"
                print(cmd, file=sql_file)
    print("COMMIT;", file=sql_file)


def read_wav_filename(filename):
    wavs = []
    with open(filename) as f:
        for line in f:
            line = line.strip()
            wavs.append(line[0:12])
    wavs = str.join(',', list(map(lambda x: ("'" + x + "'"), wavs)))
    return wavs


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        raise e
