<div id='custom_page'>
<?php 
if (in_array($custom_load_page, ['monitoring', 'birdetect'])) {
    require(getenv('PROJECT_DIR') . "/local/includes/private/$custom_load_page.php");
    $class = $custom_load_page . 'Page';
    if (!class_exists($class)) {
        echo "$class object does not exists.";
        exit;
    }
    $subpage = $clp_subpage ?? "";
    $page = new $class($subpage);
    echo $page->the_content();
}
?>
</div>
