<?php
class read_table_links_Class  {
    function print_box ($params='') {
        $a = '<a href="http://milvus.openbiomaps.org/projects/elc/view-table/k40674k4a4a4n2l5e464c5j4j524v53494c4p4p424h4b4k5y5m2m4a4n444/" target="_blank">elc_search</a>';

        $t = new table_row_template();
        $t->cell(mb_convert_case("Custom links", MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();
        $t->cell($a,2,'content');
        $t->row();

        return sprintf("<table class='mapfb'>%s</table>",$t->printOut());
    }

    function print_js ($params='') {
        echo '';
    }
}
?>
