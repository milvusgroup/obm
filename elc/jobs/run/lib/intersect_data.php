<?php
class intersect_data extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('intersect_data initialized', __FILE__, __LINE__);
        return true;
    }

    public function get_results() {
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        $cmdpart = [];
        $tblHeader = ['total rows'];
        foreach ($params as $col => $opts) {
            if ($col == 'corine' || $col = 'dem' || $col == 'habitat_retezat') {
                continue;
            }
            $cmdpart[] = "count(CASE WHEN $col IS NOT NULL THEN 1 END) AS $col";
            $tblHeader[] = $col;
        }
        if (!count($cmdpart)) {
            return 'Params not defined';
        }
        $cmd = sprintf("SELECT count(*) AS total, %s FROM %s_qgrids;", implode(', ', $cmdpart), PROJECTTABLE);

        if (!$res = query($ID,$cmd))
            return 'ERROR: query error';


        $results = pg_fetch_assoc($res[0]);

        $t = [$results['total']];
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader($tblHeader);

        foreach ($params as $col => $opts) {
            if ($col == 'corine' || $col = 'dem' || $col == 'habitat_retezat') {
                continue;
            }
            $t[] = $results[$col]. ' ('. round($results[$col] * 100 / $t[0],2) .'%)';
        }

        $tbl->addRows($t);

        return $tbl->printOut();
    }


    static function run() {
        global $ID;
        $params = parent::getJobParams(__CLASS__);
        $cmd = [];
        $limit = round(1000 / count((array)$params));
        foreach ($params as $col => $opts) {
            if (in_array($col, ['corine','dem','habitat_retezat'])) {
                $method = "intersect_with_$col";
                if (method_exists(__CLASS__,$method)) {
                    job_log($method);
                    self::$method();
                }
                continue;
            }
            $filter = (isset($opts->filterColumn) && isset($opts->filterValue))
                ? "g.{$opts->filterColumn} = '{$opts->filterValue}'"
                : "1 = 1";

            $transform = (isset($opts->srid))
                ? sprintf("ST_Transform(rqg.centroid,%s)", $opts->srid)
                : "rqg.centroid";

            $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.%2$s FROM %9$s_qgrids rqg LEFT JOIN %3$s.%4$s g ON ST_Intersects(%6$s,%7$s) WHERE %1$s IS NULL AND %5$s ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = rows.%2$s FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;',
                $col,
                $opts->valueColumn,
                $opts->schema,
                $opts->table,
                $filter,
                $transform,
                $opts->geomColumn,
                $limit,
                PROJECTTABLE
            );
            if ($opts->valueColumn !== 'geometry') {
                $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id FROM %9$s_qgrids rqg WHERE %1$s IS NULL AND NOT EXISTS (SELECT 1 FROM %3$s.%4$s g WHERE ST_Intersects(%6$s,%7$s) AND %5$s) ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = \'NA\' FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;',
                    $col,
                    $opts->valueColumn,
                    $opts->schema,
                    $opts->table,
                    $filter,
                    $transform,
                    $opts->geomColumn,
                    $limit,
                    PROJECTTABLE
                );
            }
        }
        if (count($cmd))
            query($ID, $cmd);
    }

    static function intersect_with_dem() {
        global $ID;
        $cmd = [];
        

        // főtábla
        $cmd[] = " WITH rows AS (
            SELECT obm_id, round(st_value(rast, ST_Centroid(obm_geometry))::numeric,2) as altitude
            FROM elc e LEFT JOIN shared.eu_dem_v11_e50n20 ON ST_Intersects(rast, ST_Centroid(obm_geometry))
            WHERE e.altitude IS NULL LIMIT 1000
        )
        UPDATE elc e SET altitude = rows.altitude FROM rows WHERE rows.obm_id = e.obm_id;";

        // alapegységek
        $cmd[] = " WITH rows AS (
            SELECT obm_id, round(st_value(rast, ST_Centroid(obm_geometry))::numeric,2) as altitude
            FROM elc_search e LEFT JOIN shared.eu_dem_v11_e50n20 ON ST_Intersects(rast, ST_Centroid(obm_geometry))
            WHERE e.subject = 'sampling_unit' AND e.altitude IS NULL LIMIT 1000
        )
        UPDATE elc_search e SET altitude = rows.altitude FROM rows WHERE rows.obm_id = e.obm_id;";
        query($ID, $cmd);
    }
    static function intersect_with_corine() {
        global $ID;
        $cmd = [];

        // főtábla
        $cmd[] = " WITH rows AS (
            SELECT obm_id, code_18
            FROM elc e LEFT JOIN shared.clc2018_clc2018_v2018_20 ON ST_Intersects(wkb_geometry, ST_Centroid(obm_geometry))
            WHERE e.corine_code IS NULL LIMIT 1000
        )
        UPDATE elc e SET corine_code = rows.code_18 FROM rows WHERE rows.obm_id = e.obm_id;";

        // alapegységek
        $cmd[] = " WITH rows AS (
            SELECT obm_id, code_18
            FROM elc_search e LEFT JOIN shared.clc2018_clc2018_v2018_20 ON ST_Intersects(wkb_geometry, ST_Centroid(obm_geometry))
            WHERE e.subject = 'sampling_unit' AND e.corine_code IS NULL LIMIT 1000
        )
        UPDATE elc_search e SET corine_code = rows.code_18 FROM rows WHERE rows.obm_id = e.obm_id;";

        query($ID, $cmd);
    }
    static function intersect_with_habitat_retezat() {
        global $ID;
        $cmd = [];

        // főtábla ha van metszet
        $cmd[] = " WITH rows AS (
            SELECT obm_id, h.denumire
            FROM elc e LEFT JOIN elc.habitat h ON ST_Intersects(wkb_geometry, ST_Centroid(obm_geometry))
            WHERE e.habitat IS NULL LIMIT 1000
        )
        UPDATE elc e SET habitat = rows.denumire FROM rows WHERE rows.obm_id = e.obm_id;";

        // főtábla, ha nincs metszet
        $cmd[] = 'WITH rows AS (
            SELECT obm_id FROM elc WHERE habitat IS NULL AND NOT EXISTS (
                SELECT 1 FROM elc.habitat g WHERE ST_Intersects(wkb_geometry,ST_Centroid(obm_geometry))
            ) ORDER BY obm_id DESC LIMIT 1000)
            UPDATE elc e SET habitat = \'NA\' FROM rows WHERE rows.obm_id = e.obm_id;';

        $alapegyseg = [];

        // alapegység tábla ha van metszet
        $alapegyseg[] = " WITH rows AS (
            SELECT obm_id, h.denumire
            FROM elc_search e LEFT JOIN elc.habitat h ON ST_Intersects(wkb_geometry, ST_Centroid(obm_geometry))
            WHERE e.subject = 'sampling_unit' AND e.habitat IS NULL LIMIT 1000
        )
        UPDATE elc_search e SET habitat = rows.denumire FROM rows WHERE rows.obm_id = e.obm_id;";

        // alapegység tábla, ha nincs metszet
        $alapegyseg[] = "WITH rows AS (
            SELECT obm_id FROM elc_search WHERE subject = 'sampling_unit' AND habitat IS NULL AND NOT EXISTS (
                SELECT 1 FROM elc.habitat g WHERE ST_Intersects(wkb_geometry,ST_Centroid(obm_geometry))
            ) ORDER BY obm_id DESC LIMIT 1000)
            UPDATE elc_search e SET habitat = 'NA' FROM rows WHERE rows.obm_id = e.obm_id;";
        $cmd = array_merge($cmd, $alapegyseg);
        query($ID, $cmd);
    }
}
?>
