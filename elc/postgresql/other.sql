WITH upd as (
    select nest_name, nest_place, natural_artificial, built_by_species, version, observer from elc_nests where version = 2
) 
SELECT 
    n.nest_name, 
    n.nest_place as nest_place_orig, 
    upd.nest_place as nest_place_new, 
    n.natural_artificial as natural_artificial_orig, 
    upd.natural_artificial as natural_artificial_new, 
    n.built_by_species as built_by_species_orig,  
    upd.built_by_species as built_by_species_new,
    n.observer as observer_orig,
    upd.observer as observer_new
FROM elc_nests n LEFT JOIN upd ON n.nest_name = upd.nest_name 
WHERE n.version = 1 AND n.program = 'Câmpia Nirului - Valea Ierului: Recensământul berzei albe' AND upd.nest_name IS NOT NULL;

WITH upd as (
    select nest_name, nest_place, natural_artificial, built_by_species, version, observer from elc_nests where version = 2
)
UPDATE elc_nests n SET archive = true FROM upd
WHERE n.nest_name = upd.nest_name AND n.version = 1 AND n.program = 'Câmpia Nirului - Valea Ierului: Recensământul berzei albe';

-- duplakereses observados adatokra
select distinct obm_uploading_id, program, grouping_code, sampling_unit, date, observers FROM elc WHERE source_id IN (select source_id from elc GROUP BY source_id, species, obm_geometry, age, gender, time_of_start, program, method, grouping_code, sampling_unit HAVING count(source_id) > 1);

DELETE FROM elc WHERE comments_observation = 'ObsMapp' AND (species, number, date, observers, program, method, grouping_code, sampling_unit) IN (select species, number, date, observers, program, method, grouping_code, sampling_unit from elc WHERE observers IS NOT NULL AND method IS NOT NULL AND program IS NOT NULL AND grouping_code IS NOT NULL AND sampling_unit IS NOT NULL GROUP BY date, species, number, observers, obm_geometry, program, method, grouping_code, sampling_unit HAVING count(*) > 1);

-- golya osszesítőtábla
WITH 
occup as (SELECT DISTINCT extract(year from date) as year, program, grouping_code, sampling_unit, nest_id, species FROM elc_nest_data)
SELECT 
    sampling_unit, 
    sum(ocupat_2019) as ocupat_2019, 
    sum(neocupat_2019) as neocupat_2019, 
    sum(ocupat_2020) as ocupat_2020, 
    sum(neocupat_2020) as neocupat_2020
FROM
    (SELECT 
        su.sampling_unit, 
        CASE WHEN n.year = 2019 AND species = 'Ciconia ciconia' 
            THEN 1 ELSE 0 END as ocupat_2019, 
        CASE WHEN n.year = 2019 AND species != 'Ciconia ciconia' 
            THEN 1 ELSE 0 END as neocupat_2019, 
        CASE WHEN n.year = 2020 AND species = 'Ciconia ciconia' 
            THEN 1 ELSE 0 END as ocupat_2020, 
        CASE WHEN n.year = 2020 AND species != 'Ciconia ciconia' 
            THEN 1 ELSE 0 END as neocupat_2020
    FROM elc_sampling_units su 
    LEFT JOIN occup n ON 
        su.program = n.program AND 
        su.grouping_code = n.grouping_code AND 
        su.sampling_unit = n.sampling_unit 
    WHERE 
        su.program = 'Câmpia Nirului - Valea Ierului: Recensământul berzei albe'
--        AND su.sampling_unit IN ('Urziceni', 'Urziceni Pădure', 'Foieni', 'Viișoara', 
--            'Ciumești', 'Horea', 'Sanislău', 'Scărișoara Nouă', 'Pișcolt', 
--            'Curtuișeni', 'Valea lui Mihai', 'Șimian', 'Voivozi', 'Șilindru', 
--            'Cheșereu', 'Adoni', 'Tarcea', 'Otomani', 'Galoșpetreu', 'Sălacea', 
--            'Vășad', 'Andrid', 'Pir', 'Dindești', 'Sudurău', 'Vezendiu', 'Tiream')

    ) as foo 
GROUP BY foo.sampling_unit;

--fiokak szama
WITH 
chicks as (SELECT DISTINCT extract(year from date) as year, program, grouping_code, sampling_unit, species, sum(number) as number FROM elc_nest_data WHERE species = 'Ciconia ciconia' AND age = 'pui' GROUP BY date, program, grouping_code, sampling_unit, species)
SELECT 
    sampling_unit, 
    sum(pui_2019) as pui_2019, 
    sum(pui_2020) as pui_2020 
FROM
    (SELECT 
        su.sampling_unit, 
        c.year,
        CASE WHEN c.year = 2019 AND species = 'Ciconia ciconia'
            THEN "number" ELSE 0 END as pui_2019,
        CASE WHEN c.year = 2020 AND species = 'Ciconia ciconia'
            THEN "number" ELSE 0 END as pui_2020
    FROM elc_sampling_units su 
    LEFT JOIN chicks c ON 
        su.program = c.program AND 
        su.grouping_code = c.grouping_code AND 
        su.sampling_unit = c.sampling_unit
    WHERE 
        su.program = 'Câmpia Nirului - Valea Ierului: Recensământul berzei albe' 
    --    AND su.sampling_unit IN ('Urziceni', 'Urziceni Pădure', 'Foieni', 'Viișoara', 
    --        'Ciumești', 'Horea', 'Sanislău', 'Scărișoara Nouă', 'Pișcolt', 
    --        'Curtuișeni', 'Valea lui Mihai', 'Șimian', 'Voivozi', 'Șilindru', 
    --        'Cheșereu', 'Adoni', 'Tarcea', 'Otomani', 'Galoșpetreu', 'Sălacea', 
    --        'Vășad', 'Andrid', 'Pir', 'Dindești', 'Sudurău', 'Vezendiu', 'Tiream')

    ) as foo 
GROUP BY foo.sampling_unit;


WITH 
megvan as (
  SELECT DISTINCT filename, name FROM elc_audiomoth WHERE tag IS NOT NULL
),
nincsmeg as (
  SELECT DISTINCT a.filename, a.name 
  FROM elc_audiomoth a
  LEFT JOIN megvan mv ON mv.filename = a.filename AND mv.name = a.name
  WHERE 
    priority = 1 AND
    mv.filename IS NULL
),
m AS (
  SELECT DISTINCT name, count(*) megvan FROM megvan GROUP BY name
),
n as (
    SELECT DISTINCT name, count(*) nincsmeg FROM nincsmeg GROUP BY name
),
names as (
    SELECT DISTINCT name FROM (SELECT name FROM m UNION SELECT name FROM n) bar
)
SELECT nm.name, m.megvan, n.nincsmeg FROM names nm LEFT JOIN n ON nm.name = n.name LEFT JOIN m ON nm.name = m.name;

-- Pitesti export
COPY (SELECT obm_id, st_astext(obm_geometry) as geom, obm_uploading_id, species, date, observers, method, time_of_start, time_of_end, duration, complete_list, wind_force, wind_direction, precipitation, visibility, comments_general, number, number_max, observed_unit, count_precision, gender, age, program, altitude, grouping_code, sampling_unit, cloud_cover, flight_direction, flight_altitude, secondary_time, status_of_individuals, comments_observation, exact_time, source, source_id, corine_code, projekt, obm_observation_list_id FROM elc WHERE projekt = 'Pitesti') TO '/tmp/pitest_all.csv' CSV HEADER;

-- hány érintetlen prioritás hangfelvétel van még
WITH 
megvan as (
  SELECT DISTINCT filename, name FROM elc_audiomoth WHERE tag IS NOT NULL
),
nincsmeg as (
  SELECT DISTINCT a.filename, a.name 
  FROM elc_audiomoth a
  LEFT JOIN megvan mv ON mv.filename = a.filename AND mv.name = a.name
  WHERE 
    priority = 1 AND
    mv.filename IS NULL
)
SELECT count(*) FROM nincsmeg;


-- 2020. 11. 08. Régi adatok átimportálása az OBM-ből
\copy (WITH elc_c as (select column_name from information_schema.columns where table_name = 'elc'), milvus_c as (select column_name from information_schema.columns where table_name = 'milvus') SELECT elc_c.column_name as elc_columns, milvus_c.column_name as milvus_columns FROM elc_c FULL JOIN milvus_c ON elc_c.column_name = milvus_c.column_name ) to '~/obm/milvus-custom-codes/elc/postgresql/elc_milvus_colnames.csv' CSV HEADER;

INSERT INTO elc ( "obm_geometry", "obm_uploading_id", "obm_validation", 
  "obm_comments", "obm_modifier_id", "obm_files_id", "species", "date", 
  "date_until", "observers", "method", "time_of_start", "time_of_end", 
  "duration", "complete_list", "wind_force", "wind_direction", "precipitation", 
  "snow_depth", "visibility", "temperature", "optics_used", "comments_general",
  "number", "number_max", "observed_unit", "count_precision", "gender", "age", 
  "habitat_state", "program", "colony_type", "colony_place", 
  "colony_survey_method", "disturbance", "atmospheric_pressure", "altitude", 
  "obm_access", "grouping_code", "sampling_unit", "ice_cover", "cloud_cover", 
  "wind_speed_kmh", "flight_direction", "cause_of_death", "nest_type", 
  "nest_status", "nest_condition", "distance_m", "distance_code", "habitat", 
  "habitat_2", "map_code", "timing_of_reaction", "response_type", 
  "in_flight_sitting", "flight_altitude", "migration_intensity", 
  "migration_type", "secondary_time", "status_of_individuals", "nest_place", 
  "comments_observation", "list_id", "longitude", "latitude", "data_owner", 
  "number_of_eggs", "number_of_nestlings", "method_details_species_aimed", 
  "exact_time", "precision_of_count_list", "surveyed_geometry", "source", 
  "source_id", "non_human_observer", "nest_id", "samplin_unit_coverage", 
  "active_abandoned", "type_number_trees", "type_place_roost_colony", "threats", 
  "nestling_age", "nr_dead_egg", "nr_dead_nestling", "projekt", "pair_id")
SELECT 
  "obm_geometry", "obm_uploading_id", "obm_validation", "obm_comments", 
  "obm_modifier_id", "obm_files_id", "species", "date", "date_until", 
  "observers", "method", "time_of_start", "time_of_end", "duration", 
  "comlete_list", "wind_force", "wind_direction", "precipitation", "snow_depth", 
  "visibility", "temperature", "optics_used", "comments_general", "number", 
  "number_max", "observed_unit", "count_precision", "gender", "age", 
  "habitat_state", 
  CASE WHEN program is NULL THEN local_program ELSE program END AS program,  
  "colony_type", "colony_place", 
  "colony_survey_method", "disturbance", "atmospheric_pressure", "altitude", 
  "obm_access", "local_grouping_code", "local_sampling_unit", "ice_cover", 
  "cloud_cover", "wind_speed_kmh", "flight_direction", "cause_of_death", 
  "nest_type", "nest_status", "nest_condition", "distance_m", "distance_code", 
  "habitat_id", "habitat_2", "map_code", "timing_of_reaction", "response_type", 
  "in_flight_sitting", "flight_altitude", "migration_intensity", 
  "migration_type", "secondary_time", "status_of_individuals", "nest_place", 
  "comments_observation", "list_id", "x", "y", "data_owner", "number_of_eggs", 
  "number_of_nestlings", "method_details_species_aimed", "exact_time", 
  "precision_of_count_list", "surveyed_geometry", "source", "source_id", 
  "non_human_observer", "nest_id", "samplin_unit_coverage", "active_abandoned", 
  "type_number_trees", "type_place_roost_colony", "threats", "nestling_age", 
  "nr_dead_egg", "nr_dead_nestling", "projekt", "pair_id"
FROM milvus m 
LEFT JOIN milvus_roviditesek ON local_program = rovidites 
WHERE local_program LIKE 'ELC%';

INSERT INTO elc_search (word, subject, data_table) VALUES ('Balta Tătaru POS', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Duna PC', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Dunăre, Oltenița', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Fălticeni', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Gyergyó POS', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Ianca Plopu Sărat POS', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Măxineni', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Nagyhagymás POS', 'projekt', 'elc');
INSERT INTO elc_search (word, subject, data_table) VALUES ('Stânca Costești', 'projekt', 'elc');

BEGIN;
UPDATE elc SET program = 'Balta Tătaru POS: teljes felmérés' WHERE program = 'Balta Tătaru POS - teljes felmérés';
UPDATE elc SET program = 'Balta Tătaru POS: teljes felmérés: transzektek' WHERE program = 'Balta Tătaru POS - teljes felmérés - transzektek';
UPDATE elc SET program = 'Duna PC' WHERE program = 'Duna PC';
UPDATE elc SET program = 'Dunăre, Oltenița: teljes felmérés' WHERE program = 'Dunăre, Oltenița - teljes felmérés';
UPDATE elc SET program = 'Fălticeni: teljes felmérés' WHERE program = 'Fălticeni - teljes felmérés';
UPDATE elc SET program = 'Gyergyó POS: császármadár' WHERE program = 'Gyergyó POS - császármadár';
UPDATE elc SET program = 'Gyergyó POS: faunisztika' WHERE program = 'Gyergyó POS - faunisztika';
UPDATE elc SET program = 'Gyergyó POS: gólyafelmérés' WHERE program = 'Gyergyó POS - gólyafelmérés';
UPDATE elc SET program = 'Gyergyó POS: haris' WHERE program = 'Gyergyó POS - haris';
UPDATE elc SET program = 'Gyergyó POS: harkály' WHERE program = 'Gyergyó POS - harkály';
UPDATE elc SET program = 'Gyergyó POS: lappantyú' WHERE program = 'Gyergyó POS - lappantyú';
UPDATE elc SET program = 'Gyergyó POS: légykapó' WHERE program = 'Gyergyó POS - légykapó';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - faunisztika' WHERE program = 'Gyergyó POS - Marosszoros - faunisztika';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - fehér gólya' WHERE program = 'Gyergyó POS - Marosszoros - fehér gólya';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - haris' WHERE program = 'Gyergyó POS - Marosszoros - haris';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - harkály' WHERE program = 'Gyergyó POS - Marosszoros - harkály';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - lappantyú' WHERE program = 'Gyergyó POS - Marosszoros - lappantyú';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - légykapó' WHERE program = 'Gyergyó POS - Marosszoros - légykapó';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - ragadozó' WHERE program = 'Gyergyó POS - Marosszoros - ragadozó';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - téli ragadozó' WHERE program = 'Gyergyó POS - Marosszoros - téli ragadozó';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - törpekuvik' WHERE program = 'Gyergyó POS - Marosszoros - törpekuvik';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - tövisszúró gébics' WHERE program = 'Gyergyó POS - Marosszoros - tövisszúró gébics';
UPDATE elc SET program = 'Gyergyó POS: Marosszoros - uráli' WHERE program = 'Gyergyó POS - Marosszoros - uráli';
UPDATE elc SET program = 'Gyergyó POS: ragadozó' WHERE program = 'Gyergyó POS - ragadozó';
UPDATE elc SET program = 'Gyergyó POS: téli ragadozó' WHERE program = 'Gyergyó POS - téli ragadozó';
UPDATE elc SET program = 'Gyergyó POS: törpekuvik' WHERE program = 'Gyergyó POS - törpekuvik';
UPDATE elc SET program = 'Gyergyó POS: tövisszuró gébics' WHERE program = 'Gyergyó POS - tövisszuró gébics';
UPDATE elc SET program = 'Gyergyó POS: uráli felmérés' WHERE program = 'Gyergyó POS - uráli felmérés';
UPDATE elc SET program = 'Ianca Plopu Sărat POS: teljes felmérés' WHERE program = 'Ianca Plopu Sărat POS - teljes felmérés';
UPDATE elc SET program = 'Măxineni: teljes felmérés' WHERE program = 'Măxineni - teljes felmérés';
UPDATE elc SET program = 'Nagyhagymás POS: erdei pacsirta' WHERE program = 'Nagyhagymás POS - erdei pacsirta';
UPDATE elc SET program = 'Nagyhagymás POS: faunisztika' WHERE program = 'Nagyhagymás POS - faunisztika';
UPDATE elc SET program = 'Nagyhagymás POS: gatyáskuvik' WHERE program = 'Nagyhagymás POS - gatyáskuvik';
UPDATE elc SET program = 'Nagyhagymás POS: harkály' WHERE program = 'Nagyhagymás POS - harkály';
UPDATE elc SET program = 'Nagyhagymás POS: lappantyú' WHERE program = 'Nagyhagymás POS - lappantyú';
UPDATE elc SET program = 'Nagyhagymás POS: légykapó' WHERE program = 'Nagyhagymás POS - légykapó';
UPDATE elc SET program = 'Nagyhagymás POS: ragadozó' WHERE program = 'Nagyhagymás POS - ragadozó';
UPDATE elc SET program = 'Nagyhagymás POS: siketfajd' WHERE program = 'Nagyhagymás POS - siketfajd';
UPDATE elc SET program = 'Nagyhagymás POS: szirti: vándor' WHERE program = 'Nagyhagymás POS - szirti - vándor';
UPDATE elc SET program = 'Nagyhagymás POS: törpekuvik' WHERE program = 'Nagyhagymás POS - törpekuvik';
UPDATE elc SET program = 'Nagyhagymás POS: tövisszúró gébics' WHERE program = 'Nagyhagymás POS - tövisszúró gébics';
UPDATE elc SET program = 'Nagyhagymás POS: uhu' WHERE program = 'Nagyhagymás POS - uhu';
UPDATE elc SET program = 'Nagyhagymás POS: uráli' WHERE program = 'Nagyhagymás POS - uráli';
UPDATE elc SET program = 'Stânca Costești: teljes felmérés' WHERE program = 'Stânca Costești - teljes felmérés';
END;

INSERT INTO elc_taxon (word, lang, taxon_id, status) SELECT word, lang, taxon_id, status FROM milvus_taxon WHERE status = 'accepted';
INSERT INTO elc_taxon (word, lang, status) SELECT DISTINCT species, 'species', 'undefined' FROM elc WHERE species NOT IN (SELECT word FROM elc_taxon) ;

-- regi modszerek
BEGIN; 
WITH 
    modszerek AS (SELECT obm_id, word FROM elc_search WHERE subject = 'method'),
    programok AS (SELECT obm_id, word FROM elc_search WHERE subject = 'program'),
    hianyzo_modszerek AS (
        SELECT DISTINCT method as word FROM elc WHERE 
            program IS NOT NULL AND 
            program NOT IN (SELECT word FROM programok) AND 
            method NOT IN (SELECT word FROM modszerek)
    )
    INSERT INTO elc_search (data_table, subject, word, status)
    SELECT 'elc', 'method', word, 0 FROM hianyzo_modszerek;
COMMIT;    
    

-- regi programok
BEGIN; 
WITH 
    modszerek AS (SELECT obm_id, word FROM elc_search WHERE subject = 'method'),
    programok AS (SELECT obm_id, word FROM elc_search WHERE subject = 'program'),
    hianyzo_programok AS (
        SELECT DISTINCT program as word, modszerek.obm_id as parent_id
        FROM elc
        LEFT JOIN modszerek ON modszerek.word = elc.method 
        WHERE 
            program IS NOT NULL AND 
            program NOT IN (SELECT word FROM programok)
    )
    INSERT INTO elc_search (data_table, subject, word, status, parent_id)
    SELECT 'elc', 'program', word, 0, parent_id FROM hianyzo_programok;
COMMIT;    
    
-- regi gyujtokodok
BEGIN; 
WITH 
    programok AS (SELECT obm_id, word FROM elc_search WHERE subject = 'program'),
    gyk AS (SELECT g.obm_id, g.word, g.parent_id, p.word as parent_name FROM elc_search g LEFT JOIN programok p ON g.parent_id = p.obm_id WHERE g.subject = 'grouping_code'),
    hianyzo_gyk AS (
        SELECT DISTINCT grouping_code as word, programok.obm_id as parent_id, program
        FROM elc
        LEFT JOIN programok ON programok.word = elc.program 
        WHERE 
            grouping_code IS NOT NULL AND
            (program, grouping_code)  NOT IN (SELECT parent_name, word FROM gyk)
    )
    INSERT INTO elc_search (data_table, subject, word, status, parent_id)
    SELECT 'elc', 'grouping_code', word, 0, parent_id FROM hianyzo_gyk;
COMMIT;    
    
-- regi alapegysegek
BEGIN; 
WITH 
    programok AS (SELECT obm_id, word FROM elc_search WHERE subject = 'program'),
    gyk AS (SELECT g.obm_id, g.word, g.parent_id, p.word as parent_name FROM elc_search g LEFT JOIN programok p ON g.parent_id = p.obm_id WHERE g.subject = 'grouping_code'),
    hianyzo_gyk AS (
        SELECT DISTINCT grouping_code as word, programok.obm_id as parent_id, program
        FROM elc
        LEFT JOIN programok ON programok.word = elc.program 
        WHERE 
            grouping_code IS NOT NULL AND
            (program, grouping_code)  NOT IN (SELECT parent_name, word FROM gyk)
    )
    INSERT INTO elc_search (data_table, subject, word, status, parent_id)
    SELECT 'elc', 'grouping_code', word, 0, parent_id FROM hianyzo_gyk;
COMMIT;    


-- fészkes, odvas módszer
BEGIN;
UPDATE elc_search SET word = 'str_nests_cavities' WHERE obm_id = 3945;
UPDATE elc_search SET parent_id = 3945 WHERE parent_id = 3948;
DELETE FROM elc_search WHERE obm_id = 3948;
COMMIT;

