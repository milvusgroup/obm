CREATE TRIGGER "elc_search_fill_meta" BEFORE INSERT OR UPDATE ON "elc_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "elc_taxon_fill_meta" BEFORE INSERT OR UPDATE ON "elc_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
