CREATE TRIGGER elc_am_event_set_timestamp
BEFORE UPDATE ON elc_am_event
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();