CREATE TRIGGER "phototag_update_elc" AFTER INSERT OR UPDATE ON "elc_photos" FOR EACH ROW EXECUTE PROCEDURE update_elc_phototags();
