CREATE TRIGGER elc_nests_versioning BEFORE INSERT ON public.elc_nests FOR EACH ROW EXECUTE PROCEDURE public.elc_nest_versions();
