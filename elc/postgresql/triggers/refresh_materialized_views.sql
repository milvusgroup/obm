CREATE TRIGGER "refresh_materialized_views" AFTER INSERT OR UPDATE OR DELETE ON "elc_search" FOR EACH STATEMENT EXECUTE PROCEDURE elc_refresh_materialized_views();
CREATE TRIGGER "refresh_materialized_views" AFTER INSERT OR UPDATE OR DELETE ON "elc_nests" FOR EACH STATEMENT EXECUTE PROCEDURE elc_refresh_materialized_views();
CREATE TRIGGER "refresh_materialized_views" AFTER INSERT OR UPDATE OR DELETE ON "elc" FOR EACH STATEMENT EXECUTE PROCEDURE elc_refresh_materialized_views();
