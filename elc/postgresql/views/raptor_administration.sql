DROP VIEW "elc"."raptor_administration";
CREATE OR REPLACE VIEW "elc"."raptor_administration" AS
SELECT 
    id, period_id, grouping_code, responsible, array_to_json(array_agg(observer)) as observer, finished, no_of_points
FROM (
    SELECT 
        s.id,
        s.period_id,
        s.grouping_code,
        s.responsible,
        o.observer,
        s.finished,
        count(*) as no_of_points
    FROM elc.raptor_survey_session s
    LEFT JOIN elc.raptor_period p ON s.period_id = p.id
    LEFT JOIN elc.raptor_observer o ON s.id = o.session_id
    LEFT JOIN elc_sampling_units su ON su.program = p.program AND su.grouping_code = s.grouping_code
    GROUP BY s.id, s.period_id, s.grouping_code, s.responsible, o.observer, s.finished
) as foo
GROUP BY id, period_id, grouping_code, responsible, finished, no_of_points;

GRANT ALL on "elc"."raptor_administration" TO elc_admin;