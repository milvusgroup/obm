DROP TABLE elc_tracklogs; 
DROP SEQUENCE elc_tracklogs_id_seq;
DROP VIEW elc_tracklogs;
CREATE OR REPLACE VIEW elc_tracklogs AS
SELECT 
    tracklog_id, user_id, start_time, end_time, trackname, observation_list_id, tracklog_line_geom
FROM 
    system.tracklogs WHERE project = 'elc';
    
GRANT SELECT on elc_tracklogs TO elc_admin;
GRANT SELECT on elc_tracklogs TO mihhok_gmail_com;