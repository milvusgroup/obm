DROP MATERIALIZED VIEW elc_nest_data;
CREATE MATERIALIZED VIEW elc_nest_data AS
    SELECT 
        e.obm_id, n.obm_geometry, e.obm_datum, e.obm_uploading_id,
        e.obm_validation, e.obm_comments, e.obm_modifier_id, e.obm_files_id, e.species,
        e.date, e.date_until, e.observers, e.method, e.time_of_start, e.time_of_end,
        e.duration, e.complete_list, e.wind_force, e.wind_direction, e.precipitation,
        e.snow_depth, e.visibility, e.temperature, e.optics_used, e.comments_general,
        e.number, e.number_max, e.observed_unit, e.count_precision, e.gender, e.age,
        e.habitat_state, e.program, e.colony_type, e.colony_place, e.colony_survey_method,
        e.disturbance, e.atmospheric_pressure, e.altitude, e.obm_access, e.grouping_code,
        e.sampling_unit, e.ice_cover, e.cloud_cover, e.wind_speed_kmh, e.flight_direction,
        e.cause_of_death, e.nest_type, e.nest_status, e.nest_condition, e.distance_m,
        e.distance_code, e.habitat, e.habitat_2, e.map_code, e.timing_of_reaction,
        e.response_type, e.in_flight_sitting, e.flight_altitude, e.migration_intensity,
        e.migration_type, e.secondary_time, e.status_of_individuals, e.nest_place,
        e.comments_observation, e.list_id, e.longitude, e.latitude, e.data_owner,
        e.number_of_eggs, e.number_of_nestlings, e.method_details_species_aimed,
        e.exact_time, e.precision_of_count_list, e.surveyed_geometry, e.source,
        e.source_id, e.non_human_observer, e.nest_id, e.samplin_unit_coverage,
        e.active_abandoned, e.type_number_trees, e.type_place_roost_colony, e.threats,
        e.nestling_age, e.nr_dead_egg, e.nr_dead_nestling, e.corine_code, e.projekt,
        e.obm_observation_list_id, e.building_type, e.building_status, e.building_location,
        e.sample_collected, e.pair_id
    FROM elc_nests n 
    LEFT JOIN elc e ON e.program = n.program AND e.grouping_code = n.grouping_code AND e.sampling_unit = n.sampling_unit AND e.nest_id = n.nest_name 
    WHERE n.archive = false AND e.nest_id IS NOT NULL;
    
ALTER MATERIALIZED VIEW elc_nest_data OWNER TO elc_admin;
