DROP MATERIALIZED VIEW elc_grouping_codes;
CREATE MATERIALIZED VIEW elc_grouping_codes AS
SELECT ROW_NUMBER() OVER () uid, method, projekt, program, program_id, grouping_code, grouping_code_id, active, points_on_field FROM 
(
SELECT
    m.word AS method,
    pr.word as projekt,
    p.word AS program,
    p.obm_id as program_id,
    gc.word AS grouping_code,
    gc.obm_id as grouping_code_id,
    gc.obm_geometry AS grouping_code_geometry,
    p.active as active,
    p.points_on_field
   FROM elc_search as m
     LEFT JOIN elc_search p ON m.obm_id = p.parent_id
     LEFT JOIN elc_search pr ON (p.word = pr.word OR p.word LIKE pr.word || ':%' OR p.word LIKE 'str_' || REPLACE(UNACCENT(LOWER(pr.word)),' ', '_') || '_%') AND pr.subject = 'projekt'
     LEFT JOIN elc_search gc ON p.obm_id = gc.parent_id
  WHERE 
    m.subject::text = 'method'::text AND 
    p.subject::text = 'program'::text AND 
    gc.subject::text = 'grouping_code'::text
) as foo
GROUP BY method, projekt, program, program_id, grouping_code, grouping_code_id, active, points_on_field;
ALTER MATERIALIZED VIEW elc_grouping_codes OWNER TO elc_admin;
