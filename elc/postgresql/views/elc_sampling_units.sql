DROP MATERIALIZED VIEW elc_sampling_units CASCADE;
CREATE MATERIALIZED VIEW elc_sampling_units AS
SELECT ROW_NUMBER() OVER () uid, method, projekt, program, program_id, grouping_code, grouping_code_id, sampling_unit, sampling_unit_id, sampling_unit_geometry, altitude, corine_code, habitat, active FROM (
SELECT
    m.word AS method,
    pr.word AS projekt,
    p.word AS program,
    p.obm_id as program_id, 
    gc.word AS grouping_code,
    gc.obm_id as grouping_code_id, 
    su.word AS sampling_unit,
    su.obm_id as sampling_unit_id, 
    su.obm_geometry AS sampling_unit_geometry,
    su.altitude as altitude,
    su.corine_code as corine_code,
    su.habitat as habitat,
    p.active
   FROM elc_search as m
     LEFT JOIN elc_search p ON m.obm_id = p.parent_id
     LEFT JOIN elc_search pr ON (p.word = pr.word OR p.word LIKE pr.word || ':%' OR p.word LIKE 'str_' || REPLACE(UNACCENT(LOWER(pr.word)),' ', '_') || '_%') AND pr.subject = 'projekt'
     LEFT JOIN elc_search gc ON p.obm_id = gc.parent_id
     LEFT JOIN elc_search su ON gc.obm_id = su.parent_id
  WHERE 
    m.subject::text = 'method'::text AND 
    p.subject::text = 'program'::text AND 
    gc.subject::text = 'grouping_code'::text AND 
    su.subject::text = 'sampling_unit'::text) as foo
  GROUP BY  method, projekt, program, program_id, grouping_code, grouping_code_id, sampling_unit, sampling_unit_id, sampling_unit_geometry, altitude, corine_code, habitat, active;

ALTER MATERIALIZED VIEW elc_sampling_units OWNER TO elc_admin;
GRANT SELECT ON elc_sampling_units TO mihhok_gmail_com;
