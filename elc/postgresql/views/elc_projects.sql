DROP MATERIALIZED VIEW elc_projects;
CREATE MATERIALIZED VIEW elc_projects AS
SELECT
    ROW_NUMBER() OVER () uid,
    word AS project,
    obm_id as project_id,
    active AS active
FROM elc_search 
WHERE 
    data_table = 'elc' AND
    subject= 'projekt';

ALTER MATERIALIZED VIEW elc_projects OWNER TO elc_admin;
