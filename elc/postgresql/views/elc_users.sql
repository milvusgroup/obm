CREATE OR REPLACE VIEW elc_users AS SELECT id, role_id, username as name, email FROM users WHERE project_table = 'elc';
GRANT SELECT ON elc_users TO elc_admin;
GRANT SELECT ON elc_users TO mihhok_gmail_com;
