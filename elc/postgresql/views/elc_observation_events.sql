DROP MATERIALIZED VIEW elc_observation_events;
CREATE MATERIALIZED VIEW elc_observation_events AS
SELECT 
    row_number() Over() as uid,
    su.projekt, su.program, su.grouping_code, su.sampling_unit, su.sampling_unit_geometry as su_geom_orig, su.method,
    e.date, e.observers, e.time_of_start, e.time_of_end, e.duration,
    count(*) as number_of_observations,
    count(DISTINCT species_valid) as number_of_species,
    ST_Centroid(ST_Collect(surveyed_geometry)) as geom_real,
    round(ST_Distance(ST_Transform(sampling_unit_geometry, 3844), ST_Transform(ST_Centroid(ST_Collect(surveyed_geometry)), 3844))::numeric,2) as distance_to_orig_point
FROM elc_sampling_units su
LEFT JOIN elc e ON su.projekt = e.projekt AND su.program = e.program AND su.grouping_code = e.grouping_code AND su.sampling_unit = e.sampling_unit AND su.method = e.method
WHERE 
    su.projekt IN ('Nera-Beușnița', 'Domogled Cerna', 'Buila-Vânturarița', 'Nagyhagymás', 'Jiu-Dunărea') AND
    e.sampling_unit IS NOT NULL
GROUP BY su.projekt, su.program, su.grouping_code, su.sampling_unit, su.sampling_unit_geometry, su.method, e.date, e.observers, e.time_of_start, e.time_of_end, e.duration;


ALTER MATERIALIZED VIEW elc_observation_events OWNER TO elc_admin;
grant SELECT on elc_observation_events to mihhok_gmail_com ;
