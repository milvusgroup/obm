-- elc_sampling_units original
--CREATE OR REPLACE VIEW elc_sampling_units AS
--SELECT su.obm_id,
--    m.word AS method,
--    p.word AS program,
--    gc.word AS grouping_code,
--    gc.obm_geometry AS grouping_code_geometry,
--    su.word AS sampling_unit,
--    su.obm_geometry AS sampling_unit_geometry
--   FROM (((elc_search m
--     LEFT JOIN elc_search p ON ((m.obm_id = p.parent_id)))
--     LEFT JOIN elc_search gc ON ((p.obm_id = gc.parent_id)))
--     LEFT JOIN elc_search su ON ((gc.obm_id = su.parent_id)))
--  WHERE (((((m.subject)::text = 'method'::text) AND ((p.subject)::text = 'program'::text)) AND ((gc.subject)::text = 'grouping_code'::text)) AND ((su.subject)::text = 'sampling_unit'::text));

CREATE OR REPLACE VIEW elc_megfigyelopontok AS
SELECT ROW_NUMBER() OVER () uid, method, program, grouping_code, sampling_unit, sampling_unit_geometry, altitude, corine_code, habitat, datum, count(*) as data_count FROM (
SELECT
    mp.method,
    mp.program,
    mp.grouping_code,
    mp.sampling_unit,
    mp.sampling_unit_geometry,
    mp.altitude,
    mp.corine_code,
    mp.habitat,
    e.date as datum
   FROM elc_sampling_units as mp
     LEFT JOIN elc e ON (e.method = mp.method AND e.program = mp.program AND e.grouping_code = mp.grouping_code AND e.sampling_unit = mp.sampling_unit)
  ) as foo
  GROUP BY method, program, grouping_code, sampling_unit, sampling_unit_geometry, altitude, corine_code, habitat, datum;

ALTER VIEW elc_megfigyelopontok OWNER TO elc_admin;
GRANT ALL ON elc_megfigyelopontok TO mihhok_gmail_com;
GRANT SELECT ON elc_megfigyelopontok TO fuciu_cata_yahoo_com;

