DROP VIEW elc_audiomoth;
CREATE OR REPLACE VIEW elc_audiomoth AS 
SELECT 
    e.obm_id, e.obm_uploading_id, e.obm_validation, e.obm_comments, e.obm_modifier_id, 
    e.start, e.duration, e.tag, e.observation_id, e.updated_at as tagged_at,
    r.filename, r.recorded_at, r.detection_file, r.priority, e.user_rid as listener, r.listening_finished,
    s.obm_id as session_id, s.name, s.path, s.obm_geometry,
    t.tag_category, t.species as is_species, t.breeding_status, t.gender
FROM elc_am_event e 
LEFT JOIN elc_am_tags t ON e.tag = t.tag
LEFT JOIN elc_am_record r ON e.record_id = r.obm_id
LEFT JOIN elc_am_session s ON r.session_id = s.obm_id;
--WHERE e.tag IS NOT NULL;

GRANT ALL ON elc_audiomoth TO elc_admin;
GRANT ALL ON elc_audiomoth TO mihhok_gmail_com;
