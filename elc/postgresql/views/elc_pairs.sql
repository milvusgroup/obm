DROP VIEW elc_pairs;
CREATE OR REPLACE VIEW elc_pairs AS 
SELECT DISTINCT
  p.id as obm_id,
  p.geometry as obm_geometry,
  0 as obm_uploading_id,
  0::numeric as obm_validation,
  0 as obm_modifier_id,
  project,
  pr.program,
  s.grouping_code,
  elc.sampling_unit,
  p.species,
  p.status::text as pair_status,
  min::text as min,
  max::text as max
FROM elc.raptor_pair_observation po
left join elc.raptor_pair p ON po.pair_id = p.id
LEFT JOIN elc ON elc.obm_id = po.obm_id 
LEFT JOIN elc.raptor_survey_session s ON p.session_id = s.id
LEFT JOIN elc.raptor_period pr ON pr.id = s.period_id;

GRANT ALL ON elc_pairs TO elc_admin;
GRANT ALL ON elc_pairs TO mihhok_gmail_com;
