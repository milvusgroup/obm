DROP MATERIALIZED VIEW elc_programs;
CREATE MATERIALIZED VIEW elc_programs AS
SELECT ROW_NUMBER() OVER () uid, method, projekt, program, program_id, active FROM (
    SELECT
        m.word AS method,
        pr.word as projekt,
        p.word AS program,
        p.obm_id as program_id,
        p.active AS active
     FROM elc_search as m
     LEFT JOIN elc_search p ON m.obm_id = p.parent_id
     LEFT JOIN elc_search pr ON (p.word = pr.word OR p.word LIKE pr.word || ':%' OR p.word LIKE 'str_' || REPLACE(UNACCENT(LOWER(pr.word)),' ', '_') || '_%') AND pr.subject = 'projekt'
     WHERE 
        m.subject::text = 'method'::text AND 
        p.subject::text = 'program'::text 
    ) as foo
GROUP BY  method, projekt, program, program_id, active;

ALTER MATERIALIZED VIEW elc_programs OWNER TO elc_admin;
