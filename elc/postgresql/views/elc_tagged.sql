CREATE OR REPLACE VIEW elc_tagged AS
WITH 
tagged as (
SELECT 
    e.obm_id, e.start, e.duration, e.tag, 
    r.filename, r.recorded_at, r.detection_file, r.listener, r.listening_finished, r.priority,
    s.name as device_name,
    t.tag_category, t.species as is_species,
    CASE WHEN substr(t.breeding_status, 1, 3) = 'A.2' THEN true else false END AS is_song,
    CASE WHEN substr(t.breeding_status, 1, 3) = 'B.7' THEN true else false END AS is_alarm,
    t.gender
FROM elc_am_event e
LEFT JOIN elc_am_record r ON r.obm_id = e.record_id
LEFT JOIN elc_am_session s ON s.obm_id = r.session_id
LEFT JOIN elc_am_tags t ON e.tag = t.tag
WHERE e.tag IS NOT NULL
ORDER BY name, filename, start
),
d as (SELECT DISTINCT device_name FROM tagged),
recs as (SELECT DISTINCT device_name, filename FROM tagged)
SELECT json_agg(json_build_object(
    'device_name', d.device_name,
    'records', r.records
    )) devices
FROM d
LEFT JOIN (SELECT 
    recs.device_name, 
    json_agg(
        json_build_object(
            'filename', recs.filename,
            'events', e.events
            )
        ) as records 
FROM recs 
LEFT JOIN (
    SELECT device_name, filename, json_agg(json_build_object(
        'start', start,
        'duration', duration,
        'tag_raw', tag,
        'tag', tag_category,
        'is_species', is_species,
        'is_song', is_song,
        'is_alarm', is_alarm,
        'gender', gender
        )) events
    FROM tagged
    GROUP By device_name, filename
    ) e ON recs.device_name = e.device_name AND recs.filename = e.filename
GROUP BY recs.device_name) r ON d.device_name = r.device_name;

