DROP MATERIALIZED VIEW elc_threats_mv;
CREATE MATERIALIZED VIEW elc_threats_mv AS
SELECT ROW_NUMBER() OVER () uid, impact_l1, impact_l2, impact_l3, impact_l4 FROM (
    SELECT
        l1.word AS impact_l1,
        l2.word AS impact_l2,
        l3.word AS impact_l3,
        l4.word AS impact_l4
       FROM elc_search as l1
       LEFT JOIN elc_search l2 ON l1.obm_id = l2.parent_id AND l2.subject::text = 'impact_l2'::text 
       LEFT JOIN elc_search l3 ON l2.obm_id = l3.parent_id AND l3.subject::text = 'impact_l3'::text
       LEFT JOIN elc_search l4 ON l3.obm_id = l4.parent_id AND l4.subject::text = 'impact_l4'::text
      WHERE 
        l1.subject::text = 'impact_l1'::text
) as foo;

ALTER MATERIALIZED VIEW elc_threats_mv OWNER TO elc_admin;

DROP MATERIALIZED VIEW elc_threats_mv;
CREATE MATERIALIZED VIEW elc_threats_mv AS
    SELECT
        l1.word AS impact_l1,
        l2.word AS impact_l2
      FROM elc_search as l1
      FULL OUTER JOIN elc_search AS l2 ON l2.subject = 'impact_l2' AND l2.parent_id = l1.obm_id
      WHERE 
        l1.subject::text = 'impact_l1'::text;
