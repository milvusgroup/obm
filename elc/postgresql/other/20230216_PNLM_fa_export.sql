-- UPDATE elc SET grouping_code = 'str_old_cavity_tree' WHERE projekt = 'PN Lunca Muresului' AND program = 'str_pn_lunca_muresului_tree_marking' AND grouping_code = 'öreg fa';
select distinct 
    obm_id,
    case grouping_code
        when 'str_tree_with_nest' THEN 'Arbore cu cuib'
        WHEN 'str_old_cavity_tree' THEN 'Arbore cu scorbură/lemn mort'
    END as arbore,
    st_x(obm_geometry) as lon,
    st_y(obm_geometry) as lat,
    layer as OcolSilvic,
    up,
    ua,
    coalesce(ta, fvarsta, left(ta_cns_clp, 2)::numeric)::integer as varsta,
    species
from elc 
left join elc.pnlmn_fond_forestier ON ST_intersects(obm_geometry, st_transform(geometry, 4326))
where 
    projekt = 'PN Lunca Muresului' AND 
    program = 'str_pn_lunca_muresului_tree_marking' AND
    grouping_code IN ('str_old_cavity_tree', 'str_tree_with_nest');

\copy (select distinct obm_id, grouping_code, st_x(obm_geometry) as lon, st_y(obm_geometry) as lat, layer as OcolSilvic, up, ua, coalesce(ta, fvarsta, left(ta_cns_clp, 2)::numeric)::integer as varsta, species from elc left join elc.pnlmn_fond_forestier ON ST_intersects(obm_geometry, st_transform(geometry, 4326)) where projekt = 'PN Lunca Muresului' AND program = 'str_pn_lunca_muresului_tree_marking' AND grouping_code IN ('str_old_cavity_tree', 'str_tree_with_nest')) to /home/gabor/Documents/motolla/ecologic/PNLM/pnlms_arbori.csv CSV HEADER;



select distinct 
    obm_id,
    nest_name,
    built_by_species as species,
    st_x(obm_geometry) as lon,
    st_y(obm_geometry) as lat,
    layer as OcolSilvic,
    up,
    ua,
    coalesce(ta, fvarsta, left(ta_cns_clp, 2)::numeric)::integer as varsta
from elc_nests
left join elc.pnlmn_fond_forestier ON ST_intersects(obm_geometry, st_transform(geometry, 4326))
where 
    projekt = 'PN Lunca Muresului';

\copy (select distinct obm_id, nest_name, built_by_species as species, st_x(obm_geometry) as lon, st_y(obm_geometry) as lat, layer as OcolSilvic, up, ua, coalesce(ta, fvarsta, left(ta_cns_clp, 2)::numeric)::integer as varsta from elc_nests left join elc.pnlmn_fond_forestier ON ST_intersects(obm_geometry, st_transform(geometry, 4326)) where projekt = 'PN Lunca Muresului') to /home/gabor/Documents/motolla/ecologic/PNLM/pnlms_cuiburi.csv CSV HEADER;



SELECT DISTINCT grouping_code, date, count(*)
from elc 
--left join elc.pnlmn_fond_forestier ON ST_intersects(obm_geometry, st_transform(geometry, 4326))
where 
    date > '2023-10-01' AND
    projekt = 'PN Lunca Muresului' AND 
    program = 'str_pn_lunca_muresului_tree_marking'
GROUP BY grouping_code, date;



select distinct 
    obm_id,
    case grouping_code
        when 'str_nest_placuta' THEN 'Arbore cu cuib'
        WHEN 'str_tz_white_paint' THEN 'Arbore TZ'
    END as arbore,
    st_x(obm_geometry) as lon,
    st_y(obm_geometry) as lat,
    layer as OcolSilvic,
    up,
    ua,
    coalesce(ta, fvarsta, left(ta_cns_clp, 2)::numeric)::integer as varsta,
    species
from elc 
left join elc.pnlmn_fond_forestier ON ST_intersects(obm_geometry, st_transform(geometry, 4326))
where 
    projekt = 'PN Lunca Muresului' AND 
    program = 'str_pn_lunca_muresului_tree_marking' AND
    date > '2023-10-15';



COPY (WITH d as (
SELECT 
    obm_id,
    file_id, 
    case grouping_code
        when 'str_nest_placuta' THEN 'arb_cu_placuta'
        when 'str_old_cavity_tree' THEN 'arb_cu_scorbura'
        when 'str_tampon_zone' THEN 'tz'
        when 'str_tree_with_nest' THEN 'arb_cu_cuib'
        when 'str_tz_white_paint' THEN 'tz'
    as gc,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM elc 
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    projekt = 'PN Lunca Muresului' AND 
    program = 'str_pn_lunca_muresului_tree_marking' AND
    obm_files_id IS NOT NULL 
)
SELECT 'cp /home/gabor/obm/obm-composer/local/elc/local/attached_files/' || reference || ' /tmp/elc_lm_tz/' || gc || '_' || obm_id || '_E' || x || '_N' || y || '.jpg' FROM d)
TO '/tmp/tampon_zona_masolas.sh' CSV;

SELECT st_astext(obm_geometry) as geom, species_valid as specia, number as numar, observed_unit as unitate, gender as sex, age as varsta, date as data, exact_time as timpul_exact, status_of_individuals as statut_cuibarire, observers as observatori, method as metodologia, program as protocol, grouping_code as cod_esantion_centralizator, sampling_unit as cod_esantion, lpad((time_of_start / 60)::varchar, 2, '0') || ':' || lpad((time_of_start % 60)::varchar, 2, '0') as timpul_inceperii, lpad((time_of_end / 60)::varchar, 2, '0') || ':' || lpad((time_of_end % 60)::varchar, 2, '0') as timpul_terminarii, lpad((duration / 60)::varchar, 2, '0') || ':' || lpad((duration % 60)::varchar, 2, '0') as durata, complete_list as lista_completa, wind_force as vant, cloud_cover as nebulozitate, precipitation as precipitatie, visibility as vizibilitate, distance_m as distanta, timing_of_reaction as timpul_reactiei, response_type as raspuns, st_astext(surveyed_geometry) as pozitia_observatorului FROM elc WHERE projekt = 'PN Lunca Muresului';
