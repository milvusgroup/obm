-- felelősök hozzáadáasa
WITH ss as (SELECT 1 period_id, grouping_code, ("data"->>'felelos')::integer as responsible from elc_metadata where dtype = 'administration')
INSERT INTO elc.raptor_survey_session (period_id, grouping_code, responsible) SELECT period_id, grouping_code, responsible FROM ss WHERE responsible IS NOT NULL;

-- felmerok hozzáadáasa
WITH obs as (
    SELECT 
        s.grouping_code,
        s.id as session_id,
        "data"->>'felelos' as responsible, 
        jsonb_array_elements_text("data"->'felmerok')::integer as felmero 
    FROM elc_metadata md
    LEFT JOIN elc.raptor_survey_session s ON s.grouping_code = md.grouping_code
    WHERE dtype = 'administration'
)
INSERT INTO elc.raptor_observer (session_id, observer) SELECT session_id, felmero FROM obs;

-- párok hozzáadáasa
INSERT INTO elc.raptor_pair (session_id, sampling_unit, species, min, max, status, geometry, oldid)
SELECT 
    s.id as session_id,
    md.sampling_unit,
    md.species,
    ("data"->>'min')::integer as min,
    ("data"->>'max')::integer as max,
    "data"->>'pair_status' as status,
    md.obm_geometry,
    md.obm_id as oldid
FROM elc_metadata md
LEFT JOIN elc.raptor_survey_session s ON s.grouping_code = md.grouping_code
WHERE dtype = 'pair_connect';

-- párok - megfigyelések
INSERT INTO elc.raptor_pair_observation (pair_id, obm_id)
SELECT 
    p.id as pair_id,
    jsonb_array_elements_text("data"->'obm_ids')::integer as obm_id
FROM elc_metadata md
LEFT JOIN elc.raptor_pair p ON p.oldid = md.obm_id
WHERE dtype = 'pair_connect';

-- duplák hozzáadáasa
INSERT INTO elc.raptor_double (session_id, min, max, status, oldid)
SELECT
    s.id as session_id,
    ("data"->>'min_pairs')::integer as min,
    ("data"->>'max_pairs')::integer as max,
    "data"->>'double_status' as status,
    obm_id as oldid
FROM elc_metadata md
LEFT JOIN elc.raptor_survey_session s ON s.grouping_code = md.grouping_code
WHERE dtype = 'double_pairs';

-- duplák - párok 
INSERT INTO elc.raptor_double_pair (double_id, pair_id)
SELECT 
    double_id,
    p.id as pair_id
FROM
(SELECT 
    d.id as double_id,
    jsonb_array_elements_text("data"->'obm_ids')::integer as pair_id
FROM elc_metadata md
LEFT JOIN elc.raptor_double d ON d.oldid = md.obm_id
WHERE dtype = 'double_pairs') foo
LEFT JOIN elc.raptor_pair p ON p.oldid = foo.pair_id;

-- square finished
WITH sf as (
    select grouping_code, "data"->>'comment' as cmnt from elc_metadata WHERE dtype = 'square_finished'
    )
UPDATE elc.raptor_survey_session ss SET finished = true, cmnt = sf.cmnt FROM sf WHERE ss.grouping_code = sf.grouping_code;
