-- https://trello.com/c/N5DkOYJR
SELECT DISTINCT program, grouping_code FROM elc WHERE date = '2021-10-12';
BEGIN;
UPDATE elc SET 
    method = 'monitorizare migrație de păsări cu zbor planat',
    program = 'Dobrogea de Nord: Monitorizarea migrației a păsăriilor cu zbor planat',
    grouping_code = 'Puncte de observatie - rapitoare migratoare'
WHERE date = '2021-10-12' AND program = 'Dobrogea de Nord: Monitorizarea păsărilor rapitoare';

-- https://trello.com/c/RdgSGH2j
DELETE FROM elc WHERE obm_id = 70393;

-- https://trello.com/c/1HaWMipo
DELETE FROM elc WHERE obm_id = 70356;

