WITH times AS (  
    SELECT id, (to_timestamp((metadata->>'finished_at')::bigint/1000) + '1 hour')::time as extime FROM system.uploadings u LEFT JOIN elc e ON e.obm_uploading_id = u.id WHERE e.method = 'str_audiomoth_calibration' AND u.id is not null
)
SELECT exact_time, extime FROM elc LEFT JOIN times ON times.id = obm_uploading_id WHERE obm_uploading_id IS NOT NULL AND method = 'str_audiomoth_calibration';
--UPDATE elc SET exact_time = extime FROM times WHERE times.id = obm_uploading_id AND obm_uploading_id IS NOT NULL AND method = 'str_audiomoth_calibration';
