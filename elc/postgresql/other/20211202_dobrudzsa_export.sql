SELECT count(*) FROM elc WHERE projekt = 'Dobrogea de Nord';
DELETE FROM elc WHERE obm_id IN (87362, 88827, 70395, 70394, 73756);

COPY (SELECT st_astext(obm_geometry) as wkt, obm_id, program, grouping_code, sampling_unit, method, method_details_species_aimed,
    date, make_time(time_of_start/60, mod(time_of_start, 60), 0) as time_of_start,
    make_time(time_of_end/60, mod(time_of_end, 60), 0) as time_of_end, duration, 
    complete_list, observers, disturbance, wind_force, cloud_cover, precipitation, 
    temperature, visibility, atmospheric_pressure, comments_general, altitude, 
    exact_time, observed_unit, species_valid, species_ro, number, gender, age,
    count_precision, status_of_individuals, distance_code, timing_of_reaction,
    response_type, flight_direction, flight_altitude, migration_type, migration_intensity,
    comments_observation, corine_code, habitat
FROM elc WHERE projekt = 'Dobrogea de Nord')
TO '/tmp/elc_dobrogea_date.csv' CSV HEADER;

-- scp boneg@db:/tmp/elc_dobrogea_date.csv Documents/motolla/ecologic/dobrudzsa/raport_date/
-- ogr2ogr -a_srs EPSG:4326 -f \"ESRI Shapefile\" elc_dobrogea_date.shp elc_dobrogea_date.csv -lco ENCODING=UTF-8


COPY (select st_astext(obm_geometry) as wkt, obm_id, date, site, observers, impact_type, 
    impact, impact_custom, impact_intensity, time, observations, impact_l1, 
    impact_l2, impact_l3, impact_l4, project FROM elc_threats
WHERE project = 'Dobrogea de Nord')
TO '/tmp/elc_dobrogea_presiuni.csv' CSV HEADER;

COPY (SELECT program, grouping_code, sampling_unit, st_astext(sampling_unit_geometry) as wkt, altitude, corine_code, habitat FROM elc_sampling_units WHERE program LIKE  'Dobrogea de Nord%')
TO '/tmp/elc_dobrogea_puncte_de_observatie.csv' CSV HEADER;

select column_name from information_schema.columns where table_name = 'elc_threats';
    
SELECT make_time(615/60, MOD(615, 60), 0);