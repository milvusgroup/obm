-- gc
SELECT * FROM elc_search WHERE parent_id = 6347;
UPDATE elc_search SET word = 'Puncte de observatie - rapitoare' WHERE obm_id = 6348;
UPDATE elc_search SET parent_id = 6348 WHERE parent_id IN (SELECT obm_id FROM elc_search WHERE parent_id = 6347);
DELETE FROM elc_search WHERE obm_id != 6348 AND parent_id = 6347;

SELECT * FROM elc_search WHERE parent_id = 4813;
UPDATE elc_search SET word = 'Puncte de observatie - rapitoare migratoare' WHERE obm_id = 4814;
UPDATE elc_search SET parent_id = 4814 WHERE parent_id IN (SELECT obm_id FROM elc_search WHERE parent_id = 4813);
DELETE FROM elc_search WHERE obm_id != 4814 AND parent_id = 4813;


-- adatok
UPDATE elc SET grouping_code = 'Puncte de observatie - rapitoare' WHERE program = 'Dobrogea de Nord: Monitorizarea păsărilor rapitoare';

UPDATE elc SET grouping_code = 'Puncte de observatie - rapitoare migratoare' WHERE program = 'Dobrogea de Nord: Monitorizarea migrației a păsăriilor cu zbor planat';

select from elc_search WHERE word = 'Rp01';


-- valamiért a gykódokat kitöröltük, most meg miután megvolt a felmérés az eredmények számolása miatt vissza kell tenni
-- máskor azt is le kell dokumentálni, hogy mit miért csinálunk
INSERT INTO elc_search (status, subject, word, parent_id) SELECT 0 as status, 'grouping_code' as subject, word, 6347 as parent_id  FROM elc_search WHERE parent_id = 4338;
UPDATE elc_search SET parent_id = 6422 WHERE obm_id = 6355; --RAP-Cocos
UPDATE elc_search SET parent_id = 6421 WHERE obm_id = 6381; --rap_consul1
UPDATE elc_search SET parent_id = 6421 WHERE obm_id = 6379; --RAP-Consul2
UPDATE elc_search SET parent_id = 6421 WHERE obm_id = 6382; --rap_consul3
UPDATE elc_search SET parent_id = 6421 WHERE obm_id = 6380; --RAP-Consul4
UPDATE elc_search SET parent_id = 6419 WHERE obm_id = 6374; --rap_ct1
UPDATE elc_search SET parent_id = 6419 WHERE obm_id = 6377; --RAP-CT2
UPDATE elc_search SET parent_id = 6419 WHERE obm_id = 6375; --rap_ct3
UPDATE elc_search SET parent_id = 6419 WHERE obm_id = 6376; --RAP-CT4
UPDATE elc_search SET parent_id = 6419 WHERE obm_id = 6378; --RAP-CT4
UPDATE elc_search SET parent_id = 6420 WHERE obm_id = 6358; --rap_sarica1
UPDATE elc_search SET parent_id = 6420 WHERE obm_id = 6356; --RAP-Sarika2
UPDATE elc_search SET parent_id = 6420 WHERE obm_id = 6361; --rap_sarica3
UPDATE elc_search SET parent_id = 6420 WHERE obm_id = 6370; --RAP-Sarika4
UPDATE elc_search SET parent_id = 6418 WHERE obm_id = 6360; --rap_edirlen1
UPDATE elc_search SET parent_id = 6418 WHERE obm_id = 6362; --RAP-Edirlen2
UPDATE elc_search SET parent_id = 6418 WHERE obm_id = 6363; --RAP-Edirlen2b
UPDATE elc_search SET parent_id = 6423 WHERE obm_id = 6359; --rap_mandresti
UPDATE elc_search SET parent_id = 6423 WHERE obm_id = 6357; --rap_mandresti

SELECT grouping_code, sampling_unit from elc_sampling_units WHERE program = 'Dobrogea de Nord: Monitorizarea păsărilor rapitoare';
BEGIN;
UPDATE elc e SET grouping_code = s.grouping_code FROM elc_sampling_units s WHERE e.program = 'Dobrogea de Nord: Monitorizarea păsărilor rapitoare' AND e.sampling_unit = s.sampling_unit;
SELECT grouping_code, sampling_unit from elc WHERE program = 'Dobrogea de Nord: Monitorizarea păsărilor rapitoare';

INSERT INTO elc.raptor_survey_session (period_id, grouping_code, responsible, finished) SELECT 2, word, 62, 'FALSE' FROM elc_search WHERE parent_id = 4338;

WITH ids as (
    SELECT p.id as pid,  s.id as sid
    from elc.raptor_pair_observation po
    LEFT JOIN elc.raptor_pair p on p.id = po.pair_id
    left join elc e on po.obm_id = e.obm_id 
    left join elc.raptor_survey_session s on e.grouping_code = s.grouping_code
    where p.session_id = 3
    )
UPDATE elc.raptor_pair SET session_id = sid from ids WHERE id = pid;


WITH ids as (
SELECT d.id did, p.session_id psid FROM elc.raptor_double d 
LEFT JOIN elc.raptor_double_pair dp ON d.id = dp.double_id
LEFT JOIN elc.raptor_pair p ON p.id = dp.pair_id
WHERE d.session_id != 2
)
UPDATE elc.raptor_double SET session_id = psid FROM ids WHERE id = did;