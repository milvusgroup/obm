SELECT species, word FROM tmp.efective_jiu LEFT JOIN elc_taxon ON species = word WHERE lang = 'species_valid' AND status = 'accepted' AND word IS NULL;

SELECT site, species, specia, count(*) from tmp.efective_jiu e LEFT JOIN elc_raportare_dist_specii_pg r ON species = specia AND site = codul_sitului WHERE site = 'ROSPA0010' AND projekt = 'Jiu-Dunărea' GROUP BY species, specia, site HAVING count(*) > 1 ORDER BY specia;

SELECT site, species, specia, r.tipul_populatiei, r.alte_detalii, count(*) from tmp.efective_jiu e LEFT JOIN elc_raportare_dist_specii_pg r ON species = specia AND site = codul_sitului AND e.alte_detalii IS NOT DISTINCT FROM r.alte_detalii AND e.tipul_populatiei IS NOT DISTINCT FROM r.tipul_populatiei AND projekt = 'Jiu-Dunărea' GROUP BY species, specia, site, r.tipul_populatiei, r.alte_detalii ORDER BY specia;
UPDATE elc_raportare_dist_specii_pg r SET popmin = e.popmin, popmax = e.popmax, um_tip_populatie = e.um_tip_populatie FROM tmp.efective_jiu e WHERE species = specia AND site = codul_sitului AND e.alte_detalii IS NOT DISTINCT FROM r.alte_detalii AND e.tipul_populatiei IS NOT DISTINCT FROM r.tipul_populatiei AND projekt = 'Jiu-Dunărea';
