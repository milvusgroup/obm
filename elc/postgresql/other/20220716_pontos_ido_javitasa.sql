SELECT obm_observation_list_id, time_of_start / 60 || ':' || time_of_start % 60, time_of_end / 60 || ':' || time_of_end % 60, exact_time, to_timestamp((metadata->>'finished_at')::bigint/1000)::time as finished_at FROM elc LEFT JOIN system.uploadings u ON u.id = elc.obm_uploading_id WHERE metadata->>'app_version' = 'OBM_mobile-r4_1.9.12-2' AND obm_observation_list_id IS NULL ORDER BY obm_observation_list_id, finished_at;

SELECT obm_observation_list_id, time_of_start / 60 || ':' || time_of_start % 60, time_of_end / 60 || ':' || time_of_end % 60, exact_time, to_timestamp((metadata->>'finished_at')::bigint/1000)::time as finished_at FROM milvus LEFT JOIN system.uploadings u ON u.id = milvus.obm_uploading_id WHERE metadata->>'app_version' = 'OBM_mobile-r4_1.9.12-2' AND obm_observation_list_id IS NULL AND uploader_id = 31 ORDER BY id desc limit 1;

BEGIN;
UPDATE elc SET exact_time = to_timestamp((metadata->>'finished_at')::bigint/1000)::time + interval '1 hour' FROM system.uploadings u WHERE u.id = elc.obm_uploading_id AND metadata->>'app_version' = 'OBM_mobile-r4_1.9.12-2' AND obm_observation_list_id IS NOT NULL;
ROLLBACK;

with x as (
    SELECT exact_time, date_trunc('minute', to_timestamp((metadata->>'finished_at')::bigint/1000)::time) + interval '1 hour' as finished_at FROM elc LEFT JOIN system.uploadings u ON u.id = elc.obm_uploading_id WHERE metadata->>'app_version' = 'OBM_mobile-r4_1.9.12-2' AND obm_observation_list_id IS NULL ORDER BY obm_observation_list_id, finished_at
)
SELECT exact_time, finished_at, (exact_time = finished_at) from x WHERE exact_time != finished_at;
