BEGIN;
    UPDATE elc e SET distance_m = ROUND(ST_Distance(ST_Transform(obm_geometry, 31700), ST_Transform(sampling_unit_geometry, 31700))::numeric, 2)
    FROM elc_sampling_units su 
    WHERE (e.program, e.grouping_code, e.sampling_unit) = (su.program, su.grouping_code, su.sampling_unit) AND
        e.program IS NOT NULL AND e.grouping_code IS NOT NULL AND e.sampling_unit IS NOT NULL AND
        e.obm_geometry IS NOT NULL AND su. sampling_unit_geometry IS NOT NULL AND e.distance_m IS NULL;
COMMIT;

UPDATE elc e SET distance_m = ROUND(ST_Distance(ST_Transform(obm_geometry, 31700), ST_Transform(sampling_unit_geometry, 31700))::numeric, 2) FROM elc_sampling_units su WHERE (e.program, e.grouping_code, e.sampling_unit) = (su.program, su.grouping_code, su.sampling_unit) AND e.program IS NOT NULL AND e.grouping_code IS NOT NULL AND e.sampling_unit IS NOT NULL AND e.obm_geometry IS NOT NULL AND su. sampling_unit_geometry IS NOT NULL AND e.distance_m IS NULL;
