COPY (WITH d as (
SELECT 
    obm_id,
    file_id, 
    nest_name, 
    reference
FROM elc_nests
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    nest_name IN ('KA_LM_01', 'KA_LM_03', 'KA_LM_04', 'KA_LM_05', 'KA_LM_06', 'LM_ZR_02', 'LM_ZR_03', 'LM_ZR_04', 'LM_ZR_05', 'LM_ZR_06', 'LM_ZR_07', 'LM_ZR_08', 'LM_ZR_09', 'LM_ZR_11', 'LM_ZR_12', 'PNLM_HZs_01', 'PNLM_HZs_02', 'PNLM_HZs_03', 'PNLM_HZs_04', 'PNLM_HZs_06', 'PNLM_PO_01', 'PNLM_PO_03', 'PNLM-IG-01-130122', 'PNLM-IG-04-140122', 'PNLM-IG-05-150122', 'PNLM-IG-06-150122', 'PNLM-IG-07-150122', 'PNLMk1') AND
    obm_files_id IS NOT NULL 
)
SELECT 'scp app:/home/gabor/obm/obm-composer/local/elc/local/attached_files/' || reference || ' /tmp/pnlm_hianyzo_kepek/' || nest_name || '_' || obm_id || '.jpg' FROM d)
TO '/tmp/pnlm_hianyzo_kepek.sh' CSV;


SELECT distinct nest_name, nest_id FROM elc e LEFT JOIN elc_nests n ON n.projekt = e.projekt AND nest_id = nest_name WHERE e.projekt LIKE 'PN Lunca Muresului';
