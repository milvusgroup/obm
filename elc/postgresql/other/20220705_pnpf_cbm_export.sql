SELECT date, to_char((time_of_start || ' minute')::interval, 'HH24:MI:SS') as time_of_start, exact_time, species_valid as species, grouping_code, sampling_unit, distance_m, st_x(obm_geometry) as x, st_y(obm_geometry) as y, observers, wind_force
FROM elc 
WHERE program = 'PN Porțile de Fier: Monitorizarea păsărilor comune' ORDER BY date, exact_time;

\copy (SELECT date, to_char((time_of_start || ' minute')::interval, 'HH24:MI:SS') as time_of_start, exact_time, species_valid as species, grouping_code, sampling_unit, distance_m, st_x(obm_geometry) as x, st_y(obm_geometry) as y, observers, wind_force FROM elc WHERE program = 'PN Porțile de Fier: Monitorizarea păsărilor comune') to '/tmp/pnpf_cbm.csv' CSV HEADER;

