-- A nagytudású a ragadozó űrlapban listát indított, de csak a ragadozókat és hollót írta be, minden mást külön alkalmi megfigyelésbe, lista mentéskor viszont azt írta, hogy teljes lista.
-- Állítólag ezt mindig mindenhol így cinálta, pedig mindig elmondtmam, minden módszerbe leírtam.
-- Azontúl, hogy agyonütöm s elásom, tudsz erre egy ellenőrző/javító szkirptet írni minden általa felvett adatra az idők kezdetétől? Mittudomén, ha van egy teljes listája, és van a start-stop időszak között más bevitt adata, akkor azt is tegye be az adott listába (csak a madarat persze, élőhelyet, fészket, veszélyeztetőt nem)

SELECT distinct obm_observation_list_id, date, time_of_start, time_of_end, program, count(*) from elc e left join system.uploadings u ON e.obm_uploading_id = u.id where uploader_name = 'Kiss Arnold' AND obm_observation_list_id IS NOT NULL AND program LIKE '%rap%' GROUP BY obm_observation_list_id, date, time_of_start, time_of_end, program ORDER BY date, time_of_start;

SELECT obm_id, method, program, species_valid, complete_list, time_of_start, time_of_end, exact_time FROM elc e left join system.uploadings u ON e.obm_uploading_id = u.id where uploader_name = 'Kiss Arnold' AND date = '2022-06-23' ORDER BY exact_time;
SELECT string_agg(obm_id::varchar, ',') FROM elc e left join system.uploadings u ON e.obm_uploading_id = u.id where uploader_name = 'Kiss Arnold' AND date = '2022-06-23';
