SELECT FROM elc WHERE sampling_unit = 'CO3509_16';
DELETE FROM elc WHERE sampling_unit = 'CO3509_16';

-- 1	AP4120	1	Petrencu Laurentiu		Laurnak csatolva az adatai az első körből amiket Observadoba nyomott. Megjegyzésben minden szükséges adat
-- 19	AP4120	1	Laur		Observadoba mentek az adatok
BEGIN; 
UPDATE elc SET observers = 'Petrencu Laurențiu' where grouping_code = 'AP4120';

 
-- 3	AN4101	1	Kiss Arni	május 3	Az első 5 pontját alkalmi megfigyelésbe nyomta, mert nem ment még neki a CBM  űrlap. Elvileg minden pont egy külön teljes lista és megjegyzésben írja a pont nevét. Kérlek tedd át ezeket a CBM módszerhez.
SELECT FROM elc WHERE date = '2021-05-03' AND observers = 'Kiss Arnold' AND method = 'Observații ocazionale' AND comments_general IN ('08', '09', '10', '13', '18', '23');
begin;
UPDATE elc 
SET 
    method = 'str_general_point_count',
    program = 'PN Porțile de Fier: Monitorizarea păsărilor comune',
    grouping_code = 'AN4101',
    sampling_unit = 'AN4101_' || comments_general
WHERE
    date = '2021-05-03' AND observers = 'Kiss Arnold' AND method = 'Observații ocazionale' AND comments_general IN ('08', '09', '10', '13', '18', '23');
    
-- 6	AJ3917	1	Fuciu		látom a 01-es megfigyelőpontot, holott nincs adat sem a közelében, sem máshol. Mi lehet itt?
SELECT DISTINCT species FROM elc WHERE sampling_unit = 'AJ3917_01' AND observers = 'Catalin Fuciu';
DELETE FROM elc WHERE sampling_unit = 'AJ3917_01' AND observers = 'Catalin Fuciu';

-- 7	AK3919	1	fuciu		látom a 07-es megfigyelőpontot, holott nincs adat sem a közelében, sem máshol. Mi lehet itt?
SELECT DISTINCT species FROM elc WHERE sampling_unit = 'AK3919_07';
DELETE FROM elc WHERE sampling_unit = 'AK3919_07';

-- 8	AL4004	1	Arni		látom a 10-es megfigyelőpontot, holott nincs adat sem a közelében, sem máshol. Mi lehet itt?
SELECT DISTINCT species FROM elc WHERE sampling_unit = 'AL4004_10';
DELETE FROM elc WHERE sampling_unit = 'AL4004_10';

-- 18	obm id 75734 nagyon gyanús, törölni! (Birti mezei poszáta)		Birti		obm id 75734 nagyon gyanús, törölni! (Birti mezei poszáta)
DELETE FROM elc WHERE obm_id = 75734;

-- egy kis takaritas
UPDATE elc SET count_precision = 'str_rough_estimate' WHERE count_precision = 'estimare dură';
UPDATE elc SET count_precision = 'str_precise_estimate' WHERE count_precision = 'estimare precisă';
UPDATE elc SET count_precision = 'str_exact_count' WHERE count_precision = 'numărare exactă';
UPDATE elc SET count_precision = 'str_at_least_number' WHERE count_precision = 'cel puțin numărul';

UPDATE elc SET gender = 'str_male' WHERE gender = 'mascul';
UPDATE elc SET gender = 'str_female' WHERE gender = 'femelă';
UPDATE elc SET gender = 'str_pair' WHERE gender = 'pereche';
UPDATE elc SET gender = 'str_male_female' WHERE gender = 'ambele';

UPDATE elc SET age = 'str_pull' WHERE age = 'pui';
UPDATE elc SET age = 'str_juvenile' WHERE age = 'juvenil';
UPDATE elc SET age = 'str_immature' WHERE age = 'immatur';
UPDATE elc SET age = 'str_fully_grown' WHERE age = 'dezvoltat';
UPDATE elc SET age = 'str_subadult' WHERE age = 'subadult';
UPDATE elc SET age = NULL WHERE age IN ('unknown', 'nem ismert');
