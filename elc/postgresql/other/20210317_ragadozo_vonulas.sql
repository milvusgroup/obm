INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_N_S');
INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_NE_SW');
INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_E_W');
INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_SE_NW');
INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_S_N');
INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_SW_NE');
INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_W_E');
INSERT INTO elc_terms (data_table, subject, term) VALUES ('elc', 'flight_direction', 'str_NW_SE');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_N_S', 'N – S');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_NE_SW', 'NE – SV');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_E_W', 'E – V');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_SE_NW', 'SE – NV');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_S_N', 'S – N');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_SW_NE', 'SV – NE');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_W_E', 'V – E');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_NW_SE', 'NV – SE');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_N_S', 'É – D');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_NE_SW', 'ÉK – DNy');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_E_W', 'K – Ny');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_SE_NW', 'DK – ÉNy');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_S_N', 'D – É');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_SW_NE', 'DNy – ÉK');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_W_E', 'Ny – K');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_NW_SE', 'ÉNy – DK');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_N_S', 'N – S');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_NE_SW', 'NE – SW');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_E_W', 'E – W');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_SE_NW', 'SE – NW');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_S_N', 'S – N');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_SW_NE', 'SW – NE');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_W_E', 'W – E');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_NW_SE', 'NW – SE');


INSERT INTO elc_search (data_table, subject, word, status, parent_id) SELECT data_table, subject, word, status, 4813 as parent_id from elc_search where parent_id = 4338;


INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_impact_type', 'Veszélyeztető tényező típus');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_impact_l1', 'Főkategória');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_impact_l2', 'Kategória');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_impact_l3', 'Alkategória');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_impact_l4', 'Típus');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_impact_intensity', 'Hatás intenzitás');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_observers', 'Megfigyelő');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_amenintare', 'Jövőbeli hatások');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_presiune', 'Jelenlegi nyomás');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_high', 'Magas');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_medium', 'Közepes');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_low', 'Alacsony');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_impact_type', 'Tipul impactului');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_impact_l1', 'Categoria principala');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_impact_l2', 'Categorie');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_impact_l3', 'Subcategorie');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_impact_l4', 'Tip');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_impact_intensity', 'Intensitatea impactului');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_observers', 'Observator');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_amenintare', 'Amenințare');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_presiune', 'Presiune');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_high', 'Ridicată');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_medium', 'Medie');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_low', 'Scazută');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_impact_type', 'Impact type');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_impact_l1', 'Main category');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_impact_l2', 'Category');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_impact_l3', 'Subcategory');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_impact_l4', 'Type');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_impact_intensity', 'Impact intensity');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_observers', 'Observers');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_amenintare', 'Threat');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_presiune', 'Pressur');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_high', 'High');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_medium', 'Medium');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_low', 'Low');


ALTER TABLE elc ALTER COLUMN flight_altitude TYPE character varying;



INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_migration_type_4', '4 (zbor activ)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_migration_type_4', '4 (zbor activ)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_migration_type_4', '4 (zbor activ)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_migration_type_3', '3 (zbor la înălțime mică cu ajutorul vântului)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_migration_type_3', '3 (zbor la înălțime mică cu ajutorul vântului)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_migration_type_3', '3 (zbor la înălțime mică cu ajutorul vântului)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_migration_type_2', '2 (înălțare prin termic)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_migration_type_2', '2 (înălțare prin termic)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_migration_type_2', '2 (înălțare prin termic)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_migration_type_1', '1 (la înîlțime cu zbor planat)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_migration_type_1', '1 (la înîlțime cu zbor planat)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_migration_type_1', '1 (la înîlțime cu zbor planat)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_migration_intensity_3', '3 (exemplar honăritor)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_migration_intensity_3', '3 (exemplar honăritor)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_migration_intensity_3', '3 (exemplar honăritor)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_migration_intensity_2', '2 (migrație cu oprire/hrănire)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_migration_intensity_2', '2 (migrație cu oprire/hrănire)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_migration_intensity_2', '2 (migrație cu oprire/hrănire)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_migration_intensity_1', '1 (migrație fără oprire)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_migration_intensity_1', '1 (migrație fără oprire)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_migration_intensity_1', '1 (migrație fără oprire)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_flight_altitude_high', '++ (> 200m)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_flight_altitude_high', '++ (> 200m)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_flight_altitude_high', '++ (> 200m)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_flight_altitude_above', '+ (50-200 m)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_flight_altitude_above', '+ (50-200 m)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_flight_altitude_above', '+ (50-200 m)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_flight_altitude_zero', '0 (0-50 m)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_flight_altitude_zero', '0 (0-50 m)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_flight_altitude_zero', '0 (0-50 m)');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_flight_altitude_under', '- (sub punct obs)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_flight_altitude_under', '- (sub punct obs)');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_flight_altitude_under', '- (sub punct obs)');

