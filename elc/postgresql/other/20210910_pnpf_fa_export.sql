WITH d as (
SELECT 
    obm_id,
    ST_X(obm_geometry) as x,
    ST_Y(obm_geometry) as y,
    species,
    "date"
FROM elc 
where 
    grouping_code = 'öreg fa' AND
    program = 'PN Porțile de Fier: B31B fajelölés' --AND date BETWEEN '2021-08-01' AND '2021-12-31'
)
SELECT * FROM d;

\copy (WITH d as ( SELECT obm_id, ST_X(obm_geometry) as x, ST_Y(obm_geometry) as y, species, "date" FROM elc where grouping_code = 'öreg fa' AND program = 'PN Porțile de Fier: B31B fajelölés') SELECT * FROM d) TO '/tmp/elc_oreg_fa.csv' CSV HEADER;

\copy ( WITH d as ( SELECT ST_X(obm_geometry) as x, ST_Y(obm_geometry) as y, species, "date"::date, obm_id, sampling_unit as cod FROM elc where grouping_code = 'tampon zóna' AND program = 'PN Porțile de Fier: B31B fajelölés' ) SELECT * FROM d order by cod) TO '/home/gabor/Documents/motolla/ecologic/kazan-szoros/fajeloles/tampon_zona_20220506.csv' CSV HEADER;
\copy ( WITH d as ( SELECT ST_X(obm_geometry) as x, ST_Y(obm_geometry) as y, species, "date"::date, obm_id, sampling_unit as cod FROM elc where grouping_code = 'öreg fa' AND program = 'PN Porțile de Fier: B31B fajelölés' ) SELECT * FROM d order by cod) TO '/home/gabor/Documents/motolla/ecologic/kazan-szoros/fajeloles/oreg_fa_20220506.csv' CSV HEADER;

DO $$
DECLARE r record;
BEGIN
    FOR r IN SELECT DISTINCT sampling_unit FROM elc WHERE grouping_code = 'tampon zóna' AND sampling_unit IS NOT NULL AND program = 'PN Porțile de Fier: B31B fajelölés' AND date BETWEEN '2021-05-01' AND '2021-07-31'
    LOOP
        EXECUTE 'COPY (WITH d as (
        SELECT 
            ST_X(obm_geometry) as x,
            ST_Y(obm_geometry) as y,
            species,
            date,
            sampling_unit
        FROM elc 
        where 
            grouping_code = ''tampon zóna'' AND
            program = ''PN Porțile de Fier: B31B fajelölés'' AND 
            sampling_unit = ' || quote_literal(r.sampling_unit) || ' AND
            date BETWEEN ''2021-05-01'' AND ''2021-07-31'' 
        )
        SELECT * FROM d) TO ''/tmp/elc/' || r.sampling_unit || '.csv'' CSV HEADER;';
    END LOOP;
END
$$;



COPY (WITH d as (
SELECT 
    obm_id,
    file_id, 
    CASE 
        WHEN grouping_code = 'tampon zóna' THEN 'TZ'
        WHEN grouping_code = 'öreg fa' THEN 'AB'
    END as gc,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM elc 
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'PN Porțile de Fier: B31B fajelölés' AND 
    grouping_code = 'tampon zóna' AND
    sampling_unit IS NOT NULL AND
    obm_files_id IS NOT NULL 
)
SELECT 'cp /var/www/html/biomaps/root-site/projects/elc/attached_files/' || reference || ' /tmp/elc_b31b/' || gc || '_' || obm_id || '_E' || x || '_N' || y || '.jpg' FROM d)
TO '/tmp/tampon_zona_masolas.sh' CSV;

