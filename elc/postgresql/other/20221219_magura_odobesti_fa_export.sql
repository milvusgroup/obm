WITH d as (
SELECT 
    obm_id,
    ST_X(obm_geometry) as x,
    ST_Y(obm_geometry) as y,
    species,
    "date",
    sampling_unit
FROM elc 
where 
    grouping_code = 'str_tampon_zone' AND
    program = 'str_magura_odobesti_tree_marking'
)
SELECT * FROM d;


\copy (WITH d as ( SELECT obm_id, ST_X(obm_geometry) as x, ST_Y(obm_geometry) as y, species, "date", sampling_unit FROM elc where grouping_code = 'str_tampon_zone' AND program = 'str_magura_odobesti_tree_marking') SELECT * FROM d) TO /tmp/mo_tz.csv CSV HEADER;


COPY (WITH d as (
SELECT 
    obm_id,
    file_id, 
    'TZ' as gc,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM elc 
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'str_magura_odobesti_tree_marking' AND 
    grouping_code = 'str_tampon_zone' AND
    sampling_unit IS NOT NULL AND
    obm_files_id IS NOT NULL 
)
SELECT 'cp /home/gabor/obm/obm-composer/local/elc/local/attached_files/' || reference || ' /tmp/elc_mo_tz/' || gc || '_' || obm_id || '_E' || x || '_N' || y || '.jpg' FROM d)
TO '/tmp/tampon_zona_masolas.sh' CSV;


