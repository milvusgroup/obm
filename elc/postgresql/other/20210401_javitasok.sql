SELECT DISTINCT sampling_unit FROM elc WHERE obm_id IN (71665,71667,71666,71669,71668,71671,71670,71663);
BEGIN; 
UPDATE elc SET sampling_unit = 'CD39' WHERE obm_id IN (71665,71667,71666,71669,71668,71671,71670,71663);

SELECT DISTINCT ST_AsText(s.obm_geometry) as wkt, s.word as sampling_unit, s2.word as grouping_code, s3.word as program FROM elc_search s 
LEFT JOIN system.uploadings u ON s.obm_uploading_id = u.id
LEFT JOIN elc_search s2 ON s2.subject = 'grouping_code' AND s2.obm_id = s.parent_id
LEFT JOIN elc_search s3 ON s3.subject = 'program' AND s3.obm_id = s2.parent_id
WHERE s.subject = 'sampling_unit' AND u.uploader_id = 1364;
