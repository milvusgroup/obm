WITH dat as (
    SELECT DISTINCT projekt, program FROM elc 
),
src as (
    SELECT projekt, program FROM elc_programs
)
SELECT * FROM dat FULL JOIN src ON dat.projekt = src.projekt AND dat.program = src.program;

SELECT DISTINCT program FROM elc WHERE program LIKE 'Nagy%';
--               program               
-- ------------------------------------
--  Nagyhagymás POS: faunisztika
--  Nagyhagymás POS: harkály
--  Nagyhagymás POS: lappantyú
--  Nagyhagymás POS: légykapó
--  Nagyhagymás POS: ragadozó
--  Nagyhagymás POS: siketfajd
--  Nagyhagymás POS: szirti-vándor
--  Nagyhagymás POS: törpekuvik
--  Nagyhagymás POS: tövisszúró gébics
--  Nagyhagymás POS: uhu
--  Nagyhagymás POS: uráli

--------------------------------------------------------------
--  Nagyhagymás POS: gatyáskuvik

SELECT * FROM elc_sampling_units WHERE program LIKE 'Nagyh%';
SELECT * FROM elc_search WHERE parent_id = 4263;
SELECT DISTINCT program FROM elc WHERE projekt = 'Nagyhagymás POS';

WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4263
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: gatyáskuvik'
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------

--------------------------------------------------------------
--  Nagyhagymás POS: erdei pacsirta

INSERT INTO elc_search (obm_geometry, data_table, subject, word, status, parent_id, comments) 
SELECT DISTINCT obm_geometry, 'elc', 'sampling_unit', 'LA_' || lpad(replace(replace(sampling_unit, 'b', ''), 'lul', ''), 2, '0'), '0', 4225, 'original name: ' || sampling_unit FROM elc WHERE program = 'Nagyhagymás POS: erdei pacsirta';

UPDATE elc SET grouping_code = 'PNCBH: LULARB' WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: erdei pacsirta';

WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4225
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: erdei pacsirta'
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------

--------------------------------------------------------------
--  Nagyhagymás POS: harkály

SELECT DISTINCT species FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: harkály';

SELECT DISTINCT obm_geometry, program, grouping_code, sampling_unit, count(*) FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: harkály' AND species NOT IN ('Dryocopus martius', 'Picoides tridactylus', 'Dendrocopos leucotos', 'Picus canus') GROUP BY obm_geometry, program, grouping_code, sampling_unit HAVING count(*) > 1;

UPDATE elc SET grouping_code = 'PNCBH: PICIDAE' WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: harkály';

SELECT * FROM elc_search WHERE parent_id = 4183;
UPDATE elc_search SET word = 'PNCBH: PICIDAE' WHERE obm_id = 4295;
SELECT * FROM elc_search WHERE parent_id IN (SELECT obm_id FROM elc_search WHERE parent_id = 4183);
DELETE FROM elc_search WHERE obm_id IN (4300, 4288, 4250, 4278, 4321, 4236, 4304);

WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4295
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: harkály' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: törpekuvik


SELECT DISTINCT grouping_code, count(*) FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: törpekuvik' GROUP BY grouping_code;
UPDATE elc SET grouping_code = 'PNCBH: GLAPAS' WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: törpekuvik';
UPDATE elc SET grouping_code = 'str_nagyhagymas_glapas_points' WHERE projekt = 'Nagyhagymás POS' AND program = 'PNCBH: GLAPAS';

SELECT * FROM elc_search WHERE word = 'ELC_16';
UPDATE elc_search SET word = 'PNCBH: GLAPAS' WHERE obm_id = 4216;
-- 4216

INSERT INTO elc_search (obm_geometry, data_table, subject, word, status, parent_id, comments) 
SELECT DISTINCT obm_geometry, 'elc', 'sampling_unit', 'GP_' || lpad((row_number() over())::varchar, 2, '0') as sampling_unit, '0', 4216, 'original name: ' || sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: törpekuvik' AND species = 'null' ORDER BY sampling_unit;  

SELECT DISTINCT obm_geometry, 'elc', 'sampling_unit', 'GP_' || lpad((row_number() over())::varchar, 2, '0') as sampling_unit, '0', 4216, 'original name: ' || sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: törpekuvik' AND species != 'null' ORDER BY sampling_unit;  

WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4216
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: törpekuvik' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: uráli


SELECT DISTINCT grouping_code, sampling_unit, count(*) FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: uráli' GROUP BY grouping_code, sampling_unit;

UPDATE elc SET grouping_code = 'PNCBH: STRIX' WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: uráli';

SELECT * FROM elc_search WHERE word = 'ELC_24';
UPDATE elc_search SET word = 'PNCBH: STRIX' WHERE obm_id = 4241;
-- 4241

INSERT INTO elc_search (obm_geometry, data_table, subject, word, status, parent_id, comments) 
SELECT DISTINCT obm_geometry, 'elc', sampling_unit, 'SX_' || lpad((row_number() over())::varchar, 2, '0') as sampling_unit, '0', 4241, 'original name: ' || sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: uráli' AND species = 'null';  


WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4241
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: uráli' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: lappantyú

SELECT DISTINCT grouping_code, sampling_unit, count(*) FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: lappantyú' GROUP BY grouping_code, sampling_unit;
-- ELC_15

UPDATE elc SET grouping_code = 'PNCBH: CAPEUR' WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: lappantyú';

SELECT * FROM elc_search WHERE word = 'ELC_15';
UPDATE elc_search SET word = 'PNCBH: CAPEUR' WHERE obm_id = 4260;
-- 4250

INSERT INTO elc_search (obm_geometry, data_table, subject, word, status, parent_id, comments) 
SELECT DISTINCT obm_geometry, 'elc', 'sampling_unit', 'CE_' || lpad((row_number() over())::varchar, 2, '0') as sampling_unit, '0', 4260, 'original name: ' || sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: lappantyú' AND species = 'null';  


WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4241
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: uráli' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: légykapó

SELECT count(DISTINCT sampling_unit) FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: légykapó';
SELECT count(DISTINCT obm_geometry) FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: légykapó';
-- minden megfigyelés a pontokon van

SELECT obm_id FROM elc_search WHERE word = 'Nagyhagymás POS: légykapó';
-- 4201
\copy (SELECT * FROM elc_search WHERE parent_id = 4201) TO '/tmp/fic_gyk.csv' CSV HEADER;

\copy (SELECT DISTINCT st_astext(obm_geometry) as wkt, grouping_code, sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: légykapó') TO '/tmp/fic_mfp.csv' CSV HEADER;
-- ELC_15

-- gyűjtőkódok átnevezése
UPDATE elc_search SET word = 'PNCBH: FICALB - 17' WHERE obm_id = 4283;
UPDATE elc_search SET word = 'PNCBH: FICALB - 27' WHERE obm_id = 4285;
UPDATE elc_search SET word = 'PNCBH: FICALB - 3' WHERE obm_id = 4258;
UPDATE elc_search SET word = 'PNCBH: FICALB - 41' WHERE obm_id = 4323;
UPDATE elc_search SET word = 'PNCBH: FICALB - 56' WHERE obm_id = 4245;
UPDATE elc_search SET word = 'PNCBH: FICALB - 79' WHERE obm_id = 4289;
UPDATE elc_search SET word = 'PNCBH: FICALB - 82' WHERE obm_id = 4218;
UPDATE elc_search SET word = 'PNCBH: FICALB - 21' WHERE obm_id = 4308;
UPDATE elc_search SET word = 'PNCBH: FICALB - 1' WHERE obm_id = 4268;
UPDATE elc_search SET word = 'PNCBH: FICALB - 2' WHERE obm_id = 4271;
UPDATE elc_search SET word = 'PNCBH: FICALB - 46' WHERE obm_id = 4306;
UPDATE elc_search SET word = 'PNCBH: FICALB - 13' WHERE obm_id = 4270;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: ragadozó

SELECT DISTINCT grouping_code, sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: ragadozó';

UPDATE elc SET grouping_code = 'PNCBH: RAP' WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: ragadozó';

SELECT * FROM elc_search WHERE word = 'ELC_17';
-- 4239
UPDATE elc_search SET word = 'PNCBH: RAP' WHERE obm_id = 4239;

WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4239
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: ragadozó' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: siketfajd

SELECT DISTINCT grouping_code, sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: siketfajd';

UPDATE elc SET grouping_code = 'PNCBH: TETURO' WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: siketfajd';

SELECT * FROM elc_search WHERE word = 'ELC_22';
-- 4282

UPDATE elc_search SET word = 'PNCBH: TETURO' WHERE obm_id = 4282;

WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4282
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: siketfajd' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: szirti & vándor

SELECT DISTINCT projekt, program, grouping_code, sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: szirti%';

UPDATE elc SET program = 'Nagyhagymás POS: szirti & vándor', grouping_code = 'PNCBH: AQUCHR & FALPER' WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: szirti%';

SELECT * FROM elc_search WHERE word LIKE 'Nagyhagymás POS: szirti%';
UPDATE elc_search SET word = 'Nagyhagymás POS: szirti & vándor' WHERE obm_id = 4203;
SELECT * FROM elc_search WHERE parent_id = 4203;
--7445

 
INSERT INTO elc_search (obm_geometry, data_table, subject, word, status, parent_id, comments) 
SELECT DISTINCT obm_geometry, 'elc', 'sampling_unit', 'CHR_' || lpad(substr(replace(sampling_unit, 'B', ''), 1, 2), 2, '0'), '0', 7445, 'original name: ' || sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: szirti%';

WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 7445
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: szirti & vándor' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: tövisszúró gébics

SELECT DISTINCT projekt, program, grouping_code, sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: tövisszúró gébics';

UPDATE elc SET grouping_code = 'PNCBH: LANCOL' WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: tövisszúró gébics';

SELECT * FROM elc_search WHERE word LIKE 'Nagyhagymás POS: tövisszúró gébics';
SELECT * FROM elc_search WHERE parent_id = 4180;
UPDATE elc_search SET word = 'PNCBH: LANCOL' WHERE parent_id = 4180;
--4284

 
--INSERT INTO elc_search (obm_geometry, data_table, subject, word, status, parent_id, comments) 
\copy (SELECT DISTINCT st_astext(obm_geometry), 'elc', 'sampling_unit', 'LC_' || lpad(substr(sampling_unit, 4), 2, '0') as word, '0', 4284, 'original name: ' || sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: tövisszúró gébics' ORDER BY word) TO '/tmp/lancol_mfp.csv' CSV HEADER;


WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4284
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: tövisszúró gébics' AND word IS NOT NULL
)
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------



--------------------------------------------------------------
--  Nagyhagymás POS: uhu

SELECT DISTINCT projekt, program, grouping_code, sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: uhu';

UPDATE elc SET grouping_code = 'PNCBH: BUBO' WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: uhu';

SELECT * FROM elc_search WHERE word LIKE 'Nagyhagymás POS: uhu';
SELECT * FROM elc_search WHERE parent_id = 4188;
UPDATE elc_search SET word = 'PNCBH: BUBO' WHERE obm_id = 4272;
--4272

 
INSERT INTO elc_search (obm_geometry, data_table, subject, word, status, parent_id, comments) 
SELECT DISTINCT obm_geometry, 'elc', 'sampling_unit', 'BB_0' || (row_number() OVER())::varchar, '0', 4272, 'original name: ' || sampling_unit FROM elc WHERE projekt = 'Nagyhagymás POS' AND program LIKE 'Nagyhagymás POS: uhu';


WITH asd as (
    SELECT DISTINCT elc.obm_id, sampling_unit, word 
    FROM elc 
    LEFT JOIN elc_search ON replace(comments, 'original name: ', '') = sampling_unit AND parent_id = 4272
    WHERE projekt = 'Nagyhagymás POS' AND program = 'Nagyhagymás POS: uhu' AND word IS NOT NULL
)
--SELECT * FROM asd;
UPDATE elc SET sampling_unit = asd.word FROM asd WHERE elc.obm_id = asd.obm_id;
--------------------------------------------------------------





--------------------------------------------------------------
-- projekt és programok átnevezése, fordíthatóvá tétele

SELECT word FROM elc_search WHERE word LIKE 'Nagyhagymás:%';
UPDATE elc_search SET word = replace(word, ' POS', '') WHERE word LIKE 'Nagyhagymás%';
UPDATE elc_search SET word = 'str_nagyhagymas_aegfun' WHERE word = 'Nagyhagymás: gatyáskuvik';
UPDATE elc_search SET word = 'str_nagyhagymas_lularb' WHERE word = 'Nagyhagymás: erdei pacsirta';
UPDATE elc_search SET word = 'str_nagyhagymas_lancol' WHERE word = 'Nagyhagymás: tövisszúró gébics';
UPDATE elc_search SET word = 'str_nagyhagymas_picidae' WHERE word = 'Nagyhagymás: harkály';
UPDATE elc_search SET word = 'str_nagyhagymas_aquchr' WHERE word = 'Nagyhagymás: szirti & vándor';
UPDATE elc_search SET word = 'str_nagyhagymas_bubbub' WHERE word = 'Nagyhagymás: uhu';
UPDATE elc_search SET word = 'str_nagyhagymas_capeur' WHERE word = 'Nagyhagymás: lappantyú';
UPDATE elc_search SET word = 'str_nagyhagymas_occasional_observations' WHERE word = 'Nagyhagymás: faunisztika';
UPDATE elc_search SET word = 'str_nagyhagymas_rapaces' WHERE word = 'Nagyhagymás: ragadozó';
UPDATE elc_search SET word = 'str_nagyhagymas_strura' WHERE word = 'Nagyhagymás: uráli';
UPDATE elc_search SET word = 'str_nagyhagymas_ficalb' WHERE word = 'Nagyhagymás: légykapó';
UPDATE elc_search SET word = 'str_nagyhagymas_teturo' WHERE word = 'Nagyhagymás: siketfajd';
UPDATE elc_search SET word = 'str_nagyhagymas_glapas' WHERE word = 'Nagyhagymás: törpekuvik';


UPDATE elc SET projekt = replace(projekt, ' POS', '') WHERE projekt LIKE 'Nagyhagymás%';
UPDATE elc SET program = 'str_nagyhagymas_lancol'                   WHERE program = 'Nagyhagymás POS: tövisszúró gébics';
UPDATE elc SET program = 'str_nagyhagymas_aegfun'                   WHERE program = 'Nagyhagymás POS: gatyáskuvik';
UPDATE elc SET program = 'str_nagyhagymas_lularb'                   WHERE program = 'Nagyhagymás POS: erdei pacsirta';
UPDATE elc SET program = 'str_nagyhagymas_picidae'                  WHERE program = 'Nagyhagymás POS: harkály';
UPDATE elc SET program = 'str_nagyhagymas_aquchr'                   WHERE program = 'Nagyhagymás POS: szirti & vándor';
UPDATE elc SET program = 'str_nagyhagymas_bubbub'                   WHERE program = 'Nagyhagymás POS: uhu';
UPDATE elc SET program = 'str_nagyhagymas_capeur'                   WHERE program = 'Nagyhagymás POS: lappantyú';
UPDATE elc SET program = 'str_nagyhagymas_occasional_observations'  WHERE program = 'Nagyhagymás POS: faunisztika';
UPDATE elc SET program = 'str_nagyhagymas_rapaces'                  WHERE program = 'Nagyhagymás POS: ragadozó';
UPDATE elc SET program = 'str_nagyhagymas_strura'                   WHERE program = 'Nagyhagymás POS: uráli';
UPDATE elc SET program = 'str_nagyhagymas_ficalb'                   WHERE program = 'Nagyhagymás POS: légykapó';
UPDATE elc SET program = 'str_nagyhagymas_teturo'                   WHERE program = 'Nagyhagymás POS: siketfajd';
UPDATE elc SET program = 'str_nagyhagymas_glapas'                   WHERE program = 'Nagyhagymás POS: törpekuvik';

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_aegfun', 'Nagyhagymás: tövisszúró gébics');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_lularb', 'Nagyhagymás: gatyáskuvik');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_lancol', 'Nagyhagymás: erdei pacsirta');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_picidae', 'Nagyhagymás: harkály');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_aquchr', 'Nagyhagymás: szirti & vándor');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_bubbub', 'Nagyhagymás: uhu');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_capeur', 'Nagyhagymás: lappantyú');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_occasional_observations', 'Nagyhagymás: faunisztika');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_rapaces', 'Nagyhagymás: ragadozó');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_strura', 'Nagyhagymás: uráli');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_ficalb', 'Nagyhagymás: légykapó');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_teturo', 'Nagyhagymás: siketfajd');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'hu', 'str_nagyhagymas_glapas', 'Nagyhagymás: törpekuvik');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_aegfun', 'PN Cheile Bicazului - Hasmas: tövisszúró gébics');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_lularb', 'PN Cheile Bicazului - Hasmas: gatyáskuvik');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_lancol', 'PN Cheile Bicazului - Hasmas: erdei pacsirta');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_picidae', 'PN Cheile Bicazului - Hasmas: harkály');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_aquchr', 'PN Cheile Bicazului - Hasmas: szirti & vándor');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_bubbub', 'PN Cheile Bicazului - Hasmas: uhu');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_capeur', 'PN Cheile Bicazului - Hasmas: lappantyú');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_occasional_observations', 'PN Cheile Bicazului - Hasmas: faunisztika');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_rapaces', 'PN Cheile Bicazului - Hasmas: ragadozó');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_strura', 'PN Cheile Bicazului - Hasmas: uráli');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_ficalb', 'PN Cheile Bicazului - Hasmas: légykapó');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_teturo', 'PN Cheile Bicazului - Hasmas: siketfajd');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'ro', 'str_nagyhagymas_glapas', 'PN Cheile Bicazului - Hasmas: törpekuvik');

INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_aegfun', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_lularb', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_lancol', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_picidae', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_aquchr', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_bubbub', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_capeur', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_occasional_observations', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_rapaces', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_strura', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_ficalb', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_teturo', '__not_translated__');
INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', 'elc', 'en', 'str_nagyhagymas_glapas', '__not_translated__');

--------------------------------------------------------------




--------------------------------------------------------------
-- Magyhagymas csoport hozzáférés beállítások

UPDATE elc_rules SET read = '{1492, 1866}'
WHERE data_table = 'elc' AND row_id IN (
    SELECT obm_id FROM elc WHERE projekt = 'Nagyhagymás'
);

--------------------------------------------------------------
