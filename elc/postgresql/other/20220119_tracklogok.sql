-- grant select on system.tracklogs to elc_admin;
select count(*) from system.tracklogs where project = 'elc';
DROP TABLE elc_tracklogs;

id	integer Auto Increment [nextval('elc_tracklogs_id_seq')]	
user_id	integer NULL	
start_time	timestamptz	
end_time	timestamptz	
tracklog_geom	json NULL	
trackname	text NULL	
tracklog_id	text NULL	
tracklog_line_geom	geometry(LineString,4326) NULL

	text NULL	
	integer NULL	
	timestamptz	
	timestamptz	
	text NULL	
	text	
	text NULL	
	json NULL	
	public.geometry(LineString,4326) NULL
ALTER TABLE elc_tracklogs ADD COLUMN observation_list_id text NULL;
GRANT ALL ON TABLE elc_tracklogs TO elc_admin;
GRANT ALL ON SEQUENCE elc_tracklogs_id_seq TO elc_admin;
INSERT INTO elc_tracklogs (
    user_id, start_time, end_time, tracklog_geom, trackname, tracklog_id, observation_list_id, tracklog_line_geom
)
SELECT 
    user_id, start_time, end_time, tracklog_geom, trackname, tracklog_id, observation_list_id, tracklog_line_geom
FROM 
    system.tracklogs WHERE project = 'elc';
    
GRANT SELECT on elc_tracklogs TO mihhok_gmail_com;

INSERT INTO system.tracklogs (
    user_id, start_time, end_time, tracklog_geom, trackname, tracklog_id, tracklog_line_geom, project
)
SELECT 
    user_id, start_time, end_time, tracklog_geom, trackname, tracklog_id, tracklog_line_geom, 'elc'
FROM 
    openherpmaps_tracklogs WHERE user_id = 62;
