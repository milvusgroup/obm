-- DOMOGLED CERNA
-- ellenőrzöm a rosz mfp-re feltöltött megfigyeléseket

WITH xx as (
SELECT DISTINCT 
    su.program, su.grouping_code, su.sampling_unit, e.date, e.observers, st_distance(st_transform(sampling_unit_geometry, 3844), st_transform(surveyed_geometry, 3844)) as d
FROM elc_sampling_units su 
LEFT JOIN elc e ON e.program = su.program AND e.grouping_code = su.grouping_code AND e.sampling_unit = su.sampling_unit 
WHERE su.program LIKE '%domogled_cerna_cbm%' ORDER BY d DESC, su.grouping_code, e.date, e.observers
)
SELECT distinct grouping_code, sampling_unit FROM xx WHERE d > 200 ORDER BY sampling_unit;

-- 1. AT3603 - II - Kiss Arnold - 85 adat - a panzióból töltötte fel az adatokat, mert nem volt letöltve az űrlap. Javítom a dátumot 2023-06-12-ről 06-11-re, a surveyed geometryt pedig az első kiszállás alapján
-- obm_id IN (122922,122903,122908,122890,122885,122872,122868,122860,122863,122859,122867,122865,122854,122852,122848,122862,122855,122850,122869,122853,122866,122864,122857,122858,122871,122861,122851,122856,122870,122849,122932,122894,122887,122909,122915,122892,122881,122914,122879,122884,122900,122896,122938,122912,122901,122925,122930,122931,122893,122898,122902,122918,122926,122905,122891,122888,122883,122937,122916,122934,122929,122917,122911,122913,122910,122936,122919,122904,122886,122928,122899,122889,122906,122923,122882,122880,122933,122907,122897,122935,122924,122927,122920,122895,122921)
BEGIN;
    UPDATE elc SET date = '2023-06-11' WHERE obm_id IN (122922,122903,122908,122890,122885,122872,122868,122860,122863,122859,122867,122865,122854,122852,122848,122862,122855,122850,122869,122853,122866,122864,122857,122858,122871,122861,122851,122856,122870,122849,122932,122894,122887,122909,122915,122892,122881,122914,122879,122884,122900,122896,122938,122912,122901,122925,122930,122931,122893,122898,122902,122918,122926,122905,122891,122888,122883,122937,122916,122934,122929,122917,122911,122913,122910,122936,122919,122904,122886,122928,122899,122889,122906,122923,122882,122880,122933,122907,122897,122935,122924,122927,122920,122895,122921);
    WITH sgk as (SELECT sampling_unit, st_centroid(ST_COllect(surveyed_geometry)) as helyessg FROM elc WHERE program = 'str_domogled_cerna_cbm' AND observers = 'Kiss Arnold' AND date != '2023-06-11' AND grouping_code = 'AT3603' GROUP BY sampling_unit) UPDATE elc SET surveyed_geometry = helyessg FROM sgk WHERE obm_id IN (122922,122903,122908,122890,122885,122872,122868,122860,122863,122859,122867,122865,122854,122852,122848,122862,122855,122850,122869,122853,122866,122864,122857,122858,122871,122861,122851,122856,122870,122849,122932,122894,122887,122909,122915,122892,122881,122914,122879,122884,122900,122896,122938,122912,122901,122925,122930,122931,122893,122898,122902,122918,122926,122905,122891,122888,122883,122937,122916,122934,122929,122917,122911,122913,122910,122936,122919,122904,122886,122928,122899,122889,122906,122923,122882,122880,122933,122907,122897,122935,122924,122927,122920,122895,122921) AND elc.sampling_unit = sgk.sampling_unit;
    UPDATE elc SET distance_m = NULL WHERE obm_id IN (122922,122903,122908,122890,122885,122872,122868,122860,122863,122859,122867,122865,122854,122852,122848,122862,122855,122850,122869,122853,122866,122864,122857,122858,122871,122861,122851,122856,122870,122849,122932,122894,122887,122909,122915,122892,122881,122914,122879,122884,122900,122896,122938,122912,122901,122925,122930,122931,122893,122898,122902,122918,122926,122905,122891,122888,122883,122937,122916,122934,122929,122917,122911,122913,122910,122936,122919,122904,122886,122928,122899,122889,122906,122923,122882,122880,122933,122907,122897,122935,122924,122927,122920,122895,122921);
COMMIT;


-- 2. AT3522 - I - Iacob George - 4 adat - a 03-as pont adatait a 04-re töltötte
-- 105039, 105042, 105041, 105040
UPDATE elc SET sampling_unit = 'AT3522_03', distance_m = NULL WHERE obm_id IN (105039, 105042, 105041, 105040);


-- 3. Gál László 2023-05-10 - A nera völgyi felmérését a domoglédi űrlapba töltötte fel AQ3910-t AN3806-ra javítom 
-- 110303,110294,110338,110309,110339,110332,110334,110344,110349,110319,110306,110333,110335,110345,110300,110322,110313,110286,110279,110283,110314,110350,110323,110336,110308,110299,110292,110291,110284,110282,110290,110287,110351,110341,110326,110325,110316,110310,110307,110302,110298,110293,110346,110330,110329,110304,110317,110295,110288,110285,110343,110348,110331,110315,110280,110297,110342,110296,110305,110318,110352,110328,110320,110340,110281,110353,110347,110312,110278,110311,110321,110324,110289,110327,110337,110301
UPDATE elc SET projekt = 'Nera-Beușnița', program = 'str_nera-beusnita_cbm', grouping_code = 'AN3806', sampling_unit = replace(sampling_unit, 'AQ3910', 'AN3806'), distance_m = NULL WHERE obm_id IN (110303,110294,110338,110309,110339,110332,110334,110344,110349,110319,110306,110333,110335,110345,110300,110322,110313,110286,110279,110283,110314,110350,110323,110336,110308,110299,110292,110291,110284,110282,110290,110287,110351,110341,110326,110325,110316,110310,110307,110302,110298,110293,110346,110330,110329,110304,110317,110295,110288,110285,110343,110348,110331,110315,110280,110297,110342,110296,110305,110318,110352,110328,110320,110340,110281,110353,110347,110312,110278,110311,110321,110324,110289,110327,110337,110301);


-- 4. Gál László 2023-05-11 - A nera völgyi felmérését a domoglédi űrlapba töltötte fel AR3710-t AN3801-ra javítom 
-- 110375,110387,110394,110382,110377,110391,110396,110380,110362,110354,110372,110378,110395,110357,110385,110390,110383,110359,110367,110389,110356,110404,110401,110392,110386,110376,110371,110381,110393,110384,110369,110405,110406,110355,110374,110400,110358,110399,110379,110368,110361,110366,110402,110363,110373,110403,110388,110364,110360,110398,110397,110365,110370
UPDATE elc SET projekt = 'Nera-Beușnița', program = 'str_nera-beusnita_cbm', grouping_code = 'AN3801', sampling_unit = replace(sampling_unit, 'AR3710', 'AN3801'), distance_m = NULL WHERE obm_id IN (110375,110387,110394,110382,110377,110391,110396,110380,110362,110354,110372,110378,110395,110357,110385,110390,110383,110359,110367,110389,110356,110404,110401,110392,110386,110376,110371,110381,110393,110384,110369,110405,110406,110355,110374,110400,110358,110399,110379,110368,110361,110366,110402,110363,110373,110403,110388,110364,110360,110398,110397,110365,110370);


-- 5. Kovács István 2023-05-04 - AT3622_19 helyett AT3522_19-re tette a megfigyeléseket
-- 106144,106135,106146,106133,106145,106141,106140,106137,106143,106134,106136,106142,106138,106139
UPDATE elc SET grouping_code = 'AT3622', sampling_unit = 'AT3622_19', distance_m = NULL WHERE obm_id IN (106144,106135,106146,106133,106145,106141,106140,106137,106143,106134,106136,106142,106138,106139);

-- 6. Kovács István 2023-05-04 - AT3622_04 helyett AT3522_04-re tette a megfigyeléseket
-- 106226,106230,106227,106231,106233,106221,106225,106232,106220,106228,106222,106223,106224,106229
UPDATE elc SET grouping_code = 'AT3622', sampling_unit = 'AT3622_04', distance_m = NULL WHERE obm_id IN (106226,106230,106227,106231,106233,106221,106225,106232,106220,106228,106222,106223,106224,106229);


-- 7. Gál László 2023-04-22 - AT3513_18 helyett AU3513_18-re tette a megfigyeléseket
-- 104305,104304,104306,104302,104301,104303
UPDATE elc SET grouping_code = 'AT3513', sampling_unit = 'AT3513_18', distance_m = NULL WHERE obm_id IN (104305,104304,104306,104302,104301,104303);



--  ki, hol, mikor

with p1 as (
    SELECT * FROM elc WHERE program = 'str_domogled_cerna_cbm' AND date BETWEEN '2023-04-15' AND '2023-05-15'
),
p2 as (
    SELECT * FROM elc WHERE program = 'str_domogled_cerna_cbm' AND date BETWEEN '2023-05-16' AND '2023-06-15'
)
SELECT DISTINCT su.program, su.grouping_code, su.sampling_unit, string_agg(DISTINCT p1.observers || ': ' || p1.date::varchar,',') as p1, string_agg(DISTINCT p2.observers || ': ' || p2.date::varchar,',') as p2
FROM elc_sampling_units su 
LEFT JOIN p1 ON p1.program = su.program AND p1.grouping_code = su.grouping_code AND p1.sampling_unit = su.sampling_unit 
LEFT JOIN p2 ON p2.program = su.program AND p2.grouping_code = su.grouping_code AND p2.sampling_unit = su.sampling_unit 
WHERE su.program LIKE '%domogled_cerna_cbm%'  AND COALESCE(p1.date, p2.date) IS NOT NULL GROUP BY su.program, su.grouping_code, su.sampling_unit ORDER BY su.sampling_unit;


-- NERA BEUSNITA
-- ellenőrzöm a rosz mfp-re feltöltött megfigyeléseket

WITH xx as (
SELECT DISTINCT 
    e.obm_id, su.program, su.grouping_code, su.sampling_unit, e.date, e.observers, st_distance(st_transform(sampling_unit_geometry, 3844), st_transform(surveyed_geometry, 3844)) as d
FROM elc_sampling_units su 
LEFT JOIN elc e ON e.program = su.program AND e.grouping_code = su.grouping_code AND e.sampling_unit = su.sampling_unit 
WHERE su.program = 'str_nera-beusnita_cbm' ORDER BY d DESC, su.grouping_code, e.date, e.observers
)
SELECT observers, date, string_agg(obm_id::varchar, ',') FROM xx WHERE d > 200 GROUP BY observers, date;

-- Huțuleac Volosciuc Mihail Victor | 2023-05-11 | 108634,108635,108636,108637,108638,108624,108625,108626,108602,108604,108603,108605,108606,108607,108608,108658,108659,108660,108661,108662,108663,108655,108656,108653,108654,108657,108639,108640,108641,108642,108643,108644,108645,108646,108647,108622,108619,108620,108621,108623,108609,108610,108611,108612,108613,108614,108615,108616,108617,108618,108627,108628,108629,108630,108631,108632,108633,108648,108649,108650,108651,108652 - AM3816 helyett AN3816-ra töltött fel több pontot is
UPDATE elc SET grouping_code = 'AM3816', sampling_unit = replace(sampling_unit, 'AN3816', 'AM3816'), distance_m = NULL WHERE obm_id IN (108634,108635,108636,108637,108638,108624,108625,108626,108602,108604,108603,108605,108606,108607,108608,108658,108659,108660,108661,108662,108663,108655,108656,108653,108654,108657,108639,108640,108641,108642,108643,108644,108645,108646,108647,108622,108619,108620,108621,108623,108609,108610,108611,108612,108613,108614,108615,108616,108617,108618,108627,108628,108629,108630,108631,108632,108633,108648,108649,108650,108651,108652);

-- Ajder Vitalie | 2023-06-14 | 123157,123158,123159,123160,123161,123162,123163,123164,123166,123168,123169,123244,123245,123246,123247,123248,123249,123250,123251,123252,123253,123254,123255,123256,123257,123258,123259,123165,123167,123170,123265,123266,123267,123268,123269,123270,123271,123272,123273,123274,123193,123204,123205,123206,123171,123172,123173,123174,123175,123176,123177,123178,123179,123180,123181,123182,123183,123184,123185,123186,123187,123188,123189,123191,123192,123190,123194,123195,123196,123197,123198,123199,123200,123201,123202,123203,123275,123276,123277,123278,123279,123280,123281,123260,123261,123262,123263,123264,123207,123208,123209,123210,123211,123212,123213,123214,123215,123216,123217,123218,123219,123220,123221,123222,123223,123224,123225,123237,123226,123227,123228,123229,123230,123231,123232,123233,123234,123235,123236,123238,123239,123240,123241,123242,123243 | AL3714 helyett AM3714-re töltötte fel
UPDATE elc SET grouping_code = 'AL3714', sampling_unit = replace(sampling_unit, 'AM3714', 'AL3714'), distance_m = NULL WHERE obm_id IN (123157,123158,123159,123160,123161,123162,123163,123164,123166,123168,123169,123244,123245,123246,123247,123248,123249,123250,123251,123252,123253,123254,123255,123256,123257,123258,123259,123165,123167,123170,123265,123266,123267,123268,123269,123270,123271,123272,123273,123274,123193,123204,123205,123206,123171,123172,123173,123174,123175,123176,123177,123178,123179,123180,123181,123182,123183,123184,123185,123186,123187,123188,123189,123191,123192,123190,123194,123195,123196,123197,123198,123199,123200,123201,123202,123203,123275,123276,123277,123278,123279,123280,123281,123260,123261,123262,123263,123264,123207,123208,123209,123210,123211,123212,123213,123214,123215,123216,123217,123218,123219,123220,123221,123222,123223,123224,123225,123237,123226,123227,123228,123229,123230,123231,123232,123233,123234,123235,123236,123238,123239,123240,123241,123242,123243);

-- Bóné Gábor | 2023-05-11 | 108841,108842,108843 | AN3712 -> AM3912
BEGIN; UPDATE elc SET grouping_code = 'AM3912', sampling_unit = replace(sampling_unit, 'AN3712', 'AM3912'), distance_m = NULL WHERE obm_id IN (108841,108842,108843);

-- Petrencu Laurențiu | 2023-05-12 | 109798,109803,109801,109802,109795,109804,109794,109797,109793,109796,109800,109799,109792 | AM3807_21 -> AM3807_17
BEGIN; UPDATE elc SET sampling_unit = 'AM3807_17', distance_m = NULL WHERE obm_id IN (109798,109803,109801,109802,109795,109804,109794,109797,109793,109796,109800,109799,109792);

-- Helerea Codruț | 2023-05-14 | 111596,111594,111595,111591,111592,111593,111597,111599,111601,111598 | ennyire közelítette meg a pontokat

-- Bóné Gábor | 2023-05-28 | 117925,117926,117927,117928,117929,117930,117931,117932,117933,117934,117935 | AM3912_02 -> AM3912_04
BEGIN; UPDATE elc SET sampling_unit = 'AM3912_04', distance_m = NULL WHERE obm_id IN (117925,117926,117927,117928,117929,117930,117931,117932,117933,117934,117935);

-- Ajder Vitalie | 2023-06-13 | 123118 | gondolom utólag tette be az adatot, mert a többi a helyén van - helyretettem

-- Osváth Gergely | 2023-06-02 | 120340,120339,120365,120366,120367,120341,120344,120343,120345,120342,120360,120362,120364,120359,120356,120355,120357,120358,120361,120363,120304,120302,120292,120296,120300,120301,120303,120297,120287,120289,120291,120280,120288,120290,120281,120282,120299,120294,120293,120298,120346,120347,120348,120350,120351,120352,120353,120349,120354,120295,120371,120368,120369,120370,120372 | AM3714 -> AN3714
BEGIN; UPDATE elc SET grouping_code = 'AN3714', sampling_unit = replace(sampling_unit, 'AM3714', 'AN3714'), distance_m = NULL WHERE obm_id IN (120340,120339,120365,120366,120367,120341,120344,120343,120345,120342,120360,120362,120364,120359,120356,120355,120357,120358,120361,120363,120304,120302,120292,120296,120300,120301,120303,120297,120287,120289,120291,120280,120288,120290,120281,120282,120299,120294,120293,120298,120346,120347,120348,120350,120351,120352,120353,120349,120354,120295,120371,120368,120369,120370,120372);

-- Osváth Gergely | 2023-06-02 | 120360,120362,120359,120355,120357,120358,120361,120363,120364,120356,120365,120366,120367,120370,120368,120369,120372,120371,120340,120339 | ez az előző javítás utáni káosz, ezek között van amit 200+-ra áthelyezett, van amit elírt kézzel javítom QGIS-ben az elírásokat

-- Osváth Gergely | 2023-06-02 | 120365,120366,120367,120370,120368,120369,120372,120371,120340,120339 | az előző két javítás után maradt, ennire áthelyezte

-- Ajder Vitalie | 2023-05-11 | 108720,108721,108722,108723,108724,108725,108727,108726 | AL3720_20 -> AL3720_25
BEGIN; UPDATE elc SET sampling_unit = 'AL3720_25', distance_m = NULL WHERE obm_id IN (108720,108721,108722,108723,108724,108725,108727,108726);

-- Ajder Vitalie | 2023-05-11 | 108773 - utólag tette be a listába - áthelyeztem

-- Huțuleac Volosciuc Mihail Victor | 2023-05-31 | 118828,118830,118829,118831,118832,118833,118834,118836,118837,118838,118835 | AM3622_22 -> AM3622_20
BEGIN; UPDATE elc SET sampling_unit = 'AM3622_20', distance_m = NULL WHERE obm_id IN (118828,118830,118829,118831,118832,118833,118834,118836,118837,118838,118835);

-- Gál László | 2023-05-11 | 110369 | a GPS kilengése okozhatta, mert előtte is, utánna is a helyén vannak a pontok. Helyrehúztam.

-- Osváth Gergely | 2023-06-03 | 120406,120408,120405,120402,120404,120407,120403,120425,120424,120427,120428,120426,120397,120400,120396,120413,120394,120395,120398,120399,120401,120422,120423,120418,120419,120420,120421,120375,120409,120410,120411,120412,120414,120415,120416,120417,120431,120436,120429,120432,120435,120430,120373,120374,120376,120377,120378,120379,120380,120381,120433,120434,120437,120438,120439,120440,120441,120390,120383,120384,120391,120382,120385,120386,120387,120388,120389,120392,120393 | AM3816 -> AN3816
BEGIN; UPDATE elc SET grouping_code = 'AN3816', sampling_unit = replace(sampling_unit, 'AM3816', 'AN3816'), distance_m = NULL WHERE obm_id IN (120406,120408,120405,120402,120404,120407,120403,120425,120424,120427,120428,120426,120397,120400,120396,120413,120394,120395,120398,120399,120401,120422,120423,120418,120419,120420,120421,120375,120409,120410,120411,120412,120414,120415,120416,120417,120431,120436,120429,120432,120435,120430,120373,120374,120376,120377,120378,120379,120380,120381,120433,120434,120437,120438,120439,120440,120441,120390,120383,120384,120391,120382,120385,120386,120387,120388,120389,120392,120393);

-- Ajder Vitalie | 2023-05-13 | 111009,111010,111011,111012,111013 | AM3706_11 -> AM3706_13
BEGIN; UPDATE elc SET sampling_unit = 'AM3706_13', distance_m = NULL WHERE obm_id IN (111009,111010,111011,111012,111013);

-- Huțuleac Volosciuc Mihail Victor | 2023-06-02 | 119473,119474,119475,119476,119477,119478 | AM3821_24 -> AM3816_24
BEGIN; UPDATE elc SET sampling_unit = 'AM3816_24', distance_m = NULL WHERE obm_id IN (119473,119474,119475,119476,119477,119478);

-- Kiss István | 2023-06-02 | 121494,121496,121501 | AN3706_16 -> AN3706_11
BEGIN; UPDATE elc SET sampling_unit = 'AN3706_11', distance_m = NULL WHERE obm_id IN (121494,121496,121501);

-- Kiss István | 2023-06-02 | 121498,121499,121500,121503,121507,121505,121506,121497 | AN3706_16 -> AN3706_21 
BEGIN; UPDATE elc SET sampling_unit = 'AN3706_21', distance_m = NULL WHERE obm_id IN (121498,121499,121500,121503,121507,121505,121506,121497);

-- Huțuleac Volosciuc Mihail Victor | 2023-06-01 | 119353,119354,119355,119356,119357,119358,119359,119360 | AM3621_13 -> AM3621_11
BEGIN; UPDATE elc SET sampling_unit = 'AM3621_11', distance_m = NULL WHERE obm_id IN (119353,119354,119355,119356,119357,119358,119359,119360);

--  ki, hol, mikor

with p1 as (
    SELECT * FROM elc WHERE program = 'str_nera-beusnita_cbm' AND date BETWEEN '2023-04-15' AND '2023-05-15'
),
p2 as (
    SELECT * FROM elc WHERE program = 'str_nera-beusnita_cbm' AND date BETWEEN '2023-05-16' AND '2023-06-15'
)
SELECT DISTINCT su.program, su.grouping_code, su.sampling_unit, string_agg(DISTINCT p1.observers || ': ' || p1.date::varchar,',') as p1, string_agg(DISTINCT p2.observers || ': ' || p2.date::varchar,',') as p2
FROM elc_sampling_units su 
LEFT JOIN p1 ON p1.program = su.program AND p1.grouping_code = su.grouping_code AND p1.sampling_unit = su.sampling_unit 
LEFT JOIN p2 ON p2.program = su.program AND p2.grouping_code = su.grouping_code AND p2.sampling_unit = su.sampling_unit 
WHERE su.program LIKE '%nera-beusnita_cbm%'  AND COALESCE(p1.date, p2.date) IS NOT NULL GROUP BY su.program, su.grouping_code, su.sampling_unit ORDER BY su.sampling_unit;

-- BUILA VANTURARITA
-- ellenőrzöm a rosz mfp-re feltöltött megfigyeléseket

WITH xx as (
SELECT DISTINCT 
    e.obm_id, su.program, su.grouping_code, su.sampling_unit, e.date, e.observers, st_distance(st_transform(sampling_unit_geometry, 3844), st_transform(surveyed_geometry, 3844)) as d
FROM elc_sampling_units su 
LEFT JOIN elc e ON e.program = su.program AND e.grouping_code = su.grouping_code AND e.sampling_unit = su.sampling_unit 
WHERE su.program = 'str_buila-vanturarita_cbm' ORDER BY d DESC, su.grouping_code, e.date, e.observers
)
SELECT observers, date, string_agg(obm_id::varchar, ',') FROM xx WHERE d > 200 GROUP BY observers, date;


-- Huțuleac Volosciuc Mihail Victor | 2023-05-08 | 107104,107105,107100,107101,107102,107103,107106,107099,107089,107090,107092,107093,107091 | ide van áthelyezve (csupán 600 méter)
-- Ghițu Adrian | 2023-05-22 | 114733,114728,114737,114736,114731,114735,114727,114730,114738,114734,114729,114732,114726 | ide van áthelyezve (csupán 600 méter)
-- Huțuleac Volosciuc Mihail Victor | 2023-05-09 | 107230,107231,107232,107233,107228,107229 | ide van áthelyezve
-- Miholcsa Tamás,Miholcsa Mátyás | 2023-05-10 | 107494 | megmagyarázhatatlanul ez az 1 adat az 1-es ponton van átírom
-- Lucian Grosu | 2023-05-22 | 114688,114679,114681,114682,114685,114689,114680,114683,114684,114687,114692,114691,114690,114686 | ide van áthelyezve
-- Ghițu Adrian | 2023-05-23 | 115380,115384,115383,115381,115382,115385 | ide van áthelyezve
-- Ghițu Adrian | 2023-05-08 | 107417,107415,107414,107413,107416,107418,107420,107419 | ide van áthelyezve
-- Huțuleac Volosciuc Mihail Victor | 2023-05-23 | 118795,118794,118796,118797,118777 | ide van áthelyezve

with p1 as (
    SELECT * FROM elc WHERE program = 'str_buila-vanturarita_cbm' AND date BETWEEN '2023-04-15' AND '2023-05-15'
),
p2 as (
    SELECT * FROM elc WHERE program = 'str_buila-vanturarita_cbm' AND date BETWEEN '2023-05-16' AND '2023-06-15'
)
SELECT DISTINCT su.program, su.grouping_code, su.sampling_unit, string_agg(DISTINCT p1.observers || ': ' || p1.date::varchar,',') as p1, string_agg(DISTINCT p2.observers || ': ' || p2.date::varchar,',') as p2
FROM elc_sampling_units su 
LEFT JOIN p1 ON p1.program = su.program AND p1.grouping_code = su.grouping_code AND p1.sampling_unit = su.sampling_unit 
LEFT JOIN p2 ON p2.program = su.program AND p2.grouping_code = su.grouping_code AND p2.sampling_unit = su.sampling_unit 
WHERE su.program LIKE '%buila-vanturarita_cbm%'  AND COALESCE(p1.date, p2.date) IS NOT NULL GROUP BY su.program, su.grouping_code, su.sampling_unit ORDER BY su.sampling_unit;

-- áthelyezések megfigyelőnként - just for fun
WITH xx as (
SELECT DISTINCT 
    e.obm_id, su.program, su.grouping_code, su.sampling_unit, e.date, e.observers, st_distance(st_transform(sampling_unit_geometry, 3844), st_transform(surveyed_geometry, 3844)) as d
FROM elc_sampling_units su 
LEFT JOIN elc e ON e.program = su.program AND e.grouping_code = su.grouping_code AND e.sampling_unit = su.sampling_unit 
WHERE su.program IN ('str_buila-vanturarita_cbm', 'str_domogled_cerna_cbm', 'str_nera-beusnita_cbm') ORDER BY d DESC, su.grouping_code, e.date, e.observers
)
SELECT observers, d FROM xx;

-- d <- read.csv('athelyezesek_megfigyelonkent.csv')
-- d$observers[d$observers == 'Miholcsa Tamás,Miholcda Mátyás Botond'] <- 'Miholcsa Tamás'
-- d$observers[d$observers == 'Miholcsa Tamás,Miholcsa Mátyás'] <- 'Miholcsa Tamás'
-- d$observers <- as.factor(d$observers)
-- ggplot(d, aes(x=observers, y=avg)) + geom_violin()+ theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))
-- ggsave(filename='athelyezesek_megfigyelonkent.png')

create table temporary_tables.elc_list_times as SELECT metadata->>'observation_list_id' as list_id, metadata->>'observation_list_start' as list_start, metadata->>'observation_list_end' as list_end
FROM system.uploadings 
WHERE project_table = 'elc' AND metadata::jsonb ? 'measurements_num';




WITH xx as (
SELECT DISTINCT 
    e.obm_id, 
    su.program, 
    su.grouping_code, 
    su.sampling_unit, 
    e.date, 
    e.observers, 
    st_distance(st_transform(sampling_unit_geometry, 3844), st_transform(surveyed_geometry, 3844)) as distance_from_sampling_unit,
    st_x(obm_geometry) as x,
    st_y(obm_geometry) as y,
    st_x(surveyed_geometry) as x_auto,
    st_y(surveyed_geometry) as y_auto,
    species_valid,
    number,
    gender,
    age,
    status_of_individuals,
    distance_code,
    e.exact_time,
    secondary_time,
    wind_force,
    cloud_cover,
    e.altitude,
    e.corine_code,
    comments_general,
    comments_observation,
    obm_observation_list_id,
    to_timestamp((u.metadata->>'finished_at')::bigint/1000) AT TIME ZONE 'Europe/Bucharest' as record_timestamp,
    ol.obsstart as list_start,
    ol.obsend as list_end
FROM elc_sampling_units su 
LEFT JOIN elc e ON e.program = su.program AND e.grouping_code = su.grouping_code AND e.sampling_unit = su.sampling_unit 
LEFT JOIN system.uploadings u ON e.obm_uploading_id = u.id
LEFT JOIN elc_observation_list ol ON ol.oidl = e.obm_observation_list_id
WHERE e.obm_id IS NOT NULL AND su.program IN ('str_buila-vanturarita_cbm', 'str_domogled_cerna_cbm', 'str_nera-beusnita_cbm') ORDER BY su.grouping_code, su.sampling_unit, e.date, e.exact_time
)
SELECT 
    *,
    list_end - list_start as list_duration,
    record_timestamp - list_start as time_from_list_start,
    case 
        when record_timestamp - list_start < '00:05:00'::time then 1 
        when record_timestamp - list_start < '00:07:30'::time then 2
        when record_timestamp - list_start < '00:10:00'::time then 3
        else 4
    end as period_auto
FROM xx;


with p1 as (
    SELECT * FROM elc WHERE program LIKE 'str_%_cbm' AND date BETWEEN '2023-04-15' AND '2023-05-15'
),
p2 as (
    SELECT * FROM elc WHERE program LIKE 'str_%_cbm' AND date BETWEEN '2023-05-16' AND '2023-06-15'
)
SELECT DISTINCT su.program, su.grouping_code, su.sampling_unit, string_agg(DISTINCT p1.observers || ': ' || p1.date::varchar,',') as p1, string_agg(DISTINCT p2.observers || ': ' || p2.date::varchar,',') as p2
FROM elc_sampling_units su 
LEFT JOIN p1 ON p1.program = su.program AND p1.grouping_code = su.grouping_code AND p1.sampling_unit = su.sampling_unit 
LEFT JOIN p2 ON p2.program = su.program AND p2.grouping_code = su.grouping_code AND p2.sampling_unit = su.sampling_unit 
WHERE su.program LIKE 'str_%_cbm'  AND COALESCE(p1.date, p2.date) IS NOT NULL GROUP BY su.program, su.grouping_code, su.sampling_unit ORDER BY su.sampling_unit;


BF3512

begin;
WITH yy as (
SELECT DISTINCT sampling_unit, ST_Centroid(ST_Collect(surveyed_geometry)) as c FROM elc WHERE program LIKE '%buila-vanturarita_cbm%' AND grouping_code = 'BF3512' AND date != '2023-05-08' GROUP BY sampling_unit
)
UPDATE elc SET obm_geometry = c, surveyed_geometry = c FROM yy WHERE program LIKE '%buila-vanturarita_cbm%' AND grouping_code = 'BF3512' AND date = '2023-05-08' AND yy.sampling_unit = elc.sampling_unit;
