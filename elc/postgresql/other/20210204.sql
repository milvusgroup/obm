BEGIN;
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.558997 44.705755)', 4326) WHERE nest_name = 'L1' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.558642 44.706408)', 4326) WHERE nest_name = 'L2' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.558310 44.706520)', 4326) WHERE nest_name = 'L3' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.557774 44.706415)', 4326) WHERE nest_name = 'L4' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.558607 44.706182)', 4326) WHERE nest_name = 'L5' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.558705 44.705980)', 4326) WHERE nest_name = 'L6' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.557754 44.705565)', 4326) WHERE nest_name = 'U1' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.557866 44.705792)', 4326) WHERE nest_name = 'U2' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.558188 44.706301)', 4326) WHERE nest_name = 'U3' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
UPDATE elc_nests SET obm_geometry = St_GeomFromText('POINT(22.558252 44.706086)', 4326) WHERE nest_name = 'U4' AND program = 'PN Porțile de Fier: B31C Odvak/Scorburi';
END;

-- Törlendő hibás tampon zóna fák, obm_id: 67692, 67700, 67696 Ezek egész biztosan rossz helyen vannak, vagy véletlenül nyomták be, vagy ha van hozzá fénykép is, akkor rossz a koordináta, kérlek próbáld meg kibogozni, hogy mi a helyzet!
-- Törlendő hibás öreg fák, obm_id:  67695, 67694
-- teszt adatok 
select 'sudo rm ' || reference from system.files where id in (SELECT file_id FROM system.file_connect WHERE conid in (SELECT obm_files_id FROM elc WHERE obm_id IN (67700, 67692, 67695, 67694, 67693)));
DELETE from system.files where id in (SELECT file_id FROM system.file_connect WHERE conid in (SELECT obm_files_id FROM elc WHERE obm_id IN (67700, 67692, 67695, 67694, 67693)));
DELETE FROM system.file_connect WHERE conid in (SELECT obm_files_id FROM elc WHERE obm_id IN (67700, 67692, 67695, 67694, 67693));
DELETE FROM elc WHERE obm_id IN (67700, 67692, 67695, 67694, 67693);


-- Kiexportálni a str_b31b_tree_mark összes fényképét egy csomagba, úgy, hogy a 
-- képek neve az obm_id + a koordináta (5 tizedes pontossággal) legyen Esetleg 
-- ha bele lehet még variálni a kép nevébe, hogy öreg fa vagy tampon zóna 
-- (AB – arbore batran, TZ-tampon zóna), Pl valami ilyesmi legyen egy kép név:  
-- AB_67774_E22-45672_N41-56874.jpg stb… (le kell adjuk a jelentéshez 
-- bizonyítéknak minden jelölt fa koordinátáját és fényképét)

COPY (WITH d as (
SELECT 
    obm_id,
    file_id, 
    CASE 
        WHEN grouping_code = 'tampon zóna' THEN 'TZ'
        WHEN grouping_code = 'öreg fa' THEN 'AB'
    END as gc,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM elc 
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'PN Porțile de Fier: B31B fajelölés' AND 
    obm_files_id IS NOT NULL AND 
    date > '2021-02-04'
)
SELECT 'cp /var/www/html/biomaps/projects/elc/attached_files/' || reference || ' /tmp/elc_b31b/' || gc || '_' || obm_id || '_E' || x || '_N' || y || '.jpg' FROM d)
TO '/tmp/elc_b31b.sh' CSV;
SELECT * FROM d;

-- a 67721 obm_id rekordnál két kép van, ezért nem talál a lekérdezés 113 és a kiexportált 112 kép, de képen ugyanaz van

UPDATE elc SET obm_geometry = St_GeomFromText('POINT(21.83832 44.66774)', 4326) WHERE obm_id = 67696;





COPY (WITH d as (
SELECT 
    obm_id,
    file_id, 
    CASE 
        WHEN grouping_code = 'tampon zóna' THEN 'TZ'
        WHEN grouping_code = 'öreg fa' THEN 'AB'
    END as gc,
    ST_X(obm_geometry) as x,
    ST_y(obm_geometry) as y,
    reference
FROM elc 
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    program = 'PN Porțile de Fier: B31B fajelölés' AND 
    obm_files_id IS NOT NULL AND 
    date > '2021-02-04'
)
SELECT * FROM d) TO /tmp/elc_b31b_adatok.csv CSV HEADER;
