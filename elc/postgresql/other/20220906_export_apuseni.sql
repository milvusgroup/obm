CREATE TABLE shared.eunis_species_codes (
    id SERIAL,
    species varchar,
    eunis integer,
    n2000 varchar
);
select * from shared.eunis_species_codes;

-- inserting the eunis and n2000 codes into the OBM taxon_table
with eunisjoined as (
    SELECT DISTINCT m0.taxon_id, m1.word, e.eunis, e.n2000
    FROM milvus_taxon m0 
    LEFT JOIN milvus_taxonmeta tm on tm.taxon_id = m0.taxon_id 
    LEFT JOIN milvus_taxon m1 ON m0.taxon_id = m1.taxon_id
    LEFT JOIN shared.eunis_species_codes e ON m1.word = e.species
    WHERE tm.rank = 'species' AND m0.status = 'accepted' AND m0.lang = 'species_valid'
)
INSERT INTO milvus_taxon (taxon_id, word, lang, status)
SELECT DISTINCT ok.taxon_id, ok.n2000 as word, 'n2000', 'accepted' 
FROM eunisjoined as hi
LEFT JOIN eunisjoined as ok ON hi.taxon_id = ok.taxon_id AND ok.eunis is not null
where hi.eunis is null AND ok.eunis IS NOT NULL; 

-- copying eunis and n2000 codes from milvus_taxon to elc_taxon

with onlywitheunis as (
    SELECT DISTINCT m0.taxon_id, m1.word, m0.word as eunis
    FROM milvus_taxon m0 
    LEFT JOIN milvus_taxon m1 ON m0.taxon_id = m1.taxon_id
    WHERE m0.status = 'accepted' AND m0.lang = 'eunis'
)
INSERT INTO elc_taxon (taxon_id, word, lang, status)
SELECT DISTINCT e.taxon_id, eunis, 'eunis', 'accepted'
FROM onlywitheunis o
LEFT JOIN elc_taxon e ON o.word = e.word
WHERE e.taxon_id IS NOT NULL;-- AND eunis = '997';

with onlywithn2000 as (
    SELECT DISTINCT m0.taxon_id, m1.word, m0.word as n2000
    FROM milvus_taxon m0 
    LEFT JOIN milvus_taxon m1 ON m0.taxon_id = m1.taxon_id AND m1.lang != 'species_euring'
    WHERE m0.status = 'accepted' AND m0.lang = 'n2000'
)
INSERT INTO elc_taxon (taxon_id, word, lang, status)
SELECT DISTINCT e.taxon_id, n2000, 'n2000', 'accepted'
FROM onlywithn2000 o
LEFT JOIN elc_taxon e ON o.word = e.word
WHERE e.taxon_id IS NOT NULL;-- AND n2000 = 'A031';



------------------------------------------------------
-- SpeciesDistribution
------------------------------------------------------

SELECT DISTINCT 
    CASE WHEN eunis IS NOT NULL THEN eunis ELSE species END AS codeeunis, 
    'P' as population, 
    0.0 as areah, 
    'ha' as areaunit, 
    ''::varchar as location, 
    count(*) as popmin, 
    count(*) as popmax, 
    'i' as unit, 
    'medie' as quality, 
    ''::char(1) as density, 
    ''::char(1) as sensitive, 
    ''::varchar as notes, 
    'ROSPA0081' as sitecode, 
    0 as managplan 
FROM elc 
WHERE projekt = 'Apuseni' AND species NOT IN ('Lucanus cervus', 'null') AND date < '2022-01-01'
GROUP BY eunis, species;

------------------------------------------------------
-- ImpactsDistribution
------------------------------------------------------

SELECT 
    st_astext(obm_geometry) as wkt,
    split_part(COALESCE(impact_l4, COALESCE(impact_l3, COALESCE(impact_l2, impact_l1))), ' ', 1) as impactcode, 
    CASE impact_type when 'str_presiune' THEN 'P' WHEN 'str_amenintare' THEN 'A' ELSE '' END as type, 
    ''::varchar as location, 
    CASE impact_intensity WHEN 'str_high' THEN 'R' WHEN 'str_medium' THEN 'M' WHEN 'str_low' THEN 'S' ELSE '' END as intensity, 
    'P' as sensitive, 
    ''::varchar as notes, 
    'ROSPA0081' as sitecode, 
    0 as managplan 
FROM elc_threats 
WHERE project = 'Apuseni';

\copy (SELECT st_astext(obm_geometry) as wkt, split_part(COALESCE(impact_l4, COALESCE(impact_l3, COALESCE(impact_l2, impact_l1))), ' ', 1) as impactcode, CASE impact_type when 'str_presiune' THEN 'P' WHEN 'str_amenintare' THEN 'A' ELSE '' END as type, ''::varchar as location, CASE impact_intensity WHEN 'str_high' THEN 'R' WHEN 'str_medium' THEN 'M' WHEN 'str_low' THEN 'S' ELSE '' END as intensity, 'P' as sensitive, ''::varchar as notes, 'ROSPA0081' as sitecode, 0 as managplan FROM elc_threats WHERE project = 'Apuseni') TO '/tmp/ImpactsDistribution.csv' CSV HEADER;


------------------------------------------------------
-- BirdObservations
------------------------------------------------------

SELECT 
    st_astext(obm_geometry) as wkt,
    species_valid, eunis, number as numar, observed_unit, gender, age, date, observers, method, grouping_code, sampling_unit,
    lpad((time_of_start / 59)::varchar, 2, '0') || ':' || lpad((time_of_start % 60)::varchar, 2, '0') as time_of_start,
    lpad((time_of_end / 59)::varchar, 2, '0') || ':' || lpad((time_of_end % 60)::varchar, 2, '0') as time_of_end,
    lpad((duration / 59)::varchar, 2, '0') || ':' || lpad((duration % 60)::varchar, 2, '0') as duration
FROM elc
WHERE projekt = 'Apuseni' AND species NOT IN ('Lucanus cervus')
ORDER BY method, date;


\copy (SELECT st_astext(obm_geometry) as wkt, species_valid, eunis, number as numar, observed_unit, gender, age, date, observers, method, grouping_code, sampling_unit, lpad((time_of_start / 60)::varchar, 2, '0') || ':' || lpad((time_of_start % 60)::varchar, 2, '0') as time_of_start, lpad((time_of_end / 60)::varchar, 2, '0') || ':' || lpad((time_of_end % 60)::varchar, 2, '0') as time_of_end, lpad((duration / 60)::varchar, 2, '0') || ':' || lpad((duration % 60)::varchar, 2, '0') as duration FROM elc WHERE projekt = 'Apuseni' AND species NOT IN ('Lucanus cervus') ORDER BY method, date) TO '/tmp/BirdObservations.csv' CSV HEADER;

------------------------------------------------------
-- ObservationPoints
------------------------------------------------------

SELECT st_astext(sampling_unit_geometry) as wkt, method, program, grouping_code, sampling_unit FROM elc_sampling_units WHERE program LIKE '%apuseni%';
\copy (SELECT st_astext(sampling_unit_geometry) as wkt, method, program, grouping_code, sampling_unit FROM elc_sampling_units WHERE program LIKE '%apuseni%') to '/tmp/ObservationPoints.csv' CSV HEADER;
