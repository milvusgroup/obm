BEGIN;
UPDATE elc SET 
    projekt = 'Magura Odobesti', 
    program = 'Magura Odobesti: Observații ocazionale' 
WHERE obm_id IN (90629, 90630, 90631, 90632, 90633, 90634, 90635, 90636, 90637, 
    90638, 90639, 90640, 90641, 90642, 90643, 90644, 90645, 90809, 91140, 91143,
    91474, 91475, 91503, 91504, 91505, 91506, 91507, 91508, 91509, 91510, 91511, 
    91512, 92595, 92596, 92597, 92598, 92599, 92600, 92601, 92602, 92603, 92604, 
    92605, 92606, 92607, 92608, 92609, 92610, 92611, 92612, 92613, 92614, 92615, 
    92616, 92617, 92618, 92619);
COMMIT;

UPDATE elc_habitat SET projekt = 'Magura Odobesti' WHERE obm_id NOT IN (5, 97);

UPDATE elc_nests SET projekt = 'Magura Odobest' WHERE program = 'Magura Odobesti: Căutare cuiburi';
    

COPY (
    WITH d as (
        SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'observatii' as tbl FROM elc WHERE projekt = 'Magura Odobesti' AND obm_files_id IS NOT NULL 
        UNION
        SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'habitat' as tbl FROM elc_habitat WHERE projekt = 'Magura Odobesti' AND obm_files_id IS NOT NULL 
        UNION
        SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'presiuni_amenintari' as tbl FROM elc_threats WHERE project = 'Magura Odobesti' AND obm_files_id IS NOT NULL 
        UNION
        SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'cuiburi' as tbl FROM elc_nests WHERE projekt = 'Magura Odobesti' AND obm_files_id IS NOT NULL 
    ),
    dd as (
        SELECT 
            obm_id, tbl, reference,
            SUBSTRING(REPLACE(x::text, '.', '-') from 1 for 8) as x, 
            SUBSTRING(REPLACE(y::text, '.', '-') from 1 for 8) as y
        FROM d
        LEFT JOIN system.file_connect fc ON d.obm_files_id = conid
        LEFT JOIN system.files f ON f.id = fc.file_id
    )
    SELECT 'cp /var/www/html/biomaps/root-site/projects/elc/attached_files/' || reference || ' /tmp/elc_magura_odobest/' || tbl || '_' || obm_id || '_' || x || '_' || y || '.jpg' FROM dd
)
TO '/tmp/mo_kepek_masolas.sh' CSV;

\copy ( WITH d as ( SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'observatii' as tbl FROM elc WHERE projekt = 'Magura Odobesti' AND obm_files_id IS NOT NULL UNION SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'habitat' as tbl FROM elc_habitat WHERE projekt = 'Magura Odobesti' AND obm_files_id IS NOT NULL UNION SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'presiuni_amenintari' as tbl FROM elc_threats WHERE project = 'Magura Odobesti' AND obm_files_id IS NOT NULL UNION SELECT obm_id, obm_files_id, ST_X(ST_Centroid(obm_geometry)) as x, ST_Y(ST_Centroid(obm_geometry)) as y, 'cuiburi' as tbl FROM elc_nests WHERE projekt = 'Magura Odobesti' AND obm_files_id IS NOT NULL ), dd as ( SELECT obm_id, tbl, reference, SUBSTRING(REPLACE(x::text, '.', '-') from 1 for 8) as x, SUBSTRING(REPLACE(y::text, '.', '-') from 1 for 8) as y FROM d LEFT JOIN system.file_connect fc ON d.obm_files_id = conid LEFT JOIN system.files f ON f.id = fc.file_id) SELECT 'cp /home/gabor/obm/obm-composer/local/elc/local/attached_files/' || reference || ' /tmp/elc_magura_odobest/' || tbl || '_' || obm_id || '_' || x || '_' || y || '.jpg' FROM dd) TO '/tmp/mo_kepek_masolas.sh' CSV;
