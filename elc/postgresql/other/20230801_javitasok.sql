SELECT * FROM elc WHERE (program, grouping_code, sampling_unit, observers, date) IN (SELECT program, grouping_code, sampling_unit, observers, date FROM elc_observation_events WHERE uid = 1736);

\copy elc (projekt,program,grouping_code,sampling_unit,date,species,number,gender,age,observers,method,time_of_start,time_of_end,duration,complete_list,wind_force,cloud_cover,observed_unit,count_precision,obm_access,distance_m,map_code,habitat,timing_of_reaction,response_type,comments_observation,list_id,exact_time) from /h

-- NERA DENSYR
-- 112292 áthelyezve a kijelölt mfp geometriára, mert valószínűleg elfelejtette a ponton rögzíteni és a hotelből töltötte fel.

-- NERA CBM - át volt nézve előzőleg

-- HARKALY - rendbetéve néhány megfigyelésnél rosszul volt a mfp kiválasztva, egy helyen utólag volt a null adat felvéve

-- HARIS - minden rendben

-- VONULAS - volt egy eliras - javitva

-- RAGADOZO - Ghitu Adrian es Lucian Grosu a 3-as és 4-es pontokat jól áthelyezték, ráadásul el is volt írva


-- BUILA
-- harkály - rendbetéve
-- uhu - hiányoznak az adatok a BB_4-ről
-- cbm - át volt nézve előzőleg, de a Hutuleak observadós adataiból hiányzik sok infó
-- szikla - elvileg rendben van
-- gatyas - 15 uj pontot betettem, adatokat modositottam
-- szirti - rendben
-- ragadozó - rendben


-- DOMOGLED
-- uhu - rendben
-- cbm - rendezve elozoleg
-- szikla - rendben
-- csaszarmadar - rendben
-- haris - rendben
-- balkani - rendben
-- harkaly - rendben
-- szirti - rendben
-- ragadozo - rendben
-- urali - rendben

