SELECT DISTINCT observers, projekt, program, method FROM elc e LEFT JOIN system.uploadings u ON e.obm_uploading_id = u.id WHERE u.uploader_name IN ('Miholcsa Tamás', 'Kelemen A. Márton', 'Kiss Arnold', 'Kovács István') AND date BETWEEN '2023-07-12' AND '2023-07-15';

BEGIN; UPDATE elc SET projekt = 'Jiu-Dunărea', program = 'Jiu-Dunărea: Observații ocazionale' WHERE observers = 'Miholcsa Tamás' AND date BETWEEN '2023-07-12' AND '2023-07-15' AND method = 'Observații ocazionale';

SELECT DISTINCT uploader_name FROM system.uploadings WHERE project_table = 'elc';
