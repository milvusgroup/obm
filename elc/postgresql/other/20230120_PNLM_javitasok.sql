-- 16:20
-- Térbeli ellenőrés után: a mostani adatok at a WHERE date BETWEEN '2023-01-13' AND '2023-01-18'; feltétellel lehet lekérdezni

-- projekt nem mindenhol PN Lunca Muresului
UPDATE elc SET projekt = 'PN Lunca Muresului' WHERE date BETWEEN '2023-01-13' AND '2023-01-18';

-- programok javêtása
UPDATE elc SET program = 'PN Lunca Muresului: Observații ocazionale' WHERE date BETWEEN '2023-01-13' AND '2023-01-18' AND program = 'În afara proiectelor: Observații ocazionale';
UPDATE elc SET program = 'str_pn_lunca_muresului_tree_marking' WHERE date BETWEEN '2023-01-13' AND '2023-01-18' AND program = 'PN Porțile de Fier: B31B fajelölés';
UPDATE elc SET program = 'str_pn_lunca_muresului_tree_marking' WHERE date BETWEEN '2023-01-13' AND '2023-01-18' AND program = 'str_magura_odobesti_tree_marking';
UPDATE elc SET program = 'str_pn_lunca_muresului_nest_search' WHERE date BETWEEN '2023-01-13' AND '2023-01-18' AND program = 'str_casual_nest_survey';

-- módszer javítása
UPDATE elc SET method = 'str_tree_marking' WHERE date BETWEEN '2023-01-13' AND '2023-01-18' AND method = 'Kazán-szoros B31b fajelölés';

SELECT DISTINCT species_valid, program FROM elc  WHERE date BETWEEN '2023-01-13' AND '2023-01-18' ORDER by program, species_valid;
select * from elc_nests where nest_name = 'PNLM_PO_03';

-- dupla fészkek
SELECT distinct nest_name, projekt FROM elc_nests WHERE projekt = 'PN Lunca Muresului' ORDER BY nest_name;
select count(*) from elc where nest_id = 'PNLM_HZS_07';


-- Fészkek neveinek rendberakása

\copy (SELECT distinct nest_name as elc_nests_nest_name, nest_id as elc_nest_id FROM elc_nests n LEFT JOIN elc e ON n.projekt = e.projekt AND nest_name = nest_id WHERE n.projekt = 'PN Lunca Muresului' ORDER BY nest_name) to /home/gabor/Documents/motolla/ecologic/PNLM/feszek_helyzet_20230216.csv CSV HEADER;

WITH asdf as (SELECT distinct 
    nest_name as elc_nests_nest_name, 
    nest_id as elc_nest_id, 
    CASE observer
        WHEN 'Kiss Arnold' THEN 'KA'
        WHEN 'Zeitz Róbert' THEN 'ZR'
        WHEN 'Miholcsa Tamás' THEN 'MT'
        WHEN 'Hegyeli Zsolt' THEN 'HZ'
        WHEN 'Iacob George' THEN 'IG'
        WHEN 'Komáromi István' THEN 'KI'
        WHEN 'Páczai Örs' THEN 'PO'
    END AS init,
    u.metadata->>'started_at' as started_at
FROM elc_nests n 
LEFT JOIN elc e ON n.projekt = e.projekt AND nest_name = nest_id 
LEFT JOIN system.uploadings u ON n.obm_uploading_id = u.id 
WHERE n.projekt = 'PN Lunca Muresului' ),
atnev as (SELECT 
    elc_nests_nest_name,
    'LMs_' || init || '_' || lpad((ROW_NUMBER() OVER(partition by init order by init, started_at))::text, 2, '0'::text) as rn
FROM asdf
ORDER BY init, started_at, elc_nests_nest_name)
SELECT * FROM atnev;
-- UPDATE elc_nests SET nest_name = rn FROM atnev WHERE projekt = 'PN Lunca Muresului' AND nest_name = elc_nests_nest_name;
-- UPDATE elc SET nest_id = rn FROM atnev WHERE projekt = 'PN Lunca Muresului' AND nest_id = elc_nests_nest_name;


\copy (WITH asdf as (SELECT distinct nest_name as elc_nests_nest_name, nest_id as elc_nest_id, CASE observer WHEN 'Kiss Arnold' THEN 'KA' WHEN 'Zeitz Róbert' THEN 'ZR' WHEN 'Miholcsa Tamás' THEN 'MT' WHEN 'Hegyeli Zsolt' THEN 'HZ' WHEN 'Iacob George' THEN 'IG' WHEN 'Komáromi István' THEN 'KI' WHEN 'Páczai Örs' THEN 'PO' END AS init, u.metadata->>'started_at' as started_at FROM elc_nests n LEFT JOIN elc e ON n.projekt = e.projekt AND nest_name = nest_id LEFT JOIN system.uploadings u ON n.obm_uploading_id = u.id WHERE n.projekt = 'PN Lunca Muresului' ) SELECT elc_nests_nest_name, 'LMs_' || init || '_' || lpad((ROW_NUMBER() OVER(partition by init order by init, started_at))::text, 2, '0'::text) as rn FROM asdf ORDER BY init, started_at, elc_nests_nest_name) TO /home/gabor/Documents/motolla/ecologic/PNLM/feszek_atnevezes.csv CSV HEADER;
