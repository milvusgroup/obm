-- macskabagoly, a kihelyezett koordinátákkal
SELECT 
    sampling_unit as plot_nr, 
    st_x(obm_geometry) as x,
    st_y(obm_geometry) as y,
    st_x(st_transform(obm_geometry, 3844)) as xcoord,
    st_y(st_transform(obm_geometry, 3844)) as ycoord,
    'Strix aluco' as species,
    CASE species 
        WHEN 'null' THEN 0 
        ELSE 1 
    END as presence,
    extract(year from date) as session
FROM elc
WHERE 
    projekt = 'Nagyhagymás' AND 
    program = 'str_nagyhagymas_strura' AND 
    species IN ('null', 'Strix aluco')
ORDER by session, plot_nr;

\copy (SELECT sampling_unit as plot_nr, st_x(obm_geometry) as x, st_y(obm_geometry) as y, st_x(st_transform(obm_geometry, 3844)) as xcoord, st_y(st_transform(obm_geometry, 3844)) as ycoord, 'Strix aluco' as species, CASE species WHEN 'null' THEN 0 ELSE 1 END as presence, extract(year from date) as session FROM elc WHERE projekt = 'Nagyhagymás' AND program = 'str_nagyhagymas_strura' AND species IN ('null', 'Strix aluco') ORDER by session, plot_nr) TO /home/gabor/Documents/motolla/ecologic/nagyhagymas/trim/stralu_kihelyezett_geometria.csv CSV HEADER;

-- urali bagoly, a kihelyezett koordinátákkal
SELECT 
    sampling_unit as plot_nr, 
    st_x(obm_geometry) as x,
    st_y(obm_geometry) as y,
    st_x(st_transform(obm_geometry, 3844)) as xcoord,
    st_y(st_transform(obm_geometry, 3844)) as ycoord,
    'Strix uralensis' as species,
    CASE species 
        WHEN 'null' THEN 0 
        ELSE 1 
    END as presence,
    extract(year from date) as session
FROM elc
WHERE 
    projekt = 'Nagyhagymás' AND 
    program = 'str_nagyhagymas_strura' AND 
    species IN ('null', 'Strix uralensis')
ORDER by session, plot_nr;

\copy (SELECT sampling_unit as plot_nr, st_x(obm_geometry) as x, st_y(obm_geometry) as y, st_x(st_transform(obm_geometry, 3844)) as xcoord, st_y(st_transform(obm_geometry, 3844)) as ycoord, 'Strix uralensis' as species, CASE species WHEN 'null' THEN 0 ELSE 1 END as presence, extract(year from date) as session FROM elc WHERE projekt = 'Nagyhagymás' AND program = 'str_nagyhagymas_strura' AND species IN ('null', 'Strix uralensis') ORDER by session, plot_nr) TO /home/gabor/Documents/motolla/ecologic/nagyhagymas/trim/strura_kihelyezett_geometria.csv CSV HEADER;

-- törpekuvik, a kihelyezett koordinátákkal
SELECT 
    sampling_unit as plot_nr, 
    st_x(obm_geometry) as x,
    st_y(obm_geometry) as y,
    st_x(st_transform(obm_geometry, 3844)) as xcoord,
    st_y(st_transform(obm_geometry, 3844)) as ycoord,
    'Glaucidium passerinum' as species,
    CASE species 
        WHEN 'null' THEN 0 
        ELSE 1 
    END as presence,
    extract(year from date) as session
FROM elc
WHERE 
    projekt = 'Nagyhagymás' AND 
    program = 'str_nagyhagymas_glapas' AND 
    species IN ('null', 'Glaucidium passerinum')
ORDER by session, plot_nr;

\copy (SELECT sampling_unit as plot_nr, st_x(obm_geometry) as x, st_y(obm_geometry) as y, st_x(st_transform(obm_geometry, 3844)) as xcoord, st_y(st_transform(obm_geometry, 3844)) as ycoord, 'Glaucidium passerinum' as species, CASE species WHEN 'null' THEN 0 ELSE 1 END as presence, extract(year from date) as session FROM elc WHERE projekt = 'Nagyhagymás' AND program = 'str_nagyhagymas_glapas' AND species IN ('null', 'Glaucidium passerinum') ORDER by session, plot_nr) to /home/gabor/Documents/motolla/ecologic/nagyhagymas/trim/glapas_kihelyezett_geometria.csv CSV HEADER;


-- macskabagoly, a mfp koordinátákkal
SELECT 
    e.sampling_unit as plot_nr, 
    st_x(sampling_unit_geometry) as x,
    st_y(sampling_unit_geometry) as y,
    st_x(st_transform(sampling_unit_geometry, 3844)) as xcoord,
    st_y(st_transform(sampling_unit_geometry, 3844)) as ycoord,
    'Strix aluco' as species,
    CASE species 
        WHEN 'null' THEN 0 
        ELSE 1 
    END as presence,
    extract(year from date) as session
FROM elc e
LEFT JOIN elc_sampling_units su ON e.program = su.program AND e.sampling_unit = su.sampling_unit
WHERE 
    e.projekt = 'Nagyhagymás' AND 
    e.program = 'str_nagyhagymas_strura' AND 
    e.species IN ('null', 'Strix aluco')
ORDER by session, plot_nr;

\copy (SELECT e.sampling_unit as plot_nr, st_x(sampling_unit_geometry) as x, st_y(sampling_unit_geometry) as y, st_x(st_transform(sampling_unit_geometry, 3844)) as xcoord, st_y(st_transform(sampling_unit_geometry, 3844)) as ycoord, 'Strix aluco' as species, CASE species WHEN 'null' THEN 0 ELSE 1 END as presence, extract(year from date) as session FROM elc e LEFT JOIN elc_sampling_units su ON e.program = su.program AND e.sampling_unit = su.sampling_unit WHERE e.projekt = 'Nagyhagymás' AND e.program = 'str_nagyhagymas_strura' AND e.species IN ('null', 'Strix aluco') ORDER by session, plot_nr) TO /home/gabor/Documents/motolla/ecologic/nagyhagymas/trim/stralu_mfp_geometria.csv CSV HEADER;

-- urali bagoly, a mfp koordinátákkal
SELECT 
    e.sampling_unit as plot_nr, 
    st_x(sampling_unit_geometry) as x,
    st_y(sampling_unit_geometry) as y,
    st_x(st_transform(sampling_unit_geometry, 3844)) as xcoord,
    st_y(st_transform(sampling_unit_geometry, 3844)) as ycoord,
    'Strix uralensis' as species,
    CASE species 
        WHEN 'null' THEN 0 
        ELSE 1 
    END as presence,
    extract(year from date) as session
FROM elc e
LEFT JOIN elc_sampling_units su ON e.program = su.program AND e.sampling_unit = su.sampling_unit
WHERE 
    e.projekt = 'Nagyhagymás' AND 
    e.program = 'str_nagyhagymas_strura' AND 
    e.species IN ('null', 'Strix uralensis')
ORDER by session, plot_nr;

\copy (SELECT e.sampling_unit as plot_nr, st_x(sampling_unit_geometry) as x, st_y(sampling_unit_geometry) as y, st_x(st_transform(sampling_unit_geometry, 3844)) as xcoord, st_y(st_transform(sampling_unit_geometry, 3844)) as ycoord, 'Strix uralensis' as species, CASE species WHEN 'null' THEN 0 ELSE 1 END as presence, extract(year from date) as session FROM elc e LEFT JOIN elc_sampling_units su ON e.program = su.program AND e.sampling_unit = su.sampling_unit WHERE e.projekt = 'Nagyhagymás' AND e.program = 'str_nagyhagymas_strura' AND e.species IN ('null', 'Strix uralensis') ORDER by session, plot_nr) TO /home/gabor/Documents/motolla/ecologic/nagyhagymas/trim/strura_mfp_geometria.csv CSV HEADER;

-- torpekuvik, a mfp koordinátákkal
SELECT 
    e.sampling_unit as plot_nr, 
    st_x(sampling_unit_geometry) as x,
    st_y(sampling_unit_geometry) as y,
    st_x(st_transform(sampling_unit_geometry, 3844)) as xcoord,
    st_y(st_transform(sampling_unit_geometry, 3844)) as ycoord,
    'Glaucidium passerinum' as species,
    CASE species 
        WHEN 'null' THEN 0 
        ELSE 1 
    END as presence,
    extract(year from date) as session
FROM elc e
LEFT JOIN elc_sampling_units su ON e.program = su.program AND e.sampling_unit = su.sampling_unit
WHERE 
    e.projekt = 'Nagyhagymás' AND 
    e.program = 'str_nagyhagymas_glapas' AND 
    e.species IN ('null', 'Glaucidium passerinum')
ORDER by session, plot_nr;

\copy (SELECT e.sampling_unit as plot_nr, st_x(sampling_unit_geometry) as x, st_y(sampling_unit_geometry) as y, st_x(st_transform(sampling_unit_geometry, 3844)) as xcoord, st_y(st_transform(sampling_unit_geometry, 3844)) as ycoord, 'Glaucidium passerinum' as species, CASE species WHEN 'null' THEN 0 ELSE 1 END as presence, extract(year from date) as session FROM elc e LEFT JOIN elc_sampling_units su ON e.program = su.program AND e.sampling_unit = su.sampling_unit WHERE e.projekt = 'Nagyhagymás' AND e.program = 'str_nagyhagymas_glapas' AND e.species IN ('null', 'Glaucidium passerinum') ORDER by session, plot_nr) TO /home/gabor/Documents/motolla/ecologic/nagyhagymas/trim/glapas_mfp_geometria.csv CSV HEADER;
