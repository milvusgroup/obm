------------------------------------------------------
-- SpeciesDistribution
------------------------------------------------------

SELECT DISTINCT 
    ''::varchar as cod_punct,
    CASE WHEN eunis IS NOT NULL THEN eunis ELSE species END AS codul_speciei, 
    ''::varchar as tipul_populatiei, 
    0.0 as suprafata_habitatului, 
    'ha' as um_suprafata_habitatului, 
    ''::varchar as descrierea_localizarii, 
    sum(number) as marimea_populatiei_lim_inf, 
    sum(number) as marimea_populatiei_lim_sup, 
    'i' as um_tip_populatie, 
    'Insuficientă' as calitatea_datelor, 
    ''::char(1) as clasa_densitatii, 
    ''::char(1) as confidentialitate, 
    ''::varchar as alte_detalii, 
    'ROSPA0025' as codul_sitului, 
    0 as versiunea_planului_de_management 
FROM elc 
WHERE projekt = 'Buila-Vânturarița' AND species_valid != 'null'
GROUP BY eunis, species;

------------------------------------------------------
-- BirdObservations
------------------------------------------------------

SELECT 
    st_astext(obm_geometry) as wkt,
    species_valid, eunis, number as numar, observed_unit, gender, age, date, observers, method, grouping_code, sampling_unit,
    lpad((time_of_start / 60)::varchar, 2, '0') || ':' || lpad((time_of_start % 60)::varchar, 2, '0') as time_of_start, lpad((time_of_end / 60)::varchar, 2, '0') || ':' || lpad((time_of_end % 60)::varchar, 2, '0') as time_of_end, lpad((duration / 60)::varchar, 2, '0') || ':' || lpad((duration % 60)::varchar, 2, '0') as duration
FROM elc
WHERE projekt = 'Buila-Vânturarița' 
ORDER BY method, date;


\copy (SELECT st_astext(obm_geometry) as wkt, species_valid, eunis, number as numar, observed_unit, gender, age, date, observers, method, grouping_code, sampling_unit, lpad((time_of_start / 60)::varchar, 2, '0') || ':' || lpad((time_of_start % 60)::varchar, 2, '0') as time_of_start, lpad((time_of_end / 60)::varchar, 2, '0') || ':' || lpad((time_of_end % 60)::varchar, 2, '0') as time_of_end, lpad((duration / 60)::varchar, 2, '0') || ':' || lpad((duration % 60)::varchar, 2, '0') as duration FROM elc WHERE projekt = 'Apuseni' AND species NOT IN ('Lucanus cervus') ORDER BY method, date) TO '/tmp/BirdObservations.csv' CSV HEADER;

------------------------------------------------------
-- ImpactsDistribution
------------------------------------------------------

SELECT 
    st_astext(obm_geometry) as wkt,
    split_part(COALESCE(impact_l4, COALESCE(impact_l3, COALESCE(impact_l2, impact_l1))), ' ', 1) as codul_impactului, 
    CASE impact_type when 'str_presiune' THEN 'P' WHEN 'str_amenintare' THEN 'A' ELSE '' END as tipul_impactului,
    ''::varchar as codul_speciei,
    ''::varchar as descrierea_localizarii, 
    CASE impact_intensity WHEN 'str_high' THEN 'R' WHEN 'str_medium' THEN 'M' WHEN 'str_low' THEN 'S' ELSE '' END as intensitatea_impactului, 
    'P' as confidentialitate, 
    impact_custom as alte_detalii, 
    'ROSPA0025' as codul_sitului, 
    0 as versiunea_planului_de_management 
FROM elc_threats 
WHERE project = 'Buila-Vânturarița';

\copy (SELECT st_astext(obm_geometry) as wkt, split_part(COALESCE(impact_l4, COALESCE(impact_l3, COALESCE(impact_l2, impact_l1))), ' ', 1) as impactcode, CASE impact_type when 'str_presiune' THEN 'P' WHEN 'str_amenintare' THEN 'A' ELSE '' END as type, ''::varchar as location, CASE impact_intensity WHEN 'str_high' THEN 'R' WHEN 'str_medium' THEN 'M' WHEN 'str_low' THEN 'S' ELSE '' END as intensity, 'P' as sensitive, ''::varchar as notes, 'ROSPA0081' as sitecode, 0 as managplan FROM elc_threats WHERE project = 'Apuseni') TO '/tmp/ImpactsDistribution.csv' CSV HEADER;



------------------------------------------------------
-- ObservationPoints
------------------------------------------------------

SELECT st_astext(sampling_unit_geometry) as wkt, method, program, grouping_code, sampling_unit FROM elc_sampling_units WHERE program LIKE '%apuseni%';
\copy (SELECT st_astext(sampling_unit_geometry) as wkt, method, program, grouping_code, sampling_unit FROM elc_sampling_units WHERE program LIKE '%apuseni%') to '/tmp/ObservationPoints.csv' CSV HEADER;
