CREATE TABLE tmp.nera_ap as SELECT localid, text, st_transform(geometry, 4326) as geometry  FROM shared.arii_protejate_20170829 WHERE localid IN ('RONPA0298', 'RONPA0294','RONPA0295','RONPA0003','RONPA0293','RONPA0296','RONPA0297','ROSCI0031','ROSPA0020');
CREATE INDEX nera_ap_gidx ON tmp.nera_ap USING gist (geometry);
------------------------------------------------------
-- SpeciesDistribution
------------------------------------------------------
create table tmp.nera_dist_sp as
SELECT 
    codeeunis, species, population, areah, areaunit, location, popmin, popmax, unit, quality, density, sensitive, notes, 
    string_agg(distinct localid, ',') as sitecode,
    managplan, coord_x_st70, coord_y_st70, lon, lat
FROM (
    SELECT DISTINCT 
        eunis as codeeunis, 
        species_valid AS species,
        'P' as population, 
        0.0 as areah, 
        'ha' as areaunit, 
        ''::varchar as location, 
        number as popmin, 
        coalesce(number_max, number) as popmax, 
        'i' as unit, 
        'medie' as quality, 
        ''::char(1) as density, 
        ''::char(1) as sensitive, 
        ''::varchar as notes, 
        ''::varchar as managplan,
        st_x(st_transform(obm_geometry, 31700)) as coord_x_st70,
        st_y(st_transform(obm_geometry, 31700)) as coord_y_st70,
        st_x(obm_geometry) as lon,
        st_y(obm_geometry) as lat,
        localid
    FROM elc 
    LEFT JOIN tmp.nera_ap ON ST_Intersects(obm_geometry, geometry)
    WHERE projekt = 'Nera-Beușnița' AND species_valid is NOT NULL AND species NOT IN ('null')
) as foo
group by codeeunis, species, population, areah, areaunit, location, popmin, popmax, unit, quality, density, sensitive, notes, managplan, coord_x_st70, coord_y_st70, lon, lat;

