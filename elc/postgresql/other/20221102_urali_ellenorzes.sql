WITH counts as (
    select 
        su.sampling_unit, count(distinct obm_id) 
    from elc_sampling_units su 
    left join elc e on 
        e.program = su.program AND 
        e.grouping_code = su.grouping_code AND 
        e.sampling_unit = su.sampling_unit and 
        extract(year from e.date) = 2022 
    where su.program = 'str_nagyhagymas_strura' group by su.sampling_unit
)
SELECT * FROM counts WHERE count = 0;

SELECT * FROM elc_search left join system.uploadings on id = obm_uploading_id WHERE word = 'SX_49';



UPDATE elc SET time_of_start = 1225, visibility = '>3', wind_force = '1', cloud_cover = '10' WHERE program = 'str_nagyhagymas_strura' AND sampling_unit = 'SX_49' AND date = '2022-10-26';
