SELECT DISTINCT projekt FROM elc;
SELECT DISTINCT count(*) FROM elc WHERE projekt = 'Magura Odobesti';
SELECT DISTINCT program, grouping_code, sampling_unit, nest_id FROM elc WHERE projekt = 'Magura Odobesti';


BEGIN; 
WITH x as (
    SELECT DISTINCT obm_id, nest_name, ROW_NUMBER() OVER() as rn FROM elc_nests WHERE program LIKE 'Magura Odobesti%' AND nest_name LIKE 'MO-IG%' ORDER BY nest_name
    )
SELECT * FROM x;
UPDATE elc_nests SET nest_name = 'MO-IG' || rn FROM x WHERE elc_nests.obm_id = x.obm_id;
COMMIT;

SELECT DISTINCT method, projekt, program, extract(year from date) from elc where method = 'str_nest_monitoring';
select observers, method, projekt, observed_unit, nest_id from elc where method = 'str_nest_monitoring' AND extract(year from date) = 2022 ORDER by observers;

BEGIN;
UPDATE elc SET projekt = 'Magura Odobesti' WHERE program = 'Magura Odobesti: Căutare cuiburi' AND projekt IS NULL;
UPDATE elc SET projekt = 'PN Porțile de Fier' WHERE program = 'PN Porțile de Fier: B31C Odvak/Scorburi' AND projekt IS NULL;
UPDATE elc SET projekt = 'PN Porțile de Fier' WHERE program = 'PN Porțile de Fier: Căutare cuiburi' AND projekt IS NULL;
COMMIT;