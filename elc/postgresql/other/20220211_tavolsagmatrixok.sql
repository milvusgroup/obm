WITH dta as (
    SELECT *, st_transform(obm_geometry, 31700) as geom_dp FROM elc WHERE projekt = 'Dobrogea de Nord' AND species = 'Lullula arborea'
    )
SELECT 
    DISTINCT ON(g1.obm_id) g1.obm_id AS ref_obm_id, g2.obm_id AS nn_obm_id, ST_Distance(g1.geom_dp,g2.geom_dp) AS distance
FROM dta As g1, dta As g2   
WHERE 
    g1.obm_id <> g2.obm_id
ORDER BY g1.obm_id, ST_Distance(g1.geom_dp,g2.geom_dp) 