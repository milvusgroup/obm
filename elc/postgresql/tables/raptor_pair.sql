CREATE TABLE elc.raptor_pair (
    id SERIAL,
    session_id integer NOT NULL,
    sampling_unit varchar NOT NULL,
    species varchar NOT NULL,
    min integer NOT NULL,
    max integer NOT NULL,
    status varchar NOT NULL,
    geometry geometry NULL,
    CONSTRAINT fk_session FOREIGN KEY(session_id) REFERENCES elc.raptor_survey_session(id)
);

ALTER TABLE ONLY elc.raptor_pair
    ADD CONSTRAINT raptor_pair_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON elc.raptor_pair TO elc_admin;
GRANT ALL on SEQUENCE elc.raptor_pair_id_seq to elc_admin ;

ALTER TABLE elc.raptor_pair ADD COLUMN oldid integer;
ALTER TABLE elc.raptor_pair ALTER COLUMN min DROP NOT NULL;
ALTER TABLE elc.raptor_pair ALTER COLUMN max DROP NOT NULL;
