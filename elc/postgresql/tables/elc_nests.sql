ALTER TABLE "elc_nests"
ADD CONSTRAINT "elc_nests_program_grouping_code_sampling_unit_nest_name_version" UNIQUE ("program", "grouping_code", "sampling_unit", "nest_name", "version"),
DROP CONSTRAINT "elc_nests_program_grouping_code_sampling_unit_nest_name";

ALTER TABLE elc_nests ADD COLUMN version integer;
ALTER TABLE "elc_nests" ALTER "version" TYPE integer, ALTER "version" SET DEFAULT '1', ALTER "version" SET NOT NULL;

ALTER TABLE elc_nests ADD COLUMN archive boolean NOT NULL DEFAULT false;

ALTER TABLE elc_nests ADD COLUMN nest_status character varying(32) NOT NULL DEFAULT 'new';

ALTER TABLE elc_nests RENAME COLUMN nest_status TO record_type;


ALTER TABLE elc_nests DROP COLUMN nest_status;
ALTER TABLE elc_nests DROP COLUMN nest_condition;

ALTER TABLE elc_nests ADD COLUMN projekt character varying NULL;