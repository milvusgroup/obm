ALTER TABLE public.elc_habitate_retezat SET SCHEMA elc;
ALTER TABLE elc.elc_habitate_retezat RENAME TO habitat;

ALTER TABLE elc.habitat RENAME COLUMN habitat to denumire;
ALTER TABLE elc.habitat ADD COLUMN cod_habitat varchar(32) NULL;
ALTER TABLE elc.habitat ADD COLUMN n2000 varchar(32) NULL;
