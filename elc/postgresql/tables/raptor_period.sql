CREATE TABLE elc.raptor_period (
    id SERIAL,
    period_name varchar NOT NULL,
    project varchar NOT NULL,
    program varchar NOT NULL,
    date_from date NOT NULL,
    date_until date NOT NULL
);
ALTER TABLE ONLY elc.raptor_period
    ADD CONSTRAINT raptor_period_id_pkey PRIMARY KEY (id);

GRANT ALL ON elc.raptor_period TO elc_admin;
GRANT ALL on SEQUENCE elc.raptor_period_id_seq to elc_admin ;

ALTER TABLE elc.raptor_period ADD COLUMN species_list varchar[];
