BEGIN;
    ALTER TABLE elc_am_event ADD COLUMN record_id integer;
    ALTER TABLE elc_am_event ADD CONSTRAINT record_fk FOREIGN KEY (record_id) REFERENCES elc_am_record (obm_id);
    ALTER TABLE elc_am_event ADD COLUMN start numeric;
    ALTER TABLE elc_am_event ADD COLUMN duration numeric;
    ALTER TABLE elc_am_event ADD COLUMN tag character varying NULL;
    ALTER TABLE elc_am_event ADD COLUMN observation_id integer NULL;
    ALTER TABLE elc_am_event ADD CONSTRAINT observation_fk FOREIGN KEY (observation_id) REFERENCES elc (obm_id);
    ALTER TABLE elc_am_event ADD COLUMN user_rid integer;
    ALTER TABLE elc_am_event ADD COLUMN created_at timestamp DEFAULT NOW();
    ALTER TABLE elc_am_event ADD COLUMN updated_at timestamp DEFAULT NOW();
    CREATE UNIQUE INDEX CONCURRENTLY elc_am_event_rid_start_uix ON elc_am_event (record_id, start);
    ALTER TABLE elc_am_event ADD CONSTRAINT elc_am_event_rid_start_uix UNIQUE USING INDEX elc_am_event_rid_start_uix;
COMMIT;
    
    
