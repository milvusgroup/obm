ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN building_type character varying NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN building_status character varying NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN building_location character varying NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN sample_collected smallint NULL;