CREATE TABLE elc.raptor_double (
    id SERIAL,
    session_id integer NOT NULL,
    min integer NOT NULL,
    max integer NOT NULL,
    status varchar NOT NULL,
    CONSTRAINT fk_session FOREIGN KEY(session_id) REFERENCES elc.raptor_survey_session(id)
);

ALTER TABLE ONLY elc.raptor_double
    ADD CONSTRAINT raptor_double_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON elc.raptor_double TO elc_admin;
GRANT ALL on SEQUENCE elc.raptor_double_id_seq to elc_admin;

ALTER TABLE elc.raptor_double ADD COLUMN oldid integer;
