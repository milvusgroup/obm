ALTER TABLE elc_am_session ADD COLUMN name character varying (32) NOT NULL;
ALTER TABLE elc_am_session ADD COLUMN from_date date NULL;
ALTER TABLE elc_am_session ADD COLUMN until_date date NULL;
ALTER TABLE elc_am_session ADD COLUMN placed_by character varying (128) NULL;
ALTER TABLE elc_am_session ADD COLUMN collected_by character varying (128) NULL;
ALTER TABLE elc_am_session ADD COLUMN "path" character varying (128) NULL;
CREATE UNIQUE INDEX CONCURRENTLY elc_am_session_name_uix ON elc_am_session (name);
ALTER TABLE elc_am_session ADD CONSTRAINT elc_am_session_name_uix UNIQUE USING INDEX elc_am_session_name_uix;
