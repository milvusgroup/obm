CREATE TABLE elc.rapmig_double_observation (
    double_id integer NOT NULL,
    obm_id integer NOT NULL,
    CONSTRAINT fk_double FOREIGN KEY(double_id) REFERENCES elc.rapmig_double(id) ON DELETE CASCADE,
    CONSTRAINT fk_observation FOREIGN KEY(obm_id) REFERENCES public.elc(obm_id) ON DELETE CASCADE
);

GRANT ALL ON elc.rapmig_double_observation TO elc_admin;