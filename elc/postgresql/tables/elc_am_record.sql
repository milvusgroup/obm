BEGIN;
ALTER TABLE elc_am_record ADD COLUMN session_id integer;
ALTER TABLE elc_am_record ADD CONSTRAINT session_fk FOREIGN KEY (session_id) REFERENCES elc_am_session (obm_id);
ALTER TABLE elc_am_record ADD COLUMN filename character varying (128);
ALTER TABLE elc_am_record ADD COLUMN recorded_at timestamp;
ALTER TABLE elc_am_record ADD COLUMN detection_file character varying (128) NULL;
CREATE UNIQUE INDEX CONCURRENTLY elc_am_record_sid_filename_uix ON elc_am_record (session_id, filename);
ALTER TABLE elc_am_record ADD CONSTRAINT elc_am_record_sid_filename_uix UNIQUE USING INDEX elc_am_record_sid_filename_uix;
ALTER TABLE elc_am_record ADD COLUMN listener integer NULL;
ALTER TABLE elc_am_record ADD COLUMN listening_finished boolean DEFAULT false;
COMMIT;

ALTER TABLE elc_am_record ADD COLUMN priority integer DEFAULT 0;