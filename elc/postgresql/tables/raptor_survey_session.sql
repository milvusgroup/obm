CREATE TABLE elc.raptor_survey_session (
    id SERIAL,
    period_id integer NOT NULL,
    grouping_code varchar NOT NULL,
    responsible integer NULL,
    finished boolean NOT NULL DEFAULT false,
    cmnt text NULL,
    CONSTRAINT fk_period FOREIGN KEY(period_id) REFERENCES elc.raptor_period(id)
);
ALTER TABLE ONLY elc.raptor_survey_session
    ADD CONSTRAINT raptor_survey_session_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON elc.raptor_survey_session TO elc_admin;
GRANT ALL on SEQUENCE elc.raptor_survey_session_id_seq to elc_admin ; 