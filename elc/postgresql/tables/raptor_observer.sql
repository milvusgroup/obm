CREATE TABLE elc.raptor_observer (
    id SERIAL,
    session_id integer NOT NULL,
    sampling_unit varchar NULL,
    observer integer NOT NULL,
    CONSTRAINT fk_session FOREIGN KEY(session_id) REFERENCES elc.raptor_survey_session(id)
);
ALTER TABLE ONLY elc.raptor_observer
    ADD CONSTRAINT raptor_observer_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON elc.raptor_observer TO elc_admin;
GRANT ALL on SEQUENCE elc.raptor_observer_id_seq to elc_admin;

