-- ALTER TABLE elc ADD COLUMN pair_id integer NULL;
-- ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN pair_id integer NULL;

ALTER TABLE elc ADD COLUMN species_valid character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN species_valid character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_upl ADD COLUMN species_valid character varying (64) NULL;
ALTER TABLE elc ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_upl ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE elc ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_upl ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE elc ADD COLUMN species_en character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN species_en character varying (64) NULL;
ALTER TABLE temporary_tables.elc_obm_upl ADD COLUMN species_en character varying (64) NULL;


ALTER TABLE elc ADD COLUMN traces_found character varying NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN traces_found character varying NULL;
ALTER TABLE temporary_tables.elc_obm_upl ADD COLUMN traces_found character varying NULL;

ALTER TABLE elc ADD COLUMN nest_renovation character varying NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN nest_renovation character varying NULL;
ALTER TABLE temporary_tables.elc_obm_upl ADD COLUMN nest_renovation character varying NULL;

ALTER TABLE elc ALTER COLUMN sample_collected TYPE boolean USING sample_collected::int::boolean;
ALTER TABLE temporary_tables.elc_obm_obsl ALTER COLUMN sample_collected TYPE boolean USING sample_collected::int::boolean;
ALTER TABLE temporary_tables.elc_obm_upl ALTER COLUMN sample_collected TYPE boolean USING sample_collected::int::boolean;

ALTER TABLE elc ALTER COLUMN flight_altitude TYPE character varying;
ALTER TABLE temporary_tables.elc_obm_obsl ALTER COLUMN flight_altitude TYPE character varying;
ALTER TABLE temporary_tables.elc_obm_obsl ALTER COLUMN flight_altitude TYPE character varying;

ALTER TABLE elc ADD COLUMN observed_previously character varying NULL;
ALTER TABLE temporary_tables.elc_obm_obsl ADD COLUMN observed_previously character varying NULL;
ALTER TABLE temporary_tables.elc_obm_upl ADD COLUMN observed_previously character varying NULL;
ALTER TABLE temporary_tables.elc_failed_uploads ADD COLUMN observed_previously character varying NULL;
