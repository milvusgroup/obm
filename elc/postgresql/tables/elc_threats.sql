ALTER TABLE elc_threats ADD COLUMN "obm_access" integer;
ALTER TABLE elc_threats ADD COLUMN "date" date;
ALTER TABLE elc_threats ADD COLUMN "site" character varying;
ALTER TABLE elc_threats ADD COLUMN "observers" character varying;
ALTER TABLE elc_threats ADD COLUMN "x" numeric;
ALTER TABLE elc_threats ADD COLUMN "y" numeric;
ALTER TABLE elc_threats ADD COLUMN "impact_type" character varying;
ALTER TABLE elc_threats ADD COLUMN "impact_l1" character varying;
ALTER TABLE elc_threats ADD COLUMN "impact_l2" character varying;
ALTER TABLE elc_threats ADD COLUMN "impact_l3" character varying;
ALTER TABLE elc_threats ADD COLUMN "impact_l4" character varying;
ALTER TABLE elc_threats ADD COLUMN "impact" character varying;
ALTER TABLE elc_threats ADD COLUMN "impact_custom" character varying;
ALTER TABLE elc_threats ADD COLUMN "impact_intensity" character varying;
ALTER TABLE elc_threats ADD COLUMN "time" time without time zone;
ALTER TABLE elc_threats ADD COLUMN "observations" character varying;
ALTER TABLE elc_threats ADD COLUMN "project" character varying;

BEGIN;
WITH categories as (    
    SELECT uid, trim(substr(word, 0, position('-' in word))) as cat, word from shared.amenintari
),
l1 as (    
    SELECT * FROM categories WHERE length(cat) = 1
)
INSERT INTO elc_search (data_table, subject, word, status)
SELECT 'elc_threats', 'impact_l1', word, 'accepted' FROM l1;
COMMIT;

BEGIN;
WITH categories as (    
    SELECT uid, trim(substr(word, 0, position('-' in word))) as cat, word from shared.amenintari
),
l1 as (    
    SELECT * FROM elc_search WHERE data_table = 'elc_threats' AND subject = 'impact_l1'
),
l2 as (    
    SELECT * FROM categories WHERE length(cat) = 3
)
INSERT INTO elc_search (data_table, subject, word, status, parent_id)
SELECT 'elc_threats', 'impact_l2', l2.word, 'accepted', l1.obm_id
FROM l2
LEFT JOIN l1 ON l2.cat LIKE trim(substr(l1.word, 0, position('-' in l1.word))) || '%';
COMMIT;

BEGIN;
WITH categories as (    
    SELECT uid, trim(substr(word, 0, position('-' in word))) as cat, word from shared.amenintari
),
l2 as (    
    SELECT * FROM elc_search WHERE data_table = 'elc_threats' AND subject = 'impact_l2'
),
l3 as (    
    SELECT * FROM categories WHERE length(cat) = 6
)
INSERT INTO elc_search (data_table, subject, word, status, parent_id)
SELECT 'elc_threats', 'impact_l3', l3.word, 'accepted', l2.obm_id
FROM l3
LEFT JOIN l2 ON l3.cat LIKE trim(substr(l2.word, 0, position('-' in l2.word))) || '%';
COMMIT;


BEGIN;
WITH categories as (    
    SELECT uid, trim(substr(word, 0, position('-' in word))) as cat, word from shared.amenintari
),
l3 as (    
    SELECT * FROM elc_search WHERE data_table = 'elc_threats' AND subject = 'impact_l3'
),
l4 as (    
    SELECT * FROM categories WHERE length(cat) = 9
)
INSERT INTO elc_search (data_table, subject, word, status, parent_id)
SELECT 'elc_threats', 'impact_l4', l4.word, 'accepted', l3.obm_id
FROM l4
LEFT JOIN l3 ON l4.cat LIKE trim(substr(l3.word, 0, position('-' in l3.word))) || '%';
COMMIT;
