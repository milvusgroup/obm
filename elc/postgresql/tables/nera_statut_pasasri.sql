ALTER TABLE elc_raportare_dist_specii_pg ADD COLUMN areah float NULL;
ALTER TABLE elc_raportare_dist_specii_pg ADD COLUMN areunit char(4) NULL;
UPDATE elc_raportare_dist_specii_pg SET areah = round((st_area(st_transform(obm_geometry, 3843)) / 10000)::numeric, 2), areunit = 'ha';
CREATE TABLE elc.statut_pasari_nera (
    uid SERIAL,
    specia varchar,
    statut_romania varchar NULL,
    statut_spa_sci varchar NULL,
    observatii text
);
ALTER TABLE elc.statut_pasari_nera ADD COLUMN prezenta varchar(15) NULL;
ALTER TABLE elc.statut_pasari_nera ADD COLUMN specia_elc varchar NULL;


CREATE TABLE elc.raportare_info_specii (
    uid SERIAL,
    projekt varchar,
    specia varchar,
    tipul_populatiei char(0),
    calitatea_datelor char(0),
    clasa_densitatii char(0),
    confidentialitate char(0)
);
ALTER TABLE elc.raportare_info_specii ADD COLUMN areah float NULL;
ALTER TABLE elc.raportare_info_specii ADD COLUMN areunit char(5) NULL;
ALTER TABLE elc.raportare_info_specii ADD COLUMN popmin integer NULL;
ALTER TABLE elc.raportare_info_specii ADD COLUMN popmax integer NULL;

CREATE TABLE elc.raportare_site_intersection (
    row_id integer,
    localid char(9)
);
INSERT INTO elc.raportare_site_intersection (row_id, localid) SELECT obm_id, localid FROM elc e left join shared.arii_protejate_20170829 ON st_intersects(obm_geometry, st_transform(geometry, 4326)) WHERE projekt IN ('Domogled Cerna', 'Buila-Vânturarița', 'Nera-Beușnița');
