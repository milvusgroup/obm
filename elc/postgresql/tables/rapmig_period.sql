CREATE TABLE elc.rapmig_period (
    id SERIAL,
    period_name varchar NOT NULL,
    project varchar NOT NULL,
    program varchar NOT NULL,
    date_from date NOT NULL,
    date_until date NOT NULL,
    species_list varchar[]
);
ALTER TABLE ONLY elc.rapmig_period
    ADD CONSTRAINT rapmig_period_id_pkey PRIMARY KEY (id);

GRANT ALL ON elc.rapmig_period TO elc_admin;
GRANT ALL on SEQUENCE elc.rapmig_period_id_seq to elc_admin ;

UPDATE elc.rapmig_period SET species_list = '{"Accipiter brevipes","Accipiter gentilis","Accipiter nisus","Aquila chrysaetos","Buteo buteo","Ciconia ciconia","Ciconia nigra","Circaetus gallicus","Circus aeruginosus","Circus cyaneus","Circus macrourus","Circus pygargus","Circus pygargus / macrourus","Clanga pomarina","Falco peregrinus","Falco subbuteo","Falco tinnunculus","Pandion haliaetus","Pernis apivorus","Pernis apivorus / Buteo buteo","Rapaces spec."}';