CREATE TABLE elc.raptor_pair_observation (
    pair_id integer NOT NULL,
    obm_id integer NOT NULL,
    CONSTRAINT fk_pair FOREIGN KEY(pair_id) REFERENCES elc.raptor_pair(id) ON DELETE CASCADE,
    CONSTRAINT fk_observation FOREIGN KEY(obm_id) REFERENCES public.elc(obm_id) ON DELETE CASCADE
);

GRANT ALL ON elc.raptor_pair_observation TO elc_admin;