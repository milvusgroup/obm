BEGIN;
  ALTER TABLE elc_am_tags ADD COLUMN tag varchar;
  ALTER TABLE elc_am_tags ADD COLUMN tag_category varchar;
  ALTER TABLE elc_am_tags ADD COLUMN species boolean;
  ALTER TABLE elc_am_tags ADD COLUMN breeding_status varchar;
  ALTER TABLE elc_am_tags ADD COLUMN gender varchar;
END;