CREATE TABLE elc.rapmig_double (
    id SERIAL,
    min integer NULL,
    max integer NULL,
    status varchar NOT NULL
);

ALTER TABLE ONLY elc.rapmig_double
    ADD CONSTRAINT rapmig_double_id_pkey PRIMARY KEY (id);
    
GRANT ALL ON elc.rapmig_double TO elc_admin;
GRANT ALL on SEQUENCE elc.rapmig_double_id_seq to elc_admin ;
