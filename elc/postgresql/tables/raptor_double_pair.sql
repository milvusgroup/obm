CREATE TABLE elc.raptor_double_pair (
    double_id integer NOT NULL,
    pair_id integer NOT NULL,
    CONSTRAINT fk_double FOREIGN KEY(double_id) REFERENCES elc.raptor_double(id) ON DELETE CASCADE,
    CONSTRAINT fk_pair FOREIGN KEY(pair_id) REFERENCES elc.raptor_pair(id) ON DELETE CASCADE
);

GRANT ALL ON elc.raptor_double_pair TO elc_admin;