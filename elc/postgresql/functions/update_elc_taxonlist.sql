CREATE OR REPLACE FUNCTION "update_elc_taxonlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
observer RECORD;
tbl varchar(64) = TG_TABLE_NAME;
sid integer;
sids integer[] DEFAULT '{}';
term varchar(128);
BEGIN
    IF tg_op = 'INSERT' THEN
        term := NEW.species;
        PERFORM add_term_elc(NEW.species, tbl, 'species',tbl,'taxon'::varchar);

        FOR observer IN (SELECT unnest(regexp_split_to_array(NEW.observers,E',\\s*')) as o) LOOP 
            SELECT add_term_elc(observer.o, tbl, 'observers'::varchar,tbl) INTO sid;
            sids := sids || sid;
        END LOOP;

        EXECUTE format('INSERT INTO %1$s_search_connect (data_table, row_id, observer_ids) VALUES (%1$L, %3$L, %2$L);', tbl, sids, NEW.obm_id);

        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  OLD.species != NEW.species THEN
            PERFORM remove_term(OLD.species, tbl, 'species',tbl,'taxon');
            PERFORM add_term_elc(NEW.species, tbl, 'species',tbl,'taxon'::varchar);
        END IF;

        IF  (OLD.observers != NEW.observers) THEN
            FOR observer IN (SELECT unnest(regexp_split_to_array(OLD.observers,E',\\s*')) as o) LOOP 
                PERFORM remove_term(observer.o, tbl, 'observers',tbl);
            END LOOP;
            FOR observer IN (SELECT unnest(regexp_split_to_array(NEW.observers,E',\\s*')) as n) LOOP 
                SELECT add_term_elc(observer.n, tbl, 'observers'::varchar,tbl) INTO sid;
                sids := sids || sid;
            END LOOP;

            EXECUTE format('UPDATE %1$s_search_connect SET observer_ids = %2$L WHERE data_table = %1$L AND row_id = %3$L;', tbl, sids, NEW.obm_id);

        END IF;

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        PERFORM remove_term(OLD.species, tbl, 'species',tbl,'taxon');

        EXECUTE format('DELETE FROM %1$s_search_connect WHERE data_table = %1$L AND row_id = %2$L;', tbl, OLD.obm_id);

        FOR observer IN (SELECT unnest(regexp_split_to_array(OLD.observers,E',\\s*')) as o) LOOP 
            PERFORM remove_term(observer.o, tbl, 'observers',tbl);
        END LOOP;

        RETURN OLD;

    END IF;                                                                                         
END
$$;
