CREATE OR REPLACE FUNCTION "add_term_elc" ("term" character varying(128), "tbl" character varying(64), "subj" character varying(128), "project" character varying(64) , "termstable" character varying(64) default NULL) RETURNS integer LANGUAGE plpgsql AS $$
DECLARE
sid integer;
BEGIN
    IF term IS NOT NULL AND tbl IS NOT NULL AND subj IS NOT NULL THEN
        IF termstable IS NULL THEN                
            EXECUTE format('SELECT obm_id FROM %s_search ms WHERE word = %L AND "ms"."subject" = %L AND data_table = %L;', project, term, subj, tbl) INTO sid;
            IF sid IS NULL THEN
                EXECUTE format('INSERT INTO %4$s_search (data_table,subject,meta,word,taxon_db) SELECT %2$L,%3$L,lower(replace(%1$L,'' '','''')),%1$L,1 RETURNING obm_id;',term, tbl, subj, project) INTO sid;
            END IF;
        ELSIF termstable = 'taxon' THEN
            EXECUTE format('SELECT taxon_id FROM %s_taxon WHERE word = %L;', project, term) INTO sid;
            IF (sid IS NULL) THEN
                EXECUTE format('INSERT INTO %2$s_taxon (lang,meta,word,taxon_db) SELECT %3$L,replace(%1$L,'' '',''''),%1$L,1 RETURNING taxon_id',term, project, subj);
            END IF;
        END IF;
        RETURN sid;
    END IF;
    RETURN 0;
END;
$$;

CREATE OR REPLACE FUNCTION "remove_term" ("term" character varying(128), "tbl" character varying(64), "subj" character varying(128), "project" character varying(64) , "termstable" character varying(64) default NULL) RETURNS void LANGUAGE plpgsql AS $$
BEGIN
	IF term IS NOT NULL AND tbl IS NOT NULL AND subj IS NOT NULL THEN
        IF termstable IS NULL THEN
            EXECUTE format('DELETE FROM %4$s_search WHERE data_table = %3$L AND subject = %1$L AND word = %2$L AND taxon_db = 0;',subj, term, tbl, project);
        ELSIF termstable = 'taxon' THEN
            EXECUTE format('DELETE FROM %2$s_taxon WHERE word = %1$L AND taxon_db = 0;', term, project);
        END IF;

	END IF;
END;
$$;
