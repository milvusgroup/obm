CREATE OR REPLACE FUNCTION "elc_refresh_materialized_views" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF TG_TABLE_NAME = 'elc_search' THEN
        REFRESH MATERIALIZED VIEW elc_sampling_units;
        REFRESH MATERIALIZED VIEW elc_grouping_codes;
        REFRESH MATERIALIZED VIEW elc_programs;
        REFRESH MATERIALIZED VIEW elc_projects;
    ELSIF TG_TABLE_NAME = 'elc_nests' THEN
        REFRESH MATERIALIZED VIEW elc_nest_data;
    END IF;
    RETURN NULL;
END
$$;
