CREATE OR REPLACE FUNCTION "update_elc_search_id" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF tg_op = 'INSERT' THEN
        new.search_id = new.obm_id;

        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        new.search_id = new.obm_id;

        RETURN NEW;

    ELSE
        RETURN NULL;

    END IF;
END
$$;
