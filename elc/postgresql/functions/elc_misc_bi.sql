CREATE OR REPLACE FUNCTION "public"."elc_distrib_ai" () RETURNS trigger AS
$$
DECLARE
    eunis varchar;
BEGIN

    IF (tg_op = 'INSERT' OR tg_op = 'UPDATE') and NEW.specia IS NOT NULL THEN
        SELECT e.word INTO new.codul_speciei FROM elc_taxon e LEFT JOIN elc_taxon tx ON e.taxon_id = tx.taxon_id WHERE e.lang = 'eunis' and tx.lang = 'species_valid' AND e.status = 'accepted' AND tx.status = 'accepted' AND tx.word = new.specia;

        RETURN new;

    ELSE

        RETURN new;

    END IF;


END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
