CREATE OR REPLACE FUNCTION "update_elc_phototags" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
tag RECORD;
tbl varchar(64) = TG_TABLE_NAME;
sid integer;
sids integer[] DEFAULT '{}';
term varchar(128);
BEGIN
    IF tg_op = 'INSERT' THEN

        FOR tag IN (SELECT unnest(regexp_split_to_array(NEW.tags,E',\\s*')) as o) LOOP 
            SELECT add_term_elc(tag.o, tbl, 'tags'::varchar,'elc') INTO sid;
            sids := sids || sid;
        END LOOP;

        EXECUTE format('INSERT INTO elc_search_connect (data_table, row_id, tags_ids) VALUES (%1$L, %3$L, %2$L);', tbl, sids, NEW.obm_id);

        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  (OLD.tags != NEW.tags) THEN
            FOR tag IN (SELECT unnest(regexp_split_to_array(OLD.tags,E',\\s*')) as o) LOOP 
                PERFORM remove_term(tag.o, tbl, 'tags',tbl);
            END LOOP;
            FOR tag IN (SELECT unnest(regexp_split_to_array(NEW.tags,E',\\s*')) as n) LOOP 
                SELECT add_term_elc(tag.n, tbl, 'tags'::varchar,'elc') INTO sid;
                sids := sids || sid;
            END LOOP;

            EXECUTE format('UPDATE elc_search_connect SET tags_ids = %2$L WHERE data_table = %1$L AND row_id = %3$L;', tbl, sids, NEW.obm_id);

        END IF;

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        EXECUTE format('DELETE FROM elc_search_connect WHERE data_table = %1$L AND row_id = %2$L;', tbl, OLD.obm_id);

        FOR tag IN (SELECT unnest(regexp_split_to_array(OLD.tags,E',\\s*')) as o) LOOP 
            PERFORM remove_term(tag.o, tbl, 'tags',tbl);
        END LOOP;

        RETURN OLD;

    END IF;                                                                                         
END
$$;
