CREATE OR REPLACE FUNCTION "public"."elc_nest_versions" () RETURNS trigger AS
$$
DECLARE 
    version integer;
BEGIN

    IF tg_op = 'INSERT' THEN
        IF new.record_type = 'update' THEN
            EXECUTE FORMAT('UPDATE elc_nests SET archive = true WHERE program = %L AND grouping_code = %L AND sampling_unit = %L AND nest_name = %L AND archive = false RETURNING version;',
                new.program, new.grouping_code, new.sampling_unit, new.nest_name
            ) INTO version;
            new.version = version + 1;
        ELSE
            new.version = 1;
        END IF;
        
        RETURN new;
    ELSE
        RETURN new;
    END IF;
END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
