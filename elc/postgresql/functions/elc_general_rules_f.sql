CREATE OR REPLACE FUNCTION "public"."elc_general_rules_f" () RETURNS trigger AS
$$

DECLARE

 tbl varchar;
 sens integer DEFAULT 0;

BEGIN

    tbl := TG_TABLE_NAME;
    IF tg_op = 'INSERT' THEN

        sens := new."obm_access";


        execute format('INSERT INTO elc_rules ("data_table","row_id","write","read","sensitivity")
                        SELECT ''%s'',%L,ARRAY[uploader_id],%L,%L FROM system.uploadings WHERE id = %L',tbl,new.obm_id,(SELECT uploader_id || "group" FROM system.uploadings WHERE id = new.obm_uploading_id),sens,new.obm_uploading_id);

        RETURN new;

    ELSIF tg_op = 'UPDATE' AND OLD."obm_access" != NEW."obm_access" THEN

        sens := new."obm_access";

        execute format('UPDATE elc_rules SET sensitivity = %L WHERE data_table = ''%s'' AND row_id = %L',sens,tbl,NEW.obm_id);

        RETURN new;

    ELSIF tg_op = 'DELETE' THEN

        execute format('DELETE FROM elc_rules WHERE data_table = ''%s'' AND row_id = %L',tbl,old.obm_id);
        RETURN old;

    ELSE

        RETURN new;

    END IF;


END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
