<?php
class create_symlinks extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('create_symlinks initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID, $path;
        
        $cmd = "SELECT project_table, reference, project FROM system.files f
	    	    LEFT JOIN system.file_connect fc ON fc.file_id = f.id
	    	    LEFT JOIN threats t ON t.obm_files_id = fc.conid
	    	    WHERE t.obm_files_id IS NOT NULL;";
        if (!$res = pg_query($ID, $cmd)) {
            job_log('query error', __FILE__, __LINE__);
            return false;
        }
        
        $reference_list = pg_fetch_all($res);
        $file_list = array_map(function ($p) {
           return basename($p); 
        }, glob($path . '/local/attached_files/*'));

        $missing_files = array_values(array_filter($reference_list, function ($ref) use ($file_list) {
            return (!in_array($ref['reference'], $file_list));
        }));

        foreach ($missing_files as $idx => $ref) {
            $target = $path . '/../' . $ref['project_table'] . '/local/attached_files/' . $ref['reference'];
            $link = $path . '/local/attached_files/' . $ref['reference'];
            array_map(function ($thmb) use ($path) {
                return symlink($thmb, $path . '/local/attached_files/thumbnails/' . basename($thmb));
            }, glob($path . '/../' . $ref['project_table'] . '/local/attached_files/thumbnails/*' . $ref['reference']));
            $missing_files[$idx] = symlink($target, $link);
        }

        //debug($missing_files, __FILE__, __LINE__);

    }
}
?>
