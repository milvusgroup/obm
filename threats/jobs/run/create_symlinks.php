<?php
$path = $argv[1];
$arg = $argv[2];

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'local_vars.php.inc');
require_once($path.'includes/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");

require_once($path.'includes/postgres_functions.php');
require_once($path.'includes/cache_functions.php');
require_once($path.'includes/common_pg_funcs.php');
require_once($path.'includes/default_modules.php');
require_once($path.'includes/admin_pages/jobs.php');
require_once($path.'jobs/run/lib/create_symlinks.php');

job_log('create_symlinks started');

if (!class_exists('create_symlinks')) {
    job_log("class create_symlinks does not exists\n");
    exit();
}
if (!method_exists('create_symlinks','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
call_user_func(['create_symlinks','run']);

?>
