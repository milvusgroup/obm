<?php

/**
 * Class results_asPDF_CM
 * @author yourname
 */
class results_asPDF_CM {
    /**
     * undocumented function
     *
     * @return void
     */
    public static function config_incendiere_vegetatie(){
        return [
            'useActiveForms' => true
        ];
    }

    public static function prepare_report_incendiere_vegetatie($de)
    {
        $st_col = $_SESSION['st_col'];
        $data = self::convert_data($de->as_array());

        
        try {
            return compact('data');
            
        } catch (DataPreparationException $e) {
            return [
                "error" => $e->getMessage()
            ];
        }
    }

    private static function convert_data($data) {
        $data = array_map(function ($row) {
            $row['on_n2000'] = ($row['sci'] != 'NA' || $row['spa'] != 'NA');
            $row['sci'] = ($row['sci'] == 'NA') ? null : $row['sci'];
            $row['spa'] = ($row['spa'] == 'NA') ? null : $row['spa'];
            if ($row['obm_files_id']) {
                $images = self::get_image_urls($row['obm_files_id']);
                $table = [];
                $nrows = ceil(count($images) / 2);
                for ($i=0; $i < $nrows; $i++) { 
                    $table_row = [];
                    $table_row[] = ['cells' => [$images[$i*2]]];
                    if (isset($images[$i*2+1])) {
                        $table_row[] = ['cells' => [ $images[$i*2+1] ]];
                    }
                    else {
                        $table_row[] = ['cells' => [[]]];
                    }
                    $table[] = ['rows' => $table_row];
                }
                $row['images'] = $table;
            }
            $row['map'] = [
                'bbox' => self::get_bbox($row['obm_id']),
                'obm_id' => $row['obm_id']
            ];
            $row['coordinates'] = self::get_coordinates($row['obm_id']);
            return $row;
        }, $data);
        
        # mark the last data to avoid the last pagebreak
        $data[count($data) - 1]['last'] = true;
        #debug($data, __FILE__, __LINE__);
        
        
        return $data;
    }

    private static function get_image_urls($obm_files_id) {
        global $ID;

        $cmd = "SELECT reference FROM system.file_connect fc LEFT JOIN system.files f ON fc.file_id = f.id where conid = %s;";
        $urls = [];

        if (!$res = pg_query($ID, sprintf($cmd, quote($obm_files_id)))) {
            log_action('query error', __FILE__, __LINE__);
            return $urls;
        }
        
        foreach (pg_fetch_all($res) as $file) {
            $urls[] = ['src' => self::resize_image($file['reference'], 600, 600)];
        }
        
        return $urls;
    }

    private static function get_bbox($id, $buffer = 3000) {
        global $ID;

        $cmd = sprintf("SELECT ST_Extent(ST_Buffer(ST_Transform(obm_geometry, 3857), %d)) from threats where obm_id = %d;", $buffer, $id);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return false;
        }

        $extent = pg_fetch_assoc($res);
        $bbox = str_replace(array('BOX(', ')'), '', $extent['st_extent']);
        $bbox = str_replace(' ', ',', $bbox);

        return $bbox;
    }

    private static function get_coordinates($id) {
        global $ID;

        $cmd = sprintf("WITH c as (
                SELECT st_centroid(obm_geometry) as centroid FROM threats where obm_id = %d
            )
            SELECT st_x(centroid) as x_4326, st_y(centroid) as y_4326, st_x(st_transform(centroid, 3844)) as x_3844, st_y(st_transform(centroid, 3844)) as y_3844 from c;",
            $id
        );
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return false;
        }

        $coordinates = pg_fetch_assoc($res);
        debug($coordinates, __FILE__, __LINE__);

        return $coordinates;
    }

    private static function resize_image($image_name, $w, $h) {
    
        $image_path = getenv('PROJECT_DIR') . "/local/attached_files/$image_name";
        
        if (!file_exists($image_path)) {
            return false;
        }

        list($width, $height) = getimagesize($image_path);
        $r = $width / $height;
        
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    
        $type = exif_imagetype($image_path);
    
        if ($type === 2) {
            $src = imagecreatefromjpeg($image_path); // For JPEG
        }
        elseif ($type === 3) {
            $src = imagecreatefrompng($image_path);   // For PNG
        }
        elseif ($type === 1) {
            $src = imagecreatefromgif($image_path);   // For GIF
        }
        else {
            log_action('unsopproted image type to resize');
            return false;
        }

        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        
        ob_start();
        imagejpeg($dst);
        $imageContent = ob_get_contents();
        ob_end_clean();
    
        return 'data:image/jpg;base64,' . base64_encode($imageContent);;
    }
}

class DataPreparationException extends Exception {
}
?>
