BEGIN;

CREATE SCHEMA IF NOT EXISTS threats;
--ALTER TABLE threats SET SCHEMA threats;

DROP VIEW IF EXISTS public.threats;

CREATE OR REPLACE VIEW public.threats AS
SELECT ROW_NUMBER () OVER() as obm_id, * FROM (
    SELECT 
        obm_geometry, obm_uploading_id, obm_validation, obm_comments, obm_modifier_id, obm_files_id, obm_access,
        'OBM' as project, obm_id as project_id, 
        date, site, observers, x, y, impact_type, impact_l1, impact_l2, impact_l3, impact_l4, impact_custom, impact_intensity, time,
        observations, threatened_species_group, management_measure, impact_general, reportable_impact, judet, comuna, sci, spa
    FROM milvus_threats
    LEFT JOIN milvus_threats_qgrids ON data_table = 'milvus_threats' AND obm_id = row_id
    UNION
    SELECT 
        obm_geometry, obm_uploading_id, obm_validation, obm_comments, obm_modifier_id, obm_files_id, obm_access,
        'OMM' as project, obm_id as project_id, 
        date, site, observers, x, y, impact_type, impact_l1, impact_l2, impact_l3, impact_l4, impact_custom, impact_intensity, time,
        observations, threatened_species_group, management_measure, impact_general, reportable_impact, judet, comuna, sci, spa
    FROM mammalia_threats
    LEFT JOIN mammalia_threats_qgrids ON data_table = 'mammalia_threats' AND obm_id = row_id
    UNION
    SELECT 
        obm_geometry, obm_uploading_id, obm_validation, obm_comments, obm_modifier_id, obm_files_id, obm_access,
        'OHM' as project, obm_id as project_id, 
        date, site, observers, x, y, impact_type, impact_l1, impact_l2, impact_l3, impact_l4, impact_custom, impact_intensity, time,
        observations, threatened_species_group, management_measure, impact_general, reportable_impact, judet, comuna, sci, spa
    FROM openherpmaps_threats
    LEFT JOIN openherpmaps_threats_qgrids ON data_table = 'openherpmaps_threats' AND obm_id = row_id
    UNION
    SELECT 
        obm_geometry, obm_uploading_id, obm_validation, obm_comments, obm_modifier_id, obm_files_id, obm_access,
        'THR' as project, obm_id as project_id, 
        date, site, observers, x, y, impact_type, impact_l1, impact_l2, impact_l3, impact_l4, impact_custom, impact_intensity, time,
        observations, threatened_species_group, management_measure, impact_general, reportable_impact, judet, comuna, sci, spa
    FROM threats.threats
    LEFT JOIN threats_qgrids ON data_table = 'threats' AND obm_id = row_id
) as foo ORDER BY obm_uploading_id;

DROP RULE IF EXISTS threats_ins ON threats;

CREATE RULE threats_ins AS ON INSERT TO public.threats
        DO INSTEAD
        INSERT INTO threats.threats (obm_geometry,obm_datum,obm_uploading_id,obm_validation,obm_comments,obm_modifier_id,obm_files_id,obm_access,date,site,observers,x,y,impact_type,impact_l1,impact_l2,impact_l3,impact_l4,impact_custom,impact_intensity,time,observations,threatened_species_group,management_measure,impact_general,reportable_impact) VALUES (NEW.obm_geometry,now(),NEW.obm_uploading_id,NEW.obm_validation,NEW.obm_comments,NEW.obm_modifier_id,NEW.obm_files_id,NEW.obm_access,NEW.date,NEW.site,NEW.observers,NEW.x,NEW.y,NEW.impact_type,NEW.impact_l1,NEW.impact_l2,NEW.impact_l3,NEW.impact_l4,NEW.impact_custom,NEW.impact_intensity,NEW.time,NEW.observations,NEW.threatened_species_group,NEW.management_measure,NEW.impact_general,NEW.reportable_impact) RETURNING
         obm_id::bigint,obm_geometry,obm_uploading_id,obm_validation,obm_comments,obm_modifier_id,obm_files_id,obm_access,NULL::text,NULL::integer,date,site,observers,x,y,impact_type,impact_l1,impact_l2,impact_l3,impact_l4,impact_custom,impact_intensity,time,observations,threatened_species_group,management_measure,impact_general::text,reportable_impact, NULL::varchar, NULL::varchar, NULL::varchar, NULL::varchar;


DROP RULE IF EXISTS threats_upd ON threats;

CREATE RULE threats_upd AS ON UPDATE TO public.threats
        WHERE NEW.project = 'THR'
        DO INSTEAD
        UPDATE threats.threats SET
            obm_geometry = NEW.obm_geometry,obm_uploading_id = NEW.obm_uploading_id,obm_validation = NEW.obm_validation,obm_comments = NEW.obm_comments,obm_modifier_id = NEW.obm_modifier_id,obm_files_id = NEW.obm_files_id,obm_access = NEW.obm_access,date = NEW.date,site = NEW.site,observers = NEW.observers,x = NEW.x,y = NEW.y,impact_type = NEW.impact_type,impact_l1 = NEW.impact_l1,impact_l2 = NEW.impact_l2,impact_l3 = NEW.impact_l3,impact_l4 = NEW.impact_l4,impact_custom = NEW.impact_custom,impact_intensity = NEW.impact_intensity,time = NEW.time,observations = NEW.observations,threatened_species_group = NEW.threatened_species_group,management_measure = NEW.management_measure,impact_general = NEW.impact_general,reportable_impact = NEW.reportable_impact 
        WHERE
            obm_id = NEW.project_id;

CREATE RULE threats_del AS ON DELETE TO public.threats
        WHERE OLD.project = 'THR'
        DO INSTEAD
        DELETE FROM threats.threats 
        WHERE
         obm_id = OLD.project_id;

ALTER SCHEMA threats OWNER TO threats_admin;
ALTER VIEW public.threats OWNER TO threats_admin;

GRANT SELECT ON milvus_threats TO threats_admin;
GRANT SELECT ON mammalia_threats TO threats_admin;
GRANT SELECT ON openherpmaps_threats TO threats_admin;
GRANT SELECT ON milvus_threats_qgrids TO threats_admin;
GRANT SELECT ON mammalia_threats_qgrids TO threats_admin;
GRANT SELECT ON openherpmaps_threats_qgrids TO threats_admin;

COMMIT;



