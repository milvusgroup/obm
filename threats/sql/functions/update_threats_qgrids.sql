CREATE OR REPLACE FUNCTION "update_threats_qgrids" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
 tbl varchar;
 geom geometry;
 centroid geometry;
 etrs10 RECORD;
BEGIN
        tbl := TG_TABLE_NAME;

        IF tg_op = 'INSERT' THEN

            centroid := ST_Centroid(new.obm_geometry);

            SELECT geometry,name INTO etrs10 FROM shared.etrs10 g WHERE ST_Intersects(centroid, g.geometry);

            execute format('INSERT INTO threats_qgrids ("data_table","row_id","original","etrs10_geom",etrs10_name,centroid) VALUES (%L,%L,%L,%L,%L,%L);',tbl,new.obm_id,new.obm_geometry,etrs10.geometry, etrs10.name, centroid);

            RETURN new; 

        ELSIF tg_op = 'UPDATE' AND ST_Equals(OLD."obm_geometry",NEW."obm_geometry") = FALSE THEN

            centroid := ST_Centroid(new.obm_geometry);

            SELECT geometry,name INTO etrs10 FROM shared.etrs10 g WHERE ST_Intersects(centroid, g.geometry);

            execute format('UPDATE threats_qgrids set "original" = %L, "etrs10_geom" = %L, etrs10_name = %L, centroid = %L WHERE data_table = %L AND row_id = %L;',new.obm_geometry,etrs10.geometry,etrs10.name,centroid,tbl,new.obm_id);
            RETURN new; 

        ELSIF tg_op = 'DELETE' THEN
            EXECUTE format('DELETE FROM threats_qgrids WHERE data_table = %L AND row_id = %L;',tbl,OLD.obm_id);
            RETURN old;
        ELSE
            RETURN new;
        END IF;
END; 
$$;

