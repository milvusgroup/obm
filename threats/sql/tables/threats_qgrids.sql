CREATE TABLE threats_qgrids ( 
    row_id integer NOT NULL, 
    data_table varchar NOT NULL,
    original geometry,
    centroid geometry,
    etrs10_geom geometry,
    etrs10_name varchar (64),
    judet varchar,
    comuna varchar,
    sci varchar,
    spa varchar,
    PRIMARY KEY (row_id, data_table)
);
CREATE INDEX threats_qgrids_centroid_idx ON threats_qgrids USING GIST (centroid);

INSERT INTO threats_qgrids (row_id, data_table, original) SELECT obm_id, 'threats', obm_geometry FROM threats.threats;

UPDATE threats_qgrids SET etrs10_geom = st_transform(foo.geometry,4326), etrs10_name = foo.name
FROM (
       SELECT d.obm_id,k.geometry,k.name FROM threats d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,st_transform(k.geometry,4326)))
) as foo
WHERE row_id=foo.obm_id AND data_table = 'threats';

CREATE TRIGGER "threats_qgrids_trigger" BEFORE INSERT OR UPDATE OR DELETE ON "threats"."threats" FOR EACH ROW EXECUTE PROCEDURE update_threats_qgrids();


WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.name FROM threats_qgrids rqg LEFT JOIN shared.romania_administrative_units g ON ST_Intersects(rqg.centroid,geometry_4326) WHERE rqg.judet IS NULL AND g.natlevel = '2ndOrder' ORDER BY row_id DESC ) UPDATE threats_qgrids qg SET judet = rows.name FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;
WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.name FROM threats_qgrids rqg LEFT JOIN shared.romania_administrative_units g ON ST_Intersects(rqg.centroid,geometry_4326) WHERE rqg.comuna IS NULL AND g.natlevel = '3rdOrder' ORDER BY row_id DESC ) UPDATE threats_qgrids qg SET comuna = rows.name FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;
WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.nume FROM threats_qgrids rqg LEFT JOIN shared.arii_protejate_20170829 g ON ST_Intersects(rqg.centroid,geometry) WHERE rqg.spa IS NULL AND g.tip_ap = 'Arie de protecție specială avifaunistică' ORDER BY row_id DESC ) UPDATE threats_qgrids qg SET spa = rows.nume FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;
WITH rows AS ( SELECT rqg.data_table, rqg.row_id, g.nume FROM threats_qgrids rqg LEFT JOIN shared.arii_protejate_20170829 g ON ST_Intersects(rqg.centroid,geometry) WHERE rqg.sci IS NULL AND g.tip_ap = 'Sit de importanță comunitară' ORDER BY row_id DESC ) UPDATE threats_qgrids qg SET sci = rows.nume FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;

