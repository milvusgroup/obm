#!/bin/bash

while inotifywait -r -e modify ./local; do
    rsync -avz ./local/ app:obm/obm-composer/local/threats/local/
done