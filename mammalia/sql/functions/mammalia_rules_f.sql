CREATE OR REPLACE FUNCTION "mammalia_rules_f" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
 sens integer DEFAULT 0;
BEGIN

    IF tg_op = 'INSERT' THEN

        IF new."acces" = 3 THEN
            sens := 3;
        ELSIF new."acces" = 2 THEN
            sens := 2;
        ELSIF new."acces" = 1 THEN
            sens := 1;
        ELSIF new."acces" = 0 THEN
            sens := 0;
        END IF;

        execute format('INSERT INTO mammalia_rules ("data_table","row_id","write","read","sensitivity") 
                        SELECT ''mammalia'',%L,ARRAY[uploader_id],%L,%L FROM system.uploadings WHERE id = %L',new.obm_id,(SELECT "group" FROM system.uploadings WHERE id = new.obm_uploading_id),sens,new.obm_uploading_id);
        RETURN new; 

    ELSIF tg_op = 'UPDATE' AND OLD."acces" != NEW."acces" THEN

        IF new."acces" = 3 THEN
            sens := 3;
        ELSIF new."acces" = 2 THEN
            sens := 2;
        ELSIF new."acces" = 1 THEN
            sens := 1;
        ELSIF new."acces" = 0 THEN
            sens := 0;
        END IF;
        execute format('UPDATE mammalia_rules SET sensitivity = %L WHERE row_id = %L',sens,NEW.obm_id);
        RETURN new; 
    ELSIF tg_op = 'DELETE' THEN
        execute format('DELETE FROM mammalia_rules WHERE row_id = %L',old.obm_id);
        RETURN old;
    ELSE
        RETURN new;
    END IF;
END; 
$$;