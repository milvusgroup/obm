CREATE OR REPLACE FUNCTION "mammalia_refresh_taxon_materialized_views" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW mammalia.mammalia_taxon_valid;
    REFRESH MATERIALIZED VIEW mammalia.mammalia_taxon_magyar;
    REFRESH MATERIALIZED VIEW mammalia.mammalia_taxon_roman;
    REFRESH MATERIALIZED VIEW mammalia.mammalia_taxon_angol;
    RETURN NULL;
END
$$;
