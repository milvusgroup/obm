CREATE OR REPLACE FUNCTION "update_mammalia_taxonlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
observer RECORD;
tbl varchar(64) = TG_TABLE_NAME;
sid integer;
sids integer[] DEFAULT '{}';
term varchar(128);
BEGIN
    IF tg_op = 'INSERT' THEN
        term := NEW.species;
        PERFORM add_term(term, tbl, 'species','mammalia','taxon'::varchar);

        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  OLD.species != NEW.species THEN
            PERFORM add_term(NEW.species, tbl, 'species','mammalia','taxon'::varchar);
        END IF;

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN
        
        -- azt hiszem ez már nem szükséges, mert a kézi karbantartás miatt nem kell törölni semmit
        --PERFORM remove_term(OLD.species, tbl, 'species','mammalia','taxon');

        RETURN OLD;

    END IF;                                                                                         
END;
$$;

-- INSERT INTO mammalia_search_connect (data_table, row_id, observers_ids) SELECT 'mammalia', foo.obm_id, array_agg(search_id) FROM (SELECT obm_id, unnest(regexp_split_to_array(concat_ws(',',observers),E'[,;]\\s*')) as names FROM mammalia ) as foo LEFT JOIN mammalia_search o ON names = word GROUP by foo.obm_id;

--UPDATE mammalia_search_connect sc SET species_ids = ARRAY[search_id] FROM (SELECT search_id, b.obm_id FROM mammalia b LEFT JOIN mammalia_search tx ON b.species = tx.word AND b.species IS NOT NULL WHERE b.obm_id > 221) as t WHERE sc.row_id = t.obm_id;
