-- régi nem használt függvény
CREATE OR REPLACE FUNCTION "mammalia_grid_insert" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
 tgeom geometry;
BEGIN
    IF new.method = 'str_10x10_presence' THEN
       SELECT g.geometry from milvus_grid g INNER JOIN mammalia_grid_data m on g.name = new.location INTO tgeom;
       IF NOT FOUND THEN 
           RAISE 'str_location_name_do_not_macth: %', new.location;
           RETURN NULL;
       END IF;
           IF tg_op = 'INSERT' THEN
               IF new.method='str_10x10_presence' AND new.obm_geometry IS NULL THEN
                   UPDATE mammalia_grid_data m SET obm_geometry = tgeom where m.obm_id = new.obm_id;
               END IF; 
               RETURN new; 
           END IF;
   
           IF tg_op = 'UPDATE' THEN
               IF new.method='str_10x10_presence' AND new.obm_geometry IS NULL THEN
                   UPDATE mammalia_grid_data SET obm_geometry = tgeom where m.obm_id = new.obm_id;
               END IF; 
               RETURN new; 
           END IF;
     END IF;
     RETURN new;
END; 
$$;