CREATE OR REPLACE FUNCTION mammalia_run_daily () RETURNS void LANGUAGE plpgsql AS $$
BEGIN
    DELETE FROM mammalia WHERE species_sci = 'Test';
END;
$$;
