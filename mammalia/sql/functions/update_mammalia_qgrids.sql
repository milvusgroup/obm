CREATE OR REPLACE FUNCTION "update_mammalia_qgrids" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
 tbl varchar;
 geom geometry;
 centroid geometry;
 etrs RECORD;
 cgrs RECORD;
BEGIN
        tbl := TG_TABLE_NAME;
        IF tg_op = 'INSERT' THEN

            geom := new."obm_geometry";
            centroid := ST_Centroid(geom);
            
            SELECT geometry,name INTO etrs FROM shared.etrs10 g WHERE ST_Intersects(centroid, g.geometry);
            SELECT geometry,cgrsname as name INTO cgrs FROM shared.cgrs50 g WHERE ST_Intersects(centroid, g.geometry);


            execute format('INSERT INTO %s_qgrids ("data_table","row_id","original","etrs10_geom",etrs10_name,cgrs50_geom,cgrs50_name, centroid) VALUES (%L,%L,%L,%L,%L,%L,%L, %L);',tbl,tbl,new.obm_id,new.obm_geometry,etrs.geometry, etrs.name,cgrs.geometry, cgrs.name, centroid);

            RETURN new; 

        ELSIF tg_op = 'UPDATE' AND ST_Equals(OLD."obm_geometry",NEW."obm_geometry") = FALSE THEN

            geom := new."obm_geometry";
            centroid := ST_Centroid(geom);
            
            SELECT geometry,name INTO etrs FROM shared.etrs10 g WHERE ST_Intersects(centroid, g.geometry);
            SELECT geometry,cgrsname as name INTO cgrs FROM shared.cgrs50 g WHERE ST_Intersects(centroid, g.geometry);
            
            execute format('UPDATE %s_qgrids set "original" = %L, "etrs10_geom" = %L, etrs10_name = %L, "cgrs50_geom" = %L, cgrs50_name = %L, centroid = %L WHERE data_table = %L AND row_id = %L;',tbl,new.obm_geometry, etrs.geometry, etrs.name, cgrs.geometry, cgrs.name, centroid, tbl, new.obm_id);
            RETURN new; 

        ELSIF tg_op = 'DELETE' THEN
            EXECUTE format('DELETE FROM %s_qgrids WHERE data_table = %L AND row_id = %L;',tblmammalia_qgrids_centroid_idx,tbl,OLD.obm_id);
            RETURN old;
        ELSE
            RETURN new;
        END IF;
END;
$$;