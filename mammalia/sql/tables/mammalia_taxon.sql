ALTER TABLE mammalia_taxon ADD COLUMN wid SERIAL;

\copy mammalia_taxon (taxon_id, word, lang, status) FROM '~/obm/milvus-custom-codes/mammalia/sql/tables/romaniaemloseiROHU.csv' CSV HEADER;
BEGIN; 
    DELETE FROM mammalia_taxon WHERE lang = 'species' AND status = 'undefined' AND word IN (SELECT word FROM mammalia_taxon WHERE lang != 'species' AND status = 'accepted');
END;
