ALTER TABLE mammalia_threats ADD COLUMN "obm_access" integer;
ALTER TABLE mammalia_threats ADD COLUMN "date" date;
ALTER TABLE mammalia_threats ADD COLUMN "site" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "observers" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "x" numeric;
ALTER TABLE mammalia_threats ADD COLUMN "y" numeric;
ALTER TABLE mammalia_threats ADD COLUMN "impact_type" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "impact_l1" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "impact_l2" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "impact_l3" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "impact_l4" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "impact_custom" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "impact_intensity" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "time" time without time zone;
ALTER TABLE mammalia_threats ADD COLUMN "observations" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "threatened_species_group" character varying;
ALTER TABLE mammalia_threats ADD COLUMN "management_measure" character varying;

