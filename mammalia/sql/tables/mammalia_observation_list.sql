DROP TABLE IF EXISTS "mammalia_observation_list";
DROP SEQUENCE IF EXISTS mammalia_observation_id_seq;
CREATE SEQUENCE mammalia_observation_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."mammalia_observation_list" (
    "id" integer DEFAULT nextval('mammalia_observation_id_seq') NOT NULL,
    "oidl" character varying(40),
    "uploading_id" integer,
    "obsstart" timestamp,
    "obsend" timestamp,
    "nulllist" boolean,
    CONSTRAINT "mammalia_observation_pkey" PRIMARY KEY ("id")
) WITH (oids = false);
