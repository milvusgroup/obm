ALTER TABLE mammalia_qgrids ADD COLUMN centroid point;
ALTER TABLE mammalia_qgrids ADD COLUMN judet varchar;
ALTER TABLE mammalia_qgrids ADD COLUMN comuna varchar;
ALTER TABLE mammalia_qgrids ADD COLUMN sci varchar;
ALTER TABLE mammalia_qgrids ADD COLUMN spa varchar;
ALTER TABLE mammalia_qgrids ADD COLUMN altit numeric;
ALTER TABLE mammalia_qgrids ADD COLUMN corine_code varchar(3);

INSERT INTO mammalia_qgrids (row_id, data_table, original) SELECT obm_id, 'mammalia_threats', obm_geometry FROM public.mammalia_threats;

UPDATE mammalia_qgrids SET original = foo.obm_geometry, centroid = st_centroid(foo.obm_geometry), etrs10_geom = st_transform(foo.geometry,4326), etrs10_name = foo.name, judet = NULL, comuna = NULL, sci = NULL, spa = NULL
FROM (
       SELECT d.obm_id, d.obm_geometry, k.geometry,k.name FROM mammalia_threats d LEFT JOIN shared."etrs10" k ON (st_within(d.obm_geometry,st_transform(k.geometry,4326)))
) as foo
WHERE row_id=foo.obm_id AND data_table = 'mammalia_threats';
