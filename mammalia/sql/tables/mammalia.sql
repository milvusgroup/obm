--ALTER TABLE mammalia ADD COLUMN obm_observation_list_id character varying NULL;
--ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN obm_observation_list_id character varying NULL;

-- 2020.10.27
ALTER TABLE mammalia ADD COLUMN species_sci character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN species_sci character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN species_sci character varying (64) NULL;
ALTER TABLE mammalia ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN species_hu character varying (64) NULL;
ALTER TABLE mammalia ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN species_ro character varying (64) NULL;
ALTER TABLE mammalia ADD COLUMN species_en character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN species_en character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN species_en character varying (64) NULL;

ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN hungarian_name character varying (32) NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN hungarian_name character varying (32) NULL;

ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN museum_name character varying (32) NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN museum_name character varying (32) NULL;

ALTER TABLE mammalia ADD COLUMN source character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN source character varying (64) NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN source character varying (64) NULL;

ALTER TABLE mammalia ADD COLUMN source_id integer NULL;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN source_id integer NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN source_id integer NULL;


ALTER TABLE mammalia ADD COLUMN exact_time time without time zone NULL;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN exact_time time without time zone NULL;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN exact_time time without time zone NULL;

ALTER TABLE mammalia.mammalia ADD COLUMN project varchar;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN project varchar;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN project varchar;

ALTER TABLE mammalia.mammalia ADD COLUMN program varchar;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN program varchar;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN program varchar;

ALTER TABLE mammalia.mammalia ADD COLUMN grouping_code varchar;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN grouping_code varchar;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN grouping_code varchar;

ALTER TABLE mammalia.mammalia ADD COLUMN sampling_unit varchar;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN sampling_unit varchar;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN sampling_unit varchar;

ALTER TABLE mammalia.mammalia ADD COLUMN visibility varchar;
ALTER TABLE temporary_tables.mammalia_obm_obsl ADD COLUMN visibility varchar;
ALTER TABLE temporary_tables.mammalia_obm_upl ADD COLUMN visibility varchar;