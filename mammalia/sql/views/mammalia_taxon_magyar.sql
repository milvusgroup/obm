DROP MATERIALIZED VIEW mammalia.mammalia_taxon_magyar;
CREATE MATERIALIZED VIEW mammalia.mammalia_taxon_magyar AS
SELECT DISTINCT ON (mammalia_taxon.taxon_id) mammalia_taxon.taxon_id,
    mammalia_taxon.word,
    mammalia_taxon.status
   FROM mammalia_taxon
  WHERE mammalia_taxon.lang = 'species_hu' AND mammalia_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON mammalia.mammalia_taxon_magyar (taxon_id);

GRANT SELECT ON mammalia.mammalia_taxon_magyar TO public; 
ALTER MATERIALIZED VIEW mammalia.mammalia_taxon_magyar OWNER TO mammalia_admin;
