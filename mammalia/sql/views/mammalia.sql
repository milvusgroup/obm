CREATE VIEW public.mammalia AS
    SELECT 
-- COLUMNS FROM THE MAIN TABLE
        d.obm_id,
        d.obm_uploading_id,
        d.x,
        d.y,
        d.species,
        d.variety,
        d.number,
        d.number_max,
        d.sex,
        d.age,
        d.unit,
        d.count_precision,
        d.hb,
        d.t,
        d.hf,
        d.e,
        d.weight,
        d.forearm_length,
        d.reprod_cond,
        d.marking,
        d.footprint_w,
        d.footprint_l,
        d.cause_of_death,
        d.age_of_carcass,
        d.comment,
        d.on_the_road,
        d.citation,
        d.inv_no,
        d.preparate_type,
        d.original_comment,
        d.obm_files_id,
        d.genbank_asc_no,
        d.sample_type,
        d.sample_id,
        d.predator,
        d.parasites,
        d.data_owner,
        d.doi,
        d.obm_geometry,
        d.observers,
        d.date,
        d.acces,
        d.obm_modifier_id,
        d.time_start,
        d.time_end,
        d.duration,
        d.waypoint_name,
        d.trap_no,
        d.trap_type,
        d.bait_type,
        d.trap_nights,
        d.net_length,
        d.pellet_species,
        d.no_of_pellets,
        d.observation_type,
        d.route_travelled_by,
        d.equipment_used,
        d.method,
        d.project,
        d.program,
        d.grouping_code,
        d.sampling_unit,
        d.obm_validation,
        d.obm_comments,
        d.location,
        d.number_of_traps,
        d.habitat,
        d.measured_by,
        d.collected,
        d.date_end,
        d.time_of_day,
        d.temperature,
        d.precipitation,
        d.wind,
        d.snow_depth,
        d.remarks,
        d.latimea_cursului,
        d.tip_apa_habitat,
        d.vegetatia_apa,
        d.panta_malului,
        d.substrat_mal,
        d.nivel_apa,
        d.tipul_vegetatiei,
        d.tipul_podului,
        d.suprafata_sub_pod,
        d.inaltimea_podului,
        d.latime_loc_marcare,
        d.substrat_loc_marcare,
        d.alte_constructii_hidrologice,
        d.asezare,
        d.trafic,
        d.perturbare,
        d.poluare,
        d.distanta_strabatuta,
        d.cautarea,
        d.prezenta_vidrei,
        d.colectat,
        d.metoda_inventariere_curgatoare,
        d.metoda_inventariere_statatoare,
        d.suprafata_apa,
        d.total_length,
        d.third_finger,
        d.original_id,
        d.life_code,
        d.visszafogás,
        d.camera_code,
        d.camera_type,
        d.photo_or_video,
        d.photo_video_no,
        d.adult,
        d.subadult,
        d.juvenile,
        d.adult_subadult,
        d.grassland_coverage,
        d.deciduous_scrub_cover,
        d.evergreen_scrub_cover,
        d.rocky_areas_coverage,
        d.obm_observation_list_id,
        vf2.word as species_sci,
        vf3.word as species_hu,
        vf5.word as species_ro,
        vf4.word as species_en,
        d.hungarian_name,
        d.museum_name,
        d.source,
        d.source_id,
        d.exact_time,
        d.collected_by,
        d.analysed_by,
        d.right_mandible,
        d.left_mandible,
        d.skull_or_maxilla,
        d.pellet_no,
        d.intravilan_extravilan,
        d.road_section,
        d.left_right_side,
        d.visibility
-- COLUMNS FROM THE JOINED TABLE
-- MAIN TABLE REFERENCE
    FROM mammalia.mammalia d
    LEFT JOIN mammalia_taxon vf1 ON d.species::text = vf1.word::text
    LEFT JOIN mammalia.mammalia_taxon_valid vf2 ON vf1.taxon_id = vf2.taxon_id
    LEFT JOIN mammalia.mammalia_taxon_magyar vf3 ON vf1.taxon_id = vf3.taxon_id
    LEFT JOIN mammalia.mammalia_taxon_angol vf4 ON vf1.taxon_id = vf4.taxon_id
    LEFT JOIN mammalia.mammalia_taxon_roman vf5 ON vf1.taxon_id = vf5.taxon_id;
-- JOIN STATEMENT
-- LEFT JOIN X vf ON (d.X = vf.X);

ALTER VIEW public.mammalia OWNER TO mammalia_admin;

-- materialized view mammalia.capreolus_project_species_list_species_sci depends on view mammalia
-- materialized view mammalia.capreolus_user_species_list_species_hu depends on view mammalia
-- materialized view mammalia.capreolus_project_species_list_species_hu depends on view mammalia
-- materialized view mammalia.capreolus_user_species_list_species_ro depends on view mammalia
-- materialized view mammalia.capreolus_project_species_list_species_ro depends on view mammalia
-- materialized view mammalia.capreolus_user_species_list_species_en depends on view mammalia
-- materialized view mammalia.capreolus_project_species_list_species_en depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary0_species_sci depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary0_species_hu depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary0_species_ro depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary0_species_en depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary_species_sci depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary_species_hu depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary_species_ro depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary_species_en depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary2_species_sci depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary2_species_hu depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary2_species_ro depends on view mammalia
-- materialized view mammalia.capreolus_observation_summary2_species_en depends on view mammalia
