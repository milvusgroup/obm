DROP VIEW mammalia_atlas_report;
CREATE OR REPLACE VIEW mammalia_atlas_report AS
SELECT
    ROW_NUMBER () OVER (ORDER BY species) as rn,
    foo.geom,
    foo.species,
    foo.cgrs,
    'RO' as country,
    CASE 
        WHEN year <= 1970 THEN 3
        WHEN year <= 1999 THEN 2
        ELSE 1
    END as status,
    year,
    '' as vagrant,
    'OMM' as source,
    'Zsolt Hegyeli' as submitter,
    '' as submitted
FROM (
    SELECT DISTINCT
        species_sci as species,
        cgrs50_name as cgrs, 
        cgrs50_geom as geom,
        extract(year from max(date)) as year
    FROM mammalia m
    LEFT JOIN mammalia_qgrids q ON m.obm_id = q.row_id AND data_table = 'mammalia'
    WHERE
        species_sci IS NOT NULL AND
        cgrs50_name IS NOT NULL
    GROUP BY species_sci, cgrs50_name, cgrs50_geom
) as foo;

GRANT ALL ON mammalia_atlas_report to zsolt_hegyeli_milvus_ro;
GRANT ALL ON mammalia_atlas_report to gabor_bone_milvus_ro;
