DROP MATERIALIZED VIEW mammalia.mammalia_taxon_valid;
CREATE MATERIALIZED VIEW mammalia.mammalia_taxon_valid AS
SELECT DISTINCT ON (mammalia_taxon.taxon_id) mammalia_taxon.taxon_id,
    mammalia_taxon.word,
    mammalia_taxon.status
   FROM mammalia_taxon
  WHERE mammalia_taxon.lang = 'species_sci' AND mammalia_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON mammalia.mammalia_taxon_valid (taxon_id);

GRANT SELECT ON mammalia.mammalia_taxon_valid TO public; 
ALTER MATERIALIZED VIEW mammalia.mammalia_taxon_valid OWNER TO mammalia_admin;
