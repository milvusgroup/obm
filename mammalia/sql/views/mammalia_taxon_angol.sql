DROP MATERIALIZED VIEW mammalia.mammalia_taxon_angol;
CREATE MATERIALIZED VIEW mammalia.mammalia_taxon_angol AS
SELECT DISTINCT ON (mammalia_taxon.taxon_id) mammalia_taxon.taxon_id,
    mammalia_taxon.word,
    mammalia_taxon.status
   FROM mammalia_taxon
  WHERE mammalia_taxon.lang = 'species_en' AND mammalia_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON mammalia.mammalia_taxon_angol (taxon_id);

GRANT SELECT ON mammalia.mammalia_taxon_angol TO public; 
ALTER MATERIALIZED VIEW mammalia.mammalia_taxon_angol OWNER TO mammalia_admin;
