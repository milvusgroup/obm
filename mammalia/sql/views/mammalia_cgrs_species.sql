CREATE OR REPLACE VIEW mammalia_cgrs_species AS
SELECT
    ROW_NUMBER () OVER (ORDER BY species) as rn,
    foo.species,
    foo.cgrs50_name,
    foo.geom
FROM (
    SELECT DISTINCT
        species_sci as species,
        cgrs50_name,
        cgrs50_geom as geom
    FROM mammalia m
    LEFT JOIN mammalia_qgrids q ON m.obm_id = q.row_id AND data_table = 'mammalia'
    WHERE 
        "date" >= '2000-01-01' AND species_sci IS NOT NULL
) as foo;

GRANT ALL ON mammalia_cgrs_species to zsolt_hegyeli_milvus_ro;