DROP MATERIALIZED VIEW mammalia.mammalia_taxon_roman;
CREATE MATERIALIZED VIEW mammalia.mammalia_taxon_roman AS
SELECT DISTINCT ON (mammalia_taxon.taxon_id) mammalia_taxon.taxon_id,
    mammalia_taxon.word,
    mammalia_taxon.status
   FROM mammalia_taxon
  WHERE mammalia_taxon.lang = 'species_ro' AND mammalia_taxon.status = 'accepted';
CREATE UNIQUE INDEX ON mammalia.mammalia_taxon_roman (taxon_id);

GRANT SELECT ON mammalia.mammalia_taxon_roman TO public; 
ALTER MATERIALIZED VIEW mammalia.mammalia_taxon_roman OWNER TO mammalia_admin;
