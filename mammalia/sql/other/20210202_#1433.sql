WITH species as 
SELECT word FROM mammalia_taxon WHERE lang = 'species_valid' AND status = 'accepted' AND word NOT IN ;

SELECT sci.taxon_id, sci.word, hu.word, ro.word, en.word, sci.wid as sci_wid, hu.wid as hu_wid, ro.wid as ro_wid, en.wid as en_wid FROM 
mammalia_taxon sci 
LEFT JOIN mammalia_taxon hu ON sci.taxon_id = hu.taxon_id AND hu.lang = 'species_hu' AND hu.status = 'accepted'
LEFT JOIN mammalia_taxon ro ON sci.taxon_id = ro.taxon_id AND ro.lang = 'species_ro' AND ro.status = 'accepted'
LEFT JOIN mammalia_taxon en ON sci.taxon_id = en.taxon_id AND en.lang = 'species_en' AND en.status = 'accepted'
WHERE sci.lang = 'species_sci' AND sci.status = 'accepted'
ORDER BY sci.taxon_id;