WITH a as ( 
    SELECT DISTINCT word, COUNT(*) FROM mammalia_taxon GROUP BY word HAVING count(*) > 1 
),
wids as (
    SELECT wid FROM mammalia_taxon t LEFT JOIN a ON a.taxon_id = t.taxon_id 
    WHERE a.taxon_id IS NOT NULL AND lang != 'species_sci' AND status = 'accepted'
)
SELECT * FROM mammalia_taxon WHERE wid IN (SELECT wid from wids);

BEGIN;
WITH asdf as (
SELECT wid, word, lang, ROW_NUMBER() OVER(PARTITION BY word ORDER BY lang DESC) as rn FROM mammalia_taxon GROUP BY wid, word, lang 
)
DELETE FROM mammalia_taxon WHERE wid IN (SELECT wid FROM asdf WHERE rn > 1);
ROLLBACK;
