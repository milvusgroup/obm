CREATE TRIGGER "mammalia_taxon_fill_meta" BEFORE INSERT OR UPDATE ON "mammalia_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "mammalia_search_fill_meta" BEFORE INSERT OR UPDATE ON "mammalia_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
