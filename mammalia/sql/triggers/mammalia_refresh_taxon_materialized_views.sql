CREATE TRIGGER "mammalia_refresh_taxon_materialized_views_tr" AFTER INSERT OR UPDATE OR DELETE ON "mammalia_taxon" FOR EACH STATEMENT EXECUTE PROCEDURE mammalia_refresh_taxon_materialized_views();
