CREATE TRIGGER "mammalia_qgrids_trigger" BEFORE DELETE OR INSERT OR UPDATE ON mammalia.mammalia FOR EACH ROW EXECUTE PROCEDURE public.update_mammalia_qgrids();
CREATE TRIGGER "mammalia_threats_qgrids_trigger" BEFORE DELETE OR INSERT OR UPDATE ON public.mammalia_threats FOR EACH ROW EXECUTE PROCEDURE public.update_mammalia_qgrids();

