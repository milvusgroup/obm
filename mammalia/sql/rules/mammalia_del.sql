CREATE RULE mammalia_del AS
    ON DELETE TO public.mammalia DO INSTEAD  DELETE FROM mammalia.mammalia
  WHERE (mammalia.obm_id = old.obm_id);
