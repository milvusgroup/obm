MAP
    NAME "mammalia-PRIVATE-MAP"
    SIZE 250 250
    STATUS ON
    UNITS DD

    EXTENT 20.3155848885023 43.6498278012919 29.8736903559214 48.3006906027268

    CONFIG MS_ERRORFILE "/tmp/mammalia_private_ms_error.txt"
    DEBUG 0
    CONFIG MS_ENCRYPTION_KEY "/var/lib/openbiomaps/data/maps/access.key"
    SHAPEPATH "/var/lib/openbiomaps/data/maps/"
    SYMBOLSET "/var/lib/openbiomaps/data/maps/symbols.txt"
    IMAGEQUALITY 95
    IMAGETYPE agg

    PROJECTION
        "init=epsg:4326"
    END

    WEB
        IMAGEPATH "/tmp/"
        IMAGEURL "/tmp/"
        TEMPLATE "/var/lib/openbiomaps/data/maps/empty.html"
        METADATA
            "wfs_name"           "mammalia"
            "ows_name"           "mammalia"
            "wms_name"           "mammalia"
            "wfs_title"          "mammalia"
            "wms_title"          "mammalia"
            "ows_title"          "mammalia"
            "wfs_enable_request"  "*"
            "wms_enable_request"  "*"
            "ows_enable_request"  "*"
            "wfs_encoding"        "UTF-8"
            "wms_encoding"        "UTF-8"
            "ows_encoding"        "UTF-8"
            "wms_onlineresource"  "http://openbiomaps.org/projects/mammalia/private/private.map"
        END
    END

    OUTPUTFORMAT
        NAME agg
        DRIVER agg/png
        IMAGEMODE rgb
    END

    LEGEND
        IMAGECOLOR 255 255 255
        STATUS on
        KEYSIZE 18 12
        LABEL
            TYPE bitmap
            SIZE medium
            COLOR 0 0 89
        END
    END

    #wms data point layer
    LAYER
        NAME "mammalia_points"
        STATUS on
        TYPE point
        CONNECTIONTYPE postgis
        CONNECTION "host=localhost dbname=gisdata port=5433 password={075ECCAA0E3EB11A} user=mammalia_admin options='--client_encoding=UTF8'"

        PROJECTION
            "init=epsg:4326"
        END

        METADATA
            #"wms_extent"           ""
            "wms_title"             "mammalia WMS layer"
            "wms_srs"               "epsg:4326 epsg:900913"
            "wms_enable_request"    "*"
        END

        VALIDATION
            "default_query"      ""
            "query"              ".*"
        END

        DATA "obm_geometry FROM (%query%) as new_table USING UNIQUE obm_geometry USING srid=4326"


        CLASS
            NAME 'point'
            EXPRESSION ([writable] = 0 and [selected] = 0)
            STYLE
                SYMBOL "CIRCLE"
                SIZE 8.0
                COLOR 130 130 230           # light blue circle
                OUTLINECOLOR 30 30 30
                WIDTH 1
               #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'point_own'
            EXPRESSION ([writable] = 1  and [selected] = 0)
            STYLE
                SYMBOL "CIRCLE"
                SIZE 10.0
                COLOR 210 5 5               # red circle
                OUTLINECOLOR 30 30 30
                WIDTH 1
                #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'selected_points'
            EXPRESSION ([selected] = 1) # selected / highlighted
            STYLE
                SYMBOL "circle"
                SIZE 14.0
                COLOR 250 210 0
                OUTLINECOLOR 30 30 30
                OUTLINEWIDTH 2
            END
        END

    END #wms data point layer




    #wms data lines layer
    LAYER
        NAME "mammalia_lines"
        STATUS on
        TYPE line
        CONNECTIONTYPE postgis
        CONNECTION "host=localhost dbname=gisdata port=5433 password={075ECCAA0E3EB11A} user=mammalia_admin options='--client_encoding=UTF8'"

        PROJECTION
            "init=epsg:4326"
        END

        METADATA
            #"wms_extent"           ""
            "wms_title"             "mammalia WMS layer"
            "wms_srs"               "epsg:4326 epsg:900913"
            "wms_enable_request"    "*"
        END

        VALIDATION
            "default_query"      ""
            "query"              ".*"
        END

        DATA "obm_geometry FROM (%query%) as new_table USING UNIQUE obm_geometry USING srid=4326"

        CLASS
            NAME 'line'
            EXPRESSION ([writable] = 0 and [selected] = 0)
            STYLE
                COLOR 130 130 230           # light blue
                OUTLINECOLOR 30 30 30
                WIDTH 3
                #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'line_own'
            EXPRESSION ([writable] = 1  and [selected] = 0)
            STYLE
                COLOR 210 5 5               # red
                OUTLINECOLOR 30 30 30
                WIDTH 3
                #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'selected_lines'
            EXPRESSION ([selected] = 1) # selected / highlighted
            STYLE
                COLOR 250 210 0
                OUTLINECOLOR 30 30 30
                WIDTH 3
            END
        END
    END #wms data lines layer


    #wms data polygons layer
    LAYER
        NAME "mammalia_polygons"
        STATUS on
        TYPE polygon
        CONNECTIONTYPE postgis
        CONNECTION "host=localhost dbname=gisdata port=5433 password={075ECCAA0E3EB11A} user=mammalia_admin options='--client_encoding=UTF8'"

        PROJECTION
            "init=epsg:4326"
        END

        METADATA
            #"wms_extent"           ""
            "wms_title"             "mammalia WMS layer"
            "wms_srs"               "epsg:4326 epsg:900913"
            "wms_enable_request"    "*"
        END

        VALIDATION
            "default_query"      ""
            "query"              ".*"
        END

        DATA "obm_geometry FROM (%query%) as new_table USING UNIQUE obm_geometry USING srid=4326"

        CLASS
            NAME 'polygon'
            EXPRESSION ([writable] = 0 AND [selected] = 0)
            STYLE
                COLOR 130 130 230           # light blue
                OUTLINECOLOR 30 30 30
                WIDTH 3
                #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'polygon_own'
            EXPRESSION ([writable] = 1  AND [selected] = 0)
            STYLE
                COLOR 210 5 5               # red
                OUTLINECOLOR 30 30 30
                WIDTH 3
                #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'selected_polygons'
            EXPRESSION ([selected] = 1) # selected / highlighted
            STYLE
                COLOR 250 210 0
                OUTLINECOLOR 30 30 30
                WIDTH 3
            END
        END


    END #data polygon layer

    #wms data grid layer
    LAYER
        NAME "mammalia_grid_polygons"
        STATUS on
        TYPE polygon
        CONNECTIONTYPE postgis
        CONNECTION "host=localhost dbname=gisdata port=5433 password={075ECCAA0E3EB11A} user=mammalia_admin options='--client_encoding=UTF8'"

        PROJECTION
            "init=epsg:4326"
        END

        METADATA
            #"wms_extent"           ""
            "wms_title"             "mammalia WMS layer"
            "wms_srs"               "epsg:4326 epsg:900913"
            "wms_enable_request"    "*"
        END

        VALIDATION
            "default_query"      ""
            "query"              ".*"
        END

        DATA "obm_geometry FROM (%query%) as new_table USING UNIQUE obm_geometry USING srid=4326"

        CLASS
            NAME 'polygon'
            EXPRESSION ([writable] = 0 AND [selected] = 0)
            STYLE
                COLOR 130 130 230           # light blue
                OUTLINECOLOR 30 30 30
                WIDTH 3
                #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'polygon_own'
            EXPRESSION ([writable] = 1  AND [selected] = 0)
            STYLE
                COLOR 210 5 5               # red
                OUTLINECOLOR 30 30 30
                WIDTH 3
                #MAXSCALEDENOM 165000
            END
        END
        CLASS
            NAME 'selected_polygons'
            EXPRESSION ([selected] = 1) # selected / highlighted
            STYLE
                COLOR 250 210 0
                OUTLINECOLOR 30 30 30
                WIDTH 3
            END
        END


    END #data grid polygon layer

    #wms data polygons layer
    LAYER
        NAME "layer_data_sci"
        STATUS on
        TYPE polygon
        CONNECTIONTYPE postgis
        CONNECTION "host=localhost dbname=gisdata port=5433 password={075ECCAA0E3EB11A} user=mammalia_admin options='--client_encoding=UTF8'"
    
        PROJECTION
            "init=epsg:4326"
        END
    
        METADATA
            #"wms_extent"           ""
            "wms_title"             "SCI"
            "wms_srs"               "epsg:4326 epsg:900913 epsg:3857"
            "wms_enable_request"    "*"
        END
    
        DATA "geometry FROM (SELECT geometry,localid || ' - ' || text as label FROM shared.sci) as new_table USING UNIQUE geometry USING srid=4326"

	LABELITEM 'label'
        CLASS
            NAME 'polygons'
            STYLE
		OPACITY 40
                COLOR 255 255 255
                OUTLINECOLOR 0 0 0
                WIDTH 0.5
            END
            LABEL
              COLOR 132 31 31
              TYPE TRUETYPE
              SIZE 12
              ANTIALIAS TRUE
              POSITION CL
              PARTIALS FALSE
              MINDISTANCE 300
	      MAXSCALEDENOM 200000
              BUFFER 4
              #SHADOWCOLOR 218 218 218  # prior to version 6
              #SHADOWSIZE 2 2  # prior to version 6
              STYLE    # since to version 6
                   GEOMTRANSFORM 'labelpoly'
                   COLOR 218 218 218
                   OFFSET 2 2
              END # STYLE
            END # end of label
        END
    END #data polygon layer

    #wms data polygons layer
    LAYER
        NAME "layer_data_pn"
        STATUS on
        TYPE polygon
        CONNECTIONTYPE postgis
        CONNECTION "host=localhost dbname=gisdata port=5433 password={075ECCAA0E3EB11A} user=mammalia_admin options='--client_encoding=UTF8'"
    
        PROJECTION
            "init=epsg:4326"
        END
    
        METADATA
            #"wms_extent"           ""
            "wms_title"             "SCI"
            "wms_srs"               "epsg:4326 epsg:900913 epsg:3857"
            "wms_enable_request"    "*"
        END
    
        DATA "geometry FROM (SELECT geometry,localid || ' - ' || text as label FROM shared.parcuri_nationale) as new_table USING UNIQUE geometry USING srid=4326"

	LABELITEM 'label'
        CLASS
            NAME 'polygons'
            STYLE
		OPACITY 40
                COLOR 255 255 255
                OUTLINECOLOR 0 0 0
                WIDTH 0.5
            END
            LABEL
              COLOR 132 31 31
              TYPE TRUETYPE
              SIZE 12
              ANTIALIAS TRUE
              POSITION CL
              PARTIALS FALSE
              MINDISTANCE 300
	      MAXSCALEDENOM 200000
              BUFFER 4
              #SHADOWCOLOR 218 218 218  # prior to version 6
              #SHADOWSIZE 2 2  # prior to version 6
              STYLE    # since to version 6
                   GEOMTRANSFORM 'labelpoly'
                   COLOR 218 218 218
                   OFFSET 2 2
              END # STYLE
            END # end of label
        END
    END #data polygon layer
    
END
